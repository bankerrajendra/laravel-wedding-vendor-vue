/***
	@ Backend.js - JS for admin module	
	# Created at @ 08Dec2019	
**/
 

 $(document).ready(function(){
	
	 /** show more infor user section **/
	 
	$(".show_more_info").on("click", function(e){
		e.preventDefault(); 
		var id = $(this).attr("id"); 
		
		$(".show_more_info_tr_"+id).slideToggle(function(){
			$(".rows").not(".show_more_info_tr_"+id).hide(); 
		});
	})
	
	/** users page [check/uncheck all] **/	
	$(".check_all").on("click", function(){
		var elm = $(this);
		if(elm.attr("action") !== "done"){
			elm.attr("action", "done");
			$('.checkable').prop('checked',true);
            $(".check_all").find("i").toggleClass("fa fa-circle", false);
            $(".check_all").find("i").toggleClass("fa fa-check", true);
		}else{
			$('.checkable').prop('checked',false);
			elm.removeAttr("action");
            $(".check_all").find("i").toggleClass("fa fa-check", false);
            $(".check_all").find("i").toggleClass("fa fa-circle", true);
		}
	})
 
	$("body").delegate(".checkable", "click", function(){	
		if($('.checkable:checked').length == $('.checkable').length){
			$('.init_batch').prop('checked',true);
			//$(".check_all").find("i").toggleClass("fa fa-circle fa fa-check");
		}else{
			$('.init_batch').prop('checked',false);
			//$(".check_all").find("i").toggleClass("fa fa-circle fa fa-check");
		}
        $(".check_all").find("i").toggleClass("fa fa-check", false);
        $(".check_all").find("i").toggleClass("fa fa-circle", true);
	});
	
	/*** Batch method on users pages **/	
	$("body").delegate(".init_batch","click", function(e)
	{
		e.preventDefault();
		var elm = $(this);
		var checked_profile = [];
		var _action = elm.attr("id");
		$("#tempMsg").remove();

		if(_action === "batch_feature"){
			elm.parent().after("<span id='tempMsg'><strong>Error : </strong>Batch Feature Disabled</span>");
			return;
		}

		$(".checkable:checked").each(function(){
			var check = $(this);
			checked_profile.push(check.attr("id"));			
		})

		/** Global ajax for all batch operations **/
		var server = location.protocol+"//"+location.host;
		if(location.host == 'www.muslimwedding.com' || location.host == 'www.punjabiwedding.com') {
			server += '/wedding-vendors';
		}

		$.ajax({
			url: server+"/ajax/batch_ops",
			method: "POST",
			data : "action="+_action+"&id="+checked_profile+"&_token="+$('input[name="_token"]').val(),
			success: function(result){
				$("#batch_label").html("<div class='alert alert-success'>Batch job completed successfully!</div>");
				setTimeout(function(){					
					window.location.reload();
				},200)
			}
		})
	})
})
