/***
	@__revoke element : implemented but not using
	@to activate revoked block, just remove [Revoke] attached with triggering element
	@Created at : 28Dec2018
	@Purpose : covering extra JS throuhout new UI integrations
**/
	$("#term_and_conditions-error").css({"color":"rgb(169, 68, 66)"});
(function() {
	$('.load-dependent').change(function(){
		if ($(this).val() != '') {
			var select = $(this).attr("id");
			var value = $(this).val();
			var dependent = $(this).attr('dependent');
			var _token = $('input[name="_token"]').val();

			var output;
			// show loading text for dropdown


			if (dependent == "state") {
				$('#city').empty(); //remove all child nodes
				$('#state').html('<option value="">Select State</option>');
				$('#city').html('<option value="">Select City</option>');
			} else if (dependent == "city") {
				$('#city').empty();
				$('#city').html('<option value="">Select City</option>');
			}
			
			$.ajax({
				url: route('ajax.fetchLocation'),
				method: "POST",
				data: {
					select: select,
					value: value,
					_token: _token,
					dependent: dependent
				},
				beforeSend: function () {
					// show loading image and disable the submit button
					if($('.'+dependent+'-dropdown-loader').length > 0) {
						$('.'+dependent+'-dropdown-loader').show();
					}
					$('#'+dependent).attr('disabled', true);
				},
				success: function(result) {
					for (var i = 0; i < result.length; i++) {
						output += "<option value=" + result[i].id + ">" + result[i].value + "</option>";
					}
					$('#' + dependent).append(output);
				},
				complete: function () {
					// show loading image and disable the submit button
					if($('.'+dependent+'-dropdown-loader').length > 0) {
						$('.'+dependent+'-dropdown-loader').hide();
					}
					$('#'+dependent).removeAttr('disabled');
				}
			})
		}
	});

    $(document).ready(function(e) {
        if ($("#FeedbackStat").length != 0  && sessionStorage.getItem("profile_unverified_popup_shown") != "yes" ) { 
            $('#FeedbackModal').modal({
                backdrop: 'static',
                keyboard: false
            })
        }
    });
    /** updating flaged user re-entry **/
	$('body').delegate("#updateFlagedUserre_entry_form", "submit", function(e){
		e.preventDefault();
        var url = route('ajax.updateFlagedUserre_entry');
        
		$.ajax({
			url:url,
			method:"POST",
			data:$("#updateFlagedUserre_entry_form").serialize(),
			success:function(result)
			{
                sessionStorage.setItem("profile_unverified_popup_shown","yes");
				$(".modal-footer").prepend("<span style='color:green'>Profile updated successfuly! Wait for admin to verify it again</span>");
				setTimeout(function(){
					$("#FeedbackModal").modal("hide");
					document.location.reload(true);
				},1500)
			}
		})
	});
})();
var ajaxShortList = null;
function doDeleteShortListed(vendor_id)
{
	if(confirm('Are you sure you want to delete this record?')) {
		var url = route('short-list-vendor-ajax')+'?vendor_id='+vendor_id+'&shortlist=0';
		ajaxShortList = $.ajax({
			url: url,
			method: 'GET',
			beforeSend: function () {
			},
			success: function(result) {
				if(result.status == 1) {
					$('#shortlist-id-'+vendor_id).fadeOut(300, function(){ 
						$(this).remove();
					});
				} else {
					console.log(result.message);
				}
			},
			error: function(xhr, status, error){
				var errors = xhr.responseJSON;
			},
			complete: function () {
				ajaxShortList = null;
			}
		});
	}
}
function forEachIn(iterable, functionRef) {
    for (var accessor in iterable) {
        functionRef(accessor, iterable[accessor]);
    }
}

function isIterable(element) {
    return isArray(element) || isObject(element);
}

function isArray(element) {
    return element.constructor == Array;
}

function isObject(element) {
    return element.constructor == Object;
}
function scroll_to_div(div_id)
{
	if($('#'+div_id).length > 0)
	{
		$('html, body').animate({
			scrollTop: $('#'+div_id).offset().top -55 }, 'slow');
	}
}
/** @end **/

 