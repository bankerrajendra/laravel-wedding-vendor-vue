/***
	@__revoke element : implemented but not using
	@to activate revoked block, just remove [Revoke] attached with triggering element
	@Created at : 28Dec2018
	@Purpose : covering extra JS throuhout new UI integrations
**/

(function() {

    $(document).ready(function() {
        /***
        	@Additional input. Others selection on Community & Subcasts 
        	@Triggers on change event matching any element with class supplied
        	@Output : additional adjacent input box
        **/
        $(".implementOthers").on("change", function() {
                var isOther = $(this).find("option:selected").text();
                if (isOther == "Others") {
                    var source = $(this).attr("id");
                    /** sub_cast || sect **/
                    var placeholder = "Please specify " +
                        source.charAt(0).
                    toUpperCase() +
                        source.slice(1).
                    replace("_", " ");

                    var InputBox = "<div class='sct form-group others'>";
                    InputBox += "<input type='text' name='other_" + source + "' class='form-control' placeholder='" + placeholder + "'>";
                    InputBox += "</div>";
                    $(this).parent().after(InputBox);
                } else {
                    $(".sct").remove();
                }
            })
            /* @end */

        /***
        	@Additional input. Others selection on Community & Subcasts 
        	@Triggers on change event matching any element with class supplied
        	@Output : additional adjacent input box
        **/
        $("#phone_number").on("change", function(e) {
            e.preventDefault();
            if ($(this).val() != "" && $("#phone_country_code").val() != "") {
                $("#netStepOnMobileInput").slideDown('slow');
            } else {
                $("#netStepOnMobileInput").hide('slow');
            }
        })

        $("#phone_country_code").on("change", function(e) {
            e.preventDefault();
            if ($("#phone_country_code").val() == "") {
                $(".phone_country_code_err").show();
                return;
            } else {
                $(".phone_country_code_err").hide();
            }
        })

        $("#country").on("change", function(e) {
            e.preventDefault();
            if ($("#country").val() == "") {
                $(".country_code_err").show();
                return;
            } else {
                $(".country_code_err").hide();
            }
        })

        $("#profession").on("change", function(e) {
            e.preventDefault();
            if ($("#profession").val() == "") {
                $(".profession_code_err").show();
                return;
            } else {
                $(".profession_code_err").hide();
            }
        })

        $("#sub_cast").on("change", function(e) {
            e.preventDefault();
            if ($("#sub_cast").val() == "") {
                $(".sub_cast_err").show();
                return;
            } else {
                $(".sub_cast_err").hide();
            }
        })

        $("#userInformationForm").on("submit", function() {
            var flag = 0;
            if ($("#food:checked").length < 1) {
                $(".food_err").show();
                // return;
                flag = 1;
            } else {
                $(".food_err").hide();
            }

            if ($("#smoke:checked").length < 1) {
                $(".smoke_err").show();
                flag = 1;
                // return;
            } else {
                $(".smoke_err").hide();
            }

            if ($("#drink:checked").length < 1) {
                $(".drink_err").show();
                flag = 1;
                // return;
            } else {
                $(".drink_err").hide();
            }

            if (flag == 1) {
                return false;
            }
        });

        // $("#email").on("keyup", function(){

        // var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        // if(!(re.test(String($(this).val()).toLowerCase()))){
        // $(".email_invalid_code_err").show();	
        // $("input[type='submit']").attr('disabled','disabled');
        // return;
        // }else{					
        // $(".email_invalid_code_err").hide();
        // $("input[type='submit']").removeAttr('disabled');
        // }
        // })

        $("#registrationForm").on("submit", function() {

            if ($("#phone_country_code").val() == "") {
                $(".phone_country_code_err").show();
                return;
            } else {
                $(".phone_country_code_err").hide();
            }

            /** Country valiaton **/
            if ($("#phone_number").val() != "") {
                $("#netStepOnMobileInput").slideDown('slow');
            } else {
                $("#netStepOnMobileInput").hide('slow');
            }
            /** @end **/

            /** Phone number validation **/
            if ($("#country").val() == "") {
                $(".country_code_err").show('slow');
            } else {
                $(".country_code_err").hide('slow');
            }
            /** @end **/

            /** Phone number validation **/
            if ($("#profession").val() == "") {
                $(".profession_code_err").show('slow');
            } else {
                $(".profession_code_err").hide('slow');
            }

            if ($("#sub_cast").val() == "") {
                $(".sub_cast_err").show('slow');
            } else {
                $(".sub_cast_err").hide('slow');
            }

            /** @end **/
        })

        if ($("#FeedbackStat").length != 0 && localStorage.getItem("profile_unverified_popup_shown") != "yes") {
            $('#FeedbackModal').modal({
                backdrop: 'static',
                keyboard: false
            })
        }

        /** AcceessGrantStatus validations **/
        if ($("#AcceessGrantStatus").val() != "" && $("#AcceessGrantStatus").val() == "N") {
            $(document).delegate("a", "click", function(e) {
                e.preventDefault();

                var elm = $(this);
                if (elm.text() !== " LogOut" && elm.attr("class") !== "navbar-brand") {

                    var customAttr = {
                        "data-toggle": "tooltip",
                        "data-placement": "bottom",
                        "title": "Your Access has been revoked"
                    };

                    $(elm).attr(customAttr);
                    $(elm).tooltip();
                } else {
                    window.location.href = elm.attr("href");
                    return;
                }
            })
        }
        /** @end **/
    });

})();
/** @end **/

$(document).ready(function() {
        $(".chosen_loader").remove();
        /** Why Loader? : without a loader, the default placeholder was showing a multiselect only.
        	loader will be a placeholder and once chosen libs are loaded, loader will be 
        	remove with actual dropdown content.
        **/
        $(".chosen-select").show(); 

        /** choosesn-search - partner profile page **/
        $('body').delegate(".dynamics", "change", function() {

            if ($(this).val() != '') {
                var select = $(this).attr("id");
                var value = $(this).val();
                var dependent = $(this).attr('dependent');
                var _token = $('input[name="_token"]').val();

                var output;
                if (dependent == "state") {
                    $('#state').html('<option value="">Select City</option>');
                }
                if (dependent == "city") {
                    $('#city').html('<option value="">Select City</option>');
                }
                $.ajax({
                    url: route('ajax.fetchLocation'),
                    method: "POST",
                    data: {
                        select: select,
                        value: value,
                        _token: _token,
                        dependent: dependent
                    },
                    success: function(result) {
                        for (var i = 0; i < result.length; i++) {
                            output += "<option value=" + result[i].id + ">" + result[i].value + "</option>";
                        }
                        $('#' + dependent).append(output);

                        $('#' + dependent).trigger("chosen:updated");
                    }

                })
            }
        });
        /** @end **/

        /** named search on dropdown **/
        function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }
        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);

        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {
                allow_single_deselect: true
            },
            '.chosen-select-no-single': {
                disable_search_threshold: 10
            },
            '.chosen-select-no-results': {
                no_results_text: 'Oops, nothing found!'
            },
            '.chosen-select-width': {
                width: "100%"
            }
        }

        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        $('.chosen-choices').click(function() {
           // setLiCenter();
        })

        function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }

        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);

        /** updating select box if current value = DM **/
        $(".chosen-select").on("change", function() {


            /** disabling on country/state/city **/
            if (!$(this).attr("no-filter")) {
                latest_selected=$("option:selected:last",this).val();
                console.log("latest : "+latest_selected);

                var target = $(this);
                var value = $(this).val();
                var filtered = [];

                if(latest_selected=="DM") {
                     // $(value).each(function (key, data) {
                     //     console.log(key + "--" + data);
                     //    if (data !== "DM") {
                     //        filtered = value.filter(function (value, index, arr) {
                     //            return value === "DM";
                     //        });
                        // } else {
                        //     //filtered.length=0;
                        //     filtered.push("DM");
                        //     //  return false;
                        // }
                    $(value).each(function (key, data) {
                        if (data === "DM") {
                            filtered.push("DM");
                        } else {
                            filtered = value.filter(function (value, index, arr) {
                                return value === "DM";
                            });
                        }
                    })
                    //console.log("ddd:"+latest_selected);
                } else {

                    /** modifying dropdown container **/
                    $(value).each(function (key, data) {
                        //console.log(key + "--" + data);
                        if (data !== "DM") {
                            filtered = value.filter(function (value, index, arr) {
                                return value !== "DM";
                            });
                        } else {
                            //filtered.length=0;
                            filtered.push("DM");
                            //  return false;
                        }
                    })
                }

                /** Updating choosen dropdown with modified data **/
                console.log(filtered);
                $(target).val(filtered);
                $(target).trigger("chosen:updated");
            }
        })

        /** @end **/
		
		/**
			@separate .slectpicker for my-profile country/state/city 
			@reason : partner preference country/state/city has .choosen class that mitigates DM+
			and same time it fires ajax to AjaxController where array() is sent.
			@ using same change event listener, this doesnt work on my-profile because 
			@it has .selectpicker with ajax data without array() or CSV
		**/
        $('body').delegate(".dynamic_differ", "change", function() {
            if ($(this).val() != '') {
                var select = $(this).attr("id");
                var value = $(this).val();
                var dependent = $(this).attr('dependent');
                var _token = $('input[name="_token"]').val();
                var output;
                if (dependent == "state") {
                    $('#state').html('<option value="">Select City</option>');
                } 
                if (dependent == "city") {
                    $('#city').html('<option value="">Select City</option>');
                } 
                $.ajax({
                    url: route('ajax.fetchLocation'),
                    method: "POST",
                    data: {
                        select: select,
                        value: value,
                        _token: _token,
                        dependent: dependent
                    },
                    success: function(result) {
                        for (var i = 0; i < result.length; i++) {
                            output += "<option value=" + result[i].id + ">" + result[i].value + "</option>";
                        }
                        $('#' + dependent).append(output);
                        $('.selectpicker').selectpicker('refresh');
                    }
                })
            }
        });
		
		
})
/** @end **/