(function ()
{
   var likeProfile = function(profileId, callback){
        var url = route('like-profile');
        var data = [];
        data['profileId'] = profileId;
        doAjax(url, data, function(response){
            callback(response);
        });
   };

   var shortlistProfile = function(profileId, callback){
       var url = route('shortlist-profile');
       var data = [];
       data['profileId'] = profileId;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var skipProfile = function(profileId, callback){
       var url = route('skip-profile');
       var data = [];
       data['profileId'] = profileId;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var deleteViewedMe = function(profileId, callback){
       var url = route('delete-viewed-me');
       var data = [];
       data['profileId'] = profileId;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var deleteShortlisted = function(profileId, shortlistId, callback){
       var url = route('delete-shortlisted');
       var data = [];
       data['profileId'] = profileId;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var likeBack = function(profileId, callback){
       var url = route('like-back');
       var data = [];
       data['profileId'] = profileId;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var deleteLiked = function(profileId, callback){
       var url = route('delete-liked');
       var data = [];
       data['profileId'] = profileId;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var deleteSkipped = function(profileId, callback){
       var url = route('delete-skipped');
       var data = [];
       data['profileId'] = profileId;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var sendMessage = function(profileId, message, reloadPage, id, callback){
       var url = route('send-message-view');
       var data = [];
       data['profileId'] = profileId;
       data['message'] = message;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var blockProfile = function(profileId, callback){
       var url = route('block-profile');
       var data = [];
       data['profileId'] = profileId;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var unblockUser = function(profileId, callback){
       var url = route('unblock-profile');
       var data = [];
       data['profileId'] = profileId;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

    var sharePhotos = function(profileId, callback){
        var url = route('share-photos');
        var data = [];
        data['profileId'] = profileId;
        doAjax(url, data, function(response){
            callback(response);
        });
    };

    var unsharePhotos = function(profileId, callback){
        var url = route('unshare-photos');
        var data = [];
        data['profileId'] = profileId;
        doAjax(url, data, function(response){
            callback(response);
        });
    };

    var requestToView = function(profileId, callback){
        var url = route('request-toview');
        var data = [];
        data['profileId'] = profileId;
        doAjax(url, data, function(response){
            callback(response);
        });
    };

    var sendPhotoRequest = function(profileId, callback){
        var url = route('send-photo-request');
        var data = [];
        data['profileId'] = profileId;
        doAjax(url, data, function(response){
            callback(response);
        });
    };

    var acceptRequestsPhotos = function(profileId, callback){
        var url = route('accept-photo-request');
        var data = [];
        data['profileId'] = profileId;
        doAjax(url, data, function(response){
            callback(response);
        });
    };

    var denyRequestsPhotos = function(profileId, callback){
        var url = route('deny-photo-request');
        var data = [];
        data['profileId'] = profileId;
        doAjax(url, data, function(response){
            callback(response);
        });
    };

   var isEmptyValue = function(data){
        if(data.replace(/^\s+|\s+$/g, "").length === 0)
            return true;
        else
            return false;
   };

   var toggleActivityNotification = function(profileId, value, callback){
       var url = route('alert-settings');
       var data = [];
       data['action'] = 'activity';
       data['profileId'] = profileId;
       data['value'] = value;
       doAjax(url, data, function(response){
           callback(response);
       });
   };

   var toggleInstantAlertNotification = function(profileId, value, callback){
       var data = [];
       var url = route('alert-settings');
       data['action'] = 'instant_alert';
       data['profileId'] = profileId;
       data['value'] = value;
       doAjax(url, data, function(response){
           callback(response);
       });
    };


    if(document.getElementById('add-review-section') instanceof Object) {
        // write code for review add page.
    } else {
        $('.btn-sm').click(function() {
            var action=$(this).data('action'); //show/hide
            var code=$(this).data('code');  //account
            var tableName=$(this).data('tb');
            var status='Y';
            var curobj='show';
            if(action=="show") {
                status = 'N';
                curobj = 'hide';
            }

            var fieldname=code.toLowerCase()+"_show";

            showHideAction(status, fieldname, tableName, function(response){
                if(response.status == 1){
                    jQuery('#'+action+ code).removeClass('btn-success');
                    jQuery('#'+action+ code).addClass('btn-default');
                    jQuery('#'+curobj+ code).removeClass('btn-default');
                    jQuery('#'+curobj+ code).addClass('btn-success');
                }
            });

        });
    }

    var showHideAction = function(action, code, tablename, callback){
        var url = route('settings-showhide');
        var data = [];
        data['status'] = action;
        data['fieldname'] = code;
        data['tablename'] = tablename;
        doAjax(url, data, function(response){
            callback(response);
        });
    };

   var doAjax = function(url, data, callback){
      var _token = $('input[name="_token"]').val();
      var form = new FormData();
       form.append('_token', _token);
       //console.log(data);
        for(var k in data){
            if (data.hasOwnProperty(k)) {
                form.append(k, data[k]);
            }
        }
       //console.log(form);
       jQuery.ajax({
           url: url,
           data: form,
           method: "post",
           processData: false,
           contentType: false
       }).done(function(response) {
           callback(response);
       });
   };

   jQuery(".skipProfile").on('click', function(){
        var profileId = jQuery(this).data('profileid');
       var reloadPage = false;
       var favouritedmeid = 0;
       var popup = document.getElementById("myPopup");

       var connectpage = false;
       if(jQuery(this).data('connectpage')) {
           connectpage = jQuery(this).data('connectpage');
       }

       if(jQuery(this).data('reloadpage')){
           reloadPage = true;
           popup.classList.toggle("show");
           setTimeout(function() {
               document.getElementById("myPopup").classList.toggle("show");
           }, 2000);
       }
       if(jQuery(this).data('favouritedmeid')){
           favouritedmeid = jQuery(this).data('favouritedmeid');
       }
        skipProfile(profileId, function(response){
            if(reloadPage || connectpage){
                location.reload();
            }
            else{
                jQuery("div#favouritedme-"+favouritedmeid).remove();
                if(response[0].remainingLikedMe == 0){
                    var html = "<div class=\"col-sm-12 panel-body \"><div class=\"panel panel-default\"><div class=\"panel-body\"><div class=\"alert alert-info\" role=\"alert\">No record found.</div></div></div></div>";
                    //jQuery(".container-pad").append(html);
                    jQuery("#menu2").append(html);
                }
            }
        });
   });

    $('.1btn-sm').click(function() {
        var action=jQuery(this).data('action'); //show/hide
        var code=jQuery(this).data('code');  //account
        settingShowHide(action, code, function (response) {
            if(action=="show") {
                alert(jQuery(this).parent());
                //jQuery(this).hasClass('btn-default')
                //btn-success
            }
        })

    });


   jQuery("#shortlistProfile").on('click', function(){
       var profileId = jQuery(this).data('profileid');
       shortlistProfile(profileId, function(response){
           location.reload();
       })
   });

   jQuery(".likeProfile").on('click', function(){

       var profileId = jQuery(this).data('profileid');
       var shortlistId = 0;
       var reloadPage = false;
       var sectionType="likeProfile";

       if(jQuery(this).data('reloadpage')){
           reloadPage = true;
       }
       if(jQuery(this).data('type') && jQuery(this).data('type')=="mobile"){
            sectionType="likeProfiles";
       }
       
       if(jQuery(this).data('likeid')) {
           shortlistId = jQuery(this).data('likeid');
       }

       likeProfile(profileId, function(response){
           if(reloadPage){

               if(response[0].status == 'deleted') {
                   // jQuery("#"+sectionType).find("i").removeClass('fa-heart');
                   // jQuery("#"+sectionType).find("i").css( "color", "" );
                   // jQuery("#"+sectionType).find("i").addClass('fa-heart-o');
                   jQuery("."+sectionType).find("i").removeClass('fa-heart');
                   jQuery("."+sectionType).find("i").css( "color", "" );
                   jQuery("."+sectionType).find("i").addClass('fa-heart-o');
               } else {
                   //console.log(jQuery("#likeProfile").find("i"));
                   // jQuery("#"+sectionType).find("i").addClass('fa-heart');
                   // jQuery("#"+sectionType).find("i").css("color", "red");
                   // jQuery("#"+sectionType).find("i").removeClass('fa-heart-o');
                   jQuery("."+sectionType).find("i").addClass('fa-heart');
                   jQuery("."+sectionType).find("i").css("color", "red");
                   jQuery("."+sectionType).find("i").removeClass('fa-heart-o');
                   //location.reload();
               }
           }
           else{
               if(response[0].remainingShortlisted == 0){
                   location.reload();
               }else{
                   jQuery("div#like-"+shortlistId).remove();
               }
           }
       });
   });

    jQuery(".deleteViewedMe").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        var viewid = jQuery(this).data('viewid');
        deleteViewedMe(profileId, function(response){
            if(response[0].remainingViewedMe == 0){
                location.reload();
            }else{
                jQuery("div#view-"+viewid).remove();
            }
        });
    });

    jQuery(".deleteShortlisted").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        var shortlistId = jQuery(this).data('shortlistid');
        deleteShortlisted(profileId, shortlistId, function(response){
            if(response[0].remainingShortlisted == 0){
                location.reload();
            }else{
                jQuery("div#shortlist-"+shortlistId).remove();
            }
        });
    });

    jQuery(".likeBack").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        var buttonRef = jQuery(this);
        var favmeId = jQuery(this).data('favouritedmeid');
        likeBack(profileId, function(response){
            buttonRef.prop('disabled', true);
           // jQuery("#likedBackModal").modal('show');
           // jQuery("#sendMessageButton").attr("href", "/conversation/"+profileId);
            jQuery("#favouritedme-"+favmeId).remove();
            if(response[0].remainingLikedMe == 0){
                var html = "<div class=\"col-sm-12 panel-body \"><div class=\"panel panel-default\"><div class=\"panel-body\"><div class=\"alert alert-info\" role=\"alert\">No record found.</div></div></div></div>";
                //jQuery(".container-pad").append(html);
                jQuery("#menu2").append(html);
            }
        });
    });

    jQuery(".deleteLiked").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        var likeId = jQuery(this).data('likeid');
        deleteLiked(profileId, function(response){
            jQuery("div#like-"+likeId).remove();
            if(response[0].remainingFavourited == 0) {
                //location.reload();
                var html = "<div class=\"col-sm-12 panel-body \"><div class=\"panel panel-default\"><div class=\"panel-body\"><div class=\"alert alert-info\" role=\"alert\">No record found.</div></div></div></div>";
                //jQuery(".container-pad").append(html);
                jQuery("#menu1").append(html);
            }
        });
    });

    jQuery(".deleteSkippedProfile").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        var likeId = jQuery(this).data('skipid');
        deleteSkipped(profileId, function(response){
            if(response[0].remainingSkipped == 0){
                location.reload();
            }else{
                jQuery("div#skip-"+likeId).remove();
            }
        });
    });

    jQuery(".blockProfile").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        blockProfile(profileId, function(response){
            location.reload();
        });
    });

    jQuery(".unblockProfile").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        var blockId = jQuery(this).data('blockid');
        unblockUser(profileId, function(response){
            location.reload();
            // if(response[0].remainingBlocked == 0){
            //     location.reload();
            // }else{
            //     jQuery("div#block-"+blockId).remove();
            // }
        });
    });


    jQuery(".sharePhotos").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        sharePhotos(profileId, function(response){
            //location.reload();
            window.location.href=decodeURIComponent(route('conversation', profileId));
        });
    });

    jQuery(".requestToView").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        requestToView(profileId, function(response){
            location.reload();
        });
    });

    jQuery(".sendPhotoRequest").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        sendPhotoRequest(profileId, function(response){
            location.reload();
        });
    });

    jQuery(".unsharePhotos").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        var blockId = jQuery(this).data('blockid');
        unsharePhotos(profileId, function(response){
            location.reload();
        });
    });

    jQuery(".acceptRequestsPhotos").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        var blockId = jQuery(this).data('blockid');
        acceptRequestsPhotos(profileId, function(response){
            location.reload();
        });
    });

    jQuery(".denyRequestsPhotos").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        var blockId = jQuery(this).data('blockid');
        denyRequestsPhotos(profileId, function(response){
            location.reload();
        });
    });


    jQuery(".sendMessage").on('click', function(){
        var profileId = jQuery(this).data('profileid');

        var reloadpage=jQuery(this).data('reloadpage');
        //console.log(reloadpage);
        //console.log(jQuery(this).data('reloadpage')=="connect");
        var id = jQuery(this).data('id');
        jQuery("#returnSpan" + id).html('');
        jQuery("#returnSpanconnect" + id).html('');

        var message='';
        if(jQuery(this).data('mobilepage')=="connectsub") {
            message = jQuery("#sendMessageInputconnect"+id).val();
        }         else {
            message = jQuery("#sendMessageInput"+id).val();
        }

        if(!isEmptyValue(message)){
            $("#sendMessageInput"+id).prop('disabled', true);
            if(jQuery(this).data('reloadpage')=="connect") {
                reloadPage = "connect";
            }
            else if(jQuery(this).data('reloadpage')){
                reloadPage = true;
            }else{
                reloadPage = false;
            }

            sendMessage(profileId, message,reloadPage, id, function(response){

                //console.log(reloadPage);
                if(reloadPage=="connect") {
                    jQuery("#sendMessageInputconnect"+id).val('');

                    jQuery("#sendMessageInput"+id).val('');
                    if(response.status == 0) {
                        jQuery("#returnSpan" + id).html(response.message);
                        jQuery("#returnSpanconnect" + id).html(response.message);
                    } else {
                        $('#qnimate').removeClass('popup-box-on');
                        document.getElementById('myModal').style.display = "none";

                        $("#sendMessageInput"+id).prop('disabled', false);
                        $("#sendMessageInputconnect"+id).prop('disabled', false);
                    }
                } else if(reloadPage==1){
                    location.reload();
                }
                else{
                    $("#sendMessageInput"+id).prop('disabled', false);
                    jQuery("#sendMessageInput"+id).val('');
                    console.log(response.status);
                    if(response.status == 0) {

                        jQuery("#returnSpan" + id).html(response.message);
                    } else {
                        $('#span-show-view-msg-'+id).css('display','block');
                        $('#span-show-send-msg-'+id).remove();
                        $(".close").click();
                        $('#qnimate-'+id).removeClass('popup-box-on');

                    }
                }

            });

        } else {
            jQuery("#returnSpan" + id).html('Message is required');
            jQuery("#returnSpanconnect" + id).html('Message is required');
        }
    });

    $(".sendMessageInput").each(function(){
        $(this).on('keyup', function (e) {
            var id = $(this).data('id');
            if (e.keyCode == 13) {
                jQuery("#sendMessage"+id).trigger('click');
            }
        });
    });

    $("#activity_notification").on('change', function(){
        var value = 0;
        var profileId = $(this).data('profileid');
        if($(this).is(":checked")){
            value = 1;
        }
        toggleActivityNotification(profileId, value, function(response){});
    });

    $("#instant_alert_notification").on('change', function(){
        var value = 0;
        var profileId = $(this).data('profileid');
        if($(this).is(":checked")){
            value = 1;
        }
        toggleInstantAlertNotification(profileId, value, function(response){});
    });

    jQuery(".shortlistProfilePop").on('click', function(){
        var profileId = jQuery(this).data('profileid');
        shortlistProfile(profileId, function(response){
            $('#qnimate').removeClass('popup-box-on');
            $('.personal-detail-button').removeClass('show_b');
        })
    });

    jQuery(".likeProfilePop").on('click', function(){

        var profileId = jQuery(this).data('profileid');
        var pop= document.getElementById("myPopup1");
        pop.classList.toggle("show1");
        setTimeout(function() {
            document.getElementById("myPopup1").classList.toggle("show1");
        }, 2000);

        likeProfile(profileId, function(response){
            $('#qnimate').removeClass('popup-box-on');
            $('.personal-detail-button').removeClass('show_b');
        });
    });

    jQuery("#reportUserForm").on('submit', function(e){
        e.preventDefault();
        var url = route('report-user');
        var data=[];

        data['reason'] = jQuery("#reportUserForm #reason").val();
        if(data['reason'] == ""){
            jQuery("span#reason-error").attr('style', 'display:block;color: #a94442!important');
            jQuery("#reason-error").parent().addClass('has-error');
            return false;
        }
        data['description'] = jQuery("#reportUserForm #description").val();
        data['profileId'] = jQuery("#reportUserForm #userId").val();
        doAjax(url, data, function(response){
            jQuery("#reportUserForm")[0].reset();
            jQuery("#reportUser").modal('hide');
            jQuery("#reportSuccess").modal('show');
        });
    });

    jQuery("input#reason").on('focus', function(){
        jQuery("span#reason-error").attr('style', 'display:none');
        jQuery("#reason-error").parent().removeClass('has-error');
    });

    jQuery("#messageSendForm").on('submit', function(e){
       e.preventDefault();
    });

    $("#messageFrm").on("submit", function () {
        var flag=0;
        if($("#message").val()=='') {
            $("#error_msg").show();
            flag=1;
        }
        if (flag == 1) {
            return false;
        }
    });

    jQuery("#registrationForm #email, #registrationForm #country, #registrationForm #city, #registrationForm #day").on('change', function(){
        if(jQuery("#registrationForm #email").val() != "" && jQuery("#registrationForm #country").val() !="" && jQuery("#registrationForm #city").val() !="" && jQuery("#registrationForm #day").val() !=""){
            jQuery("#registrationForm .additionalForm").show();
        }
    });

	
	
	/* @end */
})();