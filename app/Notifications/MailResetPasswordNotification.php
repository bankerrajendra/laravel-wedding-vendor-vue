<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\NewsLetterEmailSmsTemplate;
use App\Models\EmailSubject;

class MailResetPasswordNotification extends Notification
{
    use Queueable;
    public $token, $email, $first_name;

    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $email, $first_name)
    {
        //
        $this->token = $token;
        $this->email = $email;
        $this->first_name = $first_name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }
        /**
         * Get email template for Reset Password
         */
        $register_email = NewsLetterEmailSmsTemplate::where('name', '=', 'Reset Password')
                                        ->where('type', '=', 'email')
                                        ->where('status', '=', 'A')
                                        ->where('site_id', '=', config('constants.site_id'))
                                        ->first(['id', 'description']);
        if(!empty($register_email)) {
            $register_email_subject_obj = EmailSubject::where('template_id', '=', $register_email->id)->first(['subject']);
            /**
             * Replace email variables with real values
             */
            $replace_vars = [
                '[%USER_FIRST_NAME%]' => $this->first_name,
                '[%USER_EMAIL%]' => $this->email,
                '[%RESET_PASSWORD_LINK%]' => url(config('app.url').route('password.reset', $this->token, false)),
                '[%WEBSITENAME%]' => getGeneralSiteSetting('site_name')
            ];

            $register_email_updated = replaceEmailTemplateVars($register_email->description, $replace_vars);

            return (new MailMessage)->view(
                'emails.layout', [
                    'message_body' => $register_email_updated
                ]
            )
            ->subject($register_email_subject_obj->subject);
        }
           
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
