<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 2 Jul 2019 11:52:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class VendorCategorySatelliteInformation extends BaseModel
{
    protected $table = 'vendors_category_satellites_information';

    public $timestamps  = false;

    protected $fillable = [
        'vendor_category_id',
        'satellite_id',
        'title',
        'keywords',
        'description',
        'meta_description',
        'icon_image'
    ];

    public function vendor_category()
	{
		return $this->belongsTo(\App\Models\VendorCategory::class);
	}
}
