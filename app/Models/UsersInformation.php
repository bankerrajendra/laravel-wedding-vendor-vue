<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersInformation
 * 
 * @property int $id
 * @property int $user_id
 * @property string $drink
 * @property string $smoke
 * @property string $food
 * @property int $education_id
 * @property int $profession_id
 * @property string $about
 * @property string $country_code
 * @property string $mobile_number
 * @property string $number_verification_code
 * @property string $number_verified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Education $education
 * @property \App\Models\Profession $profession
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsersInformation extends BaseModel
{
	protected $table = 'users_information';

	public $timestamps  = false;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'event_date',
		'budget',
		'vendor_categories'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
