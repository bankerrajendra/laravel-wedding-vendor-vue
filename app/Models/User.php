<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use App\Logic\Membership\MembershipRepository;
use Carbon\Carbon;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Talk;
use App\Models\FavouritedUser;
use App\Notifications\MailResetPasswordNotification;




/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $gender
 * @property \Carbon\Carbon $date_of_birth
 * @property int $country_id
 * @property int $city_id
 * @property string $remember_token
 * @property bool $activated
 * @property string $token
 * @property string $signup_ip_address
 * @property string $signup_confirmation_ip_address
 * @property string $signup_sm_ip_address
 * @property string $admin_ip_address
 * @property string $updated_ip_address
 * @property string $deleted_ip_address
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\City $city
 * @property \App\Models\Country $country
 * @property \Illuminate\Database\Eloquent\Collection $activations
 * @property \Illuminate\Database\Eloquent\Collection $laravel2steps
 * @property \Illuminate\Database\Eloquent\Collection $permissions
 * @property \Illuminate\Database\Eloquent\Collection $profiles
 * @property \Illuminate\Database\Eloquent\Collection $roles
 * @property \Illuminate\Database\Eloquent\Collection $social_logins
 * @property \Illuminate\Database\Eloquent\Collection $users_images
 * @property \Illuminate\Database\Eloquent\Collection $users_informations
 * @property \Illuminate\Database\Eloquent\Collection $interests
 * @property \Illuminate\Database\Eloquent\Collection $users_preferences
 * @property \Illuminate\Database\Eloquent\Collection $users_show_hides
 * @property \Illuminate\Database\Eloquent\Collection $favourited_users
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use SoftDeletes;
    use HasRoleAndPermission;
    use HasApiTokens, Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $casts = [
        'country_id' => 'int',
        'state_id' => 'int',
        'activated' => 'bool',
        'is_paid' => 'bool'
    ];

    protected $authUsser;

    protected $hidden = [
        'password',
        'remember_token',
        'token'
    ];

    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'email',
        'password',
        'city_id',
        'mobile_country_code',
        'mobile_number',
        'country_id',
        'remember_token',
        'activated',
        'access_grant',
        'token',
        'signup_ip_address',
        'signup_confirmation_ip_address',
        'signup_sm_ip_address',
        'admin_ip_address',
        'updated_ip_address',
        'deleted_ip_address',
        'state_id',
        'site_id',
        'last_login_at',
        'last_login_ip',
        'address',
        'zip',
        'is_paid',
        'vendor_type'
    ];


    protected static function getAuthUser()
    {
        if(Auth::user()) {
            return  Auth::user();
        } else {
            return  Auth::guard('api')->user();
        }
    }

    /**
     * Build Social Relationships.
     *
     * @var array
     */
    public function social()
    {
        return $this->hasMany('App\Models\Social');
    }

    /**
     * User Profile Relationships.
     *
     * @var array
     */
    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class);
    }

    public function state()
    {
        return $this->belongsTo(\App\Models\State::class);
    }

    public function city()
    {
        return $this->belongsTo(\App\Models\City::class);
    }

    public function activations()
    {
        return $this->hasMany(\App\Models\Activation::class);
    }

    public function laravel2steps()
    {
        return $this->hasMany(\App\Models\Laravel2step::class, 'userId');
    }

    public function permissions()
    {
        return $this->belongsToMany(\App\Models\Permission::class)
            ->withPivot('id')
            ->withTimestamps();
    }

    public function profiles()
    {
        return $this->belongsToMany('App\Models\Profile')->withTimestamps();
    }

    public function roles()
    {
        return $this->belongsToMany(\App\Models\Role::class)
            ->withPivot('id')
            ->withTimestamps();
    }

    public function social_logins()
    {
        return $this->hasMany(\App\Models\Social::class)->first();
    }

    public function users_images()
    {
        return $this->hasMany(\App\Models\UsersImage::class);
    }

    public function vendors_images()
    {
        return $this->hasMany(VendorsImage::class, 'vendor_id');
    }

    public function users_information()
    {
        return $this->hasOne(\App\Models\UsersInformation::class);
    }

    public function vendors_information()
    {
        return $this->hasOne(VendorsInformation::class, 'vendor_id');
    }

    public function vendors_showcase()
    {
        return $this->hasOne(VendorShowcase::class, 'vendor_id');
    }

    public function vendors_saved_review_message()
    {
        return $this->hasOne(VendorsSavedReviewMessage::class, 'vendor_id');
    }

    public function message_filter()
    {
        return $this->hasOne(\App\Models\MessageFilter::class);
    }

    public function users_reviews()
    {
        return $this->hasMany(\App\Models\Review::class, 'user_id');
    }

    public function vendor_events()
    {
        return $this->hasMany(\App\Models\VendorEventInformation::class, 'vendor_id');
    }

    public function getfilteredSender() {
        $FinalArr= MessageFilteredUser::where('user_id', $this->id)->select('filtered_id')->get()->toArray();
        $returnArr=array();
        if(count($FinalArr)>0) {
            foreach ($FinalArr as $datar) {
                array_push($returnArr,$datar['filtered_id']);
            }
        }
        return $returnArr;
    }

    public function get_user_information(){
        return $this->users_information()->first();
    }

    public function assignUserInformation($user_information){
        return $this->users_information()->save($user_information);
    }

    public function interests()
    {
        return $this->belongsToMany(\App\Models\Interest::class, 'users_interests')
            ->withPivot('id')
            ->withTimestamps();
    }

    public function get_interests()
    {
        return $this->belongsToMany(\App\Models\Interest::class, 'users_interests')
            ->withPivot('id')
            ->withTimestamps()->get();
    }

    public function users_preferences()
    {
        return $this->hasOne(\App\Models\UsersPreference::class)->first();
    }

    public function users_show_hides()
    {
        return $this->hasMany(\App\Models\UsersShowHide::class);
    }

    public function paid_members()
    {
//        return $this->hasMany(\App\Models\PaidMembers::class, 'user_id');
        return $this->hasMany(\App\Models\PaidMembers::class)->latest();
    }

    public function favourited_users(){
        //return $this->hasMany(\App\Models\FavouritedUser::class, 'from_user');
        $user = $this;
        return FavouritedUser::where('from_user', $this->id)->orWhere(function($query) use ($user){
            $query->where('to_user', $user->id)->where('to_user_status', 1);
        });
    }

    public function favourited_by_users(){
        //return $this->hasMany(\App\Models\FavouritedUser::class, 'to_user');
        return FavouritedUser::where('to_user', $this->id)->where('to_user_status', 0);
    }

    /// below functions are for share photo, request to private photo, request to add photo. we can modify the below functions as per requirements.
    public function photonotify_users() {
        return PhotoNotification::where('from_user', $this->id)->where('notification_type', 'share_photo')->get();
    }

    public function photonotify_by_users() {
        return PhotoNotification::where('to_user', $this->id)->where('notification_type', 'private_photo_request')->get();
    }

    public function hasPrivatePhotos(){
        if($this->users_images()->where('image_type', 'private')->where('is_approved', 'Y')->count() > 0){
            return true;
        }
        return false;
    }

    public function hasSharedPhotos($user_id){
        if(PhotoNotification::where('from_user', $this->id)->where('to_user', $user_id)->where('notification_type', 'share_photo')->count() > 0){
            return true;
        }
        return false;
    }

    public function hasAlreadySentPrivatePhotoRequest($user_id){
        return PhotoNotification::where('from_user', $this->id)->where('to_user', $user_id)->where('notification_type', 'private_photo_request')->get();
    }

    public function hasAlreadySentRequest(){
        $authId=Auth::id();
        return PhotoNotification::where('from_user', $authId)->where('to_user', $this->id)->where('notification_type', 'profile_photo_request')->where('status', 'P')->count();
    }
    /// End:  functions are for share photo, request to private photo, request to add photo. we need to modify the below functions as per requirements.

    public function skipped_users(){
        return $this->hasMany(\App\Models\SkippedUser::class, 'from_user');
    }

    public function skipped_by_users(){
        return $this->hasMany(\App\Models\SkippedUser::class, 'to_user');
    }

    public function shortlisted_users(){
        return $this->hasMany(\App\Models\ShortlistedUser::class, 'from_user');
    }

    public function shortlisted_by_users(){
        return $this->hasMany(\App\Models\ShortlistedUser::class, 'to_user');
    }

    public function viewed_users(){
        return $this->hasMany(\App\Models\ViewedUser::class, 'from_user');
    }

    public function viewed_by_users(){
        return $this->hasMany(\App\Models\ViewedUser::class, 'to_user');
    }

    public function blocked_users(){
        return $this->hasMany(\App\Models\BlockedUser::class, 'from_user');
    }

    public function blocked_by_users(){
        return $this->hasMany(\App\Models\BlockedUser::class, 'to_user');
    }

    public function hasProfile($name)
    {
        foreach ($this->profiles as $profile) {
            if ($profile->name == $name) {
                return true;
            }
        }
        return false;
    }

    public function assignProfile($profile)
    {
        return $this->profiles()->attach($profile);
    }

    public function removeProfile($profile)
    {
        return $this->profiles()->detach($profile);
    }

    public function getAge(){
        $today = new DateTime();
        $birthdate = new DateTime($this->date_of_birth);
        $interval = $today->diff($birthdate);
        return $interval->format('%y');
    }

    public function get_firstname() {
        $fname = DB::table('user_profile_flags')
            ->select('*')
            ->where(array('user_id'=>$this->id,'property_name'=>'first_name'))
            ->first();
        if (!empty($fname)) {
            return config('constants.site_code').str_pad($this->id, 6, "0", STR_PAD_LEFT);

        } else {
            return $this->first_name;
        }
    }

    public function getFirstname($limit=8){
        $firstName=$this->first_name;
        if (strlen($firstName) > $limit) {
            $firstName = substr($firstName, 0, $limit);
        }
        return $firstName;
    }

    public function is_active(){
        return ($this->banned==0 && $this->activated==1 && $this->access_grant=='Y' && $this->deactivated==0)?1:0;
    }

    public function hasCoverPhoto()
    {
        $where = array(
            'vendor_id' => $this->id,
            'image_type' => 'cover'
        );
        $coverCount = VendorsImage::where($where)->count();
        if($coverCount == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function hasAllProfilePhotos()
    {
        $where = array(
            'vendor_id' => $this->id,
            'image_type' => 'profile'
        );
        $profileCount = VendorsImage::where($where)->count();
        if($profileCount < config('constants.vendor_profile_photos_allowed')) {
            return false;
        } else {
            return true;
        }
    }

    public function getProfilePic($viewprofile=''){
        $authId=User::getAuthUser()->id;
        $curId=$this->id;
        if(User::getAuthUser()){
            $currentUser = User::getAuthUser();
            if(!$currentUser->isAdmin()){
                if($this->banned == 1 || $this->deactivated == 1 || $this->deleted_at != null || $this->account_show=='N' || $this->access_grant=='N'){
                    return URL::asset('img/blocked.png');
                }
            }
        }

        if(user_blocked_to_me($this->id)>0 || user_skipped_to_me($this->id)>0) {
            return URL::asset('img/blocked.png');
        }

        $profile_pic =  $this->users_images()->where('is_profile_image', 'Y')->where('is_approved', 'Y')->first();
        //print_r($profile_pic);
        if(is_null($profile_pic)){
            $profile_pic =  $this->users_images()->where('image_type', 'public')->where('is_approved', 'Y')->first();
            if(!is_null($profile_pic)){
                return config('constants.S3_WEDDING_IMAGES').$profile_pic->image_thumb;
            }
            else{
                $profile_pic =  $this->users_images()->where('image_type', 'private')->where('is_approved', 'Y')->first();
                if(!is_null($profile_pic)) { // && $viewprofile=='profile-image'){

                    if($authId == $curId) {
                        return config('constants.S3_WEDDING_IMAGES').$profile_pic->image_thumb;
                    } else {
                        $privatePhotoPermissionCount = $this->privatePhotoPermissionCnt();

                        if ($privatePhotoPermissionCount > 0) {
                            return config('constants.S3_WEDDING_IMAGES') . $profile_pic->image_thumb;
                        } else {
                            return URL::asset('img/avatar.png');
                        }
                    }
                    // end share_photo
                } else {
                    return URL::asset('img/avatar.png');
                }
            }
        }
        else{
            return config('constants.S3_WEDDING_IMAGES').$profile_pic->image_thumb;
        }
    }


    // get vendor profile image
    // Now this function will return image for user and vendor both
    public function getVendorProfilePic($self = false) 
    {
        //users_images
        if(User::getAuthUser()){
            $currentUser = User::getAuthUser();
            if(!$currentUser->isAdmin()){
                $currUserFlag=0;
                if($this->id!=Auth::id()) {
                    $currUserFlag= ($this->access_grant=='N');
                }
                if($this->banned == 1 || $this->deactivated == 1 || $this->deleted_at != null || $this->account_show == 'N' || $currUserFlag){
                    return URL::asset('img/blocked.png');
                }
            }
        }

        if($self == true) {
           if($this->hasRole('vendor')) {
               $profile_pic =  $this->vendors_images()->where('image_type', 'profile')->first();
           } else {
               $profile_pic =  $this->users_images()->first();
           }

        } else {
            if($this->hasRole('vendor')) {
                $profile_pic =  $this->vendors_images()->where('image_type', 'profile')->where('is_approved', 'Y')->first();
            } else {
                $profile_pic =  $this->users_images()->where('is_approved', 'Y')->first();
            }
        }
        
        if(!is_null($profile_pic)) {
            return config('constants.S3_WEDDING_IMAGES').$profile_pic->image_thumb;
        } else {
            //return URL::asset('img/avatar.png');
            return URL::asset('img/default-photo.jpg');
        }
    }

    public function getVendorCoverPic($self = false) 
    {
        if(User::getAuthUser()){
            $currentUser = User::getAuthUser();
            if(!$currentUser->isAdmin()){
                if($this->banned == 1 || $this->deactivated == 1 || $this->deleted_at != null || $this->account_show == 'N' || $this->access_grant=='N'){
                    if($self == true) {
                        $cover_pic =  $this->vendors_images()->where('image_type', 'cover')->first();
                        if($cover_pic == null) {
                            return URL::asset('img/blocked.png');    
                        } else {
                            return config('constants.S3_WEDDING_IMAGES').$cover_pic->image_full;
                        }
                    } else {
                        return URL::asset('img/blocked.png');
                    }
                }
            }
        }
        if($self == true) {
            $cover_pic =  $this->vendors_images()->where('image_type', 'cover')->first();
        } else {
            $cover_pic =  $this->vendors_images()->where('image_type', 'cover')->where('is_approved', 'Y')->first();
        }
        
        if(!is_null($cover_pic)) {
            return config('constants.S3_WEDDING_IMAGES').$cover_pic->image_full;
        } else {
            return URL::asset('img/default-banner.jpg');
        }
    }


    public function privatePhotoPermissionCnt() {
        $authId=Auth::id();
        $curId=$this->id;
        $privatePerCount=0;

        $profile_pic =  $this->users_images()->where('image_type', 'public')->where('is_approved', 'Y')->first();
        if(!is_null($profile_pic)){

        }
        else{
            $profile_pic =  $this->users_images()->where('image_type', 'private')->where('is_approved', 'Y')->first();
            if(!is_null($profile_pic)) { // && $viewprofile=='profile-image'){
                if($authId != $curId) {
                    $privatePerCount = PhotoNotification::where(function ($innerQuery) use ($authId, $curId) {
                        return $innerQuery->where('from_user', $authId)->where('to_user', $curId)->where('notification_type', 'private_photo_request')->where('status', 'A');
                    })->orWhere(function ($innerQuery) use ($authId, $curId) {
                        return $innerQuery->where('from_user', $curId)->where('to_user', $authId)->where('notification_type', 'share_photo');
                    })->count();
                }
            }
        }

        return $privatePerCount;
    }

    public function isSocialLoggedIn(){
        if($this->social_logins() && !$this->get_user_information()){
            return true;
        }
        else{
            return false;
        }
    }

    public function hasCompletedProfile(){
        $userInformation = $this->get_user_information();
        if (($userInformation->drink == null || $userInformation->smoke == null || $userInformation->education_id == null || $userInformation->about == null )){
            return false;
        }
        else{
            return true;
        }
    }

    public function getEncryptedId(){
        return base64_encode($this->id);
    }

    public function getDecryptedId(){
        return base64_decode($this->id);
    }

    public function isOnline(){
        return Cache::has('user-is-online-'.$this->id);
    }

    public function profileLink(){
        $vendor_slug = $this->getEncryptedId($this->id);
        // add slug if not blank
        $getCompanySlug = VendorsInformation::where('vendor_id', $this->id)->first(['company_slug']);
        if($getCompanySlug->company_slug != '') {
            $vendor_slug = $getCompanySlug->company_slug;
        }
        if($this->banned == 1 || $this->deactivated == 1 || $this->deleted_at != null || $this->account_show == 'N') {
            if(Auth::check()) {
                if(Auth::id() == $this->id) {
                    return route('show-vendor', [$vendor_slug]);        
                }
            }
            return route('profile-not-found');
        } else {
            return route('show-vendor', [$vendor_slug]);
        }
    }

    public function popupProfileLink(){
        return route('user-profile-popup', ['username'=>$this->name]);
    }

    public function isSkipped(){
        $currentUser = Auth::user();
        if($currentUser->skipped_users()->where('to_user', '=', $this->id)->count() > 0){
            return true;
        }
        return false;
    }

    public function isSkippedMe(){
        $currentUser = Auth::user();
        if($this->skipped_users()->where('to_user', '=', $currentUser->id)->count() > 0){
            return true;
        }
        return false;
    }

    public function isLiked(){
        $currentUser = Auth::user();
        if(FavouritedUser::where('from_user', $currentUser->id)->where('to_user', $this->id)->count() > 0){
            return true;
        }
//        if($currentUser->favourited_users()->where('to_user', '=', $this->id)->count() > 0){
//            return true;
//        }
        return false;
    }

    public function isShortlisted(){
        $currentUser = Auth::user();
        if($currentUser->shortlisted_users()->where('to_user', '=', $this->id)->count() > 0){
            return true;
        }
        return false;
    }

    public function isLikedMe(){
        $currentUser = Auth::user();
        if(FavouritedUser::where('from_user', $this->id)->where('to_user', $currentUser->id)->count() > 0){
            return true;
        }
//        if($this->favourited_users()->where('to_user', '=', $currentUser->id)->count() > 0){
//            return true;
//        }
        return false;
    }

    public function isLikedBack(){
        $currentUser = Auth::user();
        if(FavouritedUser::where('from_user', $this->id)->where('to_user', $currentUser->id)->where('to_user_status', '=', 1)->count() > 0){
            return true;
        }
//        if($this->favourited_users()->where('to_user', '=', $currentUser->id)->where('to_user_status', '=', 1)->count() > 0){
//            return true;
//        }
        return false;
    }

    public function getLikeText(){
        if($this->isLikedMe()){
            return "Favorite Back";
        }
        else{
            return "Favorite";
        }
    }

    public function isBlocked(){
       // $currentUser = Auth::user();
        if(User::getAuthUser()->blocked_users()->where('to_user', '=', $this->id)->count() > 0){
            return true;
        }
        return false;
    }

    public function isBlockedMe(){
        //$currentUser = Auth::user();
        if($this->blocked_users()->where('to_user', '=', User::getAuthUser()->id)->count() > 0){
            return true;
        }
        return false;
    }

    public function hasConversation(){
        return $conversation = Message::hasConversation($this->id);
//        if($conversation){
//            if($conversation->messages->count() > 0)
//                return true;
//            else
//                return false;
//        }
//        else{
//            return false;
//        }
    }

    public function scopeSkipConditions($query){
        $user_id=Auth::id();

        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            ->skipShortlistedUsers()
            ->skipSkippedUsers()
            ->skipWhoSkippedMe()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
            ->sameAgeLimit()
            ->isActive();
//            ->whereNotIn('id', function ($innerQuery) use($user_id) {
//                $innerQuery->select('sender_id')->from('messages')->where('receiver_id',$user_id)->where('receiver_trash','N')->where('receiver_delete','N');
//            })
//            ->whereNotIn('id', function ($innerQuery) use($user_id) {
//                $innerQuery->select('receiver_id')->from('messages')->where('sender_id',$user_id)->where('sender_trash','N')->where('sender_delete','N');
//            });
    }

    public function scopeNewUserConditions($query){
        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
           // ->fromSameCountry()
            //->filterPartnerPreferences()
            //    ->skipLikedUsers()
            //    ->skipShortlistedUsers()
            //    ->skipSkippedUsers()
            //    ->skipWhoLikedMe()
            ->skipSkippedUsers()
            ->skipWhoSkippedMe()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
            //->skipViewedMe()
            ->isActive();
    }

    public function scopePartnerPreferenceConditions($query){
        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            //->fromSameCountry()
            ->filterPartnerPreferences()
            //    ->skipLikedUsers()
            //    ->skipShortlistedUsers()
            //    ->skipSkippedUsers()
            //    ->skipWhoLikedMe()
            ->skipSkippedUsers()
            ->skipWhoSkippedMe()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
            //->skipViewedMe()
            ->isActive();
    }

    public function scopePaidUserConditions($query){
        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            //  ->fromSameCountry()
            //  ->skipLikedUsers()
            //  ->skipShortlistedUsers()
            //  ->skipSkippedUsers()
            //  ->skipWhoLikedMe()
            ->skipSkippedUsers()
            ->skipWhoSkippedMe()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
            //  ->skipViewedMe()
            ->whereIn('id', function ($innerQuery)  {
                $innerQuery->select('user_id')->from('paid_members')->where('membership_end_date','>=', date("Y-m-d 00:00:00"));
            })
            ->isActive();
    }


    public function scopeNewUserProfile($query){
        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            ->fromSameCountry()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
            ->skipSkippedUsers()
            ->skipWhoSkippedMe()
            ->sameAgeLimit()
            ->isActive();
    }


    public function scopeMatchUserProfile($query){
        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            ->fromSameCountry()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
            ->skipSkippedUsers()
            ->skipWhoSkippedMe()
            ->sameAgeLimit()
            ->sameLifestyleHobbies()
            ->isActive();
        //newuserprofile + lifestyle & hobbies
    }


    public function scopeSameAgeLimit($query){
        $currentUser = Auth::user();
        $myage=$currentUser->getAge();

//        return $query->where(DB::raw("(((ROUND((DATEDIFF(CURDATE(),date_of_birth) / 365.25)) >=".($myage-8)." and ROUND((DATEDIFF(CURDATE(),date_of_birth) / 365.25)) <=".($myage+8)." and ROUND((DATEDIFF(CURDATE(),date_of_birth) / 365.25))>0)) )"));
        return $query->whereRaw('ROUND(DATEDIFF(CURDATE(),date_of_birth) / 365.25) <='.($myage+8))
            ->whereRaw('ROUND(DATEDIFF(CURDATE(),date_of_birth) / 365.25)>='.($myage-8))
            ->whereRaw('ROUND(DATEDIFF(CURDATE(),date_of_birth) / 365.25)>0');
    }

    public function scopeIsActive($query){
        return $query->where('banned',0)
            ->where('activated',1)
            ->where('access_grant','Y')
            ->where('deactivated',0)
            ->where('account_show','Y');
    }

    public function scopeFromCities($query, $cities){
        if(count($cities) > 0){
            return $query->whereIn('city_id', $cities);
        }
        else{
            return $query;
        }
    }

    public function scopeFilterAge($query, $age){
        return $query->whereDate('date_of_birth', '<=', Carbon::today()->subYears($age[0])->toDateString())
            ->whereDate('date_of_birth', '>=', Carbon::today()->subYears($age[1]+1)->toDateString());
    }

//    public function scopeFilterPartnerPreferences_old($query){
//        $currentUser = Auth::user();
//        if($currentUser->users_preferences()){
//            $pref = $currentUser->users_preferences();
//            return $query->where('city_id', $pref->city_id)
//                ->whereDate('date_of_birth', '<=', Carbon::today()->subYears($pref->age_from)->toDateString())
//                ->whereDate('date_of_birth', '>=', Carbon::today()->subYears($pref->age_to)->toDateString());
//        }
//        else{
//            return $query;
//        }
//    }

//    public function scopeSkipHasConversation($query){
//        $user_id=Auth::id();
//
//        return $query->whereHas('messages', function($innerQuery) use ($user_id){
//            $innerQuery->where(['receiver_id'=>$user_id, 'receiver_trash!='=>'N', 'receiver_delete!='=>'N'])   //'receiver_archive'=>'N',
//            ->orWhere(function($query1) use ($user_id){
//                return $query1
//                    ->where(['sender_id'=>$user_id, 'sender_trash!='=>'N', 'sender_delete!='=>'N']);  //'sender_archive'=>'N',
//            });
//
//        });
//
//    }

    public function scopeFilterPartnerPreferences($query){
        $currentUser = Auth::user();
        if($currentUser->users_preferences()){
            $pref = $currentUser->users_preferences();

//            return $query->where('city_id', $pref->city_id)
//                ->whereDate('date_of_birth', '<=', Carbon::today()->subYears($pref->age_from)->toDateString())
//                ->whereDate('date_of_birth', '>=', Carbon::today()->subYears($pref->age_to)->toDateString());
            $arr['country_living'] = explode(',',$pref->country_living);
            $arr['state_living'] = explode(',',$pref->state_living);
            $arr['city_district'] = explode(',',$pref->city_district);


            $arrInfo['food'] = explode(',',$pref->diet);
            $arrInfo['smoke'] = explode(',',$pref->smoke);
            $arrInfo['drink'] = explode(',',$pref->drink);
            $arrInfo['religion_id'] = explode(',',$pref->religion);
            $arrInfo['education_id'] = explode(',',$pref->education);
            $arrInfo['body_type'] = explode(',',$pref->body_type);
            $arrInfo['marital_status'] = explode(',',$pref->marital_status);
            $arrInfo['profession_id'] = explode(',',$pref->professional_area);
            $arrInfo['annual_income'] = explode(":",$pref->annual_income);
            $arrInfo['complexion'] = explode(',',$pref->skin_tone);
            $arrInfo['eye_color'] = explode(',',$pref->eye_color);
            $arrInfo['language_id'] = explode(',',$pref->languages);
            $arrInfo['sub_cast_id'] = explode(',',$pref->community);

          return   $query->filterAge([$pref->age_from, $pref->age_to])
                ->filterHeight([$pref->height_from, $pref->height_to])
                ->fromCountry($arr['country_living'])
                ->fromState($arr['state_living'])
                ->fromCity($arr['city_district'])
                ->fromUserInfoOrArr($arrInfo);

        }
        else{
            return $query->fromSameCountry()
                ->sameAgeLimit()
                ->sameLifestyleHobbies();
        }
    }


    public function scopeGetMessageFilterUsr($query, $search_pref = []) {
        //$query->isCompletedProfile();
            //->hasOppositeGender();
            //->skipWhoBlockedByMeAnot()
            //->skipWhoBlockedMeAnot()
            //->skipSkippedUsers()
            //->skipWhoSkippedMe()
            //->isActive();

        $arr['body_type'] = $search_pref['body_type'];
        $arr['food'] = $search_pref['diet'];
        $arr['smoke'] = $search_pref['smoke'];
        $arr['drink'] = $search_pref['drink'];

        $query->fromReligion($search_pref['religion'])
            ->filterAge([$search_pref['age_from'], $search_pref['age_to']])
            ->filterHeight([$search_pref['height_from'], $search_pref['height_to']])
            ->FromUserInfoStr($arr);

        if($search_pref['allow_without_images']!='1') {
            $query->hasImage();
        }

        return $query;
    }



    public function scopeHasImage($query){
        return $query->whereHas('users_images', function ($innerQuery) {
            $innerQuery->where('is_approved', 'Y');
        });
    }

    public function scopeFromSameCountry($query){
        $currentUser = Auth::user();
        return $query->where('country_id', $currentUser->country_id);
    }

    public function scopeWithEncryptedId($query, $encryptedId){
        $id = base64_decode($encryptedId);
        return $query->where('id', '=', $id);
    }

    public function scopeIsCompletedProfile($query){
        return $query->whereHas('users_information', function($innerQuery){
            
        });
    }

    public function scopeIsIncompletedProfile($query){
        return $query->whereHas('users_information', function($innerQuery){
            
        });
    }

    public function scopeAdminUserSearch($query,$searchTerm=''){
        return $query->where(function($query) use ($searchTerm){
            $query->where('id', 'like', '%'.$searchTerm.'%')
                ->orWhere('name', 'like', '%'.$searchTerm.'%')
                ->orWhere('email', 'like', '%'.$searchTerm.'%');
        })->fromVendorCompanyName([ 0 => $searchTerm]);
    }

    public function scopeSameLifestyleHobbies($query){
        $currentUser = Auth::user();
        $currentUserInfo = $currentUser->get_user_information();

        return $query->whereHas('users_information',
            function($innerQuery) use($currentUserInfo) {
                $innerQuery->where(
                    function($innerQuery1) use($currentUserInfo){
                        return $innerQuery1->orWhere('drink',$currentUserInfo->drink)
                            ->orWhere('smoke',$currentUserInfo->smoke)
                            ->orWhere('food',$currentUserInfo->food);
                    })
                    ->where(
                        function($innerQuery2) use($currentUserInfo) {
                            if(!is_null($currentUserInfo->hobbies)){
                                foreach(explode(",",$currentUserInfo->hobbies) as $hobby) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$hobby."',hobbies)");
                                }
                            }
                            if(!is_null($currentUserInfo->interests)){
                                foreach(explode(",",$currentUserInfo->interests) as $interest) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$interest."',interests)");
                                }
                            }
                            if(!is_null($currentUserInfo->preferred_movies)){
                                foreach(explode(",",$currentUserInfo->preferred_movies) as $preferred_movie) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$preferred_movie."',preferred_movies)");
                                }
                            }
                            if(!is_null($currentUserInfo->sports)){
                                foreach(explode(",",$currentUserInfo->sports) as $sport) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$sport."',sports)");
                                }
                            }
                            if(!is_null($currentUserInfo->favourite_music)){
                                foreach(explode(",",$currentUserInfo->favourite_music) as $fav_music) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$fav_music."',favourite_music)");
                                }
                            }
                            if(!is_null($currentUserInfo->favourite_reads)){
                                foreach(explode(",",$currentUserInfo->favourite_reads) as $favourite_read) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$favourite_read."',favourite_reads)");
                                }
                            }
                            return $innerQuery2;
                        }
                    );
            });
    }

    public function scopeHasUserRole($query){
        return $query->whereHas('roles', function ($innerQuery) {
            $innerQuery->where('roles.slug', '=', 'user');
            if(config('constants.unverified_account_allowed')){
                $innerQuery->orWhere('roles.slug', '=', 'unverified');
            }
        });
    }

    public function scopeHasVendorRole($query){
        return $query->whereHas('roles', function ($innerQuery) {
            $innerQuery->where('roles.slug', '=', 'vendor');
        });
    }

    
    public function scopeHasAdminRole($query){
        return $query->whereHas('roles', function ($innerQuery) {
            $innerQuery->where('roles.slug', '=', 'admin');
        });
    }

    public function scopeHasOppositeGender($query){
        //$user = Auth::user();
        return $query->where('gender', '!=', User::getAuthUser()->gender);
    }

    public function scopeSkipLikedUsers($query){

        return $query->whereNotIn('id', function($subQuery){
            $user = Auth::user();
            $subQuery->select('to_user')->from('favourited_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipShortlistedUsers($query){

        return $query->whereNotIn('id', function($subQuery){
          //  $user = Auth::user();
            $subQuery->select('to_user')->from('shortlisted_users')->where('from_user','=', User::getAuthUser()->id);
        });
    }

    public function scopeSkipSkippedUsers($query){

        return $query->whereNotIn('id', function($subQuery){
           // $user = Auth::user();
            $subQuery->select('to_user')->from('skipped_users')->where('from_user','=', User::getAuthUser()->id);
        });
    }

    public function scopeSkipWhoSkippedMe($query){

        return $query->whereNotIn('id', function($subQuery){
           // $user = Auth::user();
            $subQuery->select('from_user')->from('skipped_users')->where('to_user','=', User::getAuthUser()->id);
        });
    }

    public function scopeSkipWhoLikedMe($query){
        return $query->whereNotIn('id', function($subQuery){
           // $user = Auth::user();
            $subQuery->select('from_user')->from('favourited_users')->where('to_user','=', User::getAuthUser()->id);
        });
    }

//    public function scopeGetWithUsername($query, $username){
//        return $query->where('name', '=', $username);
//    }

    public function scopeGetWithProfilecode($query, $username){
        return $query->where('id', '=', base64_decode($username));
    }

    public function scopeSkipWhoBlockedByMe($query){
        return $query->whereNotIn('id', function($subQuery){
            //$user = Auth::user();
            $subQuery->select('to_user')->from('blocked_users')->where('from_user','=', User::getAuthUser()->id);
        });
    }

    public function scopeSkipWhoBlockedMe($query){
        return $query->whereNotIn('id', function($subQuery){
         //   $user = Auth::user();
            $subQuery->select('from_user')->from('blocked_users')->where('to_user','=', User::getAuthUser()->id);
        });
    }

    public function scopeSkipViewedMe ($query) {
        return $query->whereNotIn('id', function ($subQuery){
            //$user = Auth::user();
            $subQuery->select('to_user')->from('viewed_users')->where('from_user','=', User::getAuthUser()->id);
        });
    }

    public function getUserTimeZone()
    {
        if (\Session::has("usertimezone")) {
            return Session::get("usertimezone");
        } else {
            Session::put('usertimezone',getTimeZoneFromIpAddress_api());
            //return getTimeZoneFromIpAddress_api();
            //calculate and save role in session
        }
    }

    /**
     * For Searching users from new-search page - Rajendra Banker on 2nd April 2019
     */
    public function scopeGetNewSearchedUsers($query, $search_pref = []) {
        $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            ->skipWhoBlockedByMeAnot()
            ->skipWhoBlockedMeAnot()
            ->skipSkippedUsers()
            ->skipWhoSkippedMe()
            ->isActive();
        if(!isset($search_pref['keywords'])) {
            $arr['sect_id'] = $search_pref['sect'];
            $arr['language_id'] = $search_pref['language'];
            $arr['education_id'] = $search_pref['education_id'];
            $arr['body_type'] = $search_pref['body_type'];
            $arr['food'] = $search_pref['food'];
            $arr['smoke'] = $search_pref['smoke'];
            $arr['drink'] = $search_pref['drink'];
            $arr['marital_status'] = $search_pref['marital_status'];
            // custom search preference
            return $query->fromCountry($search_pref['country_code'])
                ->fromState($search_pref['state_id'])
                ->fromCity($search_pref['city_id'])
                ->fromReligion($search_pref['religion'])
                ->filterAge([$search_pref['age_from'], $search_pref['age_to']])
                ->filterHeight([$search_pref['height_from'], $search_pref['height_to']])
                ->fromUserInfoArr($arr);
        } else {
            return $query->fromKeyWords($search_pref['keywords']);
        }
    }

    public function scopeFromKeyWords($query, $keywords)
    {
        if($keywords != "") {
            $keywords_expld = explode(" ", $keywords);
            $query->where(function ($query) use($keywords_expld) {
                $query->whereHas('users_information',
                    function($innerQuery) use($keywords_expld) {
                        $innerQuery->where(
                            function($innerQuery) use($keywords_expld) {
                                if(is_array($keywords_expld) && count($keywords_expld)>0) {
                                    $innerQuery = $innerQuery->orWhere(function ($query) use($keywords_expld) {
                                        $query->whereIn('education_id', function($subQuery) use($keywords_expld) {
                                            $subQuery=$subQuery->select('id')->from('educations');
                                            foreach($keywords_expld as $keyword) {
                                                $subQuery->orWhere('education', 'like', '%'.$keyword.'%');
                                            }
                                            return $subQuery;
                                        });
                                    })
                                        ->orWhere(function ($query) use($keywords_expld) {
                                            $query->whereIn('profession_id', function($subQuery) use($keywords_expld) {
                                                $subQuery = $subQuery->select('id')->from('professions');
                                                foreach($keywords_expld as $keyword) {
                                                    $subQuery->orWhere('profession', 'like', '%' . $keyword . '%');
                                                }
                                                return $subQuery;
                                            });
                                        })
                                        ->orWhere(function ($query) use($keywords_expld) {
                                            $query->whereIn('language_id', function($subQuery) use($keywords_expld) {
                                                $subQuery = $subQuery->select('id')->from('languages');
                                                foreach($keywords_expld as $keyword) {
                                                    $subQuery->orWhere('language', 'like', '%' . $keyword . '%');
                                                }
                                                return $subQuery;
                                            });
                                        })
                                        ->orWhere(function ($query) use($keywords_expld) {
                                            $query->whereIn('sect_id', function($subQuery) use($keywords_expld) {
                                                $subQuery = $subQuery->select('id')->from('communities');
                                                foreach($keywords_expld as $keyword) {
                                                    $subQuery->orWhere('name', 'like', '%' . $keyword . '%');
                                                }
                                                return $subQuery;
                                            });
                                        })
                                        ->orWhere(function ($query) use($keywords_expld) {
                                            $query->whereIn('sub_cast_id', function($subQuery) use($keywords_expld) {
                                                $subQuery = $subQuery->select('id')->from('subcastes');
                                                foreach($keywords_expld as $keyword) {
                                                    $subQuery->orWhere('name', 'like', '%' . $keyword . '%');
                                                }
                                                return $subQuery;
                                            });
                                        });
                                }
                                return $innerQuery;
                            });
                    })
                    ->fromCountryKeyWords($keywords_expld)
                    ->fromStateKeyWords($keywords_expld)
                    ->fromCityKeyWords($keywords_expld)
                    ->fromUsers($keywords_expld);
            });
        }
    }

    public function scopeFromUsers($query, $keywords_expld)
    {
        if(!empty($keywords_expld)) {
            $site_code = config("constants.site_code");
            foreach($keywords_expld as $keyword) {
                if($keyword != "") {
                    $query = $query->orWhere('first_name', 'like', '%'.$keyword.'%')
                        ->orWhere('last_name', 'like', '%'.$keyword.'%');
                    /**
                     * Prepare text for ID base search name as profile code.
                     * MWED1 = 1 userid
                     */
                    $site_code_exists = substr($keyword, 0, 4);
                    if($site_code_exists === $site_code) {
                        // check and see the id is there
                        $keyword_search = str_replace($site_code, "", $keyword);
                        if(is_numeric($keyword_search)) {
                            $query = $query->orWhere('id', $keyword_search);
                        }
                    }
                }
            }
            return $query;
        }
    }

    public function scopeFromCountryKeyWords($query, $keywords_expld)
    {
        if(!empty($keywords_expld)) {
            $query->where(function ($query) use($keywords_expld) {
                $query->whereIn('country_id', function($subQuery) use($keywords_expld) {
                    foreach($keywords_expld as $keyword) {
                        $subQuery = $subQuery->select('id')->from('countries')->orWhere('name', 'like', '%'.$keyword.'%');
                    }
                    return $subQuery;
                });
            });
        }
    }
    public function scopeFromStateKeyWords($query, $keywords_expld)
    {
        if(!empty($keywords_expld)) {
            $query->orWhere(function ($query) use($keywords_expld) {
                $query->whereIn('state_id', function($subQuery) use($keywords_expld) {
                    foreach($keywords_expld as $keyword) {
                        $subQuery = $subQuery->select('id')->from('states')->orWhere('name', 'like', '%'.$keyword.'%');
                    }
                    return $subQuery;
                });
            });
        }
    }
    public function scopeFromCityKeyWords($query, $keywords_expld)
    {
        if(!empty($keywords_expld)) {
            $query->orWhere(function ($query) use($keywords_expld) {
                $query->whereIn('city_id', function($subQuery) use($keywords_expld) {
                    foreach($keywords_expld as $keyword) {
                        $subQuery = $subQuery->select('id')->from('cities')->orWhere('name', 'like', '%'.$keyword.'%');
                    }
                    return $subQuery;
                });
            });
        }
    }
    public function scopeFromEducationKeyWords($query, $keyword)
    {
        $query->orWhere(function ($query) use($keyword) {
            $query->whereIn('education_id', function($subQuery) use($keyword) {
                $subQuery = $subQuery->select('id')->from('educations')->orWhere('education', 'like', '%'.$keyword.'%');
                return $subQuery;
            });
        });
    }
    public function scopeFromProfessionKeyWords($query, $keyword)
    {
        $query->orWhere(function ($query) use($keyword) {
            $query->whereIn('profession_id', function($subQuery) use($keyword) {
                $subQuery = $subQuery->select('id')->from('professions')->orWhere('profession', 'like', '%'.$keyword.'%');
                return $subQuery;
            });
        });
    }
    public function scopeFromLanguageKeyWords($query, $keyword)
    {
        $query->orWhere(function ($query) use($keyword) {
            $query->whereIn('language_id', function($subQuery) use($keyword) {
                $subQuery = $subQuery->select('id')->from('languages')->orWhere('language', 'like', '%'.$keyword.'%');
                return $subQuery;
            });
        });
    }
    public function scopeFromSectKeyWords($query, $keyword)
    {
        $query->orWhere(function ($query) use($keyword) {
            $query->whereIn('sect_id', function($subQuery) use($keyword) {
                $subQuery = $subQuery->select('id')->from('communities')->orWhere('name', 'like', '%'.$keyword.'%');
                return $subQuery;
            });
        });
    }
    public function scopeFromSubCastKeyWords($query, $keyword)
    {
        $query->orWhere(function ($query) use($keyword) {
            $query->whereIn('sub_cast_id', function($subQuery) use($keyword) {
                $subQuery = $subQuery->select('id')->from('subcastes')->orWhere('name', 'like', '%'.$keyword.'%');
                return $subQuery;
            });
        });
    }
    public function scopeFromCountry($query, $country_id)
    {
        if($country_id != "" && (is_array($country_id) && !in_array(config('constants.doesnt_matter'), $country_id))) {
            return $query->whereIn('country_id', $country_id);
        }
    }
    public function scopeFromState($query, $state_id)
    {
        if($state_id != "" && (is_array($state_id) && !in_array(config('constants.doesnt_matter'), $state_id))) {
            return $query->whereIn('state_id', $state_id);
        }
    }
    public function scopeFromCity($query, $city_id)
    {
        if($city_id != "" && (is_array($city_id) && !in_array(config('constants.doesnt_matter'), $city_id))) {
            return $query->whereIn('city_id', $city_id);
        }
    }
    public function scopeFromReligion($query, $religion_id)
    {
        if($religion_id != "" && config('constants.doesnt_matter') != $religion_id) {
            return $query->whereHas('users_information',
                function($innerQuery) use($religion_id) {
                    $innerQuery->where(
                        function($innerQuery1) use($religion_id){
                            return $innerQuery1->where('religion_id', $religion_id);
                        });
                });
        }
    }

    public function scopeFromUserInfoArr($query, $arr)
    {
        return $query->whereHas('users_information',
            function($innerQuery) use($arr) {
                foreach ($arr as $key => $value) {
                    if($key != "" && $value != "" && !in_array(config('constants.doesnt_matter'), $value)) {   //!in_array(config('constants.doesnt_matter'), $value)
                        $innerQuery->whereIn($key, $value);
                    }
                }
            });

//        $arr['sect_id']=$search_pref['sect'];
//        $arr['language_id']=$search_pref['language'];
//        $arr['education_id']=$search_pref['education_id'];
//        $arr['body_type']=$search_pref['body_type'];
//        $arr['food']=$search_pref['food'];
//        $arr['smoke']=$search_pref['smoke'];
//        $arr['drink']=$search_pref['drink'];
//        $arr['marital_status']=$search_pref['marital_status'];

    }


    public function scopeFromUserInfoStr($query, $arr)
    {
        return $query->whereHas('users_information',
            function($innerQuery) use($arr) {
                foreach ($arr as $key => $value) {
                    if($key != "" && $value != "" && ($value!=config('constants.doesnt_matter'))) {
                        $innerQuery->whereIn($key, explode(",",$value));
                    }
                }
            });
    }

    public function scopeFromUserInfoOrArr($query, $arr)
    {
        return $query->whereHas('users_information',

            function($innerQuery) use($arr) {
                $innerQuery->where(
                function($innerQuery) use($arr) {
                    foreach ($arr as $key => $value) {
                        if($key != "" && $value != "" && !in_array(config('constants.doesnt_matter'), $value)) {
                            $innerQuery->orWhereIn($key, $value);
                        }
                    }
                });
            });
    }

    public function scopeFilterHeight($query, $height)
    {
        if(!empty($height)) {
            return $query->whereHas('users_information',
                function($innerQuery) use($height) {
                    $innerQuery->where(
                        function($innerQuery) use($height){
                            return $innerQuery->where('height', '>=', (int) $height[0])
                                ->where('height', '<=', (int) $height[1]);
                        });
                });
        }
    }

    public function scopeSearchAge($query, $age)
    {
        return $query->whereDate('date_of_birth', '<=', Carbon::today()->subYears((int) $age[1])->toDateString())
            ->whereDate('date_of_birth', '>=', Carbon::today()->subYears((int) $age[0])->toDateString());
    }

    public function scopeSkipWhoBlockedByMeAnot($query)
    {
        return $query->whereNotIn('users.id', function($subQuery){
            //$user = Auth::user();
            if(Auth::user()) {
                $user = Auth::user();
            } else {
                $user = Auth::guard('api')->user();
            }
            $subQuery->select('to_user')->from('blocked_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipWhoBlockedMeAnot($query)
    {
        return $query->whereNotIn('users.id', function($subQuery){
            //$user = Auth::user();

            $subQuery->select('from_user')->from('blocked_users')->where('to_user','=', User::getAuthUser()->id);
        });
    }

    /**
     * For search on new-search page by Rajendra Banker
     */
    /**
     * Membership Module Rajendra Banker STARTED
     */
    public function scopeGetPaidMembers($query, $type = '')
    {
        if($type == "p") {
            return $query->hasVendorRole()
            ->whereHas('paid_members', function($innerQuery) {
                $innerQuery->whereDate('membership_end_date','>=', date("Y-m-d"))
                            ->where('payment_gateway_name', '<>', "Free Diamond");
            });
        } else if($type == "fd") {
            return $query->hasVendorRole()
            ->whereHas('paid_members', function($innerQuery) {
                $innerQuery->whereDate('membership_end_date','>=', date("Y-m-d"))
                            ->where('payment_gateway_name', '=', "Free Diamond");
            });
        } else {
            return $query->hasVendorRole()
            ->whereHas('paid_members', function($innerQuery) {
                $innerQuery->whereDate('membership_end_date','>=', date("Y-m-d"));
            });
        }
        
    }

    public function scopeGetPlanWiseMembers($query, $duration, $duration_type)
    {
        return $query->hasVendorRole()
            ->whereHas('paid_members', function($innerQuery) use($duration, $duration_type) {
                $innerQuery->where('duration','=', $duration)
                    ->where('duration_type','=', $duration_type);
            });
    }

    public function scopeGetExpiredMembers($query)
    {
        return $query->hasVendorRole()
            ->whereHas('paid_members', function($innerQuery) {
                $innerQuery->whereDate('membership_end_date','<', date("Y-m-d"));
            });
    }

    public function scopeGetUpcomingRenewalMembers($query)
    {
        return $query->hasVendorRole()
            ->whereHas('paid_members', function($innerQuery) {
                $innerQuery->whereDate('membership_end_date','<', date("Y-m-d", strtotime("+ 1 month")))
                    ->whereDate('membership_end_date','>=', date("Y-m-d"));
            });
    }

    /**
     * Membership Module Rajendra Banker ENDED
     */
    public function currentMembershipStatus() {
        $user = $this;
        $getMembershipStatus= PaidMembers::where('user_id', $this->id)
            ->whereDate('membership_end_date', '>=', date("Y-m-d"))
            ->whereIn('plan_id', function($subQuery)  {
                $subQuery=$subQuery->select('id')->from('membership_plans');
                $subQuery->where('plan_type', 'Paid');
                return $subQuery;
            })
            ->first();

        $status="Free";
        if(!empty($getMembershipStatus)) {
            $status="Paid";
        }

        if($status=="Free") {
            $activePlan=MembershipPlans::where('plan_type', 'Free')->where('status','A')->first();
            $public_photo_allowed=$activePlan->public_photos_allowed; // config('constants.public_photos_allowed');  // this section will change later. The photos_allowed will come on the basis of user's membership plan
            $message_allowed=$activePlan->msg_allowed;
        } else {
            $activePlan=MembershipPlans::where('id',$getMembershipStatus->plan_id)->first();
            $public_photo_allowed= $activePlan->public_photos_allowed; //config('constants.public_photos_allowed');  // this section will change later. The photos_allowed will come on the basis of user's membership plan
            $message_allowed=$activePlan->msg_allowed;
        }

        $returnArr['status']=$status;
        $returnArr['public_photo_allowed']=$public_photo_allowed;
        $returnArr['message_allowed']=$message_allowed;

        return $returnArr;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token, $this->email, $this->first_name));
    }

    /**
     * Get Never Paid Users
     */
    public function scopeGetNeverPaidUsers($query)
    {
        return $query->hasVendorRole()
            ->whereNotIn('id', function($innerQuery) {
                $innerQuery->select('user_id')->from('paid_members');
            })->isActive();
    }

    /**
     * Get Never Paid Users
     */
    public function scopeGetPendingReviewUsers($query)
    {   
        return $query->whereIn('id', function($innerQuery) {
                $innerQuery->select('user_id')->distinct()->from('user_profile_flags')->where('profile_updated', '1');
            })->orWhereIn('id', function($innerQuery) {
                $innerQuery->select('user_id')->distinct()->from('users_images')->where('is_approved', 'N');
            })->isActive()->onlySite();
    }

    /**
     * Get Vendor Pending Reviews
     */
    public function scopeGetPendingReviewVendors($query)
    {   
        return $query->whereIn('id', function($innerQuery) {
                $innerQuery->select('user_id')->distinct()->from('user_profile_flags')->where('profile_updated', '1');
            })->orWhereIn('id', function($innerQuery) {
                $innerQuery->select('vendor_id as user_id')->distinct()->from('vendors_images')->where('is_approved', 'N');
            })->orWhereIn('id', function($innerQuery) {
                $innerQuery->select('vendor_id as user_id')->distinct()->from('vendors_videos')->where('is_approved', 'N');
            })->orWhereIn('id', function($innerQuery) {
                $innerQuery->select('vendor_id as user_id')->distinct()->from('vendors_custom_faqs')->where('is_approved', 'N');
            })->isActive()->onlySite();
    }

    /**
     * Get Profile Pic for not logged in user - review box
     */
    public function getProfilePicForGuest()
    {
        $curId = $this->id;
        if($this->banned == 1 || $this->deactivated == 1 || $this->deleted_at != null || $this->account_show=='N') {
            return URL::asset('img/blocked.png');
        }

        $profile_pic =  $this->users_images()->where('is_profile_image', 'Y')->where('is_approved', 'Y')->first();
        if(is_null($profile_pic)){
            $profile_pic =  $this->users_images()->where('is_approved', 'Y')->first();
            if(!is_null($profile_pic)){
                return config('constants.S3_WEDDING_IMAGES').$profile_pic->image_thumb;
            } else {
//                if ($this->gender == 'M') {
//                    return URL::asset('img/male-default.png');
//                } else {
//                    return URL::asset('img/female-default.png');
//                }
                return URL::asset('img/default.png');
            }
        } else {
            return config('constants.S3_WEDDING_IMAGES').$profile_pic->image_thumb;
        }
    }
    /**
     * Get user which are not active last week
     */
    public function scopeGetLastWeekInactiveUsers($query)
    {
        return $query->whereDate('last_seen', '<', Carbon::now()->subWeek())
                    ->hasUserRole()
                    ->isActive()
                    ->isNotDeleted();
    }
    // is not deleted
    public function scopeIsNotDeleted($query){
        return $query->where('deleted_at', null);
    }

    public function scopeVendorDeleted($query){
        return $query->where('deleted_at', '<>', null)
                    ->hasVendorRole();
    }

    /**
     * Get members whose membership expires in 5,2 days
     */
    public function scopeGetDaysRenewalMembers($query, $days)
    {
        if($days == 5) {
            return $query->hasVendorRole()
                ->whereHas('paid_members', function($innerQuery) use ($days) {
                    $innerQuery->whereDate('membership_end_date','<=', date("Y-m-d", strtotime("+ ".$days." day")))
                        ->whereDate('membership_end_date','>', date("Y-m-d", strtotime("+ 2 day")));
                });
        } elseif($days == 2) {
            return $query->hasVendorRole()
            ->whereHas('paid_members', function($innerQuery) use ($days) {
                $innerQuery->whereDate('membership_end_date','<=', date("Y-m-d", strtotime("+ ".$days." day")))
                    ->whereDate('membership_end_date','>=', date("Y-m-d"));
            });
        }
    }

    /**
     * Get expired members with 1 days
     */
    public function scopeGetMembershipExpiredUsers($query)
    {
        return $query->hasVendorRole()
            ->whereHas('paid_members', function($innerQuery) {
                $innerQuery->whereDate('membership_end_date', '>', date("Y-m-d", strtotime("- 2 day")))
                            ->whereDate('membership_end_date','<', date("Y-m-d"));
            });
    }

    /**
     * get profile field approved or not
     */

    public function checkProfileFieldApproved($key)
    {
        if($key != "") {
            if(UserProfileFlag::where('user_id', $this->id)->where('property_name', $key)->exists()) {
            if(Auth::id() == $this->id) {
                return true;
            } else {
                return false;
            }
            } else {
            return true;
            }
        } else {
        return true;
        }
    }

    public function hasProfilePhoto()
    {
        $where = array(
            'user_id' => $this->id //,
          //  'image_type' => 'public'
        );
        $coverCount = UsersImage::where($where)->count();
        if($coverCount == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function hasVendorProfilePhoto()
    {
        $where = array(
            'vendor_id' => $this->id,
            'image_type' => 'profile'
        );
        $profileCount = VendorsImage::where($where)->count();
        if($profileCount == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * For Searching vendors - Rajendra Banker on 9th September 2019
     */
    public function scopeGetVendors($query, $search_pref = []) {
        $query->hasVendorRole()
            ->isActive();
        if(!isset($search_pref['keywords'])) {
            // $arr['sect_id'] = $search_pref['sect'];
            // $arr['language_id'] = $search_pref['language'];
            // $arr['education_id'] = $search_pref['education_id'];
            // $arr['body_type'] = $search_pref['body_type'];
            // $arr['food'] = $search_pref['food'];
            // $arr['smoke'] = $search_pref['smoke'];
            // $arr['drink'] = $search_pref['drink'];
            // $arr['marital_status'] = $search_pref['marital_status'];
            // // custom search preference
            // return $query->fromCountry($search_pref['country_code'])
            //     ->fromState($search_pref['state_id'])
            //     ->fromCity($search_pref['city_id'])
            //     ->fromReligion($search_pref['religion'])
            //     ->filterAge([$search_pref['age_from'], $search_pref['age_to']])
            //     ->filterHeight([$search_pref['height_from'], $search_pref['height_to']])
            //     ->fromUserInfoArr($arr);
        } else {
            return $query->fromKeyWords($search_pref['keywords']);
        }
    }

    public function getVendorProfileLink() {
        $vendor_slug = $this->getEncryptedId($this->id);
        // add slug if not blank
        $getCompanySlug = VendorsInformation::where('vendor_id', $this->id)->first(['company_slug']);
        if($getCompanySlug->company_slug != '') {
            $vendor_slug = $getCompanySlug->company_slug;
        }
        if($this->banned == 1 || $this->deactivated == 1 || $this->deleted_at != null || $this->account_show == 'N') {
            if(Auth::check()) {
                if(Auth::id() == $this->id) {
                    return route('show-vendor', [$vendor_slug]);        
                }
            }
            return route('profile-not-found');
        } else {
            return route('show-vendor', [$vendor_slug]);
        }
    }

    // get vendor vategory
    public function getCategoryVendor() {
        return VendorCategory::where('id', $this->vendors_information->vendor_category)->first(['id','name']);
    }

    /**
     * For get vendors for category and location search - Rajendra Banker on 16th September * 2019
     */
    public function scopeGetVendorResults($query, $search_pref = []) {
        $query->hasVendorRole();
        if($search_pref['additional_cat'] == '') {
            $query->getVendorByType('wedding');
        }
        return $query->isActive()->getShowcaseOnly()->fromCatVendorCityZip($search_pref);
    }

    public function scopeFromCatVendorCityZip($query, $keywords)
    {
        $keywords_expld_city_zip = $keywords['city_zip'];
        $keywords_expld_cat_ven = $keywords['category_name'];
        $additional_cat = $keywords['additional_cat'];
        if(!empty($keywords)) {
            if(!empty($keywords_expld_cat_ven) && $keywords['category_name'] != '') {
                $query->where(function ($query) use($keywords_expld_cat_ven, $additional_cat) {
                    $query->whereHas('vendors_information',
                        function($innerQuery) use($keywords_expld_cat_ven) {
                            $innerQuery->where(
                                function($innerQuery) use($keywords_expld_cat_ven) {
                                    if($keywords_expld_cat_ven != '') {
                                        $innerQuery = $innerQuery->orWhere(function ($query) use($keywords_expld_cat_ven) {
                                            $query->whereIn('vendor_category', function($subQuery) use($keywords_expld_cat_ven) {
                                                $vendor_type_id = config('constants.vendor_type_id.wedding');
                                                $subQuery=$subQuery->select('id')->from('vendors_categories')->orWhere('name', 'like', '%'.$keywords_expld_cat_ven.'%');
                                                return $subQuery;
                                            });
                                        });
                                    }
                                    return $innerQuery;
                                });
                        })
                        ->withAdditionalCategory($additional_cat);
                        
                        //->fromVendorName($keywords_expld_cat_ven)
                        //->fromVendorCompanyName($keywords_expld_cat_ven);
                });
            }
            if($keywords['location_type'] != '' && $keywords['location_id'] != '') {
                if($keywords['location_type'] == 'country' && $keywords['location_id'] != '') {
                    $query->where('country_id', $keywords['location_id']);
                }
                if($keywords['location_type'] == 'state' && $keywords['location_id'] != '') {
                    $query->where('state_id', $keywords['location_id']);
                }
                if($keywords['location_type'] == 'city' && $keywords['location_id'] != '') {
                    $query->where('city_id', $keywords['location_id']);
                }
                return $query; 
            } else if(!empty($keywords_expld_city_zip) && $keywords['city_zip'] != '') {
                $query->where(
                    function($query) use($keywords_expld_city_zip) {
                        $innerQuery = $query->whereIn('country_id',function($subQuery) use($keywords_expld_city_zip) {
                                $subQuery = $subQuery->select('id')->from('countries')->orWhere('name', 'like', '%'.$keywords_expld_city_zip.'%');
                                return $subQuery;
                            })
                            ->orWhereIn('state_id',function($subQuery) use($keywords_expld_city_zip) {
                                $subQuery = $subQuery->select('id')->from('states')->orWhere('name', 'like', '%'.$keywords_expld_city_zip.'%');
                                return $subQuery;
                            })
                            ->orWhereIn('city_id',function($subQuery) use($keywords_expld_city_zip) {
                                $subQuery = $subQuery->select('id')->from('cities')->orWhere('name', 'like', '%'.$keywords_expld_city_zip.'%');
                                return $subQuery;
                            });
                            //->orWhere('zip', 'like', '%'.$keywords_expld_city_zip.'%');
                        return $innerQuery;
                });
            } else {
                // $ip_address = '';
                // // search with IP
                // if(Auth::check()) {
                //     // get user IP
                //     $userObj = User::where('id', Auth::id())->first(['id', 'signup_ip_address']);
                //     if($userObj->signup_ip_address != '') {
                //         $ip_address = $userObj->signup_ip_address;
                //     }
                // } else {
                //     // get guest IP
                //     $ip_address = request()->ip();
                //     // $ip_address = '43.250.158.190'; // for local
                // }
                // if($ip_address != '') {
                //     // get country and search
                //     $county_info = unserialize(file_get_contents('http://pro.ip-api.com/php/'.$ip_address.'?key=RocDHDkeHbv0ask&fields=country'));
                //     try {
                //         // search
                //         $query->where(
                //             function($query) use($county_info) {
                //                 $innerQuery = $query->whereIn('country_id',function($subQuery) use($county_info) {
                //                         $subQuery = $subQuery->select('id')->from('countries')->orWhere('name', 'like', '%'.$county_info['country'].'%');
                //                         return $subQuery;
                //                     });
                //                 return $innerQuery;
                //         });
                //     } catch(\Exception $e) {
                //         // some issue
                //     }
                // }
            }
        }
    }

    // scope search with additional category
    public function scopeWithAdditionalCategory($query, $additional_cat = '')
    {
        if($additional_cat != '') {
            $query->orWhereHas('vendors_showcase',
                    function($innerQuery) use($additional_cat) {
                        $innerQuery->where(
                            function($innerQuery) use($additional_cat) {
                                if($additional_cat != '') {
                                    $innerQuery = $innerQuery->where('additional_categories', 'like', '%;s:'.strlen($additional_cat).':"'.$additional_cat.'";%');
                                }
                                return $innerQuery;
                    });
            });
        }
    }

    // search with first name and last name
    public function scopeFromVendorName($query, $keywords)
    {
        if(!empty($keywords)) {
            foreach($keywords as $keyword) {
                if($keyword != "") {
                    $query = $query->orWhere('first_name', 'like', '%'.$keyword.'%')
                        ->orWhere('last_name', 'like', '%'.$keyword.'%');
                }
            }
            return $query;
        }
    }
    
    // search with company name
    public function scopeFromVendorCompanyName($query, $keywords)
    {   
        $query->orWhere(function ($query) use($keywords) {
            $query->whereHas('vendors_information',
                function($innerQuery) use($keywords) {
                    $innerQuery->where(
                        function($subQuery) use($keywords) {
                            if(is_array($keywords) && count($keywords)>0) {
                                foreach($keywords as $keyword) {
                                    if($keyword != "") {
                                        $subQuery = $subQuery->orWhere('company_name', 'like', '%'.$keyword.'%');
                                    }
                                }
                                return $subQuery;
                            }
                        });
                    return $innerQuery;
                });
        });
    }

    // get total review for vendor
    public function getTotalReviewVendor()
    {
        return Review::where(['vendor_id' => $this->id, 'approved' => '1'])
                    ->count();
    }

    // get average rating for vendor
    public function getAverageReviewRating()
    {
        return Review::where(['vendor_id' => $this->id, 'approved' => '1'])
                    ->avg('rating');
    }

    public function getSumReviewRating()
    {
        return Review::where(['vendor_id' => $this->id, 'approved' => '1'])
                    ->sum('rating');
    }

    // get total rating for vendor star specific
    public function getCountReviewStarSpecific($star)
    {
        return Review::where(['vendor_id' => $this->id, 'approved' => '1', 'rating' => $star])
                    ->count();
    }

    // get bar for rating as per the average
    public function getSpcificStartBarPer($br)
    {
        $bar = 0;
        if($this->getTotalReviewVendor() > 0) {
            $bar_percentage = ( $this->getCountReviewStarSpecific($br) * 100 /  $this->getTotalReviewVendor() );
        } else {
            $bar_percentage = 0;
        }
        if($bar_percentage == 0) {
            $bar = 0;
        } else if($bar_percentage > 0 && $bar_percentage <= 10) {
            $bar = 1;
        } else if($bar_percentage > 10 && $bar_percentage <= 20) {
            $bar = 2;
        } else if($bar_percentage > 20 && $bar_percentage <= 30) {
            $bar = 3;
        } else if($bar_percentage > 30 && $bar_percentage <= 40) {
            $bar = 4;
        } else if($bar_percentage > 40 && $bar_percentage <= 50) {
            $bar = 5;
        } else if($bar_percentage > 50 && $bar_percentage <= 60) {
            $bar = 6;
        } else if($bar_percentage > 60 && $bar_percentage <= 70) {
            $bar = 7;
        } else if($bar_percentage > 70 && $bar_percentage <= 80) {
            $bar = 8;
        } else if($bar_percentage > 80 && $bar_percentage <= 90) {
            $bar = 9;
        } else if($bar_percentage == 100) {
            $bar = 10;
        }
        return $bar;
    }

    // get profile flag value for user field
    public function getFlagPropertyValue($property_name)
    {
        return UserProfileFlag::where(['user_id' => $this->id, 'property_name' => $property_name])->first(['id', 'property_value']);
    }

    // get vendors from category and country, state and city combination
    public function scopeGetVendorCatCountryStateCity($query, $categories = '', $country = '', $state = '', $city = '') {
        return $query->hasVendorRole()
                ->getShowcaseOnly()
                ->isActive()
                ->getVendorByType('wedding')
                ->getVendorFromCat($categories)
                ->withShowcaseCategories($categories)
                ->getVendorFromCountryStateCity($country, $state, $city);
    }

    // scope search with showcase categories
    public function scopeWithShowcaseCategories($query, $categories = '')
    {
        $categories_arry = explode(',', $categories);
        if(!empty($categories_arry)) {
            foreach($categories_arry as $category) {
                $query->orWhereHas('vendors_showcase',
                        function($innerQuery) use($category) {
                            $innerQuery->where(
                                function($innerQuery) use($category) {
                                    if($category != '') {
                                        $innerQuery = $innerQuery->where('additional_categories', 'like', '%;s:'.strlen($category).':"'.$category.'";%');
                                    }
                                    return $innerQuery;
                        });
                });
            }
        }
    }

    public function scopeGetVendorFromCat($query, $categories = '')
    {
        $categories_arry = explode(',', $categories);
        if(!empty($categories_arry)) {
            return $query->whereHas('vendors_information',
                function($query) use($categories_arry) {
                    $query->whereIn('vendor_category', $categories_arry);
                });
        }
    }

    public function scopeGetVendorFromCountryStateCity($query, $country = '', $state = '', $city = '')
    {
        if($country != '') {
            $query->where('country_id', $country);
        }
        if($state != '') {
            $query->where('state_id', $state);
        }
        if($city != '') {
            $query->where('city_id', $city);
        }
        return $query; 
    }

    /**
     * For get vendors for category and location search - Rajendra Banker on 16th September * 2019
     */
    public function scopeGetSimilarVendorResults($query, $category = '') {
        $query->hasVendorRole()
            ->getVendorByType('wedding')
            ->isActive();
        return $query->getVendorFromCat($category);
    }

    /**
     * Get Paid Vendor user for home page
     */
    public function scopeGetPaidVendor($query, $limit = 6)
    {
        return $query->hasVendorRole()
            ->getShowcaseOnly()
            ->isActive()
            ->getVendorByType('wedding')
            ->where('is_paid', 1)
            ->inRandomOrder()
            ->limit($limit);
    }

    /**
     * Get Paid Vendor user for home page for showcase video
     */
    public function scopeGetPaidVendorForVids($query, $limit = 6)
    {
        return $query->hasVendorRole()
            ->getShowcaseOnly()
            ->isActive()
            ->getVendorByType('wedding')
            ->where('is_paid', 1)
            ->limit($limit);
    }

    /**
     * Get Paid Vendor user for home page
     */
    public function scopeGetFeaturedVendor($query, $limit = 6)
    {
        return $query->hasVendorRole()
            ->getShowcaseOnly()
            ->where('is_paid', 0)
            ->isActive()
            ->getVendorByType('wedding')
            ->inRandomOrder()
            ->limit($limit);
    }

    /**
     * has random single video for vendor
     */
    public function hasRandVideo()
    {
        if(VendorVideo::where('vendor_id', $this->id)->where('is_approved', 'Y')->exists()) {
            return true;
        }
        return false;
    }

    /**
     * get random video for vendor
     */
    public function getSingleRandVideo()
    {
        if(VendorVideo::where('vendor_id', $this->id)->where('is_approved', 'Y')->exists()) {
            return VendorVideo::where('vendor_id', $this->id)->where('is_approved', 'Y')->inRandomOrder()
            ->first();
        }
        return false;
    }

    /**
     * get profile field value for logged vendor preview
     */
    public function getProfileFieldExistsLoggedVendor($key, $type = 'u', $self = false)
    {
        if($key != "") {
            if($self == true) {
                if(UserProfileFlag::where('user_id', Auth::id())->where('property_name', $key)->exists()) {
                    $property_obj = UserProfileFlag::where('user_id', Auth::id())->where('property_name', $key)->first(['property_name', 'property_value']);
                    return $property_obj->property_value;
                } else {
                    if($type == 'u') {
                        $property_obj = User::where('id', $this->id)->first(['id', $key]);
                        return $property_obj->$key;
                    } else if($type == 'vi') {
                        $property_obj = VendorsInformation::where('vendor_id', $this->id)->first(['id', $key]);
                        return $property_obj->$key;
                    }
                }
            } else {
                // get type table
                if($type == 'u') {
                    $property_obj = User::where('id', $this->id)->first(['id', $key]);
                    return $property_obj->$key;
                } else if($type == 'vi') {
                    $property_obj = VendorsInformation::where('vendor_id', $this->id)->first(['id', $key]);
                    return $property_obj->$key;
                }
            }
        } else {
            return false;
        }
    }

    public function scopeHasNotAdminRole($query)
    {
        return $query->whereHas('roles', function ($innerQuery) {
            $innerQuery->where('roles.slug', '<>', 'admin');
        });
    }

    /**
     * Get user which are not admin
     */
    public function scopeGetActiveAllUsers($query)
    {
        return $query->hasNotAdminRole()
                    ->isActive()
                    ->isNotDeleted();
    }

    /**
     * Scope get type specific vendors
     */
    public function scopeGetVendorByType($query, $type = 'wedding')
    {
        $vendor_type_id = config('constants.vendor_type_id.'.$type);
        return $query->where('vendor_type', $vendor_type_id);
    }

    public function scopeHasActiveEventVendor($query)
    {
        return $query->hasVendorRole()
            ->isActive();
    }

    /**
     * Type of Vendor
     */
    public function isTypeOfVendor($type = 'wedding')
    {
        $vendor_type_id = config('constants.vendor_type_id.'.$type);
        return ($this->vendor_type == $vendor_type_id) ? true : false ;
    }

    /**
     * get vendor type
     */
    public function showVendorType()
    {
        return VendorType::where('id', $this->vendor_type)->first();
    }

    /**
     * Get site specific records
     */
    public function scopeOnlySite($query) {
        $site_id = config("constants.admin_site_id");
        return $query->where('site_id',$site_id);
    }

    /**
     * Get Total Events of User
     */
    public function getTotalEvents()
    {
        return VendorEventInformation::where('vendor_id', $this->id)->count();
    }

    /**
     * Get showcase vendors only
     */
    public function scopeGetShowcaseOnly($query)
    {
        $query->whereHas('vendors_showcase',
                    function($innerQuery) {
                        $site_id = config('constants.site_id');
                        $innerQuery = $innerQuery->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%');
                        return $innerQuery;          
            });
    }
}