<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class NewsLetterSubscription extends BaseModel
{
    protected $table = 'newsletter_subscriptions';
    public $timestamps = true;
    protected $fillable = [
        'email',
        'status',
        'site_id'
    ];
    // scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('email', 'like', '%'.$search.'%');
        });
    }
}
