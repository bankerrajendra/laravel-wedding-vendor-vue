<?php

/**
 * Created by Rajendra Banker.
 * Date: Mon, 20th May 2019.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class Improve extends BaseModel
{
	protected $table = 'site_improvements';

	protected $fillable = [
		'user_id',
		'subject',
		'message',
		'site_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	// scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('subject', 'like', '%'.$search.'%')
            			->orWhere('message', 'like', '%'.$search.'%');
        });
    }
}
