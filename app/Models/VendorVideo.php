<?php

/**
 * Created by Rajendra Banker.
 * Date: Tue, 6 Aug 2019 17:36:54 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class VenderVideo
 * 
 * @property int $id
 * @property int $vendor_id
 * @property string $embed_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */

class VendorVideo extends BaseModel
{
	protected $table = 'vendors_videos';

	protected $casts = [
		'vendor_id' => 'int'
	];

	protected $fillable = [
		'vendor_id',
		'video_title',
		'embed_code',
		'description',
        'is_approved',
        'disapprove_reason'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
	
}
