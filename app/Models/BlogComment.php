<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 09 Nov 2018 23:06:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class BlogComment extends BaseModel
{
    //
    protected $casts = [
        'status' => 'bool'
    ];

    protected $fillable = [
        'name',
        'email',
        'mobile_number',
        'messages',
    ];

    public function blog(){
       return $this->belongsTo(Blog::class);
    }

}
