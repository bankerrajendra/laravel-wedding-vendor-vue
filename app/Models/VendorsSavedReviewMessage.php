<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 11 Oct 2019 13:21:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class VendorsSavedReviewMessage
 * 
 * @property int $id
 * @property int $vendor_id
 * @property string $message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Education $education
 * @property \App\Models\Profession $profession
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class VendorsSavedReviewMessage extends BaseModel
{
	protected $table = 'vendors_saved_review_message';

	public $timestamps  = false;

	protected $casts = [
		'vendor_id' => 'int'
	];

	protected $fillable = [
		'vendor_id',
		'review_message'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
