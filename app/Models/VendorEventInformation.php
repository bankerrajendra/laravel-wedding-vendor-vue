<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Nov 2019 16:26:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

/**
 * Class VendorEventInformation
 * 
 * @property int $id
 * @property int $user_id
 * @property string $drink
 * @property string $smoke
 * @property string $food
 * @property int $education_id
 * @property int $profession_id
 * @property string $about
 * @property string $country_code
 * @property string $mobile_number
 * @property string $number_verification_code
 * @property string $number_verified
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Education $education
 * @property \App\Models\Profession $profession
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class VendorEventInformation extends BaseModel
{
	protected $table = 'vendors_events_information';

	public $timestamps  = true;

	protected $casts = [
		'vendor_id' => 'int',
		'country_id' => 'int',
		'state_id' => 'int',
		'city_id' => 'int',
		'is_paid' => 'int',
		'site_id' => 'int'
	];

	protected $fillable = [
		'vendor_id',
        'event_name',
        'venue',
        'artists',
		'event_slug',
		'address',
		'country_id',
		'state_id',
		'city_id',
		'zip',
		'description',
		'ticket_information',
		'categories',
		'start_date',
		'end_date',
		'ticket_url',
		'meta_keywords',
        'video_url',
        'online_seat_selection',
        'parking',
        'food_for_sale',
        'drinks_for_sale',
        'babysitting_services',
		'is_approved',
		'is_paid',
		'site_id'
	];

	protected $site_id;

	public function __construct()
    {
        $this->site_id = config('constants.site_id');
    }

	public function vendor_category()
	{
		return $this->belongsTo(VendorCategory::class);
	}

	public function vendor()
	{
		return $this->belongsTo(User::class, 'vendor_id');
	}

    public function reviews()
    {
        return $this->hasMany(\App\Models\EventReview::class, 'event_id');
    }

    public function event_images()
    {
        return $this->hasMany(\App\Models\VendorEventImage::class, 'event_id');
    }

	public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
	}
	
	public function getEncryptedId()
	{
        return base64_encode($this->id);
    }

	public function getDecryptedId()
	{
        return base64_decode($this->id);
    }

	public function scopeWithEncryptedId($query, $encryptedId)
	{
        $id = base64_decode($encryptedId);
        return $query->where('id', '=', $id);
    }

	public function scopeHasActiveEventVendor($query)
	{
		return $query->whereHas('vendor', function ($innerQuery) {
			$innerQuery->isActive()
					->hasVendorRole();
        });
	}

	/**
     * For get events for category and location search - Rajendra Banker on 29th November - 2019
     */
	public function scopeGetEventsResults($query, $search_pref = []) 
	{
        $query->hasActiveEventVendor()
		->where('is_approved', 1)
        ->where('site_id', $this->site_id)
        ->where('start_date', '>=', date("Y-m-d"));
        return $query->fromCatVendorCityZip($search_pref);
    }

    public function scopeFromCatVendorCityZip($query, $keywords)
    {
        $keywords_expld_city_zip = $keywords['city_zip'];
		$keywords_expld_cat_ven = $keywords['category_id'];
		if(!empty($keywords)) {
            if(!empty($keywords_expld_cat_ven) && $keywords['category_id'] != '') {
                $query->where('categories', 'like', '%;s:'.strlen($keywords_expld_cat_ven).':"'.$keywords_expld_cat_ven.'";%');
            }
            if($keywords['category_id'] == '' && $keywords['category_name'] != '') {
                $query->where('categories', 'like', '%;s:'.strlen($keywords_expld_cat_ven).':"'.$keywords_expld_cat_ven.'";%');
            }
            if($keywords['location_type'] != '' && $keywords['location_id'] != '') {
                if($keywords['location_type'] == 'country' && $keywords['location_id'] != '') {
                    $query->where('country_id', $keywords['location_id']);
                }
                if($keywords['location_type'] == 'state' && $keywords['location_id'] != '') {
                    $query->where('state_id', $keywords['location_id']);
                }
                if($keywords['location_type'] == 'city' && $keywords['location_id'] != '') {
                    $query->where('city_id', $keywords['location_id']);
                }
                return $query; 
            } else if(!empty($keywords_expld_city_zip) && $keywords['city_zip'] != '') {
                $query->where(
                    function($query) use($keywords_expld_city_zip) {
                        $innerQuery = $query->whereIn('country_id',function($subQuery) use($keywords_expld_city_zip) {
                                $subQuery = $subQuery->select('id')->from('countries')->orWhere('name', 'like', '%'.$keywords_expld_city_zip.'%');
                                return $subQuery;
                            })
                            ->orWhereIn('state_id',function($subQuery) use($keywords_expld_city_zip) {
                                $subQuery = $subQuery->select('id')->from('states')->orWhere('name', 'like', '%'.$keywords_expld_city_zip.'%');
                                return $subQuery;
                            })
                            ->orWhereIn('city_id',function($subQuery) use($keywords_expld_city_zip) {
                                $subQuery = $subQuery->select('id')->from('cities')->orWhere('name', 'like', '%'.$keywords_expld_city_zip.'%');
                                return $subQuery;
                            });
                        return $innerQuery;
                });
            }
        }
    }

	public function getProfileLink($self=false) 
	{
       	// add slug if not blank
        if($this->is_approved == 1) {
            return route('show-event', [$this->event_slug]);
        } else {
            if($self) {
                return route('show-event', [$this->event_slug]);
            }
            return route('event-not-found');
        }
	}

	public function events_images()
    {
		return VendorEventImage::where('event_id', $this->id);
    }
	
	// get profile image
    // Now this function will return image for user and both
    public function getProfilePic($self = false) 
    {
		$profile_pic = null;
		if($this->events_images()->where('image_type', 'profile')->exists()) {
			if($self == true) {
				$profile_pic =  $this->events_images()->where('image_type', 'profile')->first();
			} else {
                $profile_pic =  $this->events_images()->where('image_type', 'profile')->where('is_approved', 'Y')->first();
			}
		}
        
        if(!is_null($profile_pic)) {
            return config('constants.S3_WEDDING_IMAGES').$profile_pic->image_thumb;
        } else {
            return URL::asset('img/default-photo.jpg');
        }
    }

    public function getCoverPic($self = false) 
    {   
		$cover_pic = null;
		if($this->events_images()->where('image_type', 'cover')->exists()) {
			if($self == true) {
				$cover_pic =  $this->events_images()->where('image_type', 'cover')->first();
			} else {
				$cover_pic =  $this->events_images()->where('image_type', 'cover')->where('is_approved', 'Y')->first();
			}
		}
        
        if(!is_null($cover_pic)) {
            return config('constants.S3_WEDDING_IMAGES').$cover_pic->image_full;
        } else {
            return URL::asset('img/default-banner.jpg');
        }
    }

    public function getPosterPic($self = false) 
    {   
		$poster_pic = null;
		if($this->events_images()->where('image_type', 'poster')->exists()) {
			if($self == true) {
				$poster_pic =  $this->events_images()->where('image_type', 'poster')->first();
			} else {
				$poster_pic =  $this->events_images()->where('image_type', 'poster')->where('is_approved', 'Y')->first();
			}
		}
        
        if(!is_null($poster_pic)) {
            return config('constants.S3_WEDDING_IMAGES').$poster_pic->image_full;
        } else {
            return URL::asset('img/default-banner.jpg');
        }
    }

    public function getEditLink()
    {
        return route('edit-event', ['eventId' => $this->getEncryptedId()]);
    }

    // get similar events
    public function scopeGetSimilarResults($query, $category = '')
    {
        $query->hasActiveEventVendor()
		->where('is_approved', 1)
        ->where('site_id', $this->site_id)
        ->where('start_date', '>=', date("Y-m-d"));
        return $query->getEventFromCat($category)
                ->orderBy('newCountryid','asc');
    }

    public function scopeGetEventFromCat($query, $categories = [])
    {
        if(!empty($categories)) {
            if(is_array($categories) && count($categories) > 0) {
                $query->where(function ($query) use($categories) {
                    foreach($categories as $category) {
                        if($category != "") {
                            $query = $query->orWhere('categories', 'like', '%;s:'.strlen($category).':"'.$category.'";%');
                        }
                    }
                    return $query;
                });
            }
        }
    }

    // get total review for vendor
    public function getTotalReview()
    {
        return EventReview::where(['event_id' => $this->id, 'approved' => '1'])
                    ->count();
    }

    // get average rating for vendor
    public function getAverageReviewRating()
    {
        return EventReview::where(['event_id' => $this->id, 'approved' => '1'])
                    ->avg('rating');
    }

    public function getSumReviewRating()
    {
        return EventReview::where(['event_id' => $this->id, 'approved' => '1'])
                    ->sum('rating');
    }

    // get total rating for vendor star specific
    public function getCountReviewStarSpecific($star)
    {
        return EventReview::where(['event_id' => $this->id, 'approved' => '1', 'rating' => $star])
                    ->count();
    }

    // get bar for rating as per the average
    public function getSpcificStartBarPer($br)
    {
        $bar = 0;
        if($this->getTotalReview() > 0) {
            $bar_percentage = ( $this->getCountReviewStarSpecific($br) * 100 /  $this->getTotalReview() );
        } else {
            $bar_percentage = 0;
        }
        if($bar_percentage == 0) {
            $bar = 0;
        } else if($bar_percentage > 0 && $bar_percentage <= 10) {
            $bar = 1;
        } else if($bar_percentage > 10 && $bar_percentage <= 20) {
            $bar = 2;
        } else if($bar_percentage > 20 && $bar_percentage <= 30) {
            $bar = 3;
        } else if($bar_percentage > 30 && $bar_percentage <= 40) {
            $bar = 4;
        } else if($bar_percentage > 40 && $bar_percentage <= 50) {
            $bar = 5;
        } else if($bar_percentage > 50 && $bar_percentage <= 60) {
            $bar = 6;
        } else if($bar_percentage > 60 && $bar_percentage <= 70) {
            $bar = 7;
        } else if($bar_percentage > 70 && $bar_percentage <= 80) {
            $bar = 8;
        } else if($bar_percentage > 80 && $bar_percentage <= 90) {
            $bar = 9;
        } else if($bar_percentage == 100) {
            $bar = 10;
        }
        return $bar;
    }
    
    public function scopeOnlySite($query) 
    {
        $site_id = config("constants.admin_site_id");
        return $query->where('site_id',$site_id);
    }

    public function getArtistInfoBy($type = 'id', $value)
    {
        return Artist::where($type, $value)->first(['id', 'name']);
    }

    public function getEventCategoryDetail($slug = '')
    {
        if($slug != '') {
            // get category details
            return VendorCategory::where('slug', $category)
            ->where('vendor_type', 2)->first();
        } else {
            $categories = unserialize($this->categories);
            if(count($categories) > 0) {
                $category = $categories[0];
            }
            // get category details
            return VendorCategory::where('id', $category)->first();
        }
    }

    public function getCategoryName($id)
    {
        if(VendorCategory::where('id', '=', $id)->exists()) {
            return VendorCategory::where('id', '=', $id)->first(['name']);
        } else {
            return '';
        }
    }

    /**
     * Handle bulk action of list
     */
    public function handleBulkAction(Request $request)
    {
        $action = $request->has('action') ? $request->input('action') : "";
        $ids    = $request->has('ids') ? $request->input('ids') : "";
        if( $action != "" && ($action == "delete" || $action == "1" || $action == "0") && !empty($ids) ) {
            $records = $this->VendorFaqRepository->getRecords($ids);
            if($action == "1" || $action == "0") {
                try {
                    foreach($records as $record) {
                        $record->status = $action;
                        $record->save();
                    }
                    return redirect()->back()->withSuccess('Records(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating Records(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($records as $record) {
                        $this->VendorFaqRepository->deleteRecord($record);
                    }
                    return redirect()->back()->withSuccess('Record(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting Record(s), please try again.');
                }
            }
        } else if($action == "sort") {
            $orders = $request->has('orders') ? $request->input('orders') : "";
            if(!empty($orders)) {
                try {
                    foreach($orders as $record_id => $order) {
                        if(is_numeric($order)) {
                            $record = VendorFaq::find($record_id);
                            $record->order = $order;
                            $record->save();
                        }
                    }
                    return redirect()->back()->withSuccess('Orders updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating orders, please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    // scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('event_name', 'like', '%'.$search.'%')
                        ->orWhere('event_slug', 'like', '%'.$search.'%')
                        ->orWhere('address', 'like', '%'.$search.'%')
                        ->orWhere('description', 'like', '%'.$search.'%')
                        ->orWhere('ticket_information', 'like', '%'.$search.'%')
                        ->orWhere('meta_keywords', 'like', '%'.$search.'%');
        });
    }

    /**
     * get vendors which are having banner images
     */
    public function scopeVendorsWithBanners($query) 
    {
        return $query->whereHas('event_images',
            function($innerQuery) {
                $innerQuery->select('event_id')->from('vendors_events_images')->where('image_type', '=', 'cover')->where('image_thumb', '<>', '')->where('is_approved', 'Y');
                    return $innerQuery;
            });
    }
}
