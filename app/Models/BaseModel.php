<?php
namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class BaseModel extends Eloquent{

    public function getWhereCount($where = array()){
        return $this::where($where)->count();
    }
    public function getWhereOne($where = array()){
        return $this::where($where)->first();
    }

    public function getWhere($where = array()){
        return $this::where($where)->orderBy('id','DESC')->get();
    }

    public function whereUpdate($where = array(), $values = array()){
        $this::where($where)->update($values);
    }

}