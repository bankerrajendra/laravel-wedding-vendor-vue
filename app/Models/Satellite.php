<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 28 Jun 2019 18:06:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class Satellite extends BaseModel
{
    protected $table = 'satellites';

    protected $fillable = [
        'title',
        'url',
        'logo',
        'favicon'
    ];
    
    // scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('title', 'like', '%'.$search.'%');
        });
    }
}
