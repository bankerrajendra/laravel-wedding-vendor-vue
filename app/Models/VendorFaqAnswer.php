<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29th Jul 2019 18:36:45 +0000.
 */

namespace App\Models;

class VendorFaqAnswer extends BaseModel
{
    protected $table = 'vendors_faqs_answers';

    protected $fillable = [
        'answer'
    ];

    public function vendor_faq()
    {
        return $this->belongsTo('App\Models\VendorFaq');
    }

    // scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('answer', 'like', '%'.$search.'%');
        });
    }
}
