<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class EmailSubject extends BaseModel
{
    protected $table = 'email_template_subject';
    public $timestamps = false;
    protected $fillable = [
        'template_id',
        'subject'
    ];
    
    public function email_template()
    {
        return $this->belongsTo('App\Models\NewsLetterEmailSmsTemplate', 'template_id');
    }
    
}
