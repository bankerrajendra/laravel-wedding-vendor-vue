<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersInterest
 * 
 * @property int $id
 * @property int $user_id
 * @property int $interest_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Interest $interest
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsersInterest extends BaseModel
{
	protected $casts = [
		'user_id' => 'int',
		'interest_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'interest_id'
	];

	public function interest()
	{
		return $this->belongsTo(\App\Models\Interest::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
