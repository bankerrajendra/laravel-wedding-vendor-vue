<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 09 Nov 2018 23:06:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Blog
 * 
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $blog_image
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Blog extends Eloquent
{
	protected $casts = [
		'status' => 'bool'
	];

	protected $fillable = [
		'title',
		'description',
		'blog_image',
		'slug',
		'meta_title',
		'meta_description',
		'meta_keyword',
		'status'
	];
    public function blogcomments()
    {
        return $this->hasMany( BlogComment::class);
    }
}
