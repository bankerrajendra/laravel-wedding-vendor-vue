<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 24th Jul 2019 14:29:45 +0000.
 */

namespace App\Models;

class VendorFaq extends BaseModel
{
    protected $table = 'vendors_faqs';

    protected $fillable = [
        'question',
        'categories',
        'answer_type',
        'order',
        'status'
    ];

    public function vendor_faq_answer_options()
    {
        return $this->hasOne('App\Models\VendorFaqAnswerOption', 'faq_id');
    }

    public function vendor_faq_answer_placeholder()
    {
        return $this->hasOne('App\Models\VendorFaqAnswerPlaceholder', 'faq_id');
    }

    public function vendor_faq_answer()
    {
        return $this->hasMany('App\Models\VendorFaqAnswer', 'faq_id');
    }

    public function getCategoryName($id)
    {
        if(VendorCategory::where('id', '=', $id)->exists()) {
            return VendorCategory::where('id', '=', $id)->first(['name']);
        } else {
            return '';
        }
    }

    // scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('question', 'like', '%'.$search.'%');
        });
    }
}
