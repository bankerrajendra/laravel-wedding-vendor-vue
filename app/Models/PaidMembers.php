<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;

class PaidMembers extends BaseModel
{
    protected $table = 'paid_members';
    public $timestamps = true;
    protected $fillable = [
        'plan_id',
        'user_id',
        'plan_name',
        'plan_amount',
        'currency',
        'duration',
        'duration_type',
        'membership_start_date',
        'membership_end_date',
        'payment_gateway_name',
        'site_id'
    ];

    public function users()
    {
        return $this->belogsTo(\App\Models\User::class);
    }

    public function order_info()
    {
        return $this->hasOne('App\Models\PaidMembersOrdersInfo', 'paid_membership_id');
    }

    public function scopeGetMembershipPayments($query, $user_id = "")
    {
        if($user_id != "") {
            $uid = $user_id;
        } else {
            $uid = Auth::id();
        }
        return $query->where('user_id', $uid);
    }

}
