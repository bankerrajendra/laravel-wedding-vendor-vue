<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Education
 * 
 * @property int $id
 * @property string $education
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users_informations
 *
 * @package App\Models
 */
class Education extends BaseModel
{
	protected $table = 'educations';

	protected $fillable = [
		'education'
	];

	public function users_informations()
	{
		return $this->hasMany(\App\Models\UsersInformation::class);
	}
}