<?php

/**
 * Created by Rajendra Banker.
 * Date: Thu, 29th Aug 2019 19:14:45 +0000.
 */

namespace App\Models;

class UserPriceRequest extends BaseModel
{
    protected $table = 'users_price_requests';

    protected $fillable = [
        'vendor_id',
        'user_id',
        'first_name',
        'last_name',
        'mobile_number',
        'event_date',
        'preferred_contact_method',
        'about_extra_needs',
        'no_of_guest',
        'wedding_type'
    ];
}
