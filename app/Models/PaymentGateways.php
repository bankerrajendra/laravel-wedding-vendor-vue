<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class PaymentGateways extends BaseModel
{
    protected $table = 'manage_payment_gateways';
    public $timestamps = true;
    protected $fillable = [
        'name',
        'status',
        'settings',
        'site_id'
    ];
}
