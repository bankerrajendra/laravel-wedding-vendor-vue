<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 09 Nov 2018 23:06:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Events\MessageEvent;

class Message extends BaseModel
{
    protected $table = 'messages';
    public $timestamps = true;

    protected $fillable = [
        'sender_id',
        'receiver_id',
        'message',
        'sender_archive',
        'msg_type'
    ];

    public static  function sendMessageByUserId($receiverId, $message, $userId='',$msgType='0')
    {
        //
        if ($userId == '')
        {
            if(Auth::user()) {
                $userId = Auth::id();
            } else {
                $userId = Auth::guard('api')->id();
            }
        }

        $user = User::find(Auth::id());

        $messageObj = Message::create([
            'sender_id'       => $userId,
            'receiver_id'     => $receiverId,
            'sender_archive'  => 'N',
            'message'         => $message,
            'created_at'    => Carbon::now(),
            'msg_type'  => $msgType,
        ]);
        // broadcast event for chat module
        broadcast(new MessageEvent($messageObj, $user));
    }

    public static  function hasConversation($sender_id) {
        if (empty($sender_id)) {
            return false;
        }
        $user_id=Auth::id();
        if($sender_id!='') {
            $fetchconversation=Message::
            //where('receiver_trash','N')
            //->where('receiver_delete','N')
            where(function($query) use ($sender_id,$user_id){
                return $query
                    ->where(['sender_id'=>$sender_id, 'receiver_id'=>$user_id, 'receiver_trash'=>'N', 'receiver_delete'=>'N'])   //'receiver_archive'=>'N',
                    ->orWhere(function($query1) use ($sender_id,$user_id){
                        return $query1
                            ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id, 'sender_trash'=>'N', 'sender_delete'=>'N']);  //'sender_archive'=>'N',
                    });
            })
                ->count();
        }

        if($fetchconversation>0) {
           // echo $fetchconversation;
           // echo "ss";
            return 1;
        } else {

            return 0;
        }
    }
}