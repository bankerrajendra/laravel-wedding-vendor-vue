<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Talk;
use App\Models\FavouritedUser;
use App\Notifications\MailResetPasswordNotification;
/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $gender
 * @property \Carbon\Carbon $date_of_birth
 * @property int $country_id
 * @property int $city_id
 * @property string $remember_token
 * @property bool $activated
 * @property string $token
 * @property string $signup_ip_address
 * @property string $signup_confirmation_ip_address
 * @property string $signup_sm_ip_address
 * @property string $admin_ip_address
 * @property string $updated_ip_address
 * @property string $deleted_ip_address
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\City $city
 * @property \App\Models\Country $country
 * @property \Illuminate\Database\Eloquent\Collection $activations
 * @property \Illuminate\Database\Eloquent\Collection $laravel2steps
 * @property \Illuminate\Database\Eloquent\Collection $permissions
 * @property \Illuminate\Database\Eloquent\Collection $profiles
 * @property \Illuminate\Database\Eloquent\Collection $roles
 * @property \Illuminate\Database\Eloquent\Collection $social_logins
 * @property \Illuminate\Database\Eloquent\Collection $users_images
 * @property \Illuminate\Database\Eloquent\Collection $users_informations
 * @property \Illuminate\Database\Eloquent\Collection $interests
 * @property \Illuminate\Database\Eloquent\Collection $users_preferences
 * @property \Illuminate\Database\Eloquent\Collection $users_show_hides
 * @property \Illuminate\Database\Eloquent\Collection $favourited_users
 *
 * @package App\Models
 */
class User extends Authenticatable
{
	use SoftDeletes;
    use HasRoleAndPermission;
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

	protected $casts = [
		'country_id' => 'int',
		'city_id' => 'int',
        'state_id' => 'int',
		'activated' => 'bool'
	];

	protected $dates = [
		'date_of_birth'
	];

	protected $hidden = [
		'password',
		'remember_token',
		'token'
	];

	protected $fillable = [
		'name',
		'first_name',
		'last_name',
		'email',
		'password',
		'gender',
		'date_of_birth',
		'country_id',
		'city_id',
		'remember_token',
		'activated',
		'token',
		'signup_ip_address',
		'signup_confirmation_ip_address',
		'signup_sm_ip_address',
		'admin_ip_address',
		'updated_ip_address',
		'deleted_ip_address',
		'site_id',
		'state_id'
	];

    /**
     * Build Social Relationships.
     *
     * @var array
     */
    public function social()
    {
        return $this->hasMany('App\Models\Social');
    }

    /**
     * User Profile Relationships.
     *
     * @var array
     */
    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }


    public function city()
	{
		return $this->belongsTo(\App\Models\City::class);
	}

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}

    public function state()
    {
        return $this->belongsTo(\App\Models\State::class);
    }


    public function activations()
	{
		return $this->hasMany(\App\Models\Activation::class);
	}

	public function laravel2steps()
	{
		return $this->hasMany(\App\Models\Laravel2step::class, 'userId');
	}

	public function permissions()
	{
		return $this->belongsToMany(\App\Models\Permission::class)
					->withPivot('id')
					->withTimestamps();
	}

	public function profiles()
	{
        return $this->belongsToMany('App\Models\Profile')->withTimestamps();
	}

	public function roles()
	{
		return $this->belongsToMany(\App\Models\Role::class)
					->withPivot('id')
					->withTimestamps();
	}

	public function social_logins()
	{
		return $this->hasMany(\App\Models\Social::class)->first();
	}

	public function users_images()
	{
		return $this->hasMany(\App\Models\UsersImage::class);
	}

	public function users_information()
	{
		return $this->hasOne(\App\Models\UsersInformation::class);
	}

	public function get_user_information(){
        return $this->users_information()->first();
    }

	public function assignUserInformation($user_information){
        return $this->users_information()->save($user_information);
    }

	public function interests()
	{
		return $this->belongsToMany(\App\Models\Interest::class, 'users_interests')
					->withPivot('id')
					->withTimestamps();
	}

    public function get_interests()
    {
        return $this->belongsToMany(\App\Models\Interest::class, 'users_interests')
            ->withPivot('id')
            ->withTimestamps()->get();
    }

	public function users_preferences()
	{
		return $this->hasOne(\App\Models\UsersPreference::class)->first();
	}

	public function users_show_hides()
	{
		return $this->hasMany(\App\Models\UsersShowHide::class);
	}



	public function favourited_users(){
        //return $this->hasMany(\App\Models\FavouritedUser::class, 'from_user');
        $user = $this;
        return FavouritedUser::where('from_user', $this->id)->orWhere(function($query) use ($user){
            $query->where('to_user', $user->id)->where('to_user_status', 1);
        });
    }

    public function favourited_by_users(){
        //return $this->hasMany(\App\Models\FavouritedUser::class, 'to_user');
        return FavouritedUser::where('to_user', $this->id)->where('to_user_status', 0);
    }

    /// below functions are for share photo, request to private photo, request to add photo. we can modify the below functions as per requirements.
    public function photonotify_users() {
        return PhotoNotification::where('from_user', $this->id)->where('notification_type', 'share_photo')->get();
    }

    public function photonotify_by_users() {
        return PhotoNotification::where('to_user', $this->id)->where('notification_type', 'private_photo_request')->get();
    }

    public function hasPrivatePhotos(){
        if($this->users_images()->where('image_type', 'private')->where('is_approved', 'Y')->count() > 0){
            return true;
        }
        return false;
    }

    public function hasSharedPhotos($user_id){
        if(PhotoNotification::where('from_user', $this->id)->where('to_user', $user_id)->where('notification_type', 'share_photo')->count() > 0){
            return true;
        }
        return false;
    }

    public function hasAlreadySentPrivatePhotoRequest($user_id){
        return PhotoNotification::where('from_user', $this->id)->where('to_user', $user_id)->where('notification_type', 'private_photo_request')->get();
    }

    public function hasAlreadySentRequest(){
        $authId=Auth::id();
        return PhotoNotification::where('from_user', $authId)->where('to_user', $this->id)->where('notification_type', 'profile_photo_request')->where('status', 'P')->count();
    }
    /// End:  functions are for share photo, request to private photo, request to add photo. we need to modify the below functions as per requirements.

    public function skipped_users(){
        return $this->hasMany(\App\Models\SkippedUser::class, 'from_user');
    }

    public function skipped_by_users(){
        return $this->hasMany(\App\Models\SkippedUser::class, 'to_user');
    }

    public function shortlisted_users(){
        return $this->hasMany(\App\Models\ShortlistedUser::class, 'from_user');
    }

    public function shortlisted_by_users(){
        return $this->hasMany(\App\Models\ShortlistedUser::class, 'to_user');
    }

    public function viewed_users(){
        return $this->hasMany(\App\Models\ViewedUser::class, 'from_user');
    }

    public function viewed_by_users(){
        return $this->hasMany(\App\Models\ViewedUser::class, 'to_user');
    }

    public function blocked_users(){
        return $this->hasMany(\App\Models\BlockedUser::class, 'from_user');
    }

    public function blocked_by_users(){
        return $this->hasMany(\App\Models\BlockedUser::class, 'to_user');
    }

    public function hasProfile($name)
    {
        foreach ($this->profiles as $profile) {
            if ($profile->name == $name) {
                return true;
            }
        }
        return false;
    }
    public function assignProfile($profile)
    {
        return $this->profiles()->attach($profile);
    }
    public function removeProfile($profile)
    {
        return $this->profiles()->detach($profile);
    }

    public function getAge(){
        $today = new DateTime();
        $birthdate = new DateTime($this->date_of_birth);
        $interval = $today->diff($birthdate);
        return $interval->format('%y');
    }

    public function getProfilePic($viewprofile=''){
        $authId=Auth::id();
        $curId=$this->id;
        if(Auth::user()){
            $currentUser = Auth::user();
            if(!$currentUser->isAdmin()){
                if($this->banned == 1 || $this->deactivated == 1){
                    return URL::asset('img/blocked.png');
                }
            }
        }

        if(user_blocked_to_me($this->id)>0) {
            return URL::asset('img/blocked.png');
        }

        $profile_pic =  $this->users_images()->where('is_profile_image', 'Y')->first();
        //print_r($profile_pic);
        if(is_null($profile_pic)){
            $profile_pic =  $this->users_images()->where('image_type', 'public')->where('is_approved', 'Y')->first();
            if(!is_null($profile_pic)){
                return config('constants.upload_url_public').config('constants.thumb_upload_path_rel').$profile_pic->image_thumb;
            }
            else{
                $profile_pic =  $this->users_images()->where('image_type', 'private')->where('is_approved', 'Y')->first();
                if(!is_null($profile_pic)) { // && $viewprofile=='profile-image'){

                    if($authId == $curId) {
                        return config('constants.upload_url_public').config('constants.thumb_upload_path_rel').$profile_pic->image_thumb;
                    } else {
                        $privatePhotoPermissionCount = PhotoNotification::where(function ($innerQuery) use ($authId, $curId) {
                            return $innerQuery->where('from_user', $authId)->where('to_user', $curId)->where('notification_type', 'private_photo_request')->where('status', 'A');
                        })->orWhere(function ($innerQuery) use ($authId, $curId) {
                            return $innerQuery->where('from_user', $curId)->where('to_user', $authId)->where('notification_type', 'share_photo');
                        })->count();

                        if ($privatePhotoPermissionCount > 0) {
                            if($viewprofile=='profile-image') {
                                return config('constants.upload_url_public') . config('constants.thumb_upload_path_rel') . $profile_pic->image_thumb;
                            } else {
                                return URL::asset('img/unlock_images.jpg');
                            }
                        } else {
                            return URL::asset('img/lock_images.jpg');
                        }
                    }
                    // end share_photo
                } else {
                    if ($this->gender == 'M') {
                        return URL::asset('img/male-default.png');
                    } else {
                        return URL::asset('img/female-default.png');
                    }
                }
            }
        }
        else{
            return config('constants.upload_url_public').config('constants.thumb_upload_path_rel').$profile_pic->image_thumb;
        }
    }

    public function isSocialLoggedIn(){
        if($this->social_logins() && !$this->get_user_information()){
            return true;
        }
        else{
            return false;
        }
    }

    public function hasCompletedProfile(){
        $userInformation = $this->get_user_information();
        if (($userInformation->drink == null || $userInformation->smoke == null || $userInformation->education_id == null || $userInformation->about == null )){
            return false;
        }
        else{
            return true;
        }
    }

    public function getEncryptedId(){
        return base64_encode($this->id);
    }

    public function getDecryptedId(){
        return base64_decode($this->id);
    }

    public function isOnline(){
        return Cache::has('user-is-online-'.$this->id);
    }

    public function profileLink(){
        if($this->banned == 1 || $this->deactivated == 1){
            //return "javascript:;";
            return route('profile-not-found');
        }
        else{
            return route('user-profile', ['username'=>$this->getEncryptedId()]);
        }
    }

    public function popupProfileLink(){
        return route('user-profile-popup', ['username'=>$this->name]);
    }

    public function isSkipped(){
        $currentUser = Auth::user();
        if($currentUser->skipped_users()->where('to_user', '=', $this->id)->count() > 0){
            return true;
        }
        return false;
    }

    public function isLiked(){
        $currentUser = Auth::user();
        if(FavouritedUser::where('from_user', $currentUser->id)->where('to_user', $this->id)->count() > 0){
            return true;
        }
//        if($currentUser->favourited_users()->where('to_user', '=', $this->id)->count() > 0){
//            return true;
//        }
        return false;
    }

    public function isShortlisted(){
        $currentUser = Auth::user();
        if($currentUser->shortlisted_users()->where('to_user', '=', $this->id)->count() > 0){
            return true;
        }
        return false;
    }


    public function isLikedMe(){
        $currentUser = Auth::user();
        if(FavouritedUser::where('from_user', $this->id)->where('to_user', $currentUser->id)->count() > 0){
            return true;
        }
//        if($this->favourited_users()->where('to_user', '=', $currentUser->id)->count() > 0){
//            return true;
//        }
        return false;
    }

    public function isLikedBack(){
        $currentUser = Auth::user();
        if(FavouritedUser::where('from_user', $this->id)->where('to_user', $currentUser->id)->where('to_user_status', '=', 1)->count() > 0){
            return true;
        }
//        if($this->favourited_users()->where('to_user', '=', $currentUser->id)->where('to_user_status', '=', 1)->count() > 0){
//            return true;
//        }
        return false;
    }

    public function getLikeText(){
        if($this->isLikedMe()){
            return "Like Back";
        }
        else{
            return "Like";
        }
    }

    public function isBlocked(){
        $currentUser = Auth::user();
        if($currentUser->blocked_users()->where('to_user', '=', $this->id)->count() > 0){
            return true;
        }
        return false;
    }

    public function isBlockedMe(){
        $currentUser = Auth::user();
        if($this->blocked_users()->where('to_user', '=', $currentUser->id)->count() > 0){
            return true;
        }
        return false;
    }

    public function hasConversation(){
        //return Talk::isConversationExists($this->id);
        $conversation = Talk::getConversationsByUserId($this->id);
        if($conversation){
            if($conversation->messages->count() > 0)
                return true;
            else
                return false;
        }
        else{
            return false;
        }
    }

    public function scopeSkipConditions($query){
        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            ->fromSameCountry()
            ->filterPartnerPreferences()
            ->skipLikedUsers()
            ->skipShortlistedUsers()
            ->skipSkippedUsers()
            ->skipWhoLikedMe()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
            ->where('banned',0)
            ->where('deactivated',0);
    }

    public function scopeNewUserConditions($query){
        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            ->fromSameCountry()
            //->filterPartnerPreferences()
        //    ->skipLikedUsers()
        //    ->skipShortlistedUsers()
        //    ->skipSkippedUsers()
        //    ->skipWhoLikedMe()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
			//->skipViewedMe()
            ->isActive();
    }



    public function scopeNewUserProfile($query){
        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            ->fromSameCountry()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
            ->sameAgeLimit()
            ->isActive();
    }


    public function scopeMatchUserProfile($query){
        return $query->hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            ->fromSameCountry()
            ->skipWhoBlockedByMe()
            ->skipWhoBlockedMe()
            ->sameAgeLimit()
            ->sameLifestyleHobbies()
            ->isActive();
        //newuserprofile + lifestyle & hobbies
    }


    public function scopeSameAgeLimit($query){
        $currentUser = Auth::user();
        $myage=$currentUser->getAge();

//        return $query->where(DB::raw("(((ROUND((DATEDIFF(CURDATE(),date_of_birth) / 365.25)) >=".($myage-8)." and ROUND((DATEDIFF(CURDATE(),date_of_birth) / 365.25)) <=".($myage+8)." and ROUND((DATEDIFF(CURDATE(),date_of_birth) / 365.25))>0)) )"));
        return $query->whereRaw('ROUND(DATEDIFF(CURDATE(),date_of_birth) / 365.25) <='.($myage+8))
            ->whereRaw('ROUND(DATEDIFF(CURDATE(),date_of_birth) / 365.25)>='.($myage-8))
            ->whereRaw('ROUND(DATEDIFF(CURDATE(),date_of_birth) / 365.25)>0');
    }

    public function scopeIsActive($query){
        return $query->where('banned',0)
            ->where('activated',1)
            ->where('access_grant','Y')
            ->where('deactivated',0);
    }

    public function scopeFromCities($query, $cities){
        if(count($cities) > 0){
            return $query->whereIn('city_id', $cities);
        }
        else{
            return $query;
        }
    }

    public function scopeFilterAge($query, $age){
       return $query->whereDate('date_of_birth', '<=', Carbon::today()->subYears($age[0])->toDateString())
            ->whereDate('date_of_birth', '>=', Carbon::today()->subYears($age[1])->toDateString());
    }

    public function scopeFilterPartnerPreferences($query){
        $currentUser = Auth::user();
        if($currentUser->users_preferences()){
            $pref = $currentUser->users_preferences();
            return $query->where('city_id', $pref->city_id)
                ->whereDate('date_of_birth', '<=', Carbon::today()->subYears($pref->age_from)->toDateString())
                ->whereDate('date_of_birth', '>=', Carbon::today()->subYears($pref->age_to)->toDateString());
//                ->whereDate('date_of_birth', '<=', 'date_sub(curdate(), interval '.$pref->age_from.' year)')
//                ->whereDate('date_of_birth', '>=', 'date_sub(curdate(), interval '.$pref->age_to.' year)');

        }
        else{
            return $query;
        }
    }

    public function scopeFromSameCountry($query){
        $currentUser = Auth::user();
        return $query->where('country_id', $currentUser->country_id);
    }

    public function scopeWithEncryptedId($query, $encryptedId){
        $id = base64_decode($encryptedId);
        return $query->where('id', '=', $id);
    }

    public function scopeIsCompletedProfile($query){
        return $query->whereHas('users_information', function($innerQuery){
                $innerQuery->whereNotNull('drink')
                ->whereNotNull('smoke')
                ->whereNotNull('food')
                ->whereNotNull('education_id')
             //   ->whereNotNull('profession_id')
                ->whereNotNull('about');
        });
    }

    public function scopeIsIncompletedProfile($query){
        return $query->whereHas('users_information', function($innerQuery){
            $innerQuery->WhereNull('drink')
                ->orWhereNull('smoke')
                ->orWhereNull('food')
                ->orWhereNull('education_id')
                ->orWhereNull('about');
        });
    }

    public function scopeSameLifestyleHobbies($query){
        $currentUser = Auth::user();
        $currentUserInfo = $currentUser->get_user_information();

        return $query->whereHas('users_information',
            function($innerQuery) use($currentUserInfo) {
                $innerQuery->where(
                    function($innerQuery1) use($currentUserInfo){
                        return $innerQuery1->orWhere('drink',$currentUserInfo->drink)
                            ->orWhere('smoke',$currentUserInfo->smoke)
                            ->orWhere('food',$currentUserInfo->food);
                    })
                    ->where(
                        function($innerQuery2) use($currentUserInfo) {
                            if(!is_null($currentUserInfo->hobbies)){
                                foreach(explode(",",$currentUserInfo->hobbies) as $hobby) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$hobby."',hobbies)");
                                }
                            }
                            if(!is_null($currentUserInfo->interests)){
                                foreach(explode(",",$currentUserInfo->interests) as $interest) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$interest."',interests)");
                                }
                            }
                            if(!is_null($currentUserInfo->preferred_movies)){
                                foreach(explode(",",$currentUserInfo->preferred_movies) as $preferred_movie) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$preferred_movie."',preferred_movies)");
                                }
                            }
                            if(!is_null($currentUserInfo->sports)){
                                foreach(explode(",",$currentUserInfo->sports) as $sport) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$sport."',sports)");
                                }
                            }
                            if(!is_null($currentUserInfo->favourite_music)){
                                foreach(explode(",",$currentUserInfo->favourite_music) as $fav_music) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$fav_music."',favourite_music)");
                                }
                            }
                            if(!is_null($currentUserInfo->favourite_reads)){
                                foreach(explode(",",$currentUserInfo->favourite_reads) as $favourite_read) {
                                    $innerQuery2=$innerQuery2->orWhereRaw("find_in_set('".$favourite_read."',favourite_reads)");
                                }
                            }
                            return $innerQuery2;
                        }
                    );
        });
    }

    public function scopeHasUserRole($query){
        return $query->whereHas('roles', function ($innerQuery) {
                $innerQuery->where('roles.slug', '=', 'user');
                if(config('constants.unverified_account_allowed')){
                    $innerQuery->orWhere('roles.slug', '=', 'unverified');
                }
        });
    }

    public function scopeHasOppositeGender($query){
        $currentUser = Auth::user();
        return $query->where('gender', '!=', $currentUser->gender);
    }

    public function scopeSkipLikedUsers($query){

        return $query->whereNotIn('id', function($subQuery){
            $user = Auth::user();
            $subQuery->select('to_user')->from('favourited_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipShortlistedUsers($query){

        return $query->whereNotIn('id', function($subQuery){
            $user = Auth::user();
            $subQuery->select('to_user')->from('shortlisted_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipSkippedUsers($query){

        return $query->whereNotIn('id', function($subQuery){
            $user = Auth::user();
            $subQuery->select('to_user')->from('skipped_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipWhoLikedMe($query){
        return $query->whereNotIn('id', function($subQuery){
            $user = Auth::user();
            $subQuery->select('from_user')->from('favourited_users')->where('to_user','=', $user->id);
        });
    }

//    public function scopeGetWithUsername($query, $username){
//        return $query->where('name', '=', $username);
//    }

    public function scopeGetWithProfilecode($query, $username){
        return $query->where('id', '=', base64_decode($username));
    }

    public function scopeSkipWhoBlockedByMe($query){
        return $query->whereNotIn('id', function($subQuery){
            $user = Auth::user();
            $subQuery->select('to_user')->from('blocked_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipWhoBlockedMe($query){
        return $query->whereNotIn('id', function($subQuery){
            $user = Auth::user();
            $subQuery->select('from_user')->from('blocked_users')->where('to_user','=', $user->id);
        });
    }

    public function scopeSkipViewedMe ($query) {
        return $query->whereNotIn('id', function ($subQuery){
           $user = Auth::user();
           $subQuery->select('to_user')->from('viewed_users')->where('from_user','=', $user->id);
        });
    }

    public function getUserTimeZone()
    {
       if (\Session::has("usertimezone")) {
            return Session::get("usertimezone");
        } else {
           Session::put('usertimezone',getTimeZoneFromIpAddress_api());
           //return getTimeZoneFromIpAddress_api();
            //calculate and save role in session
        }
    }

}