<?php

/**
 * Created by Rajendra Banker.
 * Date: Fri, 9th Aug 2019 18:36:45 +0000.
 */

namespace App\Models;

class VendorShowcase extends BaseModel
{
    protected $table = 'vendors_showcase';

    protected $fillable = [
        'vendor_id',
        'satellites',
        'additional_categories'
    ];
}
