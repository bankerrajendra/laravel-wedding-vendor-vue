<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends BaseModel
{
    public $timestamps = false;
    //
    protected $table = 'languages';

    protected $fillable = [
        'id',
        'old_id',
        'language',
        'status',
    ];


}
