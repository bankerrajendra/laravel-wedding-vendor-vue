<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29th Jul 2019 11:29:45 +0000.
 */

namespace App\Models;

class VendorFaqAnswerPlaceholder extends BaseModel
{
    protected $table = 'vendors_faqs_answers_placeholder';

    public $timestamps  = false;

    protected $fillable = [
        'faq_id',
        'placeholder'
    ];

    public function vendor_faq()
	{
		return $this->belongsTo(\App\Models\VendorFaq::class);
	}
}
