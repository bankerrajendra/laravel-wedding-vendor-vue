<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 2 Jul 2019 11:12:45 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class VendorCategory extends BaseModel
{
    protected $table = 'vendors_categories';

    protected $fillable = [
        'name',
        'slug',
        'display_in_footer',
        'vendor_type',
        'status',
        'satellites'
    ];

    /**
     * The "booting" method of the model. to add global scope
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        // get site id from config
//        $site_id = config('constants.site_id');
//        static::addGlobalScope('satellites', function (Builder $builder) use($site_id) {
//            $builder->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%');
//        });
    }

    public function get_vendor_type()
    {
        return VendorType::where('id', $this->vendor_type)->first();
    }

    public function vendor_category_satellite_information()
    {
        return $this->hasMany('App\Models\VendorCategorySatelliteInformation');
    }

    public function getSatelliteName($id)
    {
        if(Satellite::where('id', '=', $id)->exists()) {
            return Satellite::where('id', '=', $id)->first(['title']);
        } else {
            return '';
        }
    }

    // scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('name', 'like', '%'.$search.'%');
        });
    }

    /**
     * Scope get type specific vendors
     */
    public function scopeGetVendorByType($query, $type = 'wedding')
    {
        $vendor_type_id = config('constants.vendor_type_id.'.$type);
        return $query->where('vendor_type', $vendor_type_id);
    }
}
