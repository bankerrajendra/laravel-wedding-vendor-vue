<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class NewsLetterEmailSmsTemplate extends BaseModel
{
    protected $table = 'newsletter_email_sms_templates';
    public $timestamps = true;
    protected $fillable = [
        'name',
        'description',
        'status',
        'type',
        'site_id'
    ];

    public function subject()
    {
        return $this->hasOne('App\Models\EmailSubject', 'template_id')->first(['subject']);
    }

    // scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('name', 'like', '%'.$search.'%')
                        ->orWhere('description', 'like', '%'.$search.'%');
        });
    }
    // Email records
    public function scopeGetEmailRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('name', 'like', '%'.$search.'%')
                        ->orWhere('description', 'like', '%'.$search.'%')
                        ->getEmailSubject($search);
        });
    }

    public function scopeGetEmailSubject($query, $search)
    {
        $query->orWhere(function ($query) use($search) {
            $query->whereIn('id', function($subQuery) use($search) {
                $subQuery = $subQuery->select('template_id')->from('email_template_subject')->orWhere('subject', 'like', '%'.$search.'%');
                return $subQuery;
            });
        });
    }
}
