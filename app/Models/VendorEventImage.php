<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Nov 2019 16:47:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class VendorEventImage
 * 
 * @property int $id
 * @property int $vendor_id
 * @property string $image_thumb
 * @property string $image_full
 * @property string $is_profile_image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */

class VendorEventImage extends BaseModel
{
	protected $casts = [
		'event_id' => 'int'
	];

	protected $table = 'vendors_events_images';

	protected $fillable = [
		'event_id',
		'image_thumb',
		'image_full',
		'is_profile_image',
        'image_type',
        'is_approved',
		'disapprove_reason',
		'is_new'
	];
}
