<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersPreference
 * 
 * @property int $id
 * @property int $user_id
 * @property int $age_from
 * @property int $age_to
 * @property int $country_id
 * @property int $city_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\City $city
 * @property \App\Models\Country $country
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsersPreference extends BaseModel
{
    protected $table = 'users_preferences';
	protected $casts = [
		'user_id' => 'int',
		'age_from' => 'int',
		'age_to' => 'int'
	];

	protected $fillable = [
		'user_id',
		'age_from',
		'age_to',
		'height_from',
		'height_to',
        'drink',
        'diet',
        'smoke',
        'religion',
        'education',
        'marital_status',
        'professional_area',
        'community',
        'languages',
        'country_living',
        'city_district',
        'state_living',
        'currency',
        'annual_income',
        'eye_color',
        'skin_tone'
    ];

	public function city()
	{
		return $this->belongsTo(\App\Models\City::class);
	}

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
