<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class MembershipPlanPackages extends BaseModel
{
	protected $table = 'membership_plan_packages';

	protected $casts = [
		'plan_id' => 'int',
		'duration' => 'int'
	];

	protected $fillable = [
		'plan_id',
		'amount',
		'duration',
		'type',
		'with_banner',
		'status'		
	];

	public function membership_plans()
	{
		return $this->belongsTo(\App\Models\MembershipPlans::class);
	}

	
}
