<?php
namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;


class MembershipPlans extends BaseModel
{
    //
    protected $table = 'membership_plans';

    protected $fillable = [
        'plan_name',
        'plan_type',
        'plan_amount',
        //'currency',
        'duration',
        'duration_type',
        'public_photos_allowed',
        //'private_photos_allowed',
        'msg_allowed',
        'membership_benefits',
        'site_id',
        'status'
    ];

    public function membership_plan_packages()
	{
		return $this->hasMany(\App\Models\MembershipPlanPackages::class, 'plan_id');
    }
    
    public function scopeGetPlanBenefits($query, $plan_type = "Free")
    {
        return MembershipPlans::where('plan_type', $plan_type)
                    ->where('status', 'A')
                    ->first(['membership_benefits']);
    }

    public function scopeGetPlanDetail($query, $plan_type = "Free")
    {
        return MembershipPlans::where('plan_type', $plan_type)
                    ->where('status', 'A')
                    ->first(['id', 'plan_name', 'plan_type', 'currency']);
    }

    public function scopeGetActivePacks($query, $id)
    {
        return MembershipPlanPackages::where('status', 'A')
                    ->where('plan_id', $id)
                    ->get(['id', 'plan_id', 'amount', 'duration', 'type', 'with_banner']);
    }

    // get admin packages for backend purpose
    public function scopeGetAdminPacks($query, $id)
    {
        return MembershipPlanPackages::where('status', 'ADMIN')
                    ->where('plan_id', $id)
                    ->get(['id', 'plan_id', 'amount', 'duration', 'type', 'with_banner']);
    }
}
