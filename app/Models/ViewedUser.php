<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 10 Sep 2018 22:35:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;

/**
 * Class ViewedUser
 * 
 * @property int $id
 * @property int $from_user
 * @property int $to_user
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class ViewedUser extends Eloquent
{
	protected $casts = [
		'from_user' => 'int',
		'to_user' => 'int'
	];

	protected $fillable = [
		'from_user',
		'to_user'
	];

	public function to_user()
	{
		return $this->belongsTo(\App\Models\User::class, 'to_user')->first();
	}

	public function from_user(){
        return $this->belongsTo(\App\Models\User::class, 'from_user')->first();
    }

    public function scopeSkipToConditions($query){
        $query->skipToWhoBlockedByMe()
            ->skipToWhoBlockedMe()
            ->skipDeletedUsers();
    }

    public function scopeSkipFromConditions($query, $user=null){
//        $query->skipFromWhoBlockedByMe($user)
//            ->skipFromWhoBlockedMe($user)
//            ->skipDeletedUsers();
        $query->skipDeletedUsers();
    }

    public function scopeSkipToWhoBlockedByMe($query){
        return $query->whereNotIn('to_user', function($subQuery){
            $user = Auth::user();
            $subQuery->select('to_user')->from('blocked_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipToWhoBlockedMe($query){
        return $query->whereNotIn('to_user', function($subQuery){
            $user = Auth::user();
            $subQuery->select('from_user')->from('blocked_users')->where('to_user','=', $user->id);
        });
    }

    public function scopeSkipFromWhoBlockedByMe($query, $user = null){
        if(!$user){
            $user = Auth::user();
        }
        return $query->whereNotIn('from_user', function($subQuery) use ($user){
            $subQuery->select('to_user')->from('blocked_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipFromWhoBlockedMe($query, $user=null){
        if(!$user){
            $user = Auth::user();
        }
        return $query->whereNotIn('from_user', function($subQuery) use ($user){
            //$user = Auth::user();
            $subQuery->select('from_user')->from('blocked_users')->where('to_user','=', $user->id);
        });
    }
    public function scopeSkipDeletedUsers($query){
        return $query->whereNotIn('from_user', function($subQuery){
            $subQuery->select('id')->from('users')->where('deleted_at','!=', null);
        });
        //return $query->join('users', 'viewed_users.from_user', '=', 'users.id')->where('users.deleted_at', '!=', null);
    }
}
