<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 22nd Aug 2019 18:36:45 +0000.
 */

namespace App\Models;

class VendorCustomFaq extends BaseModel
{
    protected $table = 'vendors_custom_faqs';

    protected $fillable = [
        'vendor_id',
        'category_id',
        'question',
        'answer',
        'is_approved',
        'disapprove_reason'
    ];
}
