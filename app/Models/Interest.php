<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Interest
 * 
 * @property int $id
 * @property string $interest
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Interest extends BaseModel
{
	protected $fillable = [
		'interest'
	];

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'users_interests')
					->withPivot('id')
					->withTimestamps();
	}
}
