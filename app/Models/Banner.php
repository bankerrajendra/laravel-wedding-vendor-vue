<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 09 Nov 2018 23:06:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class Banner extends BaseModel
{
    protected $table = 'banners';

    protected $fillable = [
        'title',
        'banner_type',
        'banner_link',
        'banner_image',
        'status',
        'created_at',
        'updated_at'
    ];
    //

    // scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('title', 'like', '%'.$search.'%');
        });
    }
}
