<?php

    /**
     * Created by Reliese Model.
     * Date: Tue, 21 Aug 2018 23:14:27 +0000.
     */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;

/**
 * Class UsersInformation
 *
 * @property int $id
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class MessageFilteredUser extends BaseModel
{
    protected $table = 'message_filtered_users';
    public $timestamps = true;

    protected $casts = [
        'user_id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'filtered_id',
    ];

    public static  function manageFilteredUsers($user_id, $filtered_id)
    {
        MessageFilteredUser::firstOrCreate([
            'user_id'       => $user_id,
            'filtered_id'     => $filtered_id
        ]);
    }
}

