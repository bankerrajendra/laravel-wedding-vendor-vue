<?php


/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
//use Illuminate\Database\Eloquent\Model;

class Community extends BaseModel
{
    //
    public $timestamps = false;
    protected $casts = [
        'religion_id' => 'int'
    ];

    protected $fillable = [
        'name',
        'religion_id'
    ];

    public function subcaste()
    {
        return $this->hasMany(\App\Models\subcaste::class);
    }

    public function users()
    {
        return $this->hasMany(\App\Models\User::class);
    }

    public function users_preferences()
    {
        return $this->hasMany(\App\Models\UsersPreference::class);
    }

    public function religion(){
        return $this->belongsTo(\App\Models\Religion::class);
    }
}
