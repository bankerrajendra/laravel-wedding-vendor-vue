<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 10 Sep 2018 19:33:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;

/**
 * Class FavouritedUser
 *
 * @property int $id
 * @property int $from_user
 * @property int $to_user
 * @property bool $to_user_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\User $user
 *
 * @package App\Models
 */

class PhotoNotification extends Eloquent
{
    //
    protected $casts = [
        'from_user' => 'int',
        'to_user' => 'int'
    ];

    protected $fillable = [
        'from_user',
        'to_user',
        'notification_type',
        'status'
    ];

    public function to_user()
    {
        return $this->belongsTo(\App\Models\User::class, 'to_user')->withTrashed()->first();
    }

    public function from_user()
    {
        return $this->belongsTo(\App\Models\User::class, 'from_user')->withTrashed()->first();
    }

}
