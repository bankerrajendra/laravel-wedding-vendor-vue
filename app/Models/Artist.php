<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 19 Dec 2019 17:24:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class Artist extends BaseModel
{
    protected $table = 'artists';

    protected $fillable = [
        'name',
        'vendor_id',
        'image_full',
        'is_new',
        'is_approved',
        'site_id',
        'created_at',
        'updated_at'
    ];

    public function vendor()
	{
		return $this->belongsTo(\App\Models\User::class, 'vendor_id');
	}

	// scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where('name', 'like', '%'.$search.'%');
    }

    /**
     * Get site specific records
     */
    public function scopeOnlySite($query) 
    {
        $site_id = config("constants.admin_site_id");
        return $query->where('site_id',$site_id);
    }

    /**
     * get artist image
     */
    public function getImage()
    {
        return config('constants.S3_WEDDING_IMAGES_ARTIST').$this->image_full;
    }
}
