<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use Reliese\Database\Eloquent\Model as Eloquent;


class Subcaste extends BaseModel
{
    //
    protected $fillable = [
        'name',
    ];

    public function users()
    {
        return $this->hasMany(\App\Models\User::class);
    }

    public function religion(){
        return $this->belongsTo(\App\Models\Religion::class);
    }

    public function users_preferences()
    {
        return $this->hasMany(\App\Models\UsersPreference::class);
    }
}
