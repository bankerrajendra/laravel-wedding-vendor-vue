<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Laravel2step
 * 
 * @property int $id
 * @property int $userId
 * @property string $authCode
 * @property int $authCount
 * @property bool $authStatus
 * @property \Carbon\Carbon $authDate
 * @property \Carbon\Carbon $requestDate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Laravel2step extends BaseModel
{
	protected $table = 'laravel2step';

	protected $casts = [
		'userId' => 'int',
		'authCount' => 'int',
		'authStatus' => 'bool'
	];

	protected $dates = [
		'authDate',
		'requestDate'
	];

	protected $fillable = [
		'userId',
		'authCode',
		'authCount',
		'authStatus',
		'authDate',
		'requestDate'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'userId');
	}
}
