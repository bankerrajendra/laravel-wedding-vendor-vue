<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 24th Jul 2019 14:29:45 +0000.
 */

namespace App\Models;

class VendorFaqAnswerOption extends BaseModel
{
    protected $table = 'vendors_faqs_answers_options';

    public $timestamps  = false;

    protected $fillable = [
        'faq_id',
        'options'
    ];

    public function vendor_faq()
	{
		return $this->belongsTo(\App\Models\VendorFaq::class);
	}
}
