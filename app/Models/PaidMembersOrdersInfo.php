<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class PaidMembersOrdersInfo extends BaseModel
{
    protected $table = 'paid_members_orders_info';
    public $timestamps = true;
    protected $fillable = [
        'paid_membership_id',
        'transaction_id',
        'transaction_type',
        'transaction_date_time',
        'card_user_country',
        'card_user_phone_country_code',
        'card_user_mobile_number',
        'card_holder_name',
        'card_billing_add_1',
        'card_billing_add_2',
        'card_billing_city',
        'card_billing_state',
        'card_billing_zip_code',
        'transaction_amount',
        'currency',
        'card_number',
        'card_type',
        'approval_code',
        'order_number',
        'customer_code'
    ];

    public function paid_member()
    {
        return $this->belongsTo('App\Models\PaidMembers', 'paid_membership_id');
    }
}
