<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersShowHide
 * 
 * @property int $id
 * @property int $user_id
 * @property string $attribute
 * @property string $hidden
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsersShowHide extends BaseModel
{
	protected $table = 'users_show_hide';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'attribute',
		'hidden'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
