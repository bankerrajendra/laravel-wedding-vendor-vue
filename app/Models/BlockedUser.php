<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 11 Sep 2018 21:30:12 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BlockedUser
 * 
 * @property int $id
 * @property int $from_user
 * @property int $to_user
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class BlockedUser extends Eloquent
{
	protected $casts = [
		'from_user' => 'int',
		'to_user' => 'int'
	];

	protected $fillable = [
		'from_user',
		'to_user'
	];

	public function to_user()
	{
		return $this->belongsTo(\App\Models\User::class, 'to_user')->withTrashed()->first();
	}

    public function from_user()
    {
        return $this->belongsTo(\App\Models\User::class, 'from_user')->withTrashed()->first();
    }

    public function scopeSkipToWhoBlockedByMe($query){
        return $query->whereNotIn('to_user', function($subQuery){
            $user = Auth::user();
            $subQuery->select('to_user')->from('blocked_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipToWhoBlockedMe($query){
        return $query->whereNotIn('to_user', function($subQuery){
            $user = Auth::user();
            $subQuery->select('from_user')->from('blocked_users')->where('to_user','=', $user->id);
        });
    }

    public function scopeSkipFromWhoBlockedByMe($query){
        return $query->whereNotIn('from_user', function($subQuery){
            $user = Auth::user();
            $subQuery->select('to_user')->from('blocked_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipFromWhoBlockedMe($query){
        return $query->whereNotIn('from_user', function($subQuery){
            $user = Auth::user();
            $subQuery->select('from_user')->from('blocked_users')->where('to_user','=', $user->id);
        });
    }

    public function scopeSkipDeletedUsers($query){
        return $query->whereNotIn('to_user', function($subQuery){
            $subQuery->select('id')->from('users')->where('deleted_at','!=', null);
        });
    }
}
