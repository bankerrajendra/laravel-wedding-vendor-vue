<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersImage
 * 
 * @property int $id
 * @property int $user_id
 * @property string $image_thumb
 * @property string $image_full
 * @property string $is_profile_image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */

class UsersImage extends BaseModel
{
	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'image_thumb',
		'image_full',
		'is_profile_image',
        'image_type',
		'is_approved',
		'is_new'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

}
