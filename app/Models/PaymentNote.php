<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;

class PaymentNote extends BaseModel
{
    protected $table = 'payment_notes';
    public $timestamps = true;
    protected $fillable = [
        'admin_id',
        'note',
        'site_id'
    ];

}
