<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Theme
 * 
 * @property int $id
 * @property string $name
 * @property string $link
 * @property string $notes
 * @property bool $status
 * @property string $taggable_type
 * @property int $taggable_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $profiles
 *
 * @package App\Models
 */
class Theme extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'themes';


    protected $casts = [
		'status' => 'bool',
		'taggable_id' => 'int'
	];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

	protected $fillable = [
		'name',
		'link',
		'notes',
		'status',
		'taggable_type',
		'taggable_id'
	];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return array
     */
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name'   => 'required|min:3|max:50|unique:themes,name'.($id ? ",$id" : ''),
                'link'   => 'required|min:3|max:255|unique:themes,link'.($id ? ",$id" : ''),
                'notes'  => 'max:500',
                'status' => 'required',
            ],
            $merge);
    }
    /**
     * Build Theme Relationships.
     *
     * @var array
     */
    public function profile()
    {
        return $this->hasMany('App\Models\Profile');
    }

}
