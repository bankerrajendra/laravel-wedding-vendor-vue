<?php

/**
 * Created by Rajendra Banker.
 * Date: Tue, 3rd Dec 2019.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class EventReview extends BaseModel
{
	protected $table = 'events_reviews';

	protected $fillable = [
		'event_id',
		'user_id',
		'vendor_id',
		'message',
		'rating',
		'approved',
		'site_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function event()
	{
		return $this->belongsTo(\App\Models\VendorEventInformation::class, 'event_id');
	}

	public function vendor()
	{
		return User::where('id', $this->vendor_id)->first();
	}

	// scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('message', 'like', '%'.$search.'%');
        });
    }
}
