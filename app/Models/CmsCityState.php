<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmsCityState extends BaseModel
{

    //
    protected $table = 'cms_city_states';

    protected $fillable = [
        'page_type',
        'type_id',
        'type_name',
        'slug',
        'home_page_visibility',
        'status',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'page_description',
        'page_image',
        'image_name'
    ];

}
