<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class SiteGeneralOptions extends BaseModel
{
    protected $table = 'site_general_options';
    public $timestamps = true;
    protected $fillable = [
        'key',
        'value',
        'site_id'
    ];
}
