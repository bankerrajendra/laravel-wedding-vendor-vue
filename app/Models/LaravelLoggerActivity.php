<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class LaravelLoggerActivity
 * 
 * @property int $id
 * @property string $description
 * @property string $userType
 * @property int $userId
 * @property string $route
 * @property string $ipAddress
 * @property string $userAgent
 * @property string $locale
 * @property string $referer
 * @property string $methodType
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class LaravelLoggerActivity extends BaseModel
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'laravel_logger_activity';

	protected $casts = [
		'userId' => 'int'
	];

	protected $fillable = [
		'description',
		'userType',
		'userId',
		'route',
		'ipAddress',
		'userAgent',
		'locale',
		'referer',
		'methodType'
	];
}
