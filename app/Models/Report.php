<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 11 Oct 2018 18:28:44 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Report
 * 
 * @property int $id
 * @property int $from_user
 * @property int $to_user
 * @property string $reason
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Report extends Eloquent
{
	use SoftDeletes;

	protected $casts = [
		'from_user' => 'int',
		'to_user' => 'int'
	];

	protected $fillable = [
		'from_user',
		'to_user',
		'reason',
		'description',
		'admin_reply',
		'to_user_reply',
		'user_deleted',
		'type'
	];
	
	/**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	public function from_user()
	{
		return $this->belongsTo(\App\Models\User::class, 'from_user')->withTrashed()->first();
	}
    public function to_user()
    {
        return $this->belongsTo(\App\Models\User::class, 'to_user')->withTrashed()->first();
    }

}
