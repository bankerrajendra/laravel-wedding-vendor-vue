<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 17:20:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class VendersImage
 * 
 * @property int $id
 * @property int $image_d
 * @property string $image_thumb
 * @property string $image_full
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */

class TemporaryEventVendorsImage extends BaseModel
{
	protected $table = 'temporary_events_images';
	protected $fillable = [
		'image_id',
		'image_thumb',
		'image_full',
        'image_type'
	];

}
