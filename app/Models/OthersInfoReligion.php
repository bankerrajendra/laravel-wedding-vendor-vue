<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OthersInfoReligion extends Model
{

    protected $fillable = [
        'name',
        'others_info_source',
		'user_id'
    ];

    public function users()
    {
        return $this->hasMany(\App\Models\Users::class);
    }
	
	  
   
    public function community(){
        return $this->belongsTo(\App\Models\Community::class);
    }
}
