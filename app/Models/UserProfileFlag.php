<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfileFlag extends Model
{
    protected $fillable = [
        'user_id',
		'property_name',
		'property_value',
		'feedback',
		'table_remark',
		'profile_updated'
    ];

    public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_id')->withTrashed()->first();
	}
}