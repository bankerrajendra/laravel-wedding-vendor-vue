<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 20th Nov 2019 12:42:45 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class VendorType extends BaseModel
{
    protected $table = 'vendor_types';

    protected $fillable = [
        'name',
        'slug'
    ];
}
