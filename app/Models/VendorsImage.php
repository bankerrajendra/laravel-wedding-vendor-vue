<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 Jul 2019 17:20:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class VendersImage
 * 
 * @property int $id
 * @property int $vendor_id
 * @property string $image_thumb
 * @property string $image_full
 * @property string $is_profile_image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */

class VendorsImage extends BaseModel
{
	protected $casts = [
		'vendor_id' => 'int'
	];

	protected $fillable = [
		'vendor_id',
		'image_thumb',
		'image_full',
		'is_profile_image',
        'image_type',
        'is_approved',
		'disapprove_reason',
		'is_new'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

}
