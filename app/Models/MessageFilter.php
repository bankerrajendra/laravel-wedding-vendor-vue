<?php

    /**
     * Created by Reliese Model.
     * Date: Tue, 21 Aug 2018 23:14:27 +0000.
     */

namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersInformation
 *
 * @property int $id
 * @property int $user_id
 * @property string $drink
 * @property string $smoke
 * @property string $food
 * @property int $profession_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class MessageFilter extends BaseModel
{
	protected $table = 'message_filters';

	protected $casts = [
        'user_id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'age_from',
        'age_to',
        'height_from',
        'height_to',
        'drink',
        'smoke',
        'diet',
        'religion',
        'body_type',
        'allow_without_images',
        'status'
    ];

    public function user() {
        return $this->belongsTo(\App\Models\User::class);
    }

}
