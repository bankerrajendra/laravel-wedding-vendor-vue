<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 12 Jul 2019 18:41:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersInformation
 * 
 * @property int $id
 * @property int $user_id
 * @property string $drink
 * @property string $smoke
 * @property string $food
 * @property int $education_id
 * @property int $profession_id
 * @property string $about
 * @property string $country_code
 * @property string $mobile_number
 * @property string $number_verification_code
 * @property string $number_verified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Education $education
 * @property \App\Models\Profession $profession
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class VendorsInformation extends BaseModel
{
	protected $table = 'vendors_information';

	public $timestamps  = false;

	protected $casts = [
		'vendor_id' 				=> 'int',
		'vendor_category' 			=> 'int',
		'display_address' 			=> 'int',
		'display_business_number' 	=> 'int'
	];

	protected $fillable = [
		'vendor_id',
		'company_name',
		'company_slug',
		'established_year',
		'vendor_category',
		'meta_keywords',
        'business_information',
        'specials',
		'display_address',
		'display_business_number',
        'business_website',
		'facebook',
		'instagram',
		'pinterest',
		'twitter',
		'linkedin',
		'notified_membership_renewal_five_days',
		'notified_membership_renewal_two_days'
	];

	public function vendor_category()
	{
		return $this->belongsTo(\App\Models\VendorCategory::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
