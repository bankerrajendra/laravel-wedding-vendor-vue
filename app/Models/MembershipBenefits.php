<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Education
 *
 * @property int $id
 * @property string $education
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $users_informations
 *
 * @package App\Models
 */
class MembershipBenefits extends BaseModel
{
    protected $table = 'membership_benefits';

    protected $fillable = [
        'name',
        'image',
        'status'
    ];

    public function scopeGetBenefit($query, $id)
    {
        return MembershipBenefits::where('id', $id)->first(['id', 'name', 'image']);
    }
}
