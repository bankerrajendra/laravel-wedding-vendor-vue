<?php

/**
 * Created by Rajendra Banker.
 * Date: Mon, 20th May 2019.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class Review extends BaseModel
{
	protected $fillable = [
		'user_id',
		'vendor_id',
		'message',
		'rating',
		'approved'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function vendor()
	{
		return User::where('id', $this->vendor_id)->first();
	}

	// scope function
    public function scopeGetRecordsSearch($query, $search)
    {
        return $query->where(function($innerQuery) use($search) {
            $innerQuery->where('message', 'like', '%'.$search.'%');
        });
    }
}
