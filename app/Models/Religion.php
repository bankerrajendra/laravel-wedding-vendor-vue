<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:26 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Religion extends BaseModel
{
    public $timestamps = false;

    protected $fillable = [
        'religion',
    ];

    public function users()
    {
        return $this->hasMany(\App\Models\User::class);
    }

    public function users_preferences()
    {
        return $this->hasMany(\App\Models\UsersPreference::class);
    }

    public function communities(){
        return $this->hasMany(\App\Models\Community::class);
    }

}
