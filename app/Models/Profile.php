<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 Aug 2018 23:14:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Profile
 * 
 * @property int $id
 * @property int $user_id
 * @property int $theme_id
 * @property string $location
 * @property string $bio
 * @property string $twitter_username
 * @property string $github_username
 * @property string $avatar
 * @property bool $avatar_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Theme $theme
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Profile extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'theme_id' => 'integer',
	];

	protected $fillable = [
		'user_id',
		'theme_id',
		'location',
		'bio',
		'twitter_username',
		'github_username',
		'avatar',
		'avatar_status'
	];

	public function theme()
	{
		return $this->belongsTo(\App\Models\Theme::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
