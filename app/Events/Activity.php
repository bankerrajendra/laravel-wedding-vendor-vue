<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Activity
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    protected $profileId;
    protected $action;
    /**
     * Create a new event instance.
     * @param $profileId
     * @return void
     */
    public function __construct($action, $profileId)
    {
        //
        $this->action = $action;
        $this->profileId = $profileId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function getAction(){
        return $this->action;
    }

    public function getProfileId(){
        return $this->profileId;
    }
}
