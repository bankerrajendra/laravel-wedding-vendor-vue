<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FileUpload
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    protected $request;
    protected $type;
    protected $id;
    /**
     * Create a new event instance.
     * @param $type
     * @param $request
     * @return void
     */
    public function __construct($type, $request, $id='')
    {
        //
        $this->request = $request;
        $this->type = $type;
        $this->id = $id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function getRequest(){
        return $this->request;
    }

    public function getType(){
        return $this->type;
    }
    public function getId() {
        return $this->id;
    }
}
