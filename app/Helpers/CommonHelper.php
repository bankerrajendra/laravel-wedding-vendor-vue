<?php

function get_blog_comments_count($blog_id) {
    return DB::table('blog_comments')->select('id')->where('blog_id',$blog_id)->where('status','1')->count();
}

function menuVendorCategories() {
    $site_id = config('constants.site_id');
    return $categories = \App\Models\VendorCategory::where('status', '=', 1)->where('name', '<>', 'Event Organizer')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->getVendorByType('wedding')->orderBy('name', 'ASC')->get(['id', 'name', 'slug']);
}
function menuEventCategories() {
    $site_id = config('constants.site_id');
    return $categories = \App\Models\VendorCategory::where('status', '=', '1')
        ->where('vendor_type', config('constants.vendor_type_id.event'))
        ->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')
        ->orderBy('name', 'ASC')
        ->get(['id', 'name', 'slug']);
}
function get_inbox_unread_count() {
    $user=\Illuminate\Support\Facades\Auth::user();
    $user_id=$user->id;
   // $filteredUsers=$user->getfilteredSender();

//->whereNotIn('sender_id',$filteredUsers)
    $inbox_rec=DB::table('messages')->select(DB::raw('distinct sender_id')) //->whereNotIn('sender_id',$filteredUsers)
        ->where('read_status','N')
        ->where('receiver_id', $user_id)->where('receiver_trash', 'N')->where('receiver_delete', 'N')->where('receiver_archive', 'N')->get();

//    $filter_rec=DB::table('messages')->select(DB::raw('distinct sender_id'))->whereIn('sender_id',$filteredUsers)
//        ->where('read_status','N')
//        ->where('receiver_id', $user_id)->where('receiver_trash', 'N')->where('receiver_delete', 'N')->get();

    $sent_rec=DB::table('messages')->select(DB::raw('distinct receiver_id')) //->whereNotIn('sender_id',$filteredUsers)
        ->where('read_status','N')
        ->where('sender_id', $user_id)->where('sender_trash', 'N')->where('sender_delete', 'N')->where('sender_archive', 'N')->get();

    $archive_rec=DB::table('messages')->select(DB::raw('distinct sender_id')) //->whereNotIn('sender_id',$filteredUsers)
        ->where('read_status','N')
        ->where('receiver_id', $user_id)->where('receiver_trash', 'N')->where('receiver_delete', 'N')->where('receiver_archive', 'Y')->get();

    $inbox_rec['inbox']=count($inbox_rec);
    $inbox_rec['archive']=count($archive_rec);
    //$inbox_rec['filter']=count($filter_rec);
    $inbox_rec['sent']=count($sent_rec);
    return $inbox_rec;
}

function get_spoken_languages($lang_arr){
    if(!is_array($lang_arr)) { $lang_arr1=explode(",", $lang_arr); } else
    { $lang_arr1=$lang_arr; }
    if(is_array($lang_arr1) || count($lang_arr1) > 1) {
        return DB::table('languages')->select('language')->whereIn('id', $lang_arr1)->get();
    }
}

function getPhoneCode($countrycode) {
    $country_phonecode=\App\Models\Country::select('phonecode')->where('id',$countrycode)->first();
    return $country_phonecode->phonecode;
}

function hobbyInterestConstant(&$value,$key, $p)
{
    $value=config('constants.'.$p.'.'.trim($value));;
}
function getSimilarities($user_obj) {
    $arr=array();
    $auth_user=\Illuminate\Support\Facades\Auth::user();

    if(!is_null($auth_user->users_information->hobbies) && !is_null($user_obj->users_information->hobbies)) {
        $hobbies=array_intersect(explode(",",$auth_user->users_information->hobbies), explode(",",$user_obj->users_information->hobbies));
        array_walk($hobbies,"hobbyInterestConstant","hobbies");
        if(count($hobbies)>0)
            $arr['hobbies']=implode(", ",$hobbies);
    }

    if(!is_null($auth_user->users_information->interests) && !is_null($user_obj->users_information->interests)) {
        $interests=array_intersect(explode(",",$auth_user->users_information->interests), explode(",",$user_obj->users_information->interests));
        array_walk($interests,"hobbyInterestConstant", "interests");
        if(count($interests)>0)
            $arr['interests']=implode(", ",$interests);
    }

    if(!is_null($auth_user->users_information->preferred_movies) && !is_null($user_obj->users_information->preferred_movies)) {
        $preferred_movies=array_intersect(explode(",",$auth_user->users_information->preferred_movies), explode(",",$user_obj->users_information->preferred_movies));
        array_walk($preferred_movies,"hobbyInterestConstant", "preferred_movies");
        if(count($preferred_movies)>0)
            $arr['preferred_movies']=implode(", ",$preferred_movies);
    }

    if(!is_null($auth_user->users_information->sports) && !is_null($user_obj->users_information->sports)) {
        $sports=array_intersect(explode(",",$auth_user->users_information->sports), explode(",",$user_obj->users_information->sports));
        array_walk($sports,"hobbyInterestConstant", "sports");
        if(count($sports)>0)
            $arr['sports']=implode(", ",$sports);
    }

    if(!is_null($auth_user->users_information->favourite_music) && !is_null($user_obj->users_information->favourite_music)) {
        $favourite_music=array_intersect(explode(",",$auth_user->users_information->favourite_music), explode(",",$user_obj->users_information->favourite_music));
        array_walk($favourite_music,"hobbyInterestConstant", "favourite_music");
        if(count($favourite_music)>0)
            $arr['favourite_music']=implode(", ",$favourite_music);
    }

    if(!is_null($auth_user->users_information->favourite_reads) && !is_null($user_obj->users_information->favourite_reads)) {
        $favourite_reads=array_intersect(explode(",",$auth_user->users_information->favourite_reads), explode(",",$user_obj->users_information->favourite_reads));
        array_walk($favourite_reads,"hobbyInterestConstant", "favourite_reads");
        if(count($favourite_reads)>0)
            $arr['favourite_reads']=implode(", ",$favourite_reads);
    }

    if(!is_null($auth_user->users_information->spoken_languages) && !is_null($user_obj->users_information->spoken_languages)) {
        $spoken_languages=array_intersect(explode(",",$auth_user->users_information->spoken_languages), explode(",",$user_obj->users_information->spoken_languages));
        $spoken_languagesArr=get_spoken_languages(implode(',', $spoken_languages));
        $langArr=array();
        foreach($spoken_languagesArr as $lang) {
            array_push($langArr, $lang->language);
        }
        if(count($langArr)>0)
            $arr['spoken_languages']=implode(", ",$langArr);
    }
    return $arr;
}

function admin_user_counter(){
    $stats=array();
    $stats['all_users'] = \App\Models\User::select('id')->hasUserRole()->onlySite()->count();
    $stats['all_users'] = \App\Models\User::select('id')->hasUserRole()->onlySite()->count();
    $stats['banned_users'] = \App\Models\User::select('id')->hasUserRole()->where('banned', 1)->onlySite()->count();
    $stats['deactivated_users'] = \App\Models\User::select('id')->hasUserRole()->where('deactivated', 1)->onlySite()->count();
    $stats['inactive_users'] = \App\Models\User::select('id')->hasUserRole()->where('access_grant', 'N')->where('banned', 0)->where('deactivated', 0)->onlySite()->count();
    $stats['active_users'] = \App\Models\User::select('id')->hasUserRole()->where('access_grant', 'Y')->where('banned', 0)->where('deactivated', 0)->onlySite()->count();
    $stats['deleted_users'] = \App\Models\User::onlyTrashed()->onlySite()->count();
    $stats['latest_updates'] = \App\Models\User::getPendingReviewUsers()->onlySite()->count();
    $stats['incomplete_users'] = \App\Models\User::select('id')->hasUserRole()->isIncompletedProfile()->onlySite()->count();
    $stats['paid_members'] = \App\Models\User::select('id')->hasUserRole()->getPaidMembers()->onlySite()->count();
    $stats['upcoming_renewal_members'] = \App\Models\User::select('id')->hasVendorRole()->getUpcomingRenewalMembers()->onlySite()->count();
    $stats['expired_members'] = \App\Models\User::select('id')->hasVendorRole()->getExpiredMembers()->onlySite()->count();
    // for vendor users
    $stats['all_vendors'] = \App\Models\User::select('id')->hasVendorRole()->onlySite()->count();
    $stats['banned_vendors'] = \App\Models\User::select('id')->hasVendorRole()->where('banned', 1)->onlySite()->count();
    $stats['deactivated_vendors'] = \App\Models\User::select('id')->hasVendorRole()->where('deactivated', 1)->onlySite()->count();
    $stats['inactive_vendors'] = \App\Models\User::select('id')->hasVendorRole()->where('access_grant', 'N')->where('banned', 0)->where('deactivated', 0)->onlySite()->count();
    $stats['active_vendors'] = \App\Models\User::select('id')->hasVendorRole()->where('access_grant', 'Y')->where('banned', 0)->where('deactivated', 0)->onlySite()->count();
    $stats['deleted_vendors'] = \App\Models\User::vendorDeleted()->onlySite()->count();
    $stats['latest_updates_vendors'] = \App\Models\User::getPendingReviewVendors()->hasVendorRole()->onlySite()->count();
    $stats['incomplete_vendors'] = \App\Models\User::select('id')->hasVendorRole()->isIncompletedProfile()->onlySite()->count();
    $stats['deleted_vendors'] = \App\Models\User::onlyTrashed()->hasVendorRole()->onlySite()->count();
    // for events
    $stats['all_events'] = \App\Models\VendorEventInformation::select('id')->onlySite()->count();
    // artists
    $stats['all_artists'] = \App\Models\Artist::select('id')->onlySite()->count();
    return $stats;
}

function ts2time($datetimeStr){
    return \Carbon\Carbon::createFromTimestamp(strtotime($datetimeStr), Auth::user()->getUserTimeZone())->format('h:i A, d F Y');
}


function getTimeZoneFromIpAddress_api(){
//  $timeZone='UTC';
    $ipAddress = new \App\Traits\CaptureIpTrait();
    $clientsIpAddress = $ipAddress->getClientIp();  //get_client_ip();

    try {
        if($clientsIpAddress == '::1'):
            $timeZone = 'Asia/Kolkata';
        else:
            // $clientInformation = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$clientsIpAddress));
            //$clientInformation = unserialize(file_get_contents('http://ip-api.com/php/'.$clientsIpAddress.'?key=RocDHDkeHbv0ask'));
            $clientInformation = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$clientsIpAddress.'?key=RocDHDkeHbv0ask'));
            $clientsLatitude = $clientInformation->lat;
            $clientsLongitude = $clientInformation->lon;
            $clientsCountryCode = $clientInformation->countryCode;
            $timeZone = get_nearest_timezone($clientsLatitude, $clientsLongitude, $clientsCountryCode);
        endif;
    } catch(\Exception $e) {
        $timeZone = 'Asia/Kolkata';
    }

    return $timeZone;
}

function get_nearest_timezone($cur_lat, $cur_long, $country_code = '') {
    $timezone_ids = ($country_code) ? DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country_code)
        : DateTimeZone::listIdentifiers();

    if($timezone_ids && is_array($timezone_ids) && isset($timezone_ids[0])) {

        $time_zone = '';
        $tz_distance = 0;

        //only one identifier?
        if (count($timezone_ids) == 1) {
            $time_zone = $timezone_ids[0];
        } else {

            foreach($timezone_ids as $timezone_id) {
                $timezone = new DateTimeZone($timezone_id);
                $location = $timezone->getLocation();
                $tz_lat   = $location['latitude'];
                $tz_long  = $location['longitude'];

                $theta    = $cur_long - $tz_long;
                $distance = (sin(deg2rad($cur_lat)) * sin(deg2rad($tz_lat)))
                    + (cos(deg2rad($cur_lat)) * cos(deg2rad($tz_lat)) * cos(deg2rad($theta)));
                $distance = acos($distance);
                $distance = abs(rad2deg($distance));
                // echo '<br />'.$timezone_id.' '.$distance;

                if (!$time_zone || $tz_distance > $distance) {
                    $time_zone   = $timezone_id;
                    $tz_distance = $distance;
                }

            }
        }
        return  $time_zone;
    }
    return 'unknown';
}


function get_conversation_unread_count($conversation_id){
    return DB::table('messages')->select('id')->where('user_id','!=',Auth::user()->id)->where('conversation_id',$conversation_id)->where('is_seen',0)->count();
}

function get_activity_counter() {
    $user=\Illuminate\Support\Facades\Auth::user();
    $activity['viewed_me_cnt']=$user->viewed_by_users()->skipFromConditions()->where('is_seen',0)->count();
    $activity['who_fav_me_cnt']=$user->favourited_by_users()->skipFromConditions()->where('is_seen',0)->count();
    return $activity;
}

function get_notification_counter() {
    $user=\Illuminate\Support\Facades\Auth::user();
    $user_id=$user->id;

    $notification['flag_notif_cnt']= \App\Models\Report::where('to_user', '=', $user_id)->where('is_read', 'N')->count();
    $notification['private_photo_req_cnt']=\App\Models\PhotoNotification::where('to_user', $user_id)->where('notification_type', 'private_photo_request')->where('status','P')->count();
    return $notification;
}

function user_menu_counter() {
    $stats=array();
    $user=\Illuminate\Support\Facades\Auth::user();
    $user_id=$user->id;

    $stats['message_cnt']=0;
    $stats['activity_cnt'] =0;
    $stats['notification_cnt']=0;


    $stats['message_cnt'] = DB::table('messages')->select(DB::raw('distinct sender_id'))->where('read_status','N')->where('receiver_id', $user_id)->where('receiver_trash', 'N')->where('receiver_delete', 'N')->get()->count();

    ///////////////Activity
    $viewed_me_cnt=$user->viewed_by_users()->skipFromConditions()->where('is_seen',0)->count();
    $who_fav_me_cnt=$user->favourited_by_users()->skipFromConditions()->where('is_seen',0)->count();
    $stats['activity_cnt'] = $viewed_me_cnt + $who_fav_me_cnt;

    $flag_notif_cnt= \App\Models\Report::where('to_user', '=', $user_id)->where('is_read', 'N')->count();
    $private_photo_req_cnt=\App\Models\PhotoNotification::where('to_user', $user_id)->where('notification_type', 'private_photo_request')->where('status','P')->count();
    $stats['notification_cnt']=$flag_notif_cnt + $private_photo_req_cnt;

    return $stats;
}

function get_profile_image() {
    return $authuser_img= DB::table('users_images')->select('image_thumb')->where('user_id',Auth::user()->id)->where('is_approved','Y')->orderBy('image_type','asc')->orderBy('is_profile_image','desc')->first();
}


//
//function get_thumb_image($authuser_img,$image_gender='opposite') {
//     if(isset($authuser_img) && $authuser_img->image_thumb!='') {
//         $img = "aaa".config('constants.upload_url_public') . config('constants.thumb_upload_path_rel') . $authuser_img->image_thumb;
//     } else {
//         if(Auth::user()->gender=='F'){
//             if($image_gender=='same'){
//                 $img = "img/female-default.png";
//             } else {
//                 $img = "img/male-default.png";
//             }
//         } else {
//             if($image_gender=='same'){
//                 $img = "img/male-default.png";
//             } else {
//                 $img = "img/female-default.png";
//             }
//         }
//         return $img;
//     }
//}
/***
@additional methods for admin/usermanagement
 **/

## convert subcaste id to name
function getSubcasteById($subcaste_id){
    return DB::table('subcastes')
        ->select('name')
        ->where('id',$subcaste_id)
        ->first();
}

function user_blocked_by_me($user_id) {
    return DB::table('blocked_users')
        ->where('from_user','=',(Auth::user()?Auth::user():Auth::guard('api')->user())->id)
        ->where('to_user', $user_id)->count();
}

function user_blocked_to_me($user_id) {
    return DB::table('blocked_users')
        ->where('to_user','=',(Auth::user()?Auth::user():Auth::guard('api')->user())->id)
        ->where('from_user', $user_id)->count();
}

function user_skipped_by_me($user_id) {
    return DB::table('skipped_users')
        ->where('from_user','=',(Auth::user()?Auth::user():Auth::guard('api')->user())->id)
        ->where('to_user', $user_id)->count();
}

function user_skipped_to_me($user_id) {
    return DB::table('skipped_users')
        ->where('to_user','=',(Auth::user()?Auth::user():Auth::guard('api')->user())->id)
        ->where('from_user', $user_id)->count();
}


## convert subcaste id to name
function getUserInfo($id){
    return DB::table('users_information')
        ->select('*')
        ->where('user_id',$id)
        ->first();
}

## convert sect id to name
function getSectById($sect_id){
    return DB::table('communities')
        ->select('name')
        ->where('id',$sect_id)
        ->first();
}
/** @end **/

//function getFlaggedUserData($user_id, $property_name){
//	 return DB::table('user_profile_flags')
//		->select('*')
//		->where(array('user_id'=>$user_id, 'property_name'=>$property_name))->first();
//}

function getFlaggedUserData($user_id){
    return DB::table('user_profile_flags')
        ->select('*')
        ->where('user_id',$user_id)->get();
}


## convert sect id to name
function getSects(){
    return DB::table('communities')
        ->select('*')
        ->where(array('religion_id'=>'3'))
        ->get();
}
/** @end **/


function getFlaggedUserDataAll($user_id){
    return DB::table('user_profile_flags')
        ->select('*')
        ->where(array('user_id'=>$user_id,'status'=>'1'))
        ->get();
}

function getUserProfileByProperty($user_id, $property, $table_remark){

    $user = Auth::user();
    if($table_remark == "u"){
        $table = "users";
        $userColumn = "id";
    }else{
        if($user->hasVendorRole()) {
            $table = "vendors_information";
            $userColumn = "vendor_id";
        } else {
            $table = "users_information";
            $userColumn = "user_id";
        }
    }

    $result = DB::table($table)
        ->select('*')
        ->where(array(
            $userColumn=>$user_id
        ))->first();

    return $result->{$property};
}

function getStateByCountry($country_id){

    if(is_array($country_id) || count( explode(",", $country_id)) > 1){
        return DB::table('states')
            ->select('*')
            ->whereIn('country_id', $country_id)
            ->get();
    }else{
        return DB::table('states')
            ->select('*')
            ->where(array('country_id'=>$country_id))
            ->get();
    }
}

## convert subcaste id to name
function getSubcastes(){
    return DB::table('subcastes')
        ->select('*')
        ->get();
}

## convert subcaste id to name
function getOtherReligionInfo($user_id){
    return DB::table('others_info_religions')
        ->select('*')
        ->where(array("user_id"=>$user_id))
        ->get();
}

function getCountry(){
    return DB::table('countries')
        ->select('*')
        ->get();
}

function getCityByState($state_id){

    if(is_array($state_id) || count(explode(",", $state_id))> 1){
        return DB::table('cities')
            ->select('*')
            ->whereIn('state_id',$state_id)
            ->get();
    }else{
        return DB::table('cities')
            ->select('*')
            ->where(array('state_id'=>$state_id))
            ->get();
    }
}

function checkUserAccessGrant($user_id){
    return DB::table('users')
        ->select('access_grant')
        ->where(array('id'=>$user_id))
        ->first();
}

function getLanguages(){
    return DB::table('languages')
        ->select('*')
        ->get();
}

function getProfession(){
    return DB::table('professions')
        ->select('*')
        ->get();
}

function getHobbies($category) {
    return config('constants.'.$category);
}

function getEducation(){
    return DB::table('educations')
        ->select('*')
        ->get();
}


function getCityByName($user_id){
    $city =  DB::table('users')
        ->select('city_id')
        ->where("id", $user_id)
        ->first();

    $cityName = DB::table('cities')
        ->select('name')
        ->where("id", $city->city_id)
        ->first();

    return $cityName->name;
}

function getuserImage($user_id){
    return DB::table('users_images')
        ->select('image_thumb')
        ->where("user_id", $user_id)
        ->first();

}

function getGender($user_id){
    return DB::table('users')
        ->select('gender')
        ->where("id", $user_id)
        ->first();
}


function getCurrency() {
    $currencies= DB::table('currencies')
        ->select('*')
        ->get();
    $currencyArr=array();
    foreach($currencies as $currency) {
        $currencyArr[$currency->code]=$currency->code;
    }
    return $currencyArr;
}

function getReligions() {
    $religions= DB::table('religions')
        ->select('*')
        ->get();
    $currencyArr=array();
    foreach($religions as $religion) {
        $currencyArr[$religion->id]=$religion->religion;
    }
    return $currencyArr;
}

function getProfessionUser($pid){
    return DB::table('professions')
        ->select('profession')
        ->where(array('id'=>$pid))
        ->first();
}

// To Show less text and show Read More - Rajendra Banker on 4th April 2019
function showReadMore($string, $limit, $read_more_lnk) {
    $string = strip_tags($string);
    if (strlen($string) > $limit) {
        // truncate string
        $stringCut = substr($string, 0, $limit);
        $endPoint = strrpos($stringCut, ' ');
        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        if($read_more_lnk != "") {
            $string .= '... <a href="'.$read_more_lnk.'" target="_blank">READ MORE</a>';    
        } else {
            $string .= '...';    
        }
    }
    echo $string;
}

function showReadMoreApi($string, $limit, $read_more_lnk) {
    $string = strip_tags($string);
    if (strlen($string) > $limit) {
        // truncate string
        $stringCut = substr($string, 0, $limit);
        $endPoint = strrpos($stringCut, ' ');
        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        if($read_more_lnk != "") {
            $string .= '... <a href="'.$read_more_lnk.'" target="_blank">READ MORE</a>';    
        } else {
            $string .= '...';    
        }
    }
    return $string;
}

// to get the site setting by key
function getGeneralSiteSetting($key = '') {
    $site_setting_obj = \App\Models\SiteGeneralOptions::where('site_id', config('constants.site_id'))
        ->where('key', $key)
        ->first(['value']);
    if(!empty($site_setting_obj)) {
        $site_setting_value = $site_setting_obj->value;
    } else {
        $site_setting_value = '0';
    }
    return $site_setting_value;
}
// get page title for templates pages of backend
function getPageTitleByType($type = "") {
    if($type == 'news-letter') {
        $page_title = "News Letter";
    } elseif($type == "email") {
        $page_title = "Email";
    } elseif($type == "sms") {
        $page_title = "SMS";
    } else {
        $page_title = "";
    }
    return $page_title;
}

/**
 * Replace Email Template variables with dynamic values
 */
function replaceEmailTemplateVars($email_contents, $variables_replace) {
    return strtr($email_contents, $variables_replace);        
}
/**
 * Get Unsubscribe Type depend on flag
 */
function getUnsubscribeType($flag) {
    /**
     * Unsubscribe depends on flag
     */
    switch ($flag) {
        case 'notify_sends_message':
            $type = 'M';
            break;
        case 'notify_favorites_me':
            $type = 'F';
            break;
        case 'notify_views_profile':
            $type = 'V';
            break;
        case 'notify_content_approve_deny':
            $type = 'C';
            break;
        case 'notify_new_matched_profiles':
            $type = 'P';
            break;
        case 'notify_news_letter':
            $type = 'N';
            break;
    }
    return $type;
}

/**
 * Check that user image pending
 */
function getNotApprovedImageUserData($user_id) {
    return DB::table('users_images')
        ->select('image_thumb')
        ->where("user_id", $user_id)
        ->where("is_approved", "N")
        ->count();
}

/**
 * Check that user image pending for vendor
 */
function getNotApprovedImageVendorData($vendor_id) {
    return DB::table('vendors_images')
        ->select('image_thumb')
        ->where("vendor_id", $vendor_id)
        ->where("is_approved", "N")
        ->count();
}

/**
 * Check that user videos pending for vendor
 */
function getNotApprovedVideoVendorData($vendor_id) {
    return DB::table('vendors_videos')
        ->select('video_title')
        ->where("vendor_id", $vendor_id)
        ->where("is_approved", "N")
        ->count();
}

/**
 * Check that user custom faq pending for vendor
 */
function getNotApprovedCustomFaqVendorData($vendor_id) {
    return DB::table('vendors_custom_faqs')
        ->select('id')
        ->where("vendor_id", $vendor_id)
        ->where("is_approved", "N")
        ->count();
}

/**
 * Check profile filed updated and send latest updates
 */
function sendLatestUpdateAdmin($property_name) {
    $user_id = Auth::user()->id;
    // check record exists
    $profile_flag = \App\Models\UserProfileFlag::where('property_name', '=', $property_name)->where('user_id', '=', $user_id);
    if($profile_flag->exists()) {
        // update existing record
        $profile_flag->update( ['profile_updated' => "1" ] );
    } else {
        $table_remark = ( $property_name == "first_name" || $property_name == "last_name" ) ? "u" : "ua";
        // create profile flag record
        \App\Models\UserProfileFlag::Create([
            'user_id' 		    => $user_id,
            'property_name'     => $property_name,
            'table_remark'	    => $table_remark,
            'feedback'		    => "",
            'status'            => "0",
            'profile_updated'   => "1"
        ]);
    }
}

/**
 * Get banner image by type
 */
function getBannerByType($type,$class='') {
    try {
        $banner_image = \App\Models\Banner::where('banner_type', '=', $type)
                            ->where('site_id', '=', config('constants.site_id'))
                            ->where('status', '=', 1)
                            ->get(['id', 'title', 'banner_type', 'banner_link', 'banner_image'])
                            ->random(1);
        $single_banner = \App\Models\Banner::find($banner_image[0]->id);
        $banner_html = '';
        if($single_banner) {
            $image = $single_banner->banner_image;
            if($single_banner->banner_link != "") {
                $banner_html .= '<a href="'.$single_banner->banner_link.'" target="_blank">';
            }
                $banner_html .= '<img src="'.asset('img/banners/'.$image).'" class="img-responsive '.$class.'" alt="" border="0" width="100%" />';
            if($single_banner->banner_link != "") {
                $banner_html .= '</a>';
            }
        }
        return $banner_html;
    } catch(\Exception $e) {
        return '';
    }
}

/**
 * Get user specific alert activity count
 */
function getUserActivityCounts($user) {
    $activity_counts = [];
    $activity_counts['messaged'] = DB::table('messages')->select(DB::raw('distinct sender_id'))->where('read_status','N')->where('receiver_id', $user->id)->where('receiver_trash', 'N')->where('receiver_delete', 'N')->count();
    $activity_counts['viewed'] = $user->viewed_by_users()->skipFromConditions()->where('is_seen',0)->count();
    $activity_counts['favourited'] = \App\Models\FavouritedUser::where('to_user', $user->id)->where('to_user_status', 0)->where('is_seen',0)->count();
    return $activity_counts;
}

/**
 * Get banner image by type for vue
 */
function getBannerByTypeForVue($type) {
    try {
        $banner_images = \App\Models\Banner::where('banner_type', '=', $type)
                            ->where('site_id', '=', config('constants.site_id'))
                            ->where('status', '=', 1)
                            ->get(['id', 'banner_link', 'banner_image'])
                            ->random(1);
        $banners['banner_image'] = asset('img/banners/'.$banner_images[0]->banner_image);
        $banners['banner_link'] = $banner_images[0]->banner_link;
        
        return $banners;
    } catch(\Exception $e) {
        return null;
    }
}

function slugifyText($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}
// get sites satellites
function getSiteSatellites()
{
    return $satellites = \App\Models\Satellite::where('status', '=', 1)->get(['id', 'title', 'url']);
}
// get youtube id from url
function GetYouTubeId($url)
{
    $youtube_id = '';
    if (stristr($url,'watch?v=')) {
        $video_id = explode("?v=", $url);
        if (!empty($video_id[1])) {
            if(stristr($video_id[1],'&')) {
                $video_id = explode("&", $video_id[1]);
                $youtube_id = $video_id[0];
            } else {
                $youtube_id = $video_id[1];
            }
        }
    } else {
        try {
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
            $youtube_id = $match[1];
        } catch(\Exception $e) {
            //
        }
    }
    return $youtube_id;
}

// get user country
function getUserLocationInformation()
{
    if(\Illuminate\Support\Facades\Auth::check()) {
        // get user IP
        $userObj = \App\Models\User::where('id', \Illuminate\Support\Facades\Auth::id())->first(['id', 'signup_ip_address']);
        if($userObj->signup_ip_address != '') {
            $ip_address = $userObj->signup_ip_address;
        } else {
            $ip_address = request()->ip();
        }
    } else {
        // get guest IP
        $ip_address = request()->ip();
        //$ip_address = '43.250.158.190'; // for local
    }
    if($ip_address == '::1' || $ip_address == '127.0.0.1' || $ip_address == '192.168.0.104') {
        $ip_address = '43.250.158.190'; // for local
    }

    $county_info = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$ip_address.'?key=RocDHDkeHbv0ask'));

    return $county_info;
}