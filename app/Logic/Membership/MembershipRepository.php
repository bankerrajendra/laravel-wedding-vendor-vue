<?php
namespace App\Logic\Membership;

use App\Models\User;
use App\Models\VendorsInformation;
use App\Models\MembershipPlans;
use App\Models\MembershipBenefits;
use App\Models\MembershipPlanPackages;
use App\Models\PaidMembers;
use App\Models\PaidMembersOrdersInfo;
use App\Models\PaymentNote;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralEmail;
use App\Models\Report;
use DateTime;

class MembershipRepository
{
    public function getPlanBenefits($plan_type)
    {
        $benefits = MembershipPlans::getPlanBenefits($plan_type);
        // explode benefits and get required info
        $member_benefits_arr = explode(',', $benefits->membership_benefits);
        $membership_benefits = [];
        $i = 0;
        foreach($member_benefits_arr as $benefit) {
            $benefit_single = MembershipBenefits::getBenefit($benefit);
            $membership_benefits[$i]['id'] = $benefit_single->id;
            $membership_benefits[$i]['name'] = $benefit_single->name;
            $membership_benefits[$i]['image'] = $benefit_single->image;
            $i++;
        }

        return $membership_benefits;
    }
    
    public function saveBenefitIcoImage($image)
    {
        $extension          = $image->getClientOriginalExtension();
        $fileName           = rand(11111, 99999).'_'.time().'.'.$extension;
        $img                = Image::make($_FILES['icon_image']['tmp_name']);
        // resize image
        $img->fit(40, 40);
        // save image
        $img->save(public_path('img/membership-icons/'.$fileName));
        return $fileName;
    }

    public function getPlanActivePackages($plan_id)
    {
        return MembershipPlans::getActivePacks($plan_id);
    }

    /**
     * save member as paid in single row
     */
    public function savePaidMember($payment_gateway, $submitted_vals, $fetched_transaction_object)
    {
        $user_id = isset($submitted_vals['user_id']) && isset($submitted_vals['user_id']) != "" ? $submitted_vals['user_id'] : Auth::Id(); 
        /**
         * Save the paid member information
         */
        $paid_mem_obj = new PaidMembers();
        $paid_mem_obj->plan_id = $submitted_vals['plan_id'];
        $paid_mem_obj->user_id = $user_id;
        $paid_mem_obj->plan_name = $submitted_vals['plan_name'];
        $paid_mem_obj->plan_amount = $submitted_vals['package_amount'];
        $paid_mem_obj->currency = config('constants.currency_code');
        $paid_mem_obj->duration = $submitted_vals['package_duration'];
        $paid_mem_obj->duration_type = $submitted_vals['package_duration_type'];
        /**
         * Save package id
         */
        $package_details = MembershipPlanPackages::where('plan_id', '=', $submitted_vals['plan_id'])
                                ->where('amount', '=', $submitted_vals['package_amount'])
                                ->where('duration', '=', $submitted_vals['package_duration'])
                                ->where('type', '=', $submitted_vals['package_duration_type'])
                                ->first(['id']);
        if(isset($submitted_vals['package_id']) && $submitted_vals['package_id'] != "") {
            $paid_mem_obj->package_id = $submitted_vals['package_id'];
        } else {
            $paid_mem_obj->package_id = $package_details->id;
        }
        $paid_mem_obj->membership_start_date = date('Y-m-d 00:00:00');
        // get end date from old record
        $prev_end_date_rec = PaidMembers::latest()
                                ->where('user_id', $user_id)
                                ->first(['membership_end_date']);
        $end_date = " + ".$paid_mem_obj->duration." ".strtolower($paid_mem_obj->duration_type);
        if(!empty($prev_end_date_rec)) {
            $datetime1 = new DateTime($prev_end_date_rec->membership_end_date);
            $datetime2 = new DateTime(date('Y-m-d 00:00:00'));
            if($datetime1 > $datetime2) {
                $paid_mem_obj->membership_end_date = date('Y-m-d 00:00:00', strtotime($prev_end_date_rec->membership_end_date . $end_date));                
            } else {
                $paid_mem_obj->membership_end_date = date('Y-m-d 00:00:00', strtotime($end_date));
            }
        } else {
            $paid_mem_obj->membership_end_date = date('Y-m-d 00:00:00', strtotime($end_date));
        }
        $paid_mem_obj->payment_gateway_name = $payment_gateway;
        $paid_mem_obj->site_id = config('constants.site_id');
        if($payment_gateway != "" && isset($submitted_vals['paid_by']) && $submitted_vals['paid_by'] == "Admin") {
            // save the admin note and method
            $paid_mem_obj->paid_by = "Admin";
        } else {
            $paid_mem_obj->paid_by = "User";
        }
        $paid_mem_obj->save();
        /**
         * Email to member
         */
        try {
            $user_details = User::find($user_id);
            if($payment_gateway == "Free Diamond") {
                $mail_req_arry = [
                    'mail_name' => 'Send Free Diamond Membership',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $user_details->first_name,
                        '[%EXPIRE_DURATION%]' => date('F d, Y', strtotime($paid_mem_obj->membership_end_date))
                    ]
                ];
                // send flag notification
                Report::create(
                    [
                        'from_user' => Auth::id(),
                        'to_user' => $user_details->id,
                        'reason' => 'Free Diamond Membership',
                        'description' => 'Admin send free diamond membership',
                        'admin_reply' => 'Congratulations! you have been upgraded for Diamond membership plan, and it will be expired on '.date('F d, Y', strtotime($paid_mem_obj->membership_end_date)).'.',
                        'type' => 'other'
                    ]
                );
            } else {
                $unsubscribe_link = route('unsubscribe', [ 'token' => $user_details->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Upgrade Membership',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $user_details->first_name,
                        '[%UPGRADED_PLAN_NAME%]' => $paid_mem_obj->plan_name,
                        '[%EXPIRE_DURATION%]' => date('F d, Y', strtotime($paid_mem_obj->membership_end_date))
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
            }
            Mail::to($user_details->email)->send(new GeneralEmail($mail_req_arry));
            // send payment success email
            if($paid_mem_obj->paid_by == "User") {
                $unsubscribe_link = route('unsubscribe', [ 'token' => $user_details->token, 'type' => '' ]);
                $mail_req_arry_anot = [
                    'mail_name' => 'Payment Success',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $user_details->first_name,
                        '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                        '[%WEBSITENAME%]' => getGeneralSiteSetting('site_name')
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($user_details->email)->send(new GeneralEmail($mail_req_arry_anot));
            }
        } catch (\Exception $e){
            \Log::error("Some error while sending upgrade membership mail for user id:".$user_id);
        }

        if($payment_gateway == "Paypal" || $payment_gateway == "Helcim" || $payment_gateway == "Merrco") {
            // save order info.
            $this->savePaidMemberOrderInformation($payment_gateway, $paid_mem_obj->id, $submitted_vals, $fetched_transaction_object);
        }
        if($payment_gateway != "" && isset($submitted_vals['paid_by']) && $submitted_vals['paid_by'] == "Admin") { 
            // save admin note
            $payment_notes = new PaymentNote();
            $payment_notes->admin_id = Auth::Id();
            $payment_notes->note = $submitted_vals['note'];
            $payment_notes->site_id = config('constants.site_id');
            $payment_notes->save();
        }
        // update membership notification flag to N
        $users_information = VendorsInformation::where('vendor_id', $user_id)->first();
        $users_information->notified_membership_renewal_five_days = "N";
        $users_information->notified_membership_renewal_two_days = "N";
        $users_information->save();
        // update user is_page to 1
        $is_paid_obj = User::find($user_id);
        $is_paid_obj->is_paid = 1;
        $is_paid_obj->save();
    }
    
    /**
     * add order information
    */
    protected function savePaidMemberOrderInformation($payment_gateway, $paid_member_id, $submitted_vals, $transaction_obj)
    {
        if($payment_gateway == "Helcim") {
            try {
                $paid_mem_ord_obj = new PaidMembersOrdersInfo();
                $paid_mem_ord_obj->paid_membership_id = $paid_member_id;
                $paid_mem_ord_obj->transaction_id = $transaction_obj->transactionId;
                $paid_mem_ord_obj->transaction_type = $transaction_obj->type;
                $paid_mem_ord_obj->transaction_date_time = $transaction_obj->date .' '. $transaction_obj->time;
                $paid_mem_ord_obj->card_user_country = $submitted_vals['country'];
                $paid_mem_ord_obj->card_user_phone_country_code = $submitted_vals['phone-country-code'];
                $paid_mem_ord_obj->card_user_mobile_number = $submitted_vals['mobile-number'];
                $paid_mem_ord_obj->card_holder_name = $transaction_obj->cardHolderName;
                $paid_mem_ord_obj->card_billing_add_1 = $submitted_vals['billing-add-1'];
                $paid_mem_ord_obj->card_billing_add_2 = $submitted_vals['billing-add-2'];
                $paid_mem_ord_obj->card_billing_city = $submitted_vals['billing-city'];
                $paid_mem_ord_obj->card_billing_state = $submitted_vals['billing-state'];
                $paid_mem_ord_obj->card_billing_zip_code = $submitted_vals['zip-code'];
                $paid_mem_ord_obj->transaction_amount = $transaction_obj->amount;
                $paid_mem_ord_obj->currency = $transaction_obj->currency;
                $paid_mem_ord_obj->card_number = $transaction_obj->cardNumber;
                $paid_mem_ord_obj->card_type = $transaction_obj->cardType;
                $paid_mem_ord_obj->approval_code = $transaction_obj->approvalCode;
                $paid_mem_ord_obj->order_number = $transaction_obj->orderNumber;
                $paid_mem_ord_obj->customer_code = $transaction_obj->customerCode;
                $paid_mem_ord_obj->save();
            } catch (\Exception $e) {
                \Log::Info("Error while inserting for payment order info, Helcim.");
            }
        } else if($payment_gateway == "Merrco") {
            try {
                $paid_mem_ord_obj = new PaidMembersOrdersInfo();
                $paid_mem_ord_obj->paid_membership_id = $paid_member_id;
                $paid_mem_ord_obj->transaction_id = $transaction_obj->authCode;
                $paid_mem_ord_obj->transaction_type = "purchase";
                $getDate = ( explode('T', $transaction_obj->txnTime) );
                $getTime = ( explode('Z', $getDate[1]) );
                $paid_mem_ord_obj->transaction_date_time = $getDate[0].' '.$getTime[0];
                $paid_mem_ord_obj->card_user_country = $submitted_vals['country'];
                $paid_mem_ord_obj->card_user_phone_country_code = $submitted_vals['phone-country-code'];
                $paid_mem_ord_obj->card_user_mobile_number = $submitted_vals['mobile-number'];
                $paid_mem_ord_obj->card_holder_name = $submitted_vals['first-name'] .' '. $submitted_vals['last-name'];
                $paid_mem_ord_obj->card_billing_add_1 = $submitted_vals['billing-add-1'];
                $paid_mem_ord_obj->card_billing_add_2 = $submitted_vals['billing-add-2'];
                $paid_mem_ord_obj->card_billing_city = $submitted_vals['billing-city'];
                $paid_mem_ord_obj->card_billing_state = $submitted_vals['billing-state'];
                $paid_mem_ord_obj->card_billing_zip_code = $submitted_vals['zip-code'];
                $paid_mem_ord_obj->transaction_amount = $transaction_obj->amount;
                $paid_mem_ord_obj->currency = $transaction_obj->currencyCode;
                $paid_mem_ord_obj->card_number = $transaction_obj->card->lastDigits;
                $paid_mem_ord_obj->card_type = $transaction_obj->card->type;
                $paid_mem_ord_obj->approval_code = $transaction_obj->authCode;
                $paid_mem_ord_obj->order_number = $transaction_obj->id;
                $paid_mem_ord_obj->customer_code = $transaction_obj->merchantRefNum;
                $paid_mem_ord_obj->save();
            } catch (\Exception $e) {
                \Log::Info("Error while inserting for payment order info, Merrco.");
            }
        } else if($payment_gateway == "Paypal") {
            try {
                $paid_mem_ord_obj = new PaidMembersOrdersInfo();
                $paid_mem_ord_obj->paid_membership_id = $paid_member_id;
                $paid_mem_ord_obj->transaction_id = $transaction_obj['id'];
                $paid_mem_ord_obj->transaction_type = "purchase";
                $getDate = ( explode('T', $transaction_obj['txnTime']) );
                $getTime = ( explode('Z', $getDate[1]) );
                $paid_mem_ord_obj->transaction_date_time = $getDate[0].' '.$getTime[0];
                $paid_mem_ord_obj->card_user_country = $submitted_vals['country'];
                $paid_mem_ord_obj->card_user_phone_country_code = $submitted_vals['phone-country-code'];
                $paid_mem_ord_obj->card_user_mobile_number = $submitted_vals['mobile-number'];
                $paid_mem_ord_obj->card_holder_name = $submitted_vals['first-name'] .' '. $submitted_vals['last-name'];
                $paid_mem_ord_obj->card_billing_add_1 = $submitted_vals['billing-add-1'];
                $paid_mem_ord_obj->card_billing_add_2 = $submitted_vals['billing-add-2'];
                $paid_mem_ord_obj->card_billing_city = $submitted_vals['billing-city'];
                $paid_mem_ord_obj->card_billing_state = $submitted_vals['billing-state'];
                $paid_mem_ord_obj->card_billing_zip_code = $submitted_vals['zip-code'];
                $paid_mem_ord_obj->transaction_amount = $transaction_obj['amount'];
                $paid_mem_ord_obj->currency = $transaction_obj['currencyCode'];
                $paid_mem_ord_obj->card_number = $transaction_obj['car_number'];
                $paid_mem_ord_obj->card_type = $transaction_obj['card_type'];
                $paid_mem_ord_obj->approval_code = $transaction_obj['approval_code'];
                $paid_mem_ord_obj->order_number = $transaction_obj['order_number'];
                $paid_mem_ord_obj->customer_code = $transaction_obj['customer_code'];
                $paid_mem_ord_obj->save();
            } catch (\Exception $e) {
                \Log::Info("Error while inserting for payment order info, paypal.");
            }
        }
    }

    /**
     * Get MembershipHistory
     */
    public function getUserMembershipHistory($user_id = "")
    {
        return $result_set = PaidMembers::getMembershipPayments($user_id)
                                ->orderBy('id', 'desc')
                                ->get();
    }

    /**
     * Get Latest MembershipHistory Record
     */
    public function getUserLatestMembershipHistory()
    {
        return $result_set = PaidMembers::getMembershipPayments()
                                ->latest()
                                ->first();
    }

    /**
     * Get Admin Packages
     */
    public function getPlanAdminPackages($plan_id)
    {
        return MembershipPlans::getAdminPacks($plan_id);
    }
}