<?php
namespace App\Logic\Event;

use App\Models\VendorEventInformation;
use App\Models\User;
use App\Models\Country;
use App\Models\Artist;
use App\Models\VendorEventImage;
use App\Models\TemporaryEventVendorsImage;
use App\Models\EventReview;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class EventRepository {

    protected $site_id;

    public function __construct()
    {
        $this->site_id = config('constants.site_id');
    }

    public function addRecord($request)
    {
        $online_seat_selection = $request->has('online_seat_selection') && trim($request->input('online_seat_selection')) != "" && trim($request->input('online_seat_selection')) == "1" ? 1 : 0;
        $parking = $request->has('parking') && trim($request->input('parking')) != "" && trim($request->input('parking')) == "1" ? 1 : 0;
        $food_for_sale = $request->has('food_for_sale') && trim($request->input('food_for_sale')) != "" && trim($request->input('food_for_sale')) == "1" ? 1 : 0;
        $drinks_for_sale = $request->has('drinks_for_sale') && trim($request->input('drinks_for_sale')) != "" && trim($request->input('drinks_for_sale')) == "1" ? 1 : 0;
        $babysitting_services = $request->has('babysitting_services') && trim($request->input('babysitting_services')) != "" && trim($request->input('babysitting_services')) == "1" ? 1 : 0;
        
        // events dates
        $expld_date = explode('.', $request->input('start_date'));
        $start_date = $expld_date[2].'-'.$expld_date[1].'-'.$expld_date[0];
        // validate
        $start_date_check = strtotime(date('Y-m-d', strtotime($start_date) ) );
        $todays = strtotime(date('Y-m-d'));

        $end_expld_date = explode('.', $request->input('end_date'));
        $end_date = $end_expld_date[2].'-'.$end_expld_date[1].'-'.$end_expld_date[0];
        $end_date_check = strtotime(date('Y-m-d', strtotime($end_date) ) );

        if($start_date_check < $todays || $end_date_check < $todays) {
            return response()->json([
                'status' => 0,
                'message' => 'Event\'s dates can not be past.'
            ]);
        }

        if($start_date_check > $end_date_check) {
            return response()->json([
                'status' => 0,
                'message' => 'Event\'s start date and end date are not proper, start date must less than or same as end date.'
            ]);
        }

        // validate the start time and end time if date is same
        if($request->start_time != "" && $request->end_time != "") {
            if(strtotime(date('Y-m-d h:i A', strtotime($start_date.' '.$request->start_time))) > strtotime(date('Y-m-d h:i A', strtotime($end_date.' '.$request->end_time)))) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Event\'s start time and end time are not proper, end time must greater than start time.']);
            }
        }

        $st_date = strtotime(date('d.m.Y h:i A', strtotime($request->start_date.' '.$request->start_time)));
        $start_date = date('Y-m-d H:i:s', $st_date);
            
        $ed_date = strtotime(date('d.m.Y h:i A', strtotime($request->end_date.' '.$request->end_time)));
        $end_date = date('Y-m-d H:i:s', $ed_date);

        // artists
        $artists = $request->has('artists') ? array_unique($request->input('artists')) : "";
        $artist_insert = "";
        if($artists != '') {
            // check out the submitted artists already exists
            if(count($artists) > 0) {
                foreach($artists as $artist) {
                    if($artist != null && $artist != '') {
                        if(Artist::where('name', 'like', $artist)->exists()) {
                            $artistObj = Artist::where('name', 'like', $artist)->first(['id']);
                            $artist_insert .= $artistObj->id.",";
                        } else {
                            $artistObj = Artist::create([
                                'name' => $artist,
                                'vendor_id' => Auth::id(),
                                'site_id' => config('constants.site_id')
                            ]);
                            $artist_insert .= $artistObj->id.",";
                        }
                    }
                }
            }
        }
        // remove last comma from artists insert
        if($artist_insert != '') {
            $artist_insert = rtrim($artist_insert, ',');
        }

        $record = new VendorEventInformation();
        $event_slug = slugifyText($request->input('event_name'));
        if(VendorEventInformation::where('event_slug', $event_slug)->exists()) {
            // 2. update slug by adding -2
            $slgName = VendorEventInformation::where('event_slug', 'like', '%'.$event_slug.'%')->count();
            $event_slug .= "-".($slgName+1);
        }
        $record->event_name = $request->input('event_name');
        $record->venue = $request->input('venue');
        $record->artists = $artist_insert;
        $record->vendor_id = Auth::id();
        $record->event_slug = $event_slug;
        $record->address = $request->input('address');
        $record->country_id = $request->input('country');
        $record->state_id = $request->input('state');
        $record->city_id = $request->input('city');
        $record->zip = $request->input('zip');
        $record->description = $request->input('description');
        $record->ticket_information = $request->input('ticket_information');
        $record->categories = serialize($request->input('categories'));
        $record->start_date = $start_date;
        $record->end_date = $end_date;
        $record->ticket_url = $request->input('ticket_url');
        $record->meta_keywords = $request->input('meta_keywords');
        $record->video_url = $request->input('video_url');
        $record->online_seat_selection = $online_seat_selection;
        $record->parking = $parking;
        $record->food_for_sale = $food_for_sale;
        $record->drinks_for_sale = $drinks_for_sale;
        $record->babysitting_services = $babysitting_services;
        $record->business_website = $request->input('business_website');
        $record->facebook = $request->input('facebook');
        $record->instagram = $request->input('instagram');
        $record->pinterest = $request->input('pinterest');
        $record->twitter = $request->input('twitter');
        $record->linkedin = $request->input('linkedin');
        $site_id = (int) config('constants.site_id');
        $record->site_id = $site_id;
        $record->save();
        
        // save images
        // 1. check image already there
        $postimage_id = Session::get('event_poster', '');
        $ImagePoster = TemporaryEventVendorsImage::where('image_id', $postimage_id)->where('image_type', 'poster')->first();
        // 1.1 Update the poster name as per the naming convention
        if($ImagePoster != null) {
            $PstrThumbNm = $ImagePoster->image_thumb;
            $pstrExtArry = explode('.', $PstrThumbNm);
            $pstrExt = $pstrExtArry[1];
            $poster_name = config('constants.vendor_images_pre_name').'event-poster-'.$record->event_slug;
            if(VendorEventImage::where('image_type', 'poster')->where('image_full', 'like', $poster_name.'.'.$pstrExt)->exists()) {
                $countImg = VendorEventImage::where('image_type', 'poster')->where('image_full', 'like', '%'.$poster_name.'%')->count();
                // 2. update slug by adding -2
                $poster_name .= "-".($countImg+1);
            }
            $poster_name .= ".".$pstrExt;
            $original_poster = 'vendor_photos/original/'.$ImagePoster->image_full;
            $thumb_poster = 'vendor_photos/thumb_cropped/'.$ImagePoster->image_thumb;
            Storage::disk('s3')->rename($original_poster, 'vendor_photos/original/' . $poster_name);
            Storage::disk('s3')->rename($thumb_poster, 'vendor_photos/thumb_cropped/' . $poster_name);
        }
        $image_id = Session::get('event_cover', '');
        $ImageCover = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->first();
        $pimage_id = Session::get('event_profile', '');
        $profilImg = TemporaryEventVendorsImage::where('image_id', $pimage_id)->where('image_type', 'profile')->first();
        Session::forget(['event_cover', 'event_profile', 'event_poster']);
        // 1.1 Update the image name as per the naming convention
        if($ImageCover != null) {
            $ImgThumbNm = $ImageCover->image_thumb;
            $coverExtArry = explode('.', $ImgThumbNm);
            $coverExt = $coverExtArry[1];
            $cover_name = config('constants.vendor_images_pre_name').'event-cover-'.$record->event_slug;
            if(VendorEventImage::where('image_type', 'cover')->where('image_full', 'like', $cover_name.'.'.$coverExt)->exists()) {
                $countCImg = VendorEventImage::where('image_type', 'cover')->where('image_full', 'like', '%'.$cover_name.'%')->count();
                // 2. update slug by adding -2
                $cover_name .= "-".($countCImg+1);
            }
            $cover_name .= ".".$coverExt;
            $original_cover = 'vendor_photos/original/'.$ImageCover->image_full;
            $thumb_cover = 'vendor_photos/thumb_cropped/'.$ImageCover->image_thumb;
            Storage::disk('s3')->rename($original_cover, 'vendor_photos/original/' . $cover_name);
            Storage::disk('s3')->rename($thumb_cover, 'vendor_photos/thumb_cropped/' . $cover_name);
        }
        // 1.2 Update event logo
        if($profilImg != null) {
            $PfThumbNm = $profilImg->image_thumb;
            $logoExtArry = explode('.', $PfThumbNm);
            $logoExt = $logoExtArry[1];
            $logo_name = config('constants.vendor_images_pre_name').'event-logo-'.$record->event_slug;
            if(VendorEventImage::where('image_type', 'profile')->where('image_full', 'like', $logo_name.'.'.$logoExt)->exists()) {
                $countLgImg = VendorEventImage::where('image_type', 'profile')->where('image_full', 'like', '%'.$logo_name.'%')->count();
                // 2. update slug by adding -2
                $logo_name .= "-".($countLgImg+1);
            }
            $logo_name .= ".".$logoExt;
            $original_logo = 'vendor_photos/original/'.$profilImg->image_full;
            $thumb_logo = 'vendor_photos/thumb_cropped/'.$profilImg->image_thumb;
            Storage::disk('s3')->rename($original_logo, 'vendor_photos/original/' . $logo_name);
            Storage::disk('s3')->rename($thumb_logo, 'vendor_photos/thumb_cropped/' . $logo_name);
        }
        // add event poster
        if($ImagePoster != null) {
            VendorEventImage::create([
                'event_id' => $record->id,
                'image_thumb' => $poster_name,
                'image_full'=> $poster_name,
                'is_profile_image' => 'Y',
                'image_type' => 'poster'
            ]);
        }
        // 2. add to vendors images
        if($ImageCover != null) {
            VendorEventImage::create([
                'event_id' => $record->id,
                'image_thumb' => $cover_name,
                'image_full'=> $cover_name,
                'is_profile_image' => 'Y',
                'image_type' => 'cover'
            ]);
        }
        if($profilImg != null) {
            VendorEventImage::create([
                'event_id' => $record->id,
                'image_thumb' => $logo_name,
                'image_full'=> $logo_name,
                'is_profile_image' => 'Y',
                'image_type' => 'profile'
            ]);
        }
        // 3. remove temporary image record
        TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->delete();
        TemporaryEventVendorsImage::where('image_id', $pimage_id)->where('image_type', 'profile')->delete();
        TemporaryEventVendorsImage::where('image_id', $postimage_id)->where('image_type', 'poster')->delete();

        return $record;
    }

    public function updateRecord($request)
    {
        $online_seat_selection = $request->has('online_seat_selection') && trim($request->input('online_seat_selection')) != "" && trim($request->input('online_seat_selection')) == "1" ? 1 : 0;
        $parking = $request->has('parking') && trim($request->input('parking')) != "" && trim($request->input('parking')) == "1" ? 1 : 0;
        $food_for_sale = $request->has('food_for_sale') && trim($request->input('food_for_sale')) != "" && trim($request->input('food_for_sale')) == "1" ? 1 : 0;
        $drinks_for_sale = $request->has('drinks_for_sale') && trim($request->input('drinks_for_sale')) != "" && trim($request->input('drinks_for_sale')) == "1" ? 1 : 0;
        $babysitting_services = $request->has('babysitting_services') && trim($request->input('babysitting_services')) != "" && trim($request->input('babysitting_services')) == "1" ? 1 : 0;
        
        // artists
        $artists = $request->has('artists') ? array_unique($request->input('artists')) : "";
        $artist_insert = "";
        if($artists != '') {
            // check out the submitted artists already exists
            if(count($artists) > 0) {
                foreach($artists as $artist) {
                    if($artist != null && $artist != '') {
                        if(Artist::where('name', 'like', $artist)->exists()) {
                            $artistObj = Artist::where('name', 'like', $artist)->first(['id']);
                            $artist_insert .= $artistObj->id.",";
                        } else {
                            $artistObj = Artist::create([
                                'name' => $artist,
                                'vendor_id' => Auth::id(),
                                'site_id' => config('constants.site_id')
                            ]);
                            $artist_insert .= $artistObj->id.",";
                        }
                    }
                }
            }
        }
        // remove last comma from artists insert
        if($artist_insert != '') {
            $artist_insert = rtrim($artist_insert, ',');
        }

        $record = VendorEventInformation::withEncryptedId($request->input('event_id'))->first();
        $event_slug = slugifyText($request->input('event_name'));
        if(VendorEventInformation::where('event_slug', $event_slug)->where('id', '<>', $record->id)->exists()) {
            $slgName = VendorEventInformation::where('event_slug', 'like', '%'.$event_slug.'%')->where('id', '<>', $record->id)->count();
            // 2. update slug by adding -2
            $event_slug .= "-".($slgName+1);
        }

        // check if admin then allow to update date and time
        $admin_user = Auth::user();
        if($admin_user->hasRole('admin')) {
            // events dates
            $expld_date = explode('.', $request->input('start_date'));
            $start_date = $expld_date[2].'-'.$expld_date[1].'-'.$expld_date[0];
            // validate
            $start_date_check = strtotime(date('Y-m-d', strtotime($start_date) ) );
            $todays = strtotime(date('Y-m-d'));

            $end_expld_date = explode('.', $request->input('end_date'));
            $end_date = $end_expld_date[2].'-'.$end_expld_date[1].'-'.$end_expld_date[0];
            $end_date_check = strtotime(date('Y-m-d', strtotime($end_date) ) );

            if($start_date_check < $todays || $end_date_check < $todays) {
                return back()->withErrors('Event\'s dates can not be past.');
            }

            if($start_date_check > $end_date_check) {
                return back()->withErrors('Event\'s start date and end date are not proper, start date must less than or same as end date.');
            }

            // validate the start time and end time if date is same
            if($request->start_time != "" && $request->end_time != "") {
                if(strtotime(date('Y-m-d h:i A', strtotime($start_date.' '.$request->start_time))) > strtotime(date('Y-m-d h:i A', strtotime($end_date.' '.$request->end_time)))) {
                    return back()->withErrors('Event\'s start time and end time are not proper, end time must greater than start time.');
                }
            }

            $st_date = strtotime(date('d.m.Y h:i A', strtotime($request->start_date.' '.$request->start_time)));
            $start_date = date('Y-m-d H:i:s', $st_date);
                
            $ed_date = strtotime(date('d.m.Y h:i A', strtotime($request->end_date.' '.$request->end_time)));
            $end_date = date('Y-m-d H:i:s', $ed_date);

            $record->start_date = $start_date;
            $record->end_date = $end_date;
        }

        $record->event_name = $request->input('event_name');
        $record->venue = $request->input('venue');
        $record->artists = $artist_insert;
        $record->event_slug = $event_slug;
        $record->address = $request->input('address');
        $record->country_id = $request->input('country');
        $record->state_id = $request->input('state');
        $record->city_id = $request->input('city');
        $record->zip = $request->input('zip');
        $record->description = $request->input('description');
        $record->ticket_information = $request->input('ticket_information');
        $record->categories = serialize($request->input('categories'));
        $record->ticket_url = $request->input('ticket_url');
        $record->meta_keywords = $request->input('meta_keywords');
        $record->video_url = $request->input('video_url');
        $record->online_seat_selection = $online_seat_selection;
        $record->parking = $parking;
        $record->food_for_sale = $food_for_sale;
        $record->drinks_for_sale = $drinks_for_sale;
        $record->babysitting_services = $babysitting_services;
        $record->business_website = $request->input('business_website');
        $record->facebook = $request->input('facebook');
        $record->instagram = $request->input('instagram');
        $record->pinterest = $request->input('pinterest');
        $record->twitter = $request->input('twitter');
        $record->linkedin = $request->input('linkedin');
        $record->save();
    }

    public function searchEvents($search_preference)
    {
        $per_page = 6;

        if(Auth::check()) {
            // get user IP
            $userObj = User::where('id', Auth::id())->first(['id', 'signup_ip_address']);
            if($userObj->signup_ip_address != '') {
                $ip_address = $userObj->signup_ip_address;
                
            } else {
                $ip_address = request()->ip();
            }
        } else {
            // get guest IP
            $ip_address = request()->ip();
            //$ip_address = '43.250.158.190'; // for local
        }
        if($ip_address == '::1' || $ip_address == '127.0.0.1' || $ip_address == '192.168.0.104') {
            $ip_address = '43.250.158.190'; // for local
        }

        $county_info = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$ip_address.'?key=RocDHDkeHbv0ask'));

        if(!empty($county_info) && $search_preference['city_zip'] == '') {
            $countryInfo = Country::where('code', '=', $county_info->countryCode)->first(['id']);
            return $results = VendorEventInformation::select('vendors_events_information.*', DB::raw('(CASE WHEN country_id = ' . $countryInfo->id . ' THEN 1 ELSE 
            CASE WHEN country_id = 202 THEN 2 ELSE 
                CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
                END   
            END) AS newCountryid'))
                    ->getEventsResults($search_preference)
                    // ->orderBy('vendors_events_information.is_paid', 'DESC')
                    ->orderBy('newCountryid','asc')
                    ->orderBy('created_at', 'asc')
                    ->paginate($per_page);
        } else {
            return $results = VendorEventInformation::select('vendors_events_information.*')
                    ->getEventsResults($search_preference)
                    // ->orderBy('vendors_events_information.is_paid', 'DESC')
                    ->orderBy('created_at', 'asc')
                    ->paginate($per_page);
        }
    }

    public function checkEventIsActive($encEventId)
    {
        return VendorEventInformation::withEncryptedId($encEventId)
            ->hasVendorRole()->isActive()
            ->where('is_approved', 1)
            ->where('site_id', $this->site_id)
            ->exists();
    }

    public function getEvent($encEventId)
    {
        return VendorEventInformation::withEncryptedId($encEventId)
            ->hasVendorRole()->isActive()
            ->where('is_approved', 1)
            ->where('site_id', $this->site_id)
            ->first();
    }

    public function getLoggedInEvents()
    {
        return VendorEventInformation::where('site_id', $this->site_id)
        ->where('vendor_id', Auth::id())
        ->orderBy('created_at', 'asc');
    }

    public function checkEventOwner($eventId)
    {
        return VendorEventInformation::withEncryptedId($eventId)
            ->where('site_id', $this->site_id)
            ->where('vendor_id', Auth::id())
            ->exists(); 
    }

    public function getEventRecord($eventId)
    {
        return VendorEventInformation::withEncryptedId($eventId)
            ->where('site_id', $this->site_id)
            ->where('vendor_id', Auth::id())
            ->first(); 
    }

    // get event by encid or slug
    public function getEventBy($field, $value, $self = false)
    {
        if($self == false) {
            $eventObj = VendorEventInformation::hasActiveEventVendor();
        } else {
            $eventObj = VendorEventInformation::where('vendor_id', '<>', '');
        }
        if($field == 'enc_id') {
            $eventObj->withEncryptedId($value);
        } else {
            $eventObj->where($field, $value);
        }
        if(Auth::check()) {
            if($self == false) {
                $eventObj->where('is_approved', 1);
            }
        } else {
            $eventObj->where('is_approved', 1);
        }
            return $eventObj->where('site_id', $this->site_id)
            ->first();
    }

    // get similar events
    public function getSimilarEvents($vendor_id, $categories)
    {
        if(Auth::check()) {
            // get user IP
            $userObj = User::where('id', Auth::id())->first(['id', 'signup_ip_address']);
            if($userObj->signup_ip_address != '') {
                $ip_address = $userObj->signup_ip_address;
                
            } else {
                $ip_address = request()->ip();
            }
        } else {
            // get guest IP
            $ip_address = request()->ip();
            //$ip_address = '43.250.158.190'; // for local
        }
        if($ip_address == '::1' || $ip_address == '127.0.0.1' || $ip_address == '192.168.0.104') {
            $ip_address = '43.250.158.190'; // for local
        }

        $county_info = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$ip_address.'?key=RocDHDkeHbv0ask'));
        if(!empty($county_info)) {
            $countryInfo = Country::where('code', '=', $county_info->countryCode)->first(['id']);
            return VendorEventInformation::select('vendors_events_information.*', DB::raw('(CASE WHEN country_id = ' . $countryInfo->id . ' THEN 1 ELSE 
            CASE WHEN country_id = 202 THEN 2 ELSE 
                CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
                END   
            END) AS newCountryid'))->getSimilarResults($categories)
        ->where('vendor_id', '<>', $vendor_id);
        } else {
            return VendorEventInformation::getSimilarResults($categories)
        ->where('vendor_id', '<>', $vendor_id);
        }
    }

    public function checkEventOwnerBySlug($slug)
    {
        return VendorEventInformation::where('event_slug', $slug)
            ->where('site_id', $this->site_id)
            ->where('vendor_id', Auth::id())
            ->exists(); 
    }

    public function getRecords($ids = '')
    {
        if($ids != "") {
            return VendorEventInformation::find($ids);
        } else {
            return VendorEventInformation::where('site_id', $this->site_id);
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }

    public function getStatusRecords($is_approved = '')
    {
        return VendorEventInformation::where('site_id', $this->site_id)->where('is_approved', '=', $is_approved);
    }

    public function deleteRecord($record)
    {
        // remove the related content like poster/logo/banner from s3
        $Images = VendorEventImage::where('event_id', $record->id)->get();
        // delete event reviews
        EventReview::where('event_id', $record->id)->delete();

        if($Images->count() > 0) {
            foreach($Images as $Img) {
                // 2. delete from s3
                // remove old poster image
                if(Storage::disk('s3')->exists('vendor_photos/original/' . $Img->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/original/' . $Img->image_full);
                }

                if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $Img->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $Img->image_full);
                }
                // 3. delete record
                $Img->delete();
            }
        }
        // delete faq
        $record->delete();
    }

    public function getAdminEventRecord($id)
    {
        return VendorEventInformation::where('id', $id)
            ->where('site_id', $this->site_id)
            ->first(); 
    }

    // get three recent upcoming events banner
    public function getReventEventsBanner($number = 3, $countryId = '')
    {
        $search_preference = ['city_zip' => '', 'category_id' => '', 'category_name' => '', 'location_type' => '', 'location_id' => ''];
        $countySelected = false;
        if($countryId != '') {
            // get country information by id
            if(Country::where('id', '=', $countryId)->exists()) {
                $countySelected = true;
            }
        }
        if($countySelected == true) {
            return $results = VendorEventInformation::select('vendors_events_information.*', DB::raw('(CASE WHEN country_id = ' . $countryId . ' THEN 1 ELSE 
            CASE WHEN country_id = 202 THEN 2 ELSE 
                CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
                END   
            END) AS newCountryid'))
                    ->getEventsResults($search_preference)
                    ->vendorsWithBanners()
                    ->orderBy('newCountryid','asc')
                    ->orderBy('created_at', 'asc')
                    ->paginate($number);
        }
        if(Auth::check()) {
            // get user IP
            $userObj = User::where('id', Auth::id())->first(['id', 'signup_ip_address']);
            if($userObj->signup_ip_address != '') {
                $ip_address = $userObj->signup_ip_address;
                
            } else {
                $ip_address = request()->ip();
            }
        } else {
            // get guest IP
            $ip_address = request()->ip();
            //$ip_address = '43.250.158.190'; // for local
        }
        if($ip_address == '::1' || $ip_address == '127.0.0.1' || $ip_address == '192.168.0.104') {
            $ip_address = '43.250.158.190'; // for local
        }

        $county_info = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$ip_address.'?key=RocDHDkeHbv0ask'));
        
        if(!empty($county_info)) {
            $countryInfo = Country::where('code', '=', $county_info->countryCode)->first(['id']);
            return $results = VendorEventInformation::select('vendors_events_information.*', DB::raw('(CASE WHEN country_id = ' . $countryInfo->id . ' THEN 1 ELSE 
            CASE WHEN country_id = 202 THEN 2 ELSE 
                CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
                END   
            END) AS newCountryid'))
                    ->getEventsResults($search_preference)
                    ->vendorsWithBanners()
                    ->orderBy('newCountryid','asc')
                    ->orderBy('created_at', 'asc')
                    ->paginate($number);
        } else {
            return $results = VendorEventInformation::select('vendors_events_information.*')
                    ->getEventsResults($search_preference)
                    ->vendorsWithBanners()
                    ->orderBy('created_at', 'asc')
                    ->paginate($number);
        }
    }

    // get three recent upcoming events banner
    public function getFeaturedEvents($number = 12, $countryId = '')
    {
        $search_preference = ['city_zip' => '', 'category_id' => '', 'category_name' => '', 'location_type' => '', 'location_id' => ''];
        $countySelected = false;
        if($countryId != '') {
            // get country information by id
            if(Country::where('id', '=', $countryId)->exists()) {
                $countySelected = true;
            }
        }
        if($countySelected == true) {
            return $results = VendorEventInformation::select('vendors_events_information.*', DB::raw('(CASE WHEN country_id = ' . $countryId . ' THEN 1 ELSE 
            CASE WHEN country_id = 202 THEN 2 ELSE 
                CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
                END   
            END) AS newCountryid'))
                    ->getEventsResults($search_preference)
                    ->orderBy('newCountryid','asc')
                    ->orderBy('created_at', 'asc')
                    ->paginate($number);
        }
        if(Auth::check()) {
            // get user IP
            $userObj = User::where('id', Auth::id())->first(['id', 'signup_ip_address']);
            if($userObj->signup_ip_address != '') {
                $ip_address = $userObj->signup_ip_address;
                
            } else {
                $ip_address = request()->ip();
            }
        } else {
            // get guest IP
            $ip_address = request()->ip();
            //$ip_address = '43.250.158.190'; // for local
        }
        if($ip_address == '::1' || $ip_address == '127.0.0.1' || $ip_address == '192.168.0.104') {
            $ip_address = '43.250.158.190'; // for local
        }

        $county_info = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$ip_address.'?key=RocDHDkeHbv0ask'));
        
        if(!empty($county_info)) {
            $countryInfo = Country::where('code', '=', $county_info->countryCode)->first(['id']);
            return $results = VendorEventInformation::select('vendors_events_information.*', DB::raw('(CASE WHEN country_id = ' . $countryInfo->id . ' THEN 1 ELSE 
            CASE WHEN country_id = 202 THEN 2 ELSE 
                CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
                END   
            END) AS newCountryid'))
                    ->getEventsResults($search_preference)
                    ->orderBy('newCountryid','asc')
                    ->orderBy('created_at', 'asc')
                    ->paginate($number);
        } else {
            return $results = VendorEventInformation::select('vendors_events_information.*')
                    ->getEventsResults($search_preference)
                    ->orderBy('created_at', 'asc')
                    ->paginate($number);
        }
    }

    // get featured events home page videos
    public function getHomeEventsVids($number = 1000)
    {
        $search_preference = ['city_zip' => '', 'category_id' => '', 'category_name' => '', 'location_type' => '', 'location_id' => ''];
        
        if(Auth::check()) {
            // get user IP
            $userObj = User::where('id', Auth::id())->first(['id', 'signup_ip_address']);
            if($userObj->signup_ip_address != '') {
                $ip_address = $userObj->signup_ip_address;
                
            } else {
                $ip_address = request()->ip();
            }
        } else {
            // get guest IP
            $ip_address = request()->ip();
            //$ip_address = '43.250.158.190'; // for local
        }
        if($ip_address == '::1' || $ip_address == '127.0.0.1' || $ip_address == '192.168.0.104') {
            $ip_address = '43.250.158.190'; // for local
        }

        $county_info = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$ip_address.'?key=RocDHDkeHbv0ask'));
        
        if(!empty($county_info)) {
            $countryInfo = Country::where('code', '=', $county_info->countryCode)->first(['id']);
            return $results = VendorEventInformation::select('vendors_events_information.*', DB::raw('(CASE WHEN country_id = ' . $countryInfo->id . ' THEN 1 ELSE 
            CASE WHEN country_id = 202 THEN 2 ELSE 
                CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
                END   
            END) AS newCountryid'))
                    ->getEventsResults($search_preference)
                    ->where('video_url', '<>', '')
                    ->orderBy('newCountryid','asc')
                    ->orderBy('created_at', 'asc')
                    ->paginate($number);
        } else {
            return $results = VendorEventInformation::select('vendors_events_information.*')
                    ->getEventsResults($search_preference)
                    ->where('video_url', '<>', '')
                    ->orderBy('created_at', 'asc')
                    ->paginate($number);
        }
    }
}