<?php
namespace App\Logic\Template;

use App\Models\NewsLetterEmailSmsTemplate;
use App\Models\EmailSubject;

class TemplateRepository
{
    public function getRecords($ids = '', $type = '')
    {
        if($ids != "") {
            return NewsLetterEmailSmsTemplate::find($ids);
        }
        if($type != "") {
            return NewsLetterEmailSmsTemplate::where('type', '=', $type)->where('site_id', '=', config('constants.site_id'));
        }
    }

    public function getSearchRecords($templates, $search = '', $type = '')
    {
        if($type == 'email') {
            return $templates->getEmailRecordsSearch($search);
        } else {
            return $templates->getRecordsSearch($search);
        }
    }

    public function getStatusRecords($type = '', $status = '')
    {
        return NewsLetterEmailSmsTemplate::where('type', '=', $type)
                            ->where('site_id', '=', config('constants.site_id'))
                            ->where('status', '=', $status);
    }

    public function getTemplateBy($name = '', $type = '')
    {
        return NewsLetterEmailSmsTemplate::where('name', '=', $name)->where('type', '=', $type)->where('site_id', '=', config('constants.site_id'));
    }

    public function getEmailSubject($id)
    {
        return EmailSubject::where('template_id', '=', $id)->first(['subject']);
    }
}