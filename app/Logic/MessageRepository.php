<?php
/**
 * Created by PhpStorm.
 * User: SNV Wedding Gallery
 * Date: 2018-11-09
 * Time: 3:11 PM
 */

namespace App\Logic;

use App\Models\Message;
use App\Models\MessageFilter;
use App\Models\MessageFilteredUser;
use App\Models\User;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageRepository {
    public function __construct()
    {
    }


    public function checkIfAlreadySentLimitMsg($receiver_id) {

        if(Auth::user()){
            $authAur=Auth::user();
        } else {
            $authAur=Auth::guard('api')->user();
        }

        $userId = $authAur->id;
        $todayMsg=Message::select(DB::raw('group_concat(distinct receiver_id) as todayId'))
            ->where('sender_id',$userId)
            ->where('msg_type','1')
            ->whereRaw('Date(created_at) = CURDATE()')->get();

        $oldMsg=Message::select(DB::raw('group_concat(distinct receiver_id) as oldId'))
            ->where('sender_id',$userId)
            ->where('msg_type','1')
            ->whereRaw('Date(created_at) < CURDATE()')->get();

        $todayIdArr = explode(",",$todayMsg[0]->todayId);
        $oldIdArr = explode(",",$oldMsg[0]->oldId);

        // echo "/n Todays Unique ";
        $final=array_diff($todayIdArr,$oldIdArr);
        // print_r($final);
        //return count($final);

        $getMembershipStatus=$authAur->currentMembershipStatus();
        $message_allowed = $getMembershipStatus['message_allowed'];
        $todaysMsgCnt=count($final);
        //die;
        $canSendFlag='0';

//        echo "Today's Array: ";
//        print_r($todayIdArr);
//        echo "Old Array: ";
//        print_r($oldIdArr);
//        echo "Final Filtered";
//        print_r($final);
//        echo "<br> Todays unique: ". $todaysMsgCnt;
//        echo "<br> Messages Allowed: ". $message_allowed;

        if($todaysMsgCnt<$message_allowed || in_array($receiver_id,$todayIdArr) || in_array($receiver_id,$oldIdArr)) {
            $canSendFlag = '1';
        }

        //echo "<br> Cansendflag: ".$canSendFlag;
        //die;
        return array('message_allowed'=>$message_allowed,'canSendFlag'=>$canSendFlag);
    }

    ///////////////////////////Inbox tab starts here /////////////////////////////////
    public function getInbox($request,$showunread=''){
        $user=Auth::user();
        $user_id=$user->id;
        $filteredUsers=array(); //$user->getfilteredSender();

        $pageNo=1;
        $pagintaionEnabled = config('usersmanagement.enableMsgPagination');
        if($pagintaionEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }

            if($showunread=='') {
                $fetchGroupedMessages = Message::select(DB::raw('sender_id, count(IF(read_status = \'N\', 1, NULL)) as unread_count, max(created_at) as createdAt'))
                    ->groupby('sender_id')
                    ->whereNotIn('sender_id',$filteredUsers)
                    ->where('receiver_id', $user_id)->where('receiver_trash', 'N')->where('receiver_delete', 'N')->where('receiver_archive', 'N')->orderBy('created_at', 'DESC')->paginate(config('usersmanagement.msgFrontEndSize'));
            } elseif($showunread=='unread') {
                $fetchGroupedMessages = Message::select(DB::raw('sender_id, count(IF(read_status = \'N\', 1, NULL)) as unread_count, max(created_at) as createdAt'))
                    ->groupby('sender_id')->havingRaw('count(IF(read_status = \'N\', 1, NULL))  > 0')
                    ->whereNotIn('sender_id',$filteredUsers)
                    ->where('receiver_id', $user_id)->where('receiver_trash', 'N')->where('receiver_delete', 'N')->where('receiver_archive', 'N')->orderBy('created_at', 'DESC')->paginate(config('usersmanagement.msgFrontEndSize'));
            } else {
                return abort(404);
            }
            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.msgFrontEndSize')) +1;
        }

        $returnVar['fetchInbox']=$fetchGroupedMessages;
        $returnVar['pageNo']=$pageNo;
        return $returnVar;
    }

    public function getLastConversationRec($sender_id) {
        $user_id=Auth::id();
        if($sender_id!='') {
            $fetchconversation=Message::
            //where('receiver_trash','N')
            //->where('receiver_delete','N')
            where(function($query) use ($sender_id,$user_id){
                return $query
                    ->where(['sender_id'=>$sender_id, 'receiver_id'=>$user_id, 'receiver_trash'=>'N', 'receiver_delete'=>'N'])   //'receiver_archive'=>'N',
                    ->orWhere(function($query1) use ($sender_id,$user_id){
                        return $query1
                            ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id, 'sender_trash'=>'N', 'sender_delete'=>'N']);  //'sender_archive'=>'N',
                    });
            })
                ->orderBy('created_at','desc')->limit(1)->get();
        }
        return $fetchconversation;
    }

    public function getLastSentConversationRec($sender_id) {
        $user_id=Auth::id();
        if($sender_id!='') {
            $fetchconversation=Message::where(function($query) use ($sender_id,$user_id){
                return $query
                    ->where(['sender_id'=>$sender_id, 'receiver_id'=>$user_id, 'sender_trash'=>'N', 'sender_delete'=>'N'])   //'receiver_archive'=>'N',
                    ->orWhere(function($query1) use ($sender_id,$user_id){
                        return $query1->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id, 'receiver_trash'=>'N', 'receiver_delete'=>'N']);  //'sender_archive'=>'N',
                    });
            })
                ->orderBy('created_at','desc')->limit(1)->get();
        }
        return $fetchconversation;
    }

    public function getMessageConversation($sender_id){
        $user_id=Auth::id();
        if($sender_id!='') {
            $fetchconversation=Message::
            //where('receiver_trash','N')
            //->where('receiver_delete','N')
            where(function($query) use ($sender_id,$user_id){
                return $query
                    ->where(['sender_id'=>$sender_id, 'receiver_id'=>$user_id, 'receiver_trash'=>'N', 'receiver_delete'=>'N'])   //'receiver_archive'=>'N',
                    ->orWhere(function($query1) use ($sender_id,$user_id){
                        return $query1
                            ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id, 'sender_trash'=>'N', 'sender_delete'=>'N']);  //'sender_archive'=>'N',
                    });
            })
                ->orderBy('created_at','asc')->get();
        }
        return $fetchconversation;
    }

    public function sendMessageToArchive($sender_id) {
        $user_id=Auth::id();
        if($sender_id!='') {
            Message::where('sender_trash','N')
                ->where('sender_delete','N')
                ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id])
                ->update(['sender_archive' =>'Y']);

            Message::where('receiver_trash','N')
                ->where('receiver_delete','N')
                ->where(['receiver_id'=>$user_id, 'sender_id'=>$sender_id])
                ->update(['receiver_archive' =>'Y']);
        }
    }

    public function sendMessageToInbox($sender_id) {
        $user_id=Auth::id();
        if($sender_id!='') {
            Message::where('sender_trash','N')
                ->where('sender_delete','N')
                ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id])
                ->update(['sender_archive' =>'N']);

            Message::where('receiver_trash','N')
                ->where('receiver_delete','N')
                ->where(['receiver_id'=>$user_id, 'sender_id'=>$sender_id])
                ->update(['receiver_archive' =>'N']);
        }
    }

    public function sendMessageToTrash($sender_id, $sendTrash) {
        $user_id=Auth::id();
        $my_archive = ($sendTrash=='inbox' || $sendTrash=='filter')?'N':'Y';
        if($sender_id!='') {
            $query = Message::where('sender_trash', 'N')
                ->where('sender_delete', 'N');

            if ($my_archive == 'Y') {
                $query->where('sender_archive', $my_archive);
            }

            $query->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id])
                ->update(['sender_trash'=>'Y']);

            $query1 = Message::where('receiver_trash','N')
                ->where('receiver_delete','N');

            if ($my_archive == 'Y') {
                $query1->where('receiver_archive', $my_archive);
            }

            $query1->where(['receiver_id'=>$user_id, 'sender_id'=>$sender_id])
                ->update(['receiver_trash' =>'Y']);

            if($sendTrash=='filter') {
                MessageFilteredUser::where('user_id', $user_id)->where('filtered_id',$sender_id)->delete();
            }
        }
    }
///////////////////////////Inbox tab ends here /////////////////////////////////

///////////////////////////Sent tab starts here /////////////////////////////////
    public function getSent($request,$showunread=''){
        $user=Auth::user();
        $user_id=$user->id;
        $filteredUsers=array(); //$user->getfilteredSender();

        $pageNo=1;
        $pagintaionEnabled = config('usersmanagement.enableMsgPagination');
        if($pagintaionEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }

            $fetchGroupedMessages = Message::select(DB::raw('receiver_id, count(IF(read_status = \'N\', 1, NULL)) as unread_count, max(created_at) as createdAt'))
                ->groupby('receiver_id')
                ->whereNotIn('receiver_id',$filteredUsers)
                ->where('sender_id', $user_id)->where('sender_trash', 'N')->where('sender_delete', 'N')->where('sender_archive', 'N')->orderBy('created_at', 'DESC')->paginate(config('usersmanagement.msgFrontEndSize'));

            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.msgFrontEndSize')) +1;
        }

        $returnVar['fetchInbox']=$fetchGroupedMessages;
        $returnVar['pageNo']=$pageNo;
        return $returnVar;
    }
///////////////////////////Sent tab ends here /////////////////////////////////

///////////////////////////Filter tab starts here /////////////////////////////////

    public function getUniqueSender($search_preference) {
        $user_id=Auth::id();
        $uniqueSender=Message::select('sender_id')->distinct()->where(['receiver_id'=>$user_id])->get()->toArray();
        $senderArr=array();
        foreach ($uniqueSender as $data) {
            array_push($senderArr,$data['sender_id']);
        }

//        echo implode(',',$senderArr);
        $FinalArr = User::select('id')
            ->hasUserRole()
            ->isCompletedProfile()
            ->getMessageFilterUsr($search_preference)
            ->whereIn('id', $senderArr)
            ->get()->toArray();

//        print_r($FinalArr);
        if(count($FinalArr)>0) {
            foreach ($FinalArr as $datar) {
                MessageFilteredUser::manageFilteredUsers($user_id,$datar['id']);
            }
        }
        //return $uniqueSender;
    }

    public function sendMsgApplyFilter($receiver_id) {
        $sender_id=Auth::id();
        $msgFilter=MessageFilter::where('user_id',$receiver_id)->first();

        if($msgFilter) {
            if($msgFilter['status']=="1") {
                $FinalArr = User::select('id')
                    ->hasUserRole()
                    ->getMessageFilterUsr($msgFilter)
                    ->where('id', $sender_id)
                    ->count();

                if ($FinalArr > 0) {
                    MessageFilteredUser::manageFilteredUsers($receiver_id, $sender_id);
                }
            }
        }
    }

    public function getFilter($request){
        $user=Auth::user();
        $user_id=$user->id;
        $filteredUsers=array(); //$user->getfilteredSender();

        $pageNo=1;
        $pagintaionEnabled = config('usersmanagement.enableMsgPagination');
        if($pagintaionEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }

            $fetchGroupedMessages = Message::select(DB::raw('sender_id, count(IF(read_status = \'N\', 1, NULL)) as unread_count, max(created_at) as createdAt'))
                ->groupby('sender_id')
                ->where('receiver_id', $user_id)
                ->where('receiver_trash', 'N')
                ->where('receiver_delete', 'N')
                ->whereIn('sender_id',$filteredUsers)
                ->orderBy('created_at', 'DESC')
                ->paginate(config('usersmanagement.msgFrontEndSize'));

            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.msgFrontEndSize')) +1;
        }

        $returnVar['fetchInbox']=$fetchGroupedMessages;
        $returnVar['pageNo']=$pageNo;
        return $returnVar;
    }
///////////////////////////Filter tab ends here /////////////////////////////////


///////////////////////////Archive tab starts here ////////////////////////////
    public function getArchive($request){

        $user=Auth::user();
        $user_id=$user->id;
        $filteredUsers=array(); //$user->getfilteredSender();



        $pageNo=1;
        $paginationEnabled = config('usersmanagement.enableMsgPagination');
        if($paginationEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }

            $fetchGroupedMessages = Message::select(DB::raw('sender_id, count(IF(read_status = \'N\', 1, NULL)) as unread_count, max(created_at) as createdAt'))
                ->groupby('sender_id')
                ->where('receiver_id', $user_id)
                ->where('receiver_trash', 'N')
                ->where('receiver_delete', 'N')
                ->where('receiver_archive', 'Y')
                ->whereNotIn('sender_id',$filteredUsers)
                ->orderBy('created_at', 'DESC')
                ->paginate(config('usersmanagement.msgFrontEndSize'));

            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.msgFrontEndSize')) +1;
        }

        $returnVar['fetchInbox']=$fetchGroupedMessages;
        $returnVar['pageNo']=$pageNo;
        return $returnVar;
    }

    public function getArchiveConversation($sender_id){
        $user_id=Auth::id();
        if($sender_id!='') {
            $fetchconversation=Message::
            where(function($query) use ($sender_id,$user_id){
                return $query
                    ->where(['sender_id'=>$sender_id, 'receiver_id'=>$user_id,  'receiver_trash'=>'N', 'receiver_delete'=>'N'])
                    ->orWhere(function($query1) use ($sender_id,$user_id){
                        return $query1
                            ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id,  'sender_trash'=>'N', 'sender_delete'=>'N']);
                    });
            })
                ->orderBy('created_at','asc')->get();
        }
        return $fetchconversation;
    }

///////////////////////////Archive tab ends here //////////////////////////////

///////////////////////////Trash tab starts here ////////////////////////////
    public function getTrash($request){
        $user_id=Auth::id();
        $pageNo=1;
        $paginationEnabled = config('usersmanagement.enableMsgPagination');
        if($paginationEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }

//            $fetchGroupedMessages = Message::select(DB::raw("sender_id, max(created_at) as createdAt,  if(receiver_id='".$user_id."', sender_id,receiver_id) as userId"))
//                ->groupby(DB::raw("if(receiver_id='".$user_id."', sender_id,receiver_id)"))
//                ->where(['sender_id'=>$user_id, 'receiver_id'=>userId, 'sender_trash'=>'Y'])
//                ->orWhere(['receiver_id'=>$user_id, 'sender_id'=>userId, 'receiver_trash'=>'Y'])
//                ->paginate(config('usersmanagement.msgFrontEndSize'));

            $fetchGroupedMessages = DB::query()->fromSub(function ($query) use($user_id) {
                $query->from('messages')->select(DB::raw("id, sender_id, receiver_id, receiver_trash, sender_trash, created_at, if(receiver_id='".$user_id."', sender_id,receiver_id) as userId"))
                    ->where(['sender_id'=>$user_id])
                    ->orWhere(['receiver_id'=>$user_id]);
            }, 'a')
                ->select(DB::raw("sender_id, max(created_at) as createdAt,  userId"))
                ->groupby('userId')
                ->where(['sender_id'=>$user_id, 'receiver_id'=>\DB::raw('userId'), 'sender_trash'=>'Y'])
                ->orWhere(function($query1) use ($user_id){
                    return $query1
                        ->where(['receiver_id'=>$user_id, 'sender_id'=>\DB::raw('userId'), 'receiver_trash'=>'Y']);
                })
                ->orderBy('createdAt','desc')
//                ->get();
                ->paginate(config('usersmanagement.msgFrontEndSize'));

            /*SELECT if(receiver_id='24', sender_id,receiver_id) as userId, max(IF(receiver_id='24', created_at, NULL) ) as createdAt
            FROM `messages`
            WHERE (receiver_id='24' and receiver_trash='Y') or (sender_id='24' and sender_trash='Y')
            group by userId */
            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.msgFrontEndSize')) +1;
        }

        $returnVar['fetchInbox']=$fetchGroupedMessages;
        $returnVar['pageNo']=$pageNo;
        return $returnVar;
    }

    public function getTrashConversation($sender_id){
        $user_id=Auth::id();
        if($sender_id!='') {
            $fetchconversation=Message::
            where(function($query) use ($sender_id,$user_id){
                return $query
                    ->where(['sender_id'=>$sender_id, 'receiver_id'=>$user_id, 'receiver_trash'=>'Y'])
                    ->orWhere(function($query1) use ($sender_id,$user_id){
                        return $query1
                            ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id, 'sender_trash'=>'Y']);
                    });
            })
                ->orderBy('created_at','asc')->get();
        }
        return $fetchconversation;
    }


    public function moveTrashToInbox($sender_id) {
        $user_id=Auth::id();
        if($sender_id!='') {
            Message::where('sender_trash','Y')
                ->where('sender_delete','N')
                ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id])
                ->update(['sender_trash' =>'N', 'sender_archive' =>'N']);

            Message::where('receiver_trash','Y')
                ->where('receiver_delete','N')
                ->where(['receiver_id'=>$user_id, 'sender_id'=>$sender_id])
                ->update(['receiver_trash'=>'N', 'receiver_archive' =>'N']);
        }
    }

    public function moveTrashToArchive($sender_id) {
        $user_id=Auth::id();
        if($sender_id!='') {
            Message::where('sender_trash','Y')
                ->where('sender_delete','N')
                ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id])
                ->update(['sender_trash' =>'N', 'sender_archive' =>'Y']);

            Message::where('receiver_trash','Y')
                ->where('receiver_delete','N')
                ->where(['receiver_id'=>$user_id, 'sender_id'=>$sender_id])
                ->update(['receiver_trash'=>'N', 'receiver_archive' =>'Y']);
        }
    }

    public function moveTrashToDelete($sender_id) {
        $user_id=Auth::id();
        if($sender_id!='') {
            Message::where('sender_trash','Y')
                ->where('sender_delete','N')
                ->where(['sender_id'=>$user_id, 'receiver_id'=>$sender_id])
                ->update(['sender_trash' =>'N', 'sender_archive' =>'N', 'sender_delete'=>'Y']);

            Message::where('receiver_trash','Y')
                ->where('receiver_delete','N')
                ->where(['receiver_id'=>$user_id, 'sender_id'=>$sender_id])
                ->update(['receiver_trash'=>'N', 'receiver_archive' =>'N', 'receiver_delete'=>'Y']);
        }
    }

///////////////////////////Trash tab ends here //////////////////////////////

    /**
     * Move 30 days trash message to delete cron
     */
    public function moveCronTrashToDelete($days) {
        Message::where('sender_trash','Y')
            ->where('receiver_trash','Y')
            ->whereDate('updated_at', '>=', date("Y-m-d H:i:s", strtotime("+ ".$days." day")))
            ->update(
                [
                    'sender_trash'      =>'N',
                    'receiver_trash'    =>'N',
                    'sender_archive'    =>'N',
                    'receiver_archive'  =>'N',
                    'sender_delete'     => 'Y',
                    'receiver_delete'   => 'Y'
                ]
            );
    }

    // get conversation is whole unread
    public function convWholeUnread($fromUserId)
    {
        return Message::where('sender_id', $fromUserId)->where('receiver_id', Auth::id())->where('read_status', 'Y')->exists();
    }
}