<?php
namespace App\Logic\Common;

use App\Models\User;

class VendorRepository {
    function searchVendors($search_preference = []) {
        $paginationEnabled = config('usersmanagement.enableCmsPagination');
        $search_preference['per_page'] = 10;
        $query = User::select('users.*')
            ->getVendors($search_preference)
            ->latest();
        if ($paginationEnabled) {
            return $query->paginate($search_preference['per_page']);
        } else {
            return $query->get();
        }
    }
}