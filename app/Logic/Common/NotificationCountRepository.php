<?php
namespace App\Logic\Common;
use Nahid\Talk\Facades\Talk;
use Illuminate\Support\Facades\Auth;

class NotificationCountRepository{

    protected $user = null;

    public function __construct($user = null)
    {
        if($user){
            Talk::setAuthUserId($user->id);
            $this->user = $user;
        }
        elseif(Auth::check()){
            Talk::setAuthUserId(Auth::user()->id);
        }

    }

    public function getMessagesCount(){
        return Talk::getUnreadMessagesCount();
    }

    public function getMyFavouritesCount(){
        if($this->user){
            $currentUser = $this->user;
        }
        else{
            $currentUser = Auth::user();
        }
        return $currentUser->favourited_users()
            ->SkipToConditions()->where('to_user_status',1)->where('is_from_seen',0)
            ->count();
    }

    public function getWhoFavouritedMeCount($user = null){
        if($user){
            $currentUser = $user;
        }
        else{
            $currentUser = Auth::user();
        }

        return $currentUser->favourited_by_users()
            ->skipFromConditions($currentUser)->where('is_seen',0)
            ->count();
    }

    public function getViewedMeCount($user = null){
        if($user){
            $currentUser = $user;
        }
        else{
            $currentUser = Auth::user();
        }
        return $currentUser->viewed_by_users()
            ->skipFromConditions($currentUser)->where('is_seen',0)
            ->count();
    }

}