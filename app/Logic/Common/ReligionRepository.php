<?php
namespace App\Logic\Common;

use App\Models\Community;
use App\Models\Religion;
use App\Models\Subcaste;

class ReligionRepository
{
    public function getCommunities(){
        return Country::all()->sortByDesc("sort");
    }

    public function getSubcastes($country_id){
        return State::where('country_id', $country_id)->get();
    }

    public function getCities($state_id){
        return City::where('state_id', $state_id)->get();
    }
}