<?php
namespace App\Logic\Common;

use App\Models\Education;
use App\Models\Interest;
use App\Models\Profession;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserInformationOptionsRepository
{
    public function getEducationOptions(){
        return Education::all();
    }

    public function getProfessionOptions(){
        return Profession::all();
    }

    public function getInterestOptions(){
        return Interest::all();
    }

    public function getFeaturedUsers($limit = ""){
        $query=User::where('featured', 1)->where('banned', 0)->where('deactivated',0)->whereNotNull('city_id')->hasVendorRole();
        if($limit == "") {
            $limit = 4;
        }
        return $query->inRandomOrder()->take($limit)->get();

    }


}