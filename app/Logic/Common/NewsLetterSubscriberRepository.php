<?php
namespace App\Logic\Common;

use App\Models\NewsLetterSubscription;

class NewsLetterSubscriberRepository
{
    public function getRecords($ids = '')
    {
        if($ids != "") {
            return NewsLetterSubscription::find($ids);
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }

    public function getStatusRecords($status = '')
    {
        return NewsLetterSubscription::where('site_id', '=', config('constants.site_id'))
                            ->where('status', '=', $status);
    }
}