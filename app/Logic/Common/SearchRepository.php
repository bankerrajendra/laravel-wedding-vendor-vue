<?php
namespace App\Logic\Common;

use App\Models\User;
use \Illuminate\Database\Query\JoinClause;

class SearchRepository{

    public function searchUsers($cities = array(), $age = array()){
        $paginationEnabled = config('usersmanagement.enableCmsPagination');

        $query =  User::hasUserRole()
            ->isCompletedProfile()
            ->hasOppositeGender()
            ->where('banned', 0)
            ->where('deactivated',0);
            if(count($cities) > 0){
                $query->fromCities($cities);
            }
            else{
                $query->fromSameCountry();
            }
        if ($paginationEnabled) {
            return   $query
                ->filterAge($age)
                ->skipLikedUsers()
                ->skipShortlistedUsers()
                ->skipSkippedUsers()
                ->skipWhoLikedMe()
                ->skipWhoBlockedByMe()
                ->skipWhoBlockedMe()
                ->latest()
                ->paginate(config('usersmanagement.frontEndSize'));
        } else {
            return   $query
                ->filterAge($age)
                ->skipLikedUsers()
                ->skipShortlistedUsers()
                ->skipSkippedUsers()
                ->skipWhoLikedMe()
                ->skipWhoBlockedByMe() 
                ->skipWhoBlockedMe()
                ->latest()
                ->get();
        }

    }

    public function newSearchUsers($search_preference = [])
    {
        $paginationEnabled = config('usersmanagement.enableCmsPagination');
        if($search_preference['order_by'] == "online") {
            $query = User::select('users.*')
                ->hasUserRole()
                ->getNewSearchedUsers($search_preference)
                ->orderBy('users.last_seen', 'DESC');
        } else {
            $query = User::select('users.*')
                ->hasUserRole()
                ->getNewSearchedUsers($search_preference)
                ->latest();
        }
        if ($paginationEnabled) {
            return $query->paginate($search_preference['per_page']);
        } else {
            return $query->get();
        }
    }
}