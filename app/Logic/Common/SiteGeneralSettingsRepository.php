<?php
namespace App\Logic\Common;

use App\Models\SiteGeneralOptions;
use Intervention\Image\ImageManagerStatic as Image;

class SiteGeneralSettingsRepository
{
    public function getSettingsByKey($key)
    {
        return SiteGeneralOptions::where('site_id', config('constants.site_id'))
                ->where('key', $key)
                ->first(['value']);
    }

    public function saveKeyVal($key, $value)
    {
        if(SiteGeneralOptions::where('site_id', config('constants.site_id'))->where('key', $key)->exists()) {
            $site_settings = SiteGeneralOptions::where('site_id', config('constants.site_id'))->where('key', $key)->first(['id']);
            $site_settings->value = $value;
            $site_settings->site_id = config('constants.site_id');
            $setting_res = $site_settings->save();
        } else {
            $site_settings = new SiteGeneralOptions();
            $site_settings->key = $key;
            $site_settings->value = $value;
            $site_settings->site_id = config('constants.site_id');
            $setting_res = $site_settings->save();
        }
        return $setting_res;
    }

    public function saveImage($image, $name, $field_name)
    {
        $extension          = $image->getClientOriginalExtension();
        $fileName           = $name.'.'.$extension;
        $img                = Image::make($_FILES[$field_name]['tmp_name']);
        // save image
        $img->save(public_path('img/'.$fileName));
        return $fileName;
    }

}