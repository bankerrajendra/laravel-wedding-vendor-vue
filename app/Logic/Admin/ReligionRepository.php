<?php

namespace App\Logic\Admin;
use App\Religion;
use http\Env\Request;

class ReligionRepository {
     
	public function SaveNewRecord($request)
    { 
        $religionData = new Religion;
        $religionData->religion_title = $request->input('religion_title');
        $religionData->religion_description = $request->input('religion_description');
         
        if ($religionData->save()) {
             return ['message'=>"Blog added successfully.", 'status'=>'1'];
        } else {
            return ['message'=>"There is problem in adding blog. please try again.", 'status'=>'0'];
        } 
    } 
}