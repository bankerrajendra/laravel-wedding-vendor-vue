<?php
namespace App\Logic\Admin;

use App\Models\Satellite;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class SatelliteRepository
{
    public function addRecord($request)
    {
        $record = new Satellite;
        $record->title = $request->input('title');
        $record->url = $request->input('url');
        $record->status = $request->input('status');
        if ($record->save()) {
            $id = $record->id;
            //// Logo Uploading
            $logo = $favicon = '';
            if($request->has('logo')) {
                $logo = $this->saveImage($request->file('logo'), 'logo');
                $record->logo = $logo;
            }
            if($request->has('favicon')) {
                $favicon = $this->saveImage($request->file('favicon'), 'favicon');
                $record->favicon = $favicon;
            }
            if($logo != "" && $favicon != "") {
                $record->save();
                return [
                            'message' => "Record added successfully.",
                            'status' => '1'
                    ];
            } else {
                return [
                    'message' => "Record added successfully. But logo or favicon not selected. please try again.", 
                    'status' => '2'
                ];
            }
        } else {
            return [
                        'message' => "There is problem in adding record. please try again.", 
                        'status' => '0'
                ];
        }
    }

    public function updateRecord($request)
    {
        $record = Satellite::find($request->input('id'));
        $record->title = $request->input('title');
        $record->url = $request->input('url');
        $record->status = $request->input('status');
        if ($record->save()) {
            if($request->has('logo') || $request->has('favicon')) {
                $id = $record->id;
                // delete old image
                if($request->has('logo')) {
                    $this->removeImage($record->logo, 'logo');
                }
                if($request->has('favicon')) {
                    $this->removeImage($record->favicon, 'favicon');
                }
                //// Image Uploading
                $logo = $this->saveImage($request->file('logo'), 'logo');
                $favicon = $this->saveImage($request->file('favicon'), 'favicon');
                if($logo != "" || $favicon != "") {
                    $record->logo = $logo;
                    $record->favicon = $favicon;
                    $record->save();
                    return [
                                'message' => "Record updated successfully.",
                                'status' => '1'
                        ];
                } else {
                    return [
                        'message' => "Record updated successfully. But there is problem in uploading logo OR favicon image(s). please try again.", 
                        'status' => '2'
                    ];
                }
            } else {
                return [
                    'message' => "Record updated successfully.",
                    'status' => '1'
                ];
            }
        } else {
            return [
                        'message' => "There is problem in updating record. please try again.", 
                        'status' => '0'
                ];
        }
    }
    
    protected function saveImage($image, $type)
    {
        $extension          = $image->getClientOriginalExtension();
        $fileName           = rand(11111, 99999).'_'.time().'.'.$extension;
        $img                = Image::make($_FILES[$type]['tmp_name']);
        // save image
        $img->save(public_path('img/satellites/'.$type.'/'.$fileName));
        return $fileName;
    }

    protected function removeImage($image, $type)
    {
        $img_path = public_path('img/satellites/'.$type.'/'.$image);
        if(File::exists($img_path)) {
            File::delete($img_path);
        }
    }

    public function getRecords($ids = '')
    {
        if($ids != "") {
            return Satellite::find($ids);
        } else {
            return new Satellite;
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }

    public function getStatusRecords($status = '')
    {
        return Satellite::where('status', '=', $status);
    }

    public function deleteRecord($record)
    {
        $this->removeImage($record->logo, 'logo');
        $this->removeImage($record->favicon, 'favicon');
        $record->delete();
    }
}