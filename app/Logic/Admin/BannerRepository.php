<?php
namespace App\Logic\Admin;

use App\Models\Banner;
use App\Events\FileUpload;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class BannerRepository
{
    public function addBannerRec($request)
    {
        $bannerRec = new Banner;
        $bannerRec->title = $request->input('title');
        $bannerRec->banner_type = $request->input('banner_type');
        $bannerRec->banner_link = $request->input('banner_link');
        $bannerRec->status = $request->input('status');
        $bannerRec->site_id = config('constants.site_id');
        if ($bannerRec->save()) {
            $banner_id = $bannerRec->id;
            //// Image Uploading
            $file_name = $this->saveImage($request->file('banner_image'));
            if($file_name != "") {
                $bannerRec->banner_image = $file_name;
                $bannerRec->save();
                return [
                            'message' => "Banner added successfully.",
                            'status' => '1'
                    ];
            } else {
                return [
                    'message' => "Banner added successfully. But there is problem in uploading banner image. please try again.", 
                    'status' => '2'
                ];
            }
        } else {
            return [
                        'message' => "There is problem in adding banner. please try again.", 
                        'status' => '0'
                ];
        }
    }

    public function updateBannerRec($request)
    {
        $bannerRec = Banner::find($request->input('id'));
        $bannerRec->title = $request->input('title');
        $bannerRec->banner_type = $request->input('banner_type');
        $bannerRec->banner_link = $request->input('banner_link');
        $bannerRec->status = $request->input('status');
        $bannerRec->site_id = config('constants.site_id');
        if ($bannerRec->save()) {
            if($request->has('banner_image')) {
                $banner_id = $bannerRec->id;
                // delete old image
                $image_path = public_path('img/banners/'.$bannerRec->banner_image);
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
                //// Image Uploading
                $file_name = $this->saveImage($request->file('banner_image'));
                if($file_name != "") {
                    $bannerRec->banner_image = $file_name;
                    $bannerRec->save();
                    return [
                                'message' => "Banner updated successfully.",
                                'status' => '1'
                        ];
                } else {
                    return [
                        'message' => "Banner updated successfully. But there is problem in uploading banner image. please try again.", 
                        'status' => '2'
                    ];
                }
            } else {
                return [
                    'message' => "Banner updated successfully.",
                    'status' => '1'
                ];
            }
        } else {
            return [
                        'message' => "There is problem in updating banner. please try again.", 
                        'status' => '0'
                ];
        }
    }
    
    protected function saveImage($image)
    {
        $extension          = $image->getClientOriginalExtension();
        $fileName           = rand(11111, 99999).'_'.time().'.'.$extension;
        $img                = Image::make($_FILES['banner_image']['tmp_name']);
        // save image
        $img->save(public_path('img/banners/'.$fileName));
        return $fileName;
    }

    public function getRecords($ids = '')
    {
        if($ids != "") {
            return Banner::find($ids);
        } else {
            return Banner::where('site_id', '=', config('constants.site_id'));
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }

    public function getStatusRecords($status = '')
    {
        return Banner::where('site_id', '=', config('constants.site_id'))
                            ->where('status', '=', $status);
    }

    public function deleteBanner($banner)
    {
        $image_path = public_path('img/banners/'.$banner->banner_image);
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $banner->delete();
    }
    
}