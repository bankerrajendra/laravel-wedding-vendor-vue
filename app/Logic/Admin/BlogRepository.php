<?php
/**
 * Created by PhpStorm.
 * User: SNV Wedding Gallery
 * Date: 2018-11-09
 * Time: 3:11 PM
 */

namespace App\Logic\Admin;

use App\Models\Blog;
use App\Models\BlogComment;
use App\Events\FileUpload;
use http\Env\Request;


class BlogRepository {
    public function __construct()
    {
    }

    public function getBlogs($onlyactive='', $pagination='', $request){
        $pageNo=1;
        $pagintaionEnabled = config('usersmanagement.enableCmsPagination');
        if ($pagintaionEnabled && $pagination!='nopagination') {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
            if($onlyactive=='onlyactive') {
                $fetchblogs = Blog::where('status','1')->orderBy('created_at','DESC')->paginate(config('usersmanagement.blogFrontEndSize'));
                if($pageNo>1)
                    $pageNo= (($pageNo-1)*config('usersmanagement.blogFrontEndSize')) +1;
            } else {
                $fetchblogs = Blog::orderBy('created_at','DESC')->paginate(config('usersmanagement.paginateListSize'));
                if($pageNo>1)
                    $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;
            }

        } else {
            if($onlyactive=='onlyactive') {
                $fetchblogs=Blog::where('status','1')->orderBy('created_at','DESC')->limit(10)->get();
            } else {
                $fetchblogs=Blog::get();
            }
        }


        $returnVar['fetchblogs']=$fetchblogs;
        $returnVar['pageNo']=$pageNo;
        return $returnVar;
    }


public function addBlogRec($request)
    {
        //// End og image uploading

        $blogRec = new Blog;
        $blogRec->title = $request->input('title');
        $blogRec->description = $request->input('description');
        $blogRec->slug = $request->input('slug');
        $blogRec->meta_title = $request->input('meta_title');
        $blogRec->meta_description = $request->input('meta_description');
        $blogRec->meta_keyword = $request->input('meta_keyword');
        $blogRec->status = $request->input('status');
        if ($blogRec->save()) {
            $blog_id = $blogRec->id;
            //// Image Uploading
            $status = event(new FileUpload(config('constants.fileType.blogImage'), $request, $blog_id));
            if($status[0]['status']=='0'){
                return ['message'=>"Blog added successfully. But there is problem in uploading blog image. please try again.", 'status'=>'2'];
            } else {
                $blogRec->blog_image=$status[0]['file_name'];
                $blogRec->save();
                return ['message'=>"Blog added successfully.", 'status'=>'1'];
            }

            //if()

        } else {
            return ['message'=>"There is problem in adding blog. please try again.", 'status'=>'0'];
        }

    }

    public function getBlogRec($blog_id){
        $blogRec=Blog::find($blog_id);
        return $blogRec;
    }

    public function getBlogBySlug($slug) {
        $blogRec=Blog::where('slug',$slug)->first();
        return $blogRec;
    }

    public function approveBlogComment($comment_id,$status) {
        $commentRec=BlogComment::find($comment_id);
        $commentRec->status=$status;
        $commentRec->save();
        return true;
    }

    public function updateBlogRec($blog_id,$request) {
        $blogRec=Blog::find($blog_id);
        $blogRec->title = $request->input('title');
        $blogRec->description = $request->input('description');
        $blogRec->slug = $request->input('slug');
        $blogRec->meta_title = $request->input('meta_title');
        $blogRec->meta_description = $request->input('meta_description');
        $blogRec->meta_keyword = $request->input('meta_keyword');
        $blogRec->status = $request->input('status');
        //$blogRec->save();

        if ($blogRec->save()) {
           // $blog_id = $blogRec->id;
            //// Image Uploading
            $status = event(new FileUpload(config('constants.fileType.blogImage'), $request, $blog_id));
            if($status[0]['status']=='0'){
                return ['message'=>"Blog added successfully. But there is problem in uploading blog image. please try again.", 'status'=>'2'];
            } else {
                $blogRec->blog_image=$status[0]['file_name'];
                $blogRec->save();
                return ['message'=>"Blog added successfully.", 'status'=>'1'];
            }
            //if()
        } else {
            return ['message'=>"There is problem in adding blog. please try again.", 'status'=>'0'];
        }
    }


    public function getBlogComments($blog_id, $onlyactive='', $pagination='', $request){
        $pageNo=1;
        $pagintaionEnabled = config('usersmanagement.enableCmsPagination');

        if ($pagintaionEnabled && $pagination!='nopagination') {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
            if($onlyactive=='onlyactive') {
                if($blog_id=='') {
                    $fetchblogs = BlogComment::where('status', '1')->paginate(config('usersmanagement.blogFrontEndSize'));
                } else {
                    $fetchblogs = BlogComment::where('status', '1')->where('blog_id', $blog_id)->paginate(config('usersmanagement.blogFrontEndSize'));
                }
                if($pageNo>1)
                    $pageNo= (($pageNo-1)*config('usersmanagement.blogFrontEndSize')) +1;
            } else {
                if($blog_id=='') {
                    $fetchblogs = BlogComment::paginate(config('usersmanagement.paginateListSize'));
                } else {
                    $fetchblogs = BlogComment::where('blog_id', $blog_id)->paginate(config('usersmanagement.paginateListSize'));
                }
                if($pageNo>1)
                    $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;
            }
        } else {
            if($onlyactive=='onlyactive') {
                if($blog_id=='') {
                    $fetchblogs=BlogComment::where('status','1')->orderBy('updated_at','desc')->get();
                } else {
                    $fetchblogs=BlogComment::where('status','1')->where('blog_id',$blog_id)->orderBy('updated_at','desc')->get();
                }
            } else {
                if($blog_id=='') {
                    $fetchblogs=BlogComment::get();
                } else {
                    $fetchblogs=BlogComment::where('blog_id',$blog_id)->get();
                }

            }
        }
        $returnVar['fetchblogs']=$fetchblogs;
        $returnVar['pageNo']=$pageNo;
        return $returnVar;
    }
}