<?php
namespace App\Logic\Admin;

use App\Models\Review;
use App\Models\User;

class ReviewRepository
{
    public function getRecords($ids = '')
    {
        if($ids != "") {
            return Review::find($ids);
        } else {
            return Review::where('id', '<>', '');
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }

    public function getStatusRecords($status = '')
    {
        return Review::where('approved', '=', $status);
    }

    public function getUserReview($user_id, $vendor_id){
        return Review::where(['user_id' => $user_id, 'vendor_id' => $vendor_id])->first();
    }

    public function getVendorReviews($vendor_id)
    {
        return Review::where(['vendor_id' => $vendor_id, 'approved' => '1'])->get();
    }

    public function getReviews($vendorId, $perPage = 2, $rating = 'all') 
    {
        // get reviews
        if($rating != 'all') {
            $reviewsObj = Review::where('vendor_id', '=', $vendorId)
                        ->where('approved', '=', '1')
                        ->where('rating', '=', $rating)
                        ->latest()
                        ->paginate($perPage);
        } else {
            $reviewsObj = Review::where('vendor_id', '=', $vendorId)
                        ->where('approved', '=', '1')
                        ->latest()
                        ->paginate($perPage);
        }
                
        $reviews = [];
        $i=0;
        foreach($reviewsObj as $reviewSingle) {
            if(User::where('id', $reviewSingle->user_id)->exists()) {
                $reviewUsr = User::where('id', $reviewSingle->user_id)->first(['first_name', 'last_name']);
                $reviews['reviews'][$i]['profile_link'] = '';
                $reviews['reviews'][$i]['profile_picture'] = $reviewUsr->getVendorProfilePic();
                $reviews['reviews'][$i]['name'] = $reviewUsr->first_name . ' ' . $reviewUsr->last_name;
                $reviews['reviews'][$i]['message'] = $reviewSingle->message;
                $reviews['reviews'][$i]['rating'] = $reviewSingle->rating;
                $reviews['reviews'][$i]['created_at'] = $reviewSingle->created_at->diffForHumans();
                $i++;
            }
        }
        if($reviewsObj->nextPageUrl() != "") {
            $reviews['next_page'] = 'yes';
        } else {
            $reviews['next_page'] = '';
        }
        return $reviews;
    }
}