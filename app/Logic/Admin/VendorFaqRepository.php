<?php
namespace App\Logic\Admin;

use App\Models\VendorFaq;
use App\Models\VendorFaqAnswer;
use App\Models\VendorFaqAnswerOption;
use App\Models\VendorFaqAnswerPlaceholder;

class VendorFaqRepository
{
    public function addRecord($request)
    {
        $record = new VendorFaq;
        $record->question = $request->input('question');
        $record->categories = serialize($request->input('categories'));
        $record->answer_type = $request->input('answer_type');
        $record->status = $request->input('status');
        $record->order = $request->input('order');
        if ($record->save()) {
            if(in_array($request->input('answer_type'), ['radio', 'checkbox'])) {
                $options = new VendorFaqAnswerOption;
                $options->faq_id = $record->id;
                $options->options = $request->input('options');
                $options->save();
            } else {
                if($request->has('placeholder')) {
                    $options = new VendorFaqAnswerPlaceholder;
                    $options->faq_id = $record->id;
                    $options->placeholder = $request->input('placeholder');
                    $options->save();
                }
            }
            return [
                'message' => "Record added successfully.", 
                'status' => '1'
            ];
        } else {
            return [
                    'message' => "There is problem in adding record. please try again.", 
                    'status' => '0'
                ];
        }
    }

    public function updateRecord($request)
    {
        $record = VendorFaq::find($request->input('id'));
        $record->question = $request->input('question');
        $record->categories = serialize($request->input('categories'));
        $record->answer_type = $request->input('answer_type');
        $record->status = $request->input('status');
        $record->order = $request->input('order');
        if ($record->save()) {
            if(in_array($request->input('answer_type'), ['radio', 'checkbox'])) {
                if(VendorFaqAnswerOption::where('faq_id', $record->id)->exists()) {
                    // update data
                    $options = VendorFaqAnswerOption::where('faq_id', $record->id)->first();
                    $options->options = $request->input('options');
                    $options->save();
                } else {
                    // create
                    $options = new VendorFaqAnswerOption;
                    $options->faq_id = $record->id;
                    $options->options = $request->input('options');
                    $options->save();
                }
                // delete placeholder if exists
                if(VendorFaqAnswerPlaceholder::where('faq_id', $record->id)->exists()) {
                    // delete record
                    VendorFaqAnswerPlaceholder::where('faq_id', $record->id)->delete();
                }
            } else {
                // delete option if exists
                if(VendorFaqAnswerOption::where('faq_id', $record->id)->exists()) {
                    // delete record
                    VendorFaqAnswerOption::where('faq_id', $record->id)->delete();
                }
                // add placeholder
                if($request->has('placeholder')) {
                    if(VendorFaqAnswerPlaceholder::where('faq_id', $record->id)->exists()) {
                        // update data
                        $options = VendorFaqAnswerPlaceholder::where('faq_id', $record->id)->first();
                        $options->placeholder = $request->input('placeholder');
                        $options->save();
                    } else {
                        $options = new VendorFaqAnswerPlaceholder;
                        $options->faq_id = $record->id;
                        $options->placeholder = $request->input('placeholder');
                        $options->save();
                    }
                }                
            }
            return [
                'message' => "Record updated successfully.", 
                'status' => '1'
            ];
        } else {
            return [
                    'message' => "There is problem in updating record. please try again.", 
                    'status' => '0'
                ];
        }
    }
    
    public function getRecords($ids = '')
    {
        if($ids != "") {
            return VendorFaq::find($ids);
        } else {
            return new VendorFaq;
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }

    public function getStatusRecords($status = '')
    {
        return VendorFaq::where('status', '=', $status);
    }

    public function deleteRecord($record)
    {
        // delete faq placeholder
        VendorFaqAnswerPlaceholder::where('faq_id', $record->id)->delete();
        // delete faq answer option
        VendorFaqAnswerOption::where('faq_id', $record->id)->delete();
        // delete faq answer
        VendorFaqAnswer::where('faq_id', $record->id)->delete();
        // delete faq
        $record->delete();
    }
}