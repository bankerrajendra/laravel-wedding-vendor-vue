<?php
namespace App\Logic\Admin;

use Illuminate\Support\Facades\Auth;
use App\Models\Artist;
use Illuminate\Support\Facades\Storage;

class ArtistRepository
{
    protected $site_id;

    public function __construct()
    {
        $this->site_id = config('constants.site_id');
    }

    public function getRecords($ids = '')
    {
        if($ids != "") {
            return Artist::find($ids);
        } else {
            return Artist::where('site_id', '=', $this->site_id);
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }

    public function getStatusRecords($is_approved = '')
    {
        return Artist::where('site_id', $this->site_id)->where('is_approved', '=', $is_approved);
    }

    public function getAdminRecord($id)
    {
        return Artist::where('id', $id)
            ->where('site_id', $this->site_id)
            ->first(); 
    }

    public function getArtistsWithImages($limit = 6)
    {
        return Artist::where('is_approved', 'Y')
            ->where('image_full', '<>', '')
            ->where('site_id', $this->site_id)
            ->inRandomOrder()
            ->limit($limit);
    }

    public function addRecord($request)
    {
        if(Artist::where('name', 'like', $request->input('name'))->count() > 0) {
            return $response = [
                'status' => 0,
                'message' => 'Please choose different name.'
            ];
        }
        $record = new Artist();
        $record->name = $request->input('name');
        $record->vendor_id = Auth::id();
        $record->site_id = $this->site_id;
        $record->is_approved = $request->input('is_approved');
        if($record->save()) {
            if($request->hasFile('image_full')) {
                $image_name = slugifyText($record->name);
                $image_full = $this->saveImage($request->file('image_full'), $image_name);
                $record->image_full = $image_full;
                $record->save();
            }
        }
    }

    public function updateRecord($request)
    {
        $record = Artist::where('id', $request->input('id'))->first();
        if(Artist::where('id', '<>',$request->input('id'))->where('name', 'like', $request->input('name'))->count() > 0) {
            return $response = [
                'status' => 0,
                'message' => 'Please choose different name.'
            ];
        }
        $record->name = $request->input('name');
        $record->is_approved = $request->input('is_approved');
        if($record->save()) {
            if($request->hasFile('image_full')) {
                $image_name = slugifyText($record->name);
                $image_full = $this->saveImage($request->file('image_full'), $image_name);
                $record->image_full = $image_full;
                $record->save();
            }
        }
    }

    protected function saveImage($image, $name)
    {
        $extension          = $image->getClientOriginalExtension();
        $fileName           = $name.'.'.$extension;
        // save image to s3
        Storage::disk('s3')->put('vendor_photos/artists/' . $fileName, file_get_contents($image), 'public');
        return $fileName;
    }

    public function deleteRecord($record)
    {
        // remove the related content like poster/logo/banner from s3
        $rec = Artist::where('id', $record->id)->first();
        if(Storage::disk('s3')->exists('vendor_photos/artists/' . $rec->image_full)) {
            Storage::disk('s3')->delete('vendor_photos/artists/' . $rec->image_full);
        }
        // delete faq
        $record->delete();
    }

    public function deleteImage($id)
    {
        $img = Artist::where('id', $id)->where('site_id', $this->site_id)->first();
        if($img != null){
            
            if(Storage::disk('s3')->exists('vendor_photos/artists/' . $img->image_full)) {
                Storage::disk('s3')->delete('vendor_photos/artists/' . $img->image_full);
            }

            $img->image_full = '';
            $img->save();
            $response = [
                'status' => 1,
                'message'=> 'Image deleted successfully'
            ];
        } else{
            $response = [
                'status' => 0,
                'message' => 'Invalid Id'
            ];
        }
        return $response;
    }
}