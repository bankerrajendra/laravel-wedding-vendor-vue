<?php
namespace App\Logic\Admin;

use App\Models\Improve;

class ImproveRepository
{
    public function getRecords($ids = '')
    {
        if($ids != "") {
            return Improve::find($ids);
        } else {
            return Improve::where('site_id', '=', config('constants.site_id'));
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }
}