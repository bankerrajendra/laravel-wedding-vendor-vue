<?php
namespace App\Logic\Admin;

use App\Models\EventReview;
use App\Models\User;

class EventReviewRepository
{
    public function getRecords($ids = '')
    {
        if($ids != "") {
            return EventReview::find($ids);
        } else {
            return EventReview::where('id', '<>', '');
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }

    public function getStatusRecords($status = '')
    {
        return EventReview::where('approved', '=', $status);
    }

    public function getUserReview($user_id, $event_id){
        return EventReview::where(['user_id' => $user_id, 'event_id' => $event_id])->first();
    }

    public function getVendorReviews($vendor_id)
    {
        return EventReview::where(['vendor_id' => $vendor_id, 'approved' => '1'])->get();
    }

    public function getEventReviews($event_id)
    {
        return EventReview::where(['event_id' => $event_id, 'approved' => '1'])->get();
    }

    public function getReviews($eventId, $perPage = 2, $rating = 'all') 
    {
        // get reviews
        if($rating != 'all') {
            $reviewsObj = EventReview::where('event_id', '=', $eventId)
                        ->where('approved', '=', '1')
                        ->where('rating', '=', $rating)
                        ->latest()
                        ->paginate($perPage);
        } else {
            $reviewsObj = EventReview::where('event_id', '=', $eventId)
                        ->where('approved', '=', '1')
                        ->latest()
                        ->paginate($perPage);
        }
                
        $reviews = [];
        $i=0;
        foreach($reviewsObj as $reviewSingle) {
            if(User::where('id', $reviewSingle->user_id)->exists()) {
                $reviewUsr = User::where('id', $reviewSingle->user_id)->first(['first_name', 'last_name']);
                $reviews['reviews'][$i]['profile_link'] = '';
                $reviews['reviews'][$i]['profile_picture'] = $reviewUsr->getVendorProfilePic();
                $reviews['reviews'][$i]['name'] = $reviewUsr->first_name . ' ' . $reviewUsr->last_name;
                $reviews['reviews'][$i]['message'] = $reviewSingle->message;
                $reviews['reviews'][$i]['rating'] = $reviewSingle->rating;
                $reviews['reviews'][$i]['created_at'] = $reviewSingle->created_at->diffForHumans();
                $i++;
            }
        }
        if($reviewsObj->nextPageUrl() != "") {
            $reviews['next_page'] = 'yes';
        } else {
            $reviews['next_page'] = '';
        }
        return $reviews;
    }
}