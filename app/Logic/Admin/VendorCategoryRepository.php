<?php
namespace App\Logic\Admin;

use App\Models\VendorCategory;
use App\Models\VendorCategorySatelliteInformation;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class VendorCategoryRepository
{
    public function addRecord($request)
    {
        $record = new VendorCategory;
        $record->name = $request->input('name');
        $record->vendor_type = $request->input('vendor_type');
        $record->slug = slugifyText($request->input('name'));
        $record->display_in_footer = $request->input('display_in_footer');
        $record->has_admin_faqs = $request->input('has_admin_faqs');
        $record->has_custom_faqs = $request->input('has_custom_faqs');
        $record->status = $request->input('status');
        $record->satellites = serialize($request->input('satellites'));
        if ($record->save()) {
            $id = $record->id;
            // save satellite's information
            $category_id = $id;
            $i=1;
            foreach($request->input('satellites') as $satellite_data) {
                $satelite_info = new VendorCategorySatelliteInformation;
                $satelite_info->vendor_category_id = $category_id;
                $satelite_info->satellite_id = $satellite_data;
                $satelite_info->title = $request->input('title_'.$satellite_data);
                $satelite_info->keywords = $request->input('keywords_'.$satellite_data);
                $satelite_info->description = $request->input('description_'.$satellite_data);
                $satelite_info->meta_description = $request->input('meta_description_'.$satellite_data);
                if($satelite_info->save()) {
                    if($request->hasFile('icon_image_'.$satellite_data)) {
                        $icon_image = $this->saveImage($request->file('icon_image_'.$satellite_data), 'icon_image_'.$satellite_data);
                        $satelite_info->icon_image = $icon_image;
                        $satelite_info->save();
                    }
                }
                $i++;
            }

            return [
                'message' => "Record added successfully.", 
                'status' => '1'
            ];
        } else {
            return [
                        'message' => "There is problem in adding record. please try again.", 
                        'status' => '0'
                ];
        }
    }

    public function updateRecord($request)
    {
        $record = VendorCategory::find($request->input('id'));
        // get old satelites for deleting if not there
        $old_satellites = unserialize($record->satellites);
        $record->name = $request->input('name');
        $record->vendor_type = $request->input('vendor_type');
        $record->slug = slugifyText($request->input('name'));
        $record->display_in_footer = $request->input('display_in_footer');
        $record->has_admin_faqs = $request->input('has_admin_faqs');
        $record->has_custom_faqs = $request->input('has_custom_faqs');
        $record->status = $request->input('status');
        $record->satellites = serialize($request->input('satellites'));
        $new_sats = $request->input('satellites');
        // delete old satellites data
        foreach($old_satellites as $old_satellite_id) {
            if(!in_array($old_satellite_id, $new_sats)) {
                $old_satellite_image = VendorCategorySatelliteInformation::where('vendor_category_id', $record->id)->where('satellite_id', $old_satellite_id)->first(['id', 'icon_image']);
                if(@$old_satellite_image->icon_image != "") {
                    $this->removeImage($old_satellite_image->icon_image);
                }
                if(!is_null($old_satellite_image))
                    $old_satellite_image->delete();
            }
        }
        if ($record->save()) {
            $id = $record->id;
            // save satellite's information
            $category_id = $id;
            $i=1;
            foreach($new_sats as $satellite_data) {
                if(VendorCategorySatelliteInformation::where('vendor_category_id', $category_id)->where('satellite_id', $satellite_data)->exists()) {
                    $satelite_info = VendorCategorySatelliteInformation::where('vendor_category_id', $category_id)->where('satellite_id', $satellite_data)->first(['id', 'icon_image']);
                    $satelite_info->title = $request->input('title_'.$satellite_data);
                    $satelite_info->keywords = $request->input('keywords_'.$satellite_data);
                    $satelite_info->description = $request->input('description_'.$satellite_data);
                    $satelite_info->meta_description = $request->input('meta_description_'.$satellite_data);
                    if($satelite_info->save()) {
                        if($request->hasFile('icon_image_'.$satellite_data)) {
                            // remove old image if already added before
                            if(@$satelite_info->icon_image != "") {
                                $this->removeImage($satelite_info->icon_image);
                            }
                            // add new image
                            $icon_image = $this->saveImage($request->file('icon_image_'.$satellite_data), 'icon_image_'.$satellite_data);
                            $satelite_info->icon_image = $icon_image;
                            $satelite_info->save();
                        }
                    }
                } else {
                    $satelite_info = new VendorCategorySatelliteInformation;
                    $satelite_info->vendor_category_id = $category_id;
                    $satelite_info->satellite_id = $satellite_data;
                    $satelite_info->title = $request->input('title_'.$satellite_data);
                    $satelite_info->keywords = $request->input('keywords_'.$satellite_data);
                    $satelite_info->description = $request->input('description_'.$satellite_data);
                    $satelite_info->meta_description = $request->input('meta_description_'.$satellite_data);
                    if($satelite_info->save()) {
                        if($request->hasFile('icon_image_'.$satellite_data)) {
                            $icon_image = $this->saveImage($request->file('icon_image_'.$satellite_data), 'icon_image_'.$satellite_data);
                            $satelite_info->icon_image = $icon_image;
                            $satelite_info->save();
                        }
                    }
                }
                $i++;
            }
            return [
                'message' => "Record updated successfully.", 
                'status' => '1'
            ];
        } else {
            return [
                        'message' => "There is problem in updating record. please try again.", 
                        'status' => '0'
                ];
        }
    }
    
    protected function saveImage($image, $type)
    {
        $extension          = $image->getClientOriginalExtension();
        $fileName           = rand(11111, 99999).'_'.time().'.'.$extension;
        // save image to s3
        Storage::disk('s3')->put('vendor_photos/vendor_category/' . $fileName, file_get_contents($image), 'public');
        return $fileName;
    }

    protected function removeImage($image)
    {
        if(Storage::disk('s3')->exists('vendor_photos/vendor_category/' . $image)) {
            Storage::disk('s3')->delete('vendor_photos/vendor_category/' . $image);
        }
    }

    public function getRecords($ids = '')
    {
        if($ids != "") {
            return VendorCategory::find($ids);
        } else {
            return new VendorCategory;
        }
    }

    public function getSearchRecords($records, $search = '')
    {
        return $records->getRecordsSearch($search);
    }

    public function getStatusRecords($status = '')
    {
        return VendorCategory::where('status', '=', $status);
    }

    public function getFooterCategories($status = '')
    {
        $site_id = config('constants.site_id');
        return VendorCategory::where('display_in_footer', '=', $status)->getVendorByType('wedding')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->where('status', '=', $status);
    }

    public function getHomeTopCategories()
    {
        $site_id = config('constants.site_id');
        $categries = VendorCategory::where('status', '=', '1')->getVendorByType('wedding')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')
        ->where('name', '<>', 'Event Organizer')
        ->inRandomOrder()->limit(6)->get(['id', 'name', 'slug', 'satellites']);
        $random_cats = [];
        $i = 0;
        foreach($categries as $category) {
            $random_cats[$i]['title'] = $category->name;
            $random_cats[$i]['slug'] = $category->slug;
            $img = VendorCategorySatelliteInformation::where('vendor_category_id', $category->id)->first(['icon_image']);
            $random_cats[$i]['image'] = $img->icon_image;
            $i++;
        }
        return $random_cats;
    }

    public function deleteRecord($record)
    {
        // remove category satellite information
        $removal_records = VendorCategorySatelliteInformation::where('vendor_category_id', $record->id)->get();
        foreach($removal_records as $rm_record) {
            $img = VendorCategorySatelliteInformation::where('vendor_category_id', $rm_record->vendor_category_id)->where('satellite_id', $rm_record->satellite_id)->first(['icon_image']);
            if(@$img->icon_image != "") {
                $this->removeImage($img->icon_image);
            }
            if(!is_null($rm_record))
                $rm_record->delete();
        }
        $record->delete();
    }

    // get sepcific type of category
    public function getVendorCategoryByTypeFront($slug = 'wedding')
    {
        $site_id = config('constants.site_id');
        return VendorCategory::where('status', '=', 1)
                    ->getVendorByType($slug)
                    ->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')
                    ->orderBy('name', 'ASC')
                    ->get(['id', 'name']);
     
    }

    // get sepcific type of category
    public function getVendorSingleCategoryBy($field = 'slug', $value = '', $slug = 'wedding')
    {
        $site_id = config('constants.site_id');
        return VendorCategory::where($field, $value)->getVendorByType($slug)->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%');
    }
}