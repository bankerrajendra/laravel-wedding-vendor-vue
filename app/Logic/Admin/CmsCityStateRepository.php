<?php
namespace App\Logic\Admin;

use App\Models\CmsCityState;


class CmsCityStateRepository{

    public function __construct()
    {

    }

    public function getCmsCityState($page_type) {

        $pagintaionEnabled = config('usersmanagement.enableCmsCityPagination');
        if ($pagintaionEnabled) {
            $cmsCityState = CmsCityState::where('page_type',$page_type)->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $cmsCityState = CmsCityState::where('page_type',$page_type)->get();
        }
     //   $cmsCityState = CmsCityState::all();
     //   $cmsCityState=CmsCityState::get();

        return $cmsCityState;
    }

    public function getCountryWiseStates($country_name) {
        $cmsStates=CmsCityState::where('page_name',$country_name)->where('page_type','State')->orderBy('type_name','asc')->get();
        return $cmsStates;
    }

    public function getCountryWiseStatesFront($country_name) {
        $cmsStates=CmsCityState::where('page_name',$country_name)->where('page_type','State')->where('status','1')->orderBy('type_name','asc')->get();
        //return $cmsStates;
        if(count($cmsStates)>0) {
            return $cmsStates;
        } else {
            return abort(404);
        }
    }

    public  function getCountryWiseCities($country_name) {
        $cmsCities=CmsCityState::where('page_name', $country_name)->where('page_type','City')->orderBy('type_name','asc')->get();
        return $cmsCities;
    }

    public  function getCountryWiseCitiesFront($country_name) {
        $cmsCities=CmsCityState::where('page_name', $country_name)->where('page_type','City')->where('status','1')->orderBy('type_name','asc')->get();
        //return $cmsCities;
        if(count($cmsCities)>0) {
            return $cmsCities;
        } else {
            return abort(404);
        }
    }

    public function getCmsCityStateDetail($page_type,$slug,$country_name=''){
        if($country_name=='') {
            $cmsRec = CmsCityState::where('page_type', $page_type)->where('slug', $slug)->get();
        } else {
            $cmsRec = CmsCityState::where('page_type', $page_type)->where('slug', $slug)->where('page_name', $country_name)->get();
        }
        if(count($cmsRec)>0) {
            return $cmsRec;
        } else {
            return abort(404);
        }
    }

    public function getCmsCityStateRec($page_id) {
        $cmsCityStateRec=CmsCityState::find($page_id);
        return $cmsCityStateRec;
    }

    public function updateCmsCityStateRec($id,$request) {
       // print_r($request->input());
        $cmsCityStateRec=CmsCityState::find($id);

        $cmsCityStateRec->page_description=$request->input('page_description');
        $cmsCityStateRec->slug=$request->input('slug');
        $cmsCityStateRec->status=$request->input('status');
//        $cmsCityStateRec->meta_title=$request->input('meta_title');
//        $cmsCityStateRec->meta_keyword=$request->input('meta_keyword');
//        $cmsCityStateRec->meta_description=$request->input('meta_description');
        $cmsCityStateRec->save();

    }
}
