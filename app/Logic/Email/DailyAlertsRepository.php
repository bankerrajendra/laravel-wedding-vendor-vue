<?php
namespace App\Logic\Email;

use App\Mail\Notifications;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Logic\Common\NotificationCountRepository;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\Welcome;

class DailyAlertsRepository{
    public function sendDailyAlerts(){
//        $usera=User::find(131);
//        Mail::to('sbindu1999@gmail.com')->send(new Welcome($usera));
        $users = $this->getUsersToNotify();
        foreach ($users as $user){
            $notifications = null;
            $notifications = $this->getNotificationsCount($user);
            if($notifications){
                if(($notifications['messages'] > 0) || ($notifications['viewed_me'] > 0) || ($notifications['liked_me'] > 0)) {
                    Mail::to($user->email)->send(new Notifications($user, $notifications));
//                    Mail::to('sbindu1999@gmail.com')->send(new Notifications($user, $notifications));
                }
            }
        }
    }

    public function getUsersToNotify(){
        $users = DB::table('users')
            ->join('users_information', 'users.id', '=', 'users_information.user_id')
            ->where('users_information.activity_notification', '=', 1)
            ->where('users.banned', '=', 0)
            ->where('users.deleted_at','=', NULL)
            ->select('users.*')
            ->get();
        return $users;
    }

    public function getNotificationsCount($user){
        $userObj = User::find($user->id);
        $notificationCountRepo = new NotificationCountRepository($userObj);
        $notificationCount = array();
        try{
            $notificationCount['messages'] = $notificationCountRepo->getMessagesCount();
            $notificationCount['viewed_me'] = $notificationCountRepo->getViewedMeCount($userObj);
            $notificationCount['liked_me'] = $notificationCountRepo->getWhoFavouritedMeCount($userObj);
        }
        catch (\Exception $e){
                return null;
        }
        return $notificationCount;
    }
}