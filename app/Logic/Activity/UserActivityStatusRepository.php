<?php
namespace App\Logic\Activity;

use App\Models\FavouritedUser;
use Illuminate\Support\Facades\Auth;

class UserActivityStatusRepository {
    public function __construct() {
    }

    public function getLikedUsers() {
        $paginationEnabled = config('usersmanagement.enableMsgPagination');
        //return FavouritedUser::where('from_user', '=', $this->currentUser->id)->get();
        $currentUser = Auth::user();
        $currentUser->favourited_users()
            ->SkipToConditions()->where('to_user_status',1)->where('is_from_seen',0)->update(['is_from_seen'=>1]);
        if ($paginationEnabled) {
            return $currentUser->favourited_users()->SkipToConditions()->orderBy('updated_at','DESC')->paginate(config('usersmanagement.msgFrontEndSize'));
        } else {
            return $currentUser->favourited_users()->SkipToConditions()->orderBy('updated_at','DESC')->get();
        }
    }

    public function getWhoLikedMe() {
        $paginationEnabled = config('usersmanagement.enableMsgPagination');
        //return FavouritedUser::where('to_user', '=', $this->currentUser->id)->get();
        $currentUser = Auth::user();
        $currentUser->favourited_by_users()
            ->skipFromConditions()->where('is_seen',0)->update(['is_seen'=>1]);

        if ($paginationEnabled) {
            return $currentUser->favourited_by_users()->skipFromConditions()->orderBy('updated_at','DESC')->paginate(config('usersmanagement.msgFrontEndSize'));
        } else {
            return $currentUser->favourited_by_users()->skipFromConditions()->orderBy('updated_at','DESC')->get();
        }
    }

    public function getWhoViewedMe($search_preference='') {
        $paginationEnabled = config('usersmanagement.enableMsgPagination');

        //return ViewedUser::where('to_user', '=', $this->currentUser->id)->get();
        $currentUser = Auth::user();
        $currentUser->viewed_by_users()
            ->skipFromConditions()->where('is_seen',0)->update(['is_seen'=>1]);

        if ($paginationEnabled) {
            //return $currentUser->viewed_by_users()->skipFromConditions()->orderBy('updated_at','DESC')->paginate(config('usersmanagement.msgFrontEndSize'));
            return $currentUser->viewed_by_users()->skipFromConditions()->orderBy('updated_at','DESC')->paginate($search_preference['per_page']);

        } else {
            return $currentUser->viewed_by_users()->skipFromConditions()->orderBy('updated_at','DESC')->get();
        }
    }

    public function getShortlistedUsers() {
        $paginationEnabled = config('usersmanagement.enableCmsPagination');
        //return ShortlistedUser::where('from_user', '=', $this->currentUser->id)->get();
        $currentUser = Auth::user();
        if($paginationEnabled) {
            return $currentUser->shortlisted_users()->skipToConditions()->paginate(config('usersmanagement.frontEndSize'));
        } else {
            return $currentUser->shortlisted_users()->skipToConditions()->get();
        }
    }

    public function getSkippedUsers() {
        $paginationEnabled = config('usersmanagement.enableCmsPagination');
        $currentUser = Auth::user();
        if($paginationEnabled) {
            return $currentUser->skipped_users()->skipToConditions()->paginate(config('usersmanagement.frontEndSize'));
        } else {
            return $currentUser->skipped_users()->skipToConditions()->get();
        }
    }

    public function getBlockedUsers() {
        $paginationEnabled = config('usersmanagement.enableCmsPagination');
        $currentUser = Auth::user();
        if($paginationEnabled) {
            return $currentUser->blocked_users()->skipDeletedUsers()->paginate(config('usersmanagement.frontEndSize'));
        } else {
            return $currentUser->blocked_users()->skipDeletedUsers()->get();
        }
    }

    public function getSharedPhotoUsers() {
        $paginationEnabled = config('usersmanagement.enableCmsPagination');
        $currentUser = Auth::user();
        if($paginationEnabled) {
            return $currentUser->photonotify_users()->skipToConditions()->paginate(config('usersmanagement.frontEndSize'));
        } else {
            return $currentUser->photonotify_users()->skipToConditions()->get();
        }
    }
}