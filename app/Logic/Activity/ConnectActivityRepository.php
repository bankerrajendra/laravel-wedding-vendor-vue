<?php

namespace App\Logic\Activity;

use App\Events\Activity;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ConnectActivityRepository{

    protected $randomUser;
    protected $suggestedUsers;

    public function __construct()
    {
        $this->randomUser = null;
        $this->suggestedUsers = null;
    }

    public function getRandomConnectUser(){
        $this->randomUser =  User::skipConditions()
            ->inRandomOrder()
            ->first();
//
//        if($this->randomUser){
//            event(new Activity(config('constants.activity.view_profile'), $this->randomUser->id));
//        }
        return $this->randomUser;
    }

    public function getSuggestedUsers(){
        $query = User::skipConditions();
        if($this->randomUser){
            $query = $query->where('id', '!=', $this->randomUser->id);
        }

        $suggestedUsersLimit = config('constants.suggested_users_limit');
        $result = $query->inRandomOrder()->skip(0)->take($suggestedUsersLimit)->get();
        return $result;
    }

    public function getNewOnlineUsers() {
        $authuser=Auth::user();
        $paginationEnabled = config('usersmanagement.enableDashboardPagination');

        //$query = User::skipConditions();
        $query = User::select('*', DB::raw('(CASE WHEN country_id = ' . $authuser->country_id . ' THEN 1 ELSE 2 END) AS newCountryid'))->newUserConditions();
        if ($paginationEnabled) {
            $query = $query->orderBy('newCountryid','asc')->orderBy('created_at', "desc")->paginate(config('usersmanagement.dashboardFrontEndSize'));
        } else {
            $query = $query->orderBy('newCountryid','asc')->orderBy('created_at', "desc")->limit(12)->get();
        }

        return $query;
    }

    public function getPartnerMatchUsers() {
        $authuser=Auth::user();
        $paginationEnabled = config('usersmanagement.enableDashboardPagination');

        //$query = User::skipConditions();
        $query = User::select('*', DB::raw('(CASE WHEN country_id = ' . $authuser->country_id . ' THEN 1 ELSE 2 END) AS newCountryid'))->partnerPreferenceConditions();

        if ($paginationEnabled) {
            $query = $query->orderBy('newCountryid','asc')->inRandomOrder()->paginate(config('usersmanagement.dashboardFrontEndSize'));
        } else {
            $query = $query->orderBy('newCountryid','asc')->inRandomOrder()->limit(12)->get();
        }

        return $query;
    }

    public function getDiamondUsers() {
        $authuser=Auth::user();
        $paginationEnabled = config('usersmanagement.enableDashboardPagination');

        //$query = User::skipConditions();
        $query = User::select('*', DB::raw('(CASE WHEN country_id = ' . $authuser->country_id . ' THEN 1 ELSE 2 END) AS newCountryid'))->paidUserConditions();
        if ($paginationEnabled) {
            $query = $query->orderBy('newCountryid','asc')->inRandomOrder()->paginate(config('usersmanagement.dashboardFrontEndSize'));
        } else {
            $query = $query->orderBy('newCountryid','asc')->inRandomOrder()->limit(12)->get();
        }
        return $query;
    }

    public function getNewProfiles() {
        $query = User::newUserProfile();
        $query = $query->orderBy('created_at', "desc")->limit(5)->get();  //select(DB::raw('ROUND(DATEDIFF(CURDATE(),date_of_birth) / 365.25)>40'))->
        return $query;
    }

    public function getMatchesProfiles() {
        $query = User::matchUserProfile();
        $query = $query->orderBy('created_at', "desc")->limit(5)->get();  //select(DB::raw('ROUND(DATEDIFF(CURDATE(),date_of_birth) / 365.25)>40'))->
        return $query;
    }
	
	## Get Matches
	function getMatches(){		
		 $userProfile = User::find(Auth::user()->id)	
            ->hasUserRole()
            ->hasOppositeGender()
            ->skipWhoBlockedByMe()
            ->SkipWhoBlockedMe()
             ->skipSkippedUsers()
             ->skipWhoSkippedMe()
            ->get(); 
			
			return $userProfile;
	}
	/** @end **/

	 
}
