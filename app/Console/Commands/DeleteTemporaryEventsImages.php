<?php

namespace App\Console\Commands;

use App\Models\TemporaryEventVendorsImage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;

class DeleteTemporaryEventsImages extends Command
{
    protected $messageRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete_temp_events:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete temporary created past days events images.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $images = TemporaryEventVendorsImage::where('created_at','<', date("Y-m-d 00:00:00"))->get(['id', 'image_thumb', 'image_full']);
        foreach($images as $image_instance) {
            // delete images
            if(Storage::disk('s3')->exists('vendor_photos/original/' . $image_instance->image_full)) {
                Storage::disk('s3')->delete('vendor_photos/original/' . $image_instance->image_full);
            }
            if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $image_instance->image_thumb)) {
                Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $image_instance->image_thumb);
            }
            // delete record
            TemporaryEventVendorsImage::where('id', $image_instance->id)->delete();
        }
    }
}
