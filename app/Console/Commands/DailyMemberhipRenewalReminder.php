<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralEmail;
use App\Models\User;
use App\Models\VendorsInformation;

class DailyMemberhipRenewalReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership_renewal:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send membership renewal reminder to user before 5, 2 days of expiral.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = [2, 5]; // days
        foreach ($days as $day) {
            // 1. Get users which memerbship expire in X days
            $upcoming_expire_users = User::getDaysRenewalMembers($day)
                                        ->onlySite()
                                        ->get(['id', 'first_name', 'email']);
            foreach ($upcoming_expire_users as $user) {
                if($user->users_information->notified_membership_renewal_five_days == "N" && $day == 5) {
                    try {
                        $this->emailForReminder($user, $day);
                    } catch(\Exception $e) {
                        \Log::error('Error while sending membership day reminder email to member user id:' . $user->id . ' for day :' . $day);
                    }
                } 
                if($user->users_information->notified_membership_renewal_two_days == "N" && $day == 2) {
                    try {
                        $this->emailForReminder($user, $day);
                    } catch(\Exception $e) {
                        \Log::error('Error while sending membership day reminder email to member user id:' . $user->id . ' for day :' . $day);
                    }
                }
            }
        }
    }

    /**
     * Mail function
     */
    protected function emailForReminder($user, $day)
    {
        $membership_end_date = $user->paid_members[0]->membership_end_date;
        $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
        $mail_req_arry = [
            'mail_name' => '5/2 Days Membership Renewal',
            'mail_replace_vars' => [
                '[%USER_FIRST_NAME%]' => $user->first_name,
                '[%X_DAYS%]' => $day,
                '[%EXPIRE_DURATION%]' => date('F d, Y', strtotime($membership_end_date)),
                '[%RENEWAL_LINK%]' => route('paid-membership')
            ],
            'unsubscribe_link' => $unsubscribe_link
        ];
        Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
        // set notified flag to Y
        $users_information = VendorsInformation::where('vendor_id', $user->id)->first();
        if($day == 5) {
            $users_information->notified_membership_renewal_five_days = "Y";
        } else if($day == 2) {
            $users_information->notified_membership_renewal_two_days = "Y";
        }
        $users_information->save();
    }
}
