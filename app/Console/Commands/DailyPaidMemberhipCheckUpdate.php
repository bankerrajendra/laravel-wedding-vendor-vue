<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class DailyPaidMemberhipCheckUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'member_is_paid:check_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check users table and update paid to 0 if membership expired.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $paid_expired_users = User::getDaysRenewalMembers(0)
        ->onlySite()
        ->get(['id']);
        foreach ($paid_expired_users as $user) {
            \Log::info("expired paid user id:" . $user->id);
            $user_obj = User::find($user->id);
            $user_obj->is_paid = 0;
            $user_obj->save();
        }
    }
}
