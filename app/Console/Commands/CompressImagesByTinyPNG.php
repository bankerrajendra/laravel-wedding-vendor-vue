<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UsersImage;
use App\Models\VendorsImage;
use App\Models\VendorEventImage;
use Illuminate\Support\Facades\Storage;

class CompressImagesByTinyPNG extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compress_images:tinypng_api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compress/Optimize AWS s3 Images using tinypng api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Users Images
        $users_images = UsersImage::where('is_new', '1')
                                ->get(['id', 'image_thumb', 'image_full']);
        foreach($users_images as $image_instance) {
            // compress images
            $filepath_thumb = Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$image_instance->image_thumb);
            $filepath_full = Storage::disk('s3')->url('vendor_photos/original/'.$image_instance->image_full);
            
            try {
                \Tinify\setKey(config('constants.tinify_key'));
                // compress full
                $source_full = \Tinify\fromUrl($filepath_full);
                $compressed_image_full = $source_full->toBuffer();
                Storage::disk('s3')->put('vendor_photos/original/' . $image_instance->image_full, $compressed_image_full, 'public');
                // compress thumb
                $source_thumb = \Tinify\fromUrl($filepath_thumb);
                $compressed_image_thumb = $source_thumb->toBuffer();
                Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $image_instance->image_thumb, $compressed_image_thumb, 'public');
                // update this record
                $update_rec = UsersImage::where('id', $image_instance->id)->first();
                $update_rec->is_new = '0';
                $update_rec->save();
            } catch(\Tinify\AccountException $e) {
                // Verify your API key and account limit.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(\Tinify\ClientException $e) {
                // Check your source image and request options.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(\Tinify\ServerException $e) {
                // Temporary issue with the Tinify API.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(\Tinify\ConnectionException $e) {
                // A network connection error occurred.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(Exception $e) {
                // Something else went wrong, unrelated to the Tinify API.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            }
        }

        // Vendors Images
        $vendors_images = VendorsImage::where('is_new', '1')
                                ->get(['id', 'image_thumb', 'image_full']);
        foreach($vendors_images as $image_instance) {
            // compress images
            $filepath_thumb = Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$image_instance->image_thumb);
            $filepath_full = Storage::disk('s3')->url('vendor_photos/original/'.$image_instance->image_full);
            
            try {
                \Tinify\setKey(config('constants.tinify_key'));
                // compress full
                $source_full = \Tinify\fromUrl($filepath_full);
                $compressed_image_full = $source_full->toBuffer();
                Storage::disk('s3')->put('vendor_photos/original/' . $image_instance->image_full, $compressed_image_full, 'public');
                // compress thumb
                $source_thumb = \Tinify\fromUrl($filepath_thumb);
                $compressed_image_thumb = $source_thumb->toBuffer();
                Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $image_instance->image_thumb, $compressed_image_thumb, 'public');
                // update this record
                $update_rec = VendorsImage::where('id', $image_instance->id)->first();
                $update_rec->is_new = '0';
                $update_rec->save();
            } catch(\Tinify\AccountException $e) {
                // Verify your API key and account limit.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(\Tinify\ClientException $e) {
                // Check your source image and request options.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(\Tinify\ServerException $e) {
                // Temporary issue with the Tinify API.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(\Tinify\ConnectionException $e) {
                // A network connection error occurred.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(Exception $e) {
                // Something else went wrong, unrelated to the Tinify API.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            }
        }

        // Events Images
        $events_images = VendorEventImage::where('is_new', '1')
                                ->get(['id', 'image_thumb', 'image_full']);
        foreach($events_images as $image_instance) {
            // compress images
            $filepath_thumb = Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$image_instance->image_thumb);
            $filepath_full = Storage::disk('s3')->url('vendor_photos/original/'.$image_instance->image_full);
            
            try {
                \Tinify\setKey(config('constants.tinify_key'));
                // compress full
                $source_full = \Tinify\fromUrl($filepath_full);
                $compressed_image_full = $source_full->toBuffer();
                Storage::disk('s3')->put('vendor_photos/original/' . $image_instance->image_full, $compressed_image_full, 'public');
                // compress thumb
                $source_thumb = \Tinify\fromUrl($filepath_thumb);
                $compressed_image_thumb = $source_thumb->toBuffer();
                Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $image_instance->image_thumb, $compressed_image_thumb, 'public');
                // update this record
                $update_rec = VendorsImage::where('id', $image_instance->id)->first();
                $update_rec->is_new = '0';
                $update_rec->save();
            } catch(\Tinify\AccountException $e) {
                // Verify your API key and account limit.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(\Tinify\ClientException $e) {
                // Check your source image and request options.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(\Tinify\ServerException $e) {
                // Temporary issue with the Tinify API.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(\Tinify\ConnectionException $e) {
                // A network connection error occurred.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            } catch(Exception $e) {
                // Something else went wrong, unrelated to the Tinify API.
                \Log::error('Tinify Compression Error: '. $e->getMessage());
                //return redirect('images/create')->with('error', $e->getMessage());
            }
        }
    }
}
