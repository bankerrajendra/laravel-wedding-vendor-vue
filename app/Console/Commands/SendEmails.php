<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Logic\Email\DailyAlertsRepository;

class SendEmails extends Command
{
    protected $dailyAlertsRepo;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily alerts to the users';

    /**
     * Create a new command instance.
     * @param DailyAlertsRepository $dailyAlertsRepo
     * @return void
     */
    public function __construct(DailyAlertsRepository $dailyAlertsRepo)
    {
        parent::__construct();
        $this->dailyAlertsRepo = $dailyAlertsRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->dailyAlertsRepo->sendDailyAlerts();
    }
}
