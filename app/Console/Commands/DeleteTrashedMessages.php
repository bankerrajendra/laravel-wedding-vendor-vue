<?php

namespace App\Console\Commands;

use App\Logic\MessageRepository;
use Illuminate\Console\Command;

class DeleteTrashedMessages extends Command
{
    protected $messageRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete_trashed:messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete trashed messages created before 30 days.';

    /**
     * Create a new command instance.
     *
     * DeleteTrashedMessages constructor.
     *
     * @param MessageRepository $messageRepository
     */
    public function __construct(MessageRepository $messageRepository)
    {
        parent::__construct();
        $this->messageRepository = $messageRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->messageRepository->moveCronTrashToDelete(30);
    }
}
