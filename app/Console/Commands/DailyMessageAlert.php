<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminEmail;
use App\Models\User;

class DailyMessageAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DailyMessageAlert:daily_message_alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily unread message alert notification to user every day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 1. get active users which are not last active since last 1 week
        $users = User::getActiveAllUsers()->onlySite()->get(['id', 'email', 'first_name']);
        foreach ($users as $user) {
            // get user type and its flag notification
            if($user->hasRole('vendor')) {
                $notify_sends_message = $user->vendors_information->notify_sends_message;
            } else {
                $notify_sends_message = $user->get_user_information()->notify_sends_message;
            }
            // 2. get all type of counts
            $all_counts = getUserActivityCounts($user);
            // 3. prepare the email body.
            $mail_send = false;
            $message_body  = '';
            $message_body .= '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px">
	                            <tbody>
		                            <tr>
			                            <td colspan="3">
			                                <div class="contentEditableContainer contentTextEditable">
                                            <div align="left" class="contentEditable" style="padding-left: 10px; padding-right: 10px;">
                                            <h3>Hello '.$user->first_name.',</h3>';
            // 4. check not unsubscribed
            if($notify_sends_message == "Y") {
                if(isset($all_counts['messaged']) && $all_counts['messaged'] != 0) {
                    if($all_counts['messaged'] == 1) {
                        $msg_sing_plural = 'message';
                    } else {
                        $msg_sing_plural = 'messages';
                    }
                    $message_body .= '<p>You have <span style="text-decoration: underline;">'.$all_counts['messaged'].'</span> unread '.$msg_sing_plural.' in your account.</p>';
                    $mail_send = true;
                }
            }

            $message_body .= '</div>
                            </div>
                            </td>
                        </tr>
                    </tbody>
                </table>';
            
            // send email if required.    
            if($mail_send == true) {
                $mail_req_arry = [
                    'subject' => getGeneralSiteSetting('site_name') . " Message Alert",
                    'message_body' => $message_body
                ];
                Mail::to($user->email)->send(new AdminEmail($mail_req_arry));
            }
        }
    }
}
