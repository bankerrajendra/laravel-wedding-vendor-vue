<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Report;

class FlagNotificationMembershipExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership_expired:flag_notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send flag notification upon expire of membership.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 1. Get users which memerbship expire in X days
        $expired_users = User::getMembershipExpiredUsers()
        ->onlySite()
        ->get(['id', 'first_name', 'email']);
        foreach ($expired_users as $user) {
            try {
                $membership_end_date = $user->paid_members[0]->membership_end_date;
                // send flag notification
                Report::firstOrCreate(
                    [
                        'from_user' => 1,
                        'to_user' => $user->id,
                        'reason' => 'Membership Expired',
                        'description' => 'Admin notification when membership expired',
                        'admin_reply' => 'Hi, your membership is being expired on '.date('F d, Y', strtotime($membership_end_date)).'.',
                        'type' => 'other'
                    ]
                );
            } catch(\Exception $e) {
                \Log::error('Error while sending flag notification for expired membership user id:' . $user->id . ' for day');
            }
        }
    }
}
