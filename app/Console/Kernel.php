<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\SendEmails::class,
        //'\App\Console\Commands\WeeklyActivityAlert',
        '\App\Console\Commands\DailyMemberhipRenewalReminder',
        '\App\Console\Commands\DeleteTrashedMessages',
        '\App\Console\Commands\FlagNotificationMembershipExpire',
        '\App\Console\Commands\CompressImagesByTinyPNG',
        '\App\Console\Commands\DailyPaidMemberhipCheckUpdate',
        '\App\Console\Commands\DailyMessageAlert',
        '\App\Console\Commands\DeleteTemporaryEventsImages'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // $schedule->command('activations:clean')
        //             ->daily();

        // $schedule->command('email:send')->daily();
        /**
         * For sending activity alert email
         *
         * NEED TO Uncomment  below command
         */
        // $schedule->command('SendActivityAlert:activity_alert')
        //          ->weeklyOn(1, '8:00');
        /**
         * For sending notification to Membership Renewal Reminder on 5,2 days before
         * * NEED TO Uncomment  below command
         */
        $schedule->command('membership_renewal:reminder')
                 ->daily();
        /**
         * For deleting trash messages which are created date as 30 days
         * * NEED TO Uncomment  below command
         */
        $schedule->command('delete_trashed:messages')
                 ->daily();
        /**
         * For sending flag notification upon membership expired
         * * NEED TO Uncomment  below command
         */
        // $schedule->command('membership_expired:flag_notification')
        //          ->daily();

        $schedule->command('compress_images:tinypng_api')
                ->daily();
        /**
         * Get expired users and update user is_paid = 0
         */
        $schedule->command('member_is_paid:check_update')
                ->daily();
        /**
         * For unread message email
         *
         */
        $schedule->command('DailyMessageAlert:daily_message_alert')
                ->daily();
        /**
         * For deleting temporary images for events which are created for event creation
         */
        $schedule->command('delete_temp_events:images')
                 ->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
