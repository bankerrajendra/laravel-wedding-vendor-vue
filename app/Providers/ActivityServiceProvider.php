<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Logic\Activity\ConnectActivityRepository;
use App\Logic\Activity\UserActivityStatusRepository;

class ActivityServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Logic\Activity\ConnectActivityRepository', function ($app) {
            return new ConnectActivityRepository();
        });
        $this->app->bind('App\Logic\Activity\UserActivityStatusRepository', function ($app) {
            return new UserActivityStatusRepository();
        });

    }
}