<?php

namespace App\Providers;

use App\Logic\Common\LocationRepository;
use App\Logic\Common\NotificationCountRepository;
use App\Logic\Common\SearchRepository;
use Illuminate\Support\ServiceProvider;
use App\Logic\Common\UserInformationOptionsRepository;

class CommonServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Logic\Common\LocationRepository', function ($app) {
            return new LocationRepository();
        });

        $this->app->bind('App\Logic\Common\UserInformationOptionsRepository', function ($app){
            return new UserInformationOptionsRepository();
        });

        $this->app->bind('App\Logic\Common\NotificationCountRepository', function ($app){
            return new NotificationCountRepository();
        });

        $this->app->bind('App\Logic\Common\SearchRepository', function ($app){
            return new SearchRepository();
        });
		
		$this->app->bind('App\Logic\Common\ReligionRepository', function ($app){
            return new ReligionRepository();
        });
    }
}
