<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use App\Models\User;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        //Paginator::useBootstrapThree();
        Schema::defaultStringLength(191);
        Validator::extend('phone', function($attribute, $value, $parameters, $validator) {
            return preg_match('%^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$%i', $value) && strlen($value) >= 10;
        });

        Validator::replacer('phone', function($message, $attribute, $rule, $parameters) {
            $attribute = str_replace('_', ' ', $attribute);
            return str_replace(':attribute',$attribute, ':attribute is invalid phone number');
        });

        Validator::extend('alpha_spaces', function ($attribute, $value) {

            // This will only accept alpha and spaces.
            // If you want to accept hyphens use: /^[\pL\s-]+$/u.
            return preg_match('/^[\pL\s]+$/u', $value);

        });

        // validation rule for credit card 2 digit year validation
        Validator::extend('cc_expire_year', function($attribute, $value, $parameters) {
            if (strpos($value, '/') !== false) {
                $expld_month_yr = explode("/", $value);
                return strlen($expld_month_yr[1]) == 2;
            }
        });

        // validation rule for past year
        Validator::extend('past_year', function($attribute, $value) {
            if($value != "") {
                if (strlen($value) == 4) {
                    if($value <= date("Y")) {
                        return true;
                    }
                }
            } else {
                return true;
            }
        });

        Validator::extend('phone_valid', function($attribute, $value) {
            if($value != "") {
                if(is_numeric($value)) {
                    if(strlen($value) >= 8 && strlen($value) <= 15) {
                        return true;
                    }
                }
            } else {
                return true;
            }
        });

        // validation rule for past year
        Validator::extend('emebed_url', function($attribute, $value) {
            if (strpos($value, "youtu.be/") == false && strpos($value, "youtube.com/") == false) { 
                return false;
            } else {
                return true;
            }

        });

        Validator::extend('validate_url', function($attribute, $value) {
            if($value != "") {
                return preg_match("/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/", $value);
            } else {
                return true;
            }
        });

        // validation rule for start and end date of event
        Validator::extend('future_date', function($attribute, $value) {
            $expld_date = explode('.', $value);
            $start_date = $expld_date[2].'-'.$expld_date[1].'-'.$expld_date[0];
            $start_date_check = strtotime(date('Y-m-d', strtotime($start_date) ) );
            $todays = strtotime(date('Y-m-d'));
            if($value != "") {
                if($start_date_check < $todays) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        });

        // summernote text area validation
        // Validator::extend('summernote_textarea', function($attribute, $value) {
        //     if($value != "") {
        //         if($value == '<p><br></p>') {
        //             return false;
        //         }
        //     } else {
        //         return true;
        //     }
        // });

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            //$event->menu->add('MAIN NAVIGATION');
            //User::count();
            $stats=admin_user_counter();
            $event->menu->add([
                'text'        => 'Dashboard',
                'url'         => 'admin/reports',
                'icon'        => ''
            ],
                [
                'text' => 'Site Settings',
                'url'         => '#',
                'icon'        => '',
                'submenu'     =>[
                    [
                        'text'        => 'Logo & Favicon',
                        'url'         => '/admin/site-settings/logo-favicon',
                        'icon'        => '',
                    ],
                    [
                        'text'        => 'Matri Prefix',
                        'url'         => '/admin/site-settings/matri-prefix',
                        'icon'        => '',
                    ],
                    [
                        'text'        => 'Email',
                        'url'         => '/admin/site-settings/email',
                        'icon'        => '',
                    ],
                    [
                        'text'        => 'Basic Site Settings',
                        'url'         => '/admin/site-settings/basic',
                        'icon'        => '',
                    ],
                    [
                        'text'        => 'Social Media Link',
                        'url'         => '/admin/site-settings/social-media',
                        'icon'        => '',
                    ],
                    [
                        'text'        => 'Change Password',
                        'url'         => '/admin/site-settings/change-password',
                        'icon'        => '',
                    ]
                ]
            ],
                [
                    'text'        => 'Other Basic Details',
                    'url'         => '',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'Religion',
                            'url'         => 'admin/manage-religions',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Country',
                            'url'         => 'admin/manage-countries',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'State',
                            'url'         => 'admin/manage-states',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'City',
                            'url'         => 'admin/manage-cities',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Website FAQs',
                            'url'         => '#',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Banner',
                            'url'         => 'admin/banners',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text' => 'Vendors Management',
                    'url'         => '/vendors',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'Add Vendors',
                            'url'         => '#',
                            'icon'        => '',
                        ],[
                            'text'        => 'All Vendors ('.$stats['all_vendors'].')',
                            'url'         => '/vendors',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Inactive Vendors ('.$stats['inactive_vendors'].')',
                            'url'         => '/inactive-vendors',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Deleted Vendors ('.$stats['deleted_vendors'].')',
                            'url'         => '/vendors/deleted',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Latest Updates ('.$stats['latest_updates_vendors'].')',
                            'url'         => '/admin/latest-updates-vendors',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Upcoming Renewal ('.$stats['upcoming_renewal_members'].')',
                            'url'         => '/admin/upcoming-renewal',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Planwise(Free/Paid) Vendors',
                            'url'         => '/admin/planwise-members',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Featured Vendors',
                            'url'         => '#',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Expired Vendors ('.$stats['expired_members'].')',
                            'url'         => '/admin/expired-members',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text' => 'Events Management',
                    'url'         => '/admin/events',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'All Events ('.$stats['all_events'].')',
                            'url'         => '/admin/events',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Reviews',
                            'url'         => '/admin/events-reviews/list',
                            'icon'        => '',
                        ],
                        // [
                        //     'text'        => 'Latest Updates ('.$stats['latest_updates_vendors'].')',
                        //     'url'         => '/admin/latest-updates-vendors',
                        //     'icon'        => '',
                        // ],
                        // [
                        //     'text'        => 'Upcoming Renewal ('.$stats['upcoming_renewal_members'].')',
                        //     'url'         => '/admin/upcoming-renewal',
                        //     'icon'        => '',
                        // ],
                        // [
                        //     'text'        => 'Planwise(Free/Paid) Vendors',
                        //     'url'         => '/admin/planwise-members',
                        //     'icon'        => '',
                        // ],
                        // [
                        //     'text'        => 'Featured Vendors',
                        //     'url'         => '#',
                        //     'icon'        => '',
                        // ]
                    ]
                ],
                [
                    'text' => 'Artists Management',
                    'url'         => '/admin/artists',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'All Artists ('.$stats['all_artists'].')',
                            'url'         => '/admin/artists',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'Satellite Management',
                    'url'         => '/admin/satellites',
                    'icon'        => ''
                ],
                [
                    'text' => 'Vendor Category Management',
                    'url'         => '/admin/vendor-categories',
                    'icon'        => ''
                ],
                [
                    'text'        => 'Vendor FAQ Management',
                    'url'         => '/admin/vendor-faqs',
                    'icon'        => '',
                ],
                [
                    'text' => 'Users Management',
                    'url'         => '/users',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'All Users ('.$stats['all_users'].')',
                            'url'         => '/users',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Inactive Users ('.$stats['inactive_users'].')',
                            'url'         => '/inactive-users',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Deleted Users ('.$stats['deleted_users'].')',
                            'url'         => '/users/deleted',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Latest Updates ('.$stats['latest_updates'].')',
                            'url'         => '/admin/latest-updates',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'Vendor Reports',
                    'url'         => '#',
                    'icon'        => ''
                ],
                [
                    'text'        => 'Payment Gateways',
                    'url'         => 'admin/manage-payment-gateways',
                    'icon'        => ''
                ],
                [
                    'text'        => 'How can we improve',
                    'url'         => 'admin/improves/list',
                    'icon'        => ''
                ],
                [
                    'text'        => 'Newsletter',
                    'url'         => 'admin/templates/news-letter',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'Newsletter Listing',
                            'url'         => 'admin/templates/news-letter/list',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Add New Newsletter',
                            'url'         => 'admin/templates/news-letter/add',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Send Newsletter',
                            'url'         => 'admin/templates/news-letter/send',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Subscribers List',
                            'url'         => 'admin/templates/news-letter/subscribers',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'Email Templates',
                    'url'         => 'admin/templates/email',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'Email Templates',
                            'url'         => 'admin/templates/email/list',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Add Email Templates',
                            'url'         => 'admin/templates/email/add',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'SMS Templates',
                    'url'         => 'admin/templates/sms',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'SMS Configuration',
                            'url'         => 'admin/templates/sms/config',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'SMS Templates',
                            'url'         => 'admin/templates/sms/list',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Add SMS Template',
                            'url'         => 'admin/templates/sms/add',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'Directory Management',
                    'url'         => 'admin/cms-city-state',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'Country',
                            'url'         => 'admin/cms-city-state/Country',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'State',
                            'url'         => 'admin/cms-city-state/State',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'City',
                            'url'         => 'admin/cms-city-state/City',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Language',
                            'url'         => 'admin/cms-city-state/Language',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Religion',
                            'url'         => 'admin/cms-city-state/Religion',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Sect',
                            'url'         => 'admin/cms-city-state/Community',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'CMS Pages',
                    'url'         => 'admin/cms',
                    'icon'        => '',
                ],
                [
                    'text'        => 'Blog',
                    'url'         => 'admin/blog',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'Blogs',
                            'url'         => 'admin/blog',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Blog Comments',
                            'url'         => 'admin/blog/comments',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'Membership',
                    'url'         => 'admin/membership-plans',
                    'icon'        => '',
                    'submenu'     => [
                        [
                            'text'        => 'Membership On/Off',
                            'url'         => 'admin/membership-on-off',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Plans',
                            'url'         => 'admin/membership-plans',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Benefits',
                            'url'         => 'admin/membership-benefits',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'Vendor Reviews',
                    'url'         => 'admin/reviews/list',
                    'icon'        => ''
                ]
            );
        });
        /**
         * Site membership status
         */
        $membership_status = getGeneralSiteSetting('membership_status');
        View::share('site_membership_status', $membership_status);
        /**
         * Site Satellites
         */
        $siteSatellitesLists = getSiteSatellites();
        View::share('site_satellites_lists', $siteSatellitesLists);

        view()->composer('layouts.app', 'App\Http\ViewComposers\AppComposer');
    }





    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->environment() == 'local') {
            $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
        }
    }
}
