<?php

namespace App\Providers;

use App\Logic\Email\DailyAlertsRepository;
use Illuminate\Support\ServiceProvider;

class EmailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Logic\Email\DailyAlertsRepository', function ($app) {
            return new DailyAlertsRepository();
        });
    }
}
