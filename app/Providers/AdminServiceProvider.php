<?php

namespace App\Providers;

use App\Logic\Admin\BlogRepository;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Logic\Admin\CmsCityStateRepository', function ($app) {
            return new CmsCityStateRepository();
        });

        $this->app->bind('App\Logic\Admin\CmsPagesRepository', function ($app) {
            return new CmsPagesRepository();
        });

        $this->app->bind('App\Logic\Admin\BlogRepository',function ($app){
            return new BlogRepository();
        });

        $this->app->bind('App\Logic\Admin\MessageRepository',function($app) {
            return new MessageRepository();
        });
    }
}
