<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use App\Models\User;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        //Paginator::useBootstrapThree();
        Schema::defaultStringLength(191);
        Validator::extend('phone', function($attribute, $value, $parameters, $validator) {
            return preg_match('%^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$%i', $value) && strlen($value) >= 10;
        });

        Validator::replacer('phone', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute',$attribute, ':attribute is invalid phone number');
        });

        Validator::extend('alpha_spaces', function ($attribute, $value) {

            // This will only accept alpha and spaces.
            // If you want to accept hyphens use: /^[\pL\s-]+$/u.
            return preg_match('/^[\pL\s]+$/u', $value);

        });

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            //$event->menu->add('MAIN NAVIGATION');
            //User::count();
            $event->menu->add([
                'text' => 'Users',
                'url'         => '/users',
                'icon'        => '',
                'submenu'     =>[
                    [
                        'text'        => 'Active Users (' . User::where('banned', 0)->where('deactivated', 0)->count() . ')',
                        'url'         => '/users',
                        'icon'        => '',
                    ],
                    [
                        'text'        => 'Featured Users (' . User::where('featured', 1)->where('banned', 0)->where('deactivated', 0)->count() . ')',
                        'url'         => '/featured-users',
                        'icon'        => '',
                    ],
                    [
                        'text'        => 'Banned Users (' . User::where('banned', 1)->count() . ')',
                        'url'         => '/banned-users',
                        'icon'        => '',
                    ],
                    [
                        'text'        => 'Deactivated Users (' . User::where('deactivated', 1)->count() . ')',
                        'url'         => '/deactivated-users',
                        'icon'        => '',
                    ],
                    [
                        'text'        => 'Deleted Users (' . User::onlyTrashed()->count() . ')',
                        'url'         => '/users/deleted',
                        'icon'        => '',
                    ]
                ]
            ],
                [
                    'text'        => 'User Reports',
                    'url'         => 'admin/reports',
                    'icon'        => ''
                ],
                [
                    'text'        => 'Directory Management',
                    'url'         => 'admin/cms-city-state',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'Country',
                            'url'         => 'admin/cms-city-state/Country',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'State',
                            'url'         => 'admin/cms-city-state/State',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'City',
                            'url'         => 'admin/cms-city-state/City',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Language',
                            'url'         => 'admin/cms-city-state/Language',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Religion',
                            'url'         => 'admin/cms-city-state/Religion',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Sect',
                            'url'         => 'admin/cms-city-state/Community',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'CMS Pages',
                    'url'         => 'admin/cms',
                    'icon'        => '',
                ],
                [
                    'text'        => 'Blog',
                    'url'         => 'admin/blog',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'Blogs',
                            'url'         => 'admin/blog',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Blog Comments',
                            'url'         => 'admin/blog/comments',
                            'icon'        => '',
                        ]
                    ]
                ],
                [
                    'text'        => 'Membership',
                    'url'         => 'admin/membership-plans',
                    'icon'        => '',
                    'submenu'     => [
                        [
                            'text'        => 'Plans',
                            'url'         => 'admin/membership-plans',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Benefits',
                            'url'         => 'admin/membership-benefits',
                            'icon'        => '',
                        ]
                    ] 
                ]
                ,
                [
                    'text'        => 'Other Basic Details',
                    'url'         => '',
                    'icon'        => '',
                    'submenu'     =>[
                        [
                            'text'        => 'Religion',
                            'url'         => 'admin/manage-religions',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Sects',
                            'url'         => 'admin/manage-sects',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Sub-caste/Community',
                            'url'         => 'admin/manage-subcastes',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Country',
                            'url'         => 'admin/manage-countries',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'State',
                            'url'         => 'admin/manage-states',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'City',
                            'url'         => 'admin/manage-cities',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Languages',
                            'url'         => 'admin/manage-languages',
                            'icon'        => '',
                        ],
                        [
                            'text'        => 'Banner',
                            'url'         => 'admin/manage-banners',
                            'icon'        => '',
                        ]


                    ]
                ]
				);

        });
    }





    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->environment() == 'local') {
            $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
        }
    }
}
