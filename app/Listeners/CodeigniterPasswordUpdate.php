<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Attempting;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Hash;

class CodeigniterPasswordUpdate
{
    public function handle(Attempting $event)
    {
        if(\App\Models\User::where('email', $event->credentials['email'])->first()!=null) {
            $this->check($event->credentials['email'], $event->credentials['password'], \App\Models\User::where('email', $event->credentials['email'])->first()->password ?? 'not found');
        }
    }

    public function check($email, $value, $hashedValue, array $options = [])
    {
        if($this->needsRehash($hashedValue))
        {
            if(md5($value) == $hashedValue) {
                $newHashedValue = Hash::make($value);
                \Illuminate\Support\Facades\DB::update('UPDATE users SET `password` = "'.$newHashedValue.'" WHERE `password` = "'.$hashedValue.'" AND `email` = "'.$email.'"');
                $hashedValue = $newHashedValue;
            }
        }
    }

    public function needsRehash($hashedValue, array $options = [])
    {
        return substr($hashedValue, 0, 4) != '$2y$';
    }

    // DRUPAL PASSWORD FUNCTIONS
    function user_check_password($salt, $password, $stored_hash) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);
        return ($hash && $stored_hash == $hash);
    }
}