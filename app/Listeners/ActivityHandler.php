<?php

namespace App\Listeners;

use App\Events\Activity;
use App\Logic\MessageRepository;
use App\Models\BlockedUser;
use App\Models\FavouritedUser;
use App\Models\Message;
use App\Models\PhotoNotification;
use App\Models\ShortlistedUser;
use App\Models\SkippedUser;
use App\Models\User;
use App\Models\ViewedUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
//use Talk;
use Carbon\Carbon;
use App\Mail\GeneralEmail;

class ActivityHandler
{
    protected $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->user = Auth::user();
    }

    /**
     * Handle the event.
     *
     * @param  Activity  $event
     * @return array
     */
    public function handle(Activity $event)
    {
        //
        if($event->getAction() == config('constants.activity.like')){
            return $this->likeProfile($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.skip')){
            return $this->skipProfile($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.shortlist')){
            return $this->shortlistProfile($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.view_profile')){
             $this->viewProfile($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.delete_viewed_me')){
            return $this->deleteViewedMe($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.delete_shortlisted')){
            return $this->deleteShortlisted($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.like_back')){
            return $this->likeBackProfile($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.delete_liked')){
            return $this->deleteLikedProfile($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.delete_skipped')){
            return $this->deleteSkippedProfile($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.block_profile')){
            return $this->blockProfile($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.unblock_profile')){
            return $this->unblockProfile($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.share_photos')){
            return $this->sharePhotos($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.unshare_photos')){
            return $this->unsharePhotos($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.request-toview')){
            return $this->requestToView($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.accept_request_photos')){
            return $this->acceptPhotoRequest($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.deny_request_photos')){
            return $this->denyPhotoRequest($event->getProfileId());
        }
        elseif($event->getAction() == config('constants.activity.send-photo-request')){
            return $this->sendPhotoRequest($event->getProfileId());
        }
    }

    public function likeProfile($profileId){
        $response = null;

        if(ShortlistedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() > 0){
           $alreadyShortlisted = ShortlistedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->first();
           $alreadyShortlisted->delete();
        }
        if(FavouritedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() > 0){
            $delLike=$this->deleteLikedProfile($profileId);
            $response = [
                'status' => 'deleted',
                //'message'=>'User already in liked list'
            ];
        }
        else {
            $favouriteEntry=FavouritedUser::where('to_user','=',$this->user->id)->where('from_user', $profileId)->first();
            if($favouriteEntry){
                if($favouriteEntry->to_user_status == '1') {
                    $delLike=$this->deleteLikedProfile($profileId);
                    $response = [
                        'status' => 'deleted',
                        //'message'=>'User already in liked list'
                    ];
                }
                else {
                    $favouriteEntry = FavouritedUser::where('to_user', '=', $this->user->id)->where('from_user', $profileId)->first();
                    $favouriteEntry->to_user_status = 1;
                    $favouriteEntry->save();
                    $message = config('constants.messages.like_back');

                    //Message::sendMessageByUserId($profileId, $message);

                    $toUser = User::where('id', $profileId)->first();

                    try {
                        $this->sendActivityMail($toUser, 'Someone favorites me', 'notify_favorites_me');
                    } catch (\Exception $e) {

                    }
                    $response = [
                        'status' => 1,
                        'message' => 'User has been liked back'
                    ];
                }
            }
            else{
                FavouritedUser::create(
                    [
                        'from_user' => $this->user->id,
                        'to_user'=>$profileId,
                    ]
                );
                //$user = User::get($profileId);
                $message = config('constants.messages.like');
                //Talk::sendMessageByUserId($profileId, $message);

               // Message::sendMessageByUserId($profileId, $message);

                $toUser = User::where('id',$profileId)->first();

                try {
                    $this->sendActivityMail($toUser, 'Someone favorites me', 'notify_favorites_me');
                }
                catch (\Exception $e){

                }
                $response = [
                    'status' => 1,
                    'message'=>'User added in liked list'
                ];
            }
        }
        $response['remainingShortlisted'] = ShortlistedUser::where('from_user', '=', $this->user->id)->skipToConditions()->count();
        return $response;
    }

    public function shortlistProfile($profileId){
        $response = null;
        if(ShortlistedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() > 0){
            $response = [
                'status' => 0,
                'message'=>'User already in shortlisted list'
            ];
        }
        else{
            ShortlistedUser::create(
                [
                    'from_user' => $this->user->id,
                    'to_user'=>$profileId,
                ]
            );
            $response = [
                'status' => 1,
                'message'=>'User added in shortlisted list'
            ];
        }
        return $response;
    }

    public function skipProfile($profileId){
        $response = null;
        if(SkippedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() > 0){
            $response = [
                'status' => 0,
                'message'=>'User already in skipped users list'
            ];
        }
        else{
            SkippedUser::create(
                [
                    'from_user' => $this->user->id,
                    'to_user'=>$profileId,
                ]
            );
            $response = [
                'status' => 1,
                'message'=>'User added in skipped list'
            ];
        }
        $response['remainingLikedMe'] = FavouritedUser::where('to_user', '=', $this->user->id)->where('to_user_status', 0)->skipFromConditions()->count();
        return $response;
    }

    public function viewProfile($profileId){
        if(ViewedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() == 0){
            ViewedUser::create(
                [
                    'from_user' => $this->user->id,
                    'to_user'=>$profileId,
                ]
            );
        } else {
            $viewedmeEntry = ViewedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->first();
            $viewedmeEntry->updated_at = Carbon::now();
            $viewedmeEntry->save();
        }
        try {
            $toUser = User::where('id', $profileId)->first();
            $this->sendActivityMail($toUser, 'Someone views my profile', 'notify_views_profile');
        } catch (\Exception $e) {

        }
    }

    public function deleteViewedMe($profileId){
        $response = null;
        if(ViewedUser::where('to_user','=',$this->user->id)->where('from_user', $profileId)->count() > 0){
            $viewedRecord = ViewedUser::where('to_user','=',$this->user->id)->where('from_user', $profileId)->first();
            $viewedRecord->delete();
            $response = [
                'status' => 1,
                'message'=>'User removed from viewed list'
            ];
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in viewed list'
            ];
        }
        $response['remainingViewedMe'] = ViewedUser::where('to_user', '=', $this->user->id)->skipFromConditions()->count();
        return $response;
    }

    public function deleteShortlisted($profileId){
        $response = null;
        if(ShortlistedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() > 0){
            $shortlistedRecord = ShortlistedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->first();
            $shortlistedRecord->delete();
            $response = [
                'status' => 1,
                'message'=>'User removed from shortlisted list'
            ];
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in shortlisted list'
            ];
        }
        $response['remainingShortlisted'] = ShortlistedUser::where('from_user', '=', $this->user->id)->skipToConditions()->count();
        return $response;
    }

    public function likeBackProfile($profileId){
        $response = null;
        if(FavouritedUser::where('to_user','=',$this->user->id)->where('from_user', $profileId)->count() > 0){
            $favouriteEntry = FavouritedUser::where('to_user','=',$this->user->id)->where('from_user', $profileId)->first();
            $favouriteEntry->to_user_status = 1;
            $favouriteEntry->save();
            $response = [
                'status' => 1,
                'message'=>'User has been liked back'
            ];
            //$user = User::get($profileId);
            $message = config('constants.messages.like_back');
//            Talk::sendMessageByUserId($profileId, $message);
           // Message::sendMessageByUserId($profileId, $message);
            $toUser = User::where('id',$profileId)->first();
            try {
                $this->sendActivityMail($toUser, 'Someone favorites me', 'notify_favorites_me');
            }
            catch (\Exception $e){
            }
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in liked me list'
            ];
        }
        $response['remainingLikedMe'] = FavouritedUser::where('to_user', '=', $this->user->id)->where('to_user_status',0)->skipToConditions()->count();
        return $response;
    }

    public function deleteLikedProfile($profileId){
        $response = null;
        if(FavouritedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->where('to_user_status', 0)->count() > 0){
            $favouritedRecord = FavouritedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->where('to_user_status', 0)->first();
            $favouritedRecord->delete();
            $response = [
                'status' => 1,
                'message'=>'User removed from favourited list'
            ];
        }elseif(FavouritedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->where('to_user_status', 1)->count() > 0){
            $favouritedRecord = FavouritedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->where('to_user_status', 1)->first();
            $favouritedRecord->from_user = $profileId;
            $favouritedRecord->to_user = $this->user->id;
            $favouritedRecord->to_user_status = 0;
            $favouritedRecord->save();
            $response = [
                'status' => 1,
                'message'=>'User removed from favourited list'
            ];
        }elseif(FavouritedUser::where('to_user','=',$this->user->id)->where('from_user', $profileId)->where('to_user_status', 1)->count() > 0){
            $favouritedRecord = FavouritedUser::where('to_user','=',$this->user->id)->where('from_user', $profileId)->where('to_user_status', 1)->first();
            $favouritedRecord->to_user_status = 0;
            $favouritedRecord->save();
            $response = [
                'status' => 1,
                'message'=>'User removed from favourited list'
            ];
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in favourited list'
            ];
        }
        $user = $this->user;
        $response['remainingFavourited'] = FavouritedUser::where('from_user', $this->user->id)->orWhere(function($query) use ($user){
            $query->where('to_user', $user->id)->where('to_user_status', 1);
        })->skipToConditions()->count();
        return $response;
    }

    public function deleteSkippedProfile($profileId){
        $response = null;
        if(SkippedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() > 0){
            $skippedRecords = SkippedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->first();
            $skippedRecords->delete();
            $response = [
                'status' => 1,
                'message'=>'User removed from skipped list'
            ];
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in skipped list'
            ];
        }
        $response['remainingSkipped'] = SkippedUser::where('from_user', '=', $this->user->id)->skipToConditions()->count();
        return $response;
    }

    public function unblockProfile($profileId){
        $response = null;
        if(BlockedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() > 0){
            $blockedRecord = BlockedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->first();
            $blockedRecord->delete();
            $response = [
                'status' => 1,
                'message'=>'User removed from blocked list'
            ];
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in blocked list'
            ];
        }
        $response['remainingBlocked'] = BlockedUser::where('from_user', '=', $this->user->id)->count();
        return $response;
    }

    public function blockProfile($profileId){
        $response = null;
        if(BlockedUser::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() > 0){
            $response = [
                'status' => 0,
                'message'=>'User already in blocked list'
            ];
        } else {
            BlockedUser::create(
                [
                    'from_user' => $this->user->id,
                    'to_user'=>$profileId,
                ]
            );
            $response = [
                'status' => 1,
                'message'=>'User added in Blocked list'
            ];
        }
        return $response;
    }

    public function sharePhotos($profileId){
        $response = null;
        if(PhotoNotification::where('from_user','=',$this->user->id)->where('to_user', $profileId)->where('notification_type', 'share_photo')->count() > 0){
            $response = [
                'status' => 0,
                'message'=>'User already in shared list'
            ];
        }
        else{
            PhotoNotification::create(
                [
                    'from_user' => $this->user->id,
                    'to_user'=>$profileId,
                    'notification_type' => 'share_photo',
                ]
            );

            $message = config('constants.messages.share_photo');
            Message::sendMessageByUserId($profileId, $message);

            $response = [
                'status' => 1,
                'message'=>'User added in shared list'
            ];
        }
        return $response;
    }
    public function unsharePhotos($profileId){
        $response = null;
        if(PhotoNotification::where('from_user','=',$this->user->id)->where('to_user', $profileId)->count() > 0){
            $blockedRecord = PhotoNotification::where('from_user','=',$this->user->id)->where('to_user', $profileId)->where('notification_type', 'share_photo')->first();
            $blockedRecord->delete();
            $response = [
                'status' => 1,
                'message'=>'User removed from shared list'
            ];
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in shared list'
            ];
        }
        return $response;
    }

    public function requestToView($profileId){
        $response = null;
        if(PhotoNotification::where('from_user','=',$this->user->id)->where('to_user', $profileId)->where('notification_type', 'private_photo_request')->count() > 0){
            $response = [
                'status' => 0,
                'message'=>'Request already sent.'
            ];
        }
        else{
            PhotoNotification::create(
                [
                    'from_user' => $this->user->id,
                    'to_user'=>$profileId,
                    'notification_type' => 'private_photo_request',
                    'status' => 'P',
                ]
            );

            $message = config('constants.messages.private_photo_request');
            Message::sendMessageByUserId($profileId, $message);

            $response = [
                'status' => 1,
                'message'=>'A request to view private photos has been sent'
            ];
        }
        return $response;
    }

    public function sendPhotoRequest($profileId){
        $response = null;
        if(PhotoNotification::where('from_user','=',$this->user->id)->where('to_user', $profileId)->where('notification_type', 'profile_photo_request')->count() > 0){
            $response = [
                'status' => 0,
                'message'=>'Request already sent.'
            ];
        }
        else{
            PhotoNotification::create(
                [
                    'from_user' => $this->user->id,
                    'to_user'=>$profileId,
                    'notification_type' => 'profile_photo_request',
                    'status' => 'P',
                ]
            );

            $message = config('constants.messages.send_photo_request');
            Message::sendMessageByUserId($profileId, $message);
            /**
             * Send photo request email
             */
            $toUser = User::where('id', $profileId)->first();
            try {
                $this->sendActivityMail($toUser, 'Photo Upload Request', 'notify_content_approve_deny');
            } catch (\Exception $e) {

            }

            $response = [
                'status' => 1,
                'message'=>'A request to upload profile photos has been sent'
            ];
        }
        return $response;
    }



    public function acceptPhotoRequest($profileId){
        $response = null;
        if(PhotoNotification::where('to_user','=',$this->user->id)->where('from_user', $profileId)->where('notification_type', 'private_photo_request')->count() > 0){
            $photoRequestEntry = PhotoNotification::where('to_user','=',$this->user->id)->where('from_user', $profileId)->where('notification_type', 'private_photo_request')->first();
            $photoRequestEntry->status = 'A';
            $photoRequestEntry->save();

            $message = config('constants.messages.accept_photo_request');
            Message::sendMessageByUserId($profileId, $message);
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in the list'
            ];
        }
        return $response;
    }

    public function denyPhotoRequest($profileId){
        $response = null;
        if(PhotoNotification::where('to_user','=',$this->user->id)->where('from_user', $profileId)->where('notification_type', 'private_photo_request')->count() > 0){
            $photoRequestEntry = PhotoNotification::where('to_user','=',$this->user->id)->where('from_user', $profileId)->where('notification_type', 'private_photo_request')->first();
            $photoRequestEntry->delete();

//            $message = config('constants.messages.accept_photo_request');
//            Message::sendMessageByUserId($profileId, $message);
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in the list'
            ];
        }
        return $response;
    }

    protected function sendActivityMail($toUser, $mail_name, $flag)
    {
        if($toUser->get_user_information()->$flag == "Y") {
            $type = getUnsubscribeType($flag);
            $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => $type ]);
            $mail_req_arry = [
                'mail_name' => $mail_name,
                'mail_replace_vars' => [
                    '[%USER_FIRST_NAME%]' => $toUser->first_name,
                    '[%USER_SENDER_FIRST_NAME%]' => $this->user->first_name,
                    '[%USER_SENDER_PROFILE%]' => $this->user->profileLink()
                ],
                'unsubscribe_link' => $unsubscribe_link
            ];
            Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry)); 
        }
    }

}
