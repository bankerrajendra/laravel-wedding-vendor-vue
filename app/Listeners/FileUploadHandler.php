<?php

namespace App\Listeners;

use App\Events\FileUpload;
use App\Models\User;
use App\Models\UsersImage;
use App\Models\VendorsImage;
use App\Models\TemporaryEventVendorsImage;
use App\Models\VendorEventImage;
use App\Models\VendorsInformation;
use App\Models\VendorEventInformation;
use http\Env\Response;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use File;

class FileUploadHandler
{
    /**
     * Create the event listener
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FileUpload  $event
     * @return array
     */
    public function handle(FileUpload $event)
    {
        $response = null;
        if($event->getType() == config('constants.fileType.image')){
            $response = $this->handleImageUpload($event);
        }
        elseif ($event->getType() == config('constants.fileType.imageUrl')){
            $response = $this->handleImageUploadFromUrl($event);
        } elseif ($event->getType() == config('constants.fileType.blogImage')) {
            $response = $this->handleImageUploadBlog($event);
        } elseif ($event->getType() == config('constants.fileType.vendorCover')) {
            $response = $this->handleVendorCoverUpload($event);
        } elseif ($event->getType() == config('constants.fileType.vendorImage')) {
            $response = $this->handleVendorProfileUpload($event);
        } elseif ($event->getType() == 'eventTempPoster') {
            $response = $this->handleEventTempPosterUpload($event);
        } elseif ($event->getType() == config('constants.fileType.eventTempCover')) {
            $response = $this->handleEventTempCoverUpload($event);
        } elseif ($event->getType() == config('constants.fileType.eventTempImage')) {
            $response = $this->handleEventTempProfileUpload($event);
        } elseif ($event->getType() == config('constants.fileType.userProfilePhoto')) {
            $response = $this->handleUserPhotoUpload($event);
        } elseif ($event->getType() == 'editEventPoster') {
            $response = $this->handleEditEventPosterUpload($event);
        } elseif ($event->getType() == 'editEventCover') {
            $response = $this->handleEditEventCoverUpload($event);
        } elseif ($event->getType() == 'editEventProfile') {
            $response = $this->handleEditEventProfileUpload($event);
        } elseif ($event->getType() == 'vendorcoveradmin') {
            $response = $this->handleAdminVendorCoverUpload($event);
        } elseif ($event->getType() == 'vendorprofileadmin') {
            $response = $this->handleAdminVendorProfileUpload($event);
        }

        return $response;
    }

    public function handleImageUpload($event){
        $user = Auth::user();
        $request = $event->getRequest();

        //$allowed_photos=5; //$event->getId();
        $allowed_photos= (null !== $request->input('allowed_photos'))?$request->input('allowed_photos'):"1";
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required'             => trans('auth.imageRequired'),
                'image.mimes'            => trans('auth.imageMimes'),
            ]);

        //allowed_photos
        if($allowed_photos>0 ){
            if(($allowed_photos>0  && $allowed_photos != '') && (UsersImage::where('user_id', $user->id)->where('image_type',$request->input('photo_type'))->count() >= $allowed_photos)){
                $response = array('status'=>0, 'message'=>'You are allowed to upload '.$allowed_photos.' images. Please <a href="'. route('paid-membership') .'" >upgrade your membership</a>  in order to upload unlimited photos.');
                return $response;
            }
// else {
//                $response = array('status'=>0, 'message'=>'You cannot upload more than 1 image');
//                return $response;
//            }
        }


//        if((config('constants.image_upload_limit') != 0) && (UsersImage::where('user_id', $user->id)->count() >= config('constants.image_upload_limit'))){
//            $response = array('status'=>0, 'message'=>'You cannot upload more then '.config("constants.image_upload_limit").' images');
//            return $response;
//        }
        if($request->hasFile('image')){
            $originalImage = $request->file('image');
            $fileName = md5($user->id.date('YmdHis')).".".$request->image->getClientOriginalExtension();
            $thumb_path = config('constants.thumb_upload_path');
            try{
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make($originalImage->getRealPath());
                    if(property_exists ( $thumb_data , 'rotate' )){
                        if(isset($thumb_data->rotate)){
                            if($thumb_data->rotate == 90){
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90){
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }
                    $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y)->save($thumb_path.$fileName);
                }
                else{
                    Image::make($originalImage->getRealPath())->resize(350, 350)->save($thumb_path.$fileName);
                }
                $request->image->storeAs(config('constants.image_upload_path_rel'),$fileName);

                $t = Storage::disk('s3')->put('user_photos/original/' . $fileName, file_get_contents($originalImage), 'public');
                $originalImageName = Storage::disk('s3')->url('user_photos/original/'.$fileName);

                $t1 = Storage::disk('s3')->put('user_photos/thumb_cropped/' . $fileName, file_get_contents($thumb_path . $fileName), 'public');
                $thumbImageName = Storage::disk('s3')->url('user_photos/thumb_cropped/'.$fileName);

                $is_profile_image=($request->input('set_as_profile'))?($request->input('set_as_profile')):'N';

                $userImage = UsersImage::create([
                    'user_id' => $user->id,
                    'image_full' => $fileName, //config('constants.image_upload_path_rel').$fileName,
                    'image_thumb' => $fileName, //config('constants.thumb_upload_path_rel').$fileName,
                    'image_type' => $request->input('photo_type'),
                    'is_profile_image' => $is_profile_image,
                ]);

                $response = array('status'=>1, 'message'=>'Image uploaded successfully','image_path'  =>config('constants.image_upload_path_rel').$fileName, 'thumb_path' => config('constants.thumb_upload_path_rel').$fileName, 'image_id'=> $userImage->id);

                //with('active_section','public')
                //Session::get('active_section');
                Session::put('active_section',$request->input('photo_type'));

//                $photo_type = $request->input('photo_type');
//                $user_image=UsersImage::where('user_id',$user->id)->get();
//                $user_image_count=$user_image->count();
//                if($user_image_count>=1) {
//
//                    $userImage = UsersImage::where('user_id',$user->id);
//                    $new_user_data = array(
//                        'image_type' => $photo_type,
//                        'image_full' => $fileName, // config('constants.image_upload_path_rel').$fileName,
//                        'image_thumb' => $fileName);
//                    $userImage->fill($new_user_data);
//                    $userImage->save();
//                } else {
//                    $userImage = UsersImage::create([
//                        'user_id' => $user->id,
//                        'image_type' => $photo_type,
//                        'image_full' => $fileName, // config('constants.image_upload_path_rel').$fileName,
//                        'image_thumb' => $fileName, //config('constants.thumb_upload_path_rel').$fileName,
//                    ]);
//                }
                $response = array('status'=>1, 'message'=>'Image uploaded successfully','image_path'  =>config('constants.image_upload_path_rel').$fileName, 'thumb_path' => config('constants.thumb_upload_path_rel').$fileName, 'image_id'=> $userImage->id);
				//echo config('constants.thumb_upload_path_rel');die;
                return $response;
            }catch (\Exception $e){


                $response = array('status'=>0, 'message'=>'Failed to upload image');
                return $response;
            }
        }
        else{
            $response = array('status'=>0, 'message'=>'Failed to upload image');
            return $response;
        }
    }

    public function handleImageUploadFromUrl($event){

        $request = $event->getRequest();
        $imageUrl = $request['imageUrl'];
        $userId = $request['userId'];
        $contentsThumb = file_get_contents($imageUrl."&width=300&height=300");
        $contentsFull = file_get_contents($imageUrl."&width=1920");
        $name = md5($userId.date('YmdHis')).".jpg";
        $thumb_path = config('constants.thumb_upload_path_rel');
        $original_path = config('constants.image_upload_path_rel');
        Storage::put($thumb_path.$name, $contentsThumb);
        Storage::put($original_path.$name, $contentsFull);
        $userImage = UsersImage::create([
            'user_id' => $userId,
            'image_full' => $name, //config('constants.image_upload_path_rel').$name,
            'image_thumb' => $name, //config('constants.thumb_upload_path_rel').$name,
            'image_type' => 'public',   //  by default public for facebook logged in users
        ]);
        $response = array('status'=>1, 'message'=>'Image uploaded successfully','image_path'  =>config('constants.image_upload_path_rel').$name, 'thumb_path' => config('constants.thumb_upload_path_rel').$name, 'image_id'=> $userImage->id);
        return $response;

    }

    public function handleImageUploadBlog($event) {
        //$user = Auth::user();
        $request = $event->getRequest();
        $blog_id=$event->getId();
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required'             => trans('auth.imageRequired'),
                'image.mimes'            => trans('auth.imageMimes'),
            ]);
        //echo $request->hasFile('blog_image');
        if($request->hasFile('blog_image')){
           $originalImage = $request->file('blog_image');
          // print_r($request->blog_image);
           //$fileName = md5(date('YmdHis')).".".$request->blog_image->getClientOriginalExtension();
           $fileName = str_replace('.','',str_replace($request->blog_image->getClientOriginalExtension(),'',$request->blog_image->getClientOriginalName()))."-". $blog_id .".".$request->blog_image->getClientOriginalExtension();

            try{
                //Image::make($originalImage->getRealPath())->resize(249, 249)->save($thumb_path.$fileName);
                $request->blog_image->storeAs(config('constants.blogimage_upload_path_rel'),$fileName);
                /*$userImage = UsersImage::create([
                    'user_id' => $user->id,
                    'image_full' => config('constants.image_upload_path_rel').$fileName
                ]);*/
                $response = array('status'=>1, 'message'=>'Image uploaded successfully','image_path'  =>config('constants.blogimage_upload_path_rel').$fileName, 'blog_id'=> $blog_id, 'file_name'=>$fileName);
                return $response;
            }catch (\Exception $e){
                $response = array('status'=>0, 'message'=>'Failed to upload image');
                return $response;
            }
        }
        else{
            $response = array('status'=>0, 'message'=>'Failed to upload image');
            return $response;
        }

    }

    // vendor cover image upload
    public function handleVendorCoverUpload($event) {
        $user = Auth::user();
        $request = $event->getRequest();
        
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );


        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');

            // check cover name exists
            $companySlugGet = VendorsInformation::where('vendor_id', $user->id)->first(['vendor_id', 'company_slug']);
            $img_name = config('constants.vendor_images_pre_name').$companySlugGet->company_slug;
            if(VendorsImage::where('vendor_id', '<>', $user->id)->where('image_type', 'cover')->where('image_full', '=', $img_name.'.'.$request->image->getClientOriginalExtension())->exists()) {
                // 2. update slug by adding -2
                $imNameCnt = VendorsImage::where('vendor_id', '<>', $user->id)->where('image_type', 'cover')->where('image_full', 'like','%'. $img_name.'%')->count();
                $img_name .= "-".($imNameCnt+1);
            }

            $fileName = $img_name.".".$request->image->getClientOriginalExtension();
            try {
                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');

                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y);
                    $resource = $image->stream()->detach();
                    //$resource = $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y)->save(config('constants.image_upload_path').$fileName);
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(1455, 375)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                $user_image = VendorsImage::where('vendor_id',$user->id)->where('image_type', 'cover')->first();
                $user_image_count = VendorsImage::where('vendor_id',$user->id)->where('image_type', 'cover')->count();
                if($user_image_count >= 1) {
                    // remove old cover image
                    if(Storage::disk('s3')->exists('vendor_photos/original/' . $user_image->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/original/' . $user_image->image_full);
                    }

                    if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $user_image->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $user_image->image_full);
                    }
                    
                    $userImage = VendorsImage::where('vendor_id',$user->id)->where('image_type', 'cover')->first();
                    $userImage->image_type = 'cover';
                    $userImage->image_full = $fileName;
                    $userImage->is_approved = 'N';
                    $userImage->disapprove_reason = null;
                    $userImage->image_thumb = $fileName;
                    $userImage->save();
                } else {
                    $userImage = VendorsImage::create([
                        'vendor_id' => $user->id,
                        'image_full' => $fileName,
                        'image_thumb' => $fileName,
                        'image_type' => 'cover',
                        'is_profile_image' => 'N'
                    ]);
                }

                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName), 
                        'image_id' => $userImage->id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => 'Failed to upload image'
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // vendor profile images upload
    public function handleVendorProfileUpload($event) {
        $user = Auth::user();
        $request = $event->getRequest();
        $allowed_photos = (int) (null !== $request->input('allowed_photos'))?$request->input('allowed_photos'):1;

        // total user profile images
        $total_users_profile_images = VendorsImage::where('vendor_id', $user->id)
                                        ->where('image_type', 'profile')
                                        ->count();

        //allowed_photos
        if($allowed_photos > 0){
            if(
                (
                    $allowed_photos > 0  && 
                    $allowed_photos != ''
                ) 
                && 
                (
                    $total_users_profile_images >= $allowed_photos
                )
            ) {
                return [
                    'status' => 0, 
                    'message' => 'You are allowed to upload '.$allowed_photos.' images. Please <a href="'. route('paid-membership') .'" >upgrade your membership</a>  in order to upload unlimited photos.'
                ];
            }
        }

        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );

        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');

            // check image name exists
            $companySlugGet = VendorsInformation::where('vendor_id', $user->id)->first(['vendor_id', 'company_slug']);
            $img_name = config('constants.vendor_images_pre_name').$companySlugGet->company_slug;
            if(VendorsImage::where('vendor_id', '<>', $user->id)->where('image_type', 'profile')->where('image_full', '=', $img_name.'.'.$request->image->getClientOriginalExtension())->exists()) {
                // 2. update slug by adding -2
                $imNameCnt = VendorsImage::where('vendor_id', '<>', $user->id)->where('image_type', 'profile')->where('image_full', 'like','%'. $img_name.'%')->count();
                $img_name .= "-".($imNameCnt+1);
            }
            // add image number at last
            $img_name .= '-'.$total_users_profile_images+1;

            $fileName = $img_name.".".$request->image->getClientOriginalExtension();
            try {
                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');
                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $resource = $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(350, 350)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                $userImage = VendorsImage::create([
                    'vendor_id' => $user->id,
                    'image_full' => $fileName,
                    'image_thumb' => $fileName,
                    'image_type' => 'profile',
                    'is_profile_image' => 'N',
                ]);

                //determine profile image need to replaced
                $img_div = "img-".((int) $total_users_profile_images+1);
                
                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName),
                        'img_div' => $img_div,
                        'image_id' => $userImage->id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => $e->getMessage()
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // user profile image upload
    public function handleUserPhotoUpload($event) {
        $user = Auth::user();
        $request = $event->getRequest();
        
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );


        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');
            $fileName = md5($user->id.date('YmdHis')).".".$request->image->getClientOriginalExtension();
            try {
                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');

                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')) {
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $resource = $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(1455, 375)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                $user_image = UsersImage::where('user_id',$user->id)->first();
                $user_image_count = UsersImage::where('user_id',$user->id)->count();
                if($user_image_count >= 1) {
                    // remove old profile image
                    if(Storage::disk('s3')->exists('vendor_photos/original/' . $user_image->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/original/' . $user_image->image_full);
                    }

                    if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $user_image->image_thumb)) {
                        Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $user_image->image_thumb);
                    }
                    
                    $userImage = UsersImage::where('user_id',$user->id)->first();
                    //$userImage->image_type = 'public';
                    $userImage->image_full = $fileName;
                    $userImage->image_thumb = $fileName;
                    $userImage->is_approved = 'Y';
                    //$userImage->is_profile_image = 'Y';
                    $userImage->save();
                } else {
                    $userImage = UsersImage::create([
                        'user_id' => $user->id,
                        'image_full' => $fileName,
                        'image_thumb' => $fileName,
                        //'image_type' => 'public',
                        //'is_profile_image' => 'Y',
                        'is_approved' => 'Y'
                    ]);
                }

                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName), 
                        'image_id' => $userImage->id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => $e->getMessage()
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // vendor event poster temp upload
    public function handleEventTempPosterUpload($event) 
    {
        $request = $event->getRequest();
        
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );


        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');
            $image_id = md5($request->session()->get('_token').date('YmdHis').'event_poster');
            $request->session()->put('event_poster', $image_id);
            // remove old image
            $user_image = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'poster')->first();
            $user_image_count = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'poster')->count();
            if($user_image_count >= 1) {
                // remove old poster image
                if(Storage::disk('s3')->exists('vendor_photos/original/' . $user_image->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/original/' . $user_image->image_full);
                }

                if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $user_image->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $user_image->image_full);
                }
            }
            $fileName = $image_id.".".$request->image->getClientOriginalExtension();
            try {

                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');

                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y);
                    $resource = $image->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(600, 400)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                $user_image = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'poster')->first();
                $user_image_count = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'poster')->count();
                if($user_image_count >= 1) {
                    $userImage = TemporaryEventVendorsImage::where('image_id',$image_id)->where('image_type', 'poster')->first();
                    $userImage->image_type = 'poster';
                    $userImage->image_full = $fileName;
                    $userImage->image_thumb = $fileName;
                    $userImage->save();
                } else {
                    $userImage = TemporaryEventVendorsImage::create([
                        'image_id' => $image_id,
                        'image_full' => $fileName,
                        'image_thumb' => $fileName,
                        'image_type' => 'poster'
                    ]);
                }

                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName), 
                        'image_id' => $image_id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => 'Failed to upload image'
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // vendor event cover temp upload
    public function handleEventTempCoverUpload($event) 
    {
        $request = $event->getRequest();
        
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );


        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');
            $image_id = md5($request->session()->get('_token').date('YmdHis').'event_cover');
            $request->session()->put('event_cover', $image_id);
            // remove old image
            $user_image = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->first();
            $user_image_count = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->count();
            if($user_image_count >= 1) {
                // remove old cover image
                if(Storage::disk('s3')->exists('vendor_photos/original/' . $user_image->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/original/' . $user_image->image_full);
                }

                if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $user_image->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $user_image->image_full);
                }
            }
            $fileName = $image_id.".".$request->image->getClientOriginalExtension();
            try {

                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');

                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y);
                    $resource = $image->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(1320, 350)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                $user_image = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->first();
                $user_image_count = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->count();
                if($user_image_count >= 1) {
                    $userImage = TemporaryEventVendorsImage::where('image_id',$image_id)->where('image_type', 'cover')->first();
                    $userImage->image_type = 'cover';
                    $userImage->image_full = $fileName;
                    $userImage->image_thumb = $fileName;
                    $userImage->save();
                } else {
                    $userImage = TemporaryEventVendorsImage::create([
                        'image_id' => $image_id,
                        'image_full' => $fileName,
                        'image_thumb' => $fileName,
                        'image_type' => 'cover'
                    ]);
                }

                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName), 
                        'image_id' => $image_id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => 'Failed to upload image'
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // vendor event profile temp upload
    public function handleEventTempProfileUpload($event) 
    {
        $request = $event->getRequest();
        
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );


        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');
            $image_id = md5($request->session()->get('_token').date('YmdHis').'event_profile');
            $request->session()->put('event_profile', $image_id);
            // delete old image
            $user_image = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'profile')->first();
            $user_image_count = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'profile')->count();
            if($user_image_count >= 1) {
                // remove old profile image
                if(Storage::disk('s3')->exists('vendor_photos/original/' . $user_image->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/original/' . $user_image->image_full);
                }

                if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $user_image->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $user_image->image_full);
                }
            }


            $fileName = $image_id.".".$request->image->getClientOriginalExtension();
            try {

                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');

                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y);
                    $resource = $image->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(500, 500)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                if($user_image_count >= 1) {
                    $userImage = TemporaryEventVendorsImage::where('image_id',$image_id)->where('image_type', 'profile')->first();
                    $userImage->image_type = 'profile';
                    $userImage->image_full = $fileName;
                    $userImage->image_thumb = $fileName;
                    $userImage->save();
                } else {
                    $userImage = TemporaryEventVendorsImage::create([
                        'image_id' => $image_id,
                        'image_full' => $fileName,
                        'image_thumb' => $fileName,
                        'image_type' => 'profile'
                    ]);
                }

                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName), 
                        'image_id' => $image_id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => 'Failed to upload image'
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // edit vendor event cover photo upload
    public function handleEditEventCoverUpload($event) 
    {
        $request = $event->getRequest();
        
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );

        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');
            // 1. get old images
            $event_id = base64_decode($request->input('event_id'));
            $oldImages = VendorEventImage::where('event_id', $event_id)->where('image_type', 'cover')->get();
            if($oldImages->count() > 0) {
                foreach($oldImages as $oldImg) {
                    // 2. delete from s3
                    // remove old cover image
                    if(Storage::disk('s3')->exists('vendor_photos/original/' . $oldImg->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/original/' . $oldImg->image_full);
                    }

                    if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $oldImg->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $oldImg->image_full);
                    }
                    // 3. delete record
                    $oldImg->delete();
                }
            }
            // 4. Save new image to s3
            // image name with naming convention
            $eventSlugGet = VendorEventInformation::where('id', $event_id)->first(['id', 'event_slug']);
            $img_name = config('constants.vendor_images_pre_name').'event-cover-'.$eventSlugGet->event_slug;
            if(VendorEventImage::where('event_id', '<>',$event_id)->where('image_type', 'cover')->where('image_full', '=', $img_name.'.'.$request->image->getClientOriginalExtension())->exists()) {
                // 2. update slug by adding -2
                $imNameCnt = VendorEventImage::where('event_id', '<>',$event_id)->where('image_type', 'cover')->where('image_full', 'like','%'. $img_name.'%')->count();
                $img_name .= "-".($imNameCnt+1);
            }
            $image_id = $img_name;
            $fileName = $img_name.".".$request->image->getClientOriginalExtension();
            
            try {

                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');

                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y);
                    $resource = $image->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(1320, 350)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                $arryToIns = [
                    'event_id' => $event_id,
                    'image_thumb' => $fileName,
                    'image_full'=> $fileName,
                    'is_profile_image' => 'Y',
                    'image_type' => 'cover'
                ];
                $admin_user = Auth::user();
                if($admin_user->hasRole('admin')) {
                    $arryToIns['is_approved'] = 'Y';
                }

                $coverImage = VendorEventImage::create($arryToIns);

                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName), 
                        'image_id' => $image_id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => 'Failed to upload image'
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // edit vendor event poster photo upload
    public function handleEditEventPosterUpload($event) 
    {
        $request = $event->getRequest();
        
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );

        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');
            // 1. get old images
            $event_id = base64_decode($request->input('event_id'));
            $oldImages = VendorEventImage::where('event_id', $event_id)->where('image_type', 'poster')->get();
            if($oldImages->count() > 0) {
                foreach($oldImages as $oldImg) {
                    // 2. delete from s3
                    // remove old poster image
                    if(Storage::disk('s3')->exists('vendor_photos/original/' . $oldImg->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/original/' . $oldImg->image_full);
                    }

                    if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $oldImg->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $oldImg->image_full);
                    }
                    // 3. delete record
                    $oldImg->delete();
                }
            }
            // 4. Save new image to s3
            // image name with naming convention
            $eventSlugGet = VendorEventInformation::where('id', $event_id)->first(['id', 'event_slug']);
            $img_name = config('constants.vendor_images_pre_name').'event-poster-'.$eventSlugGet->event_slug;
            if(VendorEventImage::where('event_id', '<>',$event_id)->where('image_type', 'poster')->where('image_full', '=', $img_name.'.'.$request->image->getClientOriginalExtension())->exists()) {
                // 2. update slug by adding -2
                $imNameCnt = VendorEventImage::where('event_id', '<>',$event_id)->where('image_type', 'poster')->where('image_full', 'like','%'. $img_name.'%')->count();
                $img_name .= "-".($imNameCnt+1);
            }
            $image_id = $img_name;
            $fileName = $img_name.".".$request->image->getClientOriginalExtension();
            
            try {

                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');

                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y);
                    $resource = $image->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(1280, 350)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                $arryToIns = [
                    'event_id' => $event_id,
                    'image_thumb' => $fileName,
                    'image_full'=> $fileName,
                    'is_profile_image' => 'Y',
                    'image_type' => 'poster'
                ];
                $admin_user = Auth::user();
                if($admin_user->hasRole('admin')) {
                    $arryToIns['is_approved'] = 'Y';
                }

                $posterImage = VendorEventImage::create($arryToIns);

                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName), 
                        'image_id' => $image_id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => 'Failed to upload image'
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // edit vendor event profile profile upload
    public function handleEditEventProfileUpload($event) 
    {
        $request = $event->getRequest();
        
        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );

        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');
            // 1. get old images
            $event_id = base64_decode($request->input('event_id'));
            $oldImages = VendorEventImage::where('event_id', $event_id)->where('image_type', 'profile')->get();
            if($oldImages->count() > 0) {
                foreach($oldImages as $oldImg) {
                    // 2. delete from s3
                    // remove old cover image
                    if(Storage::disk('s3')->exists('vendor_photos/original/' . $oldImg->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/original/' . $oldImg->image_full);
                    }

                    if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $oldImg->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $oldImg->image_full);
                    }
                    // 3. delete record
                    $oldImg->delete();
                }
            }
            // 4. Save new image to s3
            // image name with naming convention
            $eventSlugGet = VendorEventInformation::where('id', $event_id)->first(['id', 'event_slug']);
            $img_name = config('constants.vendor_images_pre_name').'event-logo-'.$eventSlugGet->event_slug;
            if(VendorEventImage::where('event_id', '<>',$event_id)->where('image_type', 'profile')->where('image_full', '=', $img_name.'.'.$request->image->getClientOriginalExtension())->exists()) {
                // 2. update slug by adding -2
                $imNameCnt = VendorEventImage::where('event_id', '<>',$event_id)->where('image_type', 'profile')->where('image_full', 'like','%'. $img_name.'%')->count();
                $img_name .= "-".($imNameCnt+1);
            }
            $image_id = $img_name;
            $fileName = $img_name.".".$request->image->getClientOriginalExtension();

            try {

                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');

                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y);
                    $resource = $image->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(500, 500)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                $arryToIns = [
                    'event_id' => $event_id,
                    'image_thumb' => $fileName,
                    'image_full'=> $fileName,
                    'is_profile_image' => 'Y',
                    'image_type' => 'profile'
                ];
                $admin_user = Auth::user();
                if($admin_user->hasRole('admin')) {
                    $arryToIns['is_approved'] = 'Y';
                }
                $coverImage = VendorEventImage::create($arryToIns);

                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName), 
                        'image_id' => $image_id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => 'Failed to upload image'
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // for admin
    // vendor cover image upload
    public function handleAdminVendorCoverUpload($event) {
        $request = $event->getRequest();
        $user = User::where('id', $request->user_id)->first();

        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );


        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');
            // check cover name exists
            $companySlugGet = VendorsInformation::where('vendor_id', $user->id)->first(['vendor_id', 'company_slug']);
            $img_name = config('constants.vendor_images_pre_name').$companySlugGet->company_slug;
            if(VendorsImage::where('vendor_id', '<>', $user->id)->where('image_type', 'cover')->where('image_full', '=', $img_name.'.'.$request->image->getClientOriginalExtension())->exists()) {
                // 2. update slug by adding -2
                $imNameCnt = VendorsImage::where('vendor_id', '<>', $user->id)->where('image_type', 'cover')->where('image_full', 'like','%'. $img_name.'%', 'like','%'. $img_name.'%')->count();
                $img_name .= "-".($imNameCnt+1);
            }
            $fileName = $img_name.".".$request->image->getClientOriginalExtension();
            try {
                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');

                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y);
                    $resource = $image->stream()->detach();
                    //$resource = $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y)->save(config('constants.image_upload_path').$fileName);
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(1455, 375)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                $user_image = VendorsImage::where('vendor_id',$user->id)->where('image_type', 'cover')->first();
                $user_image_count = VendorsImage::where('vendor_id',$user->id)->where('image_type', 'cover')->count();
                if($user_image_count >= 1) {
                    // remove old cover image
                    if(Storage::disk('s3')->exists('vendor_photos/original/' . $user_image->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/original/' . $user_image->image_full);
                    }

                    if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $user_image->image_full)) {
                        Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $user_image->image_full);
                    }
                    
                    $userImage = VendorsImage::where('vendor_id',$user->id)->where('image_type', 'cover')->first();
                    $userImage->image_type = 'cover';
                    $userImage->image_full = $fileName;
                    $userImage->is_approved = 'Y';
                    $userImage->disapprove_reason = null;
                    $userImage->image_thumb = $fileName;
                    $userImage->save();
                } else {
                    $userImage = VendorsImage::create([
                        'vendor_id' => $user->id,
                        'image_full' => $fileName,
                        'image_thumb' => $fileName,
                        'image_type' => 'cover',
                        'is_profile_image' => 'N',
                        'is_approved' => 'Y'
                    ]);
                }

                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName), 
                        'image_id' => $userImage->id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => 'Failed to upload image'
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

    // vendor profile images upload for admin
    public function handleAdminVendorProfileUpload($event) {
        $request = $event->getRequest();
        $user = User::where('id', $request->user_id)->first();
        $allowed_photos = (int) (null !== $request->input('allowed_photos'))?$request->input('allowed_photos'):1;

        // total user profile images
        $total_users_profile_images = VendorsImage::where('vendor_id', $user->id)
                                        ->where('image_type', 'profile')
                                        ->count();

        //allowed_photos
        if($allowed_photos > 0){
            if(
                (
                    $allowed_photos > 0  && 
                    $allowed_photos != ''
                ) 
                && 
                (
                    $total_users_profile_images >= $allowed_photos
                )
            ) {
                return [
                    'status' => 0, 
                    'message' => 'You are allowed to upload '.$allowed_photos.' images. Please <a href="'. route('paid-membership') .'" >upgrade your membership</a>  in order to upload unlimited photos.'
                ];
            }
        }

        $imageUploadValidation = [
            'image' => 'image|mimes:jpg,png,jpeg',
        ];
        $request->validate(
            $imageUploadValidation,
            [
                'image.required' => trans('auth.imageRequired'),
                'image.mimes' => trans('auth.imageMimes'),
            ]
        );

        if( $request->hasFile('image') ) {
            $originalImage = $request->file('image');
            // check image name exists
            $companySlugGet = VendorsInformation::where('vendor_id', $user->id)->first(['vendor_id', 'company_slug']);
            $img_name = config('constants.vendor_images_pre_name').$companySlugGet->company_slug;
            if(VendorsImage::where('vendor_id', '<>', $user->id)->where('image_type', 'profile')->where('image_full', '=', $img_name.'.'.$request->image->getClientOriginalExtension())->exists()) {
                // 2. update slug by adding -2
                $imNameCnt = VendorsImage::where('vendor_id', '<>', $user->id)->where('image_type', 'profile')->where('image_full', 'like','%'. $img_name.'%')->count();
                $img_name .= "-".($imNameCnt+1);
            }
            // add image number at last
            $img_name .= '-'.$total_users_profile_images+1;
            $fileName = $img_name.".".$request->image->getClientOriginalExtension();
            try {
                // save large image to AWS S3 direct
                Storage::disk('s3')->put('vendor_photos/original/' . $fileName, file_get_contents($originalImage), 'public');
                // save thumb image to AWS S3 direct
                if($request->input('thumb_data')){
                    $thumb_data = json_decode($request->input('thumb_data'));
                    $image = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName));
                    if(property_exists ( $thumb_data , 'rotate' )) {
                        if(isset($thumb_data->rotate)) {
                            if($thumb_data->rotate == 90) {
                                $thumb_data->rotate = -90;
                            } elseif($thumb_data->rotate == -90) {
                                $thumb_data->rotate = 90;
                            }
                            $image->rotate($thumb_data->rotate);
                        }
                    }

                    $resource = $image->crop((int)$thumb_data->width, (int)$thumb_data->height, (int)$thumb_data->x, (int)$thumb_data->y)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                } else {
                    $resource = Image::make(Storage::disk('s3')->url('vendor_photos/original/'.$fileName))->resize(350, 350)->stream()->detach();
                    Storage::disk('s3')->put('vendor_photos/thumb_cropped/' . $fileName, $resource, 'public');
                }
                
                $userImage = VendorsImage::create([
                    'vendor_id' => $user->id,
                    'image_full' => $fileName,
                    'image_thumb' => $fileName,
                    'image_type' => 'profile',
                    'is_profile_image' => 'N',
                    'is_approved' => 'Y',
                ]);

                //determine profile image need to replaced
                $img_div = "img-".((int) $total_users_profile_images+1);
                
                return [
                        'status' => 1, 
                        'message' => 'Image uploaded successfully',
                        'image_path' => Storage::disk('s3')->url('vendor_photos/original/'.$fileName), 
                        'thumb_path' => Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$fileName),
                        'img_div' => $img_div,
                        'image_id' => $userImage->id
                ];
            } catch (\Exception $e) {
                return [
                    'status' => 0, 
                    'message' => $e->getMessage()
                ];
            }
        } else {
            return [
                'status' => 0, 
                'message' => 'Failed to upload image'
            ];
        }
    }

}
