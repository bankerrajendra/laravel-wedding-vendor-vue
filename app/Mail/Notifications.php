<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notifications extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $notifications;
    /**
     * Create a new message instance.
     * @param $user
     * @param $notifications
     * @return void
     */
    public function __construct($user, $notifications)
    {
        //
        $this->user = $user;
        $this->notifications = $notifications;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('PunjabiGirl Activity Alert')
            ->view('emails.notification')
            ->with([
                'user'=>$this->user,
                'notifications' => $this->notifications
            ]);
    }
}
