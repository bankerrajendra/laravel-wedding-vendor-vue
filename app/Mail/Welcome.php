<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Logic\Template\TemplateRepository;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;
    private $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        //
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(TemplateRepository $template_repo)
    {
        $email = $template_repo->getTemplateBy('User Registration', 'email')
                                        ->where('status', '=', 'A')
                                        ->first(['id', 'description']);
        if(!empty($email)) {
            $email_subject_obj = $template_repo->getEmailSubject($email->id);
            $mail_replace_vars = [
                '[%WEB_URL%]' => getGeneralSiteSetting('site_url')
            ];
            $email_updated = replaceEmailTemplateVars($email->description, $mail_replace_vars);
            
            return $this->subject($email_subject_obj->subject)
                ->view('emails.layout', [
                    'message_body' => $email_updated
                ]);
        }
    }
}
