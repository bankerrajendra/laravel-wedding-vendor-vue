<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Logic\Template\TemplateRepository;

class CollectReviewEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $mail_details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mail_details)
    {
        $this->mail_details = $mail_details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(TemplateRepository $template_repo)
    {   
        $email = $template_repo->getTemplateBy($this->mail_details['mail_name'], 'email')
                                        ->where('status', '=', 'A')
                                        ->first(['id', 'description']);
        if(!empty($email)) {
            $replace_vars = $this->mail_details['mail_replace_vars'];
            $email_subject_obj = $template_repo->getEmailSubject($email->id);
            $email_subject = replaceEmailTemplateVars($email_subject_obj->subject, $replace_vars);
            $message_body = replaceEmailTemplateVars($this->mail_details['mail_body'], $replace_vars);
            if(isset($this->mail_details['unsubscribe_link'])) {
                $unsubscribe_link = $this->mail_details['unsubscribe_link'];
            } else {
                $unsubscribe_link = "";
            }
            return $this->subject($email_subject)
                ->view('emails.layout', [
                    'message_body' => $message_body,
                    'unsubscribe_link' => $unsubscribe_link
                ]);
        }
    }
}
