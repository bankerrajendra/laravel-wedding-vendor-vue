<?php

namespace App\Http\Controllers;

use App\Logic\Admin\CmsCityStateRepository;
use App\Logic\Common\LocationRepository;
use JsValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Models\CmsCityState;

class CmsCityStateController extends Controller
{
    protected $cmsCityStateService;
    protected $validationRules = [
        'name'                  => 'required|max:20|unique:users',
        'first_name'            => '',
        'last_name'             => '',
        'email'                 => 'required|email|max:255|unique:users',
        'day'                   => 'required',
        'month'                 => 'required',
        'year'                  => 'required',
        'country'               => 'required',
        'city'                  =>  'required',
        'ethnicity'            =>  'required',
        'password'              => 'required|min:6|max:30|confirmed',
        'password_confirmation' => 'required|same:password',
        'g-recaptcha-response'  => '',
        'phone_country_code'    => 'required',
        'phone_number'          => 'required|numeric|phone',
        'captcha'               => 'required|min:1',
    ];
    //
    public function __construct(CmsCityStateRepository $cmsCityStateService)
    {
        $this->cmsCityStateService=$cmsCityStateService;
    }

    /********* Frontend Functions *****/

    public function getStates($country_name) {
		
        $cmsVar=$this->cmsCityStateService->getCountryWiseStatesFront($country_name);

        $allStates = array();
        foreach ($cmsVar as $result){
           $firstChar = $result['type_name'][0];
           $allStates[ucfirst($firstChar)][] = $result;
        }
        $meta_fields['title']=($country_name=='canada'?'Canada Online Indian Dating Service, Indian Women Dating, Girls, Men':'USA Online Indian Dating Service, Indian Women Dating, Girls, Men');
        $meta_fields['keyword']=($country_name=='canada'?'Canada Online Indian Dating Service, Indian Women Dating, Girls, Men':'USA Online Indian Dating Service, Indian Women Dating, Girls, Men');
        $meta_fields['description']=($country_name=='canada'?'Join Punjabigirl.com to meet Canada Indian dating girls, men & women. Verified Contact Numbers. 100% Satisfactions. Join FREE.':'Join Punjabigirl.com to find USA Indian girls, men & women for dating. Verified Numbers. Friendship Satisfactions. Join FREE.');
        return view('pages.cms.cms-state')->with(['statelist'=>$allStates,'country_name'=>strtoupper($country_name),'pagetype'=>'State','pagetypes'=>'States','metafields'=>$meta_fields]);
    }

    public function getCities($country_name) {
        $cmsVar=$this->cmsCityStateService->getCountryWiseCitiesFront($country_name);

        $allCities = array();
        foreach ($cmsVar as $result){
            $firstChar = $result['type_name'][0];
            $allCities[ucfirst($firstChar)][] = $result;
        }
        $meta_fields['title']=($country_name=='canada'?'Canada Online Indian Dating Site, Dating Girls, Dating Men, Dating Women':'USA Online Indian Dating Site, Dating Girls, Dating Men, Dating Women');
        $meta_fields['keyword']=($country_name=='canada'?'Canada Online Indian Dating Site, Dating Girls, Dating Men, Dating Women':'USA Online Indian Dating Site, Dating Girls, Dating Men, Dating Women');
        $meta_fields['description']=($country_name=='canada'?'Meet Canada Indian single girl, men and women for dating. 100% Secure. Verified Users. Register FREE.':'Meet USA Indian single girls, men and women for long relationship. Verified Users. Register FREE.');
        return view('pages.cms.cms-state')->with(['statelist'=>$allCities,'country_name'=>strtoupper($country_name),'pagetype'=>'City','pagetypes'=>'Cities','metafields'=>$meta_fields]);
    }

    public function getLanguages() {
        $site_id= config('constants.site_id');
        $cmsVar=CmsCityState::where('page_type','Language')->where('site_id',$site_id)->get();

        $allCities = array();
        foreach ($cmsVar as $result){
            $firstChar = $result['type_name'][0];
            $allCities[ucfirst($firstChar)][] = $result;
        }
        $meta_fields['title']='Muslim Brides & Grooms Language, Arabic, Urdu, Bengali, Hindi, Malayalam, Assamese, English, Sindhi';
        $meta_fields['keyword']='Muslim Brides & Grooms Language, Arabic, Urdu, Bengali, Hindi, Malayalam, Assamese, English, SindhigetReligion';
        $meta_fields['description']='Find Muslim Brides & Grooms with proper language, Urdu, Arabic, Bengali, Tamil, Malayalam, English, French, Gujarati, Hindi, Kashmiri, Sindhi, Punjabi & Spanish. ';

        return view('pages.cms.cms-state')->with(['statelist'=>$allCities,'country_name'=>'Languages','pagetype'=>'Language','pagetypes'=>'','metafields'=>$meta_fields]);
    }

    public function  getLanguageDetail($language_name){
        $cmsVar=$this->cmsCityStateService->getCmsCityStateDetail('Language',$language_name);
        $meta_fields['title']=$cmsVar[0]->type_name.' Muslim Matrimony, '.$cmsVar[0]->type_name.' Marriage, Brides, Grooms, Matrimonial site';
        $meta_fields['keyword']=$cmsVar[0]->type_name.' Muslim Matrimony, '.$cmsVar[0]->type_name.' Marriage, Brides, Grooms, Matrimonial site';
        $meta_fields['description']=$cmsVar[0]->type_name.' Muslim Nikah Matrimony Profiles, Find perfect match '.$cmsVar[0]->type_name.' Muslim Brides & Grooms. All Profiles Verified!';
        return view('pages.cms.state-detail')->with(['cmsdetail'=>$cmsVar,'countryname'=>$language_name,'metafields'=>$meta_fields]);
    }

    public function  getStateDetail($state_name){
        $cmsVar=$this->cmsCityStateService->getCmsCityStateDetail('State',$state_name);
        $meta_fields['title']=$cmsVar[0]->type_name.' Muslim Matrimony Site, Marriage, Brides, Grooms, Matrimonial';
        $meta_fields['description']='Find Muslim grooms and brides in '.$cmsVar[0]->type_name.' with proper matching. 100% Secure. Verified Contact Details. Most Trusted Brand. Join Free. ';
        $meta_fields['keyword']=$cmsVar[0]->type_name.' Muslim Matrimony Site, Marriage, Brides, Grooms, Matrimonial';
        return view('pages.cms.state-detail')->with(['cmsdetail'=>$cmsVar,'countryname'=>$state_name,'metafields'=>$meta_fields]);
    }

    public function  getCountryDetail($state_name){
        $cmsVar=$this->cmsCityStateService->getCmsCityStateDetail('Country',$state_name);
        $meta_fields['title']=$cmsVar[0]->type_name.'  Muslim Matrimony Site, Marriage, Matrimonial, Brides, Grooms ';
        $meta_fields['description']='Muslimwedding.com is the best place to search Muslim grooms and brides in '. $cmsVar[0]->type_name.' with proper matching profiles. Trusted Brand. Verified Profiles. 100% Safe & Secure. Join FREE.';
        $meta_fields['keyword']=$cmsVar[0]->type_name.'  Muslim Matrimony Site, Marriage, Matrimonial, Brides, Grooms ';
        return view('pages.cms.state-detail')->with(['cmsdetail'=>$cmsVar,'countryname'=>$state_name,'metafields'=>$meta_fields]);
    }

    public function  getCityDetail($state_name,LocationRepository $locationRepository){

        $cmsVar=$this->cmsCityStateService->getCmsCityStateDetail('City',$state_name);
        $meta_fields['title']=$cmsVar[0]->type_name.' Muslim Matrimony Site, Matrimonial, Brides, Grooms ';
        $meta_fields['keyword']=$cmsVar[0]->type_name.' Muslim Matrimony Site, Matrimonial, Brides, Grooms ';
        $meta_fields['description']='Find Muslim life partner in '.$cmsVar[0]->type_name.' with exact matching. 100% Safe and Secure. Verified Contact Information. Trusted Brand. Registration FREE. ';
        return view('pages.cms.state-detail')->with(['cmsdetail'=>$cmsVar,'countryname'=>$state_name,'metafields'=>$meta_fields]);
    }

// this function works for community(sect) and religion both
    public function  getReligion($religion_name){

        if($religion_name=='muslim') {
            $cmsVar=$this->cmsCityStateService->getCmsCityStateDetail('Religion',$religion_name);
            $meta_fields['title']=$cmsVar[0]->type_name.' Nikah Matrimony, '.$cmsVar[0]->type_name.' Marriage, Brides, Grooms, Matrimonial Site';
            $meta_fields['keyword']=$cmsVar[0]->type_name.' Nikah Matrimony, ' .$cmsVar[0]->type_name.' Marriage, Brides, Grooms, Matrimonial Site';
            $meta_fields['description']='Find '.$cmsVar[0]->type_name.' perfect matching brides and grooms for marriage. Secure. Join FREE. Reliable Matrimony Brand. Verified Profiles.';

        } else {
            $cmsVar=$this->cmsCityStateService->getCmsCityStateDetail('Community',$religion_name);
            $meta_fields['title']=$cmsVar[0]->type_name.' Muslim Matrimony, '.$cmsVar[0]->type_name.' Marriage, Brides, Grooms, Matrimonial site';
            $meta_fields['keyword']=$cmsVar[0]->type_name.' Muslim Matrimony, '. $cmsVar[0]->type_name. ' Marriage, Brides, Grooms, Matrimonial site';
            $meta_fields['description']=$cmsVar[0]->type_name.' Marriage Matrimony, Search Brides & Grooms for Nikah '.$cmsVar[0]->type_name.' community. Join FREE!';
        }
        return view('pages.cms.state-detail')->with(['cmsdetail'=>$cmsVar,'countryname'=>$religion_name,'metafields'=>$meta_fields]);
    }

// not using this function . will remove it later
    public function  getSect($sect_name){
        $cmsVar=$this->cmsCityStateService->getCmsCityStateDetail('Community',$sect_name);
        $meta_fields['title']=$cmsVar[0]->type_name.' Muslim Matrimony, '.$cmsVar[0]->type_name.' Marriage, Brides, Grooms, Matrimonial site';
        $meta_fields['keyword']=$cmsVar[0]->type_name.' Muslim Matrimony, '. $cmsVar[0]->type_name. ' Marriage, Brides, Grooms, Matrimonial site';
        $meta_fields['description']=$cmsVar[0]->type_name.' Marriage Matrimony, Search Brides & Grooms for Nikah '.$cmsVar[0]->type_name.' community. Join FREE!';
        return view('pages.cms.state-detail')->with(['cmsdetail'=>$cmsVar,'countryname'=>$sect_name,'metafields'=>$meta_fields]);
    }

    /******** Admin Functions ********/
    public function getCmsCityState($page_type=null) {
        if(!$page_type) {
            $page_type = config('constants.cms.country');
        }
        $cmsVar=$this->cmsCityStateService->getCmsCityState($page_type);
        return view('pages.admin.cms.cms-city-state')->with(['cmscitystate'=>$cmsVar, 'page_type'=> $page_type]);
    }

    public function setCmsCityState($cms_id, Request $request) {
        Session::flash('url',$request->server('HTTP_REFERER'));
        $cmsVar=$this->cmsCityStateService->getCmsCityStateRec($cms_id);
        return view('pages.admin.cms.edit-cms-city-state')->with(['cmscitysaterecord'=>$cmsVar, 'id'=>$cms_id]);
    }

    public function updateCmsCityState($cms_id,Request $request) {
        //echo $cms_id;
        //dd($request);
       // print_r($request->input('type_name'));
        $this->cmsCityStateService->updateCmsCityStateRec($cms_id,$request);
        //return redirect()->back();
        return Redirect::to(Session::get('url'));
    }

}
