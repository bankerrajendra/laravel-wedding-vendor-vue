<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use App\Models\Artist;
use App\Models\User;
use App\Logic\Admin\ArtistRepository;

class ArtistController extends Controller
{
    protected $artistRepository;

    private $validation_rules = [
        'name'           => 'required|min:3|max:200',
        'is_approved'    => 'required',
        'image_full'     => 'sometimes|mimes:jpg,jpeg,gif,bmp,png'
    ];

    // contructor
    public function __construct(ArtistRepository $artistRepository)
    {
        $this->artistRepository = $artistRepository;
    }
    /**
     * Add Record
     */
    public function adminAdd()
    {
        $validator = JsValidator::make($this->validation_rules);
        return view('pages.admin.artists.add', [
            'validator' => $validator
        ]);
    }

    /**
     * Handle submit action
     */
    public function handleAdminAdd(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        try {
            $response = $this->artistRepository->addRecord($request);
            if(isset($response['status']) && $response['status'] ==  0) {
                return back()->withErrors($response['message']);
            }
        } catch (\Exception $e) {
            return back()->withErrors('Some issue while saving artist. Please try again.');
        }
        
        return back()->withSuccess('Artist added successfully.');
    }

    /**
     * Admin Lists
     */
    public function getAdminRecords(Request $request)
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $status         = $request->has('status') ? $request->input('status') : "";
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $search         = $request->has('search') ? $request->input('search') : "";
        $artists        = $this->artistRepository->getRecords();
        $vendor_id      = $request->has('vendor_id') ? $request->input('vendor_id') : "";

        if($search != "") {
            $artists = $this->artistRepository->getSearchRecords($artists, $search);
        }
        if($status != "") {
            $artists  = $artists->where('is_approved', '=', $status);
        }
        if($vendor_id != "") {
            $artists  = $artists->where('vendor_id', '=', $vendor_id);
        }
        $artists = $artists->latest()
                ->paginate($per_page);          
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $approved      = $this->artistRepository->getStatusRecords('Y')
                            ->count();
        $unapproved    = $this->artistRepository->getStatusRecords('N')
                            ->count();

        $all_counts = $this->artistRepository->getRecords()
                                ->count();

        // all site vendors
        $vendors = User::hasVendorRole()->onlySite()->get(['id', 'first_name', 'last_name', 'name', 'email']);

        return view('pages.admin.artists.list', [
            'records' => $artists,
            'per_page' => $per_page,
            'search' => $search,
            'status' => $status,
            'all_records' => $all_counts,
            'approved_records' => $approved,
            'unapproved_records' => $unapproved,
            'vendors' => $vendors,
            'vendor_id' => $vendor_id
        ]);
    }

    /**
     * Handle bulk action of list
     */
    public function handleBulkAction(Request $request)
    {
        $action = $request->has('action') ? $request->input('action') : "";
        $ids    = $request->has('ids') ? $request->input('ids') : "";
        if( $action != "" && ($action == "delete" || $action == "Y" || $action == "N") && !empty($ids) ) {
            $records = $this->artistRepository->getRecords($ids);
            if($action == "Y" || $action == "N") {
                try {
                    foreach($records as $record) {
                        $record->is_approved = $action;
                        $record->save();
                    }
                    return redirect()->back()->withSuccess('Records(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating Records(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($records as $record) {
                        $this->artistRepository->deleteRecord($record);
                    }
                    return redirect()->back()->withSuccess('Record(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting Record(s), please try again.');
                }
            }
        } else if($action == "sort") {
            //
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    public function adminEdit($id)
    {
        // get single event object
        $record = $this->artistRepository->getAdminRecord($id);

        $validator = JsValidator::make($this->validation_rules);
        
        return view('pages.admin.artists.edit', [
                'record' => $record,
                'validator' => $validator
            ]
        );
    }

    public function handleAdminEdit(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        try {
            $response = $this->artistRepository->updateRecord($request);
            if(isset($response['status']) && $response['status'] ==  0) {
                return back()->withErrors($response['message']);
            }
        } catch (\Exception $e) {
            return back()->withErrors('Some issue while saving artist. Please try again.');
        }
        
        return back()->withSuccess('Artist updated successfully.');
    }

    // detete image
    public function deleteImage(Request $request)
    {
        $id = $request->input('id');
        $response = $this->artistRepository->deleteImage($id);
        return response()->json($response);
    }
}
