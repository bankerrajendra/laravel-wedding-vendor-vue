<?php

namespace App\Http\Controllers;

use App\Logic\Common\SearchRepository;
use Illuminate\Http\Request;
use App\Logic\Common\LocationRepository;
use Illuminate\Support\Facades\Auth;
use JsValidator;

class SearchController extends Controller
{
    //
    protected $locationRepository;
    protected $searchRepository;
    protected $validationRules = [
        'age_from'=>'required',
        'age_to'=>'required',
        'cities'=>'required'
    ];
    protected $backendValidation = [
        'age_from'=>'required',
        'age_to'=>'required|gt:age_from',
        'cities'=>'required'
    ];
    public function __construct(LocationRepository $locationRepository, SearchRepository $searchRepository)
    {
        $this->locationRepository = $locationRepository;
        $this->searchRepository = $searchRepository;
    }

    public function showSearchPage(Request $request){
        if($request->input('age_from')){
            $validatedData =  $request->validate(
                $this->backendValidation,
                [
                    'age_from.required'=>'Age from is required',
                    'age_to.required'=>'Age to is required',
                    'age_to.gt'=>'Age to must be greater than age from',
                    'cities.required'=>'City is required'
                ]
            );
        }
        $validator = JsValidator::make($this->validationRules);
        $user = Auth::user();
        $cities = array();

        $age = array(
            config('constants.partner_preference.start_age'),
            config('constants.partner_preference.end_age')
        );
        if($request->input('country')){
            $allCities = $this->locationRepository->getCitiesOfCountries($request->input('country'));
        }
        else{
            $allCities = $this->locationRepository->getCountryCities($user->country_id);
        }
        if($request->input('cities')){
            $cities = $request->input('cities');
        }
        if($request->input('age_from')){
            $age[0] = $request->input('age_from');
        }
        if($request->input('age_to')){
            if($request->input('age_to') > $age[0]){
                $age[1] = $request->input('age_to');
            }
        }

        $results = $this->searchRepository->searchUsers($cities, $age);

        return view('pages.user.search')->with(
            [
                'locationRepo'=>$this->locationRepository,
                'user'=>$user,
                'request'=>$request,
                'results'=>$results,
                'validator'=>$validator,
                'allCities' => $allCities
            ]
        );
    }
}
