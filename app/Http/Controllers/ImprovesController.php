<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use App\Models\Improve;
use App\Logic\Admin\ImproveRepository;

class ImprovesController extends Controller
{
    private $validation_rules = [
        'subject'           => 'required|string|min:10|max:200',
        'message'           => 'required|string|min:50|max:300'
    ];

    private $validation_messages = [
        'subject.required'          => "Subject field is required.",
        'message.required'          => "Message field is required.",
        'subject.min'               => "Subject should be minimum 10 and maximum 200 in lenght.",
        'subject.max'               => "Subject should be minimum 10 and maximum 200 in lenght.",
        'message.min'               => "Message should be minimum 50 and maximum 300 in lenght.",
        'message.max'               => "Message should be minimum 50 and maximum 300 in lenght."
    ];

    /**
     * Add Improve
     */
    public function showAddImprove()
    {
        $validator = JsValidator::make($this->validation_rules, $this->validation_messages);
        return view('pages.improve.add', [
            'validator' => $validator
        ]);
    }

    /**
     * Handle submit improve action
     */
    public function handleSubmitImprove(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation_rules, $this->validation_messages );
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $user_id = Auth::id();
        try {
            Improve::create([
                'user_id'   => $user_id,
                'subject'   => $request->input('subject'),
                'message'   => $request->input('message'),
                'site_id'   => config('constants.site_id')
            ]);
            return back()->withSuccess('Suggestion added successfully.');
        } catch(\Exception $e) {
            return back()->withErrors('Something wrong while adding record.')->withInput();
        }
    }

    /**
     * Admin Lists
     */
    public function showAdminListings(Request $request, ImproveRepository $improve_repo)
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $search         = $request->has('search') ? $request->input('search') : "";
        $improves        = $improve_repo->getRecords();

        if($search != "") {
            $improves = $improve_repo->getSearchRecords($improves, $search);
        }

        $improves = $improves->latest()
                ->paginate($per_page);          
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $all_counts             = $improve_repo->getRecords()
                                ->count();

        return view('pages.admin.improves.list', [
            'improves'              => $improves,
            'per_page'              => $per_page,
            'search'                => $search,
            'all_records'           => $all_counts
        ]);
    }

    public function handleBulkActionImprove(Request $request, ImproveRepository $improve_repo)
    {
        $action       = $request->has('action') ? $request->input('action') : "";
        $improve_ids  = $request->has('improve_ids') ? $request->input('improve_ids') : "";
        if( $action != "" && ($action == "delete" || $action == "1" || $action == "0") && !empty($improve_ids) ) {
            $improves = $improve_repo->getRecords($improve_ids);
            if($action == "0" || $action == "1") {
                try {
                    foreach($improves as $improve) {
                        $improve->approved = $action;
                        $improve->save();
                    }
                    return redirect()->back()->withSuccess('Record(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating Record(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($improves as $improve) {
                        $improve->delete();
                    }
                    return redirect()->back()->withSuccess('Record(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting Record(s), please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    public function showViewImprove($id, ImproveRepository $improve_repo)
    {
        // get single record
        $improve = $improve_repo->getRecords($id);

        return view('pages.admin.improves.view', [
            'improve'        => $improve
        ]);
    }
}
