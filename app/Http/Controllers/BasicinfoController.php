<?php
/**@ BasicdetailController
@ manages admin -> country, city, state modules
@ Created : 02|05|2019
 **/

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Community;
use App\Models\Religion;
use App\Models\Subcaste;
use App\Models\Language;
use Illuminate\Support\Facades\Route;
use App\Events\Activity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class BasicinfoController extends Controller
{
    protected $countryModel;
    # Not using this repository through

    protected $validationRules =[
        'religion_title'=>'required',
        'description'=>'required'];


    protected $validationRulesServer =[
        'religion_title'=>'required',
        'description'=>'required'];

    protected $basicValidationRules =[
        'name'=>'required'
    ];

    public function __construct(Country $country)
    {
        $this->middleware('auth');
        $this->countryModel = $country;
    }

    public function listCountries()
    {
        $countryLists =$this->countryModel::all();
        return view('pages.admin.basic.country')->with(["countryLists"=>$countryLists]);
    }


    /*************************** Admin State Management ********************/
    public function getStates(Request $request){
        $pageNo=1;
        $search= $request->input('search');
        $pagintaionEnabled = config('usersmanagement.enableCmsPagination');
        if ($pagintaionEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
            if($search!="") {
                $fetchstates = State::where('name','like','%'.$search.'%')->paginate(config('usersmanagement.paginateListSize'));
            } else {
                $fetchstates = State::paginate(config('usersmanagement.paginateListSize'));
            }
            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;
        } else {
            if($search!="") {
                $fetchstates = State::where('name','like','%'.$search.'%')->get();
            } else {
                $fetchstates = State::get();
            }
        }
         return view('pages.admin.basic.states')->with(['statevar'=>$fetchstates,'pageNo'=>$pageNo,'search'=>$search]);
    }


    public function addState() {
        $validator = JsValidator::make($this->basicValidationRules);
        $countries=Country::get();
        return view('pages.admin.basic.add-state',['validator'=>$validator,'countries'=>$countries]);
    }

    public function updateState(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $stateRec = new State();
        $stateRec->name = $request->input('name');
        $stateRec->country_id = $request->input('country_id');

        if ($stateRec->save()) {
            return redirect()->back()->withSuccess('State added successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in adding state. please try again.');
        }
    }

    public function editState($id) {
        $validator = JsValidator::make($this->basicValidationRules);
        $countries=Country::get();
        $stateRec=State::where('id',$id)->first();
        return view('pages.admin.basic.edit-state',['validator'=>$validator,'id'=>$id,'countries'=>$countries,'stateRec'=>$stateRec]);
    }

    public function posteditState(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $id = $request->input('id');
        $stateRec = State::find($id);
        $stateRec->name = $request->input('name');
        $stateRec->country_id = $request->input('country_id');

        if ($stateRec->save()) {
            return redirect()->back()->withSuccess('State modified successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in modifying state. please try again.');
        }
    }

    public function deleteState($state_id) {
        $res=State::where('id',$state_id)->delete();
        if($res) {
            return redirect()->back()->withSuccess('State Deleted Successfully.');
        }else {
            return redirect()->back()->withError('State Deletion Failed. Please try again.');
        }
    }
    /*************************** END: Admin State Management ********************/

    /*************************** Admin City Management ********************/
    public function getCities(Request $request){
        $pageNo=1;
        $search= $request->input('search');
        $pagintaionEnabled = config('usersmanagement.enableCmsPagination');
        if ($pagintaionEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
            if($search!="") {
                $fetchcities = City::where('name','like','%'.$search.'%')->paginate(config('usersmanagement.paginateListSize'));
            } else {
                $fetchcities = City::paginate(config('usersmanagement.paginateListSize'));
            }
            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;
        } else {
            if($search!="") {
                $fetchcities = City::where('name','like','%'.$search.'%')->get();
            } else {
                $fetchcities = City::get();
            }
        }
        return view('pages.admin.basic.cities')->with(['cityvar'=>$fetchcities,'pageNo'=>$pageNo,'search'=>$search]);
    }


    public function addCity() {
        $validator = JsValidator::make($this->basicValidationRules);
        $states=State::get();
        return view('pages.admin.basic.add-city',['validator'=>$validator,'states'=>$states]);
    }

    public function updateCity(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $cityRec = new City();
        $cityRec->name = $request->input('name');
        $cityRec->state_id = $request->input('state_id');
        if ($cityRec->save()) {
            return redirect()->back()->withSuccess('City added successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in adding city. please try again.');
        }
    }

    public function editCity($id) {
        $validator = JsValidator::make($this->basicValidationRules);
        $states=State::get();
        $cityRec=City::where('id',$id)->first();
        return view('pages.admin.basic.edit-city',['validator'=>$validator,'id'=>$id,'states'=>$states,'cityRec'=>$cityRec]);
    }

    public function posteditCity(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $id = $request->input('id');
        $cityRec = City::find($id);
        $cityRec->name = $request->input('name');
        $cityRec->state_id = $request->input('state_id');

        if ($cityRec->save()) {
            return redirect()->back()->withSuccess('City modified successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in modifying city. please try again.');
        }
    }

    public function deleteCity($city_id) {
        $res=City::where('id',$city_id)->delete();
        if($res) {
            return redirect()->back()->withSuccess('City Deleted Successfully.');
        }else {
            return redirect()->back()->withError('City Deletion Failed. Please try again.');
        }
    }
    /*************************** END: Admin City Management ********************/

    /*************************** Admin Sects Management ********************/
    public function getSects(Request $request){
        $pageNo=1;
        $search= $request->input('search');
        $pagintaionEnabled = config('usersmanagement.enableCmsPagination');
        if ($pagintaionEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
            if($search!="") {
                $fetchsects = Community::where('name','like','%'.$search.'%')->paginate(config('usersmanagement.paginateListSize'));
            } else {
                $fetchsects = Community::paginate(config('usersmanagement.paginateListSize'));
            }
            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;
        } else {
            if($search!="") {
                $fetchsects = Community::where('name','like','%'.$search.'%')->get();
            } else {
                $fetchsects = Community::get();
            }
        }
        return view('pages.admin.basic.sects')->with(['sectvar'=>$fetchsects,'pageNo'=>$pageNo,'search'=>$search]);
    }


    public function addSect() {
        $validator = JsValidator::make($this->basicValidationRules);

        $religions = Religion::get();
        return view('pages.admin.basic.add-sect',['validator'=>$validator,'religions'=>$religions]);
    }

    public function updateSect(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $sectRec = new Community();
        $sectRec->name = $request->input('name');
        $sectRec->religion_id = $request->input('religion_id');

        if ($sectRec->save()) {
            return redirect()->back()->withSuccess('Sect added successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in adding sect. please try again.');
        }
    }

    public function editSect($id) {
        $validator = JsValidator::make($this->basicValidationRules);
        $religions=Religion::get();
        $sectRec=Community::where('id',$id)->first();
        return view('pages.admin.basic.edit-sect',['validator'=>$validator,'id'=>$id,'religions'=>$religions,'sectRec'=>$sectRec]);
    }

    public function posteditSect(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $id = $request->input('id');
        $sectRec = Community::find($id);
        $sectRec->name = $request->input('name');
        $sectRec->religion_id = $request->input('religion_id');

        if ($sectRec->save()) {
            return redirect()->back()->withSuccess('Sect modified successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in modifying sect. please try again.');
        }
    }

    public function deleteSect($sect_id) {
        $res=Community::where('id',$sect_id)->delete();
        if($res) {
            return redirect()->back()->withSuccess('Sect Deleted Successfully.');
        }else {
            return redirect()->back()->withError('Sect Deletion Failed. Please try again.');
        }
    }
    /*************************** END: Admin Sects Management ********************/

    /*************************** Admin Subcaste Management ********************/
    public function getSubcastes(Request $request){
        $pageNo=1;
        $search= $request->input('search');
        $pagintaionEnabled = config('usersmanagement.enableCmsPagination');
        if ($pagintaionEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
            if($search!="") {
                $fetchsubcastes = Subcaste::where('name','like','%'.$search.'%')->paginate(config('usersmanagement.paginateListSize'));
            } else {
                $fetchsubcastes = Subcaste::paginate(config('usersmanagement.paginateListSize'));
            }
            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;
        } else {
            if($search!="") {
                $fetchsubcastes = Subcaste::where('name','like','%'.$search.'%')->get();
            } else {
                $fetchsubcastes = Subcaste::get();
            }
        }
        return view('pages.admin.basic.subcastes')->with(['subcastevar'=>$fetchsubcastes,'pageNo'=>$pageNo,'search'=>$search]);
    }


    public function addSubcaste() {
        $validator = JsValidator::make($this->basicValidationRules);
        return view('pages.admin.basic.add-subcaste',['validator'=>$validator]);
    }

    public function updateSubcaste(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $subcasteRec = new Subcaste();
        $subcasteRec->name = $request->input('name');
        if ($subcasteRec->save()) {
            return redirect()->back()->withSuccess('Subcaste added successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in adding subcaste. please try again.');
        }
    }

    public function editSubcaste($id) {
        $validator = JsValidator::make($this->basicValidationRules);
        $subcasteRec=Subcaste::where('id',$id)->first();
        return view('pages.admin.basic.edit-subcaste',['validator'=>$validator,'id'=>$id,'subcasteRec'=>$subcasteRec]);
    }

    public function posteditSubcaste(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $id = $request->input('id');
        $subcasteRec = Subcaste::find($id);
        $subcasteRec->name = $request->input('name');

        if ($subcasteRec->save()) {
            return redirect()->back()->withSuccess('Subcaste modified successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in modifying subcaste. please try again.');
        }
    }

    public function deleteSubcaste($subcaste_id) {
        $res=Subcaste::where('id',$subcaste_id)->delete();
        if($res) {
            return redirect()->back()->withSuccess('Subcaste Deleted Successfully.');
        }else {
            return redirect()->back()->withError('Subcaste Deletion Failed. Please try again.');
        }
    }
    /*************************** END: Admin Subcaste Management ********************/


    /*************************** Admin Language Management ********************/
    public function getLanguages(Request $request){
        $pageNo=1;
        $search= $request->input('search');
        $pagintaionEnabled = config('usersmanagement.enableCmsPagination');
        if ($pagintaionEnabled) {
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
            if($search!="") {
                $fetchlanguages = Language::where('language','like','%'.$search.'%')->paginate(config('usersmanagement.paginateListSize'));
            } else {
                $fetchlanguages = Language::paginate(config('usersmanagement.paginateListSize'));
            }
            if($pageNo>1)
                $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;
        } else {
            if($search!="") {
                $fetchlanguages = Language::where('language','like','%'.$search.'%')->get();
            } else {
                $fetchlanguages = Language::get();
            }
        }
        return view('pages.admin.basic.languages')->with(['languagevar'=>$fetchlanguages,'pageNo'=>$pageNo,'search'=>$search]);
    }


    public function addLanguage() {
        $validator = JsValidator::make($this->basicValidationRules);
        return view('pages.admin.basic.add-language',['validator'=>$validator]);
    }

    public function updateLanguage(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $languageRec = new Language();
        print_r($languageRec);
        $languageRec->language = $request->input('name');
        if ($languageRec->save()) {
            return redirect()->back()->withSuccess('Language added successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in adding language. please try again.');
        }
    }

    public function editLanguage($id) {
        $validator = JsValidator::make($this->basicValidationRules);
        $languageRec=Language::where('id',$id)->first();
        return view('pages.admin.basic.edit-language',['validator'=>$validator,'id'=>$id,'languageRec'=>$languageRec]);
    }

    public function posteditLanguage(Request $request) {
        $validate =$request->validate($this->basicValidationRules);
        $id = $request->input('id');
        $languageRec = Language::find($id);
        print_r($languageRec);
        $languageRec->language = $request->input('name');

        if ($languageRec->save()) {
            return redirect()->back()->withSuccess('Language modified successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in modifying language. please try again.');
        }
    }

    public function deleteLanguage($language_id) {
        $res=Language::where('id',$language_id)->delete();
        if($res) {
            return redirect()->back()->withSuccess('Language Deleted Successfully.');
        }else {
            return redirect()->back()->withError('Language Deletion Failed. Please try again.');
        }
    }
    /*************************** END: Admin Language Management ********************/

    /**
    @ ReligionController@createReligions
    @ Form to Creates new entry in Religion table
    @ param : null
    @ returns : HTML
     **/
    public function createReligions()
    {
        $validator = JsValidator::make($this->validationRulesServer);
        return view('religions.add-new-religion',['validator'=>$validator])->with(["nextSiteId"=>3]);
    }

    /**
    @ ReligionController@saveReligion
    @ Handles createReligions reuests
    @ param : Form data
    @ returns : Event
     **/
    public function saveReligion(Request $request){
        $religionData = new Religion;
        $religionData->religion_title = $request->input('religion_title');
        $religionData->religion_description = $request->input('religion_description');

        if ($religionData->save()) {
            return redirect()->back()->withSuccess('Religion added successfully.');
        } else {
            return redirect()->back()->withError('Error occured while creating Religion');
        }
    }

    /**
    @ ReligionController@editReligions
    @ Updates data in Religion table
    @ param : Religion ID
    @ returns : HTML
     **/
    public function editReligions($religion_id)
    {
        $validator = JsValidator::make($this->validationRulesServer);
        $religionSingle = $this->religionModel::find($religion_id);

        if($religionSingle){
            return view('religions.edit-religion',['validator'=>$validator,'religionSingle'=>$religionSingle,'religion_id'=>$religion_id])->with(["nextSiteId"=>3]);
        } else {
            return abort(404);
        }
    }

    /**
    @ ReligionController@updateReligion
    @ Handles updateReligion requests
    @ param : Religion ID
    @ returns : Event
     **/
    public function updateReligion(Request $request, $religion_id){
        // echo $religion_id."<br>";

        $religionData = $this->religionModel::find($religion_id);
        $religionData->religion_title = $request->input('religion_title');
        $religionData->religion_description = $request->input('religion_description');

        if ($religionData->save()) {
            return redirect()->back()->withSuccess('Religion updated successfully.');
        } else {
            return redirect()->back()->withError('Error occured while updating Religion');
        }
    }

    /**
    @ ReligionController@updateReligion
    @ Deletes in Religion table
    @ param : Religion ID
    @ returns : Event
     **/
    public function deleteReligion($religion_id){
        $religionData = $this->religionModel::find($religion_id);

        if($religionData->delete()) {
            return redirect()->back()->withSuccess('Religion deleted successfully.');
        } else {
            return redirect()->back()->withError('Error occured while deleting Religion');
        }
    }
}