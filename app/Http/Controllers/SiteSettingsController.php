<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Logic\Common\SiteGeneralSettingsRepository;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use Illuminate\Support\Facades\Hash;

class SiteSettingsController extends Controller
{
    private $site_setting_repo;

    private $log_favicon_rules = [
        'site_logo'       => 'required|image|mimes:jpeg,jpg,png,gif,bmp|max:2048|dimensions:max_width=500,max_height=500',
        'site_favicon'    => 'required|image|mimes:jpeg,jpg,png,gif,ico|max:1024|dimensions:max_width=100,max_height=100'
    ];

    private $matri_prefix_rules = [
        'matri_prefix' => 'required|string|max:10'
    ];

    private $site_email_rules = [
        'site_email'        => 'required|email',
        'site_from_email'   => 'required|email',
        'site_to_email'     => 'required|email'
    ];

    private $site_basic_settings_rules = [
        'site_url'                      => 'required|url',
        'site_name'                     => 'required|string|max:200',
        'site_title'                    => 'required|string|max:200',
        'site_keywords'                 => 'required|string|max:1000',
        'site_description'              => 'required|string|max:1000',
        'site_footer_text'              => 'required|string|max:200',
        'site_contact_no'               => 'required|numeric|digits_between:7,12',
        'site_postal_address'           => 'required|string|max:1000',
        'site_google_analytics_code'    => 'required|string|max:1000',
        'site_tax_applicable'           => 'required|string|max:3',
        'site_tax_name'                 => 'required|string|max:20',
        'site_tax_percentage'           => 'required|numeric|digits_between:1,3',
        'site_timezone'                 => 'required|timezone'
    ];

    private $site_social_media_link_rules = [
        'site_facebook_link'    => 'required|url',
        'site_twitter_link'     => 'required|url',
        'site_linkedin_link'    => 'required|url',
        'site_google_link'      => 'required|url',
        'site_youtube_link'     => 'required|url',
        'site_instagram_link'   => 'required|url'
    ];

    private $change_password_rules = [
        'current_password'              => 'required',
        'new_password'                  => 'required|string|min:6|confirmed'
    ];

    private $log_favicon_message = [
        'site_logo.required'      => 'Site Logo required.',
        'site_favicon.required'   => 'Site Favicon required.'
    ];

    public function __construct(SiteGeneralSettingsRepository $site_setting_repo)
    {
        $this->site_setting_repo = $site_setting_repo;
    }
    // Logo Favicon
    public function showLogoFavicon()
    {
        $validator = JsValidator::make($this->log_favicon_rules, $this->log_favicon_message);

        $logo = $this->site_setting_repo->getSettingsByKey('site_logo');
        if(empty($logo)) {
            $logo = "";
        }

        $favicon = $this->site_setting_repo->getSettingsByKey('site_favicon');
        if(empty($favicon)) {
            $favicon = "";
        }

        return view('pages.admin.site-settings.logo-favicon', [
            'validator' => $validator,
            'logo'      => $logo,
            'favicon'   => $favicon
        ]);
    }
    // Handle Logo / Favicon
    public function handleLogoFavicon(Request $request)
    {
        $validator = Validator::make($request->all(), $this->log_favicon_rules, $this->log_favicon_message);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        if($request->hasFile('site_logo')) {
            // save image to system
            $logo = $this->site_setting_repo->saveImage($request->file('site_logo'), 'logo', 'site_logo');
            $this->site_setting_repo->saveKeyVal('site_logo', $logo);
        }

        if($request->hasFile('site_favicon')) {
            // save image to system
            $favicon = $this->site_setting_repo->saveImage($request->file('site_favicon'), 'favicon', 'site_favicon');
            $this->site_setting_repo->saveKeyVal('site_favicon', $favicon);
        }

        return back()->withSuccess('Logo and Favicon saved successfully.');
    }
    // Matri Prefix
    public function showMatriPrefix()
    {
        $validator = JsValidator::make($this->matri_prefix_rules);

        $matri_prefix_obj = $this->site_setting_repo->getSettingsByKey('matri_prefix');
        if(empty($matri_prefix_obj)) {
            $matri_prefix = "";
        } else {
            $matri_prefix = $matri_prefix_obj->value;
        }

        return view('pages.admin.site-settings.matri-prefix', [
            'validator'     => $validator,
            'matri_prefix'  => $matri_prefix
        ]);
    }
    // Handle Matri Prefix
    public function handleMatriPrefix(Request $request)
    {
        $validator = Validator::make($request->all(), $this->matri_prefix_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $this->site_setting_repo->saveKeyVal('matri_prefix', $request->input('matri_prefix'));
        return back()->withSuccess('Matri Prefix saved successfully.');
    }
    // Email
    public function showEmail()
    {
        $validator = JsValidator::make($this->site_email_rules);

        $site_email_obj = $this->site_setting_repo->getSettingsByKey('site_email');
        $site_from_email_obj = $this->site_setting_repo->getSettingsByKey('site_from_email');
        $site_to_email_obj = $this->site_setting_repo->getSettingsByKey('site_to_email');
        if(empty($site_email_obj)) {
            $site_email = "";
        } else {
            $site_email = $site_email_obj->value;
        }
        if(empty($site_from_email_obj)) {
            $site_from_email = "";
        } else {
            $site_from_email = $site_from_email_obj->value;
        }
        if(empty($site_to_email_obj)) {
            $site_to_email = "";
        } else {
            $site_to_email = $site_to_email_obj->value;
        }
        
        return view('pages.admin.site-settings.email', [
            'validator'         => $validator,
            'site_email'        => $site_email,
            'site_from_email'   => $site_from_email,
            'site_to_email'     => $site_to_email
        ]);
    }
    // Handle Site Email
    public function handleEmail(Request $request)
    {
        $validator = Validator::make($request->all(), $this->site_email_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $this->site_setting_repo->saveKeyVal('site_email', $request->input('site_email'));
        $this->site_setting_repo->saveKeyVal('site_from_email', $request->input('site_from_email'));
        $this->site_setting_repo->saveKeyVal('site_to_email', $request->input('site_to_email'));
        return back()->withSuccess('Emails saved successfully.');
    }
    // Basic
    public function showBasic()
    {
        $validator = JsValidator::make($this->site_basic_settings_rules);

        $site_url_obj = $this->site_setting_repo->getSettingsByKey('site_url');
        if(empty($site_url_obj)) {
            $site_url = "";
        } else {
            $site_url = $site_url_obj->value;
        }


        $site_contact_telephone_obj = $this->site_setting_repo->getSettingsByKey('site_contact_telephone');
        if(empty($site_contact_telephone_obj)) {
            $site_contact_telephone = "";
        } else {
            $site_contact_telephone = $site_contact_telephone_obj->value;
        }


        $site_name_obj = $this->site_setting_repo->getSettingsByKey('site_name');
        if(empty($site_name_obj)) {
            $site_name = "";
        } else {
            $site_name = $site_name_obj->value;
        }
        $site_title_obj = $this->site_setting_repo->getSettingsByKey('site_title');
        if(empty($site_title_obj)) {
            $site_title = "";
        } else {
            $site_title = $site_title_obj->value;
        }
        $site_keywords_obj = $this->site_setting_repo->getSettingsByKey('site_keywords');
        if(empty($site_keywords_obj)) {
            $site_keywords = "";
        } else {
            $site_keywords = $site_keywords_obj->value;
        }
        $site_description_obj = $this->site_setting_repo->getSettingsByKey('site_description');
        if(empty($site_description_obj)) {
            $site_description = "";
        } else {
            $site_description = $site_description_obj->value;
        }
        $site_footer_text_obj = $this->site_setting_repo->getSettingsByKey('site_footer_text');
        if(empty($site_footer_text_obj)) {
            $site_footer_text = "";
        } else {
            $site_footer_text = $site_footer_text_obj->value;
        }
        $site_contact_no_obj = $this->site_setting_repo->getSettingsByKey('site_contact_no');
        if(empty($site_contact_no_obj)) {
            $site_contact_no = "";
        } else {
            $site_contact_no = $site_contact_no_obj->value;
        }
        $site_postal_address_obj = $this->site_setting_repo->getSettingsByKey('site_postal_address');
        if(empty($site_postal_address_obj)) {
            $site_postal_address = "";
        } else {
            $site_postal_address = $site_postal_address_obj->value;
        }
        $site_google_analytics_code_obj = $this->site_setting_repo->getSettingsByKey('site_google_analytics_code');
        if(empty($site_google_analytics_code_obj)) {
            $site_google_analytics_code = "";
        } else {
            $site_google_analytics_code = $site_google_analytics_code_obj->value;
        }
        $site_tax_applicable_obj = $this->site_setting_repo->getSettingsByKey('site_tax_applicable');
        if(empty($site_tax_applicable_obj)) {
            $site_tax_applicable = "";
        } else {
            $site_tax_applicable = $site_tax_applicable_obj->value;
        }
        $site_tax_name_obj = $this->site_setting_repo->getSettingsByKey('site_tax_name');
        if(empty($site_tax_name_obj)) {
            $site_tax_name = "";
        } else {
            $site_tax_name = $site_tax_name_obj->value;
        }
        $site_tax_percentage_obj = $this->site_setting_repo->getSettingsByKey('site_tax_percentage');
        if(empty($site_tax_percentage_obj)) {
            $site_tax_percentage = "";
        } else {
            $site_tax_percentage = $site_tax_percentage_obj->value;
        }
        $site_timezone_obj = $this->site_setting_repo->getSettingsByKey('site_timezone');
        if(empty($site_timezone_obj)) {
            $site_timezone = "";
        } else {
            $site_timezone = $site_timezone_obj->value;
        }

        return view('pages.admin.site-settings.basic', [
            'validator'                     => $validator,
            'site_url'                      => $site_url,
            'site_name'                     => $site_name,
            'site_title'                    => $site_title,
            'site_keywords'                 => $site_keywords,
            'site_description'              => $site_description,
            'site_footer_text'              => $site_footer_text,
            'site_contact_no'               => $site_contact_no,
            'site_postal_address'           => $site_postal_address,
            'site_google_analytics_code'    => $site_google_analytics_code,
            'site_tax_applicable'           => $site_tax_applicable,
            'site_tax_name'                 => $site_tax_name,
            'site_tax_percentage'           => $site_tax_percentage,
            'site_contact_telephone'        => $site_contact_telephone,
            'site_timezone'                 => $site_timezone
        ]);
    }
    // Handle Basic Site settings
    public function handleBasic(Request $request)
    {
        $validator = Validator::make($request->all(), $this->site_basic_settings_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        
        foreach($request->input() as $key => $value) {
            if($key != "_token") { // exclude token
                $this->site_setting_repo->saveKeyVal($key, $value);
            }
        }

        return back()->withSuccess('Basic Settings saved successfully.');
    }
    // Social Media Links
    public function showSocialMedia()
    {
        $validator = JsValidator::make($this->site_social_media_link_rules);

        $site_facebook_link_obj = $this->site_setting_repo->getSettingsByKey('site_facebook_link');
        if(empty($site_facebook_link_obj)) {
            $site_facebook_link = "";
        } else {
            $site_facebook_link = $site_facebook_link_obj->value;
        }
        $site_twitter_link_obj = $this->site_setting_repo->getSettingsByKey('site_twitter_link');
        if(empty($site_twitter_link_obj)) {
            $site_twitter_link = "";
        } else {
            $site_twitter_link = $site_twitter_link_obj->value;
        }
        $site_linkedin_link_obj = $this->site_setting_repo->getSettingsByKey('site_linkedin_link');
        if(empty($site_linkedin_link_obj)) {
            $site_linkedin_link = "";
        } else {
            $site_linkedin_link = $site_linkedin_link_obj->value;
        }
        $site_google_link_obj = $this->site_setting_repo->getSettingsByKey('site_google_link');
        if(empty($site_google_link_obj)) {
            $site_google_link = "";
        } else {
            $site_google_link = $site_google_link_obj->value;
        }
        $site_youtube_link_obj = $this->site_setting_repo->getSettingsByKey('site_youtube_link');
        if(empty($site_youtube_link_obj)) {
            $site_youtube_link = "";
        } else {
            $site_youtube_link = $site_youtube_link_obj->value;
        }
        $site_instagram_link_obj = $this->site_setting_repo->getSettingsByKey('site_instagram_link');
        if(empty($site_instagram_link_obj)) {
            $site_instagram_link = "";
        } else {
            $site_instagram_link = $site_instagram_link_obj->value;
        }
        
        return view('pages.admin.site-settings.social-media', [
            'validator'             => $validator,
            'site_facebook_link'    => $site_facebook_link,
            'site_twitter_link'     => $site_twitter_link,
            'site_linkedin_link'    => $site_linkedin_link,
            'site_google_link'      => $site_google_link,
            'site_youtube_link'     => $site_youtube_link,
            'site_instagram_link'   => $site_instagram_link
        ]);
    }
    // Handle Social Media links
    public function handleSocialMedia(Request $request)
    {
        $validator = Validator::make($request->all(), $this->site_social_media_link_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        
        foreach($request->input() as $key => $value) {
            if($key != "_token") { // exclude token
                $this->site_setting_repo->saveKeyVal($key, $value);
            }
        }

        return back()->withSuccess('Social Media Links saved successfully.');
    }
    // Change Password
    public function showChangePassword()
    {
        $validator = JsValidator::make($this->change_password_rules);
        
        return view('pages.admin.site-settings.change-password', [
            'validator' => $validator
        ]);
    }
    // Handle Change Password
    public function handleChangePassword(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return back()->withErrors(
                [
                    "error" => "Your current password does not matches with the password you provided. Please try again."
                ]
            );
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return back()->withErrors(
                [
                    "error" => "New Password cannot be same as your current password. Please choose a different password."
                ]
            );
        }

        $validatedData = $request->validate($this->change_password_rules);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return back()->withSuccess("Password changed successfully !");
    }
}