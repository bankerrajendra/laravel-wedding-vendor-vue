<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logic\Admin\BannerRepository;
use App\Models\Banner;

use JsValidator;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Integer;

class BannerController extends Controller {
    protected $BannerRepository;
    protected $validationRules = [
        'title' => 'required',
        'banner_type' => 'required',
        'banner_link' => 'required|url',
        'banner_image' => 'required|image|mimes:jpeg,jpg,png,gif,bmp'
    ];

    public function __construct(BannerRepository $BannerRepository)
    {
        $this->BannerRepository = $BannerRepository;
    }

    //Backend Functions
    public function getBanners(Request $request) 
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $status         = $request->has('status') ? $request->input('status') : "";
        $search         = $request->has('search') ? $request->input('search') : "";
        $banners        = $this->BannerRepository->getRecords();

        if($search != "") {
            $banners = $this->BannerRepository->getSearchRecords($banners, $search);
        }

        if($status != "") {
            $banners  = $banners->where('status', '=', $status);
        }
        $banners = $banners->latest()
                ->paginate($per_page);          
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $active_counts      = $this->BannerRepository->getStatusRecords(1)
                            ->count();
        $inactive_counts    = $this->BannerRepository->getStatusRecords(0)
                            ->count();
        $all_counts         = $this->BannerRepository->getRecords()
                            ->count();


        return view('pages.admin.banner.list')->with(
            [
                'banners'           => $banners,
                'page'              => $page,
                'per_page'          => $per_page,
                'status'            => $status,
                'search'            => $search,
                'active_records'    => $active_counts,
                'inactive_records'  => $inactive_counts,
                'all_records'       => $all_counts
            ]
        );
    }

    /**
     * Handle bulk action of list banner
     */
    public function handleBulkActionBanner(Request $request)
    {
        $action       = $request->has('action') ? $request->input('action') : "";
        $banner_ids   = $request->has('banner_ids') ? $request->input('banner_ids') : "";
        if( $action != "" && ($action == "delete" || $action == "1" || $action == "0") && !empty($banner_ids) ) {
            $banners = $this->BannerRepository->getRecords($banner_ids);
            if($action == "1" || $action == "0") {
                try {
                    foreach($banners as $banner) {
                        $banner->status = $action;
                        $banner->save();
                    }
                    return redirect()->back()->withSuccess('Banner(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating banner(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($banners as $banner) {
                        $this->BannerRepository->deleteBanner($banner);
                    }
                    return redirect()->back()->withSuccess('Banner(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting banner(s), please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    public function addBanner() 
    {
        $validator = JsValidator::make($this->validationRules);
        return view('pages.admin.banner.add', [
                'validator' => $validator
            ]
        );
    }

    public function editBanner($id) 
    {
        $this->validationRules['banner_image'] = 'image|mimes:jpeg,jpg,png,gif,bmp';
        $validator = JsValidator::make($this->validationRules);
        $banner = $this->BannerRepository->getRecords($id);
        if($banner) {
            return view('pages.admin.banner.edit', [
                    'validator' => $validator,
                    'banner' => $banner
                ]
            );
        } else {
            return abort(404);
        }
    }

    public function handleAddBanner(Request $request) 
    {
        $validator = Validator::make($request->all(), $this->validationRules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $status = $this->BannerRepository->addBannerRec($request);
        if($status['status'] == '1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }
    }

    public function handleEditBanner(Request $request) 
    {
        $this->validationRules['banner_image'] = 'image|mimes:jpeg,jpg,png,gif,bmp';
        $validator = Validator::make($request->all(), $this->validationRules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $status = $this->BannerRepository->updateBannerRec($request);
        if($status['status']=='1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }
    }
}