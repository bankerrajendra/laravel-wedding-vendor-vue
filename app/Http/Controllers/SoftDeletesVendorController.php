<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\VendorEventInformation;
use App\Models\VendorEventImage;
use App\Models\VendorsImage;
use App\Models\VendorsInformation;
use App\Models\VendorShowcase;
use App\Models\VendorVideo;
use App\Models\VendorsSavedReviewMessage;
use App\Models\ShortlistedUser;
use App\Models\VendorCustomFaq;
use App\Models\EventReview;
use Auth;
use Illuminate\Http\Request;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SoftDeletesVendorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get Soft Deleted User.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public static function getDeletedUser($id)
    {
        $user = User::onlyTrashed()->where('id', $id)->onlySite()->hasVendorRole()->get();
        if (count($user) != 1) {


            return redirect(config('app.url').'/vendors/deleted')->with('error', trans('usersmanagement.errorUserNotFound'));
        }

        return $user[0];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "1";

        $users = User::onlyTrashed()->hasVendorRole()->onlySite()->orderBy('deleted_at', 'desc')->get();
        //$users = User::whereNotNull('deleted_at')->orderBy('deleted_at', 'desc')->get();
//        $users = DB::table('users')->select('*')
//            ->whereNotNull('deleted_at')
//            ->orderBy('deleted_at', 'desc')->get();

        $roles = Role::all();

		$stats = new \stdClass;
		
		$stats->all_users         = User::hasVendorRole()->onlySite()->count();
		$stats->banned_users      = User::where('banned', 1)->hasVendorRole()->onlySite()->count();
		//$stats->featured_users 	  = User::where('featured', 1)->where('banned', 0)->where('deactivated', 0)->count();
		$stats->deactivated_users = User::where('deactivated', 1)->hasVendorRole()->onlySite()->count();
		$stats->active_users 	  = User::where('banned', 0)->onlySite()->hasVendorRole()->where('deactivated', 0)->count();
        $stats->deleted_users 	  = User::onlyTrashed()->onlySite()->hasVendorRole()->count();
        return View('vendorsmanagement.show-deleted-vendors', compact('users', 'roles','stats'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = self::getDeletedUser($id);

        return view('vendorsmanagement.show-deleted-vendor')->withUser($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = self::getDeletedUser($id);
        $user->restore();

        return redirect(config('app.url').'/vendors/')->with('success', trans('usersmanagement.successRestore'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = self::getDeletedUser($id);

        /**
         * Delete users related events and other data
         */
        // get events
        $events = VendorEventInformation::where('vendor_id', $id)->get();
        if($events != null) {
            foreach($events as $event) {
                // get events and its images
                $imagesEvents = VendorEventImage::where('event_id', $event->id)->get();
                if($imagesEvents != null) {
                    foreach($imagesEvents as $evenImg) {
                        if(Storage::disk('s3')->exists('vendor_photos/original/' . $evenImg->image_full)) {
                            Storage::disk('s3')->delete('vendor_photos/original/' . $evenImg->image_full);
                        }
            
                        if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $evenImg->image_full)) {
                            Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $evenImg->image_full);
                        }
                        $evenImg->delete();
                    }
                }
                // delete event reviews
                EventReview::where('event_id', $event->id)->delete();
                // delete event record
                $event->delete();
            }
        }
        // get vendors images
        $vendorImgs = VendorsImage::where('vendor_id', $id)->get();
        if($vendorImgs != null) {
            foreach($vendorImgs as $imgVen) {
                if(Storage::disk('s3')->exists('vendor_photos/original/' . $imgVen->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/original/' . $imgVen->image_full);
                }
    
                if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $imgVen->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $imgVen->image_full);
                }
                $imgVen->delete();
            }
        }
        // delete vendor business information
        VendorsInformation::where('vendor_id', $id)->delete();
        // delete showcase
        VendorShowcase::where('vendor_id', $id)->delete();
        VendorVideo::where('vendor_id', $id)->delete();
        VendorsSavedReviewMessage::where('vendor_id', $id)->delete();
        ShortlistedUser::where('from_user', $id)->delete();
        ShortlistedUser::where('to_user', $id)->delete();
        VendorCustomFaq::where('vendor_id', $id)->delete();
        
        $user->forceDelete();

        return redirect(config('app.url').'/vendors/deleted')->with('success', trans('usersmanagement.successDestroy'));
    }
}
