<?php

namespace App\Http\Controllers;

use App\Events\Activity;
use App\Events\FileUpload;
use App\Logic\Common\LocationRepository;
use App\Logic\Membership\MembershipRepository;
use App\Models\MembershipPlanPackages;
use App\Models\MembershipPlans;
use App\Models\PhotoNotification;
use App\Models\UsersImage;
use App\Models\VendorsImage;
use App\Models\VendorVideo;
use App\Models\UserProfileFlag;
use App\Models\VendorCustomFaq;

use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\UsersInformation;
use App\Models\VendorsInformation;
use App\Models\Message;
use JsValidator;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralEmail;

class AjaxController extends Controller
{
    public function fetchLocation(Request $request, LocationRepository $locationRepository){
        $select = $request->get('select');
        $value = $request->get('value');

        $dependent = $request->get('dependent');
        $output = array();

        try {

            if (($select == 'country' || $select == 'country-mob') && ($dependent == 'city' || $dependent == 'city-mob')) {
                $locations = $locationRepository->getCountryCities($value);

                $count = 0;
                foreach ($locations as $location) {
                    $output[$count]['id'] = $location->id;
                    $output[$count]['value'] = $location->name;
                    $count++;
                }
                return response()->json($output);
            } elseif (($select == 'country' || $select == 'country_merrco' || $select == 'country-mob') && ($dependent == 'state' || $dependent == 'state_merrco' || $dependent == 'state-mob')) {

                $locations = $locationRepository->getStates($value);
                $count = 0;

                foreach ($locations as $location) {
                    $output[$count]['id'] = $location->id;
                    $output[$count]['value'] = $location->name;
                    $count++;
                }
                return response()->json($output);
            } elseif (($select == 'state' || $select == 'state-mob') && ($dependent == 'city' || $dependent == 'city-mob')) {
                $locations = $locationRepository->getCities($value);
                $count = 0;
                foreach ($locations as $location) {
                    $output[$count]['id'] = $location->id;
                    $output[$count]['value'] = $location->name;
                    $count++;
                }
                return response()->json($output);
            } elseif ($select == 'country_p' && $dependent == 'city_p') {
                $locations = $locationRepository->getCountryCities($value);

                $count = 0;
                foreach ($locations as $location) {
                    $output[$count]['id'] = $location->id;
                    $output[$count]['value'] = $location->name;
                    $count++;
                }
                return response()->json($output);
            }elseif ($select == 'country_p' && $dependent == 'state_p') {

                $locations = $locationRepository->getStates($value);
                $count = 0;

                foreach ($locations as $location) {
                    $output[$count]['id'] = $location->id;
                    $output[$count]['value'] = $location->name;
                    $count++;
                }
                return response()->json($output);
            }elseif ($select == 'state_p' && $dependent == 'city_p') {
                $locations = $locationRepository->getCities($value);
                $count = 0;
                foreach ($locations as $location) {
                    $output[$count]['id'] = $location->id;
                    $output[$count]['value'] = $location->name;
                    $count++;
                }
                return response()->json($output);
            }
        } catch(Exception $e) {
            return [];
        }
        return false;
    }

    /**
    @Religion - Community - Sub-caste mapping
    @params : Form input
    @Returns : JSON object
     **/
    public function	mapReligion(){
        //
    }

    public function manageUserEntries(Request $request){

        $action = $request->get("action");
        switch($action){
            ## Leave feedback against user entry
            case "leave_feedback":
                return $this->userProfileLeaveFeedback($request);
                break;

            ## Approve user entry
            case "approve_entry":
                return $this->userProfileApproveEntry($request);
                break;

            default:
                return response()->json(array("status"=>""));
        }
    }

    ## Leave feedback
    public function userProfileLeaveFeedback($data){
        $payload = explode("-",$data["payload"]);
        $tablRemark   = $payload[0];
        $propertyName = $payload[1];
        $userId 	  = $payload[2];
        $feedback     = $data["message"];

        try{
//			UserProfileFlag::Create([
//				'user_id' 		=> $userId,
//				'property_name' => $propertyName,
//				'table_remark'	=> $tablRemark,
//				'feedback'		=> $feedback
//			]);
            UserProfileFlag::where('user_id',$userId)->where('property_name',$propertyName)->update(array('status'=>'1', 'feedback'=>$feedback));
            $message = array("status"=>1);
            
            /**
             * Email when admin disapprove
             */
            $toUser = User::where('id', $userId)->first();
            if(User::where('id', $userId)->hasVendorRole()->exists()) {
                $notification_flag = $toUser->vendors_information->notify_content_approve_deny;
            } else if(User::where('id', $userId)->hasUserRole()->exists()) {
                $notification_flag = $toUser->get_user_information()->notify_content_approve_deny;
            }
            if($notification_flag == "Y") {
                $type = getUnsubscribeType('notify_content_approve_deny');
                $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Content Disapproved',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $toUser->first_name,
                        '[%FIELD_DISAPPROVED_REASON%]' => $feedback,
                        '[%FIELD_DISAPPROVED%]' => ucwords(str_replace("_", " ", $propertyName))
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry)); 
            }

        }Catch(Exception $e){
            $message = $e->getMessage();
        }
        return response()->json($message);
    }

    public function approvePhoto(Request $request) {
        $photo_id = explode("-",$request["photo_id"]);

        try {
            UsersImage::where(array("id" => $photo_id))->update(array("is_approved"=> 'Y','disapprove_reason'=>''));
            $userImg=UsersImage::select('user_id')->where("id",$photo_id)->first();
            $userId=$userImg->user_id;

            $totalApprovedImages=UsersImage::where('user_id', $userId)->where("is_approved", 'Y')->count();

            if($totalApprovedImages==1) {
                //fetch all the users from photo_notifications where notification_type='profile_photo_request' and status='P'
                $photoRequests=PhotoNotification::where('to_user', $userId)->where('notification_type', 'profile_photo_request')->where('status', 'P')->get();

                if($photoRequests) {
                    foreach($photoRequests as $photoRequest) {
                        $message = config('constants.messages.photo_request_uploaded');
                        /**
                         * Email when admin approves profile image send email to requesters
                         */
                        $fromUser = User::where('id', $photoRequest->from_user)->first();
                        $toUser = User::where('id', $userId)->first();
                        if($fromUser->get_user_information()->notify_content_approve_deny == "Y") {
                            $type = getUnsubscribeType('notify_content_approve_deny');
                            $unsubscribe_link = route('unsubscribe', [ 'token' => $fromUser->token, 'type' => $type ]);
                            $mail_req_arry = [
                                'mail_name' => 'Profile Photo Request - Status',
                                'mail_replace_vars' => [
                                    '[%USER_SENDER_FIRST_NAME%]' => $fromUser->first_name,
                                    '[%USER_FIRST_NAME%]' => $toUser->first_name,
                                    '[%USER_PROFILE%]' => $toUser->profileLink()
                                ],
                                'unsubscribe_link' => $unsubscribe_link
                            ];
                            Mail::to($fromUser->email)->send(new GeneralEmail($mail_req_arry)); 
                        }
                        Message::sendMessageByUserId($photoRequest->from_user, $message, $photoRequest->to_user);
                    }
                }

                PhotoNotification::where('to_user', $userId)->where('notification_type', 'profile_photo_request')->delete();
            }

            $message = array("status"=>1);
        } Catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($message);
    }

    public function disapprovePhoto(Request $request) {
        $photo_id = explode("-",$request["photo_id"]);
        $disapprove_reason = $request['message'];

        try {
            $user_image = UsersImage::where(array("id" => $photo_id))->update(array("is_approved"=> 'D','disapprove_reason'=>$disapprove_reason));
            $message = array("status"=>1);
            $userId_detail = UsersImage::where(array("id" => $photo_id))->first(['user_id']);
            /**
             * Email when admin disapprove image
             */
            $toUser = User::where('id', $userId_detail->user_id)->first();
            if($toUser->get_user_information()->notify_content_approve_deny == "Y") {
                $type = getUnsubscribeType('notify_content_approve_deny');
                $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Disapprove Image',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $toUser->first_name,
                        '[%FIELD_DISAPPROVED_REASON%]' => $disapprove_reason,
                        '[%FIELD_DISAPPROVED%]' => 'Image'
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry)); 
            }

        } Catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($message);
    }

    ## Approve user data
    public function userProfileApproveEntry($data){
        $payload = explode("-",$data["payload"]);
        $propertyName = $payload[1];
        $userId 	  = $payload[2];
        $responseArry = [];
        try {
            $conditions = array(
                'property_name'	=>$propertyName,
                'user_id'		=>$userId
            );

            // update user data
            $profileObj = UserProfileFlag::where($conditions)->first();
            $company_slug = '';
            if($profileObj->table_remark == "vi") {
                if($propertyName == 'company_name') {
                    // update the company slug value
                    // 1. check slug exists by slugyfy
                    $slugyfyCompanyName = slugifyText($profileObj->property_value);
                    if(VendorsInformation::where('company_slug', $slugyfyCompanyName)->exists()) {
                        $cmpNameCnt = VendorsInformation::where('company_slug', 'like', '%'.$slugyfyCompanyName.'%')->count();
                        // 2. update slug by adding -2
                        $slugyfyCompanyName .= "-".($cmpNameCnt+1);
                    }
                    // 3. Update the company slug value
                    VendorsInformation::where(['vendor_id' => $profileObj->user_id])->update(['company_slug' => $slugyfyCompanyName]);
                    $company_slug = $slugyfyCompanyName;
                }
                VendorsInformation::where(['vendor_id' => $profileObj->user_id])->update([$propertyName => $profileObj->property_value]);
            } else if($profileObj->table_remark == "u") {
                User::where(['id' => $profileObj->user_id])->update([$propertyName => $profileObj->property_value]);
            }

            // send field name with value
            $responseArry = [
                'status' => 1,
                'field' => $propertyName,
                'value' => $profileObj->property_value
            ];
            if($propertyName == 'company_name') {
                $responseArry['company_slug'] = $company_slug;
            } else if($propertyName == 'company_name' || $propertyName == 'specials') {
                unset($responseArry['value']);
            }

            UserProfileFlag::where($conditions)->delete();
            $message = array("status"=>1);

        } Catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($responseArry);
    }

    ## updateFlagedUserre_entry
    public function updateFlagedUserre_entry(Request $request){

        $this->user = Auth::user();

        $userFlagData = getFlaggedUserData($this->user->id);
        $arr=array();

        foreach ($userFlagData as $flagData) {
            if($flagData->property_name=='first_name' && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|alpha_spaces|max:25';

            if($flagData->property_name=='last_name' && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|alpha_spaces|max:25';

            if($flagData->property_name=='mobile_number' && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|numeric|phone';

            if($flagData->property_name=='address'  && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|max:200';

            if($flagData->property_name=='zip'  && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|min:5';

            if($flagData->property_name=='company_name'  && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|alpha_spaces|max:200';

            if($flagData->property_name=='established_year'  && $flagData->status=='1')
                $arr[$flagData->property_name] = 'past_year';

            if($flagData->property_name=='meta_keywords'  && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|max:500';

            if($flagData->property_name=='business_information'  && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|max:500';
            
            if($flagData->property_name=='specials'  && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|max:500';

            if(
                (
                    $flagData->property_name=='business_website' ||
                    $flagData->property_name=='instagram' || 
                    $flagData->property_name=='twitter' || 
                    $flagData->property_name=='linkedin' || 
                    $flagData->property_name=='pinterest'
                )  
                && $flagData->status=='1'
            ) {
                $arr[$flagData->property_name] = 'validate_url';
            }
            
        }

        $validatedData =  $request->validate($arr);

        $input = $request->all();
        $user_id = $input['user_id'];
        
        if($request->has('first_name')){
            // User::where(array("id" => $user_id))->update(array("first_name"=> $input['first_name']));
            $this->save_profile_property_value($user_id, 'first_name', $request->input('first_name'));
        }

        if($request->has('last_name')){
            //User::where(array("id" => $user_id))->update(array("last_name"=> $input['last_name']));
            $this->save_profile_property_value($user_id, 'last_name', $request->input('last_name'));
        }

        if($request->has('mobile_number')){
            //User::where(array("user_id" => $user_id))->update(array("mobile_number"=> $input['mobile_number']));
            $this->save_profile_property_value($user_id, 'mobile_number', $request->input('mobile_number'));
        }
        if($request->has('address')){
            //User::where(array("user_id" => $user_id))->update(array("address"=> $input['address']));
            $this->save_profile_property_value($user_id, 'address', $request->input('address'));
        }

        if($request->has('zip')){
            //User::where(array("user_id" => $user_id))->update(array("zip"=> $input['zip']));
            $this->save_profile_property_value($user_id, 'zip', $request->input('zip'));
        }

        if($request->has('company_name')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("company_name"=> $input['company_name']));
            $this->save_profile_property_value($user_id, 'company_name', $request->input('company_name'));
        }

        if($request->has('established_year')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("established_year"=> $input['established_year']));
            $this->save_profile_property_value($user_id, 'established_year', $request->input('established_year'));
        }

        if($request->has('meta_keywords')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("meta_keywords"=> $input['meta_keywords']));
            $this->save_profile_property_value($user_id, 'meta_keywords', $request->input('meta_keywords'));
        }

        if($request->has('business_information')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("business_information"=> $input['business_information']));
            $this->save_profile_property_value($user_id, 'business_information', $request->input('business_information'));
        }
        if($request->has('specials')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("specials"=> $input['specials']));
            $this->save_profile_property_value($user_id, 'specials', $request->input('specials'));
        }
        if($request->has('business_website')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("business_website"=> $input['business_website']));
            $this->save_profile_property_value($user_id, 'business_website', $request->input('business_website'));
        }
        if($request->has('facebook')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("facebook"=> $input['facebook']));
            $this->save_profile_property_value($user_id, 'facebook', $request->input('facebook'));
        }
        if($request->has('instagram')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("instagram"=> $input['instagram']));
            $this->save_profile_property_value($user_id, 'instagram', $request->input('instagram'));
        }
        if($request->has('pinterest')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("pinterest"=> $input['pinterest']));
            $this->save_profile_property_value($user_id, 'pinterest', $request->input('pinterest'));
        }
        if($request->has('twitter')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("twitter"=> $input['twitter']));
            $this->save_profile_property_value($user_id, 'twitter', $request->input('twitter'));
        }
        if($request->has('linkedin')){
            //VendorsInformation::where(array("vendor_id" => $user_id))->update(array("linkedin"=> $input['linkedin']));
            $this->save_profile_property_value($user_id, 'linkedin', $request->input('linkedin'));
        }

        UserProfileFlag::where('user_id', $user_id)->update(array('status'=>'0'));
    }

    protected function save_profile_property_value($user_id, $property_name, $property_value)
    {
        // save to profile flag table as property value
        UserProfileFlag::where(['user_id' => $user_id, 'property_name' => $property_name])
        ->update(['property_value' => $property_value]);
    }

    public function batch_ops(Request $request){
        $action = $request->post("action");
        $user_ids = explode(",", $request->post("id"));

        switch($action)
        {
            case "batch_grant_access":
                $this->process_batch_job(array(
                    "users", "access_grant", "Y", $user_ids
                ));

                /**
                 * Send activation email
                 */
                foreach($user_ids as $user_id) {
                    try {
                        $user_details = User::find($user_id);
                        $unsubscribe_link = route('unsubscribe', [ 'token' => $user_details->token, 'type' => '' ]);
                        if($user_details->hasRole('vendor')) {
                            $mail_name = 'Vendor Profile Activated';
                        } else {
                            $mail_name = 'User Profile Activated';
                        }
                        $mail_req_arry = [
                            'mail_name' => $mail_name,
                            'mail_replace_vars' => [
                                '[%USER_FIRST_NAME%]' => $user_details->first_name,
                                '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                                '[%WEB_LOGIN_URL%]' => route('login')
                            ],
                            'unsubscribe_link' => $unsubscribe_link
                        ];
                        Mail::to($user_details->email)->send(new GeneralEmail($mail_req_arry));
                    } catch (\Exception $e){
                        \Log::error("Some error while sending profile approval mail for user id:".$user_id);
                    }
                }

                break;

            case "batch_revoke_access":
                $this->process_batch_job(array(
                    "users", "access_grant", "N", $user_ids
                ));
                break;

            case "batch_ban":
                $this->process_batch_job(array(
                    "users", "banned", 1, $user_ids
                ));
                break;

            case "batch_unban":
                $this->process_batch_job(array(
                    "users", "banned", 0, $user_ids
                ));
                break;

            case "batch_remove":
                $ts = gmdate("Y-m-d h:i:s");
                $this->process_batch_job(array(
                    "users", "deleted_at",$ts,$user_ids
                ));
                $ipAddress = new CaptureIpTrait();
                $ipAdd=$ipAddress->getClientIp();
                $this->process_batch_job(array(
                    "users", "deleted_ip_address",$ipAdd,$user_ids
                ));

                $this->process_batch_job(array(
                    "users", "deactivated", 0, $user_ids
                ));
                $this->process_batch_job(array(
                    "users", "banned", 0, $user_ids
                ));
                /**
                 * Send delete email
                 */
                foreach($user_ids as $user_id) {
                    try {
                        $user_details = User::find($user_id);
                        $unsubscribe_link = route('unsubscribe', [ 'token' => $user_details->token, 'type' => '' ]);
                        $mail_req_arry = [
                            'mail_name' => 'Account deleted',
                            'mail_replace_vars' => [
                                '[%USER_FIRST_NAME%]' => $user_details->first_name
                            ],
                            'unsubscribe_link' => $unsubscribe_link
                        ];
                        Mail::to($user_details->email)->send(new GeneralEmail($mail_req_arry));
                    } catch (\Exception $e){
                        \Log::error("Some error while seding delete account mail for user id:".$user_id);
                    }
                }
                break;

            case "batch_activate":
                $this->process_batch_job(array(
                    "users", "deactivated", 0, $user_ids
                ));
                $this->process_batch_job(array(
                    "users", "banned", 0, $user_ids
                ));
                $this->process_batch_job(array(
                    "users", "access_grant", "Y", $user_ids
                ));
                /**
                 * Send activation email
                 */
                foreach($user_ids as $user_id) {
                    try {
                        $user_details = User::find($user_id);
                        $unsubscribe_link = route('unsubscribe', [ 'token' => $user_details->token, 'type' => '' ]);
                        if($user_details->hasRole('vendor')) {
                            $mail_name = 'Vendor Profile Activated';
                        } else {
                            $mail_name = 'User Profile Activated';
                        }
                        $mail_req_arry = [
                            'mail_name' => $mail_name,
                            'mail_replace_vars' => [
                                '[%USER_FIRST_NAME%]' => $user_details->first_name,
                                '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                                '[%WEB_LOGIN_URL%]' => route('login')
                            ],
                            'unsubscribe_link' => $unsubscribe_link
                        ];
                        Mail::to($user_details->email)->send(new GeneralEmail($mail_req_arry));
                    } catch (\Exception $e){
                        \Log::error("Some error while sending profile approval mail for user id:".$user_id);
                    }
                }
                break;

            case "batch_deactivate":
                $this->process_batch_job(array(
                    "users", "deactivated", 1, $user_ids
                ));
                $this->process_batch_job(array(
                    "users", "banned", 0, $user_ids
                ));
                $this->process_batch_job(array(
                    "users", "access_grant", "N", $user_ids
                ));
                /**
                 * Send deactivation email
                 */
                foreach($user_ids as $user_id) {
                    try {
                        $user_details = User::find($user_id);
                        $unsubscribe_link = route('unsubscribe', [ 'token' => $user_details->token, 'type' => '' ]);
                        $mail_req_arry = [
                            'mail_name' => 'Account Deactivated',
                            'mail_replace_vars' => [
                                '[%USER_FIRST_NAME%]' => $user_details->first_name
                            ],
                            'unsubscribe_link' => $unsubscribe_link
                        ];
                        Mail::to($user_details->email)->send(new GeneralEmail($mail_req_arry));
                    } catch (\Exception $e){
                        \Log::error("Some error while sending upgrade membership mail for user id:".$user_id);
                    }
                }
                break;

            case "batch_disable_user":
                $this->process_batch(
                    array("users", "disable", 1, $user_ids)
                );
                break;

            case "batch_feature":
                $this->process_batch_job(array(
                    "users", "featured", 1, $user_ids
                ));
                break;

            default:
                return 0;
        }
    }

    public function process_batch_job($data)
    {
        $col = $data[1];
        $table = $data[0];
        $val = $data[2];
        foreach($data[3] as $id)
        {
            # Users information table not used
            # Access grant moved to Users
            if($id !== ""){
                $user = User::withTrashed()->find($id);
                $user->$col = $val;
                $user->save();
            }
        }
    }

    /**
     * Show Membership Payment History
     */
    public function showUserPaymentHistory(Request $request, MembershipRepository $membershiprepo)
    {
        $user_id = $request->input('uid');
        if($user_id != "") {
            try {
                $user = User::find($user_id);
                $get_membership_history = $membershiprepo->getUserMembershipHistory($user_id);
                $json_arry['user_name'] = $user->first_name .' '.$user->last_name;
                $json_arry['user_email'] = $user->email;
                $json_arry['membership_history'] = $get_membership_history;
                return response()->json($json_arry);
            } catch (Exception $e){
                return $message = $e->getMessage();
            }

        }
    }

    /**
     * Show User Info
     */
    public function showUserInfo(Request $request)
    {
        $user_id = $request->input('uid');
        if($user_id != "") {
            try {
                $user = User::find($user_id);
                $json_arry['user_id'] = $user->id;
                $json_arry['user_name'] = $user->first_name .' '.$user->last_name;
                $json_arry['user_email'] = $user->email;
                // get mobile number and country code
                // $user_information = $user->get_user_information();
                // $json_arry['country_code'] = getPhoneCode($user_information->country_code);
                // $json_arry['mobile_number'] = $user_information->mobile_number;
                return response()->json($json_arry);
            } catch (Exception $e){
                return $message = $e->getMessage();
            }
        }
    }

    /**
     * Change Plan of User
     */
    public function handleEditPlanAction(Request $request, MembershipRepository $membershiprepo)
    {
        $user_id = $request->input('uid');
        if($user_id != "") {
            try {
                /**
                 * Prepare array to submit
                 */
                $submitted_vals = [];
                $submitted_vals['user_id'] = $request->input('uid');
                $submitted_vals['package_id'] = $request->input('package_id');
                // get plan info
                $package_details = MembershipPlanPackages::find($request->input('package_id'));
                // get plan name
                $plan_details = MembershipPlans::find($package_details->plan_id);
                $submitted_vals['plan_id'] = $plan_details->id;
                $submitted_vals['plan_name'] = $plan_details->plan_name;
                $submitted_vals['package_amount'] = $package_details->amount;
                $submitted_vals['package_duration'] = $package_details->duration;
                $submitted_vals['package_duration_type'] = $package_details->type;
                $payment_mode = $request->input('payment_mode');
                $submitted_vals['paid_by'] = "Admin";
                $submitted_vals['note'] = $request->input('payment_note');
                $membershiprepo->savePaidMember($payment_mode, $submitted_vals, []);
                return response()->json(['status' => 'success', 'message' => 'Plan Updated successfully.']);
            } catch (Exception $e){
                return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
            }
        }
    }

    public function approveVendorPhoto(Request $request) {
        $photo_id = explode("-",$request["photo_id"]);

        try {
            VendorsImage::where(array("id" => $photo_id))->update(array("is_approved"=> 'Y','disapprove_reason'=>''));
            $userImg = VendorsImage::select('vendor_id')->where("id",$photo_id)->first();
            $userId = $userImg->vendor_id;
            $message = array("status"=>1);
        } Catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($message);
    }

    public function disapproveVendorPhoto(Request $request) {
        $photo_id = explode("-",$request["photo_id"]);
        $disapprove_reason = $request['message'];

        try {
            $user_image = VendorsImage::where(array("id" => $photo_id))->update(array("is_approved"=> 'D','disapprove_reason'=>$disapprove_reason));
            $message = array("status"=>1);
            $imgObj = VendorsImage::where(array("id" => $photo_id))->first();
            // send email
            $toUser = User::where('id', $imgObj->vendor_id)->first();
            if(User::where('id', $toUser->id)->hasVendorRole()->exists()) {
                $notification_flag = $toUser->vendors_information->notify_content_approve_deny;
            }
            if($notification_flag == "Y") {
                $type = getUnsubscribeType('notify_content_approve_deny');
                $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Disapprove Image',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $toUser->first_name,
                        '[%FIELD_DISAPPROVED_REASON%]' => $disapprove_reason,
                        '[%FIELD_DISAPPROVED%]' => 'Image'
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
            }
        } Catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($message);
    }

    public function approveVendorVideo(Request $request) {
        $video_id = explode("-",$request["video_id"]);

        try {
            VendorVideo::where(array("id" => $video_id))->update(array("is_approved"=> 'Y','disapprove_reason'=>''));
            $message = array("status"=>1);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($message);
    }

    public function disapproveVendorVideo(Request $request) {
        $video_id = explode("-",$request["video_id"]);
        $disapprove_reason = $request['message'];

        try {
            VendorVideo::where(array("id" => $video_id))->update(array("is_approved"=> 'D','disapprove_reason'=>$disapprove_reason));
            $message = array("status"=>1);
            $vidObj = VendorVideo::where(array("id" => $video_id))->first();
            // send email
            $toUser = User::where('id', $vidObj->vendor_id)->first();
            if(User::where('id', $toUser->id)->hasVendorRole()->exists()) {
                $notification_flag = $toUser->vendors_information->notify_content_approve_deny;
            }
            if($notification_flag == "Y") {
                $type = getUnsubscribeType('notify_content_approve_deny');
                $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Disapprove Image',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $toUser->first_name,
                        '[%FIELD_DISAPPROVED_REASON%]' => $disapprove_reason,
                        '[%FIELD_DISAPPROVED%]' => 'Image'
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
            }

        } catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($message);
    }
    
    public function approveVendorCustomFaq(Request $request) {
        $faq_id = explode("-",$request["faq_id"]);

        try {
            VendorCustomFaq::where(array("id" => $faq_id))->update(array("is_approved"=> 'Y','disapprove_reason'=>''));
            $message = array("status"=>1);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($message);
    }

    public function disapproveVendorCustomFaq(Request $request) {
        $faq_id = explode("-",$request["faq_id"]);
        $disapprove_reason = $request['message'];

        try {
            VendorCustomFaq::where(array("id" => $faq_id))->update(array("is_approved"=> 'D','disapprove_reason'=>$disapprove_reason));
            $message = array("status"=>1);
        } catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($message);
    }
}