<?php

namespace App\Http\Controllers\Activity;

use App\Logic\Activity\UserActivityStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Events\Activity;
use App\Models\PhotoNotification;

class PhotoController extends Controller
{
    //
    protected $currentUser;
    protected $activityRepository;

    public function __construct(UserActivityStatusRepository $activityRepository)
    {
        $this->currentUser = Auth::user();
        $this->activityRepository = $activityRepository;
    }

    public function sharePhotos(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.share_photos'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function unsharePhotos(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withTrashed()->withEncryptedId($request->input('profileId'))->count() > 0){
                    $fromUser = User::withTrashed()->withEncryptedId($request->input('profileId'))->first();
                    $profileId = $fromUser->id;
                    $response = event(new Activity(config('constants.activity.unshare_photos'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function unshareAllPhotos(Request $request){
        $response = null;
        $auth_id = Auth::id();
        if(PhotoNotification::where('from_user','=',$auth_id)->where('notification_type', 'share_photo')->count() > 0){
            $unshareAllRecord = PhotoNotification::where('from_user','=',$auth_id)->where('notification_type', 'share_photo')->delete();
            $response = [
                'status' => 1,
                'message'=>'Users removed from shared list'
            ];
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in shared list'
            ];
        }

        return redirect()->route('share-photo-permission');
    }

    public function acceptPhotoRequest(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $fromUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $fromUser->id;
                    $response = event(new Activity(config('constants.activity.accept_request_photos'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function denyPhotoRequest(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withTrashed()->withEncryptedId($request->input('profileId'))->count() > 0){
                    $fromUser = User::withTrashed()->withEncryptedId($request->input('profileId'))->first();
                    $profileId = $fromUser->id;
                    $response = event(new Activity(config('constants.activity.deny_request_photos'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function requestToView(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.request-toview'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }


    public function sendPhotoRequest(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.send-photo-request'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function denyAllPhotos(Request $request){
        $response = null;
        $auth_id = Auth::id();
        if(PhotoNotification::where('to_user','=',$auth_id)->where('notification_type', 'private_photo_request')->count() > 0){
            $unshareAllRecord = PhotoNotification::where('to_user','=',$auth_id)->where('notification_type', 'private_photo_request')->delete();
            $response = [
                'status' => 1,
                'message'=>'All requests to view private photos has been denied.'
            ];
        }
        else{
            $response = [
                'status' => 0,
                'message'=>'User does not exist in private photo requests list'
            ];
        }

        return redirect()->route('private-photo-permission');
    }

}