<?php

namespace App\Http\Controllers\Activity;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Events\Activity;
use App\Logic\Activity\UserActivityStatusRepository;
use Illuminate\Support\Facades\Auth;

class SkipController extends Controller
{
    //
    protected $currentUser;
    protected $activityRepository;

    public function __construct(UserActivityStatusRepository $activityRepository)
    {
        $this->currentUser = Auth::user();
        $this->activityRepository = $activityRepository;
    }

    public function skipProfile(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.skip'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function deleteSkippedProfile(Request $request){
        $response = null;
        if($request->has('profileId')) {
            if ($request->filled('profileId')) {
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.delete_skipped'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }
            else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function showSkippedProfiles(){
        $skippedUsers = $this->activityRepository->getSkippedUsers();
        return view('pages.activity.skipped_users')->with(['skippedUsers'=>$skippedUsers]);
    }
}
