<?php

namespace App\Http\Controllers\Activity;

use App\Logic\Activity\UserActivityStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Events\Activity;

class ShortlistController extends Controller
{
    //
    protected $currentUser;
    protected $activityRepository;

    public function __construct(UserActivityStatusRepository $activityRepository)
    {
        $this->currentUser = Auth::user();
        $this->activityRepository = $activityRepository;
        $this->middleware('talk');
    }

    public function shortlistProfile(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.shortlist'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function showShortlistedUsers(){
        $shortlistedUsers = $this->activityRepository->getShortlistedUsers();
        return view('pages.activity.shortlisted')->with(['shortlistedUsers'=>$shortlistedUsers]);
    }

    public function deleteShortlisted(Request $request){
        $response = null;
        if($request->has('profileId')) {
            if ($request->filled('profileId')) {
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.delete_shortlisted'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }
            else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }
}
