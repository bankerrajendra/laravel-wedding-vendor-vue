<?php

namespace App\Http\Controllers\Activity;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Logic\Activity\UserActivityStatusRepository;
use Illuminate\Support\Facades\Auth;
use App\Events\Activity;
use App\Models\User;

class BlockController extends Controller
{
    //
    protected $currentUser;
    protected $activityRepository;

    public function __construct(UserActivityStatusRepository $activityRepository)
    {
        $this->currentUser = Auth::user();
        $this->activityRepository = $activityRepository;
    }

    public function blockProfile(Request $request){
        if($request->has('profileId')) {
            if ($request->filled('profileId')) {
                if (User::withTrashed()->withEncryptedId($request->input('profileId'))->count() > 0) {
                    $toUser = User::withTrashed()->withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.block_profile'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }
            else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function showBlockedProfiles(){
        $blockedProfiles = $this->activityRepository->getBlockedUsers();
        return view('pages.activity.blocked')->with(['blockedProfiles'=>$blockedProfiles]);
    }

    public function unblockProfile(Request $request){
        $response = null;
        if($request->has('profileId')) {
            if ($request->filled('profileId')) {
                if(User::withTrashed()->withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withTrashed()->withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.unblock_profile'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }
            else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

}
