<?php

namespace App\Http\Controllers\Activity;

use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Report;
use App\Models\User;
use Mail;
use App\Mail\GeneralEmail;

class ReportController extends Controller
{
    //
    public function reportUser(Request $request){
        $currentUser = Auth::user();
        if($request->has('profileId')) {
            if ($request->filled('profileId')) {
                if (User::withEncryptedId($request->input('profileId'))->count() > 0) {
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    if(Report::where('from_user', $currentUser->id)->where('to_user', $profileId)->count() > 0){
                        $report = Report::where('from_user', $currentUser->id)->where('to_user', $profileId)->first();
                        $report->reason = $request->input('reason');
                        $report->description = $request->input('description');
                        $report->save();
                    }
                    else{
                        Report::create(
                            [
                                'from_user'=>$currentUser->id,
                                'to_user'=>$profileId,
                                'reason'=>$request->input('reason'),
                                'description'=>$request->input('description')
                            ]
                        );
                    }
                    $response = array(
                        'status' => 1,
                        'message' => 'Report submitted successfully'
                    );
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }
            else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function showReports(Request $request){
        $pageNo=1;
        $pagintaionEnabled = config('usersmanagement.enablePagination');

        if($pagintaionEnabled) {
            $reports = Report::where('user_deleted', '=', '0')->where('type', '=', 'report')->latest()->paginate(config('usersmanagement.paginateListSize'));
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
        } else {
            $reports = Report::where('user_deleted', '=', '0')->where('type', '=', 'report')->latest()->get();
        }

        if($pageNo>1)
            $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;

        return view('pages.admin.reports')->with([
            'reports'=>$reports,'pageNo'=>$pageNo
        ]);
    }

    /**
     * Handle admin reply
     */
    public function reportAdminReply(Request $request)
    {
        $report_id = $request->has('report_id') ? $request->input('report_id') : "";
        if($report_id == "") {
            return response()->json([
                'status' => 0,
                'message' => 'Report Id cann\'t blank'
            ]);
        }
        $admin_reply = $request->has('admin_reply') ? $request->input('admin_reply') : "";
        if($admin_reply == "") {
            $response = [
                'status' => 0,
                'message' => "Admin Reply cann't blank"
            ];
        } else {
            if(strlen($admin_reply) > 250) {
                $response = [
                    'status' => 0,
                    'message' => 'Reply should not exceed 250 characters'
                ];
            } else {
                // update the record
                $report = Report::where('id', '=', $report_id)->first();
                $report->admin_reply = $admin_reply;
                $report->save();
                $response = [
                    'status' => 1,
                    'message' => 'Admin reply submitted successfully'
                ];
                /**
                 * Email when admin send flag notification to 'to user'
                 */
                $toUser = User::where('id', $report->to_user)->first();
                if($toUser->get_user_information()->notify_content_approve_deny == "Y") {
                    $type = getUnsubscribeType('notify_content_approve_deny');
                    $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => $type ]);
                    $mail_req_arry = [
                        'mail_name' => 'Report Flag Notification',
                        'mail_replace_vars' => [
                            '[%USER_FIRST_NAME%]' => $toUser->first_name
                        ],
                        'unsubscribe_link' => $unsubscribe_link
                    ];
                    Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
                }
            }
        }
        return response()->json($response);
    }

    /**
     * Handle admin report trash
     */
    public function reportAdminTrash($report_id)
    {
        // update the record
        $report = Report::where('id', '=', $report_id)
                        ->first()
                        ->delete();
        return redirect()->back();
    }
}
