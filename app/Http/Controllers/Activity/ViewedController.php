<?php

namespace App\Http\Controllers\Activity;

use App\Logic\Activity\UserActivityStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use App\Events\Activity;
use App\Models\User;

class ViewedController extends Controller
{
    //
    protected $currentUser;
    protected $activityRepository;

    public function __construct(UserActivityStatusRepository $activityRepository)
    {
        $this->currentUser = Auth::user();
        $this->activityRepository = $activityRepository;
        //$this->middleware('talk');
    }

    public function showViewedMeUsers(Request $request){


        $resultJson=$this->userActivity($request);
        //return view('pages.activity.viewed-me')->with(['viewedUsers'=>$viewedUsers]);
        return view('pages.activity.activity')->with(['viewedUsers'=>$resultJson['results'], 'pagination'=>$resultJson['pagination'], 'tab'=>$resultJson['tab']]);
    }

    public function userActivity(Request $request) {
        $resultJson=array();
        $tab_name="";
        $tabs['viewedme_li']=$tabs['myfav_li']=$tabs['favme_li']='';
        $per_page = $request->has('per_page') ? $request->per_page : 2;
        $page = $request->has('page') ? $request->page : 1;
        $search_arry = [
            'per_page' => $per_page,
            'page' => $page
        ];

        if($tab_name=='') {
            $viewedUsers = $this->activityRepository->getWhoViewedMe($search_arry);
            $tabs['viewedme_li']='active';
        } else if($tab_name=='partner') {
            $viewedUsers = $this->connectActivity->getPartnerMatchUsers();  // my favorites
            $tabs['myfav_li']='active';
        } else if($tab_name=='diamond') {
            $viewedUsers = $this->connectActivity->getDiamondUsers();  //who favorited me
            $tabs['favme_li']='active';
        }

        /******************* API Specific Job ***********************/

         $tabs['has_records']=(!is_null($viewedUsers) && !$viewedUsers->isEmpty());

         $i=0;
        foreach($viewedUsers as $user_profile) {
            $from_user=$user_profile->from_user();
            $disabled_status=($from_user->banned == 1 || $from_user->deactivated == 1 || $from_user->deleted_at != null || $from_user->account_show=='N' || $user_profile->to_user()->account_show=='N' || $from_user->isSkipped() || $from_user->isSkippedMe() || $from_user->isBlockedMe() || $from_user->isBlocked())?'Y':'N';

            $resultJson[$i]['viewId']=$user_profile->id;
            $resultJson[$i]['updatedate_human']=$user_profile->updated_at->diffForHumans();
            $resultJson[$i]['account_show']=$user_profile->to_user()->account_show;
            $resultJson[$i]['disabled_status']=$disabled_status;
            $resultJson[$i]['userId']=$from_user->id;
            $resultJson[$i]['encryptId']=$from_user->getEncryptedId();
            $resultJson[$i]['profileLink']=$from_user->profileLink();
            $resultJson[$i]['profilePic']=$from_user->getVendorProfilePic();
            $resultJson[$i]['firstName']=ucfirst($from_user->getFirstname());
            $resultJson[$i]['isOnline']=$from_user->isOnline();
            $resultJson[$i]['age']=$from_user->getAge();

            $resultJson[$i]['hasConversation']=$from_user->hasConversation();
            $resultJson[$i]['city_name']=$from_user->city->name;
            $resultJson[$i]['state_name']=$from_user->state->name;
            $resultJson[$i]['country_name']=$from_user->country->name;
            $resultJson[$i]['sect_show']=$from_user->users_information->sect_show;
            $resultJson[$i]['community']=$from_user->users_information->get_community();
            $profession=(isset($from_user->users_information->profession)?$from_user->users_information->profession->profession:'');
            $resultJson[$i]['profession']=$profession;
            $resultJson[$i]['profession_disp']=(strlen($profession)>18)?(substr($profession,0,18)."..."):$profession;

            $resultJson[$i]['privatePhotoPermissionCnt']=$from_user->privatePhotoPermissionCnt();
            $resultJson[$i]['viewConversationLink']=route('conversation', ['user_id' => $from_user->getEncryptedId()]);
            $resultJson[$i]['religion']=$from_user->users_information->religion->religion;
            $resultJson[$i]['height']=config('constants.height.' . $from_user->users_information->height);
            $resultJson[$i]['education_flag']=$from_user->users_information->education;
            $resultJson[$i]['education']=$from_user->users_information->education->education;
            $resultJson[$i]['body_type']=$from_user->users_information->body_type;
            $resultJson[$i]['about_flag']=$from_user->users_information->about;
            $resultJson[$i]['about']=showReadMoreApi($from_user->users_information->about, 90, $resultJson[$i]['profileLink']);
            $i++;
        }

        $pagination = [
            'total' => $viewedUsers->total(),
            'total_pages' => ceil($viewedUsers->total()/$viewedUsers->perPage()),
            'per_page' => $viewedUsers->perPage(),
            'current_page' => $viewedUsers->currentPage(),
            'last_page' => $viewedUsers->lastPage()
        ];

        if($request->has('json')) {
            return response()->json(
                [
                    'results' => $resultJson,
                    'pagination' => $pagination,
                    'tab' => $tabs,
                    'status' => 201
                ]);
        } else {
            return [
                'results' => json_encode($resultJson),
                'pagination' => json_encode($pagination),
                'tab' => json_encode($tabs),
            ];
        }
    }

    public function deleteViewedMe(Request $request){
        $response = null;
        if($request->has('profileId')) {
            if ($request->filled('profileId')) {
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.delete_viewed_me'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }
            else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

}
