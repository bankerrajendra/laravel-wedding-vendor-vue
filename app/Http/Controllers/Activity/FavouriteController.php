<?php

namespace App\Http\Controllers\Activity;

use App\Logic\Activity\UserActivityStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Events\Activity;

class FavouriteController extends Controller
{
    //
    protected $currentUser;
    protected $activityRepository;

    public function __construct(UserActivityStatusRepository $activityRepository)
    {
        $this->currentUser = Auth::user();
        $this->activityRepository = $activityRepository;
        $this->middleware('talk');
    }

    public function likeProfile(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.like'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function likeBackProfile(Request $request){
        if($request->has('profileId')){
            if($request->filled('profileId')){
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $fromUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $fromUser->id;
                    $response = event(new Activity(config('constants.activity.like_back'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function deleteLikedProfile(Request $request){
        $response = null;
        if($request->has('profileId')) {
            if ($request->filled('profileId')) {
                if(User::withEncryptedId($request->input('profileId'))->count() > 0){
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $profileId = $toUser->id;
                    $response = event(new Activity(config('constants.activity.delete_liked'), $profileId));
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }
            else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }

    public function showMyFavourites(){
        $likedUsers = $this->activityRepository->getLikedUsers();
        return view('pages.activity.favourited')->with(['likedUsers'=>$likedUsers]);
    }

    public function showWhoFavouritedMe(){
        $likedMeUsers = $this->activityRepository->getWhoLikedMe();
        return view('pages.activity.who-favourited-me')->with(['likedMeUsers'=>$likedMeUsers]);
    }
}
