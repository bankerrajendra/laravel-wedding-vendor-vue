<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminEmail;

class EmailController extends Controller
{
    public function sendEmail(Request $request)
    {
        $email = $request->has('email') ? $request->input('email') : "";
        $message_body = $request->has('message_body') ? $request->input('message_body') : "";
        $subject = $request->has('subject') ? $request->input('subject') : "";
        $ajax = $request->has('ajax') ? $request->input('ajax') : false;

        if($email == "" || $message_body == "" || $subject == "") {
            $error_message = "Email, Subject OR Message cann't be empty, try again";
            if($ajax) {
                return response()->json(['status' => 'error', 'message' => $error_message]);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }

        try {
            if($email != "" && $message_body != "" && $subject != "") {

                $mail_req_arry = [
                    'subject' => $subject,
                    'message_body' => $message_body
                ];
                Mail::to($email)->send(new AdminEmail($mail_req_arry));
                if($ajax) {
                    return response()->json(['status' => 'success', 'message' => "Email sent successfully."]);
                } else {
                    return;
                }
            } else {
                $error_message = "Configuration is not proper, Some problem while sending Email";
                if($ajax) {
                    return response()->json(['status' => 'error', 'message' => $error_message]);
                } else {
                    return redirect()->back()->withErrors($error_message);
                }
            }
        } catch (Exception $e) {
            if($ajax) {
                return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
            } else {
                return redirect()->back()->withErrors($e->getMessage());
            }
        }
    }
}
