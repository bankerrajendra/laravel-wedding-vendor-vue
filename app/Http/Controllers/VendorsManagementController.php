<?php

namespace App\Http\Controllers;

use App\Logic\Common\LocationRepository;
use App\Logic\Common\UserInformationOptionsRepository;
use App\Logic\Admin\VendorCategoryRepository;
use App\Logic\Admin\SatelliteRepository;
use App\Models\Language;
use App\Models\Profile;
use App\Models\Religion;
use App\Models\Subcaste;
use App\Models\User;
use App\Models\City;
use App\Models\OthersInfoReligion;
use App\Models\State;
//use App\Models\Country;
use App\Models\Community;
use App\Models\Education;
use App\Models\UsersPreference;
use App\Models\Profession;
use App\Models\UsersImage;
use App\Models\UsersInformation;
use App\Models\VendorsInformation;
use App\Models\VendorFaq;
use App\Models\VendorFaqAnswer;
use App\Models\VendorsImage;
use App\Models\VendorVideo;
use App\Models\VendorShowcase;
use App\Models\VendorCustomFaq;
use App\Traits\CaptureIpTrait;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralEmail;
use Validator;
use App\Events\FileUpload;

use Illuminate\Support\Facades\Storage;

class VendorsManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, UserInformationOptionsRepository $userInfoRepo)
    {

        $pageNo=1;
        $searchTerm='';
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {

            $users = User::select('*')->hasVendorRole()->onlySite();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->latest()->paginate(config('usersmanagement.paginateListSize'));

            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
        } else {
            /*** @old code
            $users = User::where('banned', 0)->where('deactivated', 0)->latest()->get();
             */
            $users = User::hasVendorRole()->onlySite();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->all();
        }

        $roles = Role::all();

        # user counts based on cu

        if($pageNo>1)
            $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;

        return View('vendorsmanagement.show-vendors', compact('users', 'roles'))->with(['pageNo'=>$pageNo, 'searchTerm'=>$searchTerm]);
        //return View('vendorsmanagement.show-vendors', compact('users'))->with(['pageNo'=>$pageNo]);
    }

    public function getBannedUsers(Request $request){
        $pageNo=1;
        $searchTerm='';
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $users = User::where('banned', 1)->hasVendorRole()->onlySite();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->latest()->paginate(config('usersmanagement.paginateListSize'));

            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
        } else {
            $users = User::where('banned', 1)->hasVendorRole()->onlySite();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->latest()->get();
        }
        $roles = Role::all();
        if($pageNo>1)
            $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;

//return View('usersmanagement.show-banned-users', compact('users', 'roles'))->with(['pageNo'=>$pageNo]);
        return View('vendorsmanagement.show-vendors', compact('users', 'roles'))->with(['pageNo'=>$pageNo, 'searchTerm'=>$searchTerm]);
    }

    public function getDeactivatedUsers(Request $request){
        $pageNo=1;
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if($pagintaionEnabled) {
            $users = User::where('deactivated', 1)->hasVendorRole()->onlySite()->latest()->paginate(config('usersmanagement.paginateListSize'));
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
        } else {
            $users = User::where('deactivated', 1)->hasVendorRole()->onlySite()->latest()->get();
        }
        $roles = Role::all();
        if($pageNo>1)
            $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;

        return View('vendorsmanagement.show-deactivated-vendors', compact('users', 'roles'))->with(['pageNo'=>$pageNo]);
    }

    public function getActiveUsers(Request $request){
        $pageNo=1;
        $searchTerm='';
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $users = User::where('access_grant', 'Y')->hasVendorRole()->onlySite();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->where('banned', 0)->where('deactivated', 0)->latest()->paginate(config('usersmanagement.paginateListSize'));

            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
        } else {
            $users = User::where('access_grant', 'Y')->hasVendorRole()->onlySite();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->where('banned', 0)->where('deactivated', 0)->latest()->get();

        }
        $roles = Role::all();
        if($pageNo>1)
            $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;

        return View('vendorsmanagement.show-vendors', compact('users', 'roles'))->with(['pageNo'=>$pageNo, 'searchTerm'=>$searchTerm]);
    }

    /***
    @ Inactive users list
    @ Param : none
     **/
    public function getInactiveUsers(Request $request){
        $pageNo=1;
        $searchTerm='';
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if($pagintaionEnabled) {
            $users = User::where('access_grant', 'N')->hasVendorRole()->onlySite();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->where('banned', 0)->where('deactivated', 0)->latest()->paginate(config('usersmanagement.paginateListSize'));

            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
        } else {
            $users = User::where('access_grant', 'N')->hasVendorRole()->onlySite();
            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }
            $users=$users->where('banned', 0)->where('deactivated', 0)->latest()->get();
        }
        $roles = Role::all();
        if($pageNo>1)
            $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;

        return View('vendorsmanagement.show-vendors', compact('users', 'roles'))->with(['pageNo'=>$pageNo, 'searchTerm'=>$searchTerm]);
    }


    /***
    @ Inactive users list
    @ Param : none
     **/
    public function getIncompleteUsers(Request $request){

        $searchTerm='';
        $pageNo=1;
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if($pagintaionEnabled) {
            $users = User::where('access_grant', 'N')->hasVendorRole()->isIncompletedProfile()->onlySite();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
            $users=$users->latest()->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $users = User::where('access_grant', 'N')->hasVendorRole()->isIncompletedProfile()->onlySite();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->latest()->get();
        }


        $roles = Role::all();
        if($pageNo>1)
            $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;

        return View('vendorsmanagement.show-vendors', compact('users', 'roles'))->with(['pageNo'=>$pageNo, 'searchTerm'=>$searchTerm]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        $data = [
            'roles' => $roles,
        ];

        return view('usersmanagement.create-user')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name'                  => 'required|max:255|unique:users',
                'first_name'            => '',
                'last_name'             => '',
                'email'                 => 'required|email|max:255|unique:users',
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
                'role'                  => 'required',
            ],
            [
                'name.unique'         => trans('auth.userNameTaken'),
                'name.required'       => trans('auth.userNameRequired'),
                'first_name.required' => trans('auth.fNameRequired'),
                'last_name.required'  => trans('auth.lNameRequired'),
                'email.required'      => trans('auth.emailRequired'),
                'email.email'         => trans('auth.emailInvalid'),
                'password.required'   => trans('auth.passwordRequired'),
                'password.min'        => trans('auth.PasswordMin'),
                'password.max'        => trans('auth.PasswordMax'),
                'role.required'       => trans('auth.roleRequired'),
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $ipAddress = new CaptureIpTrait();
        $profile = new Profile();

        $user = User::create([
            'name'             => $request->input('name'),
            'first_name'       => $request->input('first_name'),
            'last_name'        => $request->input('last_name'),
            'email'            => $request->input('email'),
            'password'         => bcrypt($request->input('password')),
            'token'            => str_random(64),
            'admin_ip_address' => $ipAddress->getClientIp(),
            'activated'        => 1,
            'site_id'          => (int) config('constants.site_id')
        ]);

        $user->profile()->save($profile);
        $user->attachRole($request->input('role'));
        $user->save();

        return redirect(config('app.url').'/vendors')->with('success', trans('usersmanagement.createSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('usersmanagement.show-user')->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param LocationRepository $locationRepo
     * @param UserInformationOptionsRepository $userInfoRepo
     * @return \Illuminate\Http\Response
     */
    public function edit($id, LocationRepository $locationRepo, VendorCategoryRepository $vendorCatRepo, SatelliteRepository $satelliteRepo)
    {
        // User
        $user = User::findOrFail($id);
        
        // images
        $userimages = array(
            "profile" => VendorsImage::where(array(
                "image_type" => "profile",
                "vendor_id" => $id
            ))->get(),

            "cover" => VendorsImage::where(array(
                "image_type" => "cover",
                "vendor_id" => $id
            ))->get(),
        );

        // vendor information
        $info = VendorsInformation::where('vendor_id', $id)->first();
        // faqs
        $faqs = VendorFaq::where('categories', 'like', '%;s:'.strlen($info->vendor_category).':"'.$info->vendor_category.'";%')
                    ->where('status', 1)
                    ->get();

        foreach($faqs as $faq) {
            try {
                $faqAnswerObj = VendorFaqAnswer::where('vendor_id', $user->id)
                            ->where('faq_id', $faq->id)
                            ->first(['answer']);
                $faq->answer = $faqAnswerObj->answer;
            } catch (\Exception $e) {
                $faq->answer = '';
            }
        }
        
        // custom faq
        $custom_faqs = VendorCustomFaq::where('vendor_id', $id)
                    ->where('category_id', $info->vendor_category)
                    ->get();
        
        // vendor videos
        $videos = VendorVideo::where('vendor_id', $id)->get();
        // showcase
        $showcase = VendorShowcase::where('vendor_id', $id)->first();
        if($showcase != null) {
            if($showcase->additional_categories == '') {
                $additional_categories = [];
            } else {
                $additional_categories = unserialize($showcase->additional_categories);
            }
            if($showcase->satellites == '') {
                $associated_satellites = [];
            } else {
                $associated_satellites = unserialize($showcase->satellites);
            }
            $categories_approved = $showcase->categories_approved;
            $cat_disapprove_reason = $showcase->disapprove_reason;
        } else {
            $additional_categories = [];
            $associated_satellites = [];
            $categories_approved = '';
            $cat_disapprove_reason = '';
        }
        $vendor_categories = $vendorCatRepo->getStatusRecords(1)->get(['id', 'name', 'slug']);
        $satellites = $satelliteRepo->getStatusRecords(1)->get(['id', 'title', 'url']);
        // get membership status
        $getMembershipStatus = $user->currentMembershipStatus();

        $status = $getMembershipStatus['status'];
        $public_photo_allowed = $getMembershipStatus['public_photo_allowed'];
        $data = [
            'user'                      => $user,
            'country_code'              => $user->country->code,
            'userimages'                => $userimages,
            'countryList'               => $locationRepo->getCountries(),
            'stateList'	                => State::all(),
            'vendorInfo'                => $info,
            'faqs'                      => $faqs,
            'videos'                    => $videos,
            'vendor_categories'         => $vendor_categories,
            'additional_categories'     => $additional_categories,
            'associated_satellites'     => $associated_satellites,
            'satellites'                => $satellites,
            'categories_approved'       => $categories_approved,
            'cat_disapprove_reason'     => $cat_disapprove_reason,
            'custom_faqs'               => $custom_faqs,
            'public_photo_allowed'      => $public_photo_allowed,
            'status'                    => $status
        ];

        return view('vendorsmanagement.edit-vendor')->with($data);
    }

    public function imageCoverUpload(Request $request)
    {
        $status = event(new FileUpload('vendorcoveradmin', $request));
        return response()->json($status);
    }

    public function imageProfileUpload(Request $request)
    {
        $status = event(new FileUpload('vendorprofileadmin', $request));
        return response()->json($status);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *`
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user();
        $user = User::find($id);
        $emailCheck = ($request->input('email') != '') && ($request->input('email') != $user->email);
        $ipAddress = new CaptureIpTrait();

        if ($emailCheck) {
            $validator = Validator::make($request->all(), [
                'email'    => 'email|max:255|unique:users',
                'password' => 'present|confirmed|min:6',
            ]);
        } else {
            $validator = Validator::make($request->all(), [

                'password' => 'nullable|confirmed|min:6',
            ]);
            // 'name'     => 'required|max:255|unique:users',
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        // $user->name = $request->input('name');
        //  $user->first_name = $request->input('first_name');
        //   $user->last_name = $request->input('last_name');

        if ($emailCheck) {
            $user->email = $request->input('email');
        }

        if ($request->input('password') != null) {
            $user->password = bcrypt($request->input('password'));
        }

        $userRole = $request->input('role');
        if ($userRole != null) {
            $user->detachAllRoles();
            $user->attachRole($userRole);
        }

        $user->updated_ip_address = $ipAddress->getClientIp();

        switch ($userRole) {
            case 3:
                $user->activated = 0;
                break;

            default:
                $user->activated = 1;
                break;
        }
        $data = $request->input();
        $user->gender = $data['gender'];
        $user->date_of_birth = date_create($data['year']."-".$data['month']."-".$data['day']);
        $user->country_id = $data['country'];
        $user->city_id = $data['city'];
        $user->save();

        if(!$user->isAdmin()){
            $userInformation = $user->get_user_information();
            if($userInformation){
                $userInformation->drink = $data['drink'];
                $userInformation->smoke = $data['smoke'];
                $userInformation->food = $data['food'];
                $userInformation->education_id = $data['education'];
                $userInformation->profession_id = $data['profession'];
                $userInformation->about = $data['about'];
                $userInformation->country_code = $data['phone_country_code'];
                $userInformation->mobile_number = $data['phone_number'];
                $userInformation->ethnicity = $data['ethnicity'];
                $userInformation->save();
            }
        }
        return back()->with('success', trans('usersmanagement.updateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser = Auth::user();
        $user = User::findOrFail($id);
        $ipAddress = new CaptureIpTrait();

        if ($user->id != $currentUser->id) {
            $user->deleted_ip_address = $ipAddress->getClientIp();
            $user->save();
            $user->delete();

            return redirect(config('app.url').'/vendors')->with('success', trans('usersmanagement.deleteSuccess'));
        }

        return back()->with('error', trans('usersmanagement.deleteSelfError'));
    }

    /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('user_search_box');
        $searchRules = [
            'user_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'user_search_box.required' => 'Search term is required',
            'user_search_box.string'   => 'Search term has invalid characters',
            'user_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];

        $validator = Validator::make($request->all(), $searchRules, $searchMessages);

        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $results = User::where('banned', 0)->where('deactivated', 0)->onlySite()->where(function($query) use ($searchTerm){
            $query->where('id', 'like', '%'.$searchTerm.'%')
                ->orWhere('name', 'like', '%'.$searchTerm.'%')
                ->orWhere('email', 'like', '%'.$searchTerm.'%');
        })->get();

        // Attach roles to results
        foreach ($results as $result) {
//            $roles = [
//                'roles' => $result->roles,
//            ];
//            $result->push($roles);
            if($result->country)
                $result->country_name = $result->country->name;
            else
                $result->country_name = '';
            if($result->city)
                $result->city_name = $result->city->name;
            else
                $result->city_name = '';
            if($result->gender == 'M')
                $result->gender = 'Male';
            else
                $result->gender = 'Female';
            $result->profile_image = $result->getVendorProfilePic();
        }

        return response()->json([
            json_encode($results),
        ], Response::HTTP_OK);
    }

    public function banUser($userId){
        $user = User::find($userId);
        if($user){
            $user->banned = 1;
            $user->save();
        }
        return redirect()->back();
    }

    public function unBanUser($userId){
        $user = User::find($userId);
        if($user){
            $user->banned = 0;
            $user->save();
        }
        return redirect()->back();
    }

//    public function deactivateAccount() {
//        return view('pages.user.deactivate-account');
//    }

    public function deleteAccount() {
        return view('pages.user.delete-account');
    }

    public function deletingAccount() {
        $user = Auth::user();

        if($user) {
            $ipAddress = new CaptureIpTrait();
            $ipAdd=$ipAddress->getClientIp();
            $user->deleted_at = gmdate("Y-m-d h:i:s");
            $user->deleted_ip_address=$ipAdd;
            $user->save();
            /**
             * Send delete account email
             */
            try {
                $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Account deleted',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $user->first_name
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
            } catch (\Exception $e){
                \Log::error("Some error while seding delete account mail for user id:".$user->id);
            }
        }
        if ($user && $user->deleted_at!=null) {
            Auth::logout();
            return redirect()->route('login')->withErrors(['Your account has been deleted successfully. You may contact to admin at info@muslimwedding.com. ']);
        }
    }

    public function disableAccount() {

        return view('pages.user.disable-account');
    }

    public  function deactivatingAccount(Request $request) {
        $user = Auth::user();
        $data = $request->input();
        // write code to set the value of deactivated field(users) to "1".
        if($user) {
//            1. change deactivated status to 1
//             2. add deactivation reason and comment (if any)
//            3. logout instantly
            $user->deactivated = 1;
            $user->deactivate_reason=$data['rdReason'];
            $user->deactivate_comment=$data['description'];
            $user->save();
            /**
             * Send deactivation email
             */
            try {
                $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Account Deactivated',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $user->first_name
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
            } catch (\Exception $e){
                \Log::error("Some error while sending upgrade membership mail for user id:".$user->id);
            }
        }

        if ($user && $user->deactivated == 1) {
            Auth::logout();
            return redirect()->route('login')->withErrors(['Your account has been deactivated successfully. Login again to activate your account. ']);
        }
    }

    public function getFeaturedUsers(Request $request){
        $pageNo=1;
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $users = User::where('featured', 1)->where('banned', 0)->onlySite()->where('deactivated', 0)->latest()->paginate(config('usersmanagement.paginateListSize'));
            if($request->input('page')) {
                $pageNo=$request->input('page');
            }
        } else {
            $users = User::where('featured', 1)->where('banned', 0)->onlySite()->where('deactivated', 0)->latest()->get();
        }
        $roles = Role::all();
        if($pageNo>1)
            $pageNo= (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;

        return View('usersmanagement.show-featured-users', compact('users', 'roles'))->with(['pageNo'=>$pageNo]);
    }

    public function featured($userId){
        $user = User::find($userId);
        if($user){
            $user->featured = 1;
            $user->save();
        }
        return redirect()->back();
    }

    public function removeFeatured($userId){
        $user = User::find($userId);
        if($user){
            $user->featured = 0;
            $user->save();
        }
        return redirect()->back();
    }



    public function deleteImage(Request $request, VendorsImage $usersImage){
        $id = $request->input('id');
        $response = null;
        if($usersImage->getWhereCount(array('id'=>$id)) > 0){
            $userImage = $usersImage->getWhereOne(array('id'=>$id));
            
            $s3imageThumb = config('constants.S3_WEDDING_IMAGES_VIEW').$userImage->image_thumb;
            $s3imageFull = config('constants.S3_WEDDING_IMAGES_ORIGINAL_VIEW').$userImage->image_full;


            if(Storage::disk('s3')->exists($s3imageThumb)) {
                Storage::disk('s3')->delete($s3imageThumb);
            }

            if(Storage::disk('s3')->exists($s3imageFull)) {
                Storage::disk('s3')->delete($s3imageFull);
            }

            $userImage->delete();

            $response = array(
                'status' => 1,
                'message'=> 'Image deleted successfully'
            );
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Invalid Id'
            );
        }
        return response()->json($response);
    }


    public function admin_update_vendor(Request $request)
    {
        $userId = $request->user_id;

        $user = User::find($userId);
        $user_old_access_grant = $user->access_grant;
        $user_old_deactivated = $user->deactivated;
        $user_old_banned = $user->banned;
        
        //save user info.
        $userObj                        = User::where('id', $userId)->first();
        $userObj->name                  = $request->input('first_name').".".$request->input('last_name');
        $userObj->first_name            = $request->input('first_name');
        $userObj->last_name             = $request->input('last_name');
        $userObj->email                 = $request->input('email');
        $userObj->country_id            = $request->input('country_code');
        $userObj->city_id               = $request->input('city');
        $userObj->mobile_country_code   = $request->input('mobile_country_code');
        $userObj->mobile_number         = $request->input('mobile_number');
        $userObj->address               = $request->input('address');
        $userObj->state_id              = $request->input('state');
        $userObj->zip                   = $request->input('zip');
        $userObj->access_grant 	        = $request->input('grant_access');
        $change_password                = $request->has('login_password') ? $request->input('login_password') : "";
        // change password if submitted
        if($change_password != "") {
            if (strlen($change_password) < 6) {
                // The passwords matches
                return back()->withErrors("Password must be minimum 6 characters.");
            } else {
                $userObj->password = bcrypt($change_password);
            }
        }
        $userObj->save();

        $slugyfyCompanyName = slugifyText($request->input('company_name'));
        if(VendorsInformation::where('company_slug', $slugyfyCompanyName)->where('vendor_id', '<>', $userObj->id)->exists()) {
            // 2. update slug by adding -2
            $vnNameCnt = VendorsInformation::where('company_slug', 'like','%'.$slugyfyCompanyName.'%')->where('vendor_id', '<>', $userObj->id)->count();
            $slugyfyCompanyName .= "-".($vnNameCnt+1);
        }

        // save vendor info.
        $vendorInfoObj = VendorsInformation::where('vendor_id', $userObj->id)->first();
        $vendorInfoObj->company_name              = $request->input('company_name');
        $vendorInfoObj->company_slug              = $slugyfyCompanyName;
        $vendorInfoObj->established_year          = $request->input('established_year');
        $vendorInfoObj->vendor_category           = $request->input('vendor_category');
        $meta_keywords_val = str_replace("\r", "", $request->input('meta_keywords'));
        $meta_keywords_val = str_replace("\n", "", $meta_keywords_val);
        $vendorInfoObj->meta_keywords             = $meta_keywords_val;
        $vendorInfoObj->business_information      = $request->input('business_information');
        $vendorInfoObj->specials                  = $request->input('specials');
        $vendorInfoObj->display_address           = $request->has('display_address') ? $request->input('display_address') : 0;
        $vendorInfoObj->display_business_number   = $request->has('display_business_number') ? $request->input('display_business_number') : 0;
        $vendorInfoObj->business_website          = $request->input('business_website');
        $vendorInfoObj->facebook                  = $request->input('facebook');
        $vendorInfoObj->instagram                 = $request->input('instagram');
        $vendorInfoObj->pinterest                 = $request->input('pinterest');
        $vendorInfoObj->twitter                   = $request->input('twitter');
        $vendorInfoObj->linkedin                  = $request->input('linkedin');
        $vendorInfoObj->save();
        // save showcase
        if( VendorShowcase::where('vendor_id', $userId)->exists() ) {
            $update_vendor_showcase = VendorShowcase::where('vendor_id', $userId)->first();
            $update_vendor_showcase->satellites = $request->has('satellites') ? serialize($request->input('satellites')) : '';
            $update_vendor_showcase->additional_categories = $request->has('categories') ? serialize($request->input('categories')) : '';
            $update_vendor_showcase->save();
        } else {
            $add_vendor_showcase = new VendorShowcase;
            $add_vendor_showcase->vendor_id = $userId;
            $add_vendor_showcase->satellites = $request->has('satellites') ? serialize($request->input('satellites')) : '';
            $add_vendor_showcase->additional_categories = $request->has('categories') ? serialize($request->input('categories')) : '';
            $add_vendor_showcase->save();
        }

        // update admin faqs answers
        $faqs = VendorFaq::where('categories', 'like', '%;s:'.strlen($vendorInfoObj->vendor_category).':"'.$vendorInfoObj->vendor_category.'";%')
                    ->where('status', 1)
                    ->get();
        foreach($faqs as $faq) {
            $field_submitted = $request->has('field-'.$faq->id) && $request->input('field-'.$faq->id) != null && $request->input('field-'.$faq->id) != "" ? $request->input('field-'.$faq->id) : "";
            if($field_submitted != "") {
                // check submitted is array or not
                if(is_array($field_submitted)) { // check box and conver it to comma separated
                    $field_submitted = implode(",", $field_submitted);
                }
                // save record
                // check if save entry exists
                if(
                    VendorFaqAnswer::where('vendor_id', $userId)
                                ->where('faq_id', $faq->id)
                                ->exists()
                ) {
                    $faqs_asnwer_obj = VendorFaqAnswer::where('vendor_id', $userId)
                                                    ->where('faq_id', $faq->id)
                                                    ->first();
                    $faqs_asnwer_obj->answer = $field_submitted;
                    $faqs_asnwer_obj->save();
                } else {
                    $faqs_asnwer_obj = new VendorFaqAnswer();
                    $faqs_asnwer_obj->vendor_id = $userId;
                    $faqs_asnwer_obj->faq_id = $faq->id;
                    $faqs_asnwer_obj->answer = $field_submitted;
                    $faqs_asnwer_obj->save();
                }
            }
        }

        // update custom faqs
        if($request->has('custom_question') && $request->has('custom_answer')) {
            $update_question_arry = $request->input('custom_question');
            $update_answer_arry = $request->input('custom_answer');
            foreach($update_question_arry as $update_faq_id => $update_faq) {
                $ucfaq = VendorCustomFaq::where('id', $update_faq_id)->first();
                if(!empty($ucfaq)) {
                    $ucfaq->question = $update_faq;
                    $ucfaq->answer = $update_answer_arry[$update_faq_id];
                    $ucfaq->save();
                }
            }
        }

        /**
         * Send email upon profile approval
         */
        if($user_old_access_grant == "N" || $user_old_deactivated == 1 || $user_old_banned == 1) {
            if($userObj->access_grant == "Y" && $userObj->deactivated == 0 && $userObj->banned == 0) {
                try {
                    $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
                    if($user->hasRole('vendor')) {
                        $mail_name = 'Vendor Profile Activated';
                    } else {
                        $mail_name = 'User Profile Activated';
                    }
                    $mail_req_arry = [
                        'mail_name' => $mail_name,
                        'mail_replace_vars' => [
                            '[%USER_FIRST_NAME%]' => $user->first_name,
                            '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                            '[%WEB_LOGIN_URL%]' => route('login')
                        ],
                        'unsubscribe_link' => $unsubscribe_link
                    ];
                    Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
                } catch (\Exception $e){
                    \Log::error("Some error while sending profile approval mail for user id:".$user->id);
                }
            }
        }
        ## userinformation table update

        
        return back()->with('success', trans("Vendor updated successfuly!"));
    }

    /** Update profile frontend **/
    public function updateUserFrontend(Request $request)
    {
         $profileValidationRules = [
        //'name'                  => 'required|max:50|unique:users',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        //'email'                 => 'required|max:255|unique:users|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
        'day'                   => 'required',
        'month'                 => 'required',
        'year'                  => 'required',
        'mobile_number'         => 'required|numeric|phone',
        'height'   				=> 'required',
        'phone_country_code'    => 'required',
        //'religion'   			=> 'required',
        'sub_cast'   			=> 'required',
        'sect'   				=> 'required',
        'country_code'          => 'required',
        'state_id'      	    => 'required',
        'city_id'    		    => 'required',
        'zipcode'   			=> 'required | min:5',
        'education'             => 'required',
        'food'                  => 'required',
        'drink'                 => 'required',
        'smoke'                 => 'required',
        'about'                 => 'required|min:50',
        'looking_for'           => 'required|min:50',
        'marital_status'   		=> 'required',
        'profession_id'   			=> 'required',
        'weight'				=> 'required',
        'weight_measure'		=> 'required'
    ];
        $validator = Validator::make($request->all(), $profileValidationRules );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

//        $user_id=Auth::user()->id;

        $user = User::find(Auth::user()->id);
        $data = $request->input();

        //$userProfile->users_information['sub_cast_id'] == "45"
        //sect_id=8

        // check field updated then send record for profile flag
        if($user->first_name != $data['first_name']) {
            sendLatestUpdateAdmin('first_name');
        }
        if($user->last_name != $data['last_name']) {
            sendLatestUpdateAdmin('last_name');
        }

        $user->name = $data['name'];
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        //$user->gender = $data['gender'];
        $user->date_of_birth = date_create($data['year']."-".$data['month']."-".$data['day']);
        $user->city_id = $data['city_id'];
        $user->state_id = $data['state_id'];
        $user->country_id = $data['country_code'];

        $user->save();

        if($user->save()){
            OthersInfoReligion::where('user_id',$user->id)->delete();

            if(isset($data['other_sect'])){
                OthersInfoReligion::create([
                    'name'=>$data['other_sect'],
                    'others_info_source'=>'community_or_sect',
                    'user_id'=>$user->id
                ]);
                $isReligionFlagged[] = "other_sect";
            }
            if(isset($data['other_sub_cast'])){
                OthersInfoReligion::create([
                    'name'=>$data['other_sub_cast'],
                    'others_info_source'=>'sub_cast',
                    'user_id'=>$user->id
                ]);
                $isReligionFlagged[] = "other_sub_cast";
            }
            if(empty($isReligionFlagged)){
                $putReligionFlag = "";
            }else{
                $putReligionFlag = json_encode($isReligionFlagged);
            }



            $userInformation = $user->get_user_information();

            /**
             * Send update profile data latest update to admin
             */
            if($userInformation->mobile_number != $data['mobile_number']) {
                sendLatestUpdateAdmin('mobile_number');
            }
            if($userInformation->about != $data['about']) {
                sendLatestUpdateAdmin('about');
            }
            if($userInformation->looking_for != $data['looking_for']) {
                sendLatestUpdateAdmin('looking_for');
            }

            if($userInformation){
                $userInformation->drink 				= @$data['drink'];
                $userInformation->smoke 				= @$data['smoke'];
                $userInformation->food 					= @$data['food'];
                $userInformation->education_id 			= @$data['education'];
                $userInformation->profession_id 		= @$data['profession_id'];
                $userInformation->about 				= @$data['about'];
                $userInformation->looking_for 			= @$data['looking_for'];
                $userInformation->marital_status 		= @$data['marital_status'];
                $userInformation->height 				= @$data['height'];
                $userInformation->country_code 			= @$data['phone_country_code'];
                $userInformation->mobile_number 		= @$data['mobile_number'];
                $userInformation->does_pray 			= @$data['does_pray'];
                $userInformation->is_born 				= @$data['is_born'];
                $userInformation->complexion 			= @$data['complexion'];
                $userInformation->eye_color 			= @$data['eye_color'];
                $userInformation->body_type 			= @$data['body_type'];
                $userInformation->weight 				= @$data['weight'];
                $userInformation->weight_measure 		= @$data['weight_measure'];
                $userInformation->horoscope 			= @$data['horoscope'];
                $userInformation->language_id 			= @$data['language_id'];
                $userInformation->currency 				= @$data['currency'];
                $userInformation->annual_income 		= @$data['annual_income'];
                $userInformation->zipcode 				= @$data['zipcode'];
                $userInformation->profile_created_by	= @$data['profile_created_by'];
                $userInformation->native_place 			= @$data['native_place'];
                $userInformation->sect_id 				= @$data['sect'];
                $userInformation->sub_cast_id 			= @$data['sub_cast'];
                $userInformation->isReligionFlagged 	= $putReligionFlag;
                $userInformation->father_occupation_id 	= @$data['father_occupation_id'];
                $userInformation->mother_occupation_id 	= @$data['mother_occupation_id'];
                $userInformation->number_of_sister 		= @$data['number_of_sister'];
                $userInformation->number_of_brother 	= @$data['number_of_brother'];
                $userInformation->family_location 		= @$data['family_location'];
                $userInformation->family_type 			= @$data['family_type'];
                $userInformation->save();
            }
        }
        return back()->with('success', trans('usersmanagement.updateSuccess'));
    }


    /** Update hobbies frontend **/
    public function updateHobbiesFrontend(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $data = $request->input();
        $userInformation = $user->get_user_information();
        if($userInformation){
            $userInformation->hobbies 			= @implode(",",@$data['hobbies']);
            $userInformation->interests 		= @implode(",",@$data['interests']);
            $userInformation->favourite_music 	= @implode(",",@$data['favourite_music']);
            $userInformation->favourite_reads 	= @implode(",",@$data['favourite_reads']);
            $userInformation->preferred_movies 	= @implode(",",@$data['preferred_movies']);
            $userInformation->sports 			= @implode(",",@$data['sports']);
            $userInformation->spoken_languages 	= @implode(",",@$data['language_spoken']);
            $userInformation->save();
        }

        return back()->with('success', trans('usersmanagement.updateSuccess'));
    }
    #@end

    /** Update PartnerPrference frontend **/
    public function updatePartnerPreferenceFrontend($id = false, Request $request)
    {


        if($id == ""){
            $user = User::find(Auth::user()->id);
        }else{
            $user = User::find($id);
        }
        $data = $request->input();

        $userPref = $user->users_preferences();

        if($userPref){
            $userPref->age_from 		= @$data['age_from'];
            $userPref->age_to 			= @$data['age_to'];
            $userPref->height_from 		= @$data['height_from'];
            $userPref->height_to 		= @$data['height_to'];
            $userPref->drink 			= @implode(",", @$data['drink']);
            $userPref->diet 			= @implode(",", @$data['food']);
            $userPref->smoke 			= @implode(",", @$data['smoke']);
            $userPref->religion 		= @$data['religion'];
            $userPref->education	 	= @implode(",", @$data['education_id']);
            $userPref->marital_status	= @implode(",",$data['marital_status']);
            $userPref->community 		= @implode(",", @$data['sect']);
            $userPref->languages 		= @implode(",", @$data['language']);
            $userPref->country_living	= @implode(",", @$data['country_code']);
            $userPref->city_district 	= @implode(",", @$data['city_id']);
            $userPref->state_living 	= @implode(",", @$data['state_id']);
            $userPref->professional_area= @implode(",", @$data['profession_id']);
            $userPref->currency 		= @$data['currency_code'];
            $userPref->annual_income	= @$data['annual_income'];
            $userPref->body_type 		= @implode(",", @$data['body_type']);
            $userPref->eye_color	 	= @implode(",",@$data['eye_color']);
            $userPref->skin_tone 		= @implode(",",@$data['complexion']);

            $userPref->save();

        }else{

            UsersPreference::Create([
                "user_id" 			=> Auth::user()->id,
                "age_from" 			=> @$data['age_from'],
                "age_to" 			=> @$data['age_to'],
                "height_from" 		=> @$data['height_from'],
                "height_to" 		=> @$data['height_to'],
                "drink" 			=> @implode(",", @$data['drink']),
                "diet" 				=> @implode(",", @$data['food']),
                "smoke" 			=> @implode(",", @$data['smoke']),
                "body_type" 		=> @implode(",", @$data['body_type']),
                "religion" 			=> @$data['religion'],
                "education" 		=> @implode(",", @$data['education_id']),
                "marital_status"	=> @implode(",",$data['marital_status']),
                "professional_area"	=> @implode(",", @$data['profession_id']),
                "community" 		=> @implode(",", @$data['sect']),
                "languages" 		=> @implode(",", @$data['language']),
                "country_living"	=> @implode(",", @$data['country_code']),
                "city_district" 	=> @implode(",", @$data['city_id']),
                "state_living" 		=> @implode(",", @$data['state_id']),
                "currency" 			=> @$data['currency_code'],
                "annual_income" 	=> @$data['annual_income'],
                "eye_color" 		=> @implode(",",@$data['eye_color']),
                "skin_tone" 		=> @implode(",",@$data['complexion'])
            ]);
        }

        return back()->with('success', trans('usersmanagement.updateSuccess'));
    }

    public function updatePartnerPreferenceBackend($user_id,$data)
    {
        $user = User::find($user_id);


        $userPref = $user->users_preferences();

        if($userPref){
            $userPref->age_from 		= @$data['admn_age_from'];
            $userPref->age_to 			= @$data['admn_age_to'];
            $userPref->height_from 		= @$data['admn_height_from'];
            $userPref->height_from 		= @$data['admn_height_to'];
            $userPref->drink 			= @implode(",", @$data['admn_drink']);
            $userPref->diet 			= @implode(",", @$data['admn_food']);
            $userPref->smoke 			= @implode(",", @$data['admn_smoke']);
            $userPref->religion 		= @$data['religion'];
            $userPref->education	 	= @implode(",", @$data['admn_education_id']);
            $userPref->marital_status	= @implode(",",@$data['admn_marital_status']);
            $userPref->community 		= @implode(",", @$data['admn_sect']);
            $userPref->languages 		= @implode(",", @$data['admn_language']);
            $userPref->country_living	= @implode(",", @$data['country_code_pref']);
            $userPref->city_district 	= @implode(",", @$data['city_id_pref']);
            $userPref->state_living 	= @implode(",", @$data['state_id_pref']);
            $userPref->professional_area= @implode(",", @$data['admn_profession_id']);
            $userPref->currency 		= @$data['admn_currency_code'];
            $userPref->body_type        = @implode(",", @$data['admn_body_type']);
            $userPref->annual_income	= @$data['admn_annual_income'];
            $userPref->eye_color	 	= @implode(",",@$data['admn_eye_color']);
            $userPref->skin_tone 		= @implode(",",@$data['admn_complexion']);
            $userPref->save();
        }else{
            UsersPreference::Create([
                "user_id" 			=> $user_id,
                "age_from" 			=> @$data['admn_age_from'],
                "age_to" 			=> @$data['admn_age_to'],
                "height_from" 		=> @$data['admn_height_from'],
                "height_from" 		=> @$data['admn_height_to'],
                "drink" 			=> @implode(",", @$data['admn_drink']),
                "diet" 				=> @implode(",", @$data['admn_food']),
                "smoke" 			=> @implode(",", @$data['admn_smoke']),
                "religion"   		=> @$data['religion'],
                "education" 		=> @implode(",", @$data['admn_education_id']),

                "professional_area"	=> @implode(",", @$data['admn_profession_id']),
                "marital_status"	=> @implode(",", @$data['admn_marital_status']),
                "body_type"			=> @implode(",", @$data['admn_body_type']),
                "community" 		=> @implode(",", @$data['admn_sect']),
                "languages" 		=> @implode(",", @$data['admn_language']),
                "country_living"	=> @implode(",", @$data['country_code_pref']),
                "city_district" 	=> @implode(",", @$data['city_id_pref']),
                "state_living" 		=> @implode(",", @$data['state_id_pref']),
                "currency" 			=> @$data['admn_currency_code'],
                "annual_income" 	=> @$data['admn_annual_income'],
                "eye_color" 		=> @implode(",",@$data['admn_eye_color']),
                "skin_tone" 		=> @implode(",",@$data['admn_complexion'])
            ]);
        }

        return back()->with('success', trans('usersmanagement.updateSuccess'));
    }
    #@end


    /*** Restore access revoked user **/

    public function restoreInactiveUser(Request $request)
    {
        $user_id = $request->user_id;
        $user = User::find($user_id);
        $user->access_grant = 'Y';
        $user->save();
        return redirect(config('app.url').'/inactive-users')->with('success', trans('usersmanagement.successRestore'));
    }

    /**
     * Show latest updates on admin side
     */
    public function showLatestUpdates(Request $request)
    {
        $searchTerm='';
        $pageNo = 1;
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $users = User::getPendingReviewVendors()->hasVendorRole();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->orderBy('updated_at', 'desc')->paginate(config('usersmanagement.paginateListSize'));

            if($request->input('page')) {
                $pageNo = $request->input('page');
            }
                        
        } else {
            $users = User::getPendingReviewVendors()->hasVendorRole();

            if($request->input('user_search_box')) {
                $searchTerm=$request->input('user_search_box');
                $users=$users->adminUserSearch($searchTerm);
            }

            $users=$users->orderBy('updated_at', 'desc')->get();

        }
        
        $roles = Role::all();
        if($pageNo>1)
            $pageNo = (($pageNo-1)*config('usersmanagement.paginateListSize')) +1;
        
        return view('vendorsmanagement.show-vendors', compact('users', 'roles'))->with(['pageNo' => $pageNo, 'searchTerm'=>$searchTerm]);
    }

    public function handleVendorVideoSubmit(Request $request)
    {   
        $response = null;
        $videoObj = new VendorVideo;
        $videoObj->vendor_id = $request->input('user_id');
        $videoObj->video_title = $request->input('video_title');
        $embed_code = $request->input('embed_code');
        // $embed_code = preg_replace('/height="(.*?)"/i', 'style="width:100%;height:275px;"', $embed_code);
        // $embed_code = preg_replace('/width="(.*?)"/i', '', $embed_code);
        $videoObj->embed_code = $embed_code;
        $videoObj->description = strip_tags($request->input('description'));
        $videoObj->is_approved = 'Y';
        if($videoObj->save()) {
            $response = [
                'status' => 1,
                'video_id' => $videoObj->id,
                'embeded_code' => $videoObj->embed_code,
                'message' => 'Your video has been added successfully.'
            ];
            return $response;
        }
        exit();
    }

    public function handleVendorVideoDelete($id)
    {
        VendorVideo::where('id', $id)->delete();
        return [
            'status' => 1,
            'message' => 'Your video has been deleted successfully.'
        ];
        exit();
    }

    /**
     * Temp method to update the vendor company slugs
     */
    public function updateAllVendorsComapnySlugs()
    {
        // get all vendors information objects
        $vendorRecords = VendorsInformation::all();
        foreach($vendorRecords as $vendorInfo) {
            // update the company slug value
            // 1. check slug exists by slugyfy
            $slugyfyCompanyName = slugifyText($vendorInfo->company_name);
            if(VendorsInformation::where('company_slug', $slugyfyCompanyName)->exists()) {
                // 2. update slug by adding -2
                $vnNameCnt = VendorsInformation::where('company_slug', 'like','%'.$slugyfyCompanyName.'%')->count();
                $slugyfyCompanyName .= "-".($vnNameCnt+1);
            }
            // 3. Update the company slug value
            VendorsInformation::where('vendor_id', $vendorInfo->vendor_id)->update(['company_slug' => $slugyfyCompanyName]);
        }

        echo "done"; exit;
    }
}