<?php

namespace App\Http\Controllers;

//use App\Mail\Message;
use App\Models\MessageFilter;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Message;
use App\Logic\MessageRepository;
use Illuminate\Support\Facades\Route;
use JsValidator;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralEmail;

class MessageController extends Controller
{
    protected $messageRepository;
    protected $validationRules =[
        'message'=>'required'
    ];

    public function __construct(MessageRepository $messageRepository)   {
        $this->messageRepository=$messageRepository;
    }

    /******************* Inbox Starts here ******************/
    public function sendMessage(Request $request) {
        $user = Auth::user();
        $data = $request->input();
        $sender_archive=($data['pageType']=='inbox')?'N':'Y';

//        Message::create([
//          'sender_id'       => $user->id,
//          'receiver_id'     => $data['receiver_id'],
//          'sender_archive'  => $sender_archive,
//          'message'         => $data['message'],
//            'created_at'    => Carbon::now()
//        ]);

        $ResArr=$this->messageRepository->checkIfAlreadySentLimitMsg($data['receiver_id']);

        $this->messageRepository->sendMsgApplyFilter($data['receiver_id']);
        $toUser = User::where('id', $data['receiver_id'])->first();
        try {
            if(!$toUser->isOnline()) {
                if($toUser->hasRole('unverified')) {
                   if($toUser->get_user_information()->notify_sends_message == "Y") {
                    $fromUser = Auth::user();
                    //$message_link = route('conversation', [ 'user_id' => $fromUser->getEncryptedId() ]);
                    $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => 'M' ]);
                    $mail_req_arry = [
                        'mail_name' => 'Message Received - User',
                        'mail_replace_vars' => [
                            '[%USER_FIRST_NAME%]' => $toUser->first_name,
                            '[%USER_SENDER_FIRST_NAME%]' => $fromUser->get_firstname(),
                            '[%WEB_LOGIN_URL%]' => route('login')
                        ],
                        'unsubscribe_link' => $unsubscribe_link
                    ];
                    Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
                   }
                } else if($toUser->hasRole('vendor')) {
                    if($toUser->vendors_information->notify_sends_message == "Y") {
                        $fromUser = Auth::user();
                        $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => 'M' ]);
                        $mail_req_arry = [
                            'mail_name' => 'Message Received - Vendor',
                            'mail_replace_vars' => [
                                '[%USER_FIRST_NAME%]' => $toUser->first_name,
                                '[%USER_SENDER_FIRST_NAME%]' => $fromUser->get_firstname(),
                                '[%WEB_LOGIN_URL%]' => route('login')
                            ],
                            'unsubscribe_link' => $unsubscribe_link
                        ];
                        Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
                    }
                }
            }
        }
        catch (\Exception $e){

        }

        if($data['message'] == '') {
            if($request->has('json')) {
                return response()->json(
                    [
                        'status' => 0,
                        'message' => 'Message cann\'t be empty'
                    ]
                );
            }
        }
        Message::sendMessageByUserId($data['receiver_id'], $data['message'],NULL,'1');
        if($request->has('json')) {
            return response()->json(
                [
                    'status' => 1,
                    'message' => 'Message sent successfully'
                ]
            );
        }
        return redirect()->back()->withSuccess('message sent successfully');
        
    }

    public function sendMessageView(Request $request){
        if($request->has('profileId')) {
            if ($request->filled('profileId')) {
                if (User::withEncryptedId($request->input('profileId'))->count() > 0) {
                    $toUser = User::withEncryptedId($request->input('profileId'))->first();
                    $ResArr=$this->messageRepository->checkIfAlreadySentLimitMsg($toUser->id);

                    //if($ResArr['canSendFlag']=='1') {

                        if ($request->filled('message')) {
                            $message = $request->input('message');
                            $this->messageRepository->sendMsgApplyFilter($toUser->id);
                            Message::sendMessageByUserId($toUser->id, $message,NULL,'1');
                            // send email if user is not online
                            try {
                                if(!$toUser->isOnline()) {
                                    if($toUser->hasRole('unverified')) {
                                       if($toUser->get_user_information()->notify_sends_message == "Y") {
                                        $fromUser = Auth::user();
                                        //$message_link = route('conversation', [ 'user_id' => $fromUser->getEncryptedId() ]);
                                        $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => 'M' ]);
                                        $mail_req_arry = [
                                            'mail_name' => 'Message Received - User',
                                            'mail_replace_vars' => [
                                                '[%USER_FIRST_NAME%]' => $toUser->first_name,
                                                '[%USER_SENDER_FIRST_NAME%]' => $fromUser->get_firstname(),
                                                '[%WEB_LOGIN_URL%]' => route('login')
                                            ],
                                            'unsubscribe_link' => $unsubscribe_link
                                        ];
                                        Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
                                       }
                                    } else if($toUser->hasRole('vendor')) {
                                        if($toUser->vendors_information->notify_sends_message == "Y") {
                                            $fromUser = Auth::user();
                                            $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => 'M' ]);
                                            $mail_req_arry = [
                                                'mail_name' => 'Message Received - Vendor',
                                                'mail_replace_vars' => [
                                                    '[%USER_FIRST_NAME%]' => $toUser->first_name,
                                                    '[%USER_SENDER_FIRST_NAME%]' => $fromUser->get_firstname(),
                                                    '[%WEB_LOGIN_URL%]' => route('login')
                                                ],
                                                'unsubscribe_link' => $unsubscribe_link
                                            ];
                                            Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
                                        }
                                    }
                                }
                            }
                            catch (\Exception $e){

                            }
                            $response = array(
                                'status' => 1,
                                'message' => 'Message sent successfully'
                            );
                        } else {
                            $response = array(
                                'status' => 0,
                                'message' => 'Message is required'
                            );
                        }
                    // } else {
                    //     $response = array(
                    //         'status' => 0,
                    //         'message' => 'You are allowed to send messages to only '.$ResArr['message_allowed'].' new members in a day. Please <a href="'. route('paid-membership') .'" >upgrade your membership</a>  in order to send unlimited messages'
                    //     );
                    // }
                }
                else{
                    $response = array(
                        'status' => 0,
                        'message' => 'User not found'
                    );
                }
            }
            else{
                $response = array(
                    'status' => 0,
                    'message' => 'Profile Id required'
                );
            }
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Profile Id required'
            );
        }
        return response()->json($response);
    }



    public function sendMessageViewApi(Request $request) {
        if($request->has('api_token')) {
            //Auth::guard('api')->user();
            $userId = Auth::guard('api')->id();
            if($userId!=null && $userId>0) {
                if ($request->has('profileId')) {
                    if ($request->filled('profileId')) {
                        if (User::withEncryptedId($request->input('profileId'))->count() > 0) {
                            $toUser = User::withEncryptedId($request->input('profileId'))->first();
                            $ResArr = $this->messageRepository->checkIfAlreadySentLimitMsg($toUser->id);

                            // if ($ResArr['canSendFlag'] == '1') {

                                if ($request->filled('message')) {
                                    $message = $request->input('message');
                                    $this->messageRepository->sendMsgApplyFilter($toUser->id);
                                    Message::sendMessageByUserId($toUser->id, $message, NULL, '1');
                                    // send email if user is not online
                                    try {
                                        if (!$toUser->isOnline()) {
                                            if($toUser->hasRole('unverified')) {
                                                if($toUser->get_user_information()->notify_sends_message == "Y") {
                                                 $fromUser = Auth::user();
                                                 //$message_link = route('conversation', [ 'user_id' => $fromUser->getEncryptedId() ]);
                                                 $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => 'M' ]);
                                                 $mail_req_arry = [
                                                     'mail_name' => 'Message Received - User',
                                                     'mail_replace_vars' => [
                                                         '[%USER_FIRST_NAME%]' => $toUser->first_name,
                                                         '[%USER_SENDER_FIRST_NAME%]' => $fromUser->get_firstname(),
                                                         '[%WEB_LOGIN_URL%]' => route('login')
                                                     ],
                                                     'unsubscribe_link' => $unsubscribe_link
                                                 ];
                                                 Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
                                                }
                                             } else if($toUser->hasRole('vendor')) {
                                                 if($toUser->vendors_information->notify_sends_message == "Y") {
                                                     $fromUser = Auth::user();
                                                     $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => 'M' ]);
                                                     $mail_req_arry = [
                                                         'mail_name' => 'Message Received - Vendor',
                                                         'mail_replace_vars' => [
                                                             '[%USER_FIRST_NAME%]' => $toUser->first_name,
                                                             '[%USER_SENDER_FIRST_NAME%]' => $fromUser->get_firstname(),
                                                             '[%WEB_LOGIN_URL%]' => route('login')
                                                         ],
                                                         'unsubscribe_link' => $unsubscribe_link
                                                     ];
                                                     Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
                                                 }
                                             }
                                        }
                                    } catch (\Exception $e) {

                                    }
                                    $response = array(
                                        'status' => 1,
                                        'message' => 'Message sent successfully'
                                    );
                                } else {
                                    $response = array(
                                        'status' => 0,
                                        'message' => 'Message is required'
                                    );
                                }
                            // } else {
                            //     $response = array(
                            //         'status' => 0,
                            //         'message' => 'You are allowed to send messages to only ' . $ResArr['message_allowed'] . ' new members in a day. Please <a href="' . route('paid-membership') . '" >upgrade your membership</a>  in order to send unlimited messages'
                            //     );
                            // }
                        } else {
                            $response = array(
                                'status' => 0,
                                'message' => 'User not found'
                            );
                        }
                    } else {
                        $response = array(
                            'status' => 0,
                            'message' => 'Profile Id required'
                        );
                    }
                } else {
                    $response = array(
                        'status' => 0,
                        'message' => 'Profile Id required'
                    );
                }
                return response()->json($response);
            } else {
                //return Response::json(array('userId'=>$userId));
                return response::json(array('status'=>'Mismatch API Token.'));
            }
        } else {
            return response::json(array('status'=>'API Token is empty.'));
        }
    }

    public function getInbox(Request $request,$showunread=''){
//		/**
//			@ Check for access_grant
//			@ Allow non restricted messagecontroller@path
//		**/
//		$user = Auth::user();
//		$currentRoute = Route::currentRouteName();
//
//		# nothing allowed in this controller
//		$revokedAccessRoute = [];
//
//		## check user access_grant
//		if($user && ($user->access_grant == "N")){
//			if(!in_array($currentRoute, $revokedAccessRoute)){
//				return redirect()->route('public.home');
//			}
//		}
//		/**
//			@End - Access grant restrictions
//		**/

//        $conversations = Talk::getInbox('desc',0,5);
//        return view('pages.message.inbox')->with(['conversations'=>$conversations]);

        $inbox= $this->messageRepository->getInbox($request,$showunread);

        $conversations=array();

        foreach($inbox['fetchInbox'] as $key=>$value) {
            //echo $value->sender_id;
            $conversations[$key]['msg']=$value;
            $conversations[$key]['last_message']=$this->messageRepository->getLastConversationRec($value->sender_id);
            $conversations[$key]['old_conversation_exists'] = $this->messageRepository->convWholeUnread($value->sender_id);
            $conversations[$key]['user']=User::withTrashed()->find($value->sender_id);
        }

        // determine the user is vendor and is paid or not
        $userObject = Auth::user();
        $validateUserCanRead = false;
        if(User::where('id', $userObject->id)->hasVendorRole()->exists()) {
            // check out the user is paid
            $membershipStatus = $userObject->currentMembershipStatus();
            if($membershipStatus['status'] == "Paid") {
                $validateUserCanRead = true;
            } else {
                $validateUserCanRead = false;
            }
        } else {
            $validateUserCanRead = true;
        }

        return view('pages.message.inbox')->with(['conversations'=>$conversations, 'conversationObj'=>$inbox['fetchInbox'], 'showunread'=>$showunread, 'validateUserCanRead' => $validateUserCanRead]);
    }

    public function getConversation($senderId, $page_type = 'inbox', Request $request){

        $validator = JsValidator::make($this->validationRules);
        $toSender = User::withTrashed()->withEncryptedId($senderId)->first();
        $otherUser=User::withTrashed()->find($toSender->id);
        $authUser=Auth::user();

        // determine the user is vendor and is paid or not
        if(User::where('id', $authUser->id)->hasVendorRole()->exists()) {
            // check out the user is paid
            $membershipStatus = $authUser->currentMembershipStatus();
            if($membershipStatus['status'] != "Paid") {
                if($this->messageRepository->convWholeUnread($toSender->id) == false) {
                    return redirect()->route('inbox');
                }
            }
        }

        $conversations= $this->messageRepository->getMessageConversation($toSender->id);

        /***** code to make the read_status_flag to "READ" ***********/
        Message::where('sender_id', $toSender->id)->where('receiver_id',$authUser->id)->update(['read_status' =>'Y']);
        /***** code to make the read_status_flag to "READ" ***********/

       // $authHasPrivatePhoto=$authUser->hasPrivatePhotos();
       // $authPhotoShareStatus=$authUser->hasSharedPhotos($toSender->id);

        $c = 0;
        $messages = [];
        foreach($conversations as $conversation) {
            $messages[$c]['id'] = $conversation->id;
            $messages[$c]['sender_id'] = $conversation->sender_id;
            $messages[$c]['receiver_id'] = $conversation->receiver_id;
            $messages[$c]['message'] = $conversation->message;
            $messages[$c]['read_status'] = $conversation->read_status;
            $messages[$c]['sender_trash'] = $conversation->sender_trash;
            $messages[$c]['receiver_trash'] = $conversation->receiver_trash;
            $messages[$c]['sender_delete'] = $conversation->sender_delete;
            $messages[$c]['receiver_delete'] = $conversation->receiver_delete;
            $messages[$c]['created_at'] = date('h:i A, d F Y', strtotime($conversation->created_at->setTimezone(Auth::user()->getUserTimeZone())));
            $messages[$c]['updated_at'] = date('h:i A, d F Y', strtotime($conversation->updated_at->setTimezone(Auth::user()->getUserTimeZone())));
            $messages[$c]['is_archive'] = $conversation->is_archive;
            $messages[$c]['sender_archive'] = $conversation->sender_archive;
            $messages[$c]['receiver_archive'] = $conversation->receiver_archive;
            $messages[$c]['msg_type'] = $conversation->msg_type;
            $c++;
        }

       // $page_type = 'inbox';
        if($request->has('json')) {
            return response()->json([
                'conversation' => $messages
            ]);
        }

        $userId = $toSender->id;

        return view('pages.message.vue-conversation')->with(
            [
                'conversation' => $messages,
                'otherUser' => [
                    'id' => $otherUser->id,
                    'getEncryptedId' => $otherUser->getEncryptedId(),
                    'first_name' => $otherUser->get_firstname(),
                    'last_name' => $otherUser->last_name,
                    'getAge' => $otherUser->getAge(),
                    'city' => '', //$otherUser->city->name,
                    'country' => '', //$otherUser->country->name,
                    'banned' => $otherUser->banned,
                    'deactivated' => $otherUser->deactivated,
                    'deleted_at' => $otherUser->deleted_at,
                    'account_show' => $otherUser->account_show,
                    'access_grant' => $otherUser->access_grant,
                    //'privatePhotoPermissionCnt' => $otherUser->privatePhotoPermissionCnt(),
                    'profileLink' => ($otherUser->hasRole('vendor'))?$otherUser->getVendorProfileLink():"#",
                    'profileTarget' => ($otherUser->hasRole('vendor'))?"_blank":"",
                    'getProfilePic' => $otherUser->getVendorProfilePic(),
                    'blockedToFlag' => user_blocked_to_me($otherUser->id),
                    'blockedByFlag' => user_blocked_by_me($otherUser->id),
                    'skippedToFlag' => user_skipped_to_me($otherUser->id),
                    'skippedByFlag' => user_skipped_by_me($otherUser->id),
                    'sendArchiveLnk' => route('send-archive', ['user_id'=>$otherUser->getEncryptedId()]),
                    'sendTrashLnk' => route('send-trash', ['user_id'=>$otherUser->getEncryptedId(), 'pageType' => $page_type]),
                ],
                'authUser' => [
                    'id' => $authUser->id,
                    'getProfilePic' => $authUser->getVendorProfilePic(true),
                    'hasPrivatePhotos' => $authUser->hasPrivatePhotos(),
                    'hasSharedPhotos' => $authUser->hasSharedPhotos($toSender->id),
                    'access_grant' => $authUser->access_grant
                ],
                'receiver_id' => $userId,
                'page_type' => $page_type,
                'page_link' => route($page_type)
            ]
        );
    }

    public function sendArchive($senderId) {
        $toSender = User::withTrashed()->withEncryptedId($senderId)->first();
        // $otherUser=User::find($toSender->id);
        $conversation= $this->messageRepository->sendMessageToArchive($toSender->id);
        return redirect()->route('inbox');
        //return redirect()->back();
    }

    public function sendTrash($senderId, $sendTrash) {
        //$toSender = User::withTrashed()->withEncryptedId($senderId)->first();
        $id = base64_decode($senderId);
        // $conversation= $this->messageRepository->sendMessageToTrash($toSender->id, $sendTrash);
        $conversation= $this->messageRepository->sendMessageToTrash($id, $sendTrash);

        return redirect(route('inbox'));
    }

    /**************** End Inbox here *******************/

    /**************** Sent Starts here **************/
    public function getSent(Request $request,$showunread=''){
        $inbox= $this->messageRepository->getSent($request,$showunread);
        $conversations=array();
        foreach($inbox['fetchInbox'] as $key=>$value) {
            //echo $value->sender_id;
            $conversations[$key]['msg']=$value;
            $conversations[$key]['last_message']=$this->messageRepository->getLastSentConversationRec($value->receiver_id);
            $conversations[$key]['user']=User::withTrashed()->find($value->receiver_id);
        }
        return view('pages.message.sent')->with(['conversations'=>$conversations, 'conversationObj'=>$inbox['fetchInbox']]);
    }

    public function getSentConversation($receiverId){
        $validator = JsValidator::make($this->validationRules);
        $toSender = User::withTrashed()->withEncryptedId($receiverId)->first();
        $otherUser=User::withTrashed()->find($toSender->id);
        $authUser=Auth::user();
        //echo $toSender->id;
        $conversation= $this->messageRepository->getMessageConversation($toSender->id);
        $userId=$toSender->id;
        return view('pages.message.conversation')->with(['conversation'=>$conversation,'otherUser'=>$otherUser,'authUser'=>$authUser, 'validator'=>$validator,'rec_id'=>$userId,'pagetype'=>'sent']);
    }
    /**************** End Sent here **************/
//
//    /**************** Filter Starts here **************/
//    public function getFilter(Request $request){
//
//        $inbox= $this->messageRepository->getFilter($request);
//
//        $conversations=array();
//
//        foreach($inbox['fetchInbox'] as $key=>$value) {
//            //echo $value->sender_id;
//            $conversations[$key]['msg']=$value;
//            $conversations[$key]['last_message']=$this->messageRepository->getLastConversationRec($value->sender_id);
//            $conversations[$key]['user']=User::withTrashed()->find($value->sender_id);
//        }
//
//        $authUser=Auth::user();
//
//        $msgFilter=$authUser->message_filter()->first();
//
//        // initialize the variables
//        $religion = @$msgFilter->religion ? $msgFilter->religion: "";
//        $body_type = @$msgFilter->body_type ? $msgFilter->body_type : "";
//        //$ageRange = $request->has('ageRange') ? $request->ageRange : "";
//        $age_from = @$msgFilter->age_from? $msgFilter->age_from : config('constants.search_defaults.start_age');
//        $age_to = @$msgFilter->age_to ? $msgFilter->age_to : config('constants.search_defaults.end_age');
//        //$heightRange = $request->has('heightRange') ? $request->heightRange : "";
//        $height_from = @$msgFilter->height_from ? $msgFilter->height_from : config('constants.search_defaults.start_height');
//        $height_to = @$msgFilter->height_to ? $msgFilter->height_to : config('constants.search_defaults.end_height');
//        $food = @$msgFilter->diet ? $msgFilter->diet : "";
//        $smoke = @$msgFilter->smoke ? $msgFilter->smoke : "";
//        $drink = @$msgFilter->drink ? $msgFilter->drink : "";
//        $filter_status = (@$msgFilter->status=="1"? "1": "0");
//        $allow_without_image = (@$msgFilter->allow_without_images=="0"?"0":"1");
//
////        if($ageRange != "" || $ageRange != 0) {
////            $ageRange_exp = explode(" - ", $ageRange);
////            $age_from = $ageRange_exp[0];
////            $age_to = $ageRange_exp[1];
////        }
////
////        if($heightRange != "" || $heightRange != 0) {
////            $heightRange_exp = explode(" - ", $heightRange);
////            $height_from = rtrim($heightRange_exp[0], " cm");
////            $height_to = rtrim($heightRange_exp[1], " cm");
////        }
//
//        $search_arry = [
//            //    'order_by' => $order_by,
//            //   'per_page' => $per_page,
//            'religion' => $religion,
//            'body_type' => $body_type,
//            'age_from' => $age_from,
//            'age_to' => $age_to,
//            'height_from' => $height_from,
//            'height_to' => $height_to,
//            'food' => $food,
//            'smoke' => $smoke,
//            'drink' => $drink,
//            'filter_status' => $filter_status,
//            'allow_without_image' => $allow_without_image
//
//        ];
//
//        return view('pages.message.filter')->with(
//            [
//                'conversations'=>$conversations,
//                'conversationObj'=>$inbox['fetchInbox'],
//                'showunread'=>'',
//                'religions'     => config('constants.religions'),
//                'age_from'      => $age_from,
//                'age_to'        => $age_to,
//                'height_from'   => $height_from,
//                'height_to'     => $height_to,
//                'search_arry'   => $search_arry
//            ]
//        );
//    }
//
//
//    public function postFilter(Request $request) {
//        $user = Auth::user();
//        $data = $request->input();
//
//        $searchCriteriaArr=array('user_id'=>$user->id);
//        $updateDataArr=array();
//        $ageRange=explode('-', $data['ageRange']);
//        $heightRange=explode('-', $data['heightRange']);
//
//        $updateDataArr['age_from']=trim($ageRange[0]);
//        $updateDataArr['age_to']=trim($ageRange[1]);
//        $updateDataArr['height_from']=explode(' ', trim($heightRange[0]))[0];
//        $updateDataArr['height_to']=explode(' ', trim($heightRange[1]))[0];
//        $updateDataArr['drink']=$request->has('drink')?implode(',',$request->drink):"";
//        $updateDataArr['smoke']=$request->has('smoke')?implode(',',$request->smoke):"";
//        $updateDataArr['diet']=$request->has('food')?implode(',',$request->food):"";
//        $updateDataArr['religion']=$request->has('religion') ? $request->religion : "";
//        $updateDataArr['body_type']=$request->has('body_type')?implode(',',$request->body_type): "";
//        $updateDataArr['allow_without_images']=$request->has('allow_without_image') ? $request->allow_without_image : "";
//        $updateDataArr['status']=$request->has('status') ? $request->status: "";
//
//        MessageFilter::updateOrCreate(
//            $searchCriteriaArr,
//            $updateDataArr
//        );
//
//        if ($updateDataArr['status']=='1') {
//            $this->filterMessages($updateDataArr);
//        }
//
//        return redirect()->back();
//    }
//
//    public function filterMessages($updateDataArr) {
//        $this->messageRepository->getUniqueSender($updateDataArr);
//    }
//
//    public function getFilterConversation($senderId, Request $request){
//        $validator = JsValidator::make($this->validationRules);
//        $toSender = User::withTrashed()->withEncryptedId($senderId)->first();
//        $otherUser=User::withTrashed()->find($toSender->id);
//        $authUser=Auth::user();
//        //echo $toSender->id;
//        $conversations= $this->messageRepository->getMessageConversation($toSender->id);
//
//        /***** code to make the read_status_flag to "READ" ***********/
//        Message::where('sender_id', $toSender->id)->where('receiver_id',$authUser->id)->update(['read_status' =>'Y']);
//        /***** code to make the read_status_flag to "READ" ***********/
//
//        $authHasPrivatePhoto=$authUser->hasPrivatePhotos();
//        $authPhotoShareStatus=$authUser->hasSharedPhotos($toSender->id);
//
//        $c = 0;
//        $messages = [];
//        foreach($conversations as $conversation) {
//            $messages[$c]['id'] = $conversation->id;
//            $messages[$c]['sender_id'] = $conversation->sender_id;
//            $messages[$c]['receiver_id'] = $conversation->receiver_id;
//            $messages[$c]['message'] = $conversation->message;
//            $messages[$c]['read_status'] = $conversation->read_status;
//            $messages[$c]['sender_trash'] = $conversation->sender_trash;
//            $messages[$c]['receiver_trash'] = $conversation->receiver_trash;
//            $messages[$c]['sender_delete'] = $conversation->sender_delete;
//            $messages[$c]['receiver_delete'] = $conversation->receiver_delete;
//            $messages[$c]['created_at'] = date('h:i A, d F Y', strtotime($conversation->created_at->setTimezone(Auth::user()->getUserTimeZone())));
//            $messages[$c]['updated_at'] = date('h:i A, d F Y', strtotime($conversation->updated_at->setTimezone(Auth::user()->getUserTimeZone())));
//            $messages[$c]['is_archive'] = $conversation->is_archive;
//            $messages[$c]['sender_archive'] = $conversation->sender_archive;
//            $messages[$c]['receiver_archive'] = $conversation->receiver_archive;
//            $messages[$c]['msg_type'] = $conversation->msg_type;
//            $c++;
//        }
//        $page_type = 'filter';
//        if($request->has('json')) {
//            return response()->json([
//                'conversation' => $messages
//            ]);
//        }
//
//        $userId = $toSender->id;
//        return view('pages.message.vue-conversation')->with(
//            [
//                'conversation' => $messages,
//                'otherUser' => [
//                    'id' => $otherUser->id,
//                    'getEncryptedId' => $otherUser->getEncryptedId(),
//                    'first_name' => $otherUser->get_firstname(),
//                    'last_name' => $otherUser->last_name,
//                    'getAge' => $otherUser->getAge(),
//                    'city' => $otherUser->city->name,
//                    'country' => $otherUser->country->name,
//                    'banned' => $otherUser->banned,
//                    'deactivated' => $otherUser->deactivated,
//                    'deleted_at' => $otherUser->deleted_at,
//                    'account_show' => $otherUser->account_show,
//                    'privatePhotoPermissionCnt' => $otherUser->privatePhotoPermissionCnt(),
//                    'profileLink' => $otherUser->profileLink(),
//                    'getProfilePic' => $otherUser->getProfilePic(),
//                    'blockedToFlag' => user_blocked_to_me($otherUser->id),
//                    'blockedByFlag' => user_blocked_by_me($otherUser->id),
//                    'skippedToFlag' => user_skipped_to_me($otherUser->id),
//                    'skippedByFlag' => user_skipped_by_me($otherUser->id),
//                    'sendArchiveLnk' => route('send-archive', ['user_id'=>$otherUser->getEncryptedId()]),
//                    'sendTrashLnk' => route('send-trash', ['user_id'=>$otherUser->getEncryptedId(), 'pageType' => $page_type]),
//                ],
//                'authUser' => [
//                    'id' => $authUser->id,
//                    'getProfilePic' => $authUser->getProfilePic(),
//                    'hasPrivatePhotos' => $authUser->hasPrivatePhotos(),
//                    'hasSharedPhotos' => $authUser->hasSharedPhotos($toSender->id)
//                ],
//                'receiver_id' => $userId,
//                'page_type' => $page_type,
//                'page_link' => route($page_type)
//            ]
//        );
//    }
//
//    /**************** End Filter here **************/



    /**************** Archive Starts here **************/
    public function getArchive(Request $request,$showunread=''){
        $archive= $this->messageRepository->getArchive($request,$showunread);
        $conversations=array();

        foreach($archive['fetchInbox'] as $key=>$value) {
            //echo $value->sender_id;
            $conversations[$key]['msg']=$value;
            $conversations[$key]['user']=User::withTrashed()->find($value->sender_id);
        }
        //print_r($conversations);

        return view('pages.message.archive')->with(['conversations'=>$conversations, 'conversationObj'=>$archive['fetchInbox'], 'showunread'=>$showunread]);
    }

    public function getArchiveConversation($senderId){
        $validator = JsValidator::make($this->validationRules);
        $toSender = User::withTrashed()->withEncryptedId($senderId)->first();
        $otherUser=User::withTrashed()->find($toSender->id);
        $authUser=Auth::user();
        //echo $toSender->id;
        $conversation= $this->messageRepository->getArchiveConversation($toSender->id);

        /***** code to make the read_status_flag to "READ" ***********/
        Message::where('sender_id', $toSender->id)->where('receiver_id',$authUser->id)->update(['read_status' =>'Y']);
        /***** code to make the read_status_flag to "READ" ***********/

        $userId=$toSender->id;

        return view('pages.message.conversation')->with(['conversation'=>$conversation,'otherUser'=>$otherUser,'authUser'=>$authUser, 'validator'=>$validator,'rec_id'=>$userId,'pagetype'=>'archive']);
    }

    public function sendInbox($senderId) {
        $toSender = User::withTrashed()->withEncryptedId($senderId)->first();
        // $otherUser=User::find($toSender->id);
        $conversation= $this->messageRepository->sendMessageToInbox($toSender->id);
        return redirect()->route('archive');
        //return redirect()->back();
    }
    /**************** End Archive here *******************/

    /**************** Trash Starts here **************/
    public function getTrash(Request $request){
        $trash= $this->messageRepository->getTrash($request);
        $conversations=array();
        foreach($trash['fetchInbox'] as $key=>$value) {
            //echo $value->sender_id;
            $conversations[$key]['msg']=$value;
            $conversations[$key]['user']=User::withTrashed()->find($value->userId);
        }
//        print_r($conversations);

        return view('pages.message.trash')->with(['conversations'=>$conversations, 'conversationObj'=>$trash['fetchInbox']]);
    }

    public function getTrashConversation($senderId){
        $validator = JsValidator::make($this->validationRules);
        $toSender = User::withTrashed()->withEncryptedId($senderId)->first();
        $otherUser=User::withTrashed()->find($toSender->id);
        $authUser=Auth::user();
        //echo $toSender->id;
        $conversation= $this->messageRepository->getTrashConversation($toSender->id);

        $userId=$toSender->id;

        return view('pages.message.conversation')->with(['conversation'=>$conversation,'otherUser'=>$otherUser,'authUser'=>$authUser, 'validator'=>$validator,'rec_id'=>$userId,'pagetype'=>'trash']);
    }

    public function moveToInbox($senderId) {
        $toSender = User::withTrashed()->withEncryptedId($senderId)->first();
        $conversation= $this->messageRepository->moveTrashToInbox($toSender->id);
        return redirect(route('trash'));
    }

    public function moveToArchive($senderId) {
        $toSender = User::withTrashed()->withEncryptedId($senderId)->first();
        $conversation= $this->messageRepository->moveTrashToArchive($toSender->id);
        return redirect(route('trash'));
    }

    public function moveToDelete($senderId) {
        $toSender = User::withTrashed()->withEncryptedId($senderId)->first();
        $conversation= $this->messageRepository->moveTrashToDelete($toSender->id);
        return redirect(route('trash'));
    }
    /**************** End Trash here *******************/

    public function deleteConversation($userId){
//        $toUser = User::withEncryptedId($userId)->first();
//        $conversation = Talk::getConversationsByUserId($toUser->id);
//        Talk::softDeleteConversation($conversation->messages[0]->conversation_id);
//        return redirect(route('inbox'));
        echo "delete conversation";
    }

}