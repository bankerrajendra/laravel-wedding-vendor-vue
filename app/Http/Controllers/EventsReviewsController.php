<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use App\Models\EventReview;
use App\Models\User;
use App\Logic\Admin\EventReviewRepository;
use App\Logic\Event\EventRepository;

class EventsReviewsController extends Controller
{
    private $validation_rules = [
        'message'           => 'required|min:50|max:500',
        'selected_rating'   => 'required|numeric|between:1,5'
    ];

    private $validation_messages = [
        'message.required'          => "Comment field is required.",
        'message.min'               => "Comment should be minimum 50 characters length.",
        'message.max'               => "Comment should be maximum 500 characters length.",
        'selected_rating.required'  => "Rating field is required.",
        'selected_rating.numeric'   => "Rating must be a number.",
        'selected_rating.between'   => "Rating must be between 1 and 5."
    ];

    /**
     * Get Reviews
     */
    public function getReviews(Request $request, EventReviewRepository $review_repo)
    {
        $per_page = $request->has('per_page') ? $request->input('per_page') : 10;
        $page = $request->has('per_page') ? $request->input('per_page') : 1;
        $reviews = $review_repo->getRecords()
                            ->where('approved', '=', "1")
                            ->latest()
                            ->paginate($per_page);

        return view('pages.review.list', [
            'reviews'   => $reviews,
            'per_page'  => $per_page,
            'page'      => $page
        ]);
    }

    /**
     * Write Review
     */
    public function showWriteReview($eventId, EventReviewRepository $review_repo, EventRepository $eventRepository)
    {
        $authUser = Auth::user();
        if(User::where('id', $authUser->id)->hasVendorRole()->exists()) {
            return view('403', ['error_message' => 'You must be logged in as a user to post event\'s reviews']);
        }
        $logged_user = Auth::id();
        
        if(!$eventRepository->checkEventIsActive($eventId)) {
            abort(403, 'Unauthorized action.');
        }
        // check is there review already added by user.
        $user_review = $review_repo->getUserReview($logged_user, base64_decode($eventId));
        // event name
        $eventInfo = $eventRepository->getEvent($eventId);
        $validator = JsValidator::make($this->validation_rules, $this->validation_messages);
        return view('pages.user.write-event-review', [
            'event_id' => $eventId,
            'user_review' => $user_review,
            'eventInfo' => $eventInfo,
            'validator' => $validator
        ]);
    }

    /**
     * Handle submit review action
     */
    public function handleSubmitReview(Request $request, EventReviewRepository $review_repo, EventRepository $eventRepository)
    {
        $user_id = Auth::id();
        $event_id = base64_decode($request->input('event_id'));
        if(!$eventRepository->checkEventIsActive($request->input('event_id'))) {
            abort(403, 'Unauthorized action.');
        }
        
        $validator = Validator::make($request->all(), $this->validation_rules, $this->validation_messages );
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        // check is there review already added by user.
        $user_review = $review_repo->getUserReview($user_id, $event_id);
        $eventInfo = $eventRepository->getEventBy('id', $event_id);
        try {
            if($user_review != '') {
                if($request->input('message') != $user_review->message) {
                    $user_review->message = $request->input('message');
                    $user_review->rating = $request->input('selected_rating');
                    $user_review->approved = '0';
                    $user_review->save();
                }
            } else {
                EventReview::create([
                    'user_id'   => $user_id,
                    'event_id'  => $event_id,
                    'vendor_id' => $eventInfo->vendor_id,
                    'message'   => $request->input('message'),
                    'rating'    => $request->input('selected_rating'),
                    'site_id'   => config('constants.site_id'),
                    'approved'  => '0'
                ]);
            }
            return redirect($eventInfo->getProfileLink());
        } catch(\Exception $e) {
            return back()->withErrors('Something wrong while adding review.')->withInput();
        }
    }

    /**
     * Admin Lists
     */
    public function showAdminListings(Request $request, EventReviewRepository $review_repo, EventRepository $eventRepository)
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $status         = $request->has('status') ? $request->input('status') : "";
        $search         = $request->has('search') ? $request->input('search') : "";
        $event          = $request->has('event') ? $request->input('event') : "";
        $reviews        = $review_repo->getRecords();

        if($search != "") {
            $reviews = $review_repo->getSearchRecords($reviews, $search);
        }

        if($status != "") {
            $reviews  = $reviews->where('approved', '=', $status);
        }
        if($event != "") {
            $reviews  = $reviews->where('event_id', '=', $event);
        }
        $reviews = $reviews->latest()
                ->paginate($per_page);          
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $approved_counts        = $review_repo->getStatusRecords('1')
                                ->count();
        $unaprroved_counts      = $review_repo->getStatusRecords('0')
                                ->count();
        $all_counts             = $review_repo->getRecords()
                                ->count();

        $events = $eventRepository->getRecords()->get(['id', 'event_name']);

        return view('pages.admin.events.reviews.list', [
            'reviews'               => $reviews,
            'per_page'              => $per_page,
            'status'                => $status,
            'search'                => $search,
            'approved_records'      => $approved_counts,
            'unaprroved_records'    => $unaprroved_counts,
            'all_records'           => $all_counts,
            'event'                 => $event,
            'events'                => $events
        ]);
    }

    public function handleBulkActionReview(Request $request, EventReviewRepository $review_repo)
    {
        $action       = $request->has('action') ? $request->input('action') : "";
        $review_ids   = $request->has('review_ids') ? $request->input('review_ids') : "";
        if( $action != "" && ($action == "delete" || $action == "1" || $action == "0") && !empty($review_ids) ) {
            $reviews = $review_repo->getRecords($review_ids);
            if($action == "0" || $action == "1") {
                try {
                    foreach($reviews as $review) {
                        $review->approved = $action;
                        $review->save();
                    }
                    return redirect()->back()->withSuccess('Review(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating review(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($reviews as $review) {
                        $review->delete();
                    }
                    return redirect()->back()->withSuccess('Review(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting review(s), please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    public function showEditReview($id, EventReviewRepository $review_repo)
    {
        $validation_rules   = [
            'approved'      => 'required|numeric',
            'id'            => 'required|numeric'
        ];
        $validator          = JsValidator::make($validation_rules);
        
        // get single record
        $review             = $review_repo->getRecords($id);

        return view('pages.admin.events.reviews.edit', [
            'validator'     => $validator,
            'review'        => $review
        ]);
    }

    public function handleEditreview(Request $request, EventReviewRepository $review_repo)
    {
        // initialize
        $approved                       = $request->has('approved') ? $request->input('approved') : "";
        $id                             = $request->has('id') ? $request->input('id') : "";

        // validate
        $validation_rules   = [
            'approved'      => 'required|numeric',
            'id'            => 'required|numeric'
        ];
        $validate                       = $request->validate($validation_rules);

        // save
        $save_review                    = $review_repo->getRecords($id);
        $save_review->approved          = $approved;

        // return with success or error
        if ($save_review->save()) {
            return redirect()->back()->withSuccess('Review updated successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in editing review, please try again.');
        }
    }

    public function showViewReview($id, EventReviewRepository $review_repo)
    {
        // get single record
        $review                 = $review_repo->getRecords($id);

        return view('pages.admin.events.reviews.view', [
            'review'        => $review
        ]);
    }
}
