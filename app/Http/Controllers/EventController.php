<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logic\Common\LocationRepository;
use App\Models\User;
use App\Models\Message;
use App\Models\VendorCategory;
use App\Models\VendorsInformation;
use App\Models\VendorEventInformation;
use App\Models\Review;
use App\Models\Artist;
use App\Logic\Admin\VendorCategoryRepository;
use App\Logic\Event\EventRepository;
use App\Models\UserPriceRequest;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Country;
use App\Models\City; 
use App\Models\State;
use Illuminate\Support\Facades\DB;
use App\Logic\Admin\CmsPagesRepository;
use App\Models\VendorCategorySatelliteInformation;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use App\Events\FileUpload;
use App\Logic\Admin\EventReviewRepository;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralEmail;
use App\Traits\CaptureIpTrait;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Models\VendorEventImage;
use Illuminate\Support\Facades\Storage;
use App\Logic\Admin\ArtistRepository;
use DateTime;

class EventController extends Controller
{
    protected $eventRepository;
    protected $vendorCatRepo;
    protected $cmsPagesService;
    protected $reviewRepo;
    protected $locationRepo;
    protected $artistRepository;

    protected $reviewPerPage = 5;

    protected $validationMessageRequest = [
        'email'                 => 'required|max:255|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        'mobile_number'         => 'phone_valid',
        'password'              => 'required|min:6|max:30',
        'message'               => 'required|max:300'
    ];

    protected $validationVendorEventRules = [
        'categories'            => 'required|array',
        'categories.*'          => 'required|string',
        'address'               => 'required|max:200',
        'country'               => 'required',
        'state'                 => 'required',
        'city'                  => 'required',
        'zip'   			    => 'required | min:5',
        'event_name'            => 'required|alpha_spaces|min:3|max:29',
        'venue'                 => 'required|min:3|max:200',
        'description'           => 'required',
        'start_date'            => 'required|min:10|max:10|future_date',
        'end_date'              => 'required|min:10|max:10|future_date',
        'start_time'            => 'required',
        'ticket_url'            => 'validate_url',
        'video_url'             => 'sometimes|nullable|emebed_url',
        'meta_keywords'         => 'sometimes|max:500',
        'business_website'      => 'validate_url',
        'facebook'              => 'validate_url',
        'instagram'             => 'validate_url',
        'twitter'               => 'validate_url',
        'linkedin'              => 'validate_url',
        'pinterest'             => 'validate_url'
    ];

    protected $site_id;

    public function __construct(EventRepository $eventRepository, VendorCategoryRepository $vendorCatRepo, CmsPagesRepository $cmsPagesService, LocationRepository $locationRepository, EventReviewRepository $reviewRepo, ArtistRepository $artistRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->vendorCatRepo = $vendorCatRepo;
        $this->cmsPagesService = $cmsPagesService;
        $this->reviewRepo = $reviewRepo;
        $this->site_id = config('constants.site_id');
        $this->locationRepo = $locationRepository;
        $this->artistRepository = $artistRepository;
        View::share('countries', $locationRepository->getCountries());
    }

    public function showLanding($countryId = '')
    {
        // 1. show top carousel recent upcoming events banners
        $topBanners = $this->eventRepository->getReventEventsBanner(3, $countryId);

        // 2. Featured Artists - show random 4 artist that are having image
        $artists = $this->artistRepository->getArtistsWithImages(4)->get();

        // 3. Show countries user logged in or based on ip and show the 4 artists from event
        $locationInformation = getUserLocationInformation();

        // 4. show featued events
        $recentFeaturedEvents = $this->eventRepository->getFeaturedEvents(12, $countryId);
        try {
            if($countryId == '') {
                $countryInfo = Country::where('code', '=', $locationInformation->countryCode)->first(['id']);
                $country_id = $countryInfo->id;
            } else {
                if(Country::where('id', '=', $countryId)->exists()) {
                    $country_id = $countryId;
                } else {
                    $country_id = '';
                }
            }
        } catch(\Exception $e) {
            $country_id = '';
        }

        $meta_fields['title'] = 'Event Landing';
        $meta_fields['keyword'] = 'Event Landing';
        $meta_fields['description'] = 'Event Landing';
        return view('pages.user.event-landing', [
            'topBanners' => $topBanners,
            'artists' => $artists,
            'country_id' => $country_id,
            'recentFeaturedEvents' => $recentFeaturedEvents,
            'metafields' => $meta_fields,
        ]);

    }

    public function showAdd()
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        // get events for sidebar
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        $events_categories = $this->vendorCatRepo->getVendorCategoryByTypeFront('event');
        $artists = Artist::where('is_approved', 'Y')
                        ->where('site_id', config('constants.site_id'))
                        ->orderBy('name', 'ASC')
                        ->get(['id', 'name']);
        $validator = JsValidator::make($this->validationVendorEventRules);
        return view('auth.events.add', [
                'events' => $events,
                'events_categories' => $events_categories,
                'artists' => $artists,
                'validator' => $validator
            ]
        );
    }

    public function handleAdd(Request $request)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), $this->validationVendorEventRules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        // get single event object
        try {
            $eventObj = $this->eventRepository->addRecord($request);
            // send event create email
            $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
            $mail_req_arry = [
                'mail_name' => 'Event Registration',
                'mail_replace_vars' => [
                    '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                    '[%WEB_NAME%]' => getGeneralSiteSetting('site_name'),
                    '[%USER_FIRST_NAME%]' => $user->first_name,
                    '[%USER_EMAIL%]' => $user->email,
                    '[%EVENT_NAME%]' => $eventObj->event_name,
                    '[%EVENT_EDIT_LINK%]' => $eventObj->getEditLink()
                ],
                'unsubscribe_link' => $unsubscribe_link
            ];
            Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => 'Some issue while saving event information. Please try again.'
            ]);
        }
        
        return response()->json([
            'status' => 1,
            'message' => 'Event added successfully.'
        ]);
    }

    public function imageEventPosterUpload(Request $request)
    {
        $status = event(new FileUpload('eventTempPoster', $request));
        return response()->json($status);
    }

    public function imageEventCoverUpload(Request $request)
    {
        $status = event(new FileUpload(config('constants.fileType.eventTempCover'), $request));
        return response()->json($status);
    }

    public function imageEventProfileUpload(Request $request)
    {
        $status = event(new FileUpload(config('constants.fileType.eventTempImage'), $request));
        return response()->json($status);
    }

    public function showEdit($eventId)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        // check event is there for this user
        if(!$this->eventRepository->checkEventOwner($eventId)) {
            abort(403, 'Unauthorized action.');
        }
        // get events for sidebar
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        // get single event object
        $eventObj = $this->eventRepository->getEventRecord($eventId);
        $events_categories = $this->vendorCatRepo->getVendorCategoryByTypeFront('event');
        // as edit event not able to update dates
        unset($this->validationVendorEventRules['start_date']);
        unset($this->validationVendorEventRules['end_date']);
        unset($this->validationVendorEventRules['start_time']);
        $artists = Artist::where('is_approved', 'Y')
                        ->where('site_id', config('constants.site_id'))
                        ->orderBy('name', 'ASC')
                        ->get(['id', 'name']);
        $validator = JsValidator::make($this->validationVendorEventRules);
        return view('auth.events.edit', [
                'event' => $eventObj,
                'events' => $events,
                'events_categories' => $events_categories,
                'artists' => $artists,
                'validator' => $validator
            ]
        );
    }

    public function handleEdit(Request $request)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $eventId = $request->input('event_id');
        // check event is there for this user
        if(!$this->eventRepository->checkEventOwner($eventId)) {
            abort(403, 'Unauthorized action.');
        }
        // as edit event not able to update dates
        unset($this->validationVendorEventRules['start_date']);
        unset($this->validationVendorEventRules['end_date']);
        unset($this->validationVendorEventRules['start_time']);
        $validator = Validator::make($request->all(), $this->validationVendorEventRules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        // get single event object
        try {
            $eventUpdate = $this->eventRepository->updateRecord($request);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => 'Some issue while updating event information. Please try again.'
            ]);
        }
        // success
        $eventObj = $this->eventRepository->getEventRecord($eventId);
        
        return response()->json([
            'status' => 1,
            'message' => 'Event updated successfully.',
            'event_link' => $eventObj->getProfileLink(true)
        ]);
    }

    public function imageEditEventCoverUpload(Request $request)
    {
        $status = event(new FileUpload('editEventCover', $request));
        return response()->json($status);
    }

    public function imageEditEventPosterUpload(Request $request)
    {
        $status = event(new FileUpload('editEventPoster', $request));
        return response()->json($status);
    }

    public function imageEditEventProfileUpload(Request $request)
    {
        $status = event(new FileUpload('editEventProfile', $request));
        return response()->json($status);
    }

    public function showDetail($eventId, Request $request)
    {
        $selfEvent = false;
        $category_slug = $request->has('category') ? $request->input('category') : '';
        // check whether logged in user is same user's profile viewing
        if(Auth::check()) {
            if($this->eventRepository->checkEventOwnerBySlug($eventId)) {
                $selfEvent = true;
            }
        }

        try {
            $event = $this->eventRepository->getEventBy('event_slug', $eventId, $selfEvent);
            if($event == null) {
                return redirect()->route('event-not-found');
            }
            if(Auth::check()) {
                if(Auth::id() != $event->vendor_id && $event->is_approved == 0) {
                    return redirect()->route('event-not-found');
                }
            }
        } catch (\Exception $e) {
            return redirect()->route('event-not-found');
        }
        $vendor = User::where('id', $event->vendor_id)->hasVendorRole()->first();
        // check logged in user
        $form_prepopulate = [];
        $allowShortList = false;
        if(Auth::check()) {
            $logged_user = User::where('id', Auth::id())->first();
            $form_prepopulate['first_name'] = $logged_user->first_name;
            $form_prepopulate['last_name'] = $logged_user->last_name;
            $form_prepopulate['mobile_number'] = $logged_user->mobile_number;
            $form_prepopulate['email'] = $logged_user->email;
            // check role and see the role
            if($logged_user->hasRole('unverified')) {
                $allowSendMessage = true;
                $allowShortList = true;
            } else {
                $allowSendMessage = false;
            }
        } else {
            $allowSendMessage = true;
        }
        $validator = JsValidator::make($this->validationMessageRequest);

        // get reviews
        $reviews = $this->reviewRepo->getReviews($event->id, $this->reviewPerPage, 'all');
        
        // get similar events
        $event_cats = unserialize($event->categories);
        $total_event_similar_cat = $this->eventRepository->getSimilarEvents($event->vendor_id, $event_cats)->count();
        if($total_event_similar_cat > 0) {
            $get_venors_similar = $this->eventRepository->getSimilarEvents($event->vendor_id, $event_cats)->get();
            if($total_event_similar_cat >= 9) {
                $similar_events_obj = $get_venors_similar->random(9);
            } else {
                $similar_events_obj = $get_venors_similar->random($total_event_similar_cat);
            }
            // prepare array
            $similar_random_events = [];
            $sm_v = 0;
            foreach($similar_events_obj as $similar_event_obj) {
                $similar_random_events[$sm_v]['image'] = $similar_event_obj->getProfilePic();
                $similar_random_events[$sm_v]['profile'] = $similar_event_obj->getProfileLink();
                $similar_random_events[$sm_v]['average_rating'] = $similar_event_obj->getAverageReviewRating();
                $similar_random_events[$sm_v]['title'] = $similar_event_obj->event_name;
                $similar_random_events[$sm_v]['city'] = $similar_event_obj->city->name;
                $similar_random_events[$sm_v]['country'] = $similar_event_obj->country->name;
                $start_date = new DateTime($similar_event_obj->start_date);
                $start_date_timestamp = $start_date->getTimestamp();
                $similar_random_events[$sm_v]['date'] = date('M d', $start_date_timestamp);
                $sm_v++;
            }
        } else {
            // no similar vendors
            $similar_random_events = [];
        }
        // intialize meta fields
        // [Event Name] [Event City Name] [Event Country Name] | Wedding Event
        $meta_fields['title'] = '';
        $meta_fields['keyword'] = '';
        $meta_fields['title'] .= $event->event_name . ' ';
        $meta_fields['title'] .= $event->city->name .' ';
        $meta_fields['title'] .= $event->country->name .' ';
        $meta_fields['title'] .= '| Event';
        
        $meta_fields['description'] = '';
        //[Event Address] , [Event "Description" (html strip)]
        if($event->address != '') {
            $meta_fields['description'] .= $event->address;
        }
        if($event->description != '') {
            $meta_fields['description'] .= ', '.showReadMoreApi(strip_tags($event->description), 100, '');
        }
        
        if($event->meta_keywords != '') {
            $meta_fields['keyword'] = $event->meta_keywords;
        }
        return view('pages.user.event-detail', [
            'metafields' => $meta_fields,
            'selfEvent' => $selfEvent,
            'allowSendMessage' => $allowSendMessage,
            'allowShortList' => $allowShortList,
            'event' => $event,
            'form_prepopulate' => $form_prepopulate,
            'reviews' => $reviews,
            'review_page_page' => $this->reviewPerPage,
            'similarEvents' => $similar_random_events,
            'vendorObj' => $vendor,
            'category_slug' => $category_slug,
            'validator' => $validator
        ]);

    }
    // event not found
    public function notFound() 
    {
        return view('pages.user.event-not-found');
    }

    public function showSearch($category = '', Request $request)
    {
        $city_zip = $request->has('city') ? $request->input('city') : "";
        $cat_information = null;
        if($category != '' && $category != 'muslim-events') {
            if($this->vendorCatRepo->getVendorSingleCategoryBy('slug', $category, 'event')->exists()) {
                $cat_information = $this->vendorCatRepo->getVendorSingleCategoryBy('slug', $category, 'event')->first(['id', 'name', 'slug']);
                $category_name = $cat_information->name;
            } else {
                $category_name = "";
            }
        } else {
            $category_name = $request->has('category') ? $request->input('category') : "";
            $cat_information = $this->vendorCatRepo->getVendorSingleCategoryBy('name', $category_name, 'event')->first(['id', 'name', 'slug']);
        }
        
        $location_type = $request->has('location_type') ? $request->input('location_type') : "";
        $location_id = $request->has('location_id') ? $request->input('location_id') : "";

        // search preference
        $search_preference = [];
        
        // check that category name is exists then and then apply filter with that
        $search_preference['category_id'] = '';
        $search_preference['category_name'] = $category_name;
        if($category_name != '') {
            if($this->vendorCatRepo->getVendorSingleCategoryBy('name', $category_name, 'event')->exists()) {
                $getCatId = $this->vendorCatRepo->getVendorSingleCategoryBy('name', $category_name, 'event')->first(['id', 'name', 'slug']);
                $search_preference['category_id'] = $getCatId->id;
            }
        }
        
        $search_preference['city_zip'] = $city_zip;
        $search_preference['location_type'] = $location_type;
        $search_preference['location_id'] = $location_id;
        
        $events = $this->eventRepository->searchEvents($search_preference);

        $cmsPageInfo = $this->cmsPagesService->getPageRec('search-events');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['heading']= $cmsPageInfo->page_description;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;
        $meta_fields['cat_description'] = $cmsPageInfo->meta_description;
        if($search_preference['category_id'] != '') {
            $meta_fields['cat_description'] = $cmsPageInfo->meta_description;
        } else {
            if($category_name != "") {
                $meta_fields['cat_description'] = '';
            }
        }

        if($category_name != '') {
            $vendor_type_id = config('constants.vendor_type_id.event');
            $meta_values = VendorCategorySatelliteInformation::where('satellite_id',config('constants.site_id'))
                ->where('vendor_category_id', function($query) use($category_name, $vendor_type_id) {
                    $query = $query->select('id')->from('vendors_categories')->where('name', 'like', '%'.$category_name.'%')->where('vendor_type', $vendor_type_id);
                    return $query;
            })->first();
            if(!is_null($meta_values)) {
                $meta_fields['title']=$meta_values->title;
                $meta_fields['keyword']=$meta_values->keywords;
                $meta_fields['description']=$meta_values->meta_description;
                $meta_fields['cat_description'] = $meta_values->description;
            }
        }
        
        // check logged in user
        if(Auth::check()) {
            $logged_user = User::where('id', Auth::id())->first();
            // check role and see the role
            if($logged_user->hasRole('unverified')) {
                $allowSendMessage = true;
            } else {
                $allowSendMessage = false;
            }
        } else {
            $allowSendMessage = true;
        }
        
        if($request->ajax()) {
            if($events->nextPageUrl() != "") {
                $nextPageUrl = $events->nextPageUrl().'&category='.urlencode($category_name).'&city='.urlencode($city_zip).'&location_type='.$location_type.'&location_id='.$location_id;
            } else {
                $nextPageUrl = '';
            }
            if($cat_information != null) {
                $category_slug = $cat_information->slug;
            } else {
                $category_slug = '';
            }
            return [
                'events' => view('pages.user.ajax.show-events', [
                    'events' => $events,
                    'allowSendMessage' => $allowSendMessage]
                )->render(),
                'next_page' => $nextPageUrl,
                'total_records' => $events->total(),
                'category_name' => urldecode($category_name),
                'category_slug' => $category_slug,
                'category_description' => $meta_fields['cat_description'],
                'location_name' => urldecode($city_zip),
                'metafields' => $meta_fields
            ];
        }
        $validator = JsValidator::make($this->validationMessageRequest);
        $categories = $this->vendorCatRepo
                            ->getStatusRecords(1)
                            ->getVendorByType('event')
                            ->where('satellites', 'like', '%;s:'.strlen($this->site_id).':"'.$this->site_id.'";%')
                            ->orderBy('name', 'ASC')
                            ->get(['id', 'name', 'slug']);
                            
        return view('pages.user.search-events', [
            'events' => $events,
            'metafields' => $meta_fields,
            'allowSendMessage' => $allowSendMessage,
            'categories' => $categories,
            'category_name' => $category_name,
            'city_zip' => $city_zip,
            'location_type' => $location_type,
            'location_id' => $location_id,
            'validator' => $validator
        ]);
    }

    public function getSearchAutoSuggestions(Request $request) 
    {
        $key = $request->has('key') ? $request->input('key') : "";
        $this->site_id = config('constants.site_id');
        $categories = $this->vendorCatRepo
                        ->getStatusRecords(1)
                        ->getRecordsSearch($key)
                        ->where('satellites', 'like', '%;s:'.strlen($this->site_id).':"'.$this->site_id.'";%')
                        ->getVendorByType('event')
                        ->orderBy('name', 'ASC')
                        ->get(['id', 'name', 'slug']);

        $output = '';
        if($categories->count() > 0) {
            $output .= '<ul class="dropdown-menu category-event-autosuggestion" style="display:block; position:absolute; max-height: 200px; overflow-y: scroll;width:100%;">';
            if($categories->count() > 0) {
                $output .= '<li><strong style="padding: 3px 20px;">Category</strong></li>';
                foreach($categories as $category)
                {
                    $output .= '
                    <li class="category"><a href="javascript:void(0);">'.$category->name.'</a></li>
                    ';
                }
            }
            
        }
        echo $output;
    }

    public function handleSubmitMessageRequest(Request $request)
    {
        if(Auth::check()) {
            unset($this->validationMessageRequest['email']);
            unset($this->validationMessageRequest['password']);
        }
        $validator = Validator::make($request->all(), $this->validationMessageRequest);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $response_error = [
            'status' => 0,
            'message' => 'Something went wrong, please try again.'
        ];
        $validator = Validator::make($request->all(), $this->validationMessageRequest);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        
        $vendor_id = $request->has('vendor_id') ? $request->input('vendor_id') : "";
        $event_id = $request->has('event_id') ? $request->input('event_id') : "";
        
        if($vendor_id != "") {
            // check vendor exists with that id
            if(User::withEncryptedId($vendor_id)->hasVendorRole()->isActive()->exists()) {
                $vendor = User::withEncryptedId($vendor_id)->first();
            } else {
                $response_error['message'] = "Event Vendor does not exist";
                return response()->json($response_error);
            }
        } else {
            return response()->json($response_error);
        }

        if($event_id != "") {
            // check event exists with that id
            if($this->eventRepository->checkEventIsActive($event_id)) {
                $event = $this->eventRepository->getEvent($event_id);
            } else {
                $response_error['message'] = "Event does not exist";
                return response()->json($response_error);
            }
        } else {
            return response()->json($response_error);
        }

        if (Auth::check()) {
            // login user
            $user = Auth::user();
            // if user is not having role with unverified = user then show error
            if(!$user->hasRole('unverified')) {
                return response()->json(
                    [
                        'status' => 0,
                        'message' => 'You are not allowing to send message! please login as User.'
                    ]
                );
            }
        } else {
            if(User::where('email', $request->input('email'))->hasUserRole()->exists()) {
                if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
                    $user = Auth::user();
                } else {
                    // pass message that user is there with same email but worng password
                    return response()->json(
                        [
                            'status' => 0,
                            'message' => 'You entered incorrect password.'
                        ]
                    );
                }
            } else {
                // check user is having other role
                if(User::where('email', $request->input('email'))->hasVendorRole()->exists()) {
                    return response()->json(
                            [
                                'status' => 0,
                                'message' => 'You are not allowing to send message! please login as User.'
                            ]
                        );
                }
                $ipAddress = new CaptureIpTrait();
                $role = Role::where('slug', '=', 'unverified')->first();
                // Register the new user or whatever.
                $user = User::create([
                    'name'                  => $request->input('first_name').".".$request->input('last_name'),
                    'first_name'            => $request->input('first_name'),
                    'last_name'             => $request->input('last_name'),
                    'email'                 => $request->input('email'),
                    'password'              => Hash::make($request->input('password')),
                    'token'                 => str_random(64),
                    'mobile_number'         => $request->input('mobile_number'),
                    'signup_ip_address'     => $ipAddress->getClientIp(),
                    'activated'             => 1,
                    'access_grant'          => 'Y',
                    'site_id'               => (int) config('constants.site_id')
                ]);

                $user->attachRole($role);
                // send welcome email
                $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'User Registration',
                    'mail_replace_vars' => [
                        '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                        '[%WEB_NAME%]' => getGeneralSiteSetting('site_name'),
                        '[%USER_FIRST_NAME%]' => $user->first_name,
                        '[%USER_EMAIL%]' => $user->email
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
                // log user in
                Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);
            }
        }
        // preferred contact
        if($request->has('check_email_contact') == true && $request->input('check_email_contact') == 1) {
            $preferred_contact_method = 'email';
        } else if ($request->has('check_phone_contact') == true && $request->input('check_phone_contact') == 1) {
            $preferred_contact_method = 'phone';
        } else if ($request->has('check_message_contact') == true && $request->input('check_message_contact') == 1) {
            $preferred_contact_method = 'message';
        } else {
            $preferred_contact_method = 'email';
        }
        $message_to_send = 'New message for Event: <b>'.$event->event_name.'</b><br><br>';
        $message_to_send .= 'Check below details<br>';
        
        if($request->has('mobile_number')) {
            $message_to_send .= 'Phone:'.$request->input('mobile_number').'<br>';
        }
        if($preferred_contact_method != '') {
            $message_to_send .= 'Preferred Contact Method:'.$preferred_contact_method.'<br>';
        }
        if($request->has('message')) {
            $message_to_send .= 'Message:'.$request->input('message').'<br>';
        }
        // send message to vendor
        Message::create([
            'sender_id'         => $user->id,
            'receiver_id'       => $vendor->id,
            'sender_archive'    => 'N',
            'message'           => $message_to_send,
            'created_at'        => Carbon::now()
        ]);
        // send email to event vendor
        $unsubscribe_link = route('unsubscribe', [ 'token' => $vendor->token, 'type' => '' ]);
        $mail_req_arry = [
            'mail_name' => 'User Event Message Request',
            'mail_replace_vars' => [
                '[%FIRST_NAME%]' => $vendor->first_name,
                '[%LAST_NAME%]' => $vendor->last_name,
                '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                '[%WEBSITENAME%]' => getGeneralSiteSetting('site_name')
            ],
            'unsubscribe_link' => $unsubscribe_link
        ];
        Mail::to($vendor->email)->send(new GeneralEmail($mail_req_arry));
        // save price ticket information
        if(UserPriceRequest::Create(
            [
                'vendor_id' => $vendor->id,
                'user_id' => Auth::id(),
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'mobile_number' => $request->input('mobile_number'),
                'preferred_contact_method' => $preferred_contact_method,
                'message' => $request->input('message'),
                'wedding_type' => null,
            ]
        )) {
            return response()->json(
                [
                    'status' => 1,
                    'message' => 'Message sent successfully.'
                ]
            );
        } else {
            $vendor_error['message'] = "Something went wrong while creating price request.";
            return response()->json($vendor_error);
        }
    }

    public function getReviewAjax(Request $request)
    {
        $event_id = base64_decode($request->input('event_id'));
        $per_rate = $request->has('per_rate') ? $request->input('per_rate') : 'all';
        // get reviews
        $reviews = $this->reviewRepo->getReviews($event_id, $this->reviewPerPage, $per_rate);
        if($request->ajax()) {
            return [
                'reviews' => view('pages.user.ajax.show-events-reviews', ['reviews' => $reviews])->render(),
                'next_page' => $request->input('page')+1,
                'next_url' => $reviews['next_page'],
                'per_rate' => $per_rate
            ];
        }
    }

    // show event reviews
    public function showReviews($eventId, Request $request)
    {
        try {
            $selfEvent = false;
            // check that it is owner or not
            if(Auth::check()) {
                $eventObj = $this->eventRepository->getEventRecord($eventId);
                // check 
                if($this->eventRepository->checkEventOwnerBySlug($eventObj->event_slug)) {
                    $selfEvent = true;
                }
            }
            $event = $this->eventRepository->getEventBy('enc_id', $eventId, $selfEvent);
            if($event == null) {
                return abort(404);    
            }
        } catch (\Exception $e) {
            return abort(404);
        }
        $event_id = base64_decode($eventId);
        $per_rate = $request->has('rate') ? $request->input('rate') : 'all';
        $reviews = $this->reviewRepo->getReviews($event_id, $this->reviewPerPage, $per_rate);

        return view('pages.user.events-reviews', [
            'reviews' => $reviews,
            'eventObj' => $event,
            'review_per_page' => $this->reviewPerPage,
            'per_rate' => $per_rate,
            'selfEvent' => $selfEvent
        ]);
    }

    // get admin events
    public function getAdminRecords(Request $request)
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $status         = $request->has('status') ? $request->input('status') : "";
        $search         = $request->has('search') ? $request->input('search') : "";
        $category       = $request->has('category') ? $request->input('category') : "";
        $vendor_id      = $request->has('vendor_id') ? $request->input('vendor_id') : "";
        $records        = $this->eventRepository->getRecords();

        if($search != "") {
            $records = $this->eventRepository->getSearchRecords($records, $search);
        }

        if($status != "") {
            $records  = $records->where('is_approved', '=', $status);
        }
        if($vendor_id != "") {
            $records  = $records->where('vendor_id', '=', $vendor_id);
        }
        if($category != "") {
            $records  = $records->where('categories', 'like', '%;s:'.strlen($category).':"'.$category.'";%');
        }
        $records = $records->orderBy('updated_at', 'desc')
                ->paginate($per_page);      
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $approved      = $this->eventRepository->getStatusRecords('1')
                            ->count();
        $unapproved    = $this->eventRepository->getStatusRecords('0')
                            ->count();
        $all_counts    = $this->eventRepository->getRecords()
                            ->count();
                            
        // get all categories
        $all_categories = $this->vendorCatRepo->getStatusRecords(1)->where('vendor_type', 2)->get(['id', 'name']);
        // all site vendors
        $vendors = User::hasVendorRole()->onlySite()->get(['id', 'first_name', 'last_name', 'name', 'email']);

        return view('pages.admin.events.list')->with(
            [
                'records'               => $records,
                'page'                  => $page,
                'per_page'              => $per_page,
                'status'                => $status,
                'search'                => $search,
                'category'              => $category,
                'approved_records'      => $approved,
                'unapproved_records'    => $unapproved,
                'all_records'           => $all_counts,
                'all_categories'        => $all_categories,
                'vendors'               => $vendors,
                'vendor_id'             => $vendor_id
            ]
        );
    }

    /**
     * Handle bulk action of list
     */
    public function handleBulkAction(Request $request)
    {
        $action = $request->has('action') ? $request->input('action') : "";
        $ids    = $request->has('ids') ? $request->input('ids') : "";
        if( $action != "" && ($action == "delete" || $action == "1" || $action == "0") && !empty($ids) ) {
            $records = $this->eventRepository->getRecords($ids);
            if($action == "1" || $action == "0") {
                try {
                    foreach($records as $record) {
                        $record->is_approved = $action;
                        $record->save();
                    }
                    return redirect()->back()->withSuccess('Records(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating Records(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($records as $record) {
                        $this->eventRepository->deleteRecord($record);
                    }
                    return redirect()->back()->withSuccess('Record(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting Record(s), please try again.');
                }
            }
        } else if($action == "sort") {
            //
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    // to check user is exists with same email and password combination else show message
    public function checkVendorExists(Request $request)
    {
        $email = $request->has('email') ? $request->input('email') : '';
        $password = $request->has('password') ? $request->input('password') : '';
        // 1. check user with this email exists
        if(User::where('email', $email)->exists()) {
            // 2. check this user has vendor role
            if(User::where('email', $email)->hasVendorRole()->exists()) {
                $user = User::where('email', $email)->first(['password']);
                // 4. Check password combination is fine or not.
                if(Hash::check($password, $user->password)) {
                    // 5. check user is not deleted
                    if(User::where('email', $email)->where('deleted_at', '<>', NULL)->exists()) {
                        return response()->json(
                            [
                                'status' => 0,
                                'type' => 'email',
                                'message' => 'Your account is delete from site, please contact with site administrator.'
                            ]
                        );    
                    }
                    // 6. return true with success that user exists
                    return response()->json(
                        [
                            'status' => 1,
                            'message' => 'User can add event with this email'
                        ]
                    );
                } else {
                    // 7. credentials are not matching please try again
                    return response()->json(
                        [
                            'status' => 0,
                            'type' => 'password',
                            'message' => 'Please enter correct password.'
                        ]
                    );
                }
            } else {
                // 3. throw error that user already exists please try diferent email
                return response()->json(
                    [
                        'status' => 0,
                        'type' => 'email',
                        'message' => 'Email already exists with other users type, you need to use different one.'
                    ]
                );
            }
        } else {
            // check wether this user deleted or not
            if(User::where('email', $email)->onlyTrashed()->exists()) {
                return response()->json(
                    [
                        'status' => 0,
                        'type' => 'email',
                        'message' => 'your account has already been deleted. Contact to admin at "'.getGeneralSiteSetting('site_email').'"'
                    ]
                );
            }
        }
        return response()->json(
            [
                'status' => 1,
                'message' => 'User can register with this email'
            ]
        );
    }
    // get artist for auto suggestion
    public function getArtistsAutoSuggestions(Request $request) 
    {
        $key = $request->has('key') ? $request->input('key') : "";
        $this->site_id = config('constants.site_id');
        $artists = Artist::where('is_approved', 'Y')
                        ->where('name', 'like', '%'.$key.'%')
                        ->where('site_id', config('constants.site_id'))
                        ->orderBy('name', 'ASC')
                        ->get(['id', 'name']);

        $output = '';
        if($artists->count() > 0) {
            $output .= '<ul class="dropdown-menu artist-autosuggestion" style="display:block; position:absolute; max-height: 200px; overflow-y: scroll;width:100%;">';
            if($artists->count() > 0) {
                $output .= '<li><strong style="padding: 3px 20px;">Artist</strong></li>';
                foreach($artists as $artist)
                {
                    $output .= '
                    <li class="artist"><a href="javascript:void(0);">'.$artist->name.'</a></li>
                    ';
                }
            }
            
        }
        echo $output;
    }

    public function adminEdit($eventId)
    {
        // get single event object
        $eventObj = $this->eventRepository->getAdminEventRecord($eventId);
        $events_categories = $this->vendorCatRepo->getVendorCategoryByTypeFront('event');

        $artists = Artist::where('is_approved', 'Y')
                        ->where('site_id', config('constants.site_id'))
                        ->orderBy('name', 'ASC')
                        ->get(['id', 'name']);
        // images
        $posterImg = VendorEventImage::where('event_id', $eventObj->id)->where('image_type', 'poster')->first();
        $coverImg = VendorEventImage::where('event_id', $eventObj->id)->where('image_type', 'cover')->first();
        $profileImg = VendorEventImage::where('event_id', $eventObj->id)->where('image_type', 'profile')->first();

        $validator = JsValidator::make($this->validationVendorEventRules);
        
        return view('pages.admin.events.edit', [
                'event' => $eventObj,
                'events_categories' => $events_categories,
                'artists' => $artists,
                'countryList' => $this->locationRepo->getCountries(),
                'stateList' => State::all(),
                'posterImg' => $posterImg,
                'coverImg' => $coverImg,
                'profileImg' => $profileImg,
                'validator' => $validator
            ]
        );
    }

    public function handleAdminEdit(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validationVendorEventRules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        // get single event object
        $eventId = $request->input('event_id');
        try {
            $eventUpdate = $this->eventRepository->updateRecord($request);
        } catch (\Exception $e) {
            return back()->withErrors('Some issue while updating event information. Please try again.');
        }
        // success
        $eventObj = $this->eventRepository->getEventRecord($eventId);
        
        return back()->withSuccess('Event updated successfully.');
    }

    public function approveEventPhoto(Request $request) {
        $photo_id = explode("-",$request["photo_id"]);

        try {
            VendorEventImage::where(array("id" => $photo_id))->update(array("is_approved"=> 'Y','disapprove_reason'=>''));
            $userImg = VendorEventImage::select('event_id')->where("id",$photo_id)->first();
            $message = array("status"=>1);
        } Catch(Exception $e) {
            $message = $e->getMessage();
        }

        return response()->json($message);
    }

    public function disapproveEventPhoto(Request $request) {
        $photo_id = explode("-",$request["photo_id"]);
        $disapprove_reason = $request['message'];

        try {
            $event_image = VendorEventImage::where(array("id" => $photo_id))->update(array("is_approved"=> 'D','disapprove_reason'=>$disapprove_reason));
            $message = array("status"=>1);
            $imgObj = VendorEventImage::where(array("id" => $photo_id))->first();
            $eventObj = VendorEventInformation::where('id', $imgObj->event_id)->first(['vendor_id']);
            // send email
            $toUser = User::where('id', $eventObj->vendor_id)->first();
            if(User::where('id', $toUser->id)->hasVendorRole()->exists()) {
                $notification_flag = $toUser->vendors_information->notify_content_approve_deny;
            }
            if($notification_flag == "Y") {
                $type = getUnsubscribeType('notify_content_approve_deny');
                $unsubscribe_link = route('unsubscribe', [ 'token' => $toUser->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Disapprove Event Image',
                    'mail_replace_vars' => [
                        '[%USER_FIRST_NAME%]' => $toUser->first_name,
                        '[%FIELD_DISAPPROVED_REASON%]' => $disapprove_reason,
                        '[%FIELD_DISAPPROVED%]' => 'Image'
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($toUser->email)->send(new GeneralEmail($mail_req_arry));
            }
        } Catch(Exception $e) {
            $message = $e->getMessage();
        }
        return response()->json($message);
    }
    // detete image
    public function deleteImage(Request $request)
    {
        $id = $request->input('id');
        $response = null;
        if(VendorEventImage::where(array("id" => $id))->count() > 0){
            $eventImage = VendorEventImage::where(array("id" => $id))->first();
            
            if(Storage::disk('s3')->exists('vendor_photos/original/' . $eventImage->image_full)) {
                Storage::disk('s3')->delete('vendor_photos/original/' . $eventImage->image_full);
            }

            if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $eventImage->image_full)) {
                Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $eventImage->image_full);
            }

            $eventImage->delete();

            $response = array(
                'status' => 1,
                'message'=> 'Image deleted successfully'
            );
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Invalid Id'
            );
        }
        return response()->json($response);
    }
}