<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use jeremykenedy\LaravelRoles\Models\Role;
use App\Models\UsersInformation;
use App\Models\UsersImage;
use App\Models\ShortlistedUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\EventReview;

class SoftDeletesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get Soft Deleted User.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public static function getDeletedUser($id)
    {
        $user = User::onlyTrashed()->onlySite()->hasUserRole()->where('id', $id)->get();
        if (count($user) != 1) {
            return redirect(config('app.url').'/users/deleted')->with('error', trans('usersmanagement.errorUserNotFound'));
        }

        return $user[0];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::onlyTrashed()->hasUserRole()->orderBy('deleted_at', 'desc')->get();
        //$users = User::whereNotNull('deleted_at')->orderBy('deleted_at', 'desc')->get();
//        $users = DB::table('users')->select('*')
//            ->whereNotNull('deleted_at')
//            ->orderBy('deleted_at', 'desc')->get();

        $roles = Role::all();

		$stats = new \stdClass;
		
		$stats->all_users         = User::hasUserRole()->onlySite()->count();
		$stats->banned_users      = User::where('banned', 1)->hasUserRole()->onlySite()->count();
		//$stats->featured_users 	  = User::where('featured', 1)->where('banned', 0)->where('deactivated', 0)->count();
		$stats->deactivated_users = User::where('deactivated', 1)->hasUserRole()->onlySite()->count();
		$stats->active_users 	  = User::where('banned', 0)->onlySite()->where('deactivated', 0)->hasUserRole()->count();
		$stats->deleted_users 	  = User::onlyTrashed()->onlySite()->hasUserRole()->count(); 
        return View('usersmanagement.show-deleted-users', compact('users', 'roles','stats'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = self::getDeletedUser($id);

        return view('usersmanagement.show-deleted-user')->withUser($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = self::getDeletedUser($id);
        $user->restore();

        return redirect(config('app.url').'/users/')->with('success', trans('usersmanagement.successRestore'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = self::getDeletedUser($id);
        // delete users images
        $userImgs = UsersImage::where('user_id', $id)->get();
        if($userImgs != null) {
            foreach($userImgs as $imgUsr) {
                if(Storage::disk('s3')->exists('vendor_photos/original/' . $imgUsr->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/original/' . $imgUsr->image_full);
                }
    
                if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $imgUsr->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $imgUsr->image_full);
                }
                $imgUsr->delete();
            }
        }
        // delete event reviews
        EventReview::where('user_id', $id)->delete();

        ShortlistedUser::where('from_user', $id)->delete();
        ShortlistedUser::where('to_user', $id)->delete();
        UsersInformation::where('user_id', $id)->delete();

        $user->forceDelete();

        return redirect(config('app.url').'/users/deleted')->with('success', trans('usersmanagement.successDestroy'));
    }
}
