<?php
namespace App\Http\Controllers;

use App\Models\VendorCategorySatelliteInformation;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\VendorsImage;
use App\Models\VendorVideo;
use App\Models\VendorFaq;
use App\Models\VendorCategory;
use App\Models\VendorFaqAnswer;
use App\Models\VendorCustomFaq;
use App\Models\VendorsInformation;
use App\Models\Review;
use App\Models\ShortlistedUser;
use App\Logic\Admin\VendorCategoryRepository;
use App\Logic\Common\VendorRepository;
use App\Logic\Admin\ReviewRepository;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Country;
use App\Models\City; 
use App\Models\State;
use Illuminate\Support\Facades\DB;
use App\Logic\Admin\CmsPagesRepository;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralEmail;

class VendorController extends Controller
{
    protected $ReviewRepository;
    protected $reviewPerPage = 5;
    protected $validationRequestPricing = [
        'email'                 => 'required|max:255|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        'event_date'            => 'required|min:10|max:10',
        'mobile_number'         => 'phone_valid',
        'password'              => 'required|min:6|max:30',
        'about_extra_needs'     => 'max:300'
    ];

    public function __construct(ReviewRepository $ReviewRepository)
    {
        $this->ReviewRepository = $ReviewRepository;
    }

    public function showVendorDetail($userId, VendorsImage $vendorImage, VendorVideo $vendorVideos, VendorCategoryRepository $vendorCatRepo)
    {
        try {
            $vendorInfo = VendorsInformation::where('company_slug', $userId)->first();
            $user_id = $vendorInfo->vendor_id;
            $vendor = User::where('id', $user_id)->hasVendorRole()->isActive()->first();
            if($vendor == null) {
                if(Auth::check()) {
                    if(User::where('id', $user_id)->hasVendorRole()->exists()) {
                        $loggedVendor = User::where('id', $user_id)->hasVendorRole()->first();
                        if(Auth::id() != $loggedVendor->id) {
                            return redirect()->route('profile-not-found');
                        } else {
                            $vendor = $loggedVendor;
                        }
                    }
                } else {
                    return redirect()->route('profile-not-found');
                }   
            }
        } catch (\Exception $e) {
            return redirect()->route('profile-not-found');
        }
        // check whether logged in user is same user's profile viewing
        $selfProfile = false;
        if(Auth::check()) {
            if($vendor->id == Auth::id()) {
                $selfProfile = true;
                if( $vendor->hasRole('vendor') && !($vendor->hasCoverPhoto()) ) {
                    return redirect()->route('upload-vendor-cover')
                    ->with([
                        'message' => 'Upload Vendor Cover Photo',
                        'status'  => 'danger',
                    ]);
                }
            }
        } else {
            $selfProfile = false;
        }

        // check logged in user
        $form_prepopulate = [];
        $allowShortList = false;
        if(Auth::check()) {
            $logged_user = User::where('id', Auth::id())->first();
            $form_prepopulate['first_name'] = $logged_user->first_name;
            $form_prepopulate['last_name'] = $logged_user->last_name;
            $form_prepopulate['mobile_number'] = $logged_user->mobile_number;
            $form_prepopulate['email'] = $logged_user->email;
            // check role and see the role
            if($logged_user->hasRole('unverified')) {
                $allowSendMessage = true;
                $allowShortList = true;
            } else {
                $allowSendMessage = false;
            }
        } else {
            $allowSendMessage = true;
        }
        if(Auth::check()) {
            unset($this->validationRequestPricing['email']);
            unset($this->validationRequestPricing['password']);
        }
        $validator = JsValidator::make($this->validationRequestPricing);

        // Images
        $where = array(
            'vendor_id' => $vendor->id,
            'image_type' => 'profile',
            'is_approved' => 'Y'
        );
        if(Auth::check()) {
            if(Auth::id() == $vendor->id) {
                unset($where['is_approved']);
            }
        }
        $vendorImages = $vendorImage->getWhere($where);
        // Videos
        $whereVid = array(
            'vendor_id' => $vendor->id,
            'is_approved' => 'Y'
        );
        if(Auth::check()) {
            if(Auth::id() == $vendor->id) {
                unset($whereVid['is_approved']);
            }
        }
        $videos = $vendorVideos->getWhere($whereVid);
        $videosCount = $vendorVideos->getWhereCount($whereVid);
        // FAQs
        $category = $vendor->vendors_information->vendor_category;
        $catInfo = $vendorCatRepo->getRecords($category);
        if($catInfo->has_admin_faqs ==  1) {
            $faqsObj = VendorFaq::where('categories', 'like', '%;s:'.strlen($category).':"'.$category.'";%');
            $faqs = $faqsObj->where('status', 1)
                        ->orderBy('order', 'ASC')
                        ->get(['id', 'question']);
            
            foreach($faqs as $faq) {
                try {
                    $faqAnswerObj = VendorFaqAnswer::where('vendor_id', $vendor->id)
                                ->where('faq_id', $faq->id)
                                ->first(['answer']);
                    $faq->answer = $faqAnswerObj->answer;
                } catch (\Exception $e) {
                    $faq->answer = '';
                }
            }
        } else {
            $faqs = [];
        }
        if($catInfo->has_custom_faqs ==  1) {
            $custom_faqs_obj = VendorCustomFaq::where('vendor_id', $vendor->id)
                    ->where('category_id', $category);
            if(Auth::check()) {
                if(Auth::id() != $vendor->id) {        
                    $custom_faqs_obj->where('is_approved', 'Y');
                }
            } else {
                $custom_faqs_obj->where('is_approved', 'Y');
            }
            $custom_faqs = $custom_faqs_obj->get(['question', 'answer']);
        } else {
            $custom_faqs = [];
        }

        // get reviews
        $reviews = $this->ReviewRepository->getReviews($vendor->id, $this->reviewPerPage, 'all');
        // shortlisted
        $shortListed = false;
        if(Auth::check()) {
            $userShortListed = User::where('id', Auth::id())->first();
            // check role and see the role
            if($userShortListed->hasRole('unverified')) {
                if(ShortlistedUser::where(['from_user' => Auth::id(), 'to_user' => $vendor->id])->exists()) {
                    $shortListed = true;
                }
            }
        }
        // sort by country
        /**
         * Get user's country and add sorting
         */
        if(Auth::check()) {
            // get user IP
            $userObj = User::where('id', Auth::id())->first(['id', 'signup_ip_address']);
            if($userObj->signup_ip_address != '') {
                $ip_address = $userObj->signup_ip_address;
                
            } else {
                $ip_address = request()->ip();
            }
        } else {
            // get guest IP
            $ip_address = request()->ip();
            //$ip_address = '43.250.158.190'; // for local
        }
        if($ip_address == '::1' || $ip_address == '127.0.0.1' || $ip_address == '192.168.0.104') {
            $ip_address = '43.250.158.190'; // for local
        }
        
        // get similar vendors
        $total_user_similar_cat = User::select('users.*')
                    ->getSimilarVendorResults($category)
                    ->where('id', '<>', $vendor->id)
                    ->count();
        if($total_user_similar_cat > 0) {
            $county_info = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$ip_address.'?key=RocDHDkeHbv0ask'));
            if(!empty($county_info)) {
                $countryInfo = Country::where('code', '=', $county_info->countryCode)->first(['id']);
                $get_venors_similar = User::select('users.*', DB::raw('(CASE WHEN country_id = ' . $countryInfo->id . ' THEN 1 ELSE 
                CASE WHEN country_id = 202 THEN 2 ELSE 
                    CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
                    END   
                END) AS newCountryid'))
                    ->getSimilarVendorResults($category)
                    ->where('id', '<>', $vendor->id)
                    ->orderBy('newCountryid','asc')
                    ->get();
            } else {
                $get_venors_similar = User::select('users.*')
                    ->getSimilarVendorResults($category)
                    ->where('id', '<>', $vendor->id)
                    ->orderBy('newCountryid','asc')
                    ->get();
            }
            
            if($total_user_similar_cat >= 4) {
                $similar_vendors_obj = $get_venors_similar->random(4);
            } else {
                $similar_vendors_obj = $get_venors_similar->random($total_user_similar_cat);
            }
            // prepare array
            $similar_random_vendors = [];
            $sm_v = 0;
            foreach($similar_vendors_obj as $similar_vendor_obj) {
                $similar_random_vendors[$sm_v]['vendor_image'] = $similar_vendor_obj->getVendorProfilePic();
                $similar_random_vendors[$sm_v]['vendor_profile'] = $similar_vendor_obj->getVendorProfileLink();
                $similar_random_vendors[$sm_v]['average_rating'] = $similar_vendor_obj->getAverageReviewRating();
                $profile_name_ven = '';
                if($similar_vendor_obj->checkProfileFieldApproved('company_name')) {
                    $profile_name_ven .= $similar_vendor_obj->vendors_information->company_name; 
                }  
                if($similar_vendor_obj->checkProfileFieldApproved('company_name') && $similar_vendor_obj->checkProfileFieldApproved('first_name')) {
                    $similar_random_vendors[$sm_v]['vendor_title'] = $profile_name_ven;
                } else {
                    $similar_random_vendors[$sm_v]['vendor_title'] = '';
                }
                $similar_random_vendors[$sm_v]['city'] = $similar_vendor_obj->city->name;
                $similar_random_vendors[$sm_v]['country'] = $similar_vendor_obj->country->name;
                $sm_v++;
            }
        } else {
            // no similar vendors
            $similar_random_vendors = [];
        }
        // intialize meta fields
        // [Vendor Company Name] [Vendor City Name] [Vendor Country Name] | Wedding Vendor
        $meta_fields['title'] = '';
        
        $meta_fields['keyword'] = '';
        if($vendor->checkProfileFieldApproved('company_name')) {
            $meta_fields['title'] .= $vendor->vendors_information->company_name . ' ';
        }
        $meta_fields['title'] .= $vendor->city->name .' ';
        $meta_fields['title'] .= $vendor->country->name .' ';
        $meta_fields['title'] .= '| Wedding Vendor';
        
        $meta_fields['description'] = '';
        //[Vendor Address] [Vendor Contact Number], [Vendor "About us" (html strip)]
        if($vendor->vendors_information->display_address == 1 && $vendor->checkProfileFieldApproved('address')) {
            $meta_fields['description'] .= $vendor->address;
        }
        if($vendor->vendors_information->display_business_number == 1 &&$vendor->checkProfileFieldApproved('mobile_number')) {
            $meta_fields['description'] .= ' '.$vendor->mobile_number;
        }
        if($vendor->checkProfileFieldApproved('business_information')) {
            $meta_fields['description'] .= ', '.showReadMoreApi(strip_tags($vendor->vendors_information->business_information), 100, '');
        }
        if($vendor->checkProfileFieldApproved('meta_keywords')) {
            $meta_fields['keyword'] = $vendor->vendors_information->meta_keywords;
        }
        return view('pages.user.vendor-detail', [
            'metafields' => $meta_fields,
            'catInfo' => $catInfo,
            'selfProfile' => $selfProfile,
            'allowSendMessage' => $allowSendMessage,
            'allowShortList' => $allowShortList,
            'vendorImages' => $vendorImages,
            'vendorObj' => $vendor,
            'videos' => $videos,
            'videosCount' => $videosCount,
            'faqs' => $faqs,
            'customFaqs' => $custom_faqs,
            'form_prepopulate' => $form_prepopulate,
            'reviews' => $reviews,
            'review_page_page' => $this->reviewPerPage,
            'shortListed' => $shortListed,
            'similarVendors' => $similar_random_vendors,
            'imgLimit' => 9, // limit per click for vendor gallery images.
            'validator' => $validator
        ]);
    }

    public function showSearchVendors($category = '', Request $request, VendorRepository $vendorRepo, VendorCategoryRepository $vendorCatRepo, CmsPagesRepository $cmsPagesService)
    {
        $site_id = config('constants.site_id');
        $city_zip = $request->has('city') ? $request->input('city') : "";
        $cat_information = null;
        if($category != '' && $category != config('constants.search_vendors_default_page_slug')) {
            if(VendorCategory::where('slug', $category)->getVendorByType('wedding')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->where('name', '<>', 'Event Organizer')->exists()) {
                $cat_information = VendorCategory::where('slug', $category)->getVendorByType('wedding')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->first(['id', 'name', 'slug']);
                $category_name = $cat_information->name;
            } else {
                $category_name = "";
            }
        } else {
            $category_name = $request->has('category') ? $request->input('category') : "";
            $cat_information = VendorCategory::where('name', $category_name)->getVendorByType('wedding')->where('name', '<>', 'Event Organizer')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->first(['id', 'name', 'slug']);
        }
        
        $location_type = $request->has('location_type') ? $request->input('location_type') : "";
        $location_id = $request->has('location_id') ? $request->input('location_id') : "";
        $categories = VendorCategory::where('status', 1)->where('name', '<>', 'Event Organizer')->getVendorByType('wedding')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->get(['id', 'name', 'slug']);
        // check logged in user
        $form_prepopulate = [];
        if(Auth::check()) {
            $logged_user = User::where('id', Auth::id())->first();
            $form_prepopulate['first_name'] = $logged_user->first_name;
            $form_prepopulate['last_name'] = $logged_user->last_name;
            $form_prepopulate['mobile_number'] = $logged_user->mobile_number;
            $form_prepopulate['email'] = $logged_user->email;
            // check role and see the role
            if($logged_user->hasRole('unverified')) {
                $allowSendMessage = true;
            } else {
                $allowSendMessage = false;
            }
        } else {
            $allowSendMessage = true;
        }

        $categories = $vendorCatRepo->getStatusRecords(1)->getVendorByType('wedding')->where('name', '<>', 'Event Organizer')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->orderBy('name', 'ASC')->get(['id', 'name', 'slug']);
        $validator = JsValidator::make($this->validationRequestPricing);

        $search_preference['category_name'] = $category_name;
        $search_preference['city_zip'] = $city_zip;
        $search_preference['location_type'] = $location_type;
        $search_preference['location_id'] = $location_id;
        // check for additional categories from vendor showcase
        if(VendorCategory::where('name', $category_name)->getVendorByType('wedding')->where('name', '<>', 'Event Organizer')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->exists()) {
            $getAddcat = VendorCategory::where('name', $category_name)->where('name', '<>', 'Event Organizer')->getVendorByType('wedding')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->first(['id', 'name']);
            $search_preference['additional_cat'] = $getAddcat->id;
        } else {
            $search_preference['additional_cat'] = '';
        }
        
        $per_page = 6;

        /**
         * Get user's country and add sorting
         */
        if(Auth::check()) {
            // get user IP
            $userObj = User::where('id', Auth::id())->first(['id', 'signup_ip_address']);
            if($userObj->signup_ip_address != '') {
                $ip_address = $userObj->signup_ip_address;
                
            } else {
                $ip_address = request()->ip();
            }
        } else {
            // get guest IP
            $ip_address = request()->ip();
            //$ip_address = '43.250.158.190'; // for local
        }
        if($ip_address == '::1' || $ip_address == '127.0.0.1' || $ip_address == '192.168.0.104') {
            $ip_address = '43.250.158.190'; // for local
        }

        $county_info = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$ip_address.'?key=RocDHDkeHbv0ask'));
        //dd($county_info);
        if(!empty($county_info) && $city_zip == '') {
            $countryInfo = Country::where('code', '=', $county_info->countryCode)->first(['id']);
            $results_vendors = User::select('users.*', DB::raw('(CASE WHEN country_id = ' . $countryInfo->id . ' THEN 1 ELSE 
            CASE WHEN country_id = 202 THEN 2 ELSE 
                CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
            END   
            END) AS newCountryid'))
                    ->getVendorResults($search_preference)
                    ->orderBy('newCountryid','asc')
                    ->orderBy('users.is_paid', 'DESC')
                    ->latest()
                    ->paginate($per_page);
        } else {
            $results_vendors = User::select('users.*')
                    ->getVendorResults($search_preference)
                    ->orderBy('users.is_paid', 'DESC')
                    ->latest()
                    ->paginate($per_page);
        }

        //************************ added by Bindu in order to add meta tags for search page, based on selected category *****************************//
        $cmsPageInfo = $cmsPagesService->getPageRec('search-vendor');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['heading']= $cmsPageInfo->page_description;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;
        $meta_fields['cat_description'] = $cmsPageInfo->meta_description;
        if($search_preference['additional_cat'] != '') {
            $meta_fields['cat_description'] = $cmsPageInfo->meta_description;
        } else {
            if($category_name != "") {
                $meta_fields['cat_description'] = '';
            }
        }
        if($category_name != '') {
            /*vendors_category_satellites_information: id, vendor_category_id, satellite_id, title, keywords, description, meta_description, icon_image*/
            $vendor_type_id = config('constants.vendor_type_id.wedding');
            $meta_values = VendorCategorySatelliteInformation::where('satellite_id',config('constants.site_id'))
                ->where('vendor_category_id', function($query) use($category_name, $vendor_type_id) {
                    $query = $query->select('id')->from('vendors_categories')->where('name', '<>', 'Event Organizer')->where('name', 'like', '%'.$category_name.'%')->where('vendor_type', $vendor_type_id);
                    return $query;
            })->first();
            if(!is_null($meta_values)) {
                $meta_fields['title']=$meta_values->title;
                $meta_fields['keyword']=$meta_values->keywords;
                $meta_fields['description']=$meta_values->meta_description;
                $meta_fields['cat_description'] = $meta_values->description;
            }
        }
        //************************ END: added by Bindu in order to add meta tags for search page, based on selected category ************************//
        
        if($request->ajax()) {
            if($results_vendors->nextPageUrl() != "") {
                $nextPageUrl = $results_vendors->nextPageUrl().'&category='.urlencode($category_name).'&city='.urlencode($city_zip).'&location_type='.$location_type.'&location_id='.$location_id;
            } else {
                $nextPageUrl = '';
            }
            if($cat_information != null) {
                $category_slug = $cat_information->slug;
            } else {
                $category_slug = '';
            }
            return [
                'vendors' => view('pages.user.ajax.show-vendors', [
                    'vendors' => $results_vendors, 
                    'form_prepopulate' => $form_prepopulate, 
                    'allowSendMessage' => $allowSendMessage]
                )->render(),
                'next_page' => $nextPageUrl,
                'total_records' => $results_vendors->total(),
                'category_name' => urldecode($category_name),
                'category_slug' => $category_slug,
                'category_description' => $meta_fields['cat_description'],
                'location_name' => urldecode($city_zip),
                'metafields' => $meta_fields
            ];
        }
        
        return view('pages.user.search-vendor', [
            'vendors' => $results_vendors,
            'metafields' => $meta_fields,
            'form_prepopulate' => $form_prepopulate,
            'allowSendMessage' => $allowSendMessage,
            'categories' => $categories,
            'category_name' => $category_name,
            'city_zip' => $city_zip,
            'location_type' => $location_type,
            'location_id' => $location_id,
            'validator' => $validator
        ]);
    }

    public function getSearchAutoSuggestions(Request $request) 
    {
        $key = $request->has('key') ? $request->input('key') : "";
        $site_id = config('constants.site_id');
        $categories = VendorCategory::where('status', 1)->getRecordsSearch($key)->where('name', '<>', 'Event Organizer')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->getVendorByType('wedding')->orderBy('name', 'ASC')->get(['id', 'name', 'slug']);

        // $vendors = User::where('first_name', 'like', '%'.$key.'%')
        //           ->orWhere('last_name', 'like', '%'.$key.'%')
        //           ->isActive()
        //           ->hasVendorRole()
        //           ->get(['id', 'first_name', 'last_name']);
        $output = '';
        if($categories->count() > 0) {
            $output .= '<ul class="dropdown-menu category-vendor-autosuggestion" style="display:block; position:absolute; max-height: 200px; overflow-y: scroll;width:100%;">';
            if($categories->count() > 0) {
                $output .= '<li><strong style="padding: 3px 20px;">Category</strong></li>';
                foreach($categories as $category)
                {
                    $output .= '
                    <li class="category"><a href="javascript:void(0);">'.$category->name.'</a></li>
                    ';
                }
            }
            // if($vendors->count() > 0) {
            //     $output .= '<li><strong style="padding: 3px 20px;">Vendor</strong></li>';
            //     foreach($vendors as $vendor)
            //     {
            //         $output .= '
            //         <li class=""><a href="'.$vendor->getVendorProfileLink().'">'.$vendor->first_name.' '.$vendor->last_name.'</a></li>
            //         ';
            //     }
            // }
            
            // $output .= '</ul>';
        }
        echo $output;
    }

    public function getLocaltionAutoSuggestions(Request $request) 
    {
        $key = $request->has('key') ? $request->input('key') : "";

        if($key == "") {
            return '';
        }

        $countries = Country::where('name', 'like', '%'.$key.'%')->get(['id', 'name']);
        $states = State::where('name', 'like', '%'.$key.'%')->get(['id', 'name']);
        $cities = City::where('name', 'like', '%'.$key.'%')->get(['id', 'name']);

        $output = '';
        if($countries->count() > 0 || $states->count() > 0 || $cities->count() > 0) {
            $output .= '<ul class="dropdown-menu location-vendor-autosuggestion" style="display:block; position:absolute; max-height: 200px; overflow-y: scroll;width:100%;">';
            if($cities->count() > 0) {
                $output .= '<li><strong style="padding: 3px 20px;">City</strong></li>';
                foreach($cities as $city)
                {
                    $output .= '
                    <li><a href="javascript:void(0);" onclick="javascript:SearchBySpec(\'city\', '.$city->id.');">'.$city->name.'</a></li>
                    ';
                }
            }
            if($states->count() > 0) {
                $output .= '<li><strong style="padding: 3px 20px;">State</strong></li>';
                foreach($states as $state)
                {
                    $output .= '
                    <li><a href="javascript:void(0);" onclick="javascript:SearchBySpec(\'state\', '.$state->id.');">'.$state->name.'</a></li>
                    ';
                }
            }
            if($countries->count() > 0) {
                $output .= '<li><strong style="padding: 3px 20px;">Country</strong></li>';
                foreach($countries as $country)
                {
                    $output .= '
                    <li><a href="javascript:void(0);" onclick="javascript:SearchBySpec(\'country\', '.$country->id.');">'.$country->name.'</a></li>
                    ';
                }
            }
            
            $output .= '</ul>';
        }
        echo $output;
    }

    public function getVendorReviewAjax(Request $request)
    {
        $vendor_id = base64_decode($request->input('vendor_id'));
        $per_rate = $request->has('per_rate') ? $request->input('per_rate') : 'all';
        // get reviews
        $reviews = $this->ReviewRepository->getReviews($vendor_id, $this->reviewPerPage, $per_rate);
        if($request->ajax()) {
            return [
                'reviews' => view('pages.user.ajax.show-reviews', ['reviews' => $reviews])->render(),
                'next_page' => $request->input('page')+1,
                'next_url' => $reviews['next_page'],
                'per_rate' => $per_rate
            ];
        }
    }

    // short list vendor
    public function shortListVendor(Request $request)
    {
        $vendor_id = base64_decode($request->input('vendor_id'));
        $shortlist = $request->has('shortlist') ? $request->input('shortlist') : 0;
        if(User::where('id', $vendor_id)->hasVendorRole()->exists() == false) {
            return response()->json([
                'status' => 0,
                'message' => 'User is not a vendor.'
            ]);
        }
        $user = Auth::user();
        if(User::where('id', $user->id)->hasUserRole()->exists() == false) {
            return response()->json([
                'status' => 0,
                'message' => 'You are not having user role.'
            ]);
        }
        // try to save shortlist status
        if($shortlist != '') {
            if(ShortlistedUser::where(['from_user' => Auth::id(), 'to_user' => $vendor_id])->exists()) {
                if($shortlist == 1) {
                    $shortListObj = ShortlistedUser::where(['from_user' => Auth::id(), 'to_user' => $vendor_id])->first();
                    $shortListObj->from_user = $user->id;
                    $shortListObj->to_user = $vendor_id;
                    $shortListObj->save();
                    $vendorDetail = User::where('id', $vendor_id)->hasVendorRole()->first();
                    $mail_req_arry = [
                        'mail_name' => 'Shortlist Vendor',
                        'mail_replace_vars' => [
                            '[%USER_FIRST_NAME%]' => $vendorDetail->first_name,
                            '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                            '[%WEB_NAME%]' => getGeneralSiteSetting('site_name'),
                            '[%WEB_LOGIN_URL%]' => route('login')
                        ],
                        'unsubscribe_link' => $unsubscribe_link
                    ];
                    Mail::to($vendorDetail->email)->send(new GeneralEmail($mail_req_arry));
                } else {
                    ShortlistedUser::where(['from_user' => Auth::id(), 'to_user' => $vendor_id])->delete();
                }
            } else {
                ShortlistedUser::create(
                    [
                        'from_user' => $user->id, 
                        'to_user' => $vendor_id,
                        'finalized' => '0'
                    ]
                );
            }
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'Shortlist can not be blank.'
            ]);
        }
        return response()->json([
            'status' => 1,
            'message' => 'Shortlisted'
        ]);
    }
    
    // short list finalize vendor
    public function shortListFinalizeVendor(Request $request)
    {
        $vendor_id = base64_decode($request->input('vendor_id'));
        $finalize = $request->has('finalize') ? $request->input('finalize') : 0;
        if(User::where('id', $vendor_id)->hasVendorRole()->exists() == false) {
            return response()->json([
                'status' => 0,
                'message' => 'User is not a vendor.'
            ]);
        }
        $user = Auth::user();
        if(User::where('id', $user->id)->hasUserRole()->exists() == false) {
            return response()->json([
                'status' => 0,
                'message' => 'You are not having user role.'
            ]);
        }
        // try to save shortlist status
        if($finalize != '') {
            if(ShortlistedUser::where(['from_user' => Auth::id(), 'to_user' => $vendor_id])->exists()) {
                if($finalize == 1) {
                    $shortListObj = ShortlistedUser::where(['from_user' => Auth::id(), 'to_user' => $vendor_id])->first();
                    $shortListObj->from_user = $user->id;
                    $shortListObj->to_user = $vendor_id;
                    $shortListObj->finalized = '1';
                    $shortListObj->save();
                    $vendorDetail = User::where('id', $vendor_id)->hasVendorRole()->first();
                    $mail_req_arry = [
                        'mail_name' => 'Finalized Vendor',
                        'mail_replace_vars' => [
                            '[%USER_FIRST_NAME%]' => $vendorDetail->first_name,
                            '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                            '[%WEB_NAME%]' => getGeneralSiteSetting('site_name'),
                            '[%WEB_LOGIN_URL%]' => route('login')
                        ],
                        'unsubscribe_link' => $unsubscribe_link
                    ];
                    Mail::to($vendorDetail->email)->send(new GeneralEmail($mail_req_arry));
                }
            }
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'Finalize can not be blank.'
            ]);
        }
        return response()->json([
            'status' => 1,
            'message' => 'Finalized'
        ]);
    }

    // show vendor reviews
    public function showVendorReviews($vendorId, Request $request)
    {
        try {
            $vendor = User::withTrashed()->withEncryptedId($vendorId)->first();
            if($vendor == null) {
                return abort(404);    
            }
        } catch (\Exception $e) {
            return abort(404);
        }
        $vendor_id = base64_decode($vendorId);
        $per_rate = $request->has('rate') ? $request->input('rate') : 'all';
        $reviews = $this->ReviewRepository->getReviews($vendor_id, $this->reviewPerPage, $per_rate);

        return view('pages.user.reviews', [
            'reviews' => $reviews,
            'vendorObj' => $vendor,
            'review_per_page' => $this->reviewPerPage,
            'per_rate' => $per_rate
        ]);
    }
}