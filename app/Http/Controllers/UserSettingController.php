<?php

namespace App\Http\Controllers;

use App\Logic\Common\LocationRepository;
use App\Models\MembershipPlans;
use App\Models\PaidMembers;
use App\Models\UsersPreference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Logic\Common\UserInformationOptionsRepository;
use JsValidator;
use App\Models\UsersInterest;
use App\Models\UsersImage;
use App\Models\Language;
use Illuminate\Support\Facades\View;
use App\Models\Profession;
use App\Models\Community;
use App\Models\Subcaste;

class UserSettingController extends Controller
{
    //
    protected $locationRepository;
    protected $userInformationOptionsRepository;
    protected $validationRules = [
      'age_from'=>'required',
      'age_to'=>'required',
      'city'=>'required'
    ];
    protected $backendValidation = [
        'age_from'=>'required',
        'age_to'=>'required|gt:age_from',
        'city'=>'required'
    ];


    protected $profileValidationRules = [
        //'name'                  => 'required|max:50|unique:users',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        //'email'                 => 'required|max:255|unique:users|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
        'day'                   => 'required',
        'month'                 => 'required',
        'year'                  => 'required',
        'mobile_number'         => 'required|numeric|phone',
        'height'   				=> 'required',
        'phone_country_code'    => 'required',
        'religion'   			=> 'required',
        'sub_cast'   			=> 'required',
        'sect'   				=> 'required',
        'country'               => 'required',
        'city'                  => 'required',
        'state'                 => 'required',
        'zipcode'   			=> 'required | min:5',
        'education'             => 'required',
        'food'                  => 'required',
        'drink'                 => 'required',
        'smoke'                 => 'required',
        'about'                 => 'required|min:50',
        'looking_for'           => 'required|min:50',
        'marital_status'   		=> 'required',
        'profession'   			=> 'required',
        'weight'				=> 'required',
        'weight_measure'		=> 'required'
    ];

    public function __construct(LocationRepository $locationRepository, UserInformationOptionsRepository $userInformationOptionsRepository)
    {
        $this->locationRepository = $locationRepository;
        $this->userInformationOptionsRepository = $userInformationOptionsRepository;
        View::share('countries', $locationRepository->getCountries());
    }

    public function showSettingPage(){
        $user = Auth::user();
        $validator = JsValidator::make($this->validationRules);
        $cities = $this->locationRepository->getCountryCities($user->country_id);
        $current_preferences = $user->users_preferences();
        return view('pages.user.setting')->with(
            [
                'cities'=>$cities,
                'current_preferences'=>$current_preferences,
                'validator'=>$validator,
                'user'=>$user
            ]
        );
    }

    public function savePartnerPreferences(Request $request){
       $user = Auth::user();
        $validatedData =  $request->validate(
            $this->backendValidation,
            [
                'age_from.required'=>'Age from is required',
                'age_to.required'=>'Age to is required',
                'age_to.gt'=>'Age to must be greater than age from',
                'city.required'=>'City is required'
            ]
        );
       if($user->users_preferences()){
            $pref = $user->users_preferences();
            $pref->age_from = $request->input('age_from');
            $pref->age_to = $request->input('age_to');
            $pref->country_id = $user->country_id;
            $pref->city_id = $request->input('city');
            $pref->save();
       }
       else{
           UsersPreference::create([
               'age_from'=>$request->input('age_from'),
               'age_to'=>$request->input('age_to'),
               'country_id'=>$user->country_id,
               'city_id'=>$request->input('city'),
               'user_id'=>$user->id
           ]);
       }
       return redirect()->back()->withSuccess(config('constants.partner_preference.save_message'));
    }

    public function saveAlertSettings(Request $request){
        $user = Auth::user();
        $user_information = $user->get_user_information();
        $action = $request->input('action');
        $value = $request->input('value');

        if($action == 'activity'){
            $user_information->activity_notification = $value;
            $user_information->save();
        }
        elseif($action == 'instant_alert'){
            $user_information->instant_alert_notification = $value;
            $user_information->save();
        }
        $output['status'] = 1;
        $output['msg'] = "Settings saved successfully";
        return response()->json($output);
    }

    public function showEditProfileForm(){
        $user = Auth::user();
        $userInformation = $user->get_user_information();
        $interests = $user->get_interests();
        $userInterests = array();
        foreach($interests as $interest){
            $userInterests[] = $interest->id;
        }

        $validation = [
            'name'                  => 'required|max:20|unique:users',
            'day'                   => 'required',
            'month'                 => 'required',
            'year'                  => 'required',
            'country'               => 'required',
            'city'                  =>  'required',
            'phone_country_code'    => 'required',
            'phone_number'          => 'required|numeric',
            'education'             => 'required',
            'profession'            => 'required',
            'food'                  => 'required',
            'drink'                 => 'required',
            'smoke'                 => 'required',
            'interest'              => 'required|array|min:1',
            'about'                 => 'required|min:50'
        ];

        $validator = JsValidator::make($validation);

        return view('pages.user.edit-profile')->with(
            [
                'user'=>$user,
                'userInformation'=>$userInformation,
                'locationRepo'=>$this->locationRepository,
                'userInfoRepo'=>$this->userInformationOptionsRepository,
                'userInterests'=>$userInterests,
                'validator'=>$validator
            ]
        );
    }

    public function getSelfProfile() {
        $user = Auth::user();
        $interests = $user->get_interests();
        $userInterests = array();
        foreach($interests as $interest){
            $userInterests[] = $interest->id;
        }

        return view('pages.user.self-profile')->with(
            [
                'userProfile'=>$user,
                'professions' => Profession::all(),
                'religions'	  => config('constants.religions'),
                'communities' => Community::all(),
                'subcastes' => Subcaste::all(),
                'heights'   => config('constants.height'),
                'gender'   => config('constants.gender'),
                'marital_status'    => config('constants.marital_status'),
                'educationOptions' => $this->userInformationOptionsRepository->getEducationOptions(),
                'interestOptions'  => $this->userInformationOptionsRepository->getInterestOptions(),
                'professionOptions' => $this->userInformationOptionsRepository->getProfessionOptions(),
                'do_you_pray'   => config('constants.do_you_pray'),
                'born_reverted'   => config('constants.born_reverted'),
                'food' => config('constants.food'),
                'drink' => config('constants.drink'),
                'smoke' => config('constants.smoke'),
                'locationRepo'=>$this->locationRepository,
                'userInfoRepo'=>$this->userInformationOptionsRepository,
                'userInterests'=>$userInterests,
            ]
        );
    }


    public function getProfile(LocationRepository $locationRepo){
        $user = Auth::user();
       // $userInformation = $user->get_user_information();
        $interests = $user->get_interests();
        $userInterests = array();
        foreach($interests as $interest){
            $userInterests[] = $interest->id;
        }
        $validator = JsValidator::make($this->profileValidationRules);


        return view('pages.user.my-profile')->with(
            [
                'userProfile'=>$user,
                'validator'=>$validator,
                'professions' => Profession::all(),
                'religions'	  => config('constants.religions'),
                'communities' => Community::all(),
                'subcastes' => Subcaste::all(),
                'heights'   => config('constants.height'),
                'gender'   => config('constants.gender'),
                'marital_status'    => config('constants.marital_status'),
                'educationOptions' => $this->userInformationOptionsRepository->getEducationOptions(),
                'interestOptions'  => $this->userInformationOptionsRepository->getInterestOptions(),
                'professionOptions' => $this->userInformationOptionsRepository->getProfessionOptions(),
                'do_you_pray'   => config('constants.do_you_pray'),
                'born_reverted'   => config('constants.born_reverted'),
                'food' => config('constants.food'),
                'drink' => config('constants.drink'),
                'smoke' => config('constants.smoke'),

                //'userInformation'=>$userInformation,
                'locationRepo'=>$this->locationRepository,
                'userInfoRepo'=>$this->userInformationOptionsRepository,
                'userInterests'=>$userInterests,
				//'countryList'  => $locationRepo->getCountries()
            ]
        );
    }

    public function editHobbies() {
        $user = Auth::user();		
		$hobbies_interests['favourite_reads']=getHobbies('favourite_reads');
        $hobbies_interests['favourite_music']=getHobbies('favourite_music');
        $hobbies_interests['interests']=getHobbies('interests');
        $hobbies_interests['hobbies']=getHobbies('hobbies');
        $hobbies_interests['sports']=getHobbies('sports');
        $hobbies_interests['preferred_movies']=getHobbies('preferred_movies');
        return view('pages.user.my-hobbies')->with(
            [
				'userProfile'=>$user,
				'language'=>Language::all(),
				//'userInformation'=>$userInformation,			
				'hobbies_interests' => $hobbies_interests
            ]
        );
    }
	
    public function editPartnerPreference() {
		 
        $user = Auth::user();
        $ppr=UsersPreference::where("user_id",$user->id)->first();

        return view('pages.user.partner-preference')->with(
            [
				'userProfile'=>$user,
				'language'=>Language::all(),
				'locationRepo'=>$this->locationRepository,
                'currency'  => getCurrency(), //config('constants.currency'),
                'religions' => config('constants.religions'),
				'ppr'=>UsersPreference::where("user_id",$user->id)->first()
            ]
        );
    }
	
	
    public function showManagePhotosPage(UsersImage $userImage){
        $user = Auth::user();
        $where = array(
            'user_id' => $user->id
        );
        $existingImages = $userImage->getWhere($where);
        $existingImagesCount = $userImage->getWhereCount($where);
        return view('pages.user.manage-photos')->with(
            [
                'user'=>$user,
                'existingImages' => $existingImages,
                'existingImagesCount' => $existingImagesCount
            ]
        );
    }

    public function showMyPhotosPage(UsersImage $userImage){
        $user = Auth::user();
        $publicwhere = array(
            'user_id' => $user->id,
            'image_type' => 'public'
        );
        $existingPublicImages = $userImage->getWhere($publicwhere);

        $privatewhere = array(
            'user_id' => $user->id,
            'image_type' => 'private'
        );
        $existingPrivateImages = $userImage->getWhere($privatewhere);

        $getMembershipStatus=$user->currentMembershipStatus();

        $status=$getMembershipStatus['status'];
        $public_photo_allowed=$getMembershipStatus['public_photo_allowed'];
        $private_photo_allowed=$getMembershipStatus['private_photo_allowed'];

        return view('pages.user.my-photos')->with(
            [
                'user'=>$user,
                'existingPublicImages' => $existingPublicImages,
                'existingPublicImagesCount' => count($existingPublicImages),
                'existingPrivateImages' => $existingPrivateImages,
                'existingPrivateImagesCount' => count($existingPrivateImages),
                'public_photo_allowed' => $public_photo_allowed,
                'private_photo_allowed' => $private_photo_allowed,
                'status' => $status
            ]
        );
    }

    public function updateProfile(Request $request){
        $user = Auth::user();
        $data = $request->input();
        $userInformation = $user->get_user_information();

        $user->date_of_birth = date_create($data['year']."-".$data['month']."-".$data['day']);
        $user->country_id = $data['country'];
        $user->city_id = $data['city'];
        $user->save();
        $userInformation->drink = $data['drink'];
        $userInformation->smoke = $data['smoke'];
        $userInformation->food = $data['food'];
        $userInformation->education_id = $data['education'];
        $userInformation->profession_id = $data['profession'];
        $userInformation->about = $data['about'];
        $userInformation->country_code = $data['phone_country_code'];
        $userInformation->mobile_number = $data['phone_number'];
        $userInformation->save();
        UsersInterest::where('user_id', $user->id)->delete();
        foreach ($request->input('interest') as $interest){
            UsersInterest::create(
                [
                    'user_id'=> $user->id,
                    'interest_id'=>$interest
                ]
            );
        }
        return redirect()->back()->withSuccess('Profile updated successfully');
    }
}
