<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VendorType;
use App\Logic\Admin\VendorCategoryRepository;
use App\Logic\Admin\SatelliteRepository;

use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use phpDocumentor\Reflection\Types\Integer;

class VendorCategoryController extends Controller {
    protected $VendorCategoryRepository;
    protected $SatelliteRepository;
    protected $validationRules = [
        'name'                  => 'required',
        'vendor_type'           => 'required',
        'display_in_footer'     => 'required',
        'has_admin_faqs'        => 'required',
        'has_custom_faqs'       => 'required',
        'status'                => 'required',
        'satellites'            => 'required'
    ];

    protected $iconExtensions = ['jpg','png','jpeg','gif','bmp'];

    public function __construct(VendorCategoryRepository $VendorCategoryRepository, SatelliteRepository $SatelliteRepository)
    {
        $this->VendorCategoryRepository = $VendorCategoryRepository;
        $this->SatelliteRepository = $SatelliteRepository;

        // get vendor types
        View::share('vendor_types', VendorType::get(['id', 'name']));
    }

    //Backend Functions
    public function get(Request $request) 
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $vendor_type    = $request->has('vendor_type') ? $request->input('vendor_type') : '';
        $status         = $request->has('status') ? $request->input('status') : "";
        $search         = $request->has('search') ? $request->input('search') : "";
        $records        = $this->VendorCategoryRepository->getRecords();

        if($search != "") {
            $records = $this->VendorCategoryRepository->getSearchRecords($records, $search);
        }

        if($status != "") {
            $records  = $records->where('status', '=', $status);
        }
        if($vendor_type != "") {
            $records  = $records->where('vendor_type', '=', $vendor_type);
        }
        $records = $records->latest()
                ->paginate($per_page);      
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $active_counts      = $this->VendorCategoryRepository->getStatusRecords(1)
                            ->count();
        $inactive_counts    = $this->VendorCategoryRepository->getStatusRecords(0)
                            ->count();
        $all_counts         = $this->VendorCategoryRepository->getRecords()
                            ->count();


        return view('pages.admin.vendor-category.list')->with(
            [
                'vendor_categories'     => $records,
                'page'                  => $page,
                'per_page'              => $per_page,
                'vendor_type'           => $vendor_type,
                'status'                => $status,
                'search'                => $search,
                'active_records'        => $active_counts,
                'inactive_records'      => $inactive_counts,
                'all_records'           => $all_counts
            ]
        );
    }

    /**
     * Handle bulk action of list
     */
    public function handleBulkAction(Request $request)
    {
        $action = $request->has('action') ? $request->input('action') : "";
        $ids    = $request->has('ids') ? $request->input('ids') : "";
        if( $action != "" && ($action == "delete" || $action == "1" || $action == "0") && !empty($ids) ) {
            $records = $this->VendorCategoryRepository->getRecords($ids);
            if($action == "1" || $action == "0") {
                try {
                    foreach($records as $record) {
                        $record->status = $action;
                        $record->save();
                    }
                    return redirect()->back()->withSuccess('Records(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating Records(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($records as $record) {
                        $this->VendorCategoryRepository->deleteRecord($record);
                    }
                    return redirect()->back()->withSuccess('Record(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting Record(s), please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    public function add() 
    {
        // get satellites
        $satellites = $this->SatelliteRepository
                                ->getRecords()
                                ->where('status', '=', 1)
                                ->get();
        $validator = JsValidator::make($this->validationRules);

        return view('pages.admin.vendor-category.add', [
                'validator' => $validator,
                'satellites' => $satellites
            ]
        );
    }

    public function edit($id) 
    {
        $satellites = $this->SatelliteRepository
                                ->getRecords()
                                ->where('status', '=', 1)
                                ->get();
        
        $validator = JsValidator::make($this->validationRules);
        $record = $this->VendorCategoryRepository->getRecords($id);
        if($record) {
            return view('pages.admin.vendor-category.edit', [
                    'validator' => $validator,
                    'record'    => $record,
                    'satellites' => $satellites
                ]
            );
        } else {
            return abort(404);
        }
    }

    public function handleAdd(Request $request) 
    {
        $validator = Validator::make($request->all(), $this->validationRules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        // validate the fields with satellites
        if(!empty($this->customErrorSatelliteSeo($request))) {
            return back()->withErrors($this->customErrorSatelliteSeo($request))->withInput();
        }
        
        $status = $this->VendorCategoryRepository->addRecord($request);
        if($status['status'] == '1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }
    }

    public function handleEdit(Request $request) 
    {
        $validator = Validator::make($request->all(), $this->validationRules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        // validate the fields with satellites
        if(!empty($this->customErrorSatelliteSeo($request))) {
            return back()->withErrors($this->customErrorSatelliteSeo($request))->withInput();
        }
        $status = $this->VendorCategoryRepository->updateRecord($request);
        if($status['status']=='1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }
    }

    public function view($id)
    {
        $record = $this->VendorCategoryRepository->getRecords($id);
        if($record) {
            return view('pages.admin.vendor-category.view', [
                    'record'    => $record
                ]
            );
        } else {
            return abort(404);
        } 
    }

    protected function customErrorSatelliteSeo($request) {
        $errors = [];
        $store_satellite_ids = $request->has('store_satellite_ids') ? $request->input('store_satellite_ids') : "";
        if($store_satellite_ids == "") {
            $errors[] = "Satellites are not selected.";
        } else {
            $sat_id_arr = explode(",", $store_satellite_ids);
            $total_sats = count($sat_id_arr);
            for($i=0; $i<$total_sats; $i++ ) {
                $i_inc = $i+1;
                if($request->input('title_'.$sat_id_arr[$i]) == "") {
                    $errors[] = "Title {$i_inc} cann't be blank.";
                }
                if($request->input('keywords_'.$sat_id_arr[$i]) == "") {
                    $errors[] = "Keyword {$i_inc} cann't be blank.";
                }
                if($request->input('description_'.$sat_id_arr[$i]) == "") {
                    $errors[] = "Description {$i_inc} cann't be blank.";
                }
                if($request->input('meta_description_'.$sat_id_arr[$i]) == "") {
                    $errors[] = "Meta Description {$i_inc} cann't be blank.";
                }
                if($request->hasFile('icon_image_'.$sat_id_arr[$i])) {
                    if(!in_array($request->file('icon_image_'.$sat_id_arr[$i])->getClientOriginalExtension(), $this->iconExtensions)) {
                        $errors[] = "Icon Image {$i_inc} is not having proper extension, please select proper image type.";
                    }
                }
            }
        }
        
        return $errors;
    }
}