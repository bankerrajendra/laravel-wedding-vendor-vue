<?php

namespace App\Http\Controllers;

use App\Logic\Admin\VendorCategoryRepository;
use App\Logic\Admin\SatelliteRepository;
use App\Models\Blog;
use App\Models\User;
use App\Models\Country;
use App\Models\ShortlistedUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Logic\Event\EventRepository;

class WelcomeController extends Controller
{
    public function __construct()
    {
        $this->videoPerPage = 6;        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome(Request $request, VendorCategoryRepository $vendorCatRepo, SatelliteRepository $SatelliteRepository, EventRepository $eventRepository)
    {
        $meta_fields['title'] = getGeneralSiteSetting('site_title');
        $meta_fields['keyword'] = getGeneralSiteSetting('site_keywords');
        $meta_fields['description'] = getGeneralSiteSetting('site_description');
        $meta_fields['canonical'] = getGeneralSiteSetting('site_url');

        $categories = $vendorCatRepo->getFooterCategories(1)->where('name', '<>', 'Event Organizer')->orderBy('name', 'ASC')->get(['id', 'name', 'slug']);
        $fetchBlogs = Blog::where('status','1')->inRandomOrder()->limit(3)->get();

        // get top random 6 categories
        $topRandCats = $vendorCatRepo->getHomeTopCategories();

        // get featured vendors
        $totalPaidVendors = User::getPaidVendor()->count();
        $featuredVendors = [];
        $i = 0;
        if($totalPaidVendors > 0) {
            $getPaidVendors = User::getPaidVendor()->get(['id', 'first_name', 'last_name']);

            foreach($getPaidVendors as $featured_user) {
                $featuredVendors[$i]['image'] = @$featured_user->getVendorProfilePic();
                $featuredVendors[$i]['title'] = $featured_user->vendors_information->company_name; 
                $description = showReadMoreApi(strip_tags($featured_user->vendors_information->business_information), 100, '');
                $featuredVendors[$i]['description'] = @$description;
                $featuredVendors[$i]['profile'] = @$featured_user->getVendorProfileLink();
                $featuredVendors[$i]['average_rating'] = @$featured_user->getAverageReviewRating();
                $featuredVendors[$i]['total_review'] = @$featured_user->getTotalReviewVendor();
                $i++;
            }
        } else {
            
        }
        // prepared showcase videos
        $forShowcaseVideos = [];
        $vd = 0;
        if($totalPaidVendors > 0) {
            // apply ip condition for getting records
            if(Auth::check()) {
                // get user IP
                $userObj = User::where('id', Auth::id())->first(['id', 'signup_ip_address']);
                if($userObj->signup_ip_address != '') {
                    $ip_address = $userObj->signup_ip_address;
                    
                } else {
                    $ip_address = request()->ip();
                }
            } else {
                // get guest IP
                $ip_address = request()->ip();
                //$ip_address = '43.250.158.190'; // for local
            }
            if($ip_address == '::1' || $ip_address == '127.0.0.1' || $ip_address == '192.168.0.104') {
                $ip_address = '43.250.158.190'; // for local
            }
    
            $county_info = json_decode(file_get_contents('https://pro.ip-api.com/json/'.$ip_address.'?key=RocDHDkeHbv0ask'));
            if(!empty($county_info)) {
                $countryInfo = Country::where('code', '=', $county_info->countryCode)->first(['id']);
                $getPaidVendorsVid = User::select('users.*', DB::raw('(CASE WHEN country_id = ' . $countryInfo->id . ' THEN 1 ELSE 
                CASE WHEN country_id = 202 THEN 2 ELSE 
                    CASE WHEN country_id = 32 THEN 3 ELSE 4 END 
                END   
                END) AS newCountryid'))->getPaidVendorForVids(1000)->orderBy('newCountryid','asc')->get(['id']);
            } else {
                $getPaidVendorsVid = User::getPaidVendorForVids(1000)->get(['id']);
            }

            foreach($getPaidVendorsVid as $showcasPVid) {
                // get showcase video
                if($showcasPVid->hasRandVideo()) {
                    $forShowcaseVideos[$vd] = $showcasPVid->id;
                    $vd++;
                }
            }
            if($vd <= $this->videoPerPage-1) {
                $getNewPaidVendorsFreeVidU = User::getFeaturedVendor(1000)->get(['id']);
                foreach($getNewPaidVendorsFreeVidU as $featured_user_free_vid_u) {
                    // get showcase video
                    if($featured_user_free_vid_u->hasRandVideo()) {
                        $forShowcaseVideos[$vd] = $featured_user_free_vid_u->id;
                        $vd++;
                    }
                }
            }
        } else {
            $getNewPaidVendorsFreeVidU = User::getFeaturedVendor(1000)->get(['id']);
            foreach($getNewPaidVendorsFreeVidU as $featured_user_free_vid_u) {
                // get showcase video
                if($featured_user_free_vid_u->hasRandVideo()) {
                    $forShowcaseVideos[$vd] = $featured_user_free_vid_u->id;
                    $vd++;
                }
            }
        }
        // save vendors that are having videos in session
        $request->session()->put('home_showcase_vid_vendors', $forShowcaseVideos);
        // get showcase videos arry
        $chunk_array_vids = array_chunk($forShowcaseVideos, $this->videoPerPage);
        $showcaseVideosArray = @$this->getShowcaseVideosArry($chunk_array_vids[0]);

        // get satellites
        $satellites = $SatelliteRepository->getStatusRecords(1)->get(['id', 'title', 'url']);

        // get showcase events videos
        $eventVids = $eventRepository->getHomeEventsVids($this->videoPerPage);
        
        return view('welcome', [
            'metafields' => $meta_fields,
            'categories' => $categories,
            'fetchBlogs' => $fetchBlogs,
            'topRandCats' => $topRandCats,
            'featuredVendors' => $featuredVendors,
            'showcaseVideosArray' => $showcaseVideosArray,
            'totalShowcaseVids' => count($forShowcaseVideos),
            'videoPerPage' => $this->videoPerPage,
            'index_page_flag' => "1",
            'isAjax' => false,
            'satellites' => $satellites,
            'eventVids' => $eventVids
        ]);
    }

    public function getVendorShowcaseVideosAjax(Request $request)
    {
        // check that session value is set for showcase videos
        if($request->session()->has('home_showcase_vid_vendors')) {
            $home_showcase_vid_vendors = $request->session()->get('home_showcase_vid_vendors');
            $chunk_array_vids = array_chunk($home_showcase_vid_vendors, $this->videoPerPage);
            $page = $request->input('page');
            $showcaseVideosArray = $this->getShowcaseVideosArry($chunk_array_vids[$page-1]);
            if(empty($showcaseVideosArray)) {
                $next_page = $page+1;
            } else {
                $next_page = '';
            }
            
            if($request->ajax()) {
                return [
                    'videos' => view('pages.user.ajax.show-showcase-videos', ['showcaseVideosArray' => $showcaseVideosArray, 'isAjax' => true])->render(),
                    'next_page' => $next_page
                ];
            }
        }
    }

    protected function getShowcaseVideosArry($forShowcaseVideos = [])
    {
        $allowShortList = 0;
        if(Auth::check()) {
            $logged_user = User::where('id', Auth::id())->first();
            if($logged_user->hasRole('unverified')) {
                $allowShortList = 1;
            }
        } else {
            $allowShortList = 1;
        }
        $showcaseVideosArray = [];
        $totalShowcaseVideosCnt = count($forShowcaseVideos);
        if($totalShowcaseVideosCnt > 0) {
            //decide loop through
            if($totalShowcaseVideosCnt > $this->videoPerPage) {
                $cnt_loop = $this->videoPerPage-1;
            } else {
                $cnt_loop = ($totalShowcaseVideosCnt-1);
            }
            // get user details
            for($i = 0; $i <= $cnt_loop; $i++) {
                if(User::where('id', $forShowcaseVideos[$i])->exists()) {
                    $userObject = User::where('id', $forShowcaseVideos[$i])->first();
                    $showcaseVideosArray[$i]['vendor_id'] = $userObject->id;
                    $showcaseVideosArray[$i]['vendor_enc_id'] = $userObject->getEncryptedId();
                    $showcaseVideosArray[$i]['vendor_profile_link'] = $userObject->getVendorProfileLink();
                    $showcaseVideosArray[$i]['average_rating'] = $userObject->getAverageReviewRating();
                    $showcaseVideosArray[$i]['allow_shortlist'] = $allowShortList;
                    if(Auth::check()) {
                        if($logged_user->hasRole('unverified')) {
                            if(ShortlistedUser::where(['from_user' => Auth::id(), 'to_user' => $userObject->id])->exists()) {
                                $showcaseVideosArray[$i]['shortlisted'] = 1;
                            } else {
                                $showcaseVideosArray[$i]['shortlisted'] = 0;
                            }
                        }
                    } else {
                        $showcaseVideosArray[$i]['shortlisted'] = 0;
                    }
                    // get video information
                    $videoObject = $userObject->getSingleRandVideo();
                    $showcaseVideosArray[$i]['vendor_id'] = $videoObject->id;
                    $showcaseVideosArray[$i]['video_title'] = $videoObject->video_title;
                    // extract video id
                    $embedVidUrl = GetYouTubeId($videoObject->embed_code);
                    $showcaseVideosArray[$i]['embed_code'] = $embedVidUrl;
                    $showcaseVideosArray[$i]['description'] = showReadMoreApi(strip_tags($videoObject->description), 80, '');
                }
            }
        }
        return $showcaseVideosArray;
    }

    public function getEventShowcaseVideosAjax(Request $request, EventRepository $eventRepository)
    {
        $page = $request->input('page');
        $eventVids = $eventRepository->getHomeEventsVids($this->videoPerPage);
        if($eventVids->nextPageUrl() != null) {
            $next_page = $page+1;
        } else {
            $next_page = '';
        }
        if($request->ajax()) {
            return [
                'videos' => view('pages.user.ajax.show-event-videos', ['eventVids' => $eventVids, 'isAjax' => true])->render(),
                'next_page' => $next_page
            ];
        }
    }
}
