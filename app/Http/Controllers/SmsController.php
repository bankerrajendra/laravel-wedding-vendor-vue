<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Jwt\ClientToken;
use GuzzleHttp\Exception\GuzzleException;
use App\Logic\Common\SiteGeneralSettingsRepository;
use GuzzleHttp\Client;
use App\Logic\Template\TemplateRepository;

class SmsController extends Controller
{
    public function sendSms(Request $request, SiteGeneralSettingsRepository $site_settings, TemplateRepository $template_repo)
    {
        $twilio_account_sid_obj         = $site_settings->getSettingsByKey('twilio_account_sid');
        $twilio_auth_token_obj          = $site_settings->getSettingsByKey('twilio_auth_token');
        $twilio_from_mobile_number_obj  = $site_settings->getSettingsByKey('twilio_from_mobile_number');
        $sms_status_obj                 = $site_settings->getSettingsByKey('sms_status');
        
        $contact_number = $request->has('contact_number') ? $request->input('contact_number') : "";
        $message_body = $request->has('message_body') ? $request->input('message_body') : "";
        $ajax = $request->has('ajax') ? $request->input('ajax') : false;
        $user_name = $request->has('user_name') ? $request->input('user_name') : "";

        if($contact_number == "" || $message_body == "") {
            $error_message = "Number OR Message cann't be empty, try again";
            if($ajax) {
                return response()->json(['status' => 'error', 'message' => $error_message]);
            } else {
                return redirect()->back()->withErrors($error_message);
            }
        }

        try {
            if(!empty($twilio_account_sid_obj) && !empty($twilio_auth_token_obj) && !empty($twilio_from_mobile_number_obj) && !empty($sms_status_obj)) {
                $sms_status                 = $sms_status_obj->value;
                $twilio_account_sid         = $twilio_account_sid_obj->value;
                $twilio_auth_token          = $twilio_auth_token_obj->value;
                $twilio_from_mobile_number  = $twilio_from_mobile_number_obj->value;
                if($sms_status == "A" && $twilio_account_sid != "" && $twilio_auth_token != "" && $twilio_from_mobile_number != "") {

                    // GET ADMIN TEMPLATE
                    $sms_template = $template_repo->getTemplateBy('Admin General', 'sms')
                                        ->where('status', '=', 'A')
                                        ->first(['id', 'description']);
                    if(!empty($sms_template)) {
                        $replace_vars = [
                            '[%USER_NAME%]' => $user_name,
                            '[%SMS_CONTENT%]' => $message_body,
                            '[%WEBSITENAME%]' => getGeneralSiteSetting('site_name')
                        ];
                        $sms_content = replaceEmailTemplateVars($sms_template->description, $replace_vars);

                        $client = new Client(['auth' => [$twilio_account_sid, $twilio_auth_token]]);
                        $result = $client->post('https://api.twilio.com/2010-04-01/Accounts/'.$twilio_account_sid.'/Messages.json',
                        [
                            'form_params' => [
                                'Body'  => $sms_content, //set message body
                                'To'    => $request->contact_number,
                                'From'  => $twilio_from_mobile_number //we get this number from twilio
                        ]]);
                    }
                    if($ajax) {
                        return response()->json(['status' => 'success', 'message' => "SMS sent successfully."]);
                    } else {
                        return $result;
                    }
                } else {
                    $error_message = "Configuration is not proper, Some problem while sending SMS";
                    if($ajax) {
                        return response()->json(['status' => 'error', 'message' => $error_message]);
                    } else {
                        return redirect()->back()->withErrors($error_message);
                    }
                }
            }
        } catch (Exception $e) {
            if($ajax) {
                return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
            } else {
                return redirect()->back()->withErrors($e->getMessage());
            }
        }
    }
}
