<?php

namespace App\Http\Controllers;

use App\Logic\Common\LocationRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\UserProfileFlag;
use App\Models\UsersInformation;
use App\Models\VendorsInformation;
use App\Models\VendorsImage;
use App\Models\OldCIVendors;
use App\Models\Country;
use jeremykenedy\LaravelRoles\Models\Role;

class OldCIVendorsMigration extends Controller
{

    public function __construct()
    {

    }

    public function getOldCiVendors()
    {
        // assign role
        $role = Role::where('slug', '=', 'vendor')->first();
        // 1. get all old ci records
        $odlCiVends = OldCIVendors::get();
        $i = 0;
        // 2. loop through each
        foreach($odlCiVends as $oldVendor) {
            // 3. create vendor user with role
            // check user should not exists with same email 
            //\Log::info('loop: '.$i.' & user id: '.$oldVendor->id . ' & email:'.$oldVendor->email);
            if($oldVendor->country != 0 && $oldVendor->state != 0 && $oldVendor->city != 0 && $oldVendor->email != '' && OldCIVendors::where('email',$oldVendor->email)->count() == 1) {
                // // prepare mobile no
                // $expldMobNum = explode('-', $oldVendor->mobile);
                // // get country id by mobile code
                // if(!empty($expldMobNum)) {
                //     if(isset($expldMobNum[0])) {
                //         $mobileCode = $expldMobNum[0];
                //         $mobielCountryId = Country::where('phonecode', $mobileCode)->first(['id']);
                //         $mobile_country_code = $mobielCountryId->id;
                //     } else {
                //         $mobile_country_code = 0;
                //     }
                //     $exact_mobile_number = $expldMobNum[1];
                // }
                // // if deleted
                // if($oldVendor->is_deleted == 'Y') {
                //     $deleted_at = date('Y-m-d H:i:s');
                // } else {
                //     $deleted_at = null;
                // }

                // $user = User::create([
                //     'name'                  => $oldVendor->first_name.".".$oldVendor->last_name,
                //     'first_name'            => $oldVendor->first_name,
                //     'last_name'             => $oldVendor->last_name,
                //     'email'                 => $oldVendor->email,
                //     'password'              => $oldVendor->password,
                //     'token'                 => str_random(64),
                //     'signup_ip_address'     => $oldVendor->ip,
                //     'activated'             => 1,
                //     'country_id'            => $oldVendor->country,
                //     'city_id'               => $oldVendor->city,
                //     'mobile_country_code'   => $mobile_country_code,
                //     'mobile_number'         => $exact_mobile_number,
                //     'address'               => $oldVendor->address,
                //     'state_id'              => $oldVendor->state,
                //     'zip'                   => $oldVendor->pin_code,
                //     'vendor_type'           => 1,
                //     'site_id'               => $oldVendor->site_id,
                //     'created_at'            => $oldVendor->created_on,
                //     'updated_at'            => $oldVendor->modified_on,
                //     'deleted_at'            => $deleted_at,
                //     'featured'              => ($oldVendor->is_featured == 'Yes')?1:0,
                //     'is_paid'               => ($oldVendor->plan_status == 'Paid')?1:0,
                // ]);

                // $user->attachRole($role);

                // // 4. after save vendor information
                // // add initial slug for company
                // // 1. check slug exists by slugyfy
                // $company_slug = slugifyText($oldVendor->company_name);
                // if(VendorsInformation::where('company_slug', $company_slug)->exists()) {
                //     // 2. update slug by adding -2
                //     $company_slug .= "-2";
                // }

                // $vendorInfoArr = [
                //         'vendor_id'             =>  $user->id,
                //         'company_name'          =>  $oldVendor->company_name,
                //         'company_slug'          =>  $company_slug,
                //         'vendor_category'       =>  $oldVendor->category_id,
                //         'business_information'  =>  $oldVendor->description,
                //         'business_website'      =>  $oldVendor->website,
                //         'facebook'              =>  $oldVendor->facebook_page,
                //         'instagram'             =>  $oldVendor->instagram_page,
                //         'pinterest'             =>  $oldVendor->pinterest_page,
                //         'twitter'               =>  $oldVendor->twitter_page,
                //         'linkedin'              =>  $oldVendor->linkedin_page,
                //     ];
                
                // VendorsInformation::create($vendorInfoArr);
                // // 5. add vendor images
                // // 5.1 Cover Image
                // if($oldVendor->cover_photo != '') {
                //     $userCover = VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->cover_photo,
                //         'image_thumb' => $oldVendor->cover_photo,
                //         'image_type' => 'cover',
                //         'is_profile_image' => 'N',
                //         'is_approved' => ($oldVendor->cover_photo_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // // 5.2 Profile Images
                // if($oldVendor->photo1 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo1,
                //         'image_thumb' => $oldVendor->photo1,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo1_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // if($oldVendor->photo2 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo2,
                //         'image_thumb' => $oldVendor->photo2,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo2_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // if($oldVendor->photo3 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo3,
                //         'image_thumb' => $oldVendor->photo3,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo3_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // if($oldVendor->photo4 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo4,
                //         'image_thumb' => $oldVendor->photo4,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo4_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // if($oldVendor->photo5 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo5,
                //         'image_thumb' => $oldVendor->photo5,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo5_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // if($oldVendor->photo6 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo6,
                //         'image_thumb' => $oldVendor->photo6,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo6_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // if($oldVendor->photo7 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo7,
                //         'image_thumb' => $oldVendor->photo7,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo7_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // if($oldVendor->photo8 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo8,
                //         'image_thumb' => $oldVendor->photo8,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo8_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // if($oldVendor->photo9 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo9,
                //         'image_thumb' => $oldVendor->photo9,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo9_approve == 'A')?'Y':'N'
                //     ]);
                // }
                // if($oldVendor->photo10 != '') {
                //     VendorsImage::create([
                //         'vendor_id' => $user->id,
                //         'image_full' => $oldVendor->photo10,
                //         'image_thumb' => $oldVendor->photo10,
                //         'image_type' => 'profile',
                //         'is_approved' => ($oldVendor->photo10_approve == 'A')?'Y':'N'
                //     ]);
                // }
            } else {
                //\Log::info('loop: '.$i.' & not added user id: '.$oldVendor->id . ' & email:'.$oldVendor->email);
            }
            $i++;
        }
    }
}