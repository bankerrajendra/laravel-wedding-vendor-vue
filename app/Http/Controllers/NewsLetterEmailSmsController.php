<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Logic\Template\TemplateRepository;
use App\Logic\Common\SiteGeneralSettingsRepository;
use App\Logic\Common\NewsLetterSubscriberRepository;
use App\Models\NewsLetterEmailSmsTemplate;
use App\Models\SiteGeneralOptions;
use App\Models\NewsLetterSubscription;
use App\Models\EmailSubject;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewsLetterEmail;

class NewsLetterEmailSmsController extends Controller
{
    protected $validation_rules = [
        'name'          => 'required|max:200',
        'description'   => 'required',
        'status'        => 'required|max:1',
        'type'          => 'required',
    ];
    public function __construct()
    {
        //
    }
    
    /**
     * Show Add Template Form
     */
    public function showAddTemplate($type)
    {
        $validation_rules = $this->validation_rules;
        if($type == "email") {
            $validation_rules['subject'] = 'required|max:250';
        }
        $validator  = JsValidator::make($validation_rules);
        // page title
        $page_title = getPageTitleByType($type);
        
        return view('pages.admin.general-templates.add', [
            'validator'     => $validator,
            'type'          => $type,
            'page_title'    => $page_title,
        ]);
    }

    /**
     * Show Add Template Form
     */
    public function showEditTemplate($type, $id, TemplateRepository $template_repo)
    {
        $validation_rules       = $this->validation_rules;
        $validation_rules['id'] = 'required|numeric';
        if($type == "email") {
            $validation_rules['subject'] = 'required|max:250';
        }
        $validator              = JsValidator::make($validation_rules);
        // page title
        $page_title             = getPageTitleByType($type);
        
        // get single record
        $template               = $template_repo->getRecords($id);

        return view('pages.admin.general-templates.edit', [
            'validator'     => $validator,
            'type'          => $type,
            'page_title'    => $page_title,
            'template'      => $template
        ]);
    }

    /**
     * Show View Template
     */
    public function showViewTemplate($type, $id, TemplateRepository $template_repo)
    {
        // page title
        $page_title         = getPageTitleByType($type);
        
        // get single record
        $template           = $template_repo->getRecords($id);

        return view('pages.admin.general-templates.view', [
            'type'          => $type,
            'page_title'    => $page_title,
            'template'      => $template
        ]);
    }

    /**
     * Handle add request
     */
    public function handleAddTemplate(Request $request)
    {
        // initialize
        $name                       = $request->has('name') ? $request->input('name') : "";
        $description                = $request->has('description') ? $request->input('description') : "";
        $status                     = $request->has('status') ? $request->input('status') : "";
        $type                       = $request->has('type') ? $request->input('type') : "";
        // validate
        $validation_rules           = $this->validation_rules;
        if($type == "email") {
            $validation_rules['subject'] = 'required|max:250';
        }
        $validate                   = $request->validate($validation_rules);

        // save
        $save_template              = new NewsLetterEmailSmsTemplate();
        $save_template->name        = $name;
        $save_template->description = $description;
        $save_template->status      = $status;
        $save_template->type        = $type;
        $save_template->site_id     = config('constants.site_id');

        // return with success or error
        if ($save_template->save()) {
            if($type == "email") { // save email subject
                $save_subject               = new EmailSubject();
                $save_subject->template_id  = $save_template->id;
                $save_subject->subject      = $request->input('subject');
                $save_subject->save();
            }
            return redirect()->back()->withSuccess('Template added successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in adding template, please try again.');
        }
    }

    /**
     * Handle edit request
     */
    public function handleEditTemplate(Request $request, TemplateRepository $template_repo)
    {
        // initialize
        $name                       = $request->has('name') ? $request->input('name') : "";
        $description                = $request->has('description') ? $request->input('description') : "";
        $status                     = $request->has('status') ? $request->input('status') : "";
        $type                       = $request->has('type') ? $request->input('type') : "";
        $id                         = $request->has('id') ? $request->input('id') : "";

        // validate
        $validation_rules           = $this->validation_rules;
        $validation_rules['id']     = 'required|numeric';
        if($type == "email") {
            $validation_rules['subject'] = 'required|max:250';
        }
        $validate                   = $request->validate($validation_rules);

        // save
        $save_template              = $template_repo->getRecords($id);
        $save_template->name        = $name;
        $save_template->description = $description;
        $save_template->status      = $status;
        $save_template->type        = $type;
        $save_template->site_id     = config('constants.site_id');

        // return with success or error
        if ($save_template->save()) {
            if($type == "email") { // save email subject
                $save_subject               = EmailSubject::where('template_id', '=', $save_template->id)->first();
                $save_subject->subject      = $request->input('subject');
                $save_subject->save();
            }
            return redirect()->back()->withSuccess('Template updated successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in editing template, please try again.');
        }
    }

    /**
     * Lists
     */
    public function showListings(Request $request, $type, TemplateRepository $template_repo)
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $status         = $request->has('status') ? $request->input('status') : "";
        $search         = $request->has('search') ? $request->input('search') : "";
        $templates      = $template_repo->getRecords('', $type);

        if($search != "") {
            $templates = $template_repo->getSearchRecords($templates, $search, $type);
        }

        if($status != "") {
            $templates  = $templates->where('status', '=', $status);
        }
        $templates = $templates->latest()
                ->paginate($per_page);          
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $active_counts      = $template_repo->getStatusRecords($type, 'A')
                            ->count();
        $inactive_counts    = $template_repo->getStatusRecords($type, 'I')
                            ->count();
        $all_counts         = $template_repo->getRecords('', $type)
                            ->count();

        $page_title = getPageTitleByType($type);

        return view('pages.admin.general-templates.list', [
            'page_title'        => $page_title,
            'type'              => $type,
            'templates'         => $templates,
            'per_page'          => $per_page,
            'status'            => $status,
            'search'            => $search,
            'active_records'    => $active_counts,
            'inactive_records'  => $inactive_counts,
            'all_records'       => $all_counts
        ]);
    }

    /**
     * Handle bulk action of list template
     */
    public function handleBulkActionTemplate(Request $request, TemplateRepository $template_repo)
    {
        $action         = $request->has('action') ? $request->input('action') : "";
        $type           = $request->has('type') ? $request->input('type') : "";
        $template_ids   = $request->has('template_ids') ? $request->input('template_ids') : "";
        if( $action != "" && ($action == "delete" || $action == "A" || $action == "I") && $type != "" && !empty($template_ids) ) {
            $templates = $template_repo->getRecords($template_ids, '');
            if($action == "A" || $action == "I") {
                try {
                    foreach($templates as $template) {
                        $template->status = $action;
                        $template->save();
                    }
                    return redirect()->back()->withSuccess('Template(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating template(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($templates as $template) {
                        if($type == 'email') {
                            // delete email subject
                            if(EmailSubject::where('template_id', $template->id)->exists()) {
                                EmailSubject::where('template_id', $template->id)->delete();
                            }
                        }
                        $template->delete();
                    }
                    return redirect()->back()->withSuccess('Template(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting template(s), please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    /**
     * SMS Configuration page
     */
    public function showSMSConfiguarion(Request $request, SiteGeneralSettingsRepository $site_settings)
    {
        $validation_rules = [
            'twilio_account_sid'            => 'required',
            'twilio_auth_token'             => 'required',
            'twilio_from_mobile_number'     => 'required',
            'sms_status'                    => 'required|max:1'
        ];
        $validator                          = JsValidator::make($validation_rules);
        $twilio_account_sid_obj             = $site_settings->getSettingsByKey('twilio_account_sid');
        $twilio_auth_token_obj              = $site_settings->getSettingsByKey('twilio_auth_token');
        $twilio_from_mobile_number_obj      = $site_settings->getSettingsByKey('twilio_from_mobile_number');
        $sms_status_obj                     = $site_settings->getSettingsByKey('sms_status');
        $sms_status = $twilio_account_sid = $twilio_auth_token = $twilio_from_mobile_number = '';
        
        if(!empty($sms_status_obj)) {
            $sms_status                     = $sms_status_obj->value;
        }
        if(!empty($twilio_account_sid_obj)) {
            $twilio_account_sid             = $twilio_account_sid_obj->value;
        }
        if(!empty($twilio_auth_token_obj)) {
            $twilio_auth_token              = $twilio_auth_token_obj->value;
        }
        if(!empty($twilio_from_mobile_number_obj)) {
            $twilio_from_mobile_number      = $twilio_from_mobile_number_obj->value;
        }
            
        return view('pages.admin.sms.config', [
            'validator'                     => $validator,
            'twilio_account_sid'            => $twilio_account_sid,
            'twilio_auth_token'             => $twilio_auth_token,
            'twilio_from_mobile_number'     => $twilio_from_mobile_number,
            'sms_status'                    => $sms_status
        ]);
    }

    /**
     * Handle SMS Configuration
     */
    public function handleSMSConfiguarion(Request $request, SiteGeneralSettingsRepository $site_settings)
    {
        $validation_rules = [
            'twilio_account_sid'            => 'required',
            'twilio_auth_token'             => 'required',
            'twilio_from_mobile_number'     => 'required',
            'sms_status'                    => 'required|max:1'
        ];
        $validate                           = $request->validate($validation_rules);
        $twilio_account_sid                 = $request->has('twilio_account_sid') ? $request->input('twilio_account_sid') : "";
        $twilio_auth_token                  = $request->has('twilio_auth_token') ? $request->input('twilio_auth_token') : "";
        $twilio_from_mobile_number          = $request->has('twilio_from_mobile_number') ? $request->input('twilio_from_mobile_number') : "";
        $sms_status                         = $request->has('sms_status') ? $request->input('sms_status') : "";
        if($site_settings->saveKeyVal('twilio_account_sid', $twilio_account_sid) && $site_settings->saveKeyVal('twilio_auth_token', $twilio_auth_token) && $site_settings->saveKeyVal('twilio_from_mobile_number', $twilio_from_mobile_number) && $site_settings->saveKeyVal('sms_status', $sms_status)) {
            return redirect()->back()->withSuccess('Setting saved.');
        }else {
            return redirect()->back()->withErrors('Setting save Failed, Please try again.');
        }
    }

    /**
     * handle front end newsletter subscription
     */
    public function ajaxHandleNewsLetterSubscription(Request $request)
    {
        // validate incoming request
        $validate = $request->validate([
            'email' => 'required|email'
        ], 
        [
            'email.required' => 'Error: Please enter Email address.',
            'email.email' => 'Error: Invalid email format.'
        ]);

        $email = $request->has('email') ? $request->input('email') : "";
        /**
         * email aready exists in list
         */
        if(NewsLetterSubscription::where('email', '=', $email)->where('site_id', '=', config('constants.site_id'))->exists()) {
            return response()->json(['success' => 'Already Subscribed.']);
        } else {
            $email_subscribe = new NewsLetterSubscription();
            $email_subscribe->email = $email;
            $email_subscribe->status = 'A';
            $email_subscribe->site_id = config('constants.site_id');
            try {
                $email_subscribe->save();
                return response()->json(['success' => 'Subscribed successfully.']);
            } catch (\Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    /**
     * Show subscrbers listing admin
     */
    public function showSubscriberListing(Request $request, NewsLetterSubscriberRepository $subscriber_repo)
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $status         = $request->has('status') ? $request->input('status') : "";
        $search         = $request->has('search') ? $request->input('search') : "";
        $records        = NewsLetterSubscription::where('site_id', '=', config('constants.site_id'));

        if($search != "") {
            $records = $subscriber_repo->getSearchRecords($records, $search);
        }

        if($status != "") {
            $records  = $records->where('status', '=', $status);
        }
        $records = $records->latest()
                ->paginate($per_page);          
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $active_counts      = $subscriber_repo->getStatusRecords('A')
                            ->count();
        $inactive_counts    = $subscriber_repo->getStatusRecords('I')
                            ->count();
        $all_counts         = NewsLetterSubscription::where('site_id', '=', config('constants.site_id'))->count();

        return view('pages.admin.newsletter-subscribers-list', [
            'records'           => $records,
            'per_page'          => $per_page,
            'status'            => $status,
            'search'            => $search,
            'active_records'    => $active_counts,
            'inactive_records'  => $inactive_counts,
            'all_records'       => $all_counts
        ]);
    }

    /**
     * Handle bulk action of list subscribers
     */
    public function handleBulkActionNewsLetterSubscribers(Request $request, NewsLetterSubscriberRepository $subscriber_repo)
    {
        $action         = $request->has('action') ? $request->input('action') : "";
        $record_ids     = $request->has('record_ids') ? $request->input('record_ids') : "";
        if( $action != "" && ($action == "delete" || $action == "A" || $action == "I") && !empty($record_ids) ) {
            $records = $subscriber_repo->getRecords($record_ids);
            if($action == "A" || $action == "I") {
                try {
                    foreach($records as $record) {
                        $record->status = $action;
                        $record->save();
                    }
                    return redirect()->back()->withSuccess('Record(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating record(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($records as $record) {
                        $record->delete();
                    }
                    return redirect()->back()->withSuccess('Record(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting record(s), please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    /**
     * Show form to send News Letters
     */
    public function showNewsLetterSend(Request $request, TemplateRepository $template_repo)
    {
        $validation_rules = [
            'status'        => 'required',
            'newsletter'    => 'required|numeric'
        ];
        $validator  = JsValidator::make($validation_rules);
        $news_letters = $template_repo->getRecords('', 'news-letter')
                            ->where('status', '=', 'A')
                            ->get(['id', 'name']);
        // get all users
        $all_users = User::hasNotAdminRole()->latest()->get(['id', 'first_name', 'last_name', 'email']);
        return view('pages.admin.send-newsletter', [
            'validator'     => $validator,
            'news_letters'  => $news_letters,
            'all_users'     => $all_users
        ]);
    }

    /**
     * Get members for newsletter
     */
    public function getMembersForNewsLetter(Request $request)
    {
        $status = $request->has('status') ? $request->input('status') : "";
        if( $status == "Subscribers") {
            $users = NewsLetterSubscription::where('site_id', '=', config('constants.site_id'))
                                                ->where('status', '=', "A")
                                                ->latest()
                                                ->get(['id', 'email']);
            return response()->json($users);
        } else {
            $users = User::hasNotAdminRole();
            if( $status == "Active" ) {
                $users->where('activated',1)
                ->where('deactivated',0);
            } else if( $status == "Inactive" ) {
                $users->where(function($users) {
                    $users->where('activated',0)
                    ->orWhere('deactivated',1);
                });
            } else if( $status == "Paid" ) {
                $users->getPaidMembers();
            } else if( $status == "Expired" ) {
                $users->getExpiredMembers();
            }   
            $users = $users->latest()
                ->get([
                    'id', 
                    'first_name', 
                    'last_name', 
                    'email'
                ]);

            return response()->json($users);
        }
    }

    /**
     * Handle send newsletter action
     */
    public function handleNewsLetterSubmit(Request $request, TemplateRepository $template_repo)
    {
        $validation_rules = [
            'status'        => 'required',
            'newsletter'    => 'required|numeric'
        ];

        $validate = $request->validate($validation_rules);
        
        $status = $request->has('status') ? $request->input('status') : "";
        $members = $request->has('members') ? $request->input('members') : [];
        $newsletter = $request->has('newsletter') ? $request->input('newsletter') : "";
        
        if(empty($members)) {
            return redirect()->back()->withErrors('Please select Members.');
        }
        try {
            /**
             * Get email is of members with status selected
             */
            if( $status == "Subscribers" ) {
                if($members[0] == "all") {
                    // send email to all active subscribers
                    $users = NewsLetterSubscription::where('site_id', '=', config('constants.site_id'))
                                                    ->where('status', '=', "A")
                                                    ->latest()
                                                    ->get(['email']);
                } else {
                    $users = NewsLetterSubscription::whereIn('id', $members)
                                                    ->where('status', '=', "A")
                                                    ->latest()
                                                    ->get(['email']);
                }
            } else {
                if($members[0] == "all") {
                    $users = User::hasNotAdminRole();
                    if( $status == "Active" ) {
                        $users->where('activated',1)
                        ->where('deactivated',0);
                    } else if( $status == "Inactive" ) {
                        $users->where(function($users) {
                            $users->where('activated',0)
                            ->orWhere('deactivated',1);
                        });
                    } else if( $status == "Paid" ) {
                        $users->getPaidMembers();
                    } else if( $status == "Expired" ) {
                        $users->getExpiredMembers();
                    }   
                    $users = $users->get([ 
                            'email'
                        ]);
                } else {
                    $users = User::hasNotAdminRole()
                                ->whereIn('id', $members)
                                ->get([
                                    'email'
                                ]);
                }
            }
            // get newsletter content
            $newsletter_obj = $template_repo->getRecords($newsletter, '');                                            
            foreach($users as $user) {
                if( $status == "Subscribers" ) {
                    $flagSendEmail = "Y";
                    $type = "SB";
                    $unsubscribe_link = route('unsubscribe', [ 'token' => base64_encode($user->email), 'type' => $type ]);
                } else {
                    if($user->hasRole('unverified')) {
                        $flagSendEmail = $user->get_user_information()->notify_news_letter;
                    }
                    if($user->hasRole('vendor')) { // vendor role
                        $flagSendEmail = $user->vendors_information->notify_news_letter;
                    }
                    $type = getUnsubscribeType('notify_news_letter');
                    $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => $type ]);
                }
                if($flagSendEmail == "Y") {
                    $mail_req_arry = [
                        'mail_name' => $newsletter_obj->name,
                        'unsubscribe_link' => $unsubscribe_link
                    ];
                    Mail::to($user->email)->send(new NewsLetterEmail($mail_req_arry)); 
                }
            }
            return redirect()->back()->withSuccess('News Letter sent successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
