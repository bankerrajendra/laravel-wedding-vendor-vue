<?php

namespace App\Http\Controllers;


use App\Logic\Admin\CmsPagesRepository;


use App\Logic\Common\UserInformationOptionsRepository;
use App\Mail\ContactSubmission;
use Illuminate\Support\Facades\Mail;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;


class CmsController extends Controller
{
    protected $cmsPagesService;
    protected $validationRules = [
        'name'=>'required|alpha_spaces',
        'email'=>'required|email',
        'subject'=>'required',
        'mobile'=>'required|numeric',
        'message'=>'required'
    ];
    protected $validationRulesServer = [
        'name'=>'required|alpha_spaces',
        'email'=>'required|email',
        'subject'=>'required',
        'mobile'=>'required',
        'message'=>'required',
        'g-recaptcha-response' => 'required|captcha'
    ];
    //
    public function __construct(CmsPagesRepository $cmsPagesService)
    {
        $this->cmsPagesService=$cmsPagesService;
    }

//    public function getStates($country_name) {
//        $cmsVar=$this->cmsCityStateService->getCountryWiseStates($country_name);
//
//        $allStates = array();
//        foreach ($cmsVar as $result){
//            $firstChar = $result['type_name'][0];
//            $allStates[ucfirst($firstChar)][] = $result;
//        }
//        $meta_fields['title']=($country_name=='canada'?'Canada Online Indian Dating Service, Indian Women Dating, Girls, Men':'USA Online Indian Dating Service, Indian Women Dating, Girls, Men');
//        $meta_fields['keyword']=($country_name=='canada'?'Canada Online Indian Dating Service, Indian Women Dating, Girls, Men':'USA Online Indian Dating Service, Indian Women Dating, Girls, Men');
//        $meta_fields['description']=($country_name=='canada'?'Join Punjabigirl.com to meet Canada Indian dating girls, men & women. Verified Contact Numbers. 100% Satisfactions. Join FREE.':'Join Punjabigirl.com to find USA Indian girls, men & women for dating. Verified Numbers. Friendship Satisfactions. Join FREE.');
//        return view('pages.cms.cms-state')->with(['statelist'=>$allStates,'country_name'=>strtoupper($country_name),'pagetype'=>'State','pagetypes'=>'States','metafields'=>$meta_fields]);
//    }

    public function getTelephone() {
        $response = array(
            'status' => 1,
            'res' => getGeneralSiteSetting('site_contact_telephone')
        );
        return $response;
    }

    public function getPageInfo($slug, UserInformationOptionsRepository $userInfoRepo) { //Request $request
       // $page_id = $request->route()->getAction()['page'];
        $featuredUsers="";
        if($slug=="about-us") {
            $featuredUsers = $userInfoRepo->getFeaturedUsers();
        }
        $cmsPageInfo=$this->cmsPagesService->getPageRec($slug);
        $meta_fields['title']=$cmsPageInfo->meta_title;
        $meta_fields['keyword']=$cmsPageInfo->meta_keyword;
        $meta_fields['description']=$cmsPageInfo->meta_description;

        return view('pages.cms.cms-page')->with(['pageinfo'=>$cmsPageInfo,'metafields'=>$meta_fields,'slug'=>$slug,'featuredUsers'=>$featuredUsers]);
    }

    public function contact() 
    {
        $slug='contact';
        $cmsPageInfo = $this->cmsPagesService->getPageRec($slug);
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;
        $validator = JsValidator::make($this->validationRules);
        return view('pages.cms.cms-contact')->with(['metafields'=>$meta_fields,'validator'=>$validator,'slug'=>$slug]);
    }

    public function sitemap() {
        $slug='sitemap';
        $cmsPageInfo=$this->cmsPagesService->getPageRec($slug);
        $meta_fields['title']=$cmsPageInfo->meta_title;
        $meta_fields['keyword']=$cmsPageInfo->meta_keyword;
        $meta_fields['description']=$cmsPageInfo->meta_description;

        $site_id=config('constants.site_id');
        $vendorCatList=menuVendorCategories();
        $eventCatList=  menuEventCategories();


        return view('pages.cms.sitemap')->with(['metafields'=>$meta_fields,
            'slug'=>$slug,
            'vendorCat'=>$vendorCatList,
            'eventCat'=>$eventCatList]);

    }

    public function sendContact(Request $request) {
        $validate =$request->validate($this->validationRulesServer,
            [
                'name.required'=>'Name is required',
                'email.required'=>'Email is required',
                'email.email'=>'Invalid email format',
                'subject.required'=>'Subject is required',
                'mobile.required'=>'Mobile is required',
                'message.required'=>'Message is required',
                'g-recaptcha-response.required'=>'Captcha  is required',
                'g-recaptcha-response.captcha'=>'Invalid Captcha'
            ]
        );

        $contact=array();
        $contact['username']=$request->name;
        $contact['email']=$request->email;
        $contact['subject']=$request->subject;
        $contact['mobile']=$request->mobile;
        $contact['message']=$request->message;

        try {
            $adminemail=config('constants.admin_email');
            Mail::to($adminemail)->send(new ContactSubmission($contact));
        }
        catch (Exception $e){

        }

        return redirect()->back()->withSuccess(config('constants.contact_save.save_message'));
    }

    public function getCms(){
        $cmsPages=$this->cmsPagesService->getPages();

        return view('pages.admin.cms.cms-page')->with(['pageslist'=>$cmsPages]);
    }

    public function setCmsPage($cms_id){
        $cmsPageInfo=$this->cmsPagesService->getPageInfo($cms_id);

        return view('pages.admin.cms.edit-cms-page')->with(['cmspageinfo'=>$cmsPageInfo,'id'=>$cms_id]);
    }

    public function updateCmsPage($cms_id, Request $request) {

       $this->cmsPagesService->updateCmsRec($cms_id, $request);
       return redirect(route('cms-listing'));
    }
}
