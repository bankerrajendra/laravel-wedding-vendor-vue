<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ApiTokenController extends Controller
{
    /**
     * Update the authenticated user's API token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function update(Request $request)
    {
        if($request->user()) {
            $token = hash('sha256', Str::random(60));
            $request->user()->forceFill([
                'api_token' => $token,
            ])->save();
            return Response::json(array('token' => $token));
        } else {
            return Response::json(array('status'=>'Session Expired'));
        }
    }
}