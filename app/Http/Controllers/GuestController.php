<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UserPriceRequest;
use App\Models\Message;
use App\Models\VendorShowcase;
use App\Traits\CaptureIpTrait;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use App\Logic\Common\LocationRepository;
use App\Logic\Admin\VendorCategoryRepository;
use JsValidator;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralEmail;
use App\Logic\Admin\CmsPagesRepository;

class GuestController extends Controller
{
    protected $cmsPagesService;
    protected $validationRequestPricing = [
        'email'                 => 'required|max:255|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        'event_date'            => 'required|min:10|max:10',
        'mobile_number'         => 'phone_valid',
        'password'              => 'required|min:6|max:30',
        'about_extra_needs'     => 'max:300'
    ];

    public function __construct(LocationRepository $locationRepository, CmsPagesRepository $cmsPagesService)
    {
        $this->cmsPagesService = $cmsPagesService;
        View::share('countries', $locationRepository->getCountries());
    }

    public function handleSubmitNewPriceRequest(Request $request)
    {
        if(Auth::check()) {
            unset($this->validationRequestPricing['email']);
            unset($this->validationRequestPricing['password']);
        }
        $validator = Validator::make($request->all(), $this->validationRequestPricing);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        
        $vendor_id = $request->has('vendor_id') ? $request->input('vendor_id') : "";
        $vendor_error = [
                'status' => 0,
                'message' => 'Something went wrong, please try again.'
            ];
        if($vendor_id != "") {
            // check vendor exists with that id
            if(User::withEncryptedId($vendor_id)->isActive()->exists()) {
                $vendor = User::withEncryptedId($vendor_id)->first();
            } else {
                $vendor_error['message'] = "Vendor does not exist";
                return response()->json($vendor_error);
            }
        } else {
            return response()->json($vendor_error);
        }

        // validate any contact should be selected
        if($request->has('check_email_contact') == false && $request->has('check_phone_contact') == false && $request->has('check_message_contact') == false) {
            return response()->json(
                [
                    'status' => 0,
                    'message' => 'Please select any one preferred contact method.'
                ]
            );
        }

        // validate for phone number is selected
        if($request->has('check_phone_contact')) {
            if($request->input('mobile_number') == '') {
                return response()->json(
                    [
                        'status' => 0,
                        'message' => 'Please enter phone number.'
                    ]
                );
            }
        }
        // event date
        $expld_date = explode('.', $request->input('event_date'));
        $event_date = $expld_date[2].'-'.$expld_date[1].'-'.$expld_date[0];
        // validate
        $event_date_check = strtotime(date('Y-m-d', strtotime($event_date) ) );
        $todays = strtotime(date('Y-m-d'));

        if($event_date_check < $todays) {
            return response()->json(
                [
                    'status' => 0,
                    'message' => 'Event date cann\'t be past date.'
                ]
            );
        }

        if (Auth::check()) {
            // login user
            $user = Auth::user();

            // if user is not having role with unverified = user then show error
            if(!$user->hasRole('unverified')) {
                return response()->json(
                    [
                        'status' => 0,
                        'message' => 'You are not allowing to send message! please login as User.'
                    ]
                );    
            }
        } else {
            if(User::where('email', $request->input('email'))->hasUserRole()->exists()) {
                if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
                    $user = Auth::user();
                } else {
                    // pass message that user is there with same email but worng password
                    return response()->json(
                        [
                            'status' => 0,
                            'message' => 'You entered incorrect password.'
                        ]
                    );
                }
            } else {
                // check user is having other role
                if(User::where('email', $request->input('email'))->hasVendorRole()->exists()) {
                    return response()->json(
                            [
                                'status' => 0,
                                'message' => 'You are not allowing to send message! please login as User.'
                            ]
                        );
                }
                $ipAddress = new CaptureIpTrait();
                $role = Role::where('slug', '=', 'unverified')->first();
                // Register the new user or whatever.
                $user = User::create([
                    'name'                  => $request->input('first_name').".".$request->input('last_name'),
                    'first_name'            => $request->input('first_name'),
                    'last_name'             => $request->input('last_name'),
                    'email'                 => $request->input('email'),
                    'password'              => Hash::make($request->input('password')),
                    'token'                 => str_random(64),
                    'mobile_number'         => $request->input('mobile_number'),
                    'signup_ip_address'     => $ipAddress->getClientIp(),
                    'activated'             => 1,
                    'access_grant'          => 'Y',
                    'site_id'               => (int) config('constants.site_id')
                ]);

                $user->attachRole($role);
                // send welcome email
                $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'User Registration',
                    'mail_replace_vars' => [
                        '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                        '[%WEB_NAME%]' => getGeneralSiteSetting('site_name'),
                        '[%USER_FIRST_NAME%]' => $user->first_name,
                        '[%USER_EMAIL%]' => $user->email
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
                // log user in
                Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);
            }
        }
        // preferred contact
        if($request->has('check_email_contact') == true && $request->input('check_email_contact') == 1) {
            $preferred_contact_method = 'email';
        } else if ($request->has('check_phone_contact') == true && $request->input('check_phone_contact') == 1) {
            $preferred_contact_method = 'phone';
        } else if ($request->has('check_message_contact') == true && $request->input('check_message_contact') == 1) {
            $preferred_contact_method = 'message';
        } else {
            $preferred_contact_method = 'email';
        }
        $message_to_send = 'New price request<br><br>';
        $message_to_send .= 'Check below details<br>';
        if($event_date != '') {
            $message_to_send .= 'Event Date: '.date('jS \of F Y', strtotime($event_date)).'<br>';
        }
        if($request->has('mobile_number')) {
            $message_to_send .= 'Phone:'.$request->input('mobile_number').'<br>';
        }
        if($preferred_contact_method != '') {
            $message_to_send .= 'Preferred Contact Method:'.$preferred_contact_method.'<br>';
        }
        if($request->has('about_extra_needs')) {
            $message_to_send .= 'Additional Message:'.$request->input('about_extra_needs').'<br>';
        }
        // send message to vendor
        Message::create([
            'sender_id'         => $user->id,
            'receiver_id'       => $vendor->id,
            'sender_archive'    => 'N',
            'message'           => $message_to_send,
            'created_at'        => Carbon::now()
        ]);
        // if(!$vendor->isOnline()) {
        //         if($vendor->vendors_information->notify_sends_message == "Y") {
                // send email to vendor
                $unsubscribe_link = route('unsubscribe', [ 'token' => $vendor->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'User Price Request',
                    'mail_replace_vars' => [
                        '[%FIRST_NAME%]' => $vendor->first_name,
                        '[%LAST_NAME%]' => $vendor->last_name,
                        '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                        '[%WEBSITENAME%]' => getGeneralSiteSetting('site_name')
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($vendor->email)->send(new GeneralEmail($mail_req_arry));
        //     }
        // }
        // save price ticket information
        if(UserPriceRequest::Create(
            [
                'vendor_id' => $vendor->id,
                'user_id' => Auth::id(),
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'mobile_number' => $request->input('mobile_number'),
                'event_date' => $event_date,
                'preferred_contact_method' => $preferred_contact_method,
                'about_extra_needs' => $request->input('about_extra_needs'),
                'wedding_type' => null,
            ]
        )) {
            return response()->json(
                [
                    'status' => 1,
                    'message' => 'Message sent successfully.'
                ]
            );
        } else {
            $vendor_error['message'] = "Something went wrong while creating price request.";
            return response()->json($vendor_error);
        }
    }
    // profile not found
    public function profileNotFound() {
        return view('pages.user.profile-not-found');
    }
    
    public function showRequestQuote()
    {
        $requestQuoteRules = [
            'no_of_guest' => 'sometimes|numeric|digits_between:1,10',
            'mobile_number' => 'phone_valid',
            'country' => 'required',
            'state'   => 'required',
            'city'   => 'required'
        ];
        if(Auth::check()) {
            $authUser = Auth::user();
            if(User::where('id', $authUser->id)->hasUserRole()->isActive()->exists() == false) {
                abort(403, 'Unauthorized access.');
            }
        }
        $validator = JsValidator::make($requestQuoteRules, [ 'no_of_guest.numeric' => 'Number of Guests must be numeric.' ]);

        $cmsPageInfo = $this->cmsPagesService->getPageRec('request-a-quote');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;

        return view('pages.user.request-a-quote', [
            'metafields' => $meta_fields,
            'validator' => $validator
        ]);
    }

    public function showUserChooseVendor(Request $request, VendorCategoryRepository $vendorCatRepo)
    {   
        $requestQuoteRules = [
            'no_of_guest' => 'sometimes|numeric|digits_between:1,10',
            'mobile_number' => 'phone_valid',
            'country' => 'required',
            'state'   => 'required',
            'city'   => 'required'
        ];
        $validator = Validator::make($request->all(), $requestQuoteRules);
        if ($validator->fails()) {
            return redirect()->route('request-a-quote')->withErrors($validator)->withInput();
        }
        if(Auth::check()) {
            $authUser = Auth::user();
            if(User::where('id', $authUser->id)->hasUserRole()->isActive()->exists() == false) {
                abort(403, 'Unauthorized access.');
            }
        }
        if($request->has('wedding_type_hindu') == true && $request->input('wedding_type_hindu') == 1) {
            $wedding_type = 'Hindu';
        } else if ($request->has('wedding_type_muslim') == true && $request->input('wedding_type_muslim') == 1) {
            $wedding_type = 'Muslim';
        } else if ($request->has('wedding_type_sikh') == true && $request->input('wedding_type_sikh') == 1) {
            $wedding_type = 'Sikh';
        } else if ($request->has('wedding_type_christian') == true && $request->input('wedding_type_christian') == 1) {
            $wedding_type = 'Christian';
        } else {
            $wedding_type = $request->session()->get('wedding_type', 'Hindu');
        }
        
        $no_of_guest = $request->has('no_of_guest')?$request->input('no_of_guest'):$request->session()->get('no_of_guest', '');
        $mobile_number = $request->has('mobile_number')?$request->input('mobile_number'):$request->session()->get('mobile_number', '');
        if($request->has('check_email_contact') == true && $request->input('check_email_contact') == 1) {
            $preferred_contact_method = 'email';
        } else if ($request->has('check_phone_contact') == true && $request->input('check_phone_contact') == 1) {
            $preferred_contact_method = 'phone';
        } else if ($request->has('check_message_contact') == true && $request->input('check_message_contact') == 1) {
            $preferred_contact_method = 'message';
        } else {
            $preferred_contact_method = $request->session()->get('check_message_contact', 'email');
        }
        $event_date = $request->has('event_date')?$request->input('event_date'):$request->session()->get('event_date', '');
        $country = $request->has('country')?$request->input('country'):$request->session()->get('country', '');
        $state = $request->has('state')?$request->input('state'):$request->session()->get('state', '');
        $city = $request->has('city')?$request->input('city'):$request->session()->get('city', '');
        if($country == '' || $state == '' || $city == '') {
            return redirect()->route('request-a-quote');
        }
        // save data in session
        $request->session()->put('wedding_type', $wedding_type);
        $request->session()->put('no_of_guest', $no_of_guest);
        $request->session()->put('mobile_number', $mobile_number);
        $request->session()->put('preferred_contact_method', $preferred_contact_method);
        $request->session()->put('event_date', $event_date);
        $request->session()->put('country', $country);
        $request->session()->put('state', $state);
        $request->session()->put('city', $city);

        $site_id = config('constants.site_id');
        $categories = $vendorCatRepo->getStatusRecords(1)->getVendorByType('wedding')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->where('name', '<>', 'Event Organizer')->get(['id', 'name', 'slug']);
        $validator = JsValidator::make(
            [
                'categories.*' => 'required'
            ]);
        $cmsPageInfo = $this->cmsPagesService->getPageRec('request-a-quote');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;

        return view('pages.user.user-choose-vendor', [
                'metafields' => $meta_fields,    
                'categories' => $categories,
                'wedding_type' => $wedding_type,
                'no_of_guest' => $no_of_guest,
                'mobile_number' => $mobile_number,
                'preferred_contact_method' => $preferred_contact_method,
                'event_date' => $event_date,
                'country' => $country,
                'state' => $state,
                'city' => $city,
                'validator' => $validator
            ]
        );
    }

    public function showUserVendorMessage(Request $request, VendorCategoryRepository $vendorCatRepo)
    {
        if(Auth::check()) {
            $authUser = Auth::user();
            if(User::where('id', $authUser->id)->hasUserRole()->isActive()->exists() == false) {
                abort(403, 'Unauthorized access.');
            }
        }
        $wedding_type = $request->has('wedding_type')?$request->input('wedding_type'):$request->session()->get('wedding_type', '');
        $no_of_guest = $request->has('no_of_guest')?$request->input('no_of_guest'):$request->session()->get('no_of_guest', '');
        $mobile_number = $request->has('mobile_number')?$request->input('mobile_number'):$request->session()->get('mobile_number', '');
        $preferred_contact_method = $request->has('preferred_contact_method')?$request->input('preferred_contact_method'):$request->session()->get('preferred_contact_method', '');
        $event_date = $request->has('event_date')?$request->input('event_date'):$request->session()->get('event_date', '');
        $country = $request->has('country')?$request->input('country'):$request->session()->get('country', '');
        $state = $request->has('state')?$request->input('state'):$request->session()->get('state', '');
        $city = $request->has('city')?$request->input('city'):$request->session()->get('city', '');
        $categories = $request->has('categories')?$request->input('categories'):explode(',', $request->session()->get('comma_sep_categories', ''));
        if($wedding_type == '' || $country == '' || $state == '' || $city == '') {
            return redirect()->route('request-a-quote');
        }
        if($categories == '') {
            return redirect()->route('request-a-quote')->withErrors(['Please choose Vendor.']);
        }
        if(User::getVendorCatCountryStateCity(implode(',', $categories), $country)->count() == 0) {
            return redirect()->route('request-a-quote')->withErrors(['Please choose different vendors as there are no vendors with this criteria matched with country you selected.']);
        }

        // check submited categories and prepare an array
        $chosen_cats = [];
        $i = 0;
        foreach($categories as $category) {
            $single_cat = $vendorCatRepo->getStatusRecords(1)->getVendorByType('wedding')->where('name', '<>', 'Event Organizer')->where('id', $category)->first(['id', 'name', 'slug']);
            $chosen_cats[$i]['id'] = $single_cat->id;
            $chosen_cats[$i]['name'] = $single_cat->name;
            $chosen_cats[$i]['slug'] = $single_cat->slug;
            $i++;
        }

        // validation rule
        $validationRules = [
            'description.*'  => 'sometimes|max:300'
        ];
        // save categories in session
        $request->session()->put('comma_sep_categories', implode(',', $categories));

        $cmsPageInfo = $this->cmsPagesService->getPageRec('request-a-quote');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;

        $validator = JsValidator::make($validationRules);
        return view('pages.user.user-vendor-message', [
                'metafields' => $meta_fields,
                'categories' => $categories,
                'wedding_type' => $wedding_type,
                'no_of_guest' => $no_of_guest,
                'mobile_number' => $mobile_number,
                'preferred_contact_method' => $preferred_contact_method,
                'event_date' => $event_date,
                'country' => $country,
                'state' => $state,
                'city' => $city,
                'chosen_cats' => $chosen_cats,
                'validator' => $validator
            ]
        );
    }

    public function showUserVendorMail(Request $request)
    {
        if(Auth::check()) {
            $authUser = Auth::user();
            if(User::where('id', $authUser->id)->hasUserRole()->isActive()->exists() == false) {
                abort(403, 'Unauthorized access.');
            }
        }
        $wedding_type = $request->has('wedding_type')?$request->input('wedding_type'):$request->session()->get('wedding_type', '');
        $no_of_guest = $request->has('no_of_guest')?$request->input('no_of_guest'):$request->session()->get('no_of_guest', '');
        $mobile_number = $request->has('mobile_number')?$request->input('mobile_number'):$request->session()->get('mobile_number', '');
        $preferred_contact_method = $request->has('preferred_contact_method')?$request->input('preferred_contact_method'):$request->session()->get('preferred_contact_method', '');
        $event_date = $request->has('event_date')?$request->input('event_date'):$request->session()->get('event_date', '');
        $country = $request->has('country')?$request->input('country'):$request->session()->get('country', '');
        $state = $request->has('state')?$request->input('state'):$request->session()->get('state', '');
        $city = $request->has('city')?$request->input('city'):$request->session()->get('city', '');
        $categories = $request->has('categories')?$request->input('categories'):$request->session()->get('comma_sep_categories', '');
        $description = $request->has('description')?$request->input('description'):'';
        if($country == '' || $state == '' || $city == '') {
            return redirect()->route('request-a-quote');
        }
        if($categories == '') {
            return redirect()->route('request-a-quote')->withErrors(['Please choose Vendor.']);
        }
        if(User::getVendorCatCountryStateCity($categories, $country)->count() == 0) {
            return redirect()->route('request-a-quote')->withErrors(['Please choose different vendors as there are not vendors with this category matched with country you selected.']);
        }
        if(!empty($description)) {
            foreach($description as $messg) {
                if(strlen($messg) > 300) {
                    return redirect()->route('request-a-quote')->withErrors(['Message characters should be less than 300.']);
                }
            }
        }

        $validation_rules = [
            'email'                     => 'required|max:255|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
            'first_name'                => 'required|alpha_spaces|max:25',
            'last_name'                 => 'required|alpha_spaces|max:25',
            'password'                  => 'required|min:6|max:30',
            'categories'                => 'required',
            'wedding_type'              => 'required',
            'no_of_guest'               => 'sometimes|numeric|digits_between:1,10',
            'mobile_number'             => 'phone_valid',
            'preferred_contact_method'  => 'required',
            'country'                   => 'required',
            'state'                     => 'required',
            'city'                      => 'required'
        ];
        if(Auth::check()) {
            unset($validation_rules['email']);
            unset($validation_rules['password']);
        }
        $validator = JsValidator::make(
            $validation_rules, [ 'no_of_guest.numeric' => 'Number of Guests must be numeric.' ]
        );

        $cmsPageInfo = $this->cmsPagesService->getPageRec('request-a-quote');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;

        // save description in session var
        $request->session()->put('description_request_a_quote', $description);

        return view('pages.user.user-vendor-mail', [
                'metafields' => $meta_fields,
                'categories' => $categories,
                'wedding_type' => $wedding_type,
                'no_of_guest' => $no_of_guest,
                'mobile_number' => $mobile_number,
                'preferred_contact_method' => $preferred_contact_method,
                'event_date' => $event_date,
                'country' => $country,
                'state' => $state,
                'city' => $city,
                'first_name' => (Auth::check())?$authUser->first_name:'',
                'last_name' => (Auth::check())?$authUser->last_name:'',
                'email' => (Auth::check())?$authUser->email:'',
                'description' => $description,
                'validator' => $validator
            ]
        );
    }

    public function handleUserVendorRequestAQuote(Request $request, VendorCategoryRepository $vendorCatRepo)
    {
        if(Auth::check()) {
            $authUser = Auth::user();
            if(User::where('id', $authUser->id)->hasUserRole()->isActive()->exists() == false) {
                return response()->json(
                    [
                        'status' => 0,
                        'message' => 'You are not allow to do this action. Please login as a User.'
                    ], 403);
            }
        }
        $wedding_type = $request->has('wedding_type')?$request->input('wedding_type'):$request->session()->get('wedding_type', '');
        $no_of_guest = $request->has('no_of_guest')?$request->input('no_of_guest'):$request->session()->get('no_of_guest', '');
        $mobile_number = $request->has('mobile_number')?$request->input('mobile_number'):$request->session()->get('mobile_number', '');
        $preferred_contact_method = $request->has('preferred_contact_method')?$request->input('preferred_contact_method'):$request->session()->get('preferred_contact_method', '');
        $event_date = $request->has('event_date')?$request->input('event_date'):$request->session()->get('event_date', '');
        $country = $request->has('country')?$request->input('country'):$request->session()->get('country', '');
        $state = $request->has('state')?$request->input('state'):$request->session()->get('state', '');
        $city = $request->has('city')?$request->input('city'):$request->session()->get('city', '');
        $categories = $request->has('categories')?$request->input('categories'):$request->session()->get('comma_sep_categories', '');
        $description = $request->has('description')?$request->input('description'):'';
        $first_name = $request->has('first_name')?$request->input('first_name'):'';
        $last_name = $request->has('last_name')?$request->input('last_name'):'';
        $email = $request->has('email')?$request->input('email'):$authUser->email;
        $password = $request->has('password')?$request->input('password'):'';
        // remove session values for request a quote
        $request->session()->forget(['wedding_type', 'no_of_guest', 'mobile_number', 'preferred_contact_method', 'event_date', 'country', 'state', 'city', 'comma_sep_categories', 'description_request_a_quote']);
        $validation_rules = [
            'description.*'             => 'sometimes|max:300',
            'email'                     => 'required|max:255|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
            'first_name'                => 'required|alpha_spaces|max:25',
            'last_name'                 => 'required|alpha_spaces|max:25',
            'password'                  => 'required|min:6|max:30',
            'categories'                => 'required',
            'wedding_type'              => 'required',
            'no_of_guest'               => 'sometimes|numeric|digits_between:1,10',
            'mobile_number'             => 'phone_valid',
            'preferred_contact_method'  => 'required',
            'country'                   => 'required',
            'state'                     => 'required',
            'city'                      => 'required'
        ];
        if(Auth::check()) {
            unset($validation_rules['email']);
            unset($validation_rules['password']);
        }
        $validator = Validator::make($request->all(), $validation_rules);
        // handle request and process
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        
        // validate any contact should be selected
        if($preferred_contact_method == "") {
            return response()->json(
                [
                    'status' => 0,
                    'message' => 'Please select any one preferred contact method.'
                ]
            );
        }

        // validate for phone number is selected
        if($preferred_contact_method == "phone") {
            if($mobile_number == '') {
                return response()->json(
                    [
                        'status' => 0,
                        'message' => 'Please enter phone number.'
                    ]
                );
            }
        }
        // event date
        $expld_date = explode('.', $event_date);
        $event_date = $expld_date[2].'-'.$expld_date[1].'-'.$expld_date[0];
        // validate
        $event_date_check = strtotime(date('Y-m-d', strtotime($event_date) ) );
        $todays = strtotime(date('Y-m-d'));

        if($event_date_check < $todays) {
            return response()->json(
                [
                    'status' => 0,
                    'message' => 'Event date cann\'t be past date.'
                ]
            );
        }
        if(!Auth::check()) {
            // check out the email and password combination
            if(User::where('email', $email)->hasUserRole()->exists()) {
                $password_chk = User::where('email', $email)->hasUserRole()->isActive()->first();
                if (Hash::check($request->password, $password_chk->password, [])) {
                    $user = User::where('email', $email)->hasUserRole()->first();
                } else {
                    return response()->json(
                        [
                            'status' => 0,
                            'message' => 'You are existing user but credentials are not matching, please enter correct Email and Password. OR Contact Site Admin.'
                        ]
                    );
                }
            } else {
                // check user is having other role
                if(User::where('email', $request->input('email'))->hasVendorRole()->exists()) {
                    return response()->json(
                            [
                                'status' => 0,
                                'message' => 'You are not allowing to send message! please login as User.'
                            ]
                        );
                }
                $ipAddress = new CaptureIpTrait();
                $role = Role::where('slug', '=', 'unverified')->first();
                // Register the new user or whatever.
                $user = User::create([
                    'name'                  => $first_name.".".$last_name,
                    'first_name'            => $first_name,
                    'last_name'             => $last_name,
                    'email'                 => $email,
                    'password'              => Hash::make($password),
                    'token'                 => str_random(64),
                    'mobile_number'         => $mobile_number,
                    'signup_ip_address'     => $ipAddress->getClientIp(),
                    'activated'             => 1,
                    'access_grant'          => 'Y',
                    'site_id'               => (int) config('constants.site_id')
                ]);

                $user->attachRole($role);
                // send welcome email
                $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'User Registration',
                    'mail_replace_vars' => [
                        '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                        '[%WEB_NAME%]' => getGeneralSiteSetting('site_name'),
                        '[%USER_FIRST_NAME%]' => $user->first_name,
                        '[%USER_EMAIL%]' => $user->email
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
            }
        } else {
            $user = Auth::user();
        }

        // get categories vendor by localtion
        if(User::getVendorCatCountryStateCity($categories, $country, $state, $city)->count() > 0) {
            $vendors_listing = User::getVendorCatCountryStateCity($categories, $country, $state, $city)->get(['id', 'email','first_name', 'last_name', 'token']);
        } else if(User::getVendorCatCountryStateCity($categories, $country, $state)->count() > 0) {
            $vendors_listing = User::getVendorCatCountryStateCity($categories, $country, $state)->get(['id', 'email','first_name', 'last_name', 'token']);
        } else if(User::getVendorCatCountryStateCity($categories, $country)->count() > 0) {
            $vendors_listing = User::getVendorCatCountryStateCity($categories, $country)->get(['id', 'email','first_name', 'last_name', 'token']);
        }

        try {
            if($vendors_listing != null) {
                foreach($vendors_listing as $single_vendor) {
                    $message_send = '';
                    $vendor_id = $single_vendor->id;
                    $vendor_category = $single_vendor->vendors_information->vendor_category;
                    if(isset($description[$vendor_category])) {
                        $vendor_cat_info_main = $vendorCatRepo->getRecords($vendor_category);
                        $vendor_category_name_main = $vendor_cat_info_main->name;
                        $message_send .= 'Category: '.$vendor_category_name_main.'<br>';
                        $message_send .= 'Message: '.$description[$vendor_category].'<br>';
                    }

                    // check for showcase category
                    $vendor_showcase = VendorShowcase::where('vendor_id', $vendor_id)->first();
                    if($vendor_showcase->additional_categories != '') {
                        $showcase_cats = unserialize($vendor_showcase->additional_categories);
                        if(count($showcase_cats) > 0) {
                            foreach($showcase_cats as $showcase_category) {
                                // check category exists in submitted
                                $cat_arry = explode(',', $categories);
                                if(in_array($showcase_category, $cat_arry)) {
                                    $vendor_cat_info = $vendorCatRepo->getRecords($showcase_category);
                                    $vendor_category_name = $vendor_cat_info->name;
                                    $message_send .= 'Category: '.$vendor_category_name.'<br>';
                                    $message_send .= 'Message: '.$description[$showcase_category].'<br>';
                                }
                            }
                        }
                    }
                    
                    $message_to_send = 'New price request<br><br>';
                    $message_to_send .= 'Check below details<br>';
                    if($wedding_type != '') {
                        $message_to_send .= 'Wedding Type:'.$wedding_type.'<br>';
                    }
                    if($event_date != '') {
                        $message_to_send .= 'Event Date:'.date('jS \of F Y', strtotime($event_date)).'<br>';
                    }
                    if($no_of_guest != '') {
                        $message_to_send .= 'No. of Guest:'.$no_of_guest.'<br>';
                    }
                    if($mobile_number != '') {
                        $message_to_send .= 'Phone:'.$mobile_number.'<br>';
                    }
                    if($preferred_contact_method != '') {
                        $message_to_send .= 'Preferred Contact Method:'.$preferred_contact_method.'<br>';
                    }
                    if($message_send != '') {
                        $message_to_send .= 'Specific Message(s)<br>'.$message_send.'<br>';
                    }
                    // send message to vendor
                    Message::create([
                        'sender_id'         => $user->id,
                        'receiver_id'       => $vendor_id,
                        'sender_archive'    => 'N',
                        'message'           => $message_to_send,
                        'created_at'        => Carbon::now()
                    ]);
                    
                    // send email to vendor
                    $unsubscribe_link = route('unsubscribe', [ 'token' => $single_vendor->token, 'type' => '' ]);
                    $mail_req_arry = [
                        'mail_name' => 'User Price Request',
                        'mail_replace_vars' => [
                            '[%FIRST_NAME%]' => $single_vendor->first_name,
                            '[%LAST_NAME%]' => $single_vendor->last_name,
                            '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                            '[%WEBSITENAME%]' => getGeneralSiteSetting('site_name')
                        ],
                        'unsubscribe_link' => $unsubscribe_link
                    ];
                    Mail::to($single_vendor->email)->send(new GeneralEmail($mail_req_arry));
                    if(config('mail.host') == 'smtp.mailtrap.io'){
                        sleep(5); //use usleep(500000) for half a second or less
                    }   
                    // save price ticket information
                    UserPriceRequest::Create(
                        [
                            'vendor_id' => $vendor_id,
                            'user_id' => $user->id,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'mobile_number' => $mobile_number,
                            'event_date' => $event_date,
                            'preferred_contact_method' => $preferred_contact_method,
                            'about_extra_needs' => $message_send,
                            'no_of_guest' => $no_of_guest,
                            'wedding_type' => $wedding_type
                        ]
                    );
                }
            }
        } catch (Exception $e) {
            $vendor_error['message'] = "Something went wrong while sending message.";
            return response()->json($vendor_error);
        }
        return response()->json(
            [
                'status' => 1,
                'message' => 'Message sent successfully.'
            ]
        );
    }
}