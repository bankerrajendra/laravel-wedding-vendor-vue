<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use JsValidator;
use App\Models\Review;
use App\Models\User;
use App\Logic\Admin\ReviewRepository;

class ReviewsController extends Controller
{
    private $validation_rules = [
        'message'           => 'required|min:50|max:500',
        'selected_rating'   => 'required|numeric|between:1,5'
    ];

    private $validation_messages = [
        'message.required'          => "Comment field is required.",
        'message.min'               => "Comment should be minimum 50 characters length.",
        'message.max'               => "Comment should be maximum 500 characters length.",
        'selected_rating.required'  => "Rating field is required.",
        'selected_rating.numeric'   => "Rating must be a number.",
        'selected_rating.between'   => "Rating must be between 1 and 5."
    ];

    /**
     * Get Reviews
     */
    public function getReviews(Request $request, ReviewRepository $review_repo)
    {
        $per_page = $request->has('per_page') ? $request->input('per_page') : 10;
        $page = $request->has('per_page') ? $request->input('per_page') : 1;
        $reviews = $review_repo->getRecords()
                            ->where('approved', '=', "1")
                            ->latest()
                            ->paginate($per_page);

        return view('pages.review.list', [
            'reviews'   => $reviews,
            'per_page'  => $per_page,
            'page'      => $page
        ]);
    }

    /**
     * Write Review
     */
    public function showWriteReview($vendorId, ReviewRepository $review_repo)
    {
        $authUser = Auth::user();
        if(User::where('id', $authUser->id)->hasVendorRole()->exists()) {
            return view('403', ['error_message' => 'You must be logged in as a user to post reviews']);
        }
        $logged_user = Auth::id();
        if(User::where('id', base64_decode($vendorId))->hasVendorRole()->exists() == false || User::where('id', $logged_user)->isActive()->hasUserRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        // check is there review already added by user.
        $user_review = $review_repo->getUserReview($logged_user, base64_decode($vendorId));
        // vendor name
        $vendorInfo = User::where('id', base64_decode($vendorId))->hasVendorRole()->first(['id', 'first_name', 'last_name']);
        $validator = JsValidator::make($this->validation_rules, $this->validation_messages);
        return view('pages.user.write-review', [
            'vendor_id' => $vendorId,
            'user_review' => $user_review,
            'vendorInfo' => $vendorInfo,
            'validator' => $validator
        ]);
    }

    /**
     * Handle submit review action
     */
    public function handleSubmitReview(Request $request, ReviewRepository $review_repo)
    {
        $user_id = Auth::id();
        $vendor_id = base64_decode($request->input('vendor_id'));
        if(User::where('id', $vendor_id)->hasVendorRole()->exists() == false || User::where('id', $user_id)->isActive()->hasUserRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        
        $validator = Validator::make($request->all(), $this->validation_rules, $this->validation_messages );
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        // check is there review already added by user.
        $user_review = $review_repo->getUserReview($user_id, $vendor_id);
        try {
            if($user_review != '') {
                if($request->input('message') != $user_review->message) {
                    $user_review->message = $request->input('message');
                    $user_review->rating = $request->input('selected_rating');
                    $user_review->approved = '0';
                    $user_review->save();
                }
            } else {
                Review::create([
                    'user_id'   => $user_id,
                    'vendor_id' => $vendor_id,
                    'message'   => $request->input('message'),
                    'rating'    => $request->input('selected_rating'),
                    'approved'  => '0'
                ]);
            }
            $vendorObj = User::where('id', $vendor_id)->first();
            return redirect($vendorObj->getVendorProfileLink());
            //return back()->withSuccess('Your review has been successfully submitted.');
        } catch(\Exception $e) {
            return back()->withErrors('Something wrong while adding review.')->withInput();
        }
    }

    /**
     * Admin Lists
     */
    public function showAdminListings(Request $request, ReviewRepository $review_repo)
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $status         = $request->has('status') ? $request->input('status') : "";
        $search         = $request->has('search') ? $request->input('search') : "";
        $reviews        = $review_repo->getRecords();

        if($search != "") {
            $reviews = $review_repo->getSearchRecords($reviews, $search);
        }

        if($status != "") {
            $reviews  = $reviews->where('approved', '=', $status);
        }
        $reviews = $reviews->latest()
                ->paginate($per_page);          
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $approved_counts        = $review_repo->getStatusRecords('1')
                                ->count();
        $unaprroved_counts      = $review_repo->getStatusRecords('0')
                                ->count();
        $all_counts             = $review_repo->getRecords()
                                ->count();

        return view('pages.admin.reviews.list', [
            'reviews'               => $reviews,
            'per_page'              => $per_page,
            'status'                => $status,
            'search'                => $search,
            'approved_records'      => $approved_counts,
            'unaprroved_records'    => $unaprroved_counts,
            'all_records'           => $all_counts
        ]);
    }

    public function handleBulkActionReview(Request $request, ReviewRepository $review_repo)
    {
        $action       = $request->has('action') ? $request->input('action') : "";
        $review_ids   = $request->has('review_ids') ? $request->input('review_ids') : "";
        if( $action != "" && ($action == "delete" || $action == "1" || $action == "0") && !empty($review_ids) ) {
            $reviews = $review_repo->getRecords($review_ids);
            if($action == "0" || $action == "1") {
                try {
                    foreach($reviews as $review) {
                        $review->approved = $action;
                        $review->save();
                    }
                    return redirect()->back()->withSuccess('Review(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating review(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($reviews as $review) {
                        $review->delete();
                    }
                    return redirect()->back()->withSuccess('Review(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting review(s), please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    public function showEditReview($id, ReviewRepository $review_repo)
    {
        $validation_rules   = [
            'approved'      => 'required|numeric',
            'id'            => 'required|numeric'
        ];
        $validator          = JsValidator::make($validation_rules);
        
        // get single record
        $review             = $review_repo->getRecords($id);

        return view('pages.admin.reviews.edit', [
            'validator'     => $validator,
            'review'        => $review
        ]);
    }

    public function handleEditreview(Request $request, ReviewRepository $review_repo)
    {
        // initialize
        $approved                       = $request->has('approved') ? $request->input('approved') : "";
        $id                             = $request->has('id') ? $request->input('id') : "";

        // validate
        $validation_rules   = [
            'approved'      => 'required|numeric',
            'id'            => 'required|numeric'
        ];
        $validate                       = $request->validate($validation_rules);

        // save
        $save_review                    = $review_repo->getRecords($id);
        $save_review->approved          = $approved;

        // return with success or error
        if ($save_review->save()) {
            return redirect()->back()->withSuccess('Review updated successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in editing review, please try again.');
        }
    }

    public function showViewReview($id, ReviewRepository $review_repo)
    {
        // get single record
        $review                 = $review_repo->getRecords($id);

        return view('pages.admin.reviews.view', [
            'review'        => $review
        ]);
    }
}
