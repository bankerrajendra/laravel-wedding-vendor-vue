<?php

namespace App\Http\Controllers;

use App\Logic\Common\LocationRepository;
use App\Logic\Membership\MembershipRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use JsValidator;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Report;
use App\Logic\Admin\ReviewRepository;

//use Illuminate\Support\Facades\Mail;
class AccountController extends Controller
{

    protected $validationRules = [
        'current_password'          => 'required|confirmed',
        'new_password'              => 'required|min:6|max:30|different:current_password',
        'new_password_confirmation' => 'required|same:new_password',

        'country_code'          => 'required',
        'state_id'      	    => 'required',
        'city_id'    		    => 'required',
        'zipcode'   			=> 'required | min:5',
    ];

    public function __construct()
    {

    }

    public function settings(LocationRepository $locationRepository) {

        $authUser=Auth::user();
        $validator = JsValidator::make($this->validationRules);
        return view('pages.account.settings', with([
            'currUser'=>$authUser,
            'validator'=>$validator,
            'countries'=>$locationRepository->getCountries()
        ]));

    }

    public function saveShowSettings(Request $request){
        $user = Auth::user();
        //status, fieldname,
        //$user_information = $user->get_user_information();
        $fieldname = $request->input('fieldname');
        $status = $request->input('status');
        $tablename = $request->input('tablename');

        if($tablename=="users") {
            $user->$fieldname = $status;
            $user->save();
        } else if($tablename=="users_information") {
            $user_information = $user->get_user_information();
            $user_information->$fieldname = $status;
            $user_information->save();
        }

//        if($action == 'activity') {
//            $user_information->activity_notification = $value;
//            $user_information->save();
//        } elseif($action == 'instant_alert') {
//            $user_information->instant_alert_notification = $value;
//            $user_information->save();
//        }

        $output['status'] = 1;
        $output['msg'] = "Settings saved successfully";
        return response()->json($output);
    }

    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return response()->json(["error"=>"Your current password does not matches with the password you provided. Please try again."]);
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            //return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            return response()->json(["error"=>"New Password cannot be same as your current password. Please choose a different password."]);
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return response()->json(["success"=>"Password changed successfully !"]);
    }

    public function changeLocation(Request $request){
        $validatedData = $request->validate([
            'country_code'          => 'required',
            'state_id'      	    => 'required',
            'city_id'    		    => 'required',
            'zipcode'   			=> 'required | min:5',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->country_id 		= $request->get('country_code');
        $user->city_id 			= $request->get('city_id');
        $user->state_id 		= $request->get('state_id');
        $user->save();

        $userInformation 					= $user->get_user_information();
        $userInformation->zipcode 			= $request->get('zipcode');
        if($userInformation->save()){
            return response()->json([
                "success"=>"Location changed successfully !",
                "country"=>$user->country->name,
                "state"=>$user->state->name,
                "city"=>$user->city->name,
                "zipcode"=>$userInformation->zipcode,
            ]);
        } else {
            return response()->json(["error"=>"There is some problem in saving the location. Try again later!!"]);
        }
    }

    public function sharePhotoPermission(Request $request){
        $authUser=Auth::user();
        $sharedProfiles=$authUser->photonotify_users();
        return view('pages.account.shared-private',with(['page'=>'shared','sharedProfiles'=>$sharedProfiles]));
    }

    public function privatePhotoPermission(Request $request){
        $authUser=Auth::user();
        $sharedProfiles=$authUser->photonotify_by_users();
        return view('pages.account.shared-private',with(['page'=>'request','sharedProfiles'=>$sharedProfiles]));
    }

    public function blockedMembers(Request $request) {
        $authUser=Auth::user();
        $blockedProfiles=$authUser->blocked_users;
        return view('pages.account.blocked-profiles',with(['page'=>'request','blockedProfiles'=>$blockedProfiles]));
    }

    public function hiddenProfiles(Request $request) {
        $authUser=Auth::user();
        $hiddenProfiles=$authUser->skipped_users;
        return view('pages.account.hidden-profiles',with(['page'=>'request','hiddenProfiles'=>$hiddenProfiles]));
    }

    public function shortlistedUsers(Request $request) {
        $authUser=Auth::user();
        $shortlistedUsers = $authUser->shortlisted_users;
        return view('pages.account.shortlisted')->with(['shortlistedUsers'=>$shortlistedUsers]);
    }

    /**
     * For showing my membership page
     */
    public function showMyMembership(Request $request, MembershipRepository $membershiprepo)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $get_membership_history = $membershiprepo->getUserMembershipHistory();
        $latest_membership_record = $membershiprepo->getUserLatestMembershipHistory();
        return view('pages.account.my-membership', [
            'membership' => $get_membership_history,
            'latest_membership' => $latest_membership_record
        ]);
    }

    /**
     * For showing notification page
     */
    public function showNotification(Request $request)
    {
        $authUser = Auth::user();
        $user_info = $authUser->users_information;
        /**
         * Show user specific report on front flag notification
         */
        $reports = Report::where('admin_reply', '<>', '')
                        ->where('to_user', '=', Auth::id())
                        ->where('user_deleted', '0')
                        ->latest()
                        ->get([
                            'id',
                            'from_user', 
                            'to_user', 
                            'admin_reply', 
                            'to_user_reply',
                            'type', 
                            'created_at', 
                            'updated_at'
                        ]);

        return view('pages.account.notification', [
            'user_info' => $user_info,
            'reports' => $reports
        ]);
    }

    /**
     * For handle email notification
     */
    public function handleEmailNotification(Request $request)
    {
        $notify_sends_message = $request->has('notify_sends_message') ? $request->input('notify_sends_message') : "N";
        $notify_news_letter = $request->has('notify_news_letter') ? $request->input('notify_news_letter') : "N";
        
        $authUser = Auth::user();
        $user_info = $authUser->users_information;
        $user_info->notify_sends_message = $notify_sends_message;
        $user_info->notify_news_letter = $notify_news_letter;

        if($user_info->save()) {
            return redirect()->back()->withSuccess('Settings saved successfully.');
        } else {
            return redirect()->back()->withErrors('Some issue while saving notification settings.');
        }
    }

    /**
     * Handle user report reply
     */
    public function handleUserReportReply(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'to_user_reply' => 'required|string|max:250',
            'report_id' => 'required|numeric'
        ], 
        [
            'to_user_reply|required' => "Reply field is required",
            'to_user_reply|max' => "Reply field should not exceed 250 characters"
        ]);
        if ($validator->fails()) {
            return $response = [
                'status' => 0,
                'message' => $validator->messages()->first()
            ];
        }

        $report_id = $request->has('report_id') ? $request->input('report_id') : "";
        $to_user_reply = $request->has('to_user_reply') ? $request->input('to_user_reply') : "";
        
        try {
            // update the record
            $report = Report::where('id', '=', $report_id)
                        ->where('to_user', '=', Auth::id())
                        ->where('admin_reply', '<>', '')
                        ->first();
            $report->to_user_reply = $to_user_reply;
            $report->save();
            $response = [
                'status' => 1,
                'report_id' => $report_id,
                'message' => 'Reply submitted successfully'
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => 0,
                'message' => 'Some error, contact site administrator.'
            ];
        }
        return response()->json($response);
    }

    /**
     * Handle user report trash
     */
    public function handleUserReportTrash(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'report_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return $response = [
                'status' => 0,
                'message' => $validator->messages()->first()
            ];
        }

        $report_id = $request->has('report_id') ? $request->input('report_id') : "";
        try {
            // update the record
            $report = Report::where('id', '=', $report_id)
                        ->where('to_user', '=', Auth::id())
                        ->where('admin_reply', '<>', '')
                        ->first();
            $report->user_deleted = "1";
            $report->save();
            $response = [
                'status' => 1,
                'message' => 'Report trashed successfully'
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => 0,
                'message' => 'Some error, contact site administrator.'
            ];
        }
        return response()->json($response);
    }

    /**
     * Mark flag notification as read
     */
    public function markFlagNotificationRead(Request $request)
    {
        try {
            // update the record
            $reports = Report::where('to_user', '=', Auth::id())
                        ->where('is_read', '=', 'N')
                        ->get();
            foreach ($reports as $report) {
                $report->is_read = "Y";
                $report->save();
            }
            $response = [
                'status' => 1,
                'message' => 'Flag mark read successfully'
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => 0,
                'message' => 'Some error, contact site administrator.'
            ];
        }
        return response()->json($response);
    }

    // show user reviews
    public function getUserReviews(Request $request, ReviewRepository $review_repo)
    {
        $per_page = $request->has('per_page') ? $request->input('per_page') : 10;
        $page = $request->has('per_page') ? $request->input('per_page') : 1;
        $reviews = $review_repo->getRecords()
                            //->where('approved', '=', "1")
                            ->where('user_id', '=', Auth::id())
                            ->latest()
                            ->paginate($per_page);

        return view('pages.review.users', [
            'reviews'   => $reviews,
            'per_page'  => $per_page,
            'page'      => $page
        ]);
    }
}