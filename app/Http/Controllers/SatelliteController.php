<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logic\Admin\SatelliteRepository;

use JsValidator;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Integer;

class SatelliteController extends Controller {
    protected $SatelliteRepository;
    protected $validationRules = [
        'title' => 'required',
        'url'   => 'required|url',
        'logo'  => 'image|mimes:jpeg,jpg,png,gif,bmp|max:2048|dimensions:max_width=500,max_height=500',
        'favicon'  => 'image|mimes:jpeg,jpg,png,gif,ico|max:1024|dimensions:max_width=100,max_height=100'
    ];

    public function __construct(SatelliteRepository $SatelliteRepository)
    {
        $this->SatelliteRepository = $SatelliteRepository;
    }

    //Backend Functions
    public function getSatellites(Request $request) 
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $status         = $request->has('status') ? $request->input('status') : "";
        $search         = $request->has('search') ? $request->input('search') : "";
        $satellites     = $this->SatelliteRepository->getRecords();

        if($search != "") {
            $satellites = $this->SatelliteRepository->getSearchRecords($satellites, $search);
        }

        if($status != "") {
            $satellites  = $satellites->where('status', '=', $status);
        }
        $satellites = $satellites->latest()
                ->paginate($per_page);          
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $active_counts      = $this->SatelliteRepository->getStatusRecords(1)
                            ->count();
        $inactive_counts    = $this->SatelliteRepository->getStatusRecords(0)
                            ->count();
        $all_counts         = $this->SatelliteRepository->getRecords()
                            ->count();


        return view('pages.admin.satellite.list')->with(
            [
                'satellites'            => $satellites,
                'page'                  => $page,
                'per_page'              => $per_page,
                'status'                => $status,
                'search'                => $search,
                'active_records'        => $active_counts,
                'inactive_records'      => $inactive_counts,
                'all_records'           => $all_counts
            ]
        );
    }

    /**
     * Handle bulk action of list satellite
     */
    public function handleBulkActionSatellite(Request $request)
    {
        $action = $request->has('action') ? $request->input('action') : "";
        $ids    = $request->has('ids') ? $request->input('ids') : "";
        if( $action != "" && ($action == "delete" || $action == "1" || $action == "0") && !empty($ids) ) {
            $satellites = $this->SatelliteRepository->getRecords($ids);
            if($action == "1" || $action == "0") {
                try {
                    foreach($satellites as $satellite) {
                        $satellite->status = $action;
                        $satellite->save();
                    }
                    return redirect()->back()->withSuccess('Records(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating Records(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($satellites as $satellite) {
                        $this->SatelliteRepository->deleteRecord($satellite);
                    }
                    return redirect()->back()->withSuccess('Record(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting Record(s), please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    public function addSatellite() 
    {
        $validator = JsValidator::make($this->validationRules);
        return view('pages.admin.satellite.add', [
                'validator' => $validator
            ]
        );
    }

    public function editSatellite($id) 
    {
        $validator = JsValidator::make($this->validationRules);
        $satellite = $this->SatelliteRepository->getRecords($id);
        if($satellite) {
            return view('pages.admin.satellite.edit', [
                    'validator' => $validator,
                    'satellite' => $satellite
                ]
            );
        } else {
            return abort(404);
        }
    }

    public function handleAddSatellite(Request $request) 
    {
        $validator = Validator::make($request->all(), $this->validationRules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $status = $this->SatelliteRepository->addRecord($request);
        if($status['status'] == '1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }
    }

    public function handleEditSatellite(Request $request) 
    {
        $validator = Validator::make($request->all(), $this->validationRules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $status = $this->SatelliteRepository->updateRecord($request);
        if($status['status']=='1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }
    }
}