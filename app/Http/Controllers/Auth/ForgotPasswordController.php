<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use JsValidator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    protected $validation =['email' => 'required|email'];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        $meta_fields['title']="Forgot Password, Retrieve Password, Recover Account, ".getGeneralSiteSetting('site_name');
        $meta_fields['keyword']="Have you forget user profile password of the ".getGeneralSiteSetting('site_name')."? Please, Submit your mail id which you used to registration detail on ".getGeneralSiteSetting('site_name')." and get new passcode which you help to generate new password.";
        $meta_fields['description']="Have you forget user profile password of the ".getGeneralSiteSetting('site_name')."? Please, Submit your mail id which you used to registration detail on ".getGeneralSiteSetting('site_name')." and get new passcode which you help to generate new password. ";

        $validator = JsValidator::make($this->validation);
        return view('auth.passwords.email')->with([
            'validator' => $validator,'metafields'=>$meta_fields
        ]);
    }
}
