<?php

namespace App\Http\Controllers\Auth;

use App\Logic\Common\LocationRepository;
use App\Models\User;
use App\Models\UserProfileFlag;
use App\Models\UsersImage;
use App\Models\VendorsImage;
use App\Models\Message;
use App\Models\UsersInterest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Logic\Common\UserInformationOptionsRepository;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\UsersInformation;
use App\Models\VendorsInformation;
use App\Models\NewsLetterSubscription;
use App\Models\VendorFaq;
use App\Models\VendorFaqAnswer;
use App\Models\VendorVideo;
use Illuminate\Support\Facades\View;
use App\Logic\Admin\SatelliteRepository;
use App\Logic\Admin\VendorCategoryRepository;
use App\Models\VendorShowcase;
use App\Models\VendorCustomFaq;
use Illuminate\Support\Facades\Mail;
use App\Mail\CollectReviewEmail;
use App\Models\Review;
use App\Models\VendorsSavedReviewMessage;
use Illuminate\Support\Facades\Hash;
use App\Logic\Event\EventRepository;

class UserInformationController extends Controller
{

    protected $educationOptions;
    protected $interestOptions;
    protected $professionOptions;
    protected $user;
    protected $locationRepository;
    protected $reviewPerPage = 2;
    protected $eventRepository;

    protected $validationRules = [
        'education'             => 'required',
        'about'                 => 'required|min:50',
		'looking_for'           => 'required|min:50',
    ];

    protected $validationPasswordRules = [
        'current_password'          => 'required|confirmed',
        'new_password'              => 'required|min:6|max:30|different:current_password',
        'new_password_confirmation' => 'required|same:new_password'
    ];

    protected $validationVendorInfoRules = [
        'company_name'          => 'required|alpha_spaces|min:3|max:29',
        'established_year'      => 'past_year',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        'meta_keywords'         => 'required|max:500',
        'business_information'  => 'required',
        'specials'              => 'required',
        'mobile_country_code'	=> 'required',
        'mobile_number'         => 'required|numeric|phone',
        'address'               => 'required|max:200',
        'country'               => 'required',
        'state'                 => 'required',
        'city'                  => 'required',
        'zip'   			    => 'required | min:5',
        'business_website'      => 'validate_url',
        'facebook'              => 'validate_url',
        'instagram'             => 'validate_url',
        'twitter'               => 'validate_url',
        'linkedin'              => 'validate_url',
        'pinterest'             => 'validate_url'
    ];

    // validation rule for collect review
    protected $validCollectReviewRules = [
        'message' => 'required|max:500'
    ];

    // review validator rules string
    protected $stringValidReview = [
        'email.required' => 'Please add comma(,) separated emails.'
    ];

    /**
     * Create a new controller instance.
     *
     * @param UserInformationOptionsRepository $userInformationOptionsRepository
     * @param LocationRepository $locationRepository
     * @return void
     */
    public function __construct(UserInformationOptionsRepository $userInformationOptionsRepository, LocationRepository $locationRepository, EventRepository $eventRepository)
    {
        $this->educationOptions = $userInformationOptionsRepository->getEducationOptions();
        $this->interestOptions = $userInformationOptionsRepository->getInterestOptions();
        $this->professionOptions = $userInformationOptionsRepository->getProfessionOptions();
        $this->locationRepository = $locationRepository;
        $this->eventRepository = $eventRepository;
        $this->user = Auth::user();
        View::share('countries', $locationRepository->getCountries());
    }

    public function showUserInformationForm(){
        $validator = JsValidator::make($this->validationRules);
        return view('auth.user-information')->with([
            'educationOptions' => $this->educationOptions,
            'interestOptions'  => $this->interestOptions,
            'professionOptions' => $this->professionOptions,
            'do_you_pray'   => config('constants.do_you_pray'),
            'born_reverted'   => config('constants.born_reverted'),
            'food' => config('constants.food'),
            'drink' => config('constants.drink'),
            'smoke' => config('constants.smoke'),
            'validator'=>$validator,
        ]);
    }

    public function update(Request $request){
        $this->user = Auth::user();
        $validatedData =  $request->validate(
            $this->validationRules,
            [
                'education.required'             => trans('auth.educationRequired'),
                //'profession.required'            => trans('auth.professionRequired'),
                'food.required'                  => trans('auth.foodRequired'),
                'drink.required'                 => trans('auth.drinkRequired'),
                'smoke.required'                 => trans('auth.smokeRequired'),
                //'interest.required'              => trans('auth.interestRequired'),
                'about.required'                 => trans('auth.aboutRequired'),
                'about.min'                      => trans('auth.aboutMin'),
            ]
        );

        $user_information = $this->user->get_user_information();
        if($user_information){
            $user_information->drink = $request->input('drink');
            $user_information->smoke = $request->input('smoke');
            $user_information->food = $request->input('food');
            $user_information->education_id = $request->input('education');
            //$user_information->profession_id = $request->input('profession');
            $user_information->about = $request->input('about');
			$user_information->does_pray = $request->input('does_pray');
			$user_information->looking_for = $request->input('looking_for');			
			$user_information->is_born = $request->input('is_born');
			
            $user_information->save();
			 
            // UsersInterest::where('user_id', $this->user->id)->delete();
            // foreach ($request->input('interest') as $interest){
                // UsersInterest::create(
                    // [
                        // 'user_id'=> $this->user->id,
                        // 'interest_id'=>$interest
                    // ]
                // );
            // }
        }
        return redirect()->route('upload-image');
    }

    public function showUploadImageForm(UsersImage $userImage){

        $user = Auth::user();
        $where = array(
            'user_id' => $user->id
        );
        $existingImages = $userImage->getWhere($where);
		// if($existingImages == ''){
			// $existingImages[] = 'http://www.shaadichoice.com/img/male-default.png';
		// }
        $existingImagesCount = $userImage->getWhereCount($where);
        return view('auth.upload-image')->with([
            'existingImages' => $existingImages,
            'existingImagesCount' => $existingImagesCount
        ]);

    }

    public function showPendingInformationForm(){
        $user=Auth::user();
        if($user) {
            $email = $user->email;
        }
         $validationRules = [
            'day'                   => 'required',
            'month'                 => 'required',
            'year'                  => 'required',
            'country'               => 'required',
            'city'                  =>  'required',
            'ethnicity'             =>  'required',
            'email'                 =>  'required|email|max:255|unique:users',
        ];
        $countries = $this->locationRepository->getCountries();
        $validator = JsValidator::make($validationRules);
        return view('auth.pending-information')->with(
            [
                'countries'=>$countries,
                'validator'=>$validator,
                'email'=>$email
            ]
        );
    }

    public function savePendingInformation(Request $request){
        $user = Auth::user();
        $validator = Validator::make($request->all(),
            [
                'day'                   => 'required',
                'month'                 => 'required',
                'year'                  => 'required',
                'country'               => 'required',
                'city'                  =>  'required',
                'ethnicity'             =>  'required',
                'email'                 =>  'required|email|max:255|unique:users,email,'.$user->id.',id',
            ],
            [
                'email.unique'         => 'Email already exists.',
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        if($user){
            $user->gender = $request->input('gender');
            $user->country_id = $request->input('country');
            $user->city_id = $request->input('city');
            $user->date_of_birth = date_create($request->input('year')."-".$request->input('month')."-".$request->input('day'));
            $user->email = $request->input('email');
            $user->save();
            $user_information = $user->get_user_information();
            if($user_information){
                $user_information->ethnicity = $request->input('ethnicity');
                $user_information->save();
            }
            else{
                UsersInformation::create([
                    'user_id'           =>  $user->id,
                    'ethnicity'         =>  $request->input('ethnicity'),
                ]);
            }
        }
        return redirect()->route('public.home');
    }

    public function unsubscribe($token, $type = ''){
        if($type == "SB") { // for subscribers
            // validate the token passes
            $subscriber = NewsLetterSubscription::where('site_id', '=', config('constants.site_id'))
                                                    ->where('status', '=', "A")
                                                    ->where('email', '=', base64_decode($token))
                                                    ->first();
            if($subscriber) {
                $subscriber->status = "I";
                $subscriber->save();
                return view('pages.unsubscribed', [
                    'success' => "You have successfully unsubscribed from News Letter notifications!",
                    'error' => ''
                ]);
            } else {
                return view('pages.unsubscribed', [
                    'success' => "",
                    'error' => "Invalid Token!"
                ]);
            }
        } else {
            $user = User::where('token',$token)->first();
            if($user) {
                $user_id=$user->id;

                if($user->hasRole('unverified')) { // user role
                    $user_information=UsersInformation::where('user_id',$user_id)->first();
                    $user_information->notify_sends_message = 'N';
                    $user_information->notify_news_letter = 'N';
                    $user_information->save();
                }

                if($user->hasRole('vendor')) { // vendor role
                    $user_information = VendorsInformation::where('vendor_id',$user_id)->first();
                    $user_information->notify_sends_message = 'N';
                    $user_information->notify_favorites_me = 'N';
                    $user_information->notify_views_profile = 'N';
                    $user_information->notify_content_approve_deny = 'N';
                    $user_information->notify_news_letter = 'N';
                    $user_information->save();
                }

                return view('pages.unsubscribed', [
                    'success' => "You have successfully unsubscribed from email notifications!",
                    'error' => ""
                ]);
            } else {
                return view('pages.unsubscribed', [
                    'success' => "",
                    'error' => "Invalid Token!"
                ]);
            }
        }
    }

    public function showUnsubscribe()
    {
        return view('pages.unsubscribed', [
            'success' => "",
            'error' => ""
        ]);
    }

    public function showUploadCoverForm(VendorsImage $vendorImage)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $where = array(
            'vendor_id' => $user->id,
            'image_type' => 'cover'
        );
        $existingImages = $vendorImage->getWhere($where);
        $existingImagesCount = $vendorImage->getWhereCount($where);
        return view('auth.upload-vendor-cover')->with([
            'existingImages' => $existingImages,
            'existingImagesCount' => $existingImagesCount,
            'vendorObj' => $user
        ]);
    }

    public function showUploadProfileForm(VendorsImage $vendorImage)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $coverWhere = array(
            'vendor_id' => $user->id,
            'image_type' => 'cover'
        );
        $coverImage = $vendorImage->getWhere($coverWhere);

        $where = array(
            'vendor_id' => $user->id,
            'image_type' => 'profile'
        );
        $existingImages = $vendorImage->where('vendor_id', $user->id)
                            ->where('image_type', 'profile')
                            ->get();
        $existingImagesCount = $vendorImage->getWhereCount($where);

        $getMembershipStatus = $user->currentMembershipStatus();

        $status = $getMembershipStatus['status'];
        $public_photo_allowed = $getMembershipStatus['public_photo_allowed'];

        return view('auth.upload-vendor-profile')->with([
            'existingImages' => $existingImages,
            'existingImagesCount' => $existingImagesCount,
            'coverImage' => $coverImage,
            'vendorObj' => $user,
            'public_photo_allowed' => $public_photo_allowed,
            'status' => $status
        ]);
    }

    public function showVendorInfo()
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        
        $arryToPassView = ['userObj' => $user];
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        $arryToPassView['events'] = $events;
        $validation_rules = $this->validationVendorInfoRules;
        $validator = JsValidator::make($validation_rules);
        $arryToPassView['validator'] = $validator;
        
        return view('auth.vendor-business-info', $arryToPassView);
    }


    public function showVendorMobile()
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        //$validator = JsValidator::make($this->validationVendorInfoRules);
        return view('auth.vendor-mobile-info', [
            'userObj' => $user,
            'events' => $events
        ]);
    }


    public function showUserProfileMobile()
    {
        $user = Auth::user();

        if(User::where('id', $user->id)->hasUserRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }

        //$validator = JsValidator::make($this->validationVendorInfoRules);
        return view('auth.user-profile-mobile-info', [
            'userObj' => $user //,
            //  'validator' => $validator
        ]);
    }

    public function showUserSettingMobile()
    {
        $user = Auth::user();

        if(User::where('id', $user->id)->hasUserRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        return view('auth.user-setting-mobile-info', [
            'userObj' => $user
        ]);
    }

    public function showVendorSettingMobile()
    {
        $user = Auth::user();

        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        return view('auth.vendor-setting-mobile-info', [
            'userObj' => $user,
            'events' => $events
        ]);
    }

    public function showVendorFaqs(VendorCategoryRepository $vendorCatRepo)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        // get user's category faqs
        $category = $user->vendors_information->vendor_category;

        $catInfo = $vendorCatRepo->getRecords($category);

        $faqs = VendorFaq::where('categories', 'like', '%;s:'.strlen($category).':"'.$category.'";%')
                    ->where('status', 1)
                    ->orderBy('order', 'ASC')
                    ->get();
        
        foreach($faqs as $faq) {
            try {
                $faqAnswerObj = VendorFaqAnswer::where('vendor_id', $user->id)
                            ->where('faq_id', $faq->id)
                            ->first(['answer']);
                $faq->answer = $faqAnswerObj->answer;
            } catch (\Exception $e) {
                $faq->answer = '';
            }
        }
        if($catInfo->has_custom_faqs ==  1) {
            $custom_faqs = VendorCustomFaq::where('vendor_id', $user->id)
                    ->where('category_id', $category)
                    ->get();
        } else {
            $custom_faqs = [];
        }
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        return view('auth.vendor-faqs', [
            'userObj' => $user,
            'faqs' => $faqs,
            'hasAdminFaq' => $catInfo->has_admin_faqs,
            'hasCustomFaq' => $catInfo->has_custom_faqs,
            'customFaqs' => $custom_faqs,
            'events' => $events
        ]);
    }

    public function getVendorFaqsAjax()
    {
        $user = Auth::user();
        // get user's category faqs
        $category = $user->vendors_information->vendor_category;
        $custom_faqs = VendorCustomFaq::where('vendor_id', $user->id)
                ->where('category_id', $category)
                ->get();

        // prepare html and return
        return response()->json($custom_faqs);
    }

    public function showVendorPhotoUploadForm(VendorsImage $vendorImage)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $whereCover = array(
            'vendor_id' => $user->id,
            'image_type' => 'cover'
        );
        $coverImage = $vendorImage->getWhere($whereCover);
        $coverImageCount = $vendorImage->getWhereCount($whereCover);
        $whereProfile = array(
            'vendor_id' => $user->id,
            'image_type' => 'profile'
        );
        $profileImages = $vendorImage->where('vendor_id', $user->id)
                            ->where('image_type', 'profile')
                            ->get();
        $profileImagesCount = $vendorImage->getWhereCount($whereProfile);

        $getMembershipStatus = $user->currentMembershipStatus();

        $status = $getMembershipStatus['status'];
        $public_photo_allowed = $getMembershipStatus['public_photo_allowed'];
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        return view('auth.vendor-photo-upload')->with([
            'coverImage' => $coverImage,
            'profileImages' => $profileImages,
            'coverImageCount' => $coverImageCount,
            'profileImagesCount' => $profileImagesCount,
            'public_photo_allowed' => $public_photo_allowed,
            'status' => $status,
            'events' => $events
        ]);
    }

    public function showVendorVideo(VendorVideo $vendorVideos)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $validator = JsValidator::make(['video_title' => 'required', 'embed_code' => 'required|emebed_url', 'description' => 'required|max:500']);
        $where = array(
            'vendor_id' => $user->id
        );
        $videos = $vendorVideos->getWhere($where);
        $videosCount = $vendorVideos->getWhereCount($where);
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        return view('auth.vendor-video', [
            'userObj' => $user,
            'validator' => $validator,
            'videos' => $videos,
            'videosCount' => $videosCount,
            'events' => $events
        ]);
    }

    public function showVendorShowcaseForm( SatelliteRepository $satelliteRepo, VendorCategoryRepository $vendorCatRepo )
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $satellites = $satelliteRepo->getStatusRecords(1)->get(['id', 'title', 'url']);
        $site_id = config('constants.site_id');
        $categories = $vendorCatRepo->getStatusRecords(1)->getVendorByType('wedding')->where('name', '<>', 'Event Organizer')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->get(['id', 'name', 'slug']);
        $vendor_showcase = VendorShowcase::where('vendor_id', $user->id)->first();
        if($vendor_showcase) {
            if($vendor_showcase->additional_categories != '') {
                $showcase_cats = unserialize($vendor_showcase->additional_categories);
            } else {
                $showcase_cats = [];    
            }
            if($vendor_showcase->satellites != '') {
                $showcase_sats = unserialize($vendor_showcase->satellites);
            } else {
                $showcase_sats = [];
            }
        } else {
            $showcase_cats = [];
            $showcase_sats = [];
        }
        $vendor_category = $user->vendors_information->vendor_category;
        $vendor_cat_info = $vendorCatRepo->getRecords($vendor_category);
        $vendor_category_name = $vendor_cat_info->name;
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        return view('auth.vendor-showcase')->with([
            'satellites' => $satellites,
            'categories' => $categories,
            'vendor_category' => $vendor_category,
            'showcase_cats' => $showcase_cats,
            'showcase_sats' => $showcase_sats,
            'vendor_category_name' => $vendor_category_name,
            'events' => $events
        ]);
    }

    public function showUserUploadPhoto(UsersImage $usersImage)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasUserRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $where = array(
            'user_id' => $user->id,
            'image_type' => 'public'
        );
        $existingImages = $usersImage->getWhere($where);
        $existingImagesCount = $usersImage->getWhereCount($where);
        return view('auth.user-upload-photo')->with([
            'existingImages' => $existingImages,
            'existingImagesCount' => $existingImagesCount
        ]);
    }

    // show vendor collect review
    public function showVendorCollectReviews(Request $request)
    {
        $type = $request->has('type') ? $request->input('type') : "vendor";
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $validator = JsValidator::make($this->validCollectReviewRules, $this->stringValidReview);

        $user_send_req = [];
        // get users conversations user
        $receivers_id = Message::select('receiver_id')->where('sender_id', $user->id)
                            ->distinct()                    
                            ->get(['receiver_id']);
        $i=0;
        foreach($receivers_id as $receiver) {
            // get and check user
            if(User::where('id', $receiver->receiver_id)->hasUserRole()->isActive()->exists()) {
                $userObj = User::where('id', $receiver->receiver_id)->first(['id', 'email', 'first_name', 'last_name']);
                $user_send_req[$i]['user_name'] = $userObj->first_name . ' ' . $userObj->last_name;
                $user_send_req[$i]['id'] = $userObj->id;
                $user_send_req[$i]['email'] = $userObj->email;
                $i++;
            }
        }

        $senders_id = Message::select('sender_id')->where('receiver_id', $user->id)
                            ->distinct()                    
                            ->get(['sender_id']);
        
        foreach($senders_id as $sender) {
            // get and check user
            if(User::where('id', $sender->sender_id)->hasUserRole()->isActive()->exists()) {
                $userObjSen = User::where('id', $sender->sender_id)->first(['id', 'email', 'first_name', 'last_name']);
                $user_send_req[$i]['user_name'] = $userObjSen->first_name . ' ' . $userObjSen->last_name;
                $user_send_req[$i]['id'] = $userObjSen->id;
                $user_send_req[$i]['email'] = $userObjSen->email;
                $i++;
            }
        }

        // setup the collect review message
        if($user->vendors_saved_review_message != null) {
            // set messsage from record
            $review_message = $user->vendors_saved_review_message->review_message;
        } else { // default message
            $review_message = config('constants.default_collect_message');
        }
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);
        
        return view('auth.vendor-collect-reviews', [
            'user_arry' => $user_send_req,
            'review_message' => $review_message,
            'events' => $events,
            'type' => $type,
            'validator' => $validator
        ]);
    }

    // handle submit collect reviews
    public function handleSubmitVendorCollectReviews(Request $request)
    {
        $user_details = Auth::user();
        $users = $request->has('users') ? $request->input('users') : "";
        $type = $request->has('type') ? $request->input('type') : "vendor";
        if($type == 'event') {
            $events = $request->has('events') ? $request->input('events') : "";
            if($events == "") {
                return response()->json([
                    'status' => 0,
                    'message' => 'Select atleast one event.'
                ]);
            }
        }
        $emails = $request->has('emails') && $request->input('emails') != '' ? explode(',', $request->input('emails')) : "";
        $message = $request->has('message') ? $request->input('message') : "";
        
        $validator = Validator::make($request->all(), $this->validCollectReviewRules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        
        if($users == "" && $request->input('emails') == "") {
            return response()->json([
                'status' => 0,
                'message' => 'Select or Fill atlest User Id or Email field.'
            ]);
        }

        try {

            if($type == 'event') {
                if(count($events) > 0) {
                    foreach($events as $event) {
                        // get event details
                        $eventObj = $this->eventRepository->getEventBy('id', $event);
                        $email_message = '<br /><a class="btn" href="'.route('write-event-review', [$eventObj->getEncryptedId()]).'">Post Your Review</a><br /><br />';
                        $email_message .= nl2br($message);
                        $mail_req_arry = [
                            'mail_name' => 'Event Collect Reviews',
                            'mail_replace_vars' => [
                                '[%FIRST_NAME%]' => $user_details->first_name,
                                '[%LAST_NAME%]' => $user_details->last_name,
                                '[%EVENT_NAME%]' => $eventObj->event_name,
                                '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                                '[%WEBSITENAME%]' => getGeneralSiteSetting('site_name')
                            ],
                            'mail_body' => $email_message
                        ];
                        // send to users
                        if(!empty($users)) {
                            foreach($users as $user) {
                                if(User::where('id', $user)->hasUserRole()->isActive()->exists()) {
                                    $userObj = User::where('id', $user)->first(['id', 'email', 'first_name', 'last_name']);
                                    Mail::to($userObj->email)->send(new CollectReviewEmail($mail_req_arry));
                                    if(config('mail.host') == 'smtp.mailtrap.io'){
                                        sleep(5); //use usleep(500000) for half a second or less
                                    }
                                } else {
                                    return response()->json([
                                        'status' => 0,
                                        'message' => 'Some issue while send collect reviews. User not exists with selected.'
                                    ]);
                                }
                            }
                        }
                        // send to emails
                        if(!empty($emails)) {
                            foreach($emails as $email) {
                                Mail::to(trim($email))->send(new CollectReviewEmail($mail_req_arry));
                                if(config('mail.host') == 'smtp.mailtrap.io'){
                                    sleep(5); //use usleep(500000) for half a second or less
                                }
                            }
                        }
                        // send to vendor
                        Mail::to($user_details->email)->send(new CollectReviewEmail($mail_req_arry));
                        if(config('mail.host') == 'smtp.mailtrap.io'){
                            sleep(5); //use usleep(500000) for half a second or less
                        }
                    }
                }
            } else {
                $email_message = '<br /><a class="btn" href="'.route('write-review', [$user_details->getEncryptedId()]).'">Post Your Review</a><br /><br />';
                $email_message .= nl2br($message);
                $mail_req_arry = [
                    'mail_name' => 'Vendor Collect Reviews',
                    'mail_replace_vars' => [
                        '[%FIRST_NAME%]' => $user_details->first_name,
                        '[%LAST_NAME%]' => $user_details->last_name,
                        '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                        '[%WEBSITENAME%]' => getGeneralSiteSetting('site_name')
                    ],
                    'mail_body' => $email_message
                ];

                // send to users
                if(!empty($users)) {
                    foreach($users as $user) {
                        if(User::where('id', $user)->hasUserRole()->isActive()->exists()) {
                            $userObj = User::where('id', $user)->first(['id', 'email', 'first_name', 'last_name']);
                            Mail::to($userObj->email)->send(new CollectReviewEmail($mail_req_arry));
                        } else {
                            return response()->json([
                                'status' => 0,
                                'message' => 'Some issue while send collect reviews. User not exists with selected.'
                            ]);
                        }
                    }
                }
                // send to emails
                if(!empty($emails)) {
                    foreach($emails as $email) {
                        Mail::to(trim($email))->send(new CollectReviewEmail($mail_req_arry));
                    }
                }
                // send to vendor
                Mail::to($user_details->email)->send(new CollectReviewEmail($mail_req_arry));

            }
            
            // save message to respected vendor
            if(VendorsSavedReviewMessage::where('vendor_id', $user_details->id)->exists()) {
                $reviewMessage = VendorsSavedReviewMessage::where('vendor_id', $user_details->id)->first();
                $reviewMessage->vendor_id = $user_details->id;
                $reviewMessage->review_message = $message;
                $reviewMessage->save();
            } else {
                $reviewMessage = new VendorsSavedReviewMessage;
                $reviewMessage->vendor_id = $user_details->id;
                $reviewMessage->review_message = $message;
                $reviewMessage->save();
            }
            
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => $e->getMessage()
            ]);
        }
        return response()->json([
            'status' => 1,
            'message' => 'Mail sent successfully.'
        ]);
    }

    // show vendor reviews
    public function showVendorReviews(Request $request)
    {
        $vendor = Auth::user();
        if(User::where('id', $vendor->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        // get reviews
        $reviewsObj = Review::where('vendor_id', '=', $vendor->id)
                        ->where('approved', '=', '1')
                        ->latest()
                        ->paginate($this->reviewPerPage);
        $reviews = [];
        $i=0;
        foreach($reviewsObj as $reviewSingle) {
            $reviewUsr = User::where('id', $reviewSingle->user_id)->first(['first_name', 'last_name']);
            $reviews[$i]['name'] = $reviewUsr->first_name . ' ' . $reviewUsr->last_name;
            $reviews[$i]['message'] = $reviewSingle->message;
            $reviews[$i]['rating'] = $reviewSingle->rating;
            $i++;
        }

        $totalReviews = Review::where('vendor_id', '=', $vendor->id)
                        ->where('approved', '=', '1')
                        ->count();
        $events = $this->eventRepository->getLoggedInEvents()->get(['id', 'event_name', 'event_slug']);

        return view('auth.vendor-reviews', [
            'reviews' => $reviews,
            'review_page_page' => $this->reviewPerPage,
            'totalReviews' => $totalReviews,
            'vendorObj' => $vendor,
            'events' => $events
        ]);
    }
    // vendor password change
    public function showChangePassword()
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }

        $validator = JsValidator::make($this->validationPasswordRules);
        
        return view('pages.user.vendor-change-password', [
            'user' => $user, 
            'validator' => $validator
        ]);   
    }

    public function handleChangePassword(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return response()->json(["error"=>"Your current password does not matches with the password you provided. Please try again."]);
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return response()->json(["error"=>"New Password cannot be same as your current password. Please choose a different password."]);
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);
        
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            return response()->json(["error"=>"Unauthorized action."], 403);
        }

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return response()->json(["success"=>"Password changed successfully !"]);
    }

    public function showVendorNotification()
    {
        $authUser = Auth::user();
        if(User::where('id', $authUser->id)->hasVendorRole()->exists() == false) {
            return response()->json(["error"=>"Unauthorized action."], 403);
        }
        $user_info = $authUser->vendors_information;

        return view('pages.user.vendor-notification', ['user_info' => $user_info]);
    }

    // handle vendor email notification
    public function handleVendorEmailNotification(Request $request)
    {
        $authUser = Auth::user();
        if(User::where('id', $authUser->id)->hasVendorRole()->exists() == false) {
            return response()->json(["error"=>"Unauthorized action."], 403);
        }
        $notify_sends_message = $request->has('notify_sends_message') ? $request->input('notify_sends_message') : "N";
        $notify_favorites_me = $request->has('notify_favorites_me') ? $request->input('notify_favorites_me') : "N";
        $notify_views_profile = $request->has('notify_views_profile') ? $request->input('notify_views_profile') : "N";
        $notify_content_approve_deny = $request->has('notify_content_approve_deny') ? $request->input('notify_content_approve_deny') : "N";
        $notify_news_letter = $request->has('notify_news_letter') ? $request->input('notify_news_letter') : "N";
        
        $authUser = Auth::user();
        $user_info = $authUser->vendors_information;
        $user_info->notify_sends_message = $notify_sends_message;
        $user_info->notify_favorites_me = $notify_favorites_me;
        $user_info->notify_views_profile = $notify_views_profile;
        $user_info->notify_content_approve_deny = $notify_content_approve_deny;
        $user_info->notify_news_letter = $notify_news_letter;

        if($user_info->save()) {
            return redirect()->back()->withSuccess('Settings saved successfully.');
        } else {
            return redirect()->back()->withErrors('Some issue while saving notification settings.');
        }
    }

    public function showVendorAccountDelete()
    {
        $user = Auth::user();

        return view('pages.user.vendor-account-delete', ['account_status' => $user->account_show]);
    }

    public function handleHideShowAccount(Request $request)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->exists() == false) {
            return response()->json(["error"=>"Unauthorized action."], 403);
        }

        //Change Hide show part
        $user = Auth::user();
        if($request->has('action') && $request->input('action') != "") {
            if($request->input('action') == 'hide') {
                $user->account_show = 'N';
            } else {
                $user->account_show = 'Y';
            }
        }
        $user->save();

        return response()->json(["success"=>"Setting saved successfully !"]);
    }

    // get logged in events for vendor
    protected function getEvents()
    {
        
    }
}
