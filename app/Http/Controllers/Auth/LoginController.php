<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use JsValidator;
use App\Models\User;
use App\Logic\Admin\CmsPagesRepository;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectAfterLogout = '/';
    protected $validation = [
        'email' => 'required|email', //required|email|max:255|
        'password' => 'required|string',
    ];
    // redirect user after login
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = url()->previous();
        $this->middleware('guest', ['except' => 'logout']);
    }

    // add site id as condition for login
    protected function credentials(Request $request)
    {
        if($request->input('email') == 'admin@admin.com') {
            return $request->only('email', 'password');
        }
        return array_merge($request->only('email', 'password'), ['site_id' => config('constants.site_id')]);
    }


    public function login(Request $request)
    {
        $this->validateLogin($request);

        // validate the login is from vendor or user
        $login_type = $request->has('login_type') ? $request->input('login_type') : "";
        if(User::where('email', $request->input('email'))->hasAdminRole()->exists() == false) {
            if($login_type == "user") {
                if(User::where('email', $request->input('email'))->hasUserRole()->exists() == null) {
                    return redirect()->back()->withErrors(['user-error' => 'User with this email is not exists.']);
                }
            }
            if($login_type == "vendor") {
                if(User::where('email', $request->input('email'))->hasVendorRole()->exists() == null) {
                    return redirect()->back()->withErrors(['vendor-error' => 'Vendor with this email is not exists.']);
                }
            }
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Logout, Clear Session, and Return.
     *
     * @return void
     */
    public function logout()
    {
        $user = Auth::user();
        Auth::logout();
        Session::flush();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    public function showLoginForm(CmsPagesRepository $cmsPagesService)
    {
        $cmsPageInfo = $cmsPagesService->getPageRec('login');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;

        $validator = JsValidator::make($this->validation);
        return view('auth.login')->with([
            'validator' => $validator,'metafields'=>$meta_fields
        ]);
    }
}