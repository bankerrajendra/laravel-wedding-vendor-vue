<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Logic\Common\LocationRepository;
use App\Logic\Admin\VendorCategoryRepository;
use App\Logic\Template\TemplateRepository;
use App\Mail\GeneralEmail;
use App\Models\User;
use App\Models\UserProfileFlag;
use App\Models\UsersInformation;
use App\Models\VendorsInformation;
use App\Models\VendorEventInformation;
use App\Models\TemporaryEventVendorsImage;
use App\Models\VendorEventImage;
use App\Models\VendorType;
use App\Traits\ActivationTrait;
use App\Traits\CaptchaTrait;
use App\Traits\CaptureIpTrait;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use jeremykenedy\LaravelRoles\Models\Role;
use JsValidator;
use Illuminate\Support\Facades\Mail;
use App\Logic\Admin\ReviewRepository;
use App\Logic\Admin\CmsPagesRepository;
use App\Events\FileUpload;
use Illuminate\Support\Facades\Storage;
use Illuminate\Auth\Events\Registered;
use App\Models\Artist;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use ActivationTrait;
    use CaptchaTrait;
    use RegistersUsers;
	

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/activate';

    protected $validationRules = [
        'email'                 => 'required|max:255|unique:users|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
        'password'              => 'required|min:6|max:30',
        'company_name'          => 'required|alpha_spaces|min:3|max:29',
        'vendor_category'       => 'required',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        'mobile_country_code'	=> 'required',
        'mobile_number'         => 'required|numeric|phone',
        'address'               => 'required|max:200',
        'country'               => 'required',
        'state'                 => 'required',
        'city'                  => 'required',
		'zip'   			    => 'required | min:5'
    ];

    protected $validationVendorEventRules = [
        'email'                 => 'required|max:255|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
        'password'              => 'required|min:6|max:30',
        'company_name'          => 'required|alpha_spaces|min:3|max:29',
        'categories'            => 'required|array',
        'categories.*'          => 'required|string',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        'mobile_country_code'	=> 'required',
        'mobile_number'         => 'required|numeric|phone',
        'address'               => 'required|max:200',
        'country'               => 'required',
        'state'                 => 'required',
        'city'                  => 'required',
        'zip'   			    => 'required | min:5',
        'event_name'            => 'required|alpha_spaces|min:3|max:29',
        'venue'                 => 'required|min:3|max:200',
        'description'           => 'required',
        'start_date'            => 'required|min:10|max:10|future_date',
        'end_date'              => 'required|min:10|max:10|future_date',
        'start_time'            => 'required',
        'ticket_url'            => 'validate_url',
        'video_url'             => 'sometimes|nullable|emebed_url',
        'meta_keywords'         => 'sometimes|max:500',
    ];

    protected $validationRulesUserRegistration = [
        'email'                 => 'required|max:255|unique:users|regex:/^[^\s@]+@[^\s@]+\.[^\s@]+$/',
        'password'              => 'required|min:6|max:30',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        'event_date'            => 'min:10|max:10',
        'budget'                => 'required'
    ];

    protected $template_repo;
    protected $VendorCategoryRepository;

    /**
     * Create a new controller instance.
     *
     * @param LocationRepository $locationRepository
     * @return void
     */
    public function __construct(
        LocationRepository $locationRepository, 
        TemplateRepository $template_repo, 
        VendorCategoryRepository $VendorCategoryRepository
    )
    {
        $this->template_repo = $template_repo;
        $this->VendorCategoryRepository = $VendorCategoryRepository;
        $this->middleware('guest', [
            'except' => 'logout',
        ]);
        View::share('countries', $locationRepository->getCountries());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if($data['type'] == 'unverified') {
            $ruleToValidate = $this->validationRulesUserRegistration;
        } else {
            if($data['vendor_type'] == 'event') {
                $ruleToValidate = $this->validationVendorEventRules;
            } else {
                $ruleToValidate = $this->validationRules;
            }
        }
        return Validator::make($data, $ruleToValidate);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $signup_type = $request->has('type') ? $request->type : "";
        $vendor_type = $request->has('vendor_type') ? $request->vendor_type : "";
        if($signup_type == "vendor") {
            if($vendor_type == 'event') {
                // events dates
                $expld_date = explode('.', $request->start_date);
                $start_date = $expld_date[2].'-'.$expld_date[1].'-'.$expld_date[0];
                // validate
                $start_date_check = strtotime(date('Y-m-d', strtotime($start_date) ) );
                $todays = strtotime(date('Y-m-d'));

                $end_expld_date = explode('.', $request->end_date);
                $end_date = $end_expld_date[2].'-'.$end_expld_date[1].'-'.$end_expld_date[0];
                // validate
                $end_date_check = strtotime(date('Y-m-d', strtotime($end_date) ) );

                if($start_date_check < $todays || $end_date_check < $todays) {
                    return back()->withErrors('Event\'s dates can not be past.');
                }

                if($start_date_check > $end_date_check) {
                    return back()->withErrors('Event\'s start date and end date are not proper, start date must less than or same as end date.');
                }

                // validate the start time and end time if date is same
                if($request->start_time != "" && $request->end_time != "") {
                    if(strtotime(date('Y-m-d h:i A', strtotime($start_date.' '.$request->start_time))) > strtotime(date('Y-m-d h:i A', strtotime($end_date.' '.$request->end_time)))) {
                        return back()->withErrors('Event\'s start time and end time are not proper, end time must greater than start time.');
                    }
                }
            }
        }

        // check out the vendor is already exists
        if($signup_type == "vendor" && $vendor_type == 'event' && $this->checkVendorExists($request->input('email'), $request->input('password'))) {
                // create an event for existing vendor
                $user = $this->create_event($request->all());

                $this->guard()->login($user);

                return redirect($this->redirectPath());
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    // to check user is exists with same email and password combination else show message
    protected function checkVendorExists($email, $password)
    {
        if(User::where('email', $email)->hasVendorRole()->where('deleted_at', NULL)->exists()) {
            $user = User::where('email', $email)->hasVendorRole()->where('deleted_at', NULL)->first();
            if(Hash::check($password, $user->password)) {
                return true;
            }
        }
        return false;
    }

    protected function create_event(array $data)
    {
        $signup_type = isset($data['type']) && trim($data['type']) != "" ? trim($data['type']) : "";
        $vendor_type = isset($data['vendor_type']) && trim($data['vendor_type']) != "" ? trim($data['vendor_type']) : "wedding";
        
        $ipAddress = new CaptureIpTrait();
        $role = Role::where('slug', '=', $signup_type)->first();
        // get site id
        $site_id = (int) config('constants.site_id');

        if($signup_type == "vendor") {
            $getVendorType = VendorType::where('slug', $vendor_type)->first(['id']);
            if($vendor_type == 'event') {
                // events dates
                $st_date = strtotime(date('d.m.Y h:i A', strtotime($data['start_date'].' '.$data['start_time'])));
                $start_date = date('Y-m-d H:i:s', $st_date);
                
                $ed_date = strtotime(date('d.m.Y h:i A', strtotime($data['end_date'].' '.$data['end_time'])));
                $end_date = date('Y-m-d H:i:s', $ed_date);
            }
            
            $user = User::where('email', $data['email'])->hasVendorRole()->where('deleted_at', NULL)->first();

            // do stuffs for events vendor
            if($vendor_type == 'event') {
                // artists
                $artists = isset($data['artists']) ? array_unique($data['artists']) : "";
                $artist_insert = "";
                if($artists != '') {
                    // check out the submitted artists already exists
                    if(count($artists) > 0) {
                        foreach($artists as $artist) {
                            if($artist != null && $artist != '') {
                                if(Artist::where('name', 'like', $artist)->exists()) {
                                    $artistObj = Artist::where('name', 'like', $artist)->first(['id']);
                                    $artist_insert .= $artistObj->id.",";
                                } else {
                                    $artistObj = Artist::create([
                                        'name' => $artist,
                                        'vendor_id' => $user->id,
                                        'site_id' => $site_id
                                    ]);
                                    $artist_insert .= $artistObj->id.",";
                                }
                            }
                        }
                    }
                }
                // remove last comma from artists insert
                if($artist_insert != '') {
                    $artist_insert = rtrim($artist_insert, ',');
                }
                // determine ammenities
                $online_seat_selection = isset($data['online_seat_selection']) && trim($data['online_seat_selection']) != "" && trim($data['online_seat_selection']) == "1" ? 1 : 0;
                $parking = isset($data['parking']) && trim($data['parking']) != "" && trim($data['parking']) == "1" ? 1 : 0;
                $food_for_sale = isset($data['food_for_sale']) && trim($data['food_for_sale']) != "" && trim($data['food_for_sale']) == "1" ? 1 : 0;
                $drinks_for_sale = isset($data['drinks_for_sale']) && trim($data['drinks_for_sale']) != "" && trim($data['drinks_for_sale']) == "1" ? 1 : 0;
                $babysitting_services = isset($data['babysitting_services']) && trim($data['babysitting_services']) != "" && trim($data['babysitting_services']) == "1" ? 1 : 0;
                // add initial slug for event
                // 1. check slug exists by slugyfy
                $event_slug = slugifyText($data['event_name']);
                if(VendorEventInformation::where('event_slug', $event_slug)->exists()) {
                    $entNameCnt = VendorEventInformation::where('event_slug', 'like', '%'.$event_slug.'%')->count();
                    // 2. update slug by adding -2
                    $event_slug .= "-".($entNameCnt+1);
                }
                
                // add events information
                $event = new VendorEventInformation();
                $event->event_name = $data['event_name'];
                $event->venue = $data['venue'];
                $event->artists = $artist_insert;
                $event->vendor_id = $user->id;
                $event->event_slug = $event_slug;
                $event->address = $data['address'];
                $event->country_id = $data['country'];
                $event->state_id = $data['state'];
                $event->city_id = $data['city'];
                $event->zip = $data['zip'];
                $event->description = $data['description'];
                $event->ticket_information = $data['ticket_information'];
                $event->categories = serialize($data['categories']);
                $event->start_date = $start_date;
                $event->end_date = $end_date;
                $event->ticket_url = $data['ticket_url'];
                $event->meta_keywords = $data['meta_keywords'];
                $event->video_url = $data['video_url'];
                $event->online_seat_selection = $online_seat_selection;
                $event->parking = $parking;
                $event->food_for_sale = $food_for_sale;
                $event->drinks_for_sale = $drinks_for_sale;
                $event->babysitting_services = $babysitting_services;
                $event->site_id = $site_id;
                $event->save();

                // add image
                // 1. check image already there
                $postimage_id = Session::get('event_poster', '');
                $ImagePoster = TemporaryEventVendorsImage::where('image_id', $postimage_id)->where('image_type', 'poster')->first();
                $image_id = Session::get('event_cover', '');
                $ImageCover = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->first();
                $pimage_id = Session::get('event_profile', '');
                $profilImg = TemporaryEventVendorsImage::where('image_id', $pimage_id)->where('image_type', 'profile')->first();
                Session::forget(['event_cover', 'event_profile', 'event_poster']);
                // 1.1 Update the poster name as per the naming convention
                if($ImagePoster != null) {
                    $PstrThumbNm = $ImagePoster->image_thumb;
                    $pstrExtArry = explode('.', $PstrThumbNm);
                    $pstrExt = $pstrExtArry[1];
                    $poster_name = config('constants.vendor_images_pre_name').'event-poster-'.$event->event_slug;
                    if(VendorEventImage::where('image_type', 'poster')->where('image_full', 'like', $poster_name.'.'.$pstrExt)->exists()) {
                        // 2. update slug by adding -2
                        $pstNameCnt = VendorEventImage::where('image_type', 'poster')->where('image_full', 'like', '%'.$poster_name.'%')->count();
                        $poster_name .= "-".($pstNameCnt+1);
                    }
                    $poster_name .= ".".$pstrExt;
                    $original_poster = 'vendor_photos/original/'.$ImagePoster->image_full;
                    $thumb_poster = 'vendor_photos/thumb_cropped/'.$ImagePoster->image_thumb;
                    Storage::disk('s3')->rename($original_poster, 'vendor_photos/original/' . $poster_name);
                    Storage::disk('s3')->rename($thumb_poster, 'vendor_photos/thumb_cropped/' . $poster_name);
                }
                // 1.2 Update the image name as per the naming convention
                if($ImageCover != null) {
                    $ImgThumbNm = $ImageCover->image_thumb;
                    $coverExtArry = explode('.', $ImgThumbNm);
                    $coverExt = $coverExtArry[1];
                    $cover_name = config('constants.vendor_images_pre_name').'event-cover-'.$event->event_slug;
                    if(VendorEventImage::where('image_type', 'cover')->where('image_full', 'like', $cover_name.'.'.$coverExt)->exists()) {
                        // 2. update slug by adding -2
                        $cvrNameCnt = VendorEventImage::where('image_type', 'cover')->where('image_full', 'like', '%'.$cover_name.'%')->count();
                        $cover_name .= "-".($cvrNameCnt+1);
                    }
                    $cover_name .= ".".$coverExt;
                    $original_cover = 'vendor_photos/original/'.$ImageCover->image_full;
                    $thumb_cover = 'vendor_photos/thumb_cropped/'.$ImageCover->image_thumb;
                    Storage::disk('s3')->rename($original_cover, 'vendor_photos/original/' . $cover_name);
                    Storage::disk('s3')->rename($thumb_cover, 'vendor_photos/thumb_cropped/' . $cover_name);
                }
                // 1.3 Update event logo
                if($profilImg != null) {
                    $PfThumbNm = $profilImg->image_thumb;
                    $logoExtArry = explode('.', $PfThumbNm);
                    $logoExt = $logoExtArry[1];
                    $logo_name = config('constants.vendor_images_pre_name').'event-logo-'.$event->event_slug;
                    if(VendorEventImage::where('image_type', 'profile')->where('image_full', 'like', $logo_name.'.'.$logoExt)->exists()) {
                        // 2. update slug by adding -2
                        $lgNameCnt = VendorEventImage::where('image_type', 'profile')->where('image_full', 'like', '%'.$logo_name.'%')->count();
                        $logo_name .= "-".($lgNameCnt+1);
                    }
                    $logo_name .= ".".$logoExt;
                    $original_logo = 'vendor_photos/original/'.$profilImg->image_full;
                    $thumb_logo = 'vendor_photos/thumb_cropped/'.$profilImg->image_thumb;
                    Storage::disk('s3')->rename($original_logo, 'vendor_photos/original/' . $logo_name);
                    Storage::disk('s3')->rename($thumb_logo, 'vendor_photos/thumb_cropped/' . $logo_name);
                }
                // add event poster
                if($ImagePoster != null) {
                    VendorEventImage::create([
                        'event_id' => $event->id,
                        'image_thumb' => $poster_name,
                        'image_full'=> $poster_name,
                        'is_profile_image' => 'Y',
                        'image_type' => 'poster'
                    ]);
                }
                // 2. add to vendors images
                if($ImageCover != null) {
                    VendorEventImage::create([
                        'event_id' => $event->id,
                        'image_thumb' => $cover_name,
                        'image_full'=> $cover_name,
                        'is_profile_image' => 'Y',
                        'image_type' => 'cover'
                    ]);
                }
                if($profilImg != null) {
                    VendorEventImage::create([
                        'event_id' => $event->id,
                        'image_thumb' => $logo_name,
                        'image_full' => $logo_name,
                        'is_profile_image' => 'Y',
                        'image_type' => 'profile'
                    ]);
                }
                // 3. remove temporary image record
                TemporaryEventVendorsImage::where('image_id', $postimage_id)->where('image_type', 'poster')->delete();
                TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->delete();
                TemporaryEventVendorsImage::where('image_id', $pimage_id)->where('image_type', 'profile')->delete();
                // send event create email
                $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Event Registration',
                    'mail_replace_vars' => [
                        '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                        '[%WEB_NAME%]' => getGeneralSiteSetting('site_name'),
                        '[%USER_FIRST_NAME%]' => $user->first_name,
                        '[%USER_EMAIL%]' => $user->email,
                        '[%EVENT_NAME%]' => $event->event_name,
                        '[%EVENT_EDIT_LINK%]' => $event->getEditLink()
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
            }
        }
        
        return $user;
    }

    protected function create(array $data)
    {
        $signup_type = isset($data['type']) && trim($data['type']) != "" ? trim($data['type']) : "";
        $vendor_type = isset($data['vendor_type']) && trim($data['vendor_type']) != "" ? trim($data['vendor_type']) : "wedding";
        
        $ipAddress = new CaptureIpTrait();
        $role = Role::where('slug', '=', $signup_type)->first();
        // get site id
        $site_id = (int) config('constants.site_id');

        if($signup_type == "vendor") {
            $getVendorType = VendorType::where('slug', $vendor_type)->first(['id']);
            if($vendor_type == 'event') {
                // events dates
                $st_date = strtotime(date('d.m.Y h:i A', strtotime($data['start_date'].' '.$data['start_time'])));
                $start_date = date('Y-m-d H:i:s', $st_date);
                
                $ed_date = strtotime(date('d.m.Y h:i A', strtotime($data['end_date'].' '.$data['end_time'])));
                $end_date = date('Y-m-d H:i:s', $ed_date);
            }
            
            $user = User::create([
                    'name'                  => $data['first_name'].".".$data['last_name'],
                    'first_name'            => array_key_exists('first_name', $data)?$data['first_name']:'',
                    'last_name'             => array_key_exists('last_name', $data)?$data['last_name']:'',
                    'email'                 => $data['email'],
                    'password'              => Hash::make($data['password']),
                    'token'                 => str_random(64),
                    'signup_ip_address'     => $ipAddress->getClientIp(),
                    'activated'             => !config('settings.activation'),
                    'country_id'            => $data['country'],
                    'city_id'               => $data['city'],
                    'mobile_country_code'   => $data['mobile_country_code'],
                    'mobile_number'         => $data['mobile_number'],
                    'address'               => $data['address'],
                    'state_id'              => $data['state'],
                    'zip'                   => $data['zip'],
                    'vendor_type'           => $getVendorType->id,
                    'site_id'               => config('constants.site_id')
                ]);
        
        
            UserProfileFlag::Create([
                'user_id' 		    => $user->id,
                'property_name'     => "company_name",
                'property_value'    => $data['company_name'],
                'table_remark'	    => "vi",
                'feedback'		    => "",
                'status'            => "0"
            ]);

            // add initial slug for company
            // 1. check slug exists by slugyfy
            $company_slug = slugifyText($data['company_name']);
            if(VendorsInformation::where('company_slug', $company_slug)->exists()) {
                // 2. update slug by adding -2
                $cmpNameCnt = VendorsInformation::where('company_slug', 'like','%'.$company_slug.'%')->count();
                $company_slug .= "-".($cmpNameCnt+1);
            }

            if($vendor_type == 'event') {
                // decide category for event type
                $eventOrg = $this->VendorCategoryRepository->getVendorSingleCategoryBy('name', 'Event Organizer')->first(['id']);
                $vendorInfoArr = [
                    'vendor_id'             =>  $user->id,
                    'company_name'          =>  $data['company_name'],
                    'company_slug'          =>  $company_slug,
                    'vendor_category'       =>  $eventOrg->id
                ];
            } else {
                $vendorInfoArr = [
                    'vendor_id'             =>  $user->id,
                    'company_name'          =>  $data['company_name'],
                    'company_slug'          =>  $company_slug,
                    'vendor_category'       =>  $data['vendor_category']
                ];
            }
            
            VendorsInformation::create($vendorInfoArr);

            // do stuffs for events vendor
            if($vendor_type == 'event') {
                // determine ammenities
                $online_seat_selection = isset($data['online_seat_selection']) && trim($data['online_seat_selection']) != "" && trim($data['online_seat_selection']) == "1" ? 1 : 0;
                $parking = isset($data['parking']) && trim($data['parking']) != "" && trim($data['parking']) == "1" ? 1 : 0;
                $food_for_sale = isset($data['food_for_sale']) && trim($data['food_for_sale']) != "" && trim($data['food_for_sale']) == "1" ? 1 : 0;
                $drinks_for_sale = isset($data['drinks_for_sale']) && trim($data['drinks_for_sale']) != "" && trim($data['drinks_for_sale']) == "1" ? 1 : 0;
                $babysitting_services = isset($data['babysitting_services']) && trim($data['babysitting_services']) != "" && trim($data['babysitting_services']) == "1" ? 1 : 0;
                // add initial slug for event
                // 1. check slug exists by slugyfy
                $event_slug = slugifyText($data['event_name']);
                if(VendorEventInformation::where('event_slug', $event_slug)->exists()) {
                    // 2. update slug by adding -2
                    $envNameCnt = VendorEventInformation::where('event_slug', 'like','%'.$event_slug.'%')->count();
                    $event_slug .= "-".($envNameCnt+1);
                }
                // artists
                $artists = isset($data['artists']) ? array_unique($data['artists']) : "";
                $artist_insert = "";
                if($artists != '') {
                    // check out the submitted artists already exists
                    if(count($artists) > 0) {
                        foreach($artists as $artist) {
                            if($artist != null && $artist != '') {
                                if(Artist::where('name', 'like', $artist)->exists()) {
                                    $artistObj = Artist::where('name', 'like', $artist)->first(['id']);
                                    $artist_insert .= $artistObj->id.",";
                                } else {
                                    $artistObj = Artist::create([
                                        'name' => $artist,
                                        'vendor_id' => $user->id,
                                        'site_id' => $site_id
                                    ]);
                                    $artist_insert .= $artistObj->id.",";
                                }
                            }
                        }
                    }
                }
                // remove last comma from artists insert
                if($artist_insert != '') {
                    $artist_insert = rtrim($artist_insert, ',');
                }
                
                // add events information
                $event = new VendorEventInformation();
                $event->event_name = $data['event_name'];
                $event->venue = $data['venue'];
                $event->artists = $artist_insert;
                $event->vendor_id = $user->id;
                $event->event_slug = $event_slug;
                $event->address = $data['address'];
                $event->country_id = $data['country'];
                $event->state_id = $data['state'];
                $event->city_id = $data['city'];
                $event->zip = $data['zip'];
                $event->description = $data['description'];
                $event->ticket_information = $data['ticket_information'];
                $event->categories = serialize($data['categories']);
                $event->start_date = $start_date;
                $event->end_date = $end_date;
                $event->ticket_url = $data['ticket_url'];
                $event->meta_keywords = $data['meta_keywords'];
                $event->video_url = $data['video_url'];
                $event->online_seat_selection = $online_seat_selection;
                $event->parking = $parking;
                $event->food_for_sale = $food_for_sale;
                $event->drinks_for_sale = $drinks_for_sale;
                $event->babysitting_services = $babysitting_services;
                $event->site_id = $site_id;
                $event->save();

                // add image
                // 1. check image already there
                $postimage_id = Session::get('event_poster', '');
                $ImagePoster = TemporaryEventVendorsImage::where('image_id', $postimage_id)->where('image_type', 'poster')->first();
                $image_id = Session::get('event_cover', '');
                $ImageCover = TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->first();
                $pimage_id = Session::get('event_profile', '');
                $profilImg = TemporaryEventVendorsImage::where('image_id', $pimage_id)->where('image_type', 'profile')->first();
                Session::forget(['event_cover', 'event_profile', 'event_poster']);
                // 1.1 Update the poster name as per the naming convention
                if($ImagePoster != null) {
                    $PstrThumbNm = $ImagePoster->image_thumb;
                    $pstrExtArry = explode('.', $PstrThumbNm);
                    $pstrExt = $pstrExtArry[1];
                    $poster_name = config('constants.vendor_images_pre_name').'event-poster-'.$event->event_slug;
                    if(VendorEventImage::where('image_type', 'poster')->where('image_full', 'like', $poster_name.'.'.$pstrExt)->exists()) {
                        // 2. update slug by adding -2
                        $pstNameCnt = VendorEventImage::where('image_type', 'poster')->where('image_full', 'like', '%'.$poster_name.'%')->count();
                        $poster_name .= "-".($pstNameCnt+1);
                    }
                    $poster_name .= ".".$pstrExt;
                    $original_poster = 'vendor_photos/original/'.$ImagePoster->image_full;
                    $thumb_poster = 'vendor_photos/thumb_cropped/'.$ImagePoster->image_thumb;
                    Storage::disk('s3')->rename($original_poster, 'vendor_photos/original/' . $poster_name);
                    Storage::disk('s3')->rename($thumb_poster, 'vendor_photos/thumb_cropped/' . $poster_name);
                }
                // 1.2 Update the image name as per the naming convention
                if($ImageCover != null) {
                    $ImgThumbNm = $ImageCover->image_thumb;
                    $coverExtArry = explode('.', $ImgThumbNm);
                    $coverExt = $coverExtArry[1];
                    $cover_name = config('constants.vendor_images_pre_name').'event-cover-'.$event->event_slug;
                    if(VendorEventImage::where('image_type', 'cover')->where('image_full', 'like', $cover_name.'.'.$coverExt)->exists()) {
                        // 2. update slug by adding -2
                        $cvrNameCnt = VendorEventImage::where('image_type', 'cover')->where('image_full', 'like', '%'.$cover_name.'%')->count();
                        $cover_name .= "-".($cvrNameCnt+1);
                    }
                    $cover_name .= ".".$coverExt;
                    $original_cover = 'vendor_photos/original/'.$ImageCover->image_full;
                    $thumb_cover = 'vendor_photos/thumb_cropped/'.$ImageCover->image_thumb;
                    Storage::disk('s3')->rename($original_cover, 'vendor_photos/original/' . $cover_name);
                    Storage::disk('s3')->rename($thumb_cover, 'vendor_photos/thumb_cropped/' . $cover_name);
                }
                // 1.3 Update event logo
                if($profilImg != null) {
                    $PfThumbNm = $profilImg->image_thumb;
                    $logoExtArry = explode('.', $PfThumbNm);
                    $logoExt = $logoExtArry[1];
                    $logo_name = config('constants.vendor_images_pre_name').'event-logo-'.$event->event_slug;
                    if(VendorEventImage::where('image_type', 'profile')->where('image_full', 'like', $logo_name.'.'.$logoExt)->exists()) {
                        // 2. update slug by adding -2
                        $lgNameCnt = VendorEventImage::where('image_type', 'profile')->where('image_full', 'like', '%'.$logo_name.'%')->count();
                        $logo_name .= "-".($lgNameCnt+1);
                    }
                    $logo_name .= ".".$logoExt;
                    $original_logo = 'vendor_photos/original/'.$profilImg->image_full;
                    $thumb_logo = 'vendor_photos/thumb_cropped/'.$profilImg->image_thumb;
                    Storage::disk('s3')->rename($original_logo, 'vendor_photos/original/' . $logo_name);
                    Storage::disk('s3')->rename($thumb_logo, 'vendor_photos/thumb_cropped/' . $logo_name);
                }
                // add event poster
                if($ImagePoster != null) {
                    VendorEventImage::create([
                        'event_id' => $event->id,
                        'image_thumb' => $poster_name,
                        'image_full'=> $poster_name,
                        'is_profile_image' => 'Y',
                        'image_type' => 'poster'
                    ]);
                }
                // 2. add to vendors images
                if($ImageCover != null) {
                    VendorEventImage::create([
                        'event_id' => $event->id,
                        'image_thumb' => $cover_name,
                        'image_full'=> $cover_name,
                        'is_profile_image' => 'Y',
                        'image_type' => 'cover'
                    ]);
                }
                if($profilImg != null) {
                    VendorEventImage::create([
                        'event_id' => $event->id,
                        'image_thumb' => $logo_name,
                        'image_full' => $logo_name,
                        'is_profile_image' => 'Y',
                        'image_type' => 'profile'
                    ]);
                }
                // 3. remove temporary image record
                TemporaryEventVendorsImage::where('image_id', $postimage_id)->where('image_type', 'poster')->delete();
                TemporaryEventVendorsImage::where('image_id', $image_id)->where('image_type', 'cover')->delete();
                TemporaryEventVendorsImage::where('image_id', $pimage_id)->where('image_type', 'profile')->delete();
                // send event create email
                $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
                $mail_req_arry = [
                    'mail_name' => 'Event Registration',
                    'mail_replace_vars' => [
                        '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                        '[%WEB_NAME%]' => getGeneralSiteSetting('site_name'),
                        '[%USER_FIRST_NAME%]' => $user->first_name,
                        '[%USER_EMAIL%]' => $user->email,
                        '[%EVENT_NAME%]' => $event->event_name,
                        '[%EVENT_EDIT_LINK%]' => $event->getEditLink()
                    ],
                    'unsubscribe_link' => $unsubscribe_link
                ];
                Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
            }

        } else if($signup_type == "unverified") {
            // event date
            $expld_date = explode('.', $data['event_date']);
            $event_date = $expld_date[2].'-'.$expld_date[1].'-'.$expld_date[0];
            // validate
            $event_date_check = strtotime(date('Y-m-d', strtotime($event_date) ) );
            $todays = strtotime(date('Y-m-d'));

            if($event_date_check < $todays) {
                return redirect()->back()->withErrors('Event date cann\'t be past date.');
            }
            
            $user = User::create([
                'name'                  => $data['first_name'].".".$data['last_name'],
                'first_name'            => array_key_exists('first_name', $data)?$data['first_name']:'',
                'last_name'             => array_key_exists('last_name', $data)?$data['last_name']:'',
                'email'                 => $data['email'],
                'password'              => Hash::make($data['password']),
                'token'                 => str_random(64),
                'signup_ip_address'     => $ipAddress->getClientIp(),
                'activated'             => 1,
                'access_grant'          => 'Y',
                'site_id'               => config('constants.site_id')
            ]);

            // User Information
            UsersInformation::create([
                'user_id' => $user->id,
                'event_date' => $event_date,
                'budget' => $data['budget']
            ]);

        }
        
        $user->attachRole($role);
        
        if($signup_type == "vendor") {
            UserProfileFlag::Create([
                'user_id' 		    => $user->id,
                'property_name'     => "first_name",
                'property_value'    => $data['first_name'],
                'table_remark'	    => "u",
                'feedback'		    => "",
                'status'            => "0"
            ]);	
            
            UserProfileFlag::Create([
                'user_id' 		    => $user->id,
                'property_name'     => "last_name",
                'property_value'    => $data['last_name'],
                'table_remark'	    => "u",
                'feedback'		    => "",
                'status'            => "0"
            ]);
            
            UserProfileFlag::Create([
                'user_id' 		    => $user->id,
                'property_name'     => "mobile_number",
                'property_value'    => $data['mobile_number'],
                'table_remark'	    => "u",
                'feedback'		    => "",
                'status'            => "0"
            ]);
            
            UserProfileFlag::Create([
                'user_id' 		    => $user->id,
                'property_name'     => "address",
                'property_value'    => $data['address'],
                'table_remark'	    => "u",
                'feedback'		    => "",
                'status'            => "0"
            ]);
            
            UserProfileFlag::Create([
                'user_id' 		    => $user->id,
                'property_name'     => "zip",
                'property_value'    => $data['zip'],
                'table_remark'	    => "u",
                'feedback'		    => "",
                'status'            => "0"
            ]);
        }

        /**
        * Get Register Email template
        */
        try {
            if($signup_type == "unverified") {
                $mail_name = 'User Registration';
            } else {
                $mail_name = 'Vendor Registration';
            }
            $unsubscribe_link = route('unsubscribe', [ 'token' => $user->token, 'type' => '' ]);
            $mail_req_arry = [
                'mail_name' => $mail_name,
                'mail_replace_vars' => [
                    '[%WEB_URL%]' => getGeneralSiteSetting('site_url'),
                    '[%WEB_NAME%]' => getGeneralSiteSetting('site_name'),
                    '[%USER_FIRST_NAME%]' => $user->first_name,
                    '[%USER_EMAIL%]' => $user->email
                ],
                'unsubscribe_link' => $unsubscribe_link
            ];
            Mail::to($user->email)->send(new GeneralEmail($mail_req_arry));
        } catch (\Exception $e){
            \Log::error("Some error while sending registration mail for user id:".$user->id);
        }

        return $user;
    }

    public function showRegistrationForm(ReviewRepository $review_repo)
    {
	    $meta_fields['title']="Muslim Wedding, Muslim Matrimony, Matrimonial Site, Brides, Grooms - Marriage";
        $meta_fields['keyword']="Muslim Wedding, Muslim Matrimony, Matrimonial Site, Brides, Grooms, Marriage";
        $meta_fields['description']="Find Muslim brides and grooms for marriage (Nikah) from Muslim Matrimony profiles. Register Profile FREE!. Muslim Matrimony & Marriage Site.";
        $meta_fields['canonical']="https://www.muslimwedding.com";
		$validator = JsValidator::make($this->validationRules);
        
		$religionList = config('constants.religions');
        $site_id= config('constants.site_id');

        // $sectList=CmsCityState::where('page_type','Community')->where('type_name','!=','Muslim')->where('site_id',$site_id)->get();
        // $countryList=CmsCityState::where('page_type','Country')->where('site_id',$site_id)->get();
        // $languageList=CmsCityState::where('page_type','Language')->where('site_id',$site_id)->orderByRaw('RAND()')->take(10)->get();

        $reviews = $review_repo->getRecords()
                            ->where('approved', '=', "1")
                            ->latest()
                            ->paginate(3);

        return view('auth.register')->with([
            'validator' => $validator,'metafields'=>$meta_fields,
            "religions"=>$religionList,
            'index_page_flag'=>'1',
            'reviews' => $reviews
        ]);
    }

    public function showSmallRegistrationForm(CmsPagesRepository $cmsPagesService)
    {

        $cmsPageInfo = $cmsPagesService->getPageRec('vendor-signup');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;

        $site_id= config('constants.site_id');

        $vendor_categories = $this->VendorCategoryRepository
                ->getStatusRecords(1)
                ->getVendorByType('wedding')
                ->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')
                ->where('name', '<>', 'Event Organizer')
                ->orderBy('name', 'ASC')
                ->get(['id', 'name']);

        $validator = JsValidator::make($this->validationRules);

        return view('auth.register-small')->with([
            'validator'  => $validator,
            'metafields' => $meta_fields,
            'vendor_categories' => $vendor_categories
        ]);
    }

    public function showEventRegistrationForm(Request $request, CmsPagesRepository $cmsPagesService)
    {
        $cmsPageInfo = @$cmsPagesService->getPageRec('vendor-signup');
        if($cmsPageInfo != null) {
            $meta_fields['title'] = @$cmsPageInfo->meta_title;
            $meta_fields['keyword'] = @$cmsPageInfo->meta_keyword;
            $meta_fields['description'] = @$cmsPageInfo->meta_description;
        } else {
            $meta_fields['title'] = '';
            $meta_fields['keyword'] = '';
            $meta_fields['description'] = '';
        }
        // poster
        $posterImg = '';
        $image_id = $request->session()->get('event_poster', '');
        $evntImagePoster = TemporaryEventVendorsImage::where('image_id',$image_id)->where('image_type', 'poster')->first();
        if($evntImagePoster != null) {
            $posterImg = Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$evntImagePoster->image_thumb);
        }
        // cover
        $coverImg = '';
        $image_id = $request->session()->get('event_cover', '');
        $userImageCover = TemporaryEventVendorsImage::where('image_id',$image_id)->where('image_type', 'cover')->first();
        if($userImageCover != null) {
            $coverImg = Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$userImageCover->image_thumb);
        }
        // photo
        $photoImg = '';
        $pimage_id = $request->session()->get('event_profile', '');
        $userImageProfile = TemporaryEventVendorsImage::where('image_id',$pimage_id)->where('image_type', 'profile')->first();
        if($userImageProfile != null) {
            $photoImg = Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$userImageProfile->image_thumb);
        }
        $events_categories = $this->VendorCategoryRepository->getVendorCategoryByTypeFront('event');

        $validator = JsValidator::make($this->validationVendorEventRules);

        $artists = Artist::where('is_approved', 'Y')
                        ->where('site_id', config('constants.site_id'))
                        ->orderBy('name', 'ASC')
                        ->get(['id', 'name']);

        return view('auth.vendor-event-signup')->with([
            'validator'  => $validator,
            'metafields' => $meta_fields,
            'events_categories' => $events_categories,
            'cover_image' => $coverImg,
            'profile_photo' => $photoImg,
            'poster_image' => $posterImg,
            'artists' => $artists
        ]);
    }

    public function imageEventPosterUpload(Request $request)
    {
        $status = event(new FileUpload('eventTempPoster', $request));
        return response()->json($status);
    }

    public function imageEventCoverUpload(Request $request)
    {
        $status = event(new FileUpload(config('constants.fileType.eventTempCover'), $request));
        return response()->json($status);
    }

    public function imageEventProfileUpload(Request $request)
    {
        $status = event(new FileUpload(config('constants.fileType.eventTempImage'), $request));
        return response()->json($status);
    }

    public function showUserSmallRegistrationForm(CmsPagesRepository $cmsPagesService)
    {
        $cmsPageInfo = $cmsPagesService->getPageRec('user-signup');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;

        $validator = JsValidator::make($this->validationRulesUserRegistration);

        return view('auth.user-register-small')->with([
            'validator'  => $validator,
            'metafields' => $meta_fields
        ]);
    }

}
