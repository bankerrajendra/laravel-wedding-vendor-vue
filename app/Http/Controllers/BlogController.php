<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logic\Admin\BlogRepository;
use App\Models\Blog;
use App\Models\BlogComment;
use Illuminate\Routing\Route;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Integer;
use App\Logic\Admin\CmsPagesRepository;

class BlogController extends Controller {
    protected $blogRepository;
    protected $validationRules =[
        'title'=>'required',
        'description'=>'required',
        'slug'=>'required'];

    protected $validationRulesServer =[
        'title'=>'required',
        'description'=>'required',
        'slug'=>'required'];


    protected $commentvalidationRules =[
        'name'=>'required|alpha_spaces',
        'email'=>'required|email|max:255',
        'mobile_number'=>'required|numeric|phone',
        'messages'=>'required'
    ];

    public function __construct(BlogRepository $blogRepository)   {
        $this->blogRepository=$blogRepository;
    }

    // Frontend Functions
    public function getBlogs(Request $request, CmsPagesRepository $cmsPagesService) {
        $blogList=$this->blogRepository->getBlogs('onlyactive','',$request);
        $blogListSidebar=$this->blogRepository->getBlogs('onlyactive','nopagination',$request);
        $cmsPageInfo = $cmsPagesService->getPageRec('blog');
        $meta_fields['title'] = $cmsPageInfo->meta_title;
        $meta_fields['keyword'] = $cmsPageInfo->meta_keyword;
        $meta_fields['description'] = $cmsPageInfo->meta_description;
        return view('pages.blog.blogs')->with(['bloglist'=>$blogList['fetchblogs'], 'bloglistsidebar'=>$blogListSidebar['fetchblogs'], 'pageNo'=>$blogList['pageNo'],'metafields'=>$meta_fields]);
    }

    public function getBlogInfo($slug,Request $request) {
        //echo $slug;
        $blogRec=$this->blogRepository->getBlogBySlug($slug);
        if($blogRec){
            $commentRec=$this->blogRepository->getBlogComments($blogRec->id, 'onlyactive', $pagination='nopagination', $request); //onlyactive
            //print_r($commentRec);
            $blogList=$this->blogRepository->getBlogs('onlyactive','nopagination',$request);
            $validator = JsValidator::make($this->commentvalidationRules);
            //print_r($validator);
            $blogListSidebar=$this->blogRepository->getBlogs('onlyactive','nopagination',$request);
            $meta_fields['title']=$blogRec->meta_title;
            $meta_fields['keyword']=$blogRec->meta_keyword;
            $meta_fields['description']=$blogRec->meta_description;
            return view('pages.blog.blog-detail')->with(['blogRec'=>$blogRec,'metafields'=>$meta_fields,'bloglistsidebar'=>$blogListSidebar['fetchblogs'],'bloglist'=>$blogList['fetchblogs'], 'blogcomments'=>$commentRec['fetchblogs'], 'validator'=>$validator]);
        } else {
            return abort(404);
        }

    }

    //Backend Functions
    public function getBlog(Request $request) {
        $blogVar=$this->blogRepository->getBlogs('','',$request);

        return view('pages.admin.blog.blogs')->with(['blogvar'=>$blogVar['fetchblogs'],'pageNo'=>$blogVar['pageNo']]);
    }
    public function  getComments($blog_id='',Request $request) {
        $commentVar=$this->blogRepository->getBlogComments($blog_id,'','',$request);

        return view('pages.admin.blog.comments')->with(['blogvar'=>$commentVar['fetchblogs'],'pageNo'=>$commentVar['pageNo']]);
    }

    public function actionComments($comment_id,$status) {
        $res=$this->blogRepository->approveBlogComment($comment_id,$status);
        return redirect()->back();
    }

    public function addBlog() {
        $validator = JsValidator::make($this->validationRulesServer);
        return view('pages.admin.blog.add-blog',['validator'=>$validator]);
    }

    public function editBlog($blog_id) {
        $validator=JsValidator::make($this->validationRulesServer);
        $blogRec=$this->blogRepository->getBlogRec($blog_id);
        if($blogRec){
            return view('pages.admin.blog.edit-blog',['validator'=>$validator,'blogRec'=>$blogRec,'blog_id'=>$blog_id]);
        } else {
            return abort(404);
        }
    }

    public function posteditBlog($blog_id,Request $request) {
        $status=$this->blogRepository->updateBlogRec($blog_id,$request);
        //return redirect()->back();
        if($status['status']=='1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }
    }

    public function updateBlog(Request $request) {
        $status=$this->blogRepository->addBlogRec($request);
        if($status['status']=='1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }

        // return redirect()->back()->withSuccess(config('constants.contact_save.save_message'));
//        if ($validator->fails()) {
//            return back()->withErrors($validator)->withInput();
//        }

    }

    public function deleteBlog($blog_id) {
        $res=Blog::where('id',$blog_id)->delete();
        if($res) {
            return redirect()->back()->withSuccess('Blog Deleted Successfully.');
        }else {
            return redirect()->back()->withError('Blog Deletion Failed. Please try again.');
        }
    }

    public function deleteComment($comment_id) {
        $res=BlogComment::where('id',$comment_id)->delete();
        if($res) {
            return redirect()->back()->withSuccess('Blog Comment Deleted Successfully.');
        }else {
            return redirect()->back()->withError('Blog Comment Deletion Failed. Please try again.');
        }
    }

    public function addComment(Request $request) {
        $validate =$request->validate($this->commentvalidationRules,
            [
                'name.required'=>'Name is required',
                'email.required'=>'Email is required',
                'email.email'=>'Invalid email format',
                'mobile.required'=>'Mobile is required',
                'mobile.numeric'=>'Only numbers are allowed',
                'message.required'=>'Message is required'

            ]
        );


        $blogRec = new BlogComment();
        $blogRec->name = $request->input('name');
        $blogRec->email = $request->input('email');
        $blogRec->mobile_number = $request->input('mobile_number');
        $blogRec->messages = $request->input('messages');
        $blogRec->blog_id = $request->input('blog_id');

        if ($blogRec->save()) {
            return redirect()->back()->withSuccess('Waiting for admin approval'); //return ['message'=>"Blog added successfully.", 'status'=>'1'];
        } else {
            return redirect()->back()->withErrors('There is problem in sending reply. please try again.'); //return ['message'=>"There is problem in sending reply. please try again.", 'status'=>'0'];
        }



    }
}