<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VendorFaq;
use App\Logic\Admin\VendorFaqRepository;
use App\Logic\Admin\VendorCategoryRepository;

use JsValidator;
use Illuminate\Support\Facades\Validator;

class VendorFaqController extends Controller {
    protected $VendorCategoryRepository;
    protected $validationRules = [
        'question' => 'required',
        'categories' => 'required',
        'answer_type' => 'required',
        'status'   => 'required'
    ];

    public function __construct(VendorFaqRepository $VendorFaqRepository,VendorCategoryRepository $VendorCategoryRepository)
    {
        $this->VendorFaqRepository = $VendorFaqRepository;
        $this->VendorCategoryRepository = $VendorCategoryRepository;
    }

    //Backend Functions
    public function get(Request $request) 
    {
        $page           = $request->has('page') ? $request->input('page') : 1;
        $per_page       = $request->has('per_page') ? $request->input('per_page') : config('usersmanagement.paginateListSize');
        $status         = $request->has('status') ? $request->input('status') : "";
        $search         = $request->has('search') ? $request->input('search') : "";
        $category       = $request->has('category') ? $request->input('category') : "";
        $records        = $this->VendorFaqRepository->getRecords();

        if($search != "") {
            $records = $this->VendorFaqRepository->getSearchRecords($records, $search);
        }

        if($status != "") {
            $records  = $records->where('status', '=', $status);
        }
        if($category != "") {
            $records  = $records->where('categories', 'like', '%;s:'.strlen($category).':"'.$category.'";%');
        }
        $records = $records->latest()
                ->paginate($per_page);      
        if($page > 1) {
            $page = (($page - 1) * $per_page) + 1;
        }

        $active_counts      = $this->VendorFaqRepository->getStatusRecords(1)
                            ->count();
        $inactive_counts    = $this->VendorFaqRepository->getStatusRecords(0)
                            ->count();
        $all_counts         = $this->VendorFaqRepository->getRecords()
                            ->count();
                            
        // get all categories
        $all_categories = $this->VendorCategoryRepository->getStatusRecords(1)->get(['id', 'name']);

        return view('pages.admin.vendor-faq.list')->with(
            [
                'records'               => $records,
                'page'                  => $page,
                'per_page'              => $per_page,
                'status'                => $status,
                'search'                => $search,
                'category'              => $category,
                'active_records'        => $active_counts,
                'inactive_records'      => $inactive_counts,
                'all_records'           => $all_counts,
                'all_categories'        => $all_categories
            ]
        );
    }

    /**
     * Handle bulk action of list
     */
    public function handleBulkAction(Request $request)
    {
        $action = $request->has('action') ? $request->input('action') : "";
        $ids    = $request->has('ids') ? $request->input('ids') : "";
        if( $action != "" && ($action == "delete" || $action == "1" || $action == "0") && !empty($ids) ) {
            $records = $this->VendorFaqRepository->getRecords($ids);
            if($action == "1" || $action == "0") {
                try {
                    foreach($records as $record) {
                        $record->status = $action;
                        $record->save();
                    }
                    return redirect()->back()->withSuccess('Records(s) updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating Records(s), please try again.');
                }
                
            } else if($action == "delete") {
                try {
                    // delete
                    foreach($records as $record) {
                        $this->VendorFaqRepository->deleteRecord($record);
                    }
                    return redirect()->back()->withSuccess('Record(s) deleted successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in deleting Record(s), please try again.');
                }
            }
        } else if($action == "sort") {
            $orders = $request->has('orders') ? $request->input('orders') : "";
            if(!empty($orders)) {
                try {
                    foreach($orders as $record_id => $order) {
                        if(is_numeric($order)) {
                            $record = VendorFaq::find($record_id);
                            $record->order = $order;
                            $record->save();
                        }
                    }
                    return redirect()->back()->withSuccess('Orders updated successfully');
                } catch(\Exception $e) {
                    return redirect()->back()->withErrors('There is problem in updating orders, please try again.');
                }
            }
        }
        return redirect()->back()->withErrors('There is some problem, please try again.');
    }

    public function add() 
    {
        // get categories
        $categories = $this->VendorCategoryRepository
                                ->getRecords()
                                ->where('status', '=', 1)
                                ->get();
        $validator = JsValidator::make($this->validationRules);

        return view('pages.admin.vendor-faq.add', [
                'validator' => $validator,
                'categories' => $categories
            ]
        );
    }

    public function edit($id) 
    {
        $categories = $this->VendorCategoryRepository
                                ->getRecords()
                                ->where('status', '=', 1)
                                ->get();
        
        $validator = JsValidator::make($this->validationRules);
        $record = $this->VendorFaqRepository->getRecords($id);
        if($record) {
            return view('pages.admin.vendor-faq.edit', [
                    'validator' => $validator,
                    'record'    => $record,
                    'categories' => $categories
                ]
            );
        } else {
            return abort(404);
        }
    }

    public function handleAdd(Request $request) 
    {
        $val_rules = $this->validationRules;
        if($request->input('answer_type') == 'radio' || $request->input('answer_type') == 'checkbox') {
            $val_rules['options'] = 'required'; 
        }
        $validator = Validator::make($request->all(), $val_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        
        $status = $this->VendorFaqRepository->addRecord($request);
        if($status['status'] == '1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }
    }

    public function handleEdit(Request $request) 
    {
        $val_rules = $this->validationRules;
        if($request->input('answer_type') == 'radio' || $request->input('answer_type') == 'checkbox') {
            $val_rules['options'] = 'required'; 
        }
        $validator = Validator::make($request->all(), $val_rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        
        $status = $this->VendorFaqRepository->updateRecord($request);
        if($status['status']=='1') {
            return redirect()->back()->withSuccess($status['message']);
        } else {
            return redirect()->back()->withError($status['message']);
        }
    }
}