<?php

namespace App\Http\Controllers;

use App\Models\MembershipBenefits;
use App\Models\MembershipPlans;
use App\Models\MembershipPlanPackages;
use App\Models\Country;
use App\Models\State;
use App\Models\PaymentGateways;
use App\Models\User;
use Illuminate\Http\Request;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use App\Logic\Common\UserInformationOptionsRepository;
use App\Logic\Membership\MembershipRepository;
use App\Logic\Common\LocationRepository;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use App\Logic\Template\TemplateRepository;

class MembershipController extends Controller
{
    private $_api_context;
    //
    protected $validationRules =[
        'name' => 'required',
        'icon_image' => 'image|mimes:jpeg,jpg,png,gif,ico|max:1024|dimensions:max_width=100,max_height=100'
    ];

    protected  $planValidationRules = [
        'plan_name'                 => 'required',
        'plan_type'                 => 'required',
        'package_amount.*'          => 'required',
        'package_duration.*'        => 'required',
        'package_type.*'            => 'required',
        'with_banner.*'             => 'required',
        'package_status'            => 'required',
        'public_photos_allowed'     => 'required|numeric',
        //'private_photos_allowed'    => 'required|numeric',
        'msg_allowed'               => 'required|numeric',
        'status'                    => 'required',
        'membership_benefits'       => 'required'
    ];

    protected $paymentValidationRules = [
        'plan_name'                 => 'required',
        'plan_id'                   => 'required|numeric',
        'package_amount'            => 'required|numeric',
        'package_duration'          => 'required|numeric',
        'package_duration_type'     => 'required|string',
        'country'                   => 'required|numeric|digits_between:1,4',
        'card-number'               => 'required|ccn',
        'card-expiry-month-yy'      => 'required|ccd|cc_expire_year',
        'cvv-cvc'                   => 'required|cvc',
        'first-name'                => 'required|alpha_spaces|max:25',
        'last-name'                 => 'required|alpha_spaces|max:25',
        'phone-country-code'        => 'required|numeric|digits_between:1,4',
        'mobile-number'             => 'required|numeric|digits_between:7,12',
        'billing-add-1'             => 'required|max:100',
        'billing-add-2'             => 'max:100',
        'billing-city'              => 'required|max:100',
        'billing-state'             => 'required|numeric|digits_between:1,4',
        'zip-code'                  => 'required|max:20',
        'conf-complete-purchase'    => 'required',
    ];

    protected $customPaymentRuleStrings = [
        'country.required'                      => 'Country required.',
        'card-number.required'                  => 'Card Number required.',
        'card-number.ccn'                       => 'Card Number is not proper.',
        'card-expiry-month-yy.required'         => 'Expity Date required.',
        'card-expiry-month-yy.ccd'              => 'Expity Date is not proper.',
        'cvv-cvc.required'                      => 'CVV/CVC required.',
        'cvv-cvc.cvc'                           => 'CVV/CVC is not proper.',
        'first-name.required'                   => 'First Name required.',
        'first-name.alpha_spaces'               => 'First Name may only contain letters and spaces.',
        'first-name.max'                        => 'First Name may not be greater than :max.',
        'last-name.required'                    => 'Last Name required.',
        'last-name.alpha_spaces'                => 'Last Name may only contain letters and spaces.',
        'last-name.max'                         => 'Last Name may not be greater than :max.',
        'phone-country-code.required'           => 'Phone country code required.',
        'phone-country-code.numeric'            => 'Phone country code must be a number.',
        'phone-country-code.digits_between'     => 'Phone country code must be between :min and :max digits.',
        'mobile-number.required'                => 'Mobile Number required.',
        'mobile-number.numeric'                 => 'Mobile Number must be a number.',
        'mobile-number.digits_between'          => 'Mobile Number must be between :min and :max digits.',
        'billing-add-1.required'                => 'Address Line 1 required.',
        'billing-add-1.alpha_spaces'            => 'Address Line 1 may only contain letters and spaces.',
        'billing-add-1.max'                     => 'Address Line 1 may not be greater than :max.',
        'billing-add-2.alpha_spaces'            => 'Address Line 2 may only contain letters and spaces.',
        'billing-add-2.max'                     => 'Address Line 2 may not be greater than :max.',
        'billing-city.required'                 => 'City required.',
        'billing-city.alpha_spaces'             => 'City may only contain letters and spaces.',
        'billing-city.max'                      => 'City may not be greater than :max.',
        'billing-state.required'                => 'State required.',
        'billing-state.numeric'                 => 'State must be a number.',
        'billing-state.digits_between'          => 'State must be between :min and :max digits.',
        'zip-code.required'                     => 'Zip/Postal code required.',
        'zip-code.max'                          => 'Zip/Postal code may not be greater than :max.',
        'conf-complete-purchase.required'       => 'Please check Complete purchase field.',
    ];

    public function __construct(LocationRepository $locationRepository)
    {
        $paypal_gateway_rec = PaymentGateways::where('name', '=', 'Paypal')->where('status', '=', '1')->first(['settings']);
        if(isset($paypal_gateway_rec->settings)) {
            $paypal_settings = unserialize($paypal_gateway_rec->settings);
        } else {
            $paypal_settings = [];
        }
        $paypal_log_filename = isset($paypal_settings['log_filename']) ? $paypal_settings['log_filename'] : "";
        $paypal_conf = [
            'client_id' => isset($paypal_settings['client_id']) ? $paypal_settings['client_id'] : "",
            'secret'    => isset($paypal_settings['secret_key']) ? $paypal_settings['secret_key'] : "",
            'settings'  => [
                'mode' => isset($paypal_settings['mode']) ? $paypal_settings['mode'] : "",
                'http.ConnectionTimeOut' => isset($paypal_settings['connection_timeout']) ? $paypal_settings['connection_timeout'] : "",
                'log.LogEnabled' => isset($paypal_settings['log_enabled']) ? $paypal_settings['log_enabled'] : "",
                'log.FileName' => storage_path() . '/logs/'. $paypal_log_filename,
                'log.LogLevel' => isset($paypal_settings['log_level']) ? $paypal_settings['log_level'] : ""
            ]
        ];
        $this->_api_context = new ApiContext(
            new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
        View::share('countries', $locationRepository->getCountries());
    }

    public function showMembershipPage(UserInformationOptionsRepository $userInfoRepo){
        $featuredusers=$userInfoRepo->getFeaturedUsers();
        return view('pages.user.membership',['featuredusers'=>$featuredusers]);
    }

    public  function getPlans() {
        $plans=MembershipPlans::all();
        return view('pages.admin.membership.plans', ['plans'=>$plans]);
    }

    public function getPlanbenefits()
    {
        $benefits=MembershipBenefits::all();
        return view('pages.admin.membership.benefits', ['benefits'=>$benefits]);
    }

    public function deleteBenefit($benefit_id) {
        $data_obj = MembershipBenefits::where('id',$benefit_id);
        $single_row = $data_obj->first();
        $image_name = $single_row->image;
        // delete icon image if exists
        if (\File::exists(public_path('img/membership-icons/'.$image_name))) {
            \File::delete(public_path('img/membership-icons/'.$image_name));
        }
        $res = $data_obj->delete();
        if($res) {
            return redirect()->back()->withSuccess('Membership Benefit Deleted Successfully.');
        }else {
            return redirect()->back()->withError('Membership Benefit Deletion Failed. Please try again.');
        }
    }

    public function deletePlan($plan_id) {
        $res=MembershipPlans::where('id',$plan_id)->delete();
        if($res) {
            return redirect()->back()->withSuccess('Membership Plan Deleted Successfully.');
        }else {
            return redirect()->back()->withError('Membership Plan Deletion Failed. Please try again.');
        }
    }


    public function addBenefit() {
        $validator = JsValidator::make($this->validationRules);
        return view('pages.admin.membership.add-benefit',['validator'=>$validator]);
    }

    public function addPlan() {
        $validator = JsValidator::make($this->planValidationRules);
        $benefits=MembershipBenefits::where('status','A')->get();
        return view('pages.admin.membership.add-plan',['validator'=>$validator,'benefits'=>$benefits]);
    }


    public function editBenefit($id) {
        $validator = JsValidator::make($this->validationRules);
        $benefitRec=MembershipBenefits::where('id',$id)->first();
        return view('pages.admin.membership.edit-benefit',['validator'=>$validator,'id'=>$id,'benefitRec'=>$benefitRec]);
    }

    public function editPlan($id) {
        $validator = JsValidator::make($this->planValidationRules,
            [
                'package_amount.required' => "Required",
                'package_duration.required' => "Required",
                'package_duration.numeric' => "Must be a Number"
            ]
        );
        $benefits = MembershipBenefits::where('status','A')->get();
        $planRec = MembershipPlans::where('id',$id)->first();
        // packages
        $packages = $planRec->membership_plan_packages;
        return view('pages.admin.membership.edit-plan',['validator'=>$validator,'id'=>$id,'benefits'=>$benefits,'planRec'=>$planRec, 'planPack' => $packages]);
    }

    public function updateBenefit(Request $request, MembershipRepository $memberRepo) {
        $validate = $request->validate($this->validationRules);
        $benefitRec = new MembershipBenefits();
        $benefitRec->name = $request->input('name');
        $benefitRec->status = $request->input('status');
        if($request->hasFile('icon_image')) {
            // save image to system
            $benefitRec->image = $memberRepo->saveBenefitIcoImage($request->file('icon_image'));
        }
        if ($benefitRec->save()) {
            return redirect()->back()->withSuccess('Membership benefit added successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in adding benefits. please try again.');
        }
    }
    public function updatePlan(Request $request) {
        if($request->input('plan_type') == "Free") {
            unset($this->planValidationRules['package_amount']);
            unset($this->planValidationRules['package_duration']);
            unset($this->planValidationRules['package_type']);
            unset($this->planValidationRules['with_banner']);
            unset($this->planValidationRules['package_status']);
        }
        $validate =$request->validate($this->planValidationRules, [
            'package_amount.*.required' => "Please add package amount.",
            'package_duration.*.required' => "Please add package duration.",
            'package_type.*.required' => "Please add package duration type.",
            'with_banner.*.required' => "Please add package with banner.",
            'package_status.*.required' => "Please add package status."
        ]);
        $planRec = new MembershipPlans();
        $planRec->plan_name = $request->input('plan_name');
        $planRec->plan_type = $request->input('plan_type');
        $planRec->currency = '$';
        $planRec->public_photos_allowed = $request->input('public_photos_allowed');
        //$planRec->private_photos_allowed = $request->input('private_photos_allowed');
        $planRec->msg_allowed = $request->input('msg_allowed');
        $planRec->site_id = config('constants.admin_site_id');
        $planRec->membership_benefits = implode(",",$request->input('membership_benefits'));

        $planRec->status = $request->input('status');

        if ($planRec->save()) {

            /**
             * Add Packages
             */
            if($planRec->plan_type === "Paid") {
                $i = 0;
                foreach($request->input('package_amount') as $package_amount) {
                    $planPlanPack = new MembershipPlanPackages();
                    $planPlanPack->plan_id = $planRec->id;
                    $planPlanPack->amount = $request->input('package_amount')[$i];
                    $planPlanPack->duration = $request->input('package_duration')[$i];
                    $planPlanPack->type = $request->input('package_type')[$i];
                    $planPlanPack->with_banner = $request->input('with_banner')[$i];
                    $planPlanPack->status = $request->input('package_status')[$i];
                    $planPlanPack->save();
                    $i++;
                }
            }

            return redirect()->back()->withSuccess('Membership plan added successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in adding plans. please try again.');
        }
    }

    public function posteditBenefit(Request $request, MembershipRepository $memberRepo) {
        $validate =$request->validate($this->validationRules,
            [
                'name.required'=>'Name is required'
            ]
        );
        $id = $request->input('id');
        $benefitRec = MembershipBenefits::find($id);
        $benefitRec->name = $request->input('name');
        $benefitRec->status = $request->input('status');
        if($request->hasFile('icon_image')) {
            // delete old image if there and add new
            $image_name = $benefitRec->image;
            // check file exits if exists remove
            if (\File::exists(public_path('img/membership-icons/'.$image_name))) {
                \File::delete(public_path('img/membership-icons/'.$image_name));
            }
            // save image to system
            $benefitRec->image = $memberRepo->saveBenefitIcoImage($request->file('icon_image'));
        }

        if ($benefitRec->save()) {
            return redirect()->back()->withSuccess('Membership benefit modified successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in modifying benefit. please try again.');
        }
    }

    public function posteditPlan(Request $request) {
        if($request->input('plan_type') == "Free") {
            unset($this->planValidationRules['package_amount']);
            unset($this->planValidationRules['package_duration']);
            unset($this->planValidationRules['package_type']);
            unset($this->planValidationRules['with_banner']);
            unset($this->planValidationRules['package_status']);
        }
        $validate = $request->validate($this->planValidationRules, [
            'package_amount.*.required' => "Please add package amount.",
            'package_duration.*.required' => "Please add package duration.",
            'package_type.*.required' => "Please add package duration type.",
            'with_banner.*.required' => "Please add package with banner.",
            'package_status.*.required' => "Please add package status."
        ]);

        $id = $request->input('id');
        $planRec = MembershipPlans::find($id);
        $planRec->plan_name = $request->input('plan_name');
        $planRec->plan_type = $request->input('plan_type');
        $planRec->currency = config('constants.currency_symbol');
        $planRec->public_photos_allowed = $request->input('public_photos_allowed');
        //$planRec->private_photos_allowed = $request->input('private_photos_allowed');
        $planRec->msg_allowed = $request->input('msg_allowed');
        $planRec->site_id = config('constants.admin_site_id');
        $planRec->membership_benefits = implode(",",$request->input('membership_benefits'));

        $planRec->status = $request->input('status');

        if ($planRec->save()) {
            /**
             * Add Packages
             */
            $p = 0;
            if($planRec->plan_type === "Paid") {

                if($request->has('package_id')) {
                    /**
                     * Remove Packge if not there in db
                     */
                    MembershipPlanPackages::whereNotIn('id', $request->input('package_id'))
                        ->where('plan_id', $planRec->id)
                        ->delete();
                    /**
                     * Update already existing entry
                     */
                    foreach($request->input('package_id') as $pack_id) {
                        if(MembershipPlanPackages::where('id', '=', $request->input('package_id')[$p])->exists()) {
                            $planPlanPack = MembershipPlanPackages::find($request->input('package_id')[$p]);
                            $planPlanPack->amount = $request->input('package_amount')[$p];
                            $planPlanPack->duration = $request->input('package_duration')[$p];
                            $planPlanPack->type = $request->input('package_type')[$p];
                            $planPlanPack->with_banner = $request->input('with_banner')[$p];
                            $planPlanPack->status = $request->input('package_status')[$p];
                            $planPlanPack->save();
                            $p++;
                        }
                    }
                }

                /**
                 * Add new pack
                 */
                $i = $p;
                if($request->has('package_id')) {
                    $loop = count($request->input('package_amount')) - count($request->input('package_id')) + $p - 1;
                } else {
                    $loop = count($request->input('package_amount')) - 1;
                }
                for ($i = $p; $i <= $loop ; $i++) {
                    $planPlanPack = new MembershipPlanPackages();
                    $planPlanPack->plan_id = $id;
                    $planPlanPack->amount = $request->input('package_amount')[$i];
                    $planPlanPack->duration = $request->input('package_duration')[$i];
                    $planPlanPack->type = $request->input('package_type')[$i];
                    $planPlanPack->with_banner = $request->input('with_banner')[$i];
                    $planPlanPack->status = $request->input('package_status')[$i];
                    $planPlanPack->save();
                }
            }
            return redirect()->back()->withSuccess('Membership plan modified successfully');
        } else {
            return redirect()->back()->withErrors('There is problem in modifying plan. please try again.');
        }
    }

    /**
     * View Membership Page - Rajendra Banker 9th April 2019
     */
    public function ShowFreeMemberShip(Request $request, UserInformationOptionsRepository $userInfoRepo, MembershipRepository $memberRepo)
    {
        $featuredUsers = $userInfoRepo->getFeaturedUsers(3);
        $membership_benefits = $memberRepo->getPlanBenefits('Free');
        return view('pages.user.membership', [
            'featured_users' => $featuredUsers,
            'free_member_benefits' => $membership_benefits
        ]);
    }

    /**
     * View Paid Membership Page - Rajendra Banker 11th April 2019
     */
    public function ShowPaidMemberShip(Request $request, UserInformationOptionsRepository $userInfoRepo, MembershipRepository $memberRepo)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $validator = JsValidator::make($this->paymentValidationRules, $this->customPaymentRuleStrings);

        
        $membership_benefits = $memberRepo->getPlanBenefits('Paid');
        $plan = MembershipPlans::getPlanDetail('Paid');
        // packages
        $packages = $memberRepo->getPlanActivePackages($plan->id);
        /**
         * Payment Gateways
         */
        $paypal_gateway = PaymentGateways::where('name', '=', 'Paypal')->first(['status']);
        $helcim_gateway = PaymentGateways::where('name', '=', 'Helcim')->first(['status']);
        $merrco_gateway = PaymentGateways::where('name', '=', 'Merrco')->first(['status']);
        return view('pages.user.paid-membership', [
            'userProfile'           => $user,
            'paid_member_benefits'  => $membership_benefits,
            'packages'              => $packages,
            'plan_name'             => $plan->plan_name,
            'plan_id'               => $plan->id,
            'currency'              => $plan->currency,
            'validator'             => $validator,
            'paypal_status'         => isset($paypal_gateway->status) ? $paypal_gateway->status : "",
            'helcim_status'         => isset($helcim_gateway->status) ? $helcim_gateway->status : "",
            'merrco_status'         => isset($merrco_gateway->status) ? $merrco_gateway->status : ""
        ]);
    }

    /**
     * Handle Halcim Payment Submission - Rajendra Banker 11th April 2019
     */
    public function HandleHelcimPaymentSubmission(Request $request, MembershipRepository $memberRepo)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), $this->paymentValidationRules, $this->customPaymentRuleStrings );
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        /**
         * Save all variables
         */
        $submitted_vals = $request->input();
        $array_to_submit = $request->input();
        // add helcom required variables
        $helcim_gateway = PaymentGateways::where('name', '=', 'Helcim')->where('status', '=', '1')->first(['settings']);
        $helcim_settings = unserialize($helcim_gateway->settings);
        // SET URL
        $url = $helcim_settings['api_url'];

        // BUILD POST ARRAY
        $array_to_submit['accountId'] = $helcim_settings['account_id'];
        $array_to_submit['apiToken'] = $helcim_settings['api_token'];
        $array_to_submit['transactionType'] = $helcim_settings['transaction_type'];
        $array_to_submit['terminalId'] = $helcim_settings['terminal_id'];
        $array_to_submit['test'] = $helcim_settings['test_mode'];
        $array_to_submit['amount'] = $array_to_submit['package_amount'];
        $array_to_submit['cardHolderName'] = $array_to_submit['first-name'] .' '. $array_to_submit['last-name'];
        $array_to_submit['cardNumber'] = $array_to_submit['card-number'];
        // prepare expiry month and year
        $expld_month_yr = explode("/", $array_to_submit['card-expiry-month-yy']);
        $array_to_submit['cardExpiry'] = $expld_month_yr[0].$expld_month_yr[1];
        $array_to_submit['cardCVV'] = $array_to_submit['cvv-cvc'];
        // prepare country and state
        $country_detail = Country::where('id', $array_to_submit['country'])->first(['name']);
        $state_detail = State::where('id', $array_to_submit['billing-state'])->first(['name']);
        $array_to_submit['cardHolderAddress'] = $array_to_submit['billing-add-1'] .' '. $array_to_submit['billing-add-2'] .' '. $array_to_submit['billing-city'] .' '. $state_detail->name .' '. $country_detail->name;
        $array_to_submit['cardHolderPostalCode'] = $array_to_submit['zip-code'];
        // \Log::info("Payment Submission Array");
        // \Log::info(print_r($array_to_submit,1));

        // CREATE POST STRING
        $postString = http_build_query($array_to_submit);
        // \Log::info("Payment Submission Post string");
        // \Log::info($postString);
        // return $postString;
        // SET CURL OPTIONS
        $curlOptions = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_AUTOREFERER => TRUE,
            CURLOPT_FRESH_CONNECT => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $postString,
            CURLOPT_TIMEOUT => 30 );

        // CREATE NEW CURL RESOURCE
        $curl = curl_init($url);

        // SET CURL OPTIONS
        curl_setopt_array($curl,$curlOptions);

        // PROCESS TRANSACTION - GET RESPONSE
        $response = curl_exec($curl);

        // CLOSE CURL REOURCE
        curl_close($curl);

        //CREATE XML OBJECT
        $xmlObject = @simplexml_load_string($response);
        try {
            if(is_object($xmlObject)) {
                $finalResultObj = json_decode(json_encode($xmlObject));
                // \Log::info(print_r($finalResultObj,1));
                if( $finalResultObj->response == 1 ) {
                    /**
                     * Pass array to save paid member details
                     */

                    $memberRepo->savePaidMember('Helcim', $submitted_vals, $finalResultObj->transaction);

                    // echo 'Successfully payment captured';
                    // echo '<br>';
                    // echo 'transactionId : '.$finalResultObj->transaction->transactionId;echo '<br>';
                    // echo 'type : '.$finalResultObj->transaction->type;echo '<br>';
                    // echo 'date : '.$finalResultObj->transaction->date;echo '<br>';
                    // echo 'time : '.$finalResultObj->transaction->time;echo '<br>';
                    // echo 'cardHolderName : '.$finalResultObj->transaction->cardHolderName;echo '<br>';
                    // echo 'amount : '.$finalResultObj->transaction->amount;echo '<br>';
                    // echo 'currency : '.$finalResultObj->transaction->currency;echo '<br>';
                    // echo 'cardNumber : '.$finalResultObj->transaction->cardNumber;echo '<br>';
                    // echo 'cardToken : '.$finalResultObj->transaction->cardToken;echo '<br>';
                    // echo 'expiryDate : '.$finalResultObj->transaction->expiryDate;echo '<br>';
                    // echo 'cardType : '.$finalResultObj->transaction->cardType;echo '<br>';
                    // echo 'avsResponse : '.$finalResultObj->transaction->avsResponse;echo '<br>';
                    // echo 'cvvResponse : '.$finalResultObj->transaction->cvvResponse;echo '<br>';
                    // echo 'approvalCode : '.$finalResultObj->transaction->approvalCode;echo '<br>';
                    // echo 'orderNumber : '.$finalResultObj->transaction->orderNumber;echo '<br>';
                    // echo 'customerCode : '.$finalResultObj->transaction->customerCode;echo '<br>';
                    /**
                     * Insert Member as Paid and Add Payment information
                     */
                    return Redirect::to('/payment-success?a='.$finalResultObj->transaction->amount.'&c='.config('constants.currency_symbol'));
                } else {
                    // Some error occured.
                    return Redirect::to('/payment-error')->withErrors($finalResultObj->responseMessage);    
                }
            } else {
                // Some error occured.
                return Redirect::to('/payment-error')->withErrors("Some error while making payment, contact site administrator.");
            }
        } catch(\Exceptio $e) {
            //error("Payment error message for Helcim: ". $e->getMessage());
            return Redirect::to('/payment-error')->withErrors($e->getMessage());
        }
    }

    /**
     * Handle Merrco Payment Submission - Rajendra Banker 11th April 2019
     */
    public function HandleMerrcoPaymentSubmission(Request $request, MembershipRepository $memberRepo)
    {
        $validator = Validator::make($request->all(), $this->paymentValidationRules, $this->customPaymentRuleStrings );
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        /**
         * Save all variables
         */
        $submitted_vals = $request->input();
        $array_to_submit = $request->input();
        // add helcom required variables

        // prepare expiry month and year
        $expld_month_yr = explode("/", $array_to_submit['card-expiry-month-yy']);
        // prepare country and state
        $country_detail = Country::where('id', $array_to_submit['country'])->first(['name', 'code']);
        $state_detail = State::where('id', $array_to_submit['billing-state'])->first(['name']);
        /**
         * Get gateway settings
         */
        $merrco_gateway = PaymentGateways::where('name', '=', 'Merrco')->where('status', '=', '1')->first(['settings']);
        $merrco_settings = unserialize($merrco_gateway->settings);
        //\Log::info(print_r($merrco_settings,1));

        if($merrco_settings['test_mode'] ==  1) {
            $test_mode = '.test';
        } else {
            $test_mode = '';
        }

        $PaymentUrl             = "https://api".$test_mode.".paysafe.com/cardpayments/v1/accounts/".$merrco_settings['account_id']."/auths";
        //\Log::info("Payment url:" . $PaymentUrl);

        $merchantRefNum         = 'merchant-_'.time().'_'.rand('1','9999999');
        $Amount                 = str_replace(".","", $array_to_submit['package_amount']);
        // if year is 2 digit than add century
        if(strlen($expld_month_yr[1]) == 2) {
            $expiry_year = (int) "20".$expld_month_yr[1];
        } else if(strlen($expld_month_yr[1]) == 4) {
            $expiry_year = (int) $expld_month_yr[1];
        }

        $jsonData               = [
            'merchantRefNum'    => $merchantRefNum,
            'amount'            => (int) $Amount,
            'settleWithAuth'    => false,
            'card'              => [
                'cardNum'       => $array_to_submit['card-number'], //'4111111111111111',
                'cardExpiry'    => [
                    'month'         => $expld_month_yr[0],
                    'year'          => $expiry_year
                ],

                'cvv'       => $array_to_submit['cvv-cvc'],
            ],
            "billingDetails" => [
                'street'    => $array_to_submit['billing-add-1'] .' '. $array_to_submit['billing-add-2'],
                'city'      => $array_to_submit['billing-city'],
                'state'     => $state_detail->name,
                'country'   => $country_detail->code,
                'zip'       => $array_to_submit['zip-code'],
            ],
            "customerIp"    => $_SERVER['REMOTE_ADDR'],
            "description"   => "Paid Membership"
        ];
        $auth_key           = $merrco_settings['basic_auth_key'];
        $RES                = $this->postCurl($PaymentUrl, $jsonData, $auth_key);
        $DATA               = json_decode($RES);
        //\Log::info(print_r($DATA,1));
        $merrcoData         = base64_encode(serialize($_POST));
        if(property_exists($DATA, 'status') && $DATA->status == 'COMPLETED') {
            $memberRepo->savePaidMember('Merrco', $submitted_vals, $DATA);
            return Redirect::to('/payment-success?a='.$DATA->amount.'&c='.config('constants.currency_symbol'));
        } else {
            try {
                if(property_exists($DATA, 'error') && $DATA->error->message != "") {
                    return Redirect::to('/payment-error')->withErrors('Code:'.$DATA->error->code.', Message: '.$DATA->error->message);
                } else {
                    return Redirect::to('/payment-error')->withErrors("Some error while making payment, contact site administrator.");
                }
            } catch (\Exception $e) {
                return Redirect::to('/payment-error')->withErrors($e->getMessage());
            }
        }
    }

    public function payWithpaypal(Request $request)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName($request->input('plan_name')) /** item name **/
                ->setCurrency(config('constants.currency_code'))
                ->setQuantity(1)
                ->setDescription("Plan id:".$request->input('plan_id')."::Duration:".$request->get('package_duration')."::Type:".$request->get('package_duration_type'))
                ->setPrice($request->get('package_amount')); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency(config('constants.currency_code'))
            ->setTotal($request->get('package_amount'));
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription($request->input('plan_name'));
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('payment-status-paypal-success')) /** Specify return URL **/
        ->setCancelUrl(URL::to('payment-status-paypal-cancel'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (Config::get('app.debug')) {
                return Redirect::to('/payment-error')->withErrors('Connection timeout');
            } else {
                return Redirect::to('/payment-error')->withErrors('Some error occur, sorry for inconvenient');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        return Redirect::to('/payment-error')->withErrors('Unknown error occurred');
    }

    public function getPaymentStatus(MembershipRepository $memberRepo)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            Session::put('error', 'Payment failed');
            return Redirect::to('/payment-error');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {

            /**
             * Prepare array to save data in member table
             */
            $expld_descr = explode("::", $result->getTransactions()[0]->getItemList()->getItems()[0]->getDescription());
            if(!empty($expld_descr)) {
                if(isset($expld_descr[0])) {
                    $expld_plan_id = explode(":", $expld_descr[0]);
                    $submitted_vals['plan_id'] = $expld_plan_id[1];
                } else {
                    $submitted_vals['plan_id'] = "";
                }
                if(isset($expld_descr[1])) {
                    $expld_plan_duration = explode(":", $expld_descr[1]);
                    $submitted_vals['package_duration'] = $expld_plan_duration[1];
                } else {
                    $submitted_vals['package_duration'] = "";
                }
                if(isset($expld_descr[2])) {
                    $expld_type = explode(":", $expld_descr[2]);
                    $submitted_vals['package_duration_type'] = $expld_type[1];
                } else {
                    $submitted_vals['package_duration_type'] = "";
                }
            } else {
                $submitted_vals['plan_id'] = $submitted_vals['package_duration'] = $submitted_vals['package_duration_type'] = "";
            }
            
            $submitted_vals['plan_name'] = @$result->getTransactions()[0]->getItemList()->getItems()[0]->getName();
            $submitted_vals['package_amount'] = @$result->getTransactions()[0]->getItemList()->getItems()[0]->getPrice();
            $cnt_details = Country::where('code', @$result->getTransactions()[0]->getItemList()->getShippingAddress()->getCountryCode())->first(['id']);
            $submitted_vals['country'] = $cnt_details->id;
            $submitted_vals['phone-country-code'] = 0;
            $submitted_vals['mobile-number'] = "";
            $submitted_vals['billing-add-1'] = @$result->getTransactions()[0]->getItemList()->getShippingAddress()->getLine1();
            $submitted_vals['billing-add-2'] = "";
            $submitted_vals['billing-city'] = @$result->getTransactions()[0]->getItemList()->getShippingAddress()->getCity();
            $submitted_vals['billing-state'] = 0;
            $submitted_vals['zip-code'] = @$result->getTransactions()[0]->getItemList()->getShippingAddress()->getPostalCode();
            $expld_name = explode(" ", @$result->getTransactions()[0]->getItemList()->getShippingAddress()->getRecipientName());
            $submitted_vals['first-name'] = $expld_name[0];
            $submitted_vals['last-name'] = $expld_name[1];
            /**
             * Set transaction array
             */
            $transaction['id'] = @$result->getId();
            $transaction['txnTime'] = @$result->getTransactions()[0]->getRelatedResources()[0]->getSale()->getCreateTime();
            $transaction['amount'] = @$result->getTransactions()[0]->getAmount()->getTotal();
            $transaction['currencyCode'] = @$result->getTransactions()[0]->getAmount()->getCurrency();
            $transaction['car_number'] = "";
            $transaction['card_type'] = "";
            $transaction['approval_code'] = "";
            $transaction['order_number'] = @$result->getCart();
            $transaction['customer_code'] = @$result->getTransactions()[0]->getRelatedResources()[0]->getSale()->getId();

            // \Log::info(print_r($result->getId(),1));
            // \Log::info(print_r($result->getPayer()->getPayerInfo(),1));
            // \Log::info(print_r($result->getTransactions(),1));
            // \Log::info(print_r($result->getTransactions()[0]->getAmount(),1));
            // \Log::info(print_r($result->getTransactions()[0]->getItemList()->getItems()[0],1));
            // \Log::info(print_r($result->getTransactions()[0]->getItemList()->getShippingAddress(),1));
            Session::put('success', 'Payment success');
            //\Log::info(print_r($submitted_vals,1));
            //\Log::info(print_r($transaction,1));
            $memberRepo->savePaidMember('Paypal', $submitted_vals, $transaction);
            return Redirect::to('/payment-success?a='.$transaction['amount'].'&c='.config('constants.currency_symbol'));
        }
        Session::put('error', 'Payment failed');
        return Redirect::to('/payment-error');
    }

    // Show payment success page only
    public function showPaymentSuccess(Request $request)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $amount = $request->input('a');
        $currency = $request->input('c');
        return view('pages.payment.success', [
            'amount' => $amount,
            'currency' => $currency
        ]);
    }

    // Show payment paypal cancel page
    public function showPaymentPaypalCancel()
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        return view('pages.payment.cancel');
    }

    // Show payment paypal error page
    public function showPaymentError()
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasVendorRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        return view('pages.payment.error');
    }

    /**
     * Show Paid Members
     */
    public function showPaidMembers(Request $request, $type = "", MembershipRepository $membershiprepo)
    {
        $plan = MembershipPlans::getPlanDetail('Paid');
        // packages
        $packages = $membershiprepo->getPlanActivePackages($plan->id);
        $pageNo = 1;
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $users = User::getPaidMembers($type)->latest()->paginate(config('usersmanagement.paginateListSize'));
            if($request->input('page')) {
                $pageNo = $request->input('page');
            }
        } else {
            $users = User::getPaidMembers($type)->latest()->get();
        }
        if($pageNo > 1) {
            $pageNo = (($pageNo - 1) * config('usersmanagement.paginateListSize')) + 1;
        }
        
        $all_count = User::getPaidMembers()->count();
        $paid_members_count = User::getPaidMembers('p')->count();
        $free_diamond_count = User::getPaidMembers('fd')->count();

        return view('usersmanagement.show-paid-members', [
            'users' => $users,
            'pageNo' => $pageNo,
            'packages' => $packages,
            'page_title' => 'Paid',
            'type' => $type,
            'all_count' => $all_count,
            'pm_count' => $paid_members_count,
            'fdm_count' => $free_diamond_count
        ]); 
    }

    /**
     * Show Upcoming Renewals Members
     */
    public function showUpcomingRenewalMembers(Request $request, MembershipRepository $membershiprepo)
    {
        $plan = MembershipPlans::getPlanDetail('Paid');
        // packages
        $packages = $membershiprepo->getPlanActivePackages($plan->id);
        $pageNo = 1;
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $users = User::getUpcomingRenewalMembers()->latest()->paginate(config('usersmanagement.paginateListSize'));
            if($request->input('page')) {
                $pageNo = $request->input('page');
            }
        } else {
            $users = User::getUpcomingRenewalMembers()->latest()->get();
        }
        if($pageNo > 1) {
            $pageNo = (($pageNo - 1) * config('usersmanagement.paginateListSize')) + 1;
        }
        
        return view('usersmanagement.show-paid-members', [
            'users' => $users,
            'pageNo' => $pageNo,
            'packages' => $packages,
            'page_title' => 'Upcoming Renewal'
        ]); 
    }

    /**
     * Show Expired Members
     */
    public function showExpiredMembers(Request $request, MembershipRepository $membershiprepo)
    {
        $plan = MembershipPlans::getPlanDetail('Paid');
        // packages
        $packages = $membershiprepo->getPlanActivePackages($plan->id);
        $pageNo = 1;
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $users = User::getExpiredMembers()->latest()->paginate(config('usersmanagement.paginateListSize'));
            if($request->input('page')) {
                $pageNo = $request->input('page');
            }
        } else {
            $users = User::getExpiredMembers()->latest()->get();
        }
        if($pageNo > 1) {
            $pageNo = (($pageNo - 1) * config('usersmanagement.paginateListSize')) + 1;
        }
        
        return view('usersmanagement.show-paid-members', [
            'users' => $users,
            'pageNo' => $pageNo,
            'packages' => $packages,
            'page_title' => 'Expired'
        ]); 
    }

    /**
     * Show Plan Wise Members
     */
    public function showPlanWiseMembers(Request $request, MembershipRepository $membershiprepo)
    {
        $plan = MembershipPlans::getPlanDetail('Paid');
        // packages
        $packages = $membershiprepo->getPlanActivePackages($plan->id);
        $i = 0;
        foreach($packages as $package) {
            $packages[$i]['count'] = User::getPlanWiseMembers($package->duration, $package->type)
                                        ->count();
            $i++;
        }

        /**
         * Initialize Vars
         */
        $page = $request->has('page') ? $request->input('page') : 1;
        $duration = $request->has('duration') ? $request->input('duration') : $packages[0]->duration;
        $type = $request->has('type') ? $request->input('type') : $packages[0]->type;
        
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $users = User::getPlanWiseMembers($duration, $type)->latest()->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $users = User::getPlanWiseMembers($duration, $type)->latest()->get();
        }
        if($page > 1) {
            $page = (($page - 1) * config('usersmanagement.paginateListSize')) + 1;
        }
        return view('usersmanagement.show-plan-wise-members', [
            'users'         => $users,
            'page'          => $page,
            'duration'      => $duration,
            'type'          => $type,
            'packages'      => $packages
        ]); 
    }

    /**
     * Show Send Free Diamond Membership
     */
    public function showSendFreeDiamondMembership(MembershipRepository $membershiprepo, TemplateRepository $template_repo)
    {
        $free_users = User::getNeverPaidUsers()->count();
        $plan = MembershipPlans::getPlanDetail('Paid');
        // packages
        $packages = $membershiprepo->getPlanAdminPackages($plan->id);

        $email = $template_repo->getTemplateBy('Send Free Diamond Membership', 'email')
                                        ->where('status', '=', 'A')
                                        ->first(['id', 'name']);
        if(!empty($email)) {
            $mail_template_name = $email->name;
            $mail_id = $email->id;
        } else {
            $mail_template_name = '';
            $mail_id = '';
        }
        $validator = JsValidator::make([
            'package_id' => 'required|numeric'
        ],[
            'package_id.required' => "Please select plan"
        ]);
        return view('usersmanagement.show-send-free-membership', [
            'validator' => $validator,
            'packages' => $packages,
            'free_users' => $free_users,
            'mail_template_name' => $mail_template_name,
            'mail_id' => $mail_id
        ]); 
    }

    /**
     * Handle Send Free Diamond Membership
     */
    public function handleSendFreeDiamondMembership(Request $request, MembershipRepository $membershiprepo)
    {
        $validator = Validator::make($request->all(), [
            'package_id' => 'required|numeric'
        ],[
            'package_id.required' => "Please select plan"
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        /**
         * check for 100 random active users.
         */
        $total_users = User::getNeverPaidUsers()->count();
        $total_random_users = 100;
        if($total_users > 0) {
            if($total_users < $total_random_users) {
                $users_send = $total_users;
            } else {
                $users_send = $total_random_users;
            }
            $users = User::getNeverPaidUsers()->get([
                'id', 
                'name', 
                'first_name', 
                'last_name',
                'email'
            ])->random($users_send);
            //\Log::info("=======SEND FREE DIAMOND USERS START==========");
            foreach($users as $user) {
                //\Log::info("User ID: ".$user->id);
                /**
                 * Prepare array to submit
                 */
                $submitted_vals = [];
                $submitted_vals['user_id'] = $user->id;
                $submitted_vals['package_id'] = $request->input('package_id');
                // get plan info
                $package_details = MembershipPlanPackages::find($request->input('package_id'));
                // get plan name
                $plan_details = MembershipPlans::find($package_details->plan_id);
                $submitted_vals['plan_id'] = $plan_details->id;
                $submitted_vals['plan_name'] = $plan_details->plan_name;
                $submitted_vals['package_amount'] = $package_details->amount;
                $submitted_vals['package_duration'] = $package_details->duration;
                $submitted_vals['package_duration_type'] = $package_details->type;
                $payment_mode = "Free Diamond";
                $submitted_vals['paid_by'] = "Admin";
                $submitted_vals['note'] = "Free Diamond Membership";
                $membershiprepo->savePaidMember($payment_mode, $submitted_vals, []);
            }
            //\Log::info("=======SEND FREE DIAMOND USERS END==========");
            return back()->withSuccess('Send Free Diamond Membership to '.$users_send.' random users');
        } else {
            $users_send = 0;
            return back()->withErrors('There is not user in system remaining as Free member');
        }

    }

    protected function postCurl($_url, $jsonData, $auth_key)
    {
        $jsonDataEncoded = json_encode($jsonData);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Authorization: Basic ".$auth_key;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output=curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
