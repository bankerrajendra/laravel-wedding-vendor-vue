<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use URL;

use App\Models\PaymentGateways;
use App\Models\SiteGeneralOptions;

class ManagePaymentGatewaysController extends Controller
{
    protected $validationRules =[
        //'name' => 'required',
    ];

    // Show manage payment gateways page
    public function showManagePaymentGateways()
    {
        $paypal_gateway = PaymentGateways::where('name', '=', 'Paypal')->first();
        $helcim_gateway = PaymentGateways::where('name', '=', 'Helcim')->first();
        $merrco_gateway = PaymentGateways::where('name', '=', 'Merrco')->first();

        if(!empty($paypal_gateway)) {
            $paypal        = unserialize($paypal_gateway->settings);
            $paypal_status = $paypal_gateway->status;
        } else {
            $paypal        = [];
            $paypal_status = '';
        }
        if(!empty($helcim_gateway)) {
            $helcim        = unserialize($helcim_gateway->settings);
            $helcim_status = $helcim_gateway->status;
        } else {
            $helcim        = [];
            $helcim_status = '';
        }
        if(!empty($merrco_gateway)) {
            $merrco        = unserialize($merrco_gateway->settings);
            $merrco_status = $merrco_gateway->status;
        } else {
            $merrco        = [];
            $merrco_status = '';
        }

        $validator = JsValidator::make($this->validationRules);
        return view('pages.admin.manage-payment-gateways', [
            'validator'     => $validator,
            'paypal'        => $paypal,
            'paypal_status' => $paypal_status,
            'helcim'        => $helcim,
            'helcim_status' => $helcim_status,
            'merrco'        => $merrco,
            'merrco_status' => $merrco_status
        ]);
    }

    // Handle manage payment gateways post request
    public function handleManagePaymentGateways(Request $request)
    {
        $paypal_gateway = $request->input('paypal');
        $helcim_gateway = $request->input('helcim');
        $merrco_gateway = $request->input('merrco');
        /**
         * Paypal
         */
        if(PaymentGateways::where('name', '=', 'Paypal')->exists()) {
            $paymentRecord = PaymentGateways::where('name', '=', 'Paypal')->first(['id']);
            $paymentRecord->settings = serialize($paypal_gateway);
            $paymentRecord->status = $request->input('paypal_status');
            $paymentRecord->site_id = config('constants.site_id');
            $paypal_res = $paymentRecord->save();
        } else {
            $paymentRecord = new PaymentGateways();
            $paymentRecord->name = 'Paypal';
            $paymentRecord->settings = serialize($paypal_gateway);
            $paymentRecord->status = $request->input('paypal_status');
            $paymentRecord->site_id = config('constants.site_id');
            $paypal_res = $paymentRecord->save();
        }
        /**
         * Helcim
         */
        if(PaymentGateways::where('name', '=', 'Helcim')->exists()) {
            $paymentRecord = PaymentGateways::where('name', '=', 'Helcim')->first(['id']);
            $paymentRecord->settings = serialize($helcim_gateway);
            $paymentRecord->status = $request->input('helcim_status');
            $paymentRecord->site_id = config('constants.site_id');
            $helcim_res = $paymentRecord->save();
        } else {
            $paymentRecord = new PaymentGateways();
            $paymentRecord->name = 'Helcim';
            $paymentRecord->settings = serialize($helcim_gateway);
            $paymentRecord->status = $request->input('helcim_status');
            $paymentRecord->site_id = config('constants.site_id');
            $helcim_res = $paymentRecord->save();
        }
        /**
         * Merrco
         */
        if(PaymentGateways::where('name', '=', 'Merrco')->exists()) {
            $paymentRecord = PaymentGateways::where('name', '=', 'Merrco')->first(['id']);
            $paymentRecord->settings = serialize($merrco_gateway);
            $paymentRecord->status = $request->input('merrco_status');
            $paymentRecord->site_id = config('constants.site_id');
            $merrco_res = $paymentRecord->save();
        } else {
            $paymentRecord = new PaymentGateways();
            $paymentRecord->name = 'Merrco';
            $paymentRecord->settings = serialize($merrco_gateway);
            $paymentRecord->status = $request->input('merrco_status');
            $paymentRecord->site_id = config('constants.site_id');
            $merrco_res = $paymentRecord->save();
        }
        if($paypal_res && $helcim_res && $merrco_res) {
            return redirect()->back()->withSuccess('Payment Information Updated Successfully.');
        }else {
            return redirect()->back()->withError('Payment Information Updation Failed. Please try again.');
        }
    }

    // manage membership on - off
    public function showMembershipOnOff()
    {
        $membership_status_obj = SiteGeneralOptions::where('site_id', config('constants.site_id'))
                                ->where('key', 'membership_status')
                                ->first(['value']);
        if(!empty($membership_status_obj)) {
            $membership_status = $membership_status_obj->value;
        } else {
            $membership_status = '';
        }
        return view('pages.admin.membership.membership-on-off', [
            'membership_status' => $membership_status
        ]);
    }

    public function handleMembershipOnOff(Request $request)
    {
        if(SiteGeneralOptions::where('site_id', config('constants.site_id'))->where('key', 'membership_status')->exists()) {
            $site_settings = SiteGeneralOptions::where('site_id', config('constants.site_id'))->where('key', 'membership_status')->first(['id']);
            $site_settings->value = $request->input('membership_on_off');
            $site_settings->site_id = config('constants.site_id');
            $setting_res = $site_settings->save();
        } else {
            $site_settings = new SiteGeneralOptions();
            $site_settings->key = 'membership_status';
            $site_settings->value = $request->input('membership_on_off');
            $site_settings->site_id = config('constants.site_id');
            $setting_res = $site_settings->save();
        }
        
        if($setting_res) {
            return redirect()->back()->withSuccess('Setting saved.');
        }else {
            return redirect()->back()->withError('Setting save Failed, Please try again.');
        }
    }
}
