<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Logic\Common\SearchRepository;
use Illuminate\Http\Request;
use App\Logic\Common\LocationRepository;
use App\Models\UsersPreference;
use App\Logic\Common\UserInformationOptionsRepository;
use JsValidator;
use App\Models\Language;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Laravel\Passport\Passport;

class NewSearchController extends Controller
{
    protected $locationRepository;
    protected $searchRepository;
    protected $userInformationOptionsRepository;
    protected $validationRules = [
        'age_from' => 'required',
        'age_to' => 'required',
        'city' => 'required',
        'message' => 'required'
    ];
    protected $backendValidation = [
        'age_from'=>'required',
        'age_to'=>'required|gt:age_from',
        'city'=>'required'
    ];
    public function __construct(LocationRepository $locationRepository, SearchRepository $searchRepository, UserInformationOptionsRepository $userInformationOptionsRepository) {
        $this->locationRepository = $locationRepository;
        $this->searchRepository = $searchRepository;
        $this->userInformationOptionsRepository = $userInformationOptionsRepository;
        View::share('countries', $locationRepository->getCountries());
    }
    public function newSearchPage(Request $request) {
        $keywords = $request->has('keywords') ? $request->keywords : '';
        $keywordstop = $request->has('keywordstop') ? $request->keywordstop : "";
        $keywordsdown = $request->has('keywordsdown') ? $request->keywordsdown : "";

        if($keywordstop != "" && $keywordsdown == "") {
            $keywords = $keywordstop;
        }
        if($keywordsdown != "" && $keywordstop == "") {
            $keywords = $keywordsdown;
        }

        // initialize the variables
        $order_by = $request->has('order_by') ? $request->order_by : 'latest';
        $per_page = $request->has('per_page') ? $request->per_page : 2;
        $page = $request->has('page') ? $request->page : 1;
        $country_code = $request->has('country_code') ? $request->country_code : "";
        $state_id = $request->has('state_id') ? $request->state_id : "";
        $city_id = $request->has('city_id') ? $request->city_id : "";
        $religion = $request->has('religion') ? $request->religion : "";
        $sect = $request->has('sect') ? $request->sect : "";
        $language = $request->has('language') ? $request->language : "";
        $education_id = $request->has('education_id') ? $request->education_id : "";
        $body_type = $request->has('body_type') ? $request->body_type : "";
        $ageRange = $request->has('ageRange') ? $request->ageRange : "";
        $age_from = $request->has('age_from') ? $request->age_from : config('constants.search_defaults.start_age');
        $age_to = $request->has('age_to') ? $request->age_to : config('constants.search_defaults.end_age');
        $heightRange = $request->has('heightRange') ? $request->heightRange : "";
        $height_from = $request->has('height_from') ? $request->height_from : config('constants.search_defaults.start_height');
        $height_to = $request->has('height_to') ? $request->height_to : config('constants.search_defaults.end_height');
        $food = $request->has('food') ? $request->food : "";
        $smoke = $request->has('smoke') ? $request->smoke : "";
        $drink = $request->has('drink') ? $request->drink : "";
        $marital_status = $request->has('marital_status') ? $request->marital_status : "";

        /**
         * Age and height
         */
        if($ageRange != "" || $ageRange != 0) {
            $ageRange_exp = explode(" - ", $ageRange);
            $age_from = $ageRange_exp[0];
            $age_to = $ageRange_exp[1];
        }

        if($heightRange != "" || $heightRange != 0) {
            $heightRange_exp = explode(" - ", $heightRange);
            $height_from = rtrim($heightRange_exp[0], " cm");
            $height_to = rtrim($heightRange_exp[1], " cm");
        }

        $validator = JsValidator::make(['message' => 'required']);
        $user = Auth::user();
        // initializes search sections
        if($keywords != "") {
            $search_arry = [
                'order_by' => $order_by,
                'per_page' => $per_page,
                'keywords' => $keywords
            ];
        } else {
            $search_arry = [
                'order_by' => $order_by,
                'per_page' => $per_page,
                'country_code' => $country_code,
                'state_id' => $state_id,
                'city_id' => $city_id,
                'religion' => $religion,
                'sect' => $sect,
                'language' => $language,
                'education_id' => $education_id,
                'body_type' => $body_type,
                'age_from' => $age_from,
                'age_to' => $age_to,
                'height_from' => $height_from,
                'height_to' => $height_to,
                'food' => $food,
                'smoke' => $smoke,
                'drink' => $drink,
                'marital_status' => $marital_status
            ];
        }

        // get user based on search array
        $results = $this->searchRepository->newSearchUsers($search_arry);
        $countries = [];
        $i = 0;
        foreach(getCountry() as $country) {
            $countries[$i]['id'] = $country->id;
            $countries[$i]['name'] = $country->name;
            $i++;
        }

        $resultJson = [];

        $i=0;
        foreach($results as $user_profile) {
            $resultJson[$i]['userId']=$user_profile->id;
            $resultJson[$i]['profileId']=$user_profile->getEncryptedId();
            $resultJson[$i]['profileLink']=$user_profile->profileLink();
            $resultJson[$i]['profilePic']=$user_profile->getVendorProfilePic();
            $resultJson[$i]['firstName']=$user_profile->getFirstname();
            $resultJson[$i]['isOnline']=$user_profile->isOnline();
            $resultJson[$i]['age']=$user_profile->getAge();
            $resultJson[$i]['city_name']=$user_profile->city->name;
            $resultJson[$i]['state_name']=$user_profile->state->name;
            $resultJson[$i]['country_name']=$user_profile->country->name;

            $resultJson[$i]['hasConversation']=$user_profile->hasConversation();
            $resultJson[$i]['viewConversationLink']=route('conversation', ['user_id' => $user_profile->getEncryptedId()]);
            $resultJson[$i]['religion']=$user_profile->users_information->religion->religion;
            $resultJson[$i]['height']=config('constants.height.' . $user_profile->users_information->height);
            $resultJson[$i]['education_flag']=$user_profile->users_information->education;
            $resultJson[$i]['education']=$user_profile->users_information->education->education;
            $resultJson[$i]['body_type']=$user_profile->users_information->body_type;
            $resultJson[$i]['about_flag']=$user_profile->users_information->about;
            $resultJson[$i]['about']=showReadMoreApi($user_profile->users_information->about, 90, $user_profile->profileLink());
            $resultJson[$i]['private_photo_permission_cnt'] = $user_profile->privatePhotoPermissionCnt();

            $i++;
        }

        $pagination = [
            'total' => $results->total(),
            'total_pages' => ceil($results->total()/$results->perPage()),
            'per_page' => $results->perPage(),
            'current_page' => $results->currentPage(),
            'last_page' => $results->lastPage()
        ];

        if($request->has('json')) {
            /**
             * Send banners
             */
            $banner = getBannerByTypeForVue('Search');
            $resultJson[$per_page-1]['banner'] = $banner;
            
            $response = [
                    'results' => $resultJson, 
                    'pagination' => $pagination,
                    'status' => 201
                ];
            return response()->json($response);
        }
        
        return view('pages.user.vue-search')->with(
            [
                'userProfile'       => $user,
                'language'          => Language::all(),
                'locationRepo'      => $this->locationRepository,
                'currency'          => getCurrency(),
                'religions'         => config('constants.religions'),
                'religions_vue'     => json_encode([config('constants.religions')]),
                'body_types'        => json_encode([config('constants.body_type')]),
                'foods'             => json_encode([config('constants.food')]),
                'smokes'            => json_encode([config('constants.smoke')]),
                'drinks'            => json_encode([config('constants.drink')]),
                'marital_status'    => json_encode([config('constants.marital_status')]),
                'results'           => json_encode($resultJson),
                'search_arry'       => $search_arry,
                'validator'         => $validator,
                'age_from'          => (int) $age_from,
                'age_to'            => (int) $age_to,
                'height_from'       => (int) $height_from,
                'height_to'         => (int) $height_to,
                'countries'         => json_encode($countries),
                'sect'              => json_encode(getSects()),
                'languages'         => json_encode(Language::all()),
                'educations'        => json_encode(getEducation()),
                'pagination'        => json_encode($pagination),
            ]
        );
    }

    public function newSearchApi(Request $request) {

        if($request->has('api_token')) {
            //Auth::guard('api')->user();
             $userId = Auth::guard('api')->id();
             if($userId!=null && $userId>0) {

  //               print_r(Auth::guard('api')->user());
//die;
                 $keywords = $request->has('keywords') ? $request->keywords : '';
                 $keywordstop = $request->has('keywordstop') ? $request->keywordstop : "";
                 $keywordsdown = $request->has('keywordsdown') ? $request->keywordsdown : "";

                 if($keywordstop != "" && $keywordsdown == "") {
                     $keywords = $keywordstop;
                 }
                 if($keywordsdown != "" && $keywordstop == "") {
                     $keywords = $keywordsdown;
                 }

                 // initialize the variables
                 $order_by = $request->has('order_by') ? $request->order_by : 'latest';
                 $per_page = $request->has('per_page') ? $request->per_page : 2;
                 $page = $request->has('page') ? $request->page : 1;
                 $country_code = $request->has('country_code') ? $request->country_code : "";
                 $state_id = $request->has('state_id') ? $request->state_id : "";
                 $city_id = $request->has('city_id') ? $request->city_id : "";
                 $religion = $request->has('religion') ? $request->religion : "";
                 $sect = $request->has('sect') ? $request->sect : "";
                 $language = $request->has('language') ? $request->language : "";
                 $education_id = $request->has('education_id') ? $request->education_id : "";
                 $body_type = $request->has('body_type') ? $request->body_type : "";
                 $ageRange = $request->has('ageRange') ? $request->ageRange : "";
                 $age_from = $request->has('age_from') ? $request->age_from : config('constants.search_defaults.start_age');
                 $age_to = $request->has('age_to') ? $request->age_to : config('constants.search_defaults.end_age');
                 $heightRange = $request->has('heightRange') ? $request->heightRange : "";
                 $height_from = $request->has('height_from') ? $request->height_from : config('constants.search_defaults.start_height');
                 $height_to = $request->has('height_to') ? $request->height_to : config('constants.search_defaults.end_height');
                 $food = $request->has('food') ? $request->food : "";
                 $smoke = $request->has('smoke') ? $request->smoke : "";
                 $drink = $request->has('drink') ? $request->drink : "";
                 $marital_status = $request->has('marital_status') ? $request->marital_status : "";

                 /**
                  * Age and height
                  */
                 if($ageRange != "" || $ageRange != 0) {
                     $ageRange_exp = explode(" - ", $ageRange);
                     $age_from = $ageRange_exp[0];
                     $age_to = $ageRange_exp[1];
                 }

                 if($heightRange != "" || $heightRange != 0) {
                     $heightRange_exp = explode(" - ", $heightRange);
                     $height_from = rtrim($heightRange_exp[0], " cm");
                     $height_to = rtrim($heightRange_exp[1], " cm");
                 }


                 $user = Auth::guard('api')->user();
                 // initializes search sections
                 if($keywords != "") {
                     $search_arry = [
                         'order_by' => $order_by,
                         'per_page' => $per_page,
                         'keywords' => $keywords
                     ];
                 } else {
                     $search_arry = [
                         'order_by' => $order_by,
                         'per_page' => $per_page,
                         'country_code' => $country_code,
                         'state_id' => $state_id,
                         'city_id' => $city_id,
                         'religion' => $religion,
                         'sect' => $sect,
                         'language' => $language,
                         'education_id' => $education_id,
                         'body_type' => $body_type,
                         'age_from' => $age_from,
                         'age_to' => $age_to,
                         'height_from' => $height_from,
                         'height_to' => $height_to,
                         'food' => $food,
                         'smoke' => $smoke,
                         'drink' => $drink,
                         'marital_status' => $marital_status
                     ];
                 }

                 // get user based on search array
                 $results = $this->searchRepository->newSearchUsers($search_arry);
                 /******************* API Specific Job ***********************/
                 $resultJson=array();

                 $i=0;
                 foreach($results as $user_profile) {
                     $resultJson[$i]['userId']=$user_profile->id;
                     $resultJson[$i]['profileLink']=$user_profile->profileLink();
                     $resultJson[$i]['profilePic']=$user_profile->getVendorProfilePic();
                     $resultJson[$i]['firstName']=$user_profile->getFirstname();
                     $resultJson[$i]['isOnline']=$user_profile->isOnline();
                     $resultJson[$i]['age']=$user_profile->getAge();
                     $resultJson[$i]['city_name']=$user_profile->city->name;
                     $resultJson[$i]['state_name']=$user_profile->state->name;
                     $resultJson[$i]['country_name']=$user_profile->country->name;

                     $resultJson[$i]['hasConversation']=$user_profile->hasConversation();
                     $resultJson[$i]['viewConversationLink']=route('conversation', ['user_id' => $user_profile->getEncryptedId()]);
                     $resultJson[$i]['religion']=$user_profile->users_information->religion->religion;
                     $resultJson[$i]['height']=config('constants.height.' . $user_profile->users_information->height);
                     $resultJson[$i]['education_flag']=$user_profile->users_information->education;
                     $resultJson[$i]['education']=$user_profile->users_information->education->education;
                     $resultJson[$i]['body_type']=$user_profile->users_information->body_type;
                     $resultJson[$i]['about_flag']=$user_profile->users_information->about;
                     $resultJson[$i]['about']=showReadMoreApi($user_profile->users_information->about, 90, $user_profile->profileLink());

                     $i++;
                 }
                // $resultJson['paginations']=json_encode((array)$results->appends($search_arry)->links());

                 /****************** END of API Specific JOB *****************/

                 return Response::json($resultJson);

             } else {
                 //return Response::json(array('userId'=>$userId));
                 return Response::json(array('status'=>'Mismatch API Token.'));
             }
        } else {
            return Response::json(array('status'=>'API Token is empty.'));
        }

        die;
    }
}
