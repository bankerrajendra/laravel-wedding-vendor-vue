<?php
/** 
@ ReligionController 
@ manages Religion module
@ Created : 12|21|2018	
**/

namespace App\Http\Controllers;

use App\Models\Religion;
use Illuminate\Support\Facades\Route;
use App\Events\Activity;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class ReligionController extends Controller
{
	
	protected $religionModel;    
	protected $religionRepository;
	# Not using this repository through
	
	protected $validationRules =[	   
	   'religion_title'=>'required',
	   'description'=>'required'];
	   
	   
	protected $validationRulesServer =[
	   'religion_title'=>'required',
	   'description'=>'required'];

	
    public function __construct(Religion $religion)
    {
		$this->middleware('auth');
		$this->religionModel = $religion;            
    }
 
	/**
	@ ReligionController@listReligions
	@ param : null
	@ returns : HTML
	**/	
    public function listReligions()
    {
		$religionLists =$this->religionModel::all();
		return view('religions.list-religions')->with(["religionLists"=>$religionLists]);
    }

	/**
	@ ReligionController@createReligions
	@ Form to Creates new entry in Religion table
	@ param : null
	@ returns : HTML
	**/
	public function createReligions()
    {
		$validator = JsValidator::make($this->validationRulesServer);		
		return view('religions.add-new-religion',['validator'=>$validator])->with(["nextSiteId"=>3]);		
    }
	
	/**
	@ ReligionController@saveReligion
	@ Handles createReligions reuests
	@ param : Form data
	@ returns : Event
	**/
	public function saveReligion(Request $request){		 
		$religionData = new Religion;
        $religionData->religion_title = $request->input('religion_title');
        $religionData->religion_description = $request->input('religion_description');
         
        if ($religionData->save()) {
			 return redirect()->back()->withSuccess('Religion added successfully.');            
        } else {
            return redirect()->back()->withError('Error occured while creating Religion');
        }
	}

	/**
	@ ReligionController@editReligions
	@ Updates data in Religion table
	@ param : Religion ID
	@ returns : HTML
	**/
	public function editReligions($religion_id)
    {	
		$validator = JsValidator::make($this->validationRulesServer);	
		$religionSingle = $this->religionModel::find($religion_id);
		
        if($religionSingle){
            return view('religions.edit-religion',['validator'=>$validator,'religionSingle'=>$religionSingle,'religion_id'=>$religion_id])->with(["nextSiteId"=>3]);
        } else {
            return abort(404);
        } 
    }	
	
	/**
	@ ReligionController@updateReligion
	@ Handles updateReligion requests
	@ param : Religion ID
	@ returns : Event
	**/
	public function updateReligion(Request $request, $religion_id){	
		// echo $religion_id."<br>"; 
		
		$religionData = $this->religionModel::find($religion_id);
        $religionData->religion_title = $request->input('religion_title');
        $religionData->religion_description = $request->input('religion_description'); 

        if ($religionData->save()) {			 
			 return redirect()->back()->withSuccess('Religion updated successfully.');            
        } else {			 
            return redirect()->back()->withError('Error occured while updating Religion');
        }
	}
	
	/**
	@ ReligionController@updateReligion
	@ Deletes in Religion table
	@ param : Religion ID
	@ returns : Event
	**/
	public function deleteReligion($religion_id){	
		$religionData = $this->religionModel::find($religion_id);
		
		if($religionData->delete()) {			 
			 return redirect()->back()->withSuccess('Religion deleted successfully.');            
        } else {			 
            return redirect()->back()->withError('Error occured while deleting Religion');
        }
	}	 
}