<?php

namespace App\Http\Controllers;

use App\Logic\Activity\ConnectActivityRepository;
use App\Logic\Activity\UserActivityStatusRepository;
use App\Logic\Admin\VendorCategoryRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\UserProfileFlag;
use App\Models\VendorsInformation;
use App\Models\UsersInformation;
use App\Models\VendorFaq;
use App\Models\VendorFaqAnswer;
use App\Events\Activity;
use App\Models\UsersImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use App\Events\FileUpload;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Resources\Json\JsonResource;
use DateTime;
use App\Models\VendorsImage;
use App\Models\VendorVideo;
use App\Models\VendorShowcase;
use App\Models\VendorCustomFaq;
use App\Models\ShortlistedUser;
use File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    protected $userModel;
    protected $connectActivity;

    protected $validationRules =[
        'message'=>'required'
    ];

    protected $validationPasswordRules = [
        'current_password'          => 'required|confirmed',
        'new_password'              => 'required|min:6|max:30|different:current_password',
        'new_password_confirmation' => 'required|same:new_password'
    ];

    protected $validationVendorInfoRules = [
        'company_name'          => 'required|alpha_spaces|min:3|max:29',
        'established_year'      => 'past_year',
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        'meta_keywords'         => 'required|max:500',
        'business_information'  => 'required',
        'specials'              => 'required',
        'mobile_country_code'	=> 'required',
        'mobile_number'         => 'required|numeric|phone',
        'address'               => 'required|max:200',
        'country'               => 'required',
        'state'                 => 'required',
        'city'                  => 'required',
        'zip'   			    => 'required | min:5',
        'business_website'      => 'validate_url',
        'facebook'              => 'validate_url',
        'instagram'             => 'validate_url',
        'twitter'               => 'validate_url',
        'linkedin'              => 'validate_url',
        'pinterest'             => 'validate_url'
    ];

    protected $userManageProfileRules = [
        'first_name'            => 'required|alpha_spaces|max:25',
        'last_name'             => 'required|alpha_spaces|max:25',
        'event_date'            => 'nullable|min:10|max:10',
        'budget'                => 'required'
    ];

    /**
     * Create a new controller instance.
     * @param User $user
     * @param ConnectActivityRepository $connectActivityRepository
     * @return void
     */
    public function __construct(User $user, ConnectActivityRepository $connectActivityRepository)
    {
            $this->middleware('auth');
            $this->userModel = $user;
            $this->connectActivity = $connectActivityRepository;
    }




    public function getConnect(UsersImage $usersImage) {
        $auth_user = Auth::user();
        $validator = JsValidator::make($this->validationRules);

        if ($auth_user->isAdmin()) {
            return view('pages.admin.home');
        }

        $userProfile = $this->connectActivity->getRandomConnectUser(); //1 user information

        if($userProfile){
            event(new Activity(config('constants.activity.view_profile'), $userProfile->id));
        }

        $newArr = array();
        $existingPublicImages=array();
        if($userProfile) {
            $newArr = $this->similarities($userProfile);  //2 similarities
            $publicwhere = array(
                'user_id' => $userProfile->id,
                'image_type' => 'public',
                'is_approved' => 'Y'
            );
            $usersImage = $usersImage->where($publicwhere);

            $userHasPhotoSharedAccess=$userProfile->hasSharedPhotos($auth_user->id);
            $priPhotoRequestStatus='';
            $privatePhotoRequestRes=$auth_user->hasAlreadySentPrivatePhotoRequest($userProfile->id);
            if(count($privatePhotoRequestRes)>0) {
                $privatePhotoRequestRec = $privatePhotoRequestRes[0];
                $priPhotoRequestStatus = $privatePhotoRequestRec->status;
            }

            if($userHasPhotoSharedAccess || $priPhotoRequestStatus=='A') {
                $privatewhere = array(
                    'user_id' => $userProfile->id,
                    'image_type' => 'private',
                    'is_approved' => 'Y'
                );
                $usersImage = $usersImage->orWhere(function($innerQuery2) use($privatewhere) {
                    return $innerQuery2->where($privatewhere);
                });
            }

            $existingPublicImages = $usersImage->get();
        }

        return view('pages.connect.connect')->with(['connectProfiles'=>$userProfile,
            'existingPublicImages' => $existingPublicImages,
            'similarities' => $newArr,
            'validator'=>$validator
        ]);

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tab_name='')
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return view('pages.admin.home');
        }

        $userFlagData = getFlaggedUserData($user->id);
        $arr=array();
        foreach ($userFlagData as $flagData) {
            if($flagData->property_name=='first_name' && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|alpha_spaces|max:25';

            if($flagData->property_name=='last_name' && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|alpha_spaces|max:25';

            if($flagData->property_name=='mobile_number' && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|numeric|phone';

            if($flagData->property_name=='about'  && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|min:50';

            if($flagData->property_name=='looking_for'  && $flagData->status=='1')
                $arr[$flagData->property_name] = 'required|min:50';
        }

        $validator = JsValidator::make($arr);

        return view('pages.user.home')->with(
            [
                'validator'=>$validator
            ]
        );
    }


    public function userDashboard($tab_name='',$first='') {
        $diamondFlags = $this->connectActivity->getDiamondUsers()->count();
        $resultJson=array();
        $resultJson['tab']['new_li']=$resultJson['tab']['partner_li']=$resultJson['tab']['diamond_li']='';
        if($tab_name=='') {
            $getMatches = $this->connectActivity->getNewOnlineUsers();
            $resultJson['tab']['new_li']='active';
        } else if($tab_name=='partner') {
            $getMatches = $this->connectActivity->getPartnerMatchUsers();
            $resultJson['tab']['partner_li']='active';
        } else if($tab_name=='diamond') {
            $getMatches = $this->connectActivity->getDiamondUsers();
            $resultJson['tab']['diamond_li']='active';
        }
        $resultJson['tab']['diamondFlags']=$diamondFlags;

        /******************* API Specific Job ***********************/

        $i=0;
        foreach($getMatches as $user_profile) {

            $resultJson['data'][$i]['userId']=$user_profile->id;
            $resultJson['data'][$i]['profileLink']=$user_profile->profileLink();
            $resultJson['data'][$i]['profilePic']=$user_profile->getVendorProfilePic();
            $resultJson['data'][$i]['firstName']=ucfirst($user_profile->getFirstname());
            $resultJson['data'][$i]['isOnline']=$user_profile->isOnline();
            $resultJson['data'][$i]['age']=$user_profile->getAge();
            $resultJson['data'][$i]['city_name']=$user_profile->city->name;
            $resultJson['data'][$i]['state_name']=$user_profile->state->name;
            $resultJson['data'][$i]['country_name']=$user_profile->country->name;
            $resultJson['data'][$i]['sect_show']=$user_profile->users_information->sect_show;
            $resultJson['data'][$i]['community']=$user_profile->users_information->get_community();
            $profession=(isset($user_profile->users_information->profession)?$user_profile->users_information->profession->profession:'');
            $resultJson['data'][$i]['profession']=$profession;
            $resultJson['data'][$i]['profession_disp']=(strlen($profession)>18)?(substr($profession,0,18)."..."):$profession;

            $resultJson['data'][$i]['privatePhotoPermissionCnt']=$user_profile->privatePhotoPermissionCnt();
            $resultJson['data'][$i]['viewConversationLink']=route('conversation', ['user_id' => $user_profile->getEncryptedId()]);
            $resultJson['data'][$i]['religion']=$user_profile->users_information->religion->religion;
            $resultJson['data'][$i]['height']=config('constants.height.' . $user_profile->users_information->height);
            $resultJson['data'][$i]['education_flag']=$user_profile->users_information->education;
            $resultJson['data'][$i]['education']=$user_profile->users_information->education->education;
            $resultJson['data'][$i]['body_type']=$user_profile->users_information->body_type;
            $resultJson['data'][$i]['about_flag']=$user_profile->users_information->about;
            $resultJson['data'][$i]['about']=showReadMoreApi($user_profile->users_information->about, 90, $user_profile->profileLink());

            $i++;
        }
        if($first=="first") {
            return json_encode($resultJson);
        } else {
            return response()->json($resultJson);
        }
    }

    public function newonline() {
        $user=Auth::user();
        if($user->isAdmin()) {
            return ('pages.admin.home');
        }
        $newOnlineUser=$this->connectActivity->getNewOnlineUsers();
       // echo "<pre>";
        //print_r($newOnlineUser);
        return view('pages.activity.new-online')->with(['viewedUsers'=>$newOnlineUser]);
    }

    public function newprofiles() {
        $user=Auth::user();
        if($user->isAdmin()) {
            return ('pages.admin.home');
        }
        $newOnlineUser=$this->connectActivity->getNewProfiles();
        return $newOnlineUser;
    }

    public function matchesprofiles() {
        $user=Auth::user();
        if($user->isAdmin()) {
            return ('pages.admin.home');
        }
        $newOnlineUser=$this->connectActivity->getMatchesProfiles();
        return $newOnlineUser;
    }


    public function getProfile($username, UserActivityStatusRepository $repo, UsersImage $usersImage){
        $auth_user=Auth::user();
        $validator = JsValidator::make($this->validationRules);
        $userProfile = User::getWithProfilecode($username)
                        ->hasUserRole()
                        ->hasOppositeGender()
                       // ->skipWhoBlockedByMe()
                        ->SkipWhoBlockedMe()
                        ->SkipWhoSkippedMe()
                        ->first();
//        print_r($userProfile);
        if(is_null($userProfile) || $userProfile->isEmpty()){
            return redirect()->route('profile-not-found');
        }
        if($userProfile->banned == 1 || $userProfile->deactivated == 1 || $userProfile->account_show=='N'){
            return redirect()->route('profile-not-found');
        }

        if($userProfile){
            event(new Activity(config('constants.activity.view_profile'), $userProfile->id));
        }

        $publicwhere = array(
            'user_id' => $userProfile->id,
            'image_type' => 'public',
            'is_approved' => 'Y'
        );
        $existingPublicImages = $usersImage->getWhere($publicwhere);

        $privatewhere = array(
            'user_id' => $userProfile->id,
            'image_type' => 'private',
            'is_approved' => 'Y'
        );
        $existingPrivateImages = $usersImage->getWhere($privatewhere);

        //$public_photo_allowed=config('constants.public_photos_allowed');  // this section will change later. The photos_allowed will come on the basis of user's membership plan
        $private_photo_allowed=config('constants.private_photos_allowed'); // this section will change later. The photos_allowed will come on the basis of user's membership plan

        $blockedFlag=user_blocked_by_me($userProfile->id);  //to check if user blocked by me
        $skippedFlag=user_skipped_by_me($userProfile->id);  //to check if user skipped by me

        /******* New Profile section *******/
        $newProfile = $this->newprofiles();

        /******* Matches Profile section *******/
        $matchesprofile = $this->matchesprofiles();

        /******* Start Similarities ********/
//        $similarities=array();
//        $simCount=0;
//
//        //Age
//        $authAge=$auth_user->getAge();
//        $currAge=$userProfile->getAge();
//        if($currAge>=($authAge-5) && $currAge<=($authAge+5)) {
//            $similarities['age']=$currAge." Years";
//            $simCount++;
//        }
//
//        //Religion
//        if($auth_user->users_information->religion_id == $userProfile->users_information->religion_id ) {
//            $similarities['religion']=$userProfile->users_information->religion->religion;
//            $simCount++;
//        }
//
//        //Do you pray
//        if($auth_user->users_information->does_pray == $userProfile->users_information->does_pray) {
//            $similarities['do_you_pray']=config('constants.do_you_pray.'.$userProfile->users_information->does_pray);
//            $simCount++;
//        }
//
//        //food
//        if($auth_user->users_information->food == $userProfile->users_information->food) {
//            $similarities['food']= config('constants.food.'.$userProfile->users_information->food);
//            $simCount++;
//        }
//
//        //drink
//        if($auth_user->users_information->drink == $userProfile->users_information->drink) {
//            $similarities['drink']=config('constants.drink.'.$userProfile->users_information->drink);
//            $simCount++;
//        }
//
//        //smoke
//        if($auth_user->users_information->smoke == $userProfile->users_information->smoke) {
//            $similarities['smoke']=config('constants.smoke.'.$userProfile->users_information->smoke);
//            $simCount++;
//        }
//
//        $similaritiesArr=getSimilarities($userProfile);
//        $newArr=array_merge($similarities,$similaritiesArr);
        $newArr=$this->similarities($userProfile);
        /******* END Similarities ********/

        $authHasPrivatePhoto=$auth_user->hasPrivatePhotos();
        $authPhotoShareStatus=$auth_user->hasSharedPhotos($userProfile->id);

        $userHasPhotoSharedAccess=$userProfile->hasSharedPhotos($auth_user->id);

        $priPhotoRequestStatus='';
        $privatePhotoRequestRes=$auth_user->hasAlreadySentPrivatePhotoRequest($userProfile->id);
        if(count($privatePhotoRequestRes)>0) {
            $privatePhotoRequestRec = $privatePhotoRequestRes[0];
            $priPhotoRequestStatus = $privatePhotoRequestRec->status;
        }

        $requestToUploadPhotoStatus=$userProfile->hasAlreadySentRequest();

        return view('pages.user.profile')->with(['userProfile'=>$userProfile,
            'existingPublicImages' => $existingPublicImages,
            'existingPrivateImages' => $existingPrivateImages,
            'existingPrivateImagesCount' => count($existingPrivateImages),
            'private_photo_allowed' => $private_photo_allowed,
            'blockedFlag' => $blockedFlag,
            'skippedFlag' => $skippedFlag,
            'newProfiles' => $newProfile,
            'matchesProfiles' =>$matchesprofile,
            'similarities' => $newArr,
            'authHasPrivatePhoto' => $authHasPrivatePhoto,
            'authPhotoShareStatus' => $authPhotoShareStatus,
            'userHasPhotoSharedAccess' => $userHasPhotoSharedAccess,
            'priPhotoRequestStatus' => $priPhotoRequestStatus,
            'requestToUploadPhotoStatus' => $requestToUploadPhotoStatus,
            'validator'=>$validator
        ]);
    }

    public function similarities($userProfile) {
        $auth_user=Auth::user();
        /******* Start Similarities ********/
        $similarities=array();
        $simCount=0;

        //Age
        $authAge=$auth_user->getAge();
        $currAge=$userProfile->getAge();
        if($currAge>=($authAge-5) && $currAge<=($authAge+5)) {
            $similarities['age']=$currAge." Years";
            $simCount++;
        }

        //Religion
        if($auth_user->users_information->religion_id == $userProfile->users_information->religion_id ) {
            $similarities['religion']=$userProfile->users_information->religion->religion;
            $simCount++;
        }

        //Do you pray
        if($auth_user->users_information->does_pray == $userProfile->users_information->does_pray) {
            $similarities['do_you_pray']=config('constants.do_you_pray.'.$userProfile->users_information->does_pray);
            $simCount++;
        }

        //food
        if($auth_user->users_information->food == $userProfile->users_information->food) {
            $similarities['food']= config('constants.food.'.$userProfile->users_information->food);
            $simCount++;
        }

        //drink
        if($auth_user->users_information->drink == $userProfile->users_information->drink) {
            $similarities['drink']=config('constants.drink.'.$userProfile->users_information->drink);
            $simCount++;
        }

        //smoke
        if($auth_user->users_information->smoke == $userProfile->users_information->smoke) {
            $similarities['smoke']=config('constants.smoke.'.$userProfile->users_information->smoke);
            $simCount++;
        }

        $similaritiesArr=getSimilarities($userProfile);
        $newArr=array_merge($similarities,$similaritiesArr);

        return $newArr;
        /******* END Similarities ********/
    }


    public function imageUpload(Request $request){

        $status = event(new FileUpload(config('constants.fileType.image'), $request));
        return response()->json($status);
    }

//    public function deleteImage(Request $request, UsersImage $usersImage){
    public function deleteImage($id, $activesection,UsersImage $usersImage) {
        $user = Auth::user();
        //$id = $request->input('id');
        $response = null;
        if($usersImage->getWhereCount(array('id'=>$id, 'user_id' => $user->id)) > 0){
            $userImage = $usersImage->getWhereOne(array('id'=>$id, 'user_id' => $user->id));
            $imageThumb = $userImage->image_thumb;
            $imageFull = $userImage->image_full;
            if(Storage::exists($imageFull)){
                Storage::delete($imageFull);
                Storage::delete($imageThumb);
            }

            $s3imageThumb = config('constants.S3_WEDDING_IMAGES_VIEW').$userImage->image_thumb;
            $s3imageFull = config('constants.S3_WEDDING_IMAGES_ORIGINAL_VIEW').$userImage->image_full;

            if(Storage::disk('s3')->exists($s3imageThumb)) {
                Storage::disk('s3')->delete($s3imageThumb);
            }

            if(Storage::disk('s3')->exists($s3imageFull)) {
                Storage::disk('s3')->delete($s3imageFull);
            }


            $userImage->delete();
            $response = array(
                'status' => 1,
                'message'=> 'Image deleted successfully'
            );
            return redirect()->back()->withSuccess($response['message'])->with('active_section',$activesection);
        }
        else{
            $response = array(
                'status' => 0,
                'message' => 'Invalid Id'
            );
            return redirect()->back()->withError($response['message'])->with('active_section',$activesection);
        }
       // return response()->json($response);
    }

//    public function setProfileImage(Request $request){
    public function setProfileImage($id, UsersImage $usersImage) {
        $user = Auth::user();
        //$id = $request->input('id');
        $usersImage->whereUpdate(array('user_id' => $user->id, 'is_profile_image'=>'Y'), array('is_profile_image'=>'N'));

        $response = null;
        if($usersImage->getWhereCount(array('id'=>$id, 'user_id' => $user->id)) > 0) {
            $userImage = UsersImage::find($id);
            $userImage->is_profile_image = 'Y';
            if ($userImage->save()) {
                $response = array(
                    'status' => 1,
                    'message' => 'Your image has been set as a profile picture successfully'
                );
                return redirect()->back()->withSuccess($response['message'])->with('active_section', 'public');
            } else{
                $response = array(
                    'status' => 0,
                    'message' => 'There is an internal problem. Please try again later.'
                );
                return redirect()->back()->withError($response['message'])->with('active_section','public');
            }
        } else{
            $response = array(
                'status' => 0,
                'message' => 'Invalid Id'
            );
            return redirect()->back()->withError($response['message'])->with('active_section','public');
        }
        //return response()->json($response);
    }

    //public function movetoprivateImage(Request $request, UsersImage $usersImage) {
    public function movetoprivateImage($id, UsersImage $usersImage) {
        $user = Auth::user();
       // $id = $request->input('id');
        $userid=$user->id;
        //die;
        $response = null;

        $getMembershipStatus=$user->currentMembershipStatus();
        //$status=$getMembershipStatus['status'];
        //$public_photo_allowed=$getMembershipStatus['public_photo_allowed'];
        $private_photo_allowed=$getMembershipStatus['private_photo_allowed'];

        if($usersImage->getWhereCount(array('user_id' => $userid, 'image_type'=>'private')) >=$private_photo_allowed){
            $response = array(
                'status' => 0,
                'message' => 'You already have ' . $private_photo_allowed . ' photos under Private section.'
            );
            return redirect()->back()->withError($response['message'])->with('active_section','public');
        } else {
            if($usersImage->getWhereCount(array('id'=>$id, 'user_id' => $user->id)) > 0) {
                $userImage = UsersImage::find($id);
                $userImage->image_type = 'private';
                $userImage->is_profile_image = 'N';
                if ($userImage->save()) {
                    $response = array(
                        'status' => 1,
                        'message' => 'Your image has been moved under private section successfully.'
                    );
                    return redirect()->back()->withSuccess($response['message'])->with('active_section','public');
                } else {
                    $response = array(
                        'status' => 0,
                        'message' => 'There is problem moving photo. Please try again.'
                    );
                    return redirect()->back()->withError($response['message'])->with('active_section','public');
                }
            } else {
                $response = array(
                    'status' => 0,
                    'message' => 'Invalid Id'
                );
                return redirect()->back()->withError($response['message'])->with('active_section','public');
            }
        }

        //return response()->json($response);
    }

//    public function movetopublicImage (Request $request, UsersImage $usersImage) {
    public function movetopublicImage($id, UsersImage $usersImage) {
        $user = Auth::user();
        $userid=$user->id;
        //$id = $request->input('id');
        $getMembershipStatus=$user->currentMembershipStatus();
        $public_photo_allowed=$getMembershipStatus['public_photo_allowed'];

        $response = null;
        if($usersImage->getWhereCount(array('user_id' => $userid, 'image_type'=>'public')) >=$public_photo_allowed){
            $response = array(
                'status' => 0,
                'message' => 'You already have ' . $public_photo_allowed . ' photos under Public section.'
            );
            return redirect()->back()->withError($response['message'])->with('active_section','private');
 
        } else {

            if($usersImage->getWhereCount(array('id'=>$id, 'user_id' => $user->id)) > 0) {
               // $userImage = UsersImage::where('id',$id)->where('user_id',$userid);
                $userImage = UsersImage::find($id);
                $userImage->image_type = 'public';
                if ($userImage->save()) {
                    $response = array(
                        'status' => 1,
                        'message' => 'Your image has been moved under public section successfully.'
                    );
                    return redirect()->back()->withSuccess($response['message'])->with('active_section','private');
                } else {
                    $response = array(
                        'status' => 0,
                        'message' => 'There is problem moving photo. Please try again.'
                    );
                    return redirect()->back()->withError($response['message'])->with('active_section','private');
                }
            } else {
                $response = array(
                    'status' => 0,
                    'message' => 'Invalid Id'
                );
                return redirect()->back()->withError($response['message'])->with('active_section','private');
            }
        }
        //return response()->json($response);
    }

    public function getProfilePopup($username, UserActivityStatusRepository $repo){
        $userProfile = User::getWithUsername($username)
            ->hasUserRole()
            ->hasOppositeGender()
            ->skipWhoBlockedByMe()
            ->SkipWhoBlockedMe()
            ->skipSkippedUsers()
            ->skipWhoSkippedMe()
            ->first();
        if($userProfile){
            event(new Activity(config('constants.activity.view_profile'), $userProfile->id));
        }
        $validationRules = [
            'message'                  => 'required'
        ];
        $validator = JsValidator::make($validationRules);
        return view('pages.user.profile-popup')->with(['userProfile'=>$userProfile,  'validator'=>$validator]);
    }

    public function imageCoverUpload(Request $request)
    {
        $status = event(new FileUpload(config('constants.fileType.vendorCover'), $request));
        return response()->json($status);
    }

    public function imageProfileUpload(Request $request)
    {
        $status = event(new FileUpload(config('constants.fileType.vendorImage'), $request));
        return response()->json($status);
    }

    public function imageProfileDelete(Request $request)
    {   
        $user = Auth::user();
        $user_image = VendorsImage::where('vendor_id',$user->id)
                            ->where('image_type', 'profile')
                            ->where('id', $request->input('id'))
                            ->first();
        $status = [];
        if($user_image) {
            // remove images
            try {
                if(Storage::disk('s3')->exists('vendor_photos/original/' . $user_image->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/original/' . $user_image->image_full);
                }
                if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $user_image->image_thumb)) {
                    Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $user_image->image_thumb);
                }

                $user_image->delete();

                $status = [
                    'status' => 1,
                    'message' => 'Image deleted successfully'
                ];

            } catch(\Exception $e) {
                $status = [
                    'status' => 0,
                    'message' => 'Some error while deleting image',
                    'error' => $e->getMessage()
                ];
            }
            
        }
        return response()->json($status);
    }

    public function imageProfilePhotoDelete(Request $request)
    {   
        $user = Auth::user();
        $user_image = UsersImage::where('user_id',$user->id)
                            //->where('image_type', 'public')
                            ->where('id', $request->input('id'))
                            ->first();
        $status = [];
        if($user_image) {
            // remove images
            try {
                if(Storage::disk('s3')->exists('vendor_photos/original/' . $user_image->image_full)) {
                    Storage::disk('s3')->delete('vendor_photos/original/' . $user_image->image_full);
                }
                if(Storage::disk('s3')->exists('vendor_photos/thumb_cropped/' . $user_image->image_thumb)) {
                    Storage::disk('s3')->delete('vendor_photos/thumb_cropped/' . $user_image->image_thumb);
                }

                $user_image->delete();

                $status = [
                    'status' => 1,
                    'message' => 'Image deleted successfully'
                ];

            } catch(\Exception $e) {
                $status = [
                    'status' => 0,
                    'message' => 'Some error while deleting image',
                    'error' => $e->getMessage()
                ];
            }
            
        }
        return response()->json($status);
    }

    public function handleVendorInfo(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validationVendorInfoRules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        try {
            
            //save user info.
            $userObj                        = User::where('id', Auth::id())->first();
            $vendorDataObj                  = VendorsInformation::where('vendor_id', Auth::id())->first();
            
            $userObj->name = $request->input('first_name').".".$request->input('last_name');

            if(UserProfileFlag::where(['user_id' => $userObj->id, 'property_name' => 'first_name'])->exists() == false && $userObj->first_name == $request->input('first_name')) {
                $userObj->first_name = $request->input('first_name');
            }
            
            if(UserProfileFlag::where(['user_id' => $userObj->id, 'property_name' => 'last_name'])->exists() == false && $userObj->last_name == $request->input('last_name')) {
                $userObj->last_name = $request->input('last_name');
            }
            $userObj->country_id            = $request->input('country');
            $userObj->city_id               = $request->input('city');
            $userObj->mobile_country_code   = $request->input('mobile_country_code');
            if(UserProfileFlag::where(['user_id' => $userObj->id, 'property_name' => 'mobile_number'])->exists() == false && $userObj->mobile_number == $request->input('mobile_number')) {
                $userObj->mobile_number = $request->input('mobile_number');
            }
            if(UserProfileFlag::where(['user_id' => $userObj->id, 'property_name' => 'address'])->exists() == false && $userObj->address == $request->input('address')) {
                $userObj->address = $request->input('address');
            }
            $userObj->state_id              = $request->input('state');
            if(UserProfileFlag::where(['user_id' => $userObj->id, 'property_name' => 'zip'])->exists() == false && $userObj->zip == $request->input('zip')) {
                $userObj->zip = $request->input('zip');
            }
            $userObj->save();
            // save vendor info.
            $vendorInfoObj = VendorsInformation::where('vendor_id', $userObj->id)->first();
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'company_name'])->exists() == false && $vendorInfoObj->company_name == $request->input('company_name')) {
                $vendorInfoObj->company_name = $request->input('company_name');
            }
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'established_year'])->exists() == false && $vendorInfoObj->established_year == $request->input('established_year')) {
                $vendorInfoObj->established_year = $request->input('established_year');
            }
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'meta_keywords'])->exists() == false) {
                $meta_keywords_val = str_replace("\r", "", $request->input('meta_keywords'));
                $meta_keywords_val = str_replace("\n", "", $meta_keywords_val);
                if($vendorInfoObj->meta_keywords == $meta_keywords_val) {
                    $vendorInfoObj->meta_keywords = $meta_keywords_val;
                }
            }
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'business_information'])->exists() == false && $vendorInfoObj->business_information == $request->input('business_information') ) {
                $vendorInfoObj->business_information = $request->input('business_information');
            }
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'specials'])->exists() == false && $vendorInfoObj->specials == $request->input('specials')) {
                $vendorInfoObj->specials = $request->input('specials');
            }
            
            $vendorInfoObj->display_address           = $request->input('display_address');
            $vendorInfoObj->display_business_number   = $request->input('display_business_number');
            
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'business_website'])->exists() == false && $vendorInfoObj->business_website == $request->input('business_website')) {
                $vendorInfoObj->business_website = $request->has('business_website') ? $request->input('business_website') : "";
            }
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'facebook'])->exists() == false && $vendorInfoObj->facebook == $request->input('facebook')) {
                $vendorInfoObj->facebook = $request->has('facebook') ? $request->input('facebook') : "";
            } else {
                if($request->input('facebook') == "") {
                    $vendorInfoObj->facebook = '';
                }
            }
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'instagram'])->exists() == false && $vendorInfoObj->instagram == $request->input('instagram')) {
                $vendorInfoObj->instagram = $request->has('instagram') ? $request->input('instagram') : "";
            } else {
                if($request->input('instagram') == "") {
                    $vendorInfoObj->instagram = '';
                }
            }
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'pinterest'])->exists() == false && $vendorInfoObj->pinterest == $request->input('pinterest')) {
                $vendorInfoObj->pinterest = $request->has('pinterest') ? $request->input('pinterest') : "";
            } else {
                if($request->input('pinterest') == "") {
                    $vendorInfoObj->pinterest = '';
                }
            }
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'twitter'])->exists() == false && $vendorInfoObj->twitter == $request->input('twitter')) {
                $vendorInfoObj->twitter = $request->has('twitter') ? $request->input('twitter') : "";
            } else {
                if($request->input('twitter') == "") {
                    $vendorInfoObj->twitter = '';
                }
            }
            if(UserProfileFlag::where(['user_id' => $vendorInfoObj->vendor_id, 'property_name' => 'linkedin'])->exists() == false && $vendorInfoObj->linkedin == $request->input('linkedin')) {
                $vendorInfoObj->linkedin = $request->has('linkedin') ? $request->input('linkedin') : "";
            } else {
                if($request->input('linkedin') == "") {
                    $vendorInfoObj->linkedin = '';
                }
            }
            
            $vendorInfoObj->save();

            // add profile flags //
            // profile fields for separate tables array
            $usrProfFldsArry = [
                'first_name', 
                'last_name', 
                'mobile_number', 
                'address', 
                'zip'
            ];
            
            
            $vendInforFldsArry = [
                'company_name', 
                'established_year', 
                'meta_keywords', 
                'business_information', 
                'specials', 
                'business_website', 
                'facebook', 
                'instagram', 
                'pinterest', 
                'twitter', 
                'linkedin'
            ];
            
            // property compare array
            $propComArry = [
                'first_name',
                'last_name',
                'mobile_number',
                'address',
                'zip',
                'company_name', 
                'established_year', 
                'meta_keywords',
                'business_information', 
                'specials', 
                'business_website', 
                'facebook', 
                'instagram', 
                'pinterest', 
                'twitter', 
                'linkedin'
            ];
            

            foreach($propComArry as $property_name) {
                    $profileUpIns = false;
                    $tableRemark = "";
                    if($request->input($property_name) != "") {
                        if(in_array($property_name, $usrProfFldsArry)) {
                            // check that field value is matched with old value
                            if($userObj->$property_name != $request->input($property_name)) {
                                $profileUpIns = true;
                                $tableRemark = "u";
                            }
                        } else if(in_array($property_name, $vendInforFldsArry)) {
                            // check that field value is matched with old value
                            if($vendorDataObj->$property_name != $request->input($property_name)) {
                                $profileUpIns = true;
                                $tableRemark = "vi";
                            }
                        }
                        // insert or update
                        if($profileUpIns == true) {
                            if($property_name == 'meta_keywords') {
                                //Removes all 3 types of line breaks
                                $input_value = str_replace("\r", "", $request->input($property_name));
                                $input_value = str_replace("\n", "", $input_value);
                            } else {
                                $input_value = $request->input($property_name);
                            }
                            
                            if( UserProfileFlag::where('user_id', $userObj->id)->where('property_name', '=', $property_name)->exists() == false ) {
                                UserProfileFlag::Create([
                                    'user_id' 		    => $userObj->id,
                                    'property_name'     => $property_name,
                                    'property_value'    => $input_value,
                                    'table_remark'	    => $tableRemark,
                                    'feedback'		    => "",
                                    'status'            => "0"
                                ]);
                            } else {
                                UserProfileFlag::where(['user_id' => $userObj->id, 'property_name' => $property_name])->update(['property_value' => $input_value]);
                            }
                        }
                    } else {
                        if(in_array($property_name, ['facebook', 'instagram', 'pinterest', 'twitter', 'linkedin'])) {
                            // delete profile flag
                            UserProfileFlag::where(['user_id' => $userObj->id, 'property_name' => $property_name])->delete();
                        }
                    }
                }

            
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => 'Some issue while updating vendor information. Please try again.'
            ]);
        }
        return response()->json([
            'status' => 1,
            'message' => 'Information updated successfully.'
        ]);
    }

    public function handleSubmitFaqAnswers(Request $request)
    {
        $vendor = Auth::user();
        $category = $vendor->vendors_information->vendor_category;
        $faqs = VendorFaq::where('categories', 'like', '%;s:'.strlen($category).':"'.$category.'";%')
                    ->where('status', 1)
                    ->get();
        // save answer submitted
        try {
            foreach($faqs as $faq) {
                $field_submitted = $request->has('field-'.$faq->id) && $request->input('field-'.$faq->id) != null && $request->input('field-'.$faq->id) != "" ? $request->input('field-'.$faq->id) : "";
                // if($field_submitted != "") {
                    // check submitted is array or not
                    if(is_array($field_submitted)) { // check box and conver it to comma separated
                        $field_submitted = implode(",", $field_submitted);
                    }
                    // save record
                    // check if save entry exists
                    if(
                        VendorFaqAnswer::where('vendor_id', Auth::id())
                                    ->where('faq_id', $faq->id)
                                    ->exists()
                    ) {
                        $faqs_asnwer_obj = VendorFaqAnswer::where('vendor_id', Auth::id())
                                                        ->where('faq_id', $faq->id)
                                                        ->first();
                        $faqs_asnwer_obj->answer = $field_submitted;
                        $faqs_asnwer_obj->save();
                    } else {
                        $faqs_asnwer_obj = new VendorFaqAnswer();
                        $faqs_asnwer_obj->vendor_id = Auth::id();
                        $faqs_asnwer_obj->faq_id = $faq->id;
                        $faqs_asnwer_obj->answer = $field_submitted;
                        $faqs_asnwer_obj->save();
                    }
                //}
            }
        } catch (\Exception $e) {
            return back()->withErrors('Some issue while adding faqs answer. Please try again.')->withInput();
        }
        return back()->withSuccess('FAQs saved successfully.');
    }

    public function handleSubmitFaqCustom(Request $request, VendorCategoryRepository $vendorCatRepo)
    {
        $vendor = Auth::user();
        $category = $vendor->vendors_information->vendor_category;
        $catInfo = $vendorCatRepo->getRecords($category);
        if($catInfo->has_custom_faqs ==  0) {
            return response()->json(
                [
                    'status' => 0,
                    'message' => 'Custom FAQs are not now allowed for this type of category.'
                ]
            );
        }
        if($request->has('edit_question') || $request->has('add_question')) {
            try {
                // update answer
                $update_question_arry = $request->input('edit_question');
                $update_answer_arry = $request->input('edit_answer');
                if(isset($update_question_arry) && isset($update_answer_arry)) {
                    foreach($update_question_arry as $update_faq_id => $update_faq) {
                        $ucfaq = VendorCustomFaq::where('vendor_id', $vendor->id)->where('id', $update_faq_id)->first();
                        if(!empty($ucfaq)) {
                            $ucfaq->category_id = $category;
                            if($ucfaq->question != $update_faq || $ucfaq->answer != $update_answer_arry[$update_faq_id]) {
                                $ucfaq->is_approved = 'N';
                                $ucfaq->disapprove_reason = '';
                            }
                            $ucfaq->question = $update_faq;
                            $ucfaq->answer = $update_answer_arry[$update_faq_id];
                            $ucfaq->save();
                        }
                    }
                }
                // save answer newly submitted
                $question_arry = $request->input('add_question');
                $answer_arry = $request->input('add_answer');
                $i = 0;
                if(isset($question_arry) && isset($answer_arry)) {
                    foreach($question_arry as $cust_faq) {
                        $cfaq = new VendorCustomFaq();
                        $cfaq->vendor_id = $vendor->id;
                        $cfaq->category_id = $category;
                        $cfaq->question = $cust_faq;
                        $cfaq->answer = $answer_arry[$i];
                        $cfaq->save();
                        $i++;
                    }
                }
                return response()->json(
                    [
                        'status' => 1,
                        'message' => 'Custom FAQs saved successfully.'
                    ]
                );
            } catch(\Exception $e) {
                return response()->json([
                    'status' => 0,
                    //'message' => 'Some error while saving FAQ',
                    'message' => $e->getMessage()
                ]);
            }
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'Empty FAQ data, please add/update.',
                'error' => 'Empty FAQ data, please add/update.'
            ], 422);
        }
    }


    public function ajaxDeleteCustomFaq(Request $request)
    {
        $vendor = Auth::user();
        try {
            VendorCustomFaq::where('vendor_id', $vendor->id)->where('id', $request->input('id'))->delete();
            $status = [
                'status' => 1,
                'message' => 'FAQ deleted successfully'
            ];

        } catch(\Exception $e) {
            $status = [
                'status' => 0,
                'message' => 'Some error while deleting faq.',
                'error' => $e->getMessage()
            ];
        }
        return response()->json($status);
    }

    public function setVendorProfileImage($id, VendorsImage $vendorsImage) {
        $user = Auth::user();
        $vendorsImage->whereUpdate(
            [ 
                'vendor_id' => $user->id, 
                'is_profile_image' => 'Y'
            ], 
            [ 
                'is_profile_image' => 'N' 
            ]
        );

        $response = null;
        if($vendorsImage->getWhereCount([ 'id' => $id, 'vendor_id' => $user->id, 'is_approved' => 'Y' ]) > 0) {
            $vendorImage = VendorsImage::find($id);
            $vendorImage->is_profile_image = 'Y';
            if ($vendorImage->save()) {
                $response = array(
                    'status' => 1,
                    'message' => 'Your image has been set as a profile picture successfully'
                );
                return redirect()->back()->withSuccess($response['message'])->with('active_section', 'public');
            } else{
                $response = array(
                    'status' => 0,
                    'message' => 'There is an internal problem. Please try again later.'
                );
                return redirect()->back()->withError($response['message'])->with('active_section','public');
            }
        } else{
            $response = array(
                'status' => 0,
                'message' => 'Invalid Id'
            );
            return redirect()->back()->withError($response['message'])->with('active_section','public');
        }
    }

    public function handleVendorVideoSubmit(Request $request, VendorVideo $vendorVideos)
    {
        $validator = Validator::make($request->all(), ['video_title' => 'required', 'embed_code' => 'required|emebed_url', 'description' => 'required|max:500']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        
        $user = Auth::user();
        $userid = $user->id;
        $where = [
            'vendor_id' => $user->id
        ];
        $videosCount = $vendorVideos->getWhereCount($where);
        $allowed_videos = config('constants.vendor_allowed_videos');
        $response = null;
        if( $videosCount >= $allowed_videos ) {
            $response = [
                'status' => 0,
                'message' => 'You already have ' . $allowed_videos . ' videos'
            ];
            return redirect()->back()->withError($response['message']);
        } else {
            $videoObj = new VendorVideo;
            $videoObj->vendor_id = $userid;
            $videoObj->video_title = $request->input('video_title');
            $videoObj->description = strip_tags($request->input('description'));
            // remove height and width attribute and add style.
            $embed_code = $request->input('embed_code');
            // $embed_code = preg_replace('/height="(.*?)"/i', 'style="width:100%;height:275px;"', $embed_code);
            // $embed_code = preg_replace('/width="(.*?)"/i', '', $embed_code);
            $videoObj->embed_code = $embed_code;
            if($videoObj->save()) {
                $response = [
                    'status' => 1,
                    'message' => 'Your video has been added successfully.'
                ];
                return redirect()->back()->withSuccess($response['message']);
            }
        }
    }

    public function handleVendorVideoDelete($id)
    {
        $user = Auth::user();
        // check whether vendor trying to delete his/her own video or not
        if( VendorVideo::where('vendor_id', $user->id)->where('id', $id)->exists() ) {
            if(VendorVideo::where('vendor_id', $user->id)->where('id', $id)->delete()) {
                $response = [
                    'status' => 1,
                    'message' => 'Your video has been deleted successfully.'
                ];
                return redirect()->back()->withSuccess($response['message']);
            }
        } else {
            $response = [
                'status' => 0,
                'message' => 'Delete your video only.'
            ];
            return redirect()->back()->withError($response['message']);
        }
    }

    public function handleVendorShowcaseSubmit(Request $request)
    {
        $user = Auth::user();
        // check record there if there then update else add
        if( VendorShowcase::where('vendor_id', $user->id)->exists() ) {
            $update_vendor_showcase = VendorShowcase::where('vendor_id', $user->id)->first();
            $update_vendor_showcase->additional_categories = $request->has('categories') ? serialize($request->input('categories')) : '';
            if($update_vendor_showcase->save()) {
                $response = [
                    'status' => 1,
                    'message' => 'Your showcase has been updated successfully.'
                ];
                return redirect()->back()->withSuccess($response['message']);
            } else {
                $response = [
                    'status' => 0,
                    'message' => 'Some error while updating showcase.'
                ];
                return redirect()->back()->withError($response['message']);
            }
        } else {
            $add_vendor_showcase = new VendorShowcase;
            $add_vendor_showcase->vendor_id = $user->id;
            $add_vendor_showcase->additional_categories = $request->has('categories') ? serialize($request->input('categories')) : '';
            if($add_vendor_showcase->save()) {
                $response = [
                    'status' => 1,
                    'message' => 'Your showcase has been added successfully.'
                ];
                return redirect()->back()->withSuccess($response['message']);
            } else {
                $response = [
                    'status' => 0,
                    'message' => 'Some error while updating showcase.'
                ];
                return redirect()->back()->withError($response['message']);
            }
        }
    }

    public function ajaxHandleVendorSatellitesSubmit(Request $request)
    {
        $user = Auth::user();
        $action = $request->has('action') ? $request->input('action') : "";
        $satellite_id = $request->has('id') ? $request->input('id') : "";
        // check record there if there then update else add
        if( VendorShowcase::where('vendor_id', $user->id)->exists() ) {
            $update_vendor_showcase = VendorShowcase::where('vendor_id', $user->id)->first();
            $satellites_unserialize = unserialize($update_vendor_showcase->satellites);
            if(is_array($satellites_unserialize)) {
                if( !in_array($satellite_id, $satellites_unserialize) ) {
                    // add into array
                    if($action == 'add') {
                        array_push($satellites_unserialize, $satellite_id);
                        $update_vendor_showcase->satellites = serialize($satellites_unserialize);
                    }
                } else {
                    if($action == 'remove') {
                        // remove element if more than one else empty record
                        if(count($satellites_unserialize) == 1) {
                            $update_vendor_showcase->satellites = '';
                        } else {
                            if (($key = array_search($satellite_id, $satellites_unserialize)) !== false) {
                                unset($satellites_unserialize[$key]);
                                $update_vendor_showcase->satellites = serialize($satellites_unserialize);
                            }
                        }
                    }
                }
            } else {
                if($action == 'add') {
                    $update_vendor_showcase->satellites = serialize([0 => $satellite_id]);
                }
            }
            if($update_vendor_showcase->save()) {
                $response = [
                    'status' => 1,
                    'message' => 'Your showcase has been updated successfully.'
                ];
            } else {
                $response = [
                    'status' => 0,
                    'message' => 'Some error while updating showcase.'
                ];
            }
        } else {
            $add_vendor_showcase = new VendorShowcase;
            $add_vendor_showcase->vendor_id = $user->id;
            if($action == 'add') {
                $add_vendor_showcase->satellites = serialize([0 => $satellite_id]);
            }
            if($add_vendor_showcase->save()) {
                $response = [
                    'status' => 1,
                    'message' => 'Your showcase has been added successfully.'
                ];
            } else {
                $response = [
                    'status' => 0,
                    'message' => 'Some error while updating showcase.'
                ];
            }
        }
        return response()->json($response);
    }

    // upload user profile photo
    public function imageUploadUser(Request $request)
    {
        $status = event(new FileUpload(config('constants.fileType.userProfilePhoto'), $request));
        return response()->json($status);
    }
    
    // show shortlisted vendors
    public function showShortListedVendors(Request $request)
    {
        // get shortlisted vendors
        $user_logged = User::where('id', Auth::id())->first();
        $per_page = $request->has('per_page') ? $request->input('per_page') : 10;
        if($user_logged->hasRole('unverified') == false) {
            abort(403, 'Unauthorized access.');
        }
        $shortlisted = ShortlistedUser::where(['from_user' => $user_logged->id, 'finalized' => '0'])
                    ->latest()
                    ->paginate($per_page);
        
        if($request->ajax()) {
            if($shortlisted->nextPageUrl() != "") {
                $nextPageUrl = 'yes';
            } else {
                $nextPageUrl = '';
            }
            
            return [
                'shortlisted' => view('pages.user.ajax.show-shortlisted', ['shortlisted' => $shortlisted])->render(),
                'next_page' => $request->input('page')+1,
                'next_url' => $nextPageUrl
            ];
        }

        return view('pages.user.shortlisted-vendors', [
            'shortlisted' => $shortlisted,
            'per_page' => $per_page
        ]);
    }

    // show finalized vendors
    public function showFinalizedVendors(Request $request)
    {
        // get finalized vendors
        $user_logged = User::where('id', Auth::id())->first();
        $per_page = $request->has('per_page') ? $request->input('per_page') : 10;
        if($user_logged->hasRole('unverified') == false) {
            abort(403, 'Unauthorized access.');
        }
        $finalized = ShortlistedUser::where(['from_user' => $user_logged->id, 'finalized' => '1'])
                    ->latest()
                    ->paginate($per_page);
        
        if($request->ajax()) {
            if($finalized->nextPageUrl() != "") {
                $nextPageUrl = 'yes';
            } else {
                $nextPageUrl = '';
            }
            
            return [
                'finalized' => view('pages.user.ajax.show-finalized', ['finalized' => $finalized])->render(),
                'next_page' => $request->input('page')+1,
                'next_url' => $nextPageUrl
            ];
        }

        return view('pages.user.finalized-vendors', [
            'finalized' => $finalized,
            'per_page' => $per_page
        ]);
    }

    // user manage profile
    public function showManageProfile(VendorCategoryRepository $vendorCatRepo)
    {
        $auth_user = Auth::user();
        if(User::where('id', $auth_user->id)->hasUserRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $site_id = config('constants.site_id');
        $categories = $vendorCatRepo->getStatusRecords(1)->getVendorByType('wedding')->where('satellites', 'like', '%;s:'.strlen($site_id).':"'.$site_id.'";%')->get(['id', 'name', 'slug']);

        // event date, budget, and vendor categories
        if(@$auth_user->users_information->event_date != '') {
            $expld_date = explode('-', $auth_user->users_information->event_date);
            $event_date = $expld_date[2].'.'.$expld_date[1].'.'.$expld_date[0];
        } else {
            $event_date = '';
        }

        if(@$auth_user->users_information->budget != '') {
            $budget = $auth_user->users_information->budget;
        } else {
            $budget = '';
        }

        if(@$auth_user->users_information->vendor_categories != '') {
            $vendor_categories = unserialize($auth_user->users_information->vendor_categories);
        } else {
            $vendor_categories = [];
        }

        $validator = JsValidator::make($this->userManageProfileRules);
        return view('pages.user.manage-profile', [
            'userObj' => $auth_user,
            'categories' => $categories,
            'event_date' => $event_date,
            'budget' => $budget,
            'vendor_categories' => $vendor_categories,
            'validator' => $validator
        ]);
    }

    // submit manage profile
    public function handleSubmitManageProfile(Request $request)
    {
        $validator = Validator::make($request->all(), $this->userManageProfileRules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $auth_user = Auth::user();
        if(User::where('id', $auth_user->id)->hasUserRole()->exists() == false) {
            return response()->json(['status' => 0, 'message' => 'Unauthorized action.'], 403);
        }
        try {
            //save user info.
            $userObj = User::where('id', Auth::id())->first();
            $userObj->name = $request->input('first_name').".".$request->input('last_name');
            $userObj->first_name = $request->input('first_name');
            $userObj->last_name = $request->input('last_name');
            $userObj->save();
            if($request->has('event_date') && $request->input('event_date') != '') {
                // event date
                $expld_date = explode('.', $request->input('event_date'));
                $event_date = $expld_date[2].'-'.$expld_date[1].'-'.$expld_date[0];
                // validate
                $event_date_check = strtotime(date('Y-m-d', strtotime($event_date) ) );
                $todays = strtotime(date('Y-m-d'));
            } else {
                $event_date = null;
            }

            // if($event_date_check < $todays) {
            //     return response()->json(
            //         [
            //             'status' => 0,
            //             'message' => 'Event date cann\'t be past date.'
            //         ]
            //     );
            // }

            if(UsersInformation::where('user_id', Auth::id())->exists()) {
                $userInfoObj = UsersInformation::where('user_id', Auth::id())->first();
                $userInfoObj->event_date = $event_date;
                $userInfoObj->budget = $request->input('budget');
                if($request->input('vendor_categories') == null) {
                    $userInfoObj->vendor_categories = null;
                } else {
                    $userInfoObj->vendor_categories = serialize($request->input('vendor_categories'));
                }
                $userInfoObj->save();
            } else {
                UsersInformation::create([
                    'user_id' => Auth::id(),
                    'event_date' => $event_date,
                    'budget' => $request->input('budget'),
                    'vendor_categories' => ($request->input('vendor_categories') == null ? null : serialize($request->input('vendor_categories')))
                ]);
            }
            
        } catch (\Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => $e->getMessage()
            ]);
        }
        return response()->json([
            'status' => 1,
            'message' => 'Information updated successfully.'
        ]);
    }

    public function showManageUserPhoto(UsersImage $usersImage)
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasUserRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }
        $where = array(
            'user_id' => $user->id//,
            //'image_type' => 'public'
        );
        $existingImages = $usersImage->getWhere($where);
        $existingImagesCount = $usersImage->getWhereCount($where);
        return view('pages.user.manage-photo')->with([
            'existingImages' => $existingImages,
            'existingImagesCount' => $existingImagesCount
        ]);   
    }

    public function showChangePassword()
    {
        $user = Auth::user();
        if(User::where('id', $user->id)->hasUserRole()->exists() == false) {
            abort(403, 'Unauthorized action.');
        }

        $validator = JsValidator::make($this->validationPasswordRules);
        
        return view('pages.user.change-password', [
            'user' => $user, 
            'validator' => $validator
        ]);   
    }
    
    public function handleChangePassword(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords matches
            return response()->json(["error"=>"Your current password does not matches with the password you provided. Please try again."]);
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            //Current password and new password are same
            return response()->json(["error"=>"New Password cannot be same as your current password. Please choose a different password."]);
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);
        
        $user = Auth::user();
        if(User::where('id', $user->id)->hasUserRole()->exists() == false) {
            return response()->json(["error"=>"Unauthorized action."], 403);
        }

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return response()->json(["success"=>"Password changed successfully !"]);
    }

    public function showDisableAccount()
    {
        return view('pages.user.delete-deactive-account');
    }

    public function showDeactivateAccount()
    {
        return view('pages.user.deactive-account');
    }

    public function showDeleteAccount()
    {
        return view('pages.user.user-delete-account');
    }

    public function showUserNotification()
    {
        $authUser = Auth::user();
        if(User::where('id', $authUser->id)->hasUserRole()->exists() == false) {
            return response()->json(["error"=>"Unauthorized action."], 403);
        }
        $user_info = $authUser->users_information;

        return view('pages.user.user-notification', ['user_info' => $user_info]);
    }
}