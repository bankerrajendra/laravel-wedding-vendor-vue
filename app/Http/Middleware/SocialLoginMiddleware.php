<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class SocialLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $currentRoute = Route::currentRouteName();
        $routesAllowed = [
            'activation-required',
            'activate/{token}',
            'activate',
            'activation',
            'exceeded',
            'authenticated.activate',
            'authenticated.activation-resend',
            'social/redirect/{provider}',
            'social/handle/{provider}',
            'logout',
            'welcome',
            'additional-info',
        ];
		
		## Social login access check
        if (!in_array($currentRoute, $routesAllowed) && !($user->isAdmin())) {

            if ($user && $user->isSocialLoggedIn()) {


               /**if (!is_null($user->deleted_at )) {
                   //Log::info('Deleted user attempted to visit '.$currentRoute.'. ', [$user]);
                   //Auth::logout();
                //   return redirect()->route('login')->withErrors(['Account is deleted by admin. ']);
               } **/
 
                Log::info('User additional Information required'.$currentRoute.'. ', [$user]);

                return redirect()->route('pending-information')
                    ->with([
                        'message' => 'Additional Information is required. ',
                        'status'  => 'danger',
                    ]);
            }
        }

        return $next($request);
    }
}
