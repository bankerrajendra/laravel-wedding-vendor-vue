<?php

namespace App\Http\Middleware;


use Auth;
use Closure;
use Illuminate\Support\Facades\Route;

class CheckGrantAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();
        $currentRoute = Route::currentRouteName();

        $routesAllowed = [
            'activation-required',
            'activate/{token}',
            'activate',
            'activation',
            'exceeded',
            'authenticated.activate',
            'authenticated.activation-resend',
            'social/redirect/{provider}',
            'social/handle/{provider}',
            'logout',
            'welcome',
            'additional-info',
            'public.home',
            'activation-required',
            'additional-information',
            'post-additional-information',
            'pending-information',
            'post-pending-information',
            'upload-image',
            'ajax-upload-image',
            'ajax-delete-image',
            'ajax-movetoprivate-image',
            'ajax-movetopublic-image',
            'set-profile-image',
            'change-password',
            'edit-profile',
            'my-profile',
            'my-hobbies',
            'partner-preference',
            'self-profile',
            'my-photos',
            'manage-photos',
            'edit-profile',
            'dashboard',
            'updateUserFrontend',
            'updateHobbiesFrontend',
            'updatePartnerPreferenceFrontend',
            'delete-image/{id}/{activesection}',
            'ajax-delete-image',
            'change-password',
            'admin-update-user',
            'home/{partner}',
            'api/search-result',
            'upload-vendor-cover',
            'ajax-upload-vendor-cover',
            'upload-vendor-profile',
            'ajax-upload-vendor-profile',
            'ajax-remove-vendor-profile-image',
            'vendor-business-info',
            'handle-post-vendor-business-info',
            'vendor-faqs',
            'get-vendor-faqs-ajax',
            'handle-submit-vendor-faq-answers',
            'handle-submit-vendor-faq-custom',
            'ajax-remove-vendor-custom-faq',
            'vendor-photo-upload',
            'set-vendor-profile-image',
            'vendor-video',
            'handle-vendor-video-submit',
            'delete-vendor-video',
            'vendor-showcase',
            'handle-vendor-showcase-submit',
            'ajax-handle-vendor-satellites-submit',
            'vendor-detail',
            'user-upload-photo',
            'ajax-user-upload-photo',
            'vendor-collect-reviews',
            'handle-submit-collect-reviews',
            'vendor-reviews',
            'user-manage-profile',
            'handle-submit-manage-profile',
            'user-manage-photo',
            'ajax-remove-user-profile-image',
            'vendor-business-mobile',
            'vendor-change-password',
            'vendor-setting-mobile',
            'handle-vendor-change-password',
            'vendor-notification',
            'handle-vendor-email-notification',
            'vendor-delete-account',
            'hide-show-account',
            'inbox',
            'conversation',
            'sent',
            'archive',
            'trash',
            'send-trash',
            'send-archive',
            'sent-conversation',
            'my-membership',
            'add-event',
            'handle-event-add',
            'edit-event',
            'handle-event-edit',
            'ajax-upload-event-edit-cover',
            'ajax-upload-event-edit-profile',
            'ajax-upload-event-edit-poster',
            'ajax-upload-event-add-cover',
            'ajax-upload-event-add-profile',
            'ajax-upload-event-add-poster',
            'vendor-events-reviews',
        ];

        if($user && ($user->access_grant == 'N')){
            if(!in_array($currentRoute, $routesAllowed)){
                // \Log::info('Check Grant access middleware called');
                if($user->hasRole('unverified')) {
                    return redirect()->route('user-manage-profile');
                } else if ($user->hasRole('vendor')) {
                    $vendor_type_wedding_id = config('constants.vendor_type_id.wedding');
                    $vendor_type_event_id = config('constants.vendor_type_id.event');
                    if($user->vendor_type == $vendor_type_wedding_id) {
                        if($user->is_paid == 0) {
                            return redirect()->route('paid-membership');    
                        }
                        return redirect($user->getVendorProfileLink());
                    } else if($user->vendor_type == $vendor_type_event_id) {
                       return redirect()->route('vendor-business-info');
                    }
                } else {
                    return redirect()->route('public.home');
                }
            }
        }

        return $next($request);
    }
}
