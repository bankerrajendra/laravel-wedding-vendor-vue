<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class ActivateIfDeactivated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();
        $currentRoute = Route::currentRouteName();
        $routesAllowed = [
            'activation-required',
            'activate/{token}',
            'activate',
            'activation',
            'exceeded',
            'authenticated.activate',
            'authenticated.activation-resend',
            'social/redirect/{provider}',
            'social/handle/{provider}',
            'logout',
            'welcome',
            'login'
        ];
        if (!in_array($currentRoute, $routesAllowed)) {
            if ($user && $user->deactivated == 1) {
                $user->deactivated =0;
                $user->save();
                Session::flash('account-activated','Your account has been activated.');
//                Log::info('Deactivated user gets activated '.$currentRoute.'. ', [$user]);
            }
        }
        if (!$user) {
            // Log::info('Activated if deactiveated Non registered visit to '.$currentRoute.'. ');
            //return redirect()->route('welcome');
        }
        return $next($request);
    }
}
