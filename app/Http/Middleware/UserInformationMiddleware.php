<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class UserInformationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        $user = Auth::user();
        $currentRoute = Route::currentRouteName();
        $routesAllowed = [
            'activation-required',
            'activate/{token}',
            'activate',
            'activation',
            'exceeded',
            'authenticated.activate',
            'authenticated.activation-resend',
            'social/redirect/{provider}',
            'social/handle/{provider}',
            'logout',
            'welcome'
        ]; 
		
        // if (!in_array($currentRoute, $routesAllowed) && !($user->isAdmin()) && !($user->hasRole('vendor'))) {
        //     if ($user && !($user->hasProfilePhoto())) {
        //         //Log::info('User Profile Photo required'.$currentRoute.'. ', [$user]);

        //         return redirect()->route('user-upload-photo')
        //             ->with([
        //                 'message' => 'User Photo is required. ',
        //                 'status'  => 'danger',
        //             ]);
        //     }
        // }
        if(!in_array($currentRoute, $routesAllowed) && !($user->isAdmin()) && $user->hasRole('vendor') && !($user->hasRole('unverified'))) {
            // check vendor is having completed their required profile fields
            if( $user ) {
                $vendor_type_wedding_id = config('constants.vendor_type_id.wedding');
                $vendor_type_event_id = config('constants.vendor_type_id.event');
                if($user->vendor_type == $vendor_type_wedding_id && !($user->hasCoverPhoto())) {
                    return redirect()->route('upload-vendor-cover')
                    ->with([
                        'message' => 'Upload Vendor Cover Photo',
                        'status'  => 'danger',
                    ]);
                } else if($user->vendor_type == $vendor_type_event_id) {
                    return redirect()->route('vendor-business-info');
                }
            }

            // if( $user && !($user->hasVendorProfilePhoto()) ) {
            //     return redirect()->route('upload-vendor-profile')
            //     ->with([
            //         'message' => 'Upload Vendor Profile Photos',
            //         'status'  => 'danger',
            //     ]);
            // }
        }

        return $next($request);
    }
}
