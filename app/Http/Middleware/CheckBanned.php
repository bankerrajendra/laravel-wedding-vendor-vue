<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class CheckBanned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();
        $currentRoute = Route::currentRouteName();
        $routesAllowed = [
            'activation-required',
            'activate/{token}',
            'activate',
            'activation',
            'exceeded',
            'authenticated.activate',
            'authenticated.activation-resend',
            'social/redirect/{provider}',
            'social/handle/{provider}',
            'logout',
            'welcome',
            'login'
        ];
        if (!in_array($currentRoute, $routesAllowed)) {
            if ($user && $user->banned == 1) {
                Log::info('Banned user attempted to visit '.$currentRoute.'. ', [$user]);
                Auth::logout();
                return redirect()->route('login')->withErrors(['Your account has been disabled!, You may write to us at info@muslimwedding.com ']);
            }
        }
        if (!$user) {
            // Log::info('Check banned Non registered visit to '.$currentRoute.'. ');

            //return redirect()->route('welcome');
        }
        return $next($request);
    }
}
