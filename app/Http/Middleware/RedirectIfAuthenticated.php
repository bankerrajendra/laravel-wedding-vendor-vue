<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $user = Auth::user();
            // \Log::info('Redirect if authenticated middleware called');
            if($user->hasRole('unverified')) {
                if($request->has('redirect')) {
                    return Redirect::to(urldecode($request->input('redirect')));    
                }
                return redirect()->route('user-manage-profile');
            } else if ($user->hasRole('vendor')) {
                $vendor_type_wedding_id = config('constants.vendor_type_id.wedding');
                $vendor_type_event_id = config('constants.vendor_type_id.event');
                if($user->vendor_type == $vendor_type_wedding_id) {
                    if($user->is_paid == 0) {
                        return redirect()->route('paid-membership');    
                    }
                    return redirect($user->getVendorProfileLink());
                } else if($user->vendor_type == $vendor_type_event_id) {
                   return redirect()->route('vendor-business-info');
                }
            } else {
                return redirect()->route('public.home');
            }
        }

        return $next($request);
    }
}
