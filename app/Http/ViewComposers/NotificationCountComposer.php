<?php

namespace App\Http\ViewComposers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use App\Logic\Common\NotificationCountRepository;

class NotificationCountComposer
{
    protected $user;
    protected $notificationCountRepository;

    public function __construct(NotificationCountRepository $notificationCountRepository)
    {
        $this->user = Auth::user();
        $this->notificationCountRepository = $notificationCountRepository;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $notificationCount = null;
        if (Auth::check()) {
            $notificationCount['messages'] = $this->notificationCountRepository->getMessagesCount();
            $notificationCount['myFavourites'] = $this->notificationCountRepository->getMyFavouritesCount();
            $notificationCount['favouritedMe'] = $this->notificationCountRepository->getWhoFavouritedMeCount();
            $notificationCount['viewedMe'] = $this->notificationCountRepository->getViewedMeCount();
        }
        $view->with('notificationCount', $notificationCount);
    }
}
