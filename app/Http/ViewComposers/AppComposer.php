<?php
namespace App\Http\ViewComposers;

// app/Http/ViewComposers/AppComposer.php
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AppComposer {

    public function compose(View $view)
    {
        if(Auth::check()) {
            // check role is vendor or user
            $userObj = User::where('id', Auth::id());
            $VendorCheck = User::where('id', Auth::id())->hasVendorRole()->exists();
            $userCheck = User::where('id', Auth::id())->hasUserRole()->exists();
            if($VendorCheck == true) {
                $view->with('userRole', 'vendor');
            } else if($userCheck == true) {
                $view->with('userRole', 'user');
            } else {
                $view->with('userRole', 'admin');
            }
            //
        }
        
    }

}