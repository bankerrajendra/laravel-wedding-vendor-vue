<?php

namespace App\Http;

use App\Http\Middleware\CheckGrantAccess;
use App\Http\Middleware\CheckIsUserActivated;
use App\Http\Middleware\LastUserActivity;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
        \App\Http\Middleware\HttpsProtocol::class,
        \Barryvdh\Cors\HandleCors::class,

    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\LastUserActivity::class,
            \App\Http\Middleware\CheckBanned::class,
            \App\Http\Middleware\ActivateIfDeactivated::class,
            \Laravel\Passport\Http\Middleware\CreateFreshApiToken::class,
        ],
        'api' => [
            'throttle:60,1',
            \Laravel\Passport\Http\Middleware\CreateFreshApiToken::class,
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            'bindings',
        ],
        'activated' => [
            CheckIsUserActivated::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'          => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic'    => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings'      => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can'           => \Illuminate\Auth\Middleware\Authorize::class,
        'guest'         => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle'      => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'activated'     => CheckIsUserActivated::class,
        'role'          => \jeremykenedy\LaravelRoles\Middleware\VerifyRole::class,
        'permission'    => \jeremykenedy\LaravelRoles\Middleware\VerifyPermission::class,
        'level'         => \jeremykenedy\LaravelRoles\Middleware\VerifyLevel::class,
        'currentUser'   => \App\Http\Middleware\CheckCurrentUser::class,
        'userInfo'      => \App\Http\Middleware\UserInformationMiddleware::class, // Register this new middleware
        'grantAccess'   => CheckGrantAccess::class,
        'talk'  =>  \Nahid\Talk\Middleware\TalkMiddleware::class,
        'socialLogin' => \App\Http\Middleware\SocialLoginMiddleware::class,
        'banned' => \App\Http\Middleware\CheckBanned::class,
    ];
}
