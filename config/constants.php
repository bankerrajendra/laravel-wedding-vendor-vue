<?php

return [
    'image_upload_path' => storage_path('users/uploads/images/original/'),
    'vendor_image_upload_path' => storage_path('users/uploads/images/vendor/original/'),
    'thumb_upload_path' => storage_path('users/uploads/images/thumb/'),
    'vendor_thumb_upload_path' => storage_path('users/uploads/images/vendor/thumb/'),
    'image_upload_path_rel' => 'images/original/',
    'vendor_image_upload_path_rel' => 'uploads/images/vendor/original/',
    'thumb_upload_path_rel' => 'images/thumb/',
    'vendor_thumb_upload_path_rel' => 'uploads/images/vendor/thumb/',
    'blogimage_upload_path_rel' => 'images/blog/',
    'upload_path'   => storage_path('users/uploads/'),
    'upload_url_public' => config('app.url').'/uploads/',
    'S3_WEDDING_IMAGES' => 'https://s3.us-east-2.amazonaws.com/weddingportalphotos/vendor_photos/thumb_cropped/', //'http://weddingportalphotos.s3.amazonaws.com/user_photos/thumb_cropped/');
    'S3_WEDDING_IMAGES_CATEGORY' => 'https://s3.us-east-2.amazonaws.com/weddingportalphotos/vendor_photos/vendor_category/', // category images s3
    'S3_WEDDING_IMAGES_ARTIST' => 'https://s3.us-east-2.amazonaws.com/weddingportalphotos/vendor_photos/artists/', // artist images s3
    'S3_WEDDING_IMAGES_ORIGINAL' => 'https://s3.us-east-2.amazonaws.com/weddingportalphotos/vendor_photos/original/',

    'S3_WEDDING_IMAGES_VIEW' => 'vendor_photos/thumb_cropped/', //'http://weddingportalphotos.s3.amazonaws.com/vendor_photos/thumb_cropped/');
    'S3_WEDDING_IMAGES_ORIGINAL_VIEW' => 'vendor_photos/original/',


    'image_upload_limit' => 1,
    'suggested_users_limit' => 7,
    'admin_email' => 'sbindu1999@gmail.com',

    'unverified_account_allowed' => true,

    'activity' => [
        'like' => 'like',
        'shortlist' => 'shortlist',
        'skip' => 'skip',
        'view_profile' => 'view_profile',
        'delete_viewed_me'=>'delete_viewed_me',
        'delete_shortlisted'=>'delete_shortlisted',
        'like_back'=>'like_back',
        'delete_liked'=>'delete_liked',
        'delete_skipped'=>'delete_skipped',
        'block_profile'=>'block_profile',
        'unblock_profile'=>'unblock_profile',
        'share_photos'=>'share_photos',
        'unshare_photos'=>'unshare_photos',
        'unshare_all_photos'=>'unshare_all_photos',
        'request-toview'=>'request-toview',
        'send-photo-request'=>'send-photo-request',
        'accept_request_photos'=>'accept_request_photos',
        'deny_request_photos'=>'deny_request_photos'
    ],

    'fileType' => [
        'image'=>'image',
        'imageUrl'=>'imageUrl',
        'blogImage'=>'blogImage',
        'vendorCover' => 'vendorCover',
        'vendorImage' => 'vendorImage',
        'userProfilePhoto' => 'userProfilePhoto',
        'eventTempCover' => 'eventTempCover',
        'eventTempImage' => 'eventTempImage'
    ],
    'cms' => [
        'country' =>'Country',
        'city'     => 'City',
        'state' => 'State',
        'language'  => 'Language',
        'religion'  => 'Religion',
        'sect'  => 'Sects'
    ],

    'messages'=>[
        'like' => 'I like your profile',
        'like_back'=> 'You\'ve favorited favorite you back',
        'share_photo'=>'I have shared my private photos with you',
        'private_photo_request'=>'I have sent you the request to view your private photos',
        'accept_photo_request'=>'I have accepted your private photo request. You can view my private photos.',
        'send_photo_request'=>'I have sent you the request to upload your photos',
        'photo_request_uploaded'=>'You can visit my profile in order to see my photos'
    ],

    'partner_preference'=>[
        'start_age' => 18,
        'end_age'=>70,
        'save_message'=>'Partner Preferences has been saved successfully'
    ],

    'search_defaults'=>[
        'start_age' => 18,
        'end_age' => 70,
        'start_height' => 88,
        'end_height' => 213
    ],

    'contact_save'=>[
        'save_message'=>'Your request has been submitted successfully!'
    ],

    'blog_save'=>[
        'save_blog'=>'Blog saved successfully'
    ],


    # religion constants

    'religions'=>[
        'muslim'=>3,
//		'christian'=>4,
//		'hindu'=>2,
//		'sikh'=>8
    ],

    'site_id'=>'1',
    'admin_site_id'=>'1',
    'site_code'=>'MWED',
    'site_vendor_code'=>'MWEDV',
    'site_user_code'=>'MWEDU',
    #payment
    'currency_code' => 'CAD',
    'currency_symbol' => '$',

    'height'=>[
        '88'=> '3ft - 88cm',
        '90'=>'3ft 1in - 90cm',
        '93'=>'3ft 2in - 93cm',
        '95'=>'3ft 2in - 95cm',
        '98'=>'3ft 3in - 98cm',
        '101'=>'3ft 4in - 101cm',
        '103'=>'3ft 5in - 103cm',
        '106'=>'3ft 6in - 106cm',
        '108'=>'3ft 7in - 108cm',
        '111'=>'3ft 8in - 111cm',
        '113'=>'3ft 9in - 113cm',
        '116'=>'3ft 10in - 116cm',
        '118'=>'3ft 11in - 118cm',
        '121'=>'4ft - 121cm',
        '123'=>'4ft 1in - 123cm',
        '127'=>'4ft 2in - 127cm',
        '129'=>'4ft 3in - 129cm',
        '132'=>'4ft 4in - 132cm',
        '134'=>'4ft 5in - 134cm',
        '137'=>'4ft 6in - 137cm',
        '139'=>'4ft 7in - 139cm',
        '142'=>'4ft 8in - 142cm',
        '144'=>'4ft 9in - 144cm',
        '147'=>'4ft 10in - 147cm',
        '149'=>'4ft 11in - 149cm',
        '152'=>'5ft - 152cm',
        '154'=>'5ft 1in - 154cm',
        '157'=>'5ft 2in - 157cm',
        '160'=>'5ft 3in - 160cm',
        '162'=>'5ft 4in - 162cm',
        '165'=>'5ft 5in - 165cm',
        '167'=>'5ft 6in - 167cm',
        '170'=>'5ft 7in - 170cm',
        '172'=>'5ft 8in - 172cm',
        '175'=>'5ft 9in - 175cm',
        '177'=>'5ft 10in - 177cm',
        '180'=>'5ft 11in - 180cm',
        '182'=>'6ft - 182cm',
        '185'=>'6ft 1in - 185cm',
        '187'=>'6ft 2in - 187cm',
        '190'=>'6ft 3in - 190cm',
        '193'=>'6ft 4in - 193cm',
        '195'=>'6ft 5in - 195cm',
        '198'=>'6ft 6in - 198cm',
        '200'=>'6ft 7in - 200cm',
        '203'=>'6ft 8in - 203cm',
        '205'=>'6ft 9in - 205cm',
        '208'=>'6ft 10in - 208cm',
        '210'=>'6ft 11in - 210cm',
        '213'=>'7ft - 213cm'
    ],

    'do_you_pray'=>[
        'always'   => 'Always Pray',
        'never'=> 'Do Not Pray',
        'sometimes'	   => 'Sometimes Pray',
    ],

    'born_reverted'=>[
        'yes'   => 'Born',
        'reverted'=> 'Reverted',
    ],

    'marital_status'=>[
        'never_married'   => 'Never Married',
        'widowed'=> 'Widowed',
        'divorced'	   => 'Divorced',
        'separated'=> 'Separated',
        'annulled'	   => 'Annulled',
    ],

    'gender'=>[
        'M'   => 'Male',
        'F'=> 'Female',
    ],

    'profile_created_by'=>[
        'self'   => 'Self',
        'parents'=> 'Parents',
        'guardian'   => 'Guardian',
        'sibling'=> 'Sibling',
        'friend'   => 'Friend',
        'other'=> 'Other',
    ],

    'skin_tone'=>[
        'Very Fair'   => 'Very Fair',
        'Fair'=> 'Fair',
        'Wheatish'   => 'Wheatish',
        'Dark'=> 'Dark',
        'Brown'   => 'Brown',
        'Medium'=> 'Medium',
        'Wheatish Dark'=> 'Wheatish Dark',
    ],

    'eye_color'=>[
        'Black'   => 'Black',
        'Blue'=> 'Blue',
        'Brown'   => 'Brown',
        'Gray'=> 'Gray',
        'Green'   => 'Green',
        'Hazel'=> 'Hazel',
    ],

    'body_type'=>[
        'Slim'   => 'Slim',
        'Athletic'=> 'Athletic',
        'Average'   => 'Average',
        'Heavy'=> 'Heavy',
    ],

    'currency'=>[
        'INR'   => 'INR',
        'GBP'=> 'GBP',
        'USD'   => 'USD',
        'CAD'=> 'CAD',
        'AUD'=> 'AUD',
    ],

    'weight_measure'=>[
        'lbs' => 'LB',
        'kg'    => 'KG',
    ],

    'food'=>[
        'Veg'   => 'Veg',
        'Non-Veg'   => 'Non Veg',
        'Vegan'   => 'Vegan',
        'Halal' => 'Halal',
        'Others'    => 'Others',
    ],

    'drink'=>[
        'Y'   => 'Yes',
        'N'=> 'No',
        'S'   => 'Occasionally',
    ],

    'smoke'=>[
        'Y'   => 'Yes',
        'N'=> 'No',
        'S'   => 'Occasionally',
    ],

    'horoscope'=>[
        'Aries'   => 'Aries',
        'Taurus'=> 'Taurus',
        'Gemini'   => 'Gemini',
        'Cancer'=> 'Cancer',
        'Leo'   => 'Leo',
        'Vigro'=> 'Vigro',
        'Libra'   => 'Libra',
        'Vruschika'=> 'Vruschika',
        'Sagittarius'   => 'Sagittarius',
        'Capricorn'=> 'Capricorn',
        'Aquarius'   => 'Aquarius',
        'Pisces'=> 'Pisces',
    ],

    'annual_income'=>[
        'Less Than 25,000'   => 'Less Than 25,000',
        '25,001 to 35,000'=> '25,001 to 35,000',
        '35,001 to 50,000'   => '35,001 to 50,000',
        '50,001 to 75,000'=> '50,001 to 75,000',
        '75,001 to 100,000'   => '75,001 to 100,000',
        '100,001 to 150,000'=> '100,001 to 150,000',
        '150,001 to 200,000'   => '150,001 to 200,000',
        '200,001 to 250,000'=> '200,001 to 250,000',
        '250,001 to 300,000'   => '250,001 to 300,000',
        '300,001 to 350,000'=> '300,001 to 350,000',
        '350,001 to 400,000'   => '350,001 to 400,000',
        '400,000+'=> '400,000+',
        'Dont want to specify'=> 'Dont want to specify',
    ],

    /**# religion map with wedding sites
    'weddingSites'=>[
    'muslimwedding'   => [1,2],
    'christianwedding'=> [2],
    'hinduwedding'	   => [3],
    'sikhwedding'     => [1,2,3,4],
    ], **/

    'preferred_movies'=>[
        '1'   => 'Action Suspense',
        '2' => 'Documentaries',
        '3'   => 'Horror',
        '4'   => 'Classics',
        '5'   => 'Drama',
        '6'   => 'Fantasy',
        '7'   => 'Art',
        '8'   => 'Short Films',
        '9'   => 'Comedy',
        '10'   => 'Epics',
        '11'   => 'Hunting',
        '12'   => 'Romantic',
        '13'   => 'World Cinema',
    ],


    'sports'=>[
        '1'   => 'Baseball',
        '2' => 'Basketball',
        '3'   => 'Gardening',
        '4'   => 'Badminton',
        '5'   => 'Boxing Wrestling',
        '6'   => 'Chess',
        '7'   => 'Football Soccer',
        '8'   => 'Hockey',
        '9'   => 'Golf',
        '10'   => 'Cricket',
        '11'   => 'Shooting',
        '12'   => 'Tennis',
        '13'   => 'Athletics',
    ],


    'hobbies'=>[
        '1'   => 'Acting',
        '2' => 'Astrology',
        '3'   => 'Gardening',
        '4'   => 'Interior decoration',
        '5'   => 'Painting',
        '6'   => 'Singing',
        '7'   => 'Animal breeding',
        '8'   => 'Cooking',
        '9'   => 'Film Making',
        '10'   => 'Graphology',
        '11'   => 'Hunting',
        '12'   => 'Photography',
        '13'   => 'Art Handicraft',
        '14'   => 'Bird watching',
        '15'   => 'Dancing',
        '16'   => 'Fishing',
        '17'   => 'Model building',
    ],

    'interests'=>[
        '1'   => 'Acting',
        '2' => 'Health and Fitness',
        '3'   => 'Listening to music',
        '4'   => 'Pets',
        '5'   => 'Theatre',
        '6'   => 'Religion',
        '7'   => 'Bikes and Cars',
        '8'   => 'Driving',
        '9'   => 'Mehendi Designing',
        '10'   => 'Nature',
        '11'   => 'Shopping',
        '12'   => 'Writing',
        '13'   => 'Travel',
        '14'   => 'Movies',
        '15'   => 'Learning new languages',
        '16'   => 'Technology',
        '17'   => 'Yoga',
        '18'   => 'Clubbing',
    ],

    'favourite_music'=>[
        '1'   => 'Acid Rock',
        '2' => 'Bhangra',
        '3'   => 'Classical',
        '4'   => 'Classical Western',
        '5'   => 'Old film songs',
        '6'   => 'Remixes',
        '7'   => 'Ghazals',
        '8'   => 'House music',
        '9'   => 'Classical Hindustani',
        '10'   => 'Soft Rock',
        '11'   => 'Rap',
        '12'   => 'Christian',
        '13'   => 'Bhajans',
        '14'   => 'Sufi Music',
        '15'   => 'World Music',
        '16'   => 'Qawalis',
    ],

    'favourite_reads'=>[
        '1'   => 'Biographies',
        '2' => 'Comics',
        '3'   => 'Humour',
        '4'   => 'Poetry',
        '5'   => 'Business Occupational',
        '6'   => 'Fantasy',
        '7'   => 'Magazines Newspapers',
        '8'   => 'Short Stories',
        '9'   => 'History',
        '10'   => 'Science Fiction',
        '11'   => 'Thriller Suspense',
        '12'   => 'Philosophy',
    ],
    'public_photos_allowed'=>'2',
    'private_photos_allowed'=>'2',
    'photo_thumb_show_for_paid_members'=>'6',
    'doesnt_matter'=>'DM',
    'vendor_profile_photos_allowed' => '10',
    'tinify_key' => 'Hly1HX6tMx1k5nqyStJn0z6TpFtmjdG4',
    'vendor_allowed_videos' => '20',
    'user_budget' => [
        '10000-20000' => '$10000-$20000',
        '20000-30000' => '$20000-$30000',
        '30000-40000' => '$30000-$40000',
        '40000-50000' => '$40000-$50000'
    ],
    'default_collect_message' => 'It was a pleasure to be part of your event! If you have a moment, could you provide a review of our services?<br><br>Thank you in advance for your feedback. We greatly appreciate your help!<br><br>Best,',
    'default_category_title_for_search_vendor' => 'Muslim Wedding Vendors',
    'default_category_title_for_search_event' => 'Muslim Wedding Events',
    'default_category_description_for_search_vendor' => 'To help you get prepared of your special day we have mapped out a list of the Indian wedding vendors that will help plan your wedding. Find Wedding Venues, Event Designer, Photographers, Makeup artists, Bollywood Performers, Wedding Florists, Jewelry, Mehndi Artists, DJ and more',
    'default_category_description_for_search_event' => 'Find Events',
    'account_inactive_text' => 'Your account is currently under review and is waiting admin approval.',
    'vendor_type_id' => [
        'wedding' => 1,
        'event' => 2
    ],
    'vendor_images_pre_name' => 'muslimwedding-',
    'search_vendors_default_page_slug' => 'muslim-vendors'
];