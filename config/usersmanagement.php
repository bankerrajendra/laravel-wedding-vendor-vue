<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Laravel-users setting
    |--------------------------------------------------------------------------
    */
    // Users List Pagination
    'enablePagination'              => true,
    'pagintaionEnabled'             => true,
    'paginateListSize'              => 50, //env('USER_LIST_PAGINATION_SIZE', 25),
    'frontEndSize'                  => 10,
    'blogFrontEndSize'              => 12,
    'enableCmsPagination'           => true,    // This flag is being used in throughout the pages to check if pagination is enabled
    'enableMessagePagination'       => true,
    'messageFrontEndSize'           => 10,

    // Enable Search Users- Uses jQuery Ajax
    'enableSearchUsers'             => true,
    'enableCmsCityPagination'       => false,   //// This flag is being used in admin->cms-city-state page. The dataTable Js will be applied if its flag is false

    'enableMsgPagination'           => true,    // This flag is being used in messages module to check if pagination is enabled
    'msgFrontEndSize'               => 10,

    'enableDashboardPagination'     => false,    // This flag is being used in messages module to check if pagination is enabled
    'dashboardFrontEndSize'         => 8,

    ///
    // Users List JS DataTables - not recommended use with pagination
    'enabledDatatablesJs'           => true,
    'datatablesJsStartCount'        => 25,
    'datatablesCssCDN'              => 'https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css',
    'datatablesJsCDN'               => 'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',
    'datatablesJsPresetCDN'         => 'https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js',
    // Bootstrap Tooltips
    'tooltipsEnabled'               => true,
    'enableBootstrapPopperJsCdn'    => true,
    'bootstrapPopperJsCdn'          => 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
];
