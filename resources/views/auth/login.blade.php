@extends('layouts.app-login')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('content')
<!--login-->
<div class="login_p">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <div class="tabbable-line">
                    <a href="{{config('app.url')}}"><img src="{{ asset('img/logo.png') }}" style="width:60%;"></a>
                    <hr>
                    <!--Login Tab-->
                    <ul class="nav nav-tabs ">
                        <li @if(!$errors->has('vendor-error'))class="active"@endif>
                            <a href="#user-log" data-toggle="tab">
                            User </a>
                        </li>
                        <li @if($errors->has('vendor-error'))class="active"@endif>
                            <a href="#vendor-log" data-toggle="tab">
                            Vendor </a>
                        </li>
                    </ul>
                    <!--End Login Tab-->
                    
                    <div class="tab-content">
                        <!--User Login-->
                        <div class="tab-pane @if(!$errors->has('vendor-error'))active @endif" id="user-log">
                            <h4>User Login</h4>
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            
                            @if($errors->has('user-error') || $errors->has('vendor-error'))
                                @if($errors->has('user-error'))
                                <div class="alert alert-danger">
                                    {{$errors->first('user-error')}}
                                </div>
                                @endif
                            @else
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            @endif
                            <form action="{{ route('login') }}" method="POST" name="user-login-form">
                                @csrf
                                <input type="hidden" name="login_type" value="user" />
                                <div class="form-group login">
                                    <input type="text" name="email" id="email" class="form-control{{$errors->has('email')? ' is-invalid' : ''}}" value="{{old('email')}}" placeholder="Email" >
                                    <span class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                </div>
                                <br>
                                <div class="form-group login">
                                    <input type="password" name="password" id="password" class="form-control{{$errors->has('password')? ' is-invalid' : ''}}" placeholder="Password">
                                    <span class="icon"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                                    @if ($errors->has('password'))
                                        <div class="has-error" style="display:none">   <span class="invalid-feedback help-block error-help-block">
                                            {{ $errors->first('password') }}
                                        </span></div>
                                    @endif
                                </div>
                                <div class="checkbox pull-left">
                                    <label>
                                        <input type="checkbox" value="Yes" name="remember" id="u_remember" {{ old('remember') ? 'checked' : '' }}> Remember login
                                    </label>
                                </div>
                                <div class="form-group text-right">
                                    <a href="{{ route('password.request') }}" class="forgot-pwd">Forgot Password ?</a>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-default btn-block">LOGIN</button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <h5>Don't have an account? <a href="{{ route('user-signup') }}"> Sign Up Now!</a></h5>
                                <h5> Are you a Vendor? <a href="{{ route('vendor-signup') }}"> Sign Up Now!</a></h5>
                                <p>You confirm that you accept the Terms of Service and Privacy Policy</p>
                            </div>
                        </div>
                        <!--End User Login-->
                        
                        <!--Vendor Login-->
                        <div class="tab-pane @if($errors->has('vendor-error'))active @endif" id="vendor-log">
                            <h4>Vendor Login</h4>
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            
                            @if($errors->has('vendor-error') || $errors->has('user-error'))
                                @if($errors->has('vendor-error'))
                                <div class="alert alert-danger">
                                    {{$errors->first('vendor-error')}}
                                </div>
                                @endif
                            @else
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            @endif
                            <form action="{{ route('login') }}" method="POST" name="vendor-login-form">
                                @csrf
                                <input type="hidden" name="login_type" value="vendor" />
                                <div class="form-group login">
                                    <input type="text" name="email" id="email-vendor" class="form-control{{$errors->has('email')? ' is-invalid' : ''}}" value="{{old('email')}}" placeholder="Email">
                                    <span class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                </div>
                                <br>
                                <div class="form-group login">
                                    <input type="password" name="password" id="password-vendor" class="form-control{{$errors->has('password')? ' is-invalid' : ''}}" placeholder="Password">
                                    <span class="icon"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                                    @if ($errors->has('password'))
                                        <div class="has-error" style="display:none">   <span class="invalid-feedback help-block error-help-block">
                                            {{ $errors->first('password') }}
                                        </span></div>
                                    @endif
                                </div>
                                <div class="checkbox pull-left">
                                    <label>
                                        <input type="checkbox" value="Yes" name="remember" id="v_remember" {{ old('remember') ? 'checked' : '' }}> Remember login
                                    </label>
                                </div>
                                <div class="form-group text-right">
                                    <a href="{{ route('password.request') }}" class="forgot-pwd">Forgot Password ?</a>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-default btn-block">LOGIN</button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <h5>Don't have an account? <a href="{{ route('vendor-signup') }}">Sign Up Now!</a></h5>
                                <p>You confirm that you accept the Terms of Service and Privacy Policy</p>
                            </div>
                        </div>
                        <!--End Vendor Login-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Login-->
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection