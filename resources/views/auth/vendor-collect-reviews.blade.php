@extends('layouts.app')
@section('template_linked_css')
<link href="{{asset('css/select-box.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('summernote-0.8.12-dist/summernote.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/summernote-bs3.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                @if(!(Auth::user()->is_active()))
                    <div class="text-center">
                        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                    </div>
                @endif
                <div class="col-sm-4 hidden-xs">
                    @include('partials.vendor-profile-side')
                </div>
                <div class="col-sm-8">
                    <div class="alert alert-danger show-collect-reviews-error" style="display: none;"></div>
                    <div class="alert alert-success show-collect-reviews-success" style="display: none;"></div>
                    <form name="frm-submit-collect-review" id="frm-submit-collect-review" method="POST" action="{{ route('handle-submit-collect-reviews') }}">
                        @csrf
                        <input type="hidden" name="type" value="{{$type}}" />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    Collect Reviews
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Recipient(s)</h4>
                                <p>Collect reviews by sharing your profile URL to all your past clients to boost your credibility with new potential couples. 90% of couples select a vendor if they have positive reviews. Edit and send this message to request reviews from your clients. You will also receive a copy of the email.</p>
                                <h4><b>TO</b></h4>
                            </div>
                            @if($type == 'event')
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="users">Events</label>
                                    <select data-placeholder="Select Events" name="events[]" id="events" class="form-control chosen-select" multiple="" >
                                        @foreach ($events as $event)
                                            <option value="{{$event->id}}">{{$event->event_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="users">User Id</label>
                                    <select data-placeholder="Select User" name="users[]" id="users" class="form-control chosen-select" multiple="" >
                                        @foreach ($user_arry as $user)
                                            <option value="{{$user['id']}}">{{$user['user_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="emails">Email address</label>
                                    <textarea name="emails" id="emails" class="form-control" rows="3"></textarea>
                                    <small>Add comma(,) separated email addresses.</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="tEditor">Message</label>
                                    <textarea name="message" id="tEditor" class="form-control" rows="8" style="display: none;">{!!$review_message!!}</textarea>
                                </div>
                            </div>
                        </div>		
                        <button type="submit" name="submit-collect-reviews-btn" class="btn btn-default">SEND</button>
                        <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none;margin-left: 10px;margin-top: 5px;">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script src="{{asset('js/select-box.js')}}"></script>
<script src="{{asset('summernote-0.8.12-dist/summernote.min.js')}}"></script>
<script type="text/javascript">
$("#tEditor").summernote({height: 300});
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"100%"}
    };
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

$("#frm-submit-collect-review").on('submit', function(e) {
    e.preventDefault();
    $(".show-collect-reviews-error, .show-collect-reviews-success").hide();
    $(".show-collect-reviews-error").html("");
    $(".show-collect-reviews-success").html("");
    var submit_frm = true;
    @if($type == 'event')
    if($('#events').val() == null) {
        $(".show-collect-reviews-error").html("Select atleast one event.");
        $(".show-collect-reviews-error").show();
        submit_frm = false;
    } else {
        $(".show-collect-reviews-error").hide();
    }
    @endif
    if($('#users').val() == null && $('#emails').val() == '') {
        $(".show-collect-reviews-error").html("Select or Fill atlest User Id or Email field.");
        $(".show-collect-reviews-error").show();
        submit_frm = false;
    } else {
        $(".show-collect-reviews-error").hide();
    }
    var form = $("#frm-submit-collect-review");
    var url = form.attr('action');
    var data = new FormData(form[0]);
    if(submit_frm == true) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                // show loading image and disable the submit button
                $("button[name='submit-collect-reviews-btn']").attr('disabled','disabled');
                $("#loader").show();
            },
            success: function(result) {
                // show success message
                if(result.status == 0) {
                    $(".show-collect-reviews-error").html(result.message);
                    if(result.message != '') {
                        $(".show-collect-reviews-error").show();
                    }
                } else {
                    $(".show-collect-reviews-success").html(result.message);
                    if(result.message != '') {
                        $(".show-collect-reviews-success").show();
                    }
                }
            },
            error: function(xhr, status, error){
                var errors = xhr.responseJSON;
                //console.log(errors);
                var err_html = 'Errors<br />';
                for (var key in errors) {
                    if (errors.hasOwnProperty(key)) {
                        var val = errors[key];
                        //console.log(val);
                        err_html += ' - '+val[0]+'<br />';
                    }
                }
                $(".show-collect-reviews-error").html(err_html);
                $(".show-collect-reviews-error").show();
            },
            complete: function () {
                // hide loading image and enable the submit button
                $("#loader").hide();
                $("button[name='submit-collect-reviews-btn']").removeAttr('disabled');
            }
        });
        return false;
    } else {
        return false;
    }
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection