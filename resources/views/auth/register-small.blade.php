@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('content')
<div class="login_p signup">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h3><i>NEW BUSINESS LISTING</i></h3>
                <p>Yay! You’re on your way to creating a New Vendor listing on the world’s biggest wedding resource. Simply fill out the below information.</p>
            </div>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="tabbable-line">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{ route('register') }}" id="registrationForm">
		            @csrf
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Log In Information
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="email">Email address *</label>
                                <input type="email" name="email" id="email" class="form-control">
                                @if ($errors->has('email'))
                                    <span id="email-error" class="help-block error-help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="password">Password *</label>
                                <input type="password" name="password" id="password" class="form-control">
                                @if ($errors->has('password'))
                                    <span id="password-error" class="help-block error-help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Business Information
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="company_name">Company Name *</label>
                                <input type="text" name="company_name" id="company_name" class="form-control">
                                <small>Note: characters length must not be more than 29</small>
                                @if ($errors->has('company_name'))
                                    <span id="company_name-error" class="help-block error-help-block">{{ $errors->first('company_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="vendor_category">Vendor Category *</label>	
                                <select name="vendor_category" id="vendor_category" class="form-control" required="" aria-required="true">
                                    <option value="">Select Category</option>
                                    @foreach($vendor_categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('vendor_category'))
                                    <span id="vendor_category-error" class="help-block error-help-block">{{ $errors->first('vendor_category') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>	
                    <div class="row">	
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="first_name">First Name *</label>
                                <input type="text" name="first_name" id="first_name" class="form-control">
                                @if ($errors->has('first_name'))
                                    <span id="first_name-error" class="help-block error-help-block">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="last_name">Last Name *</label>
                                <input type="text" name="last_name" id="last_name" class="form-control">
                                @if ($errors->has('last_name'))
                                    <span id="last_name-error" class="help-block error-help-block">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>	
                    <div class="row">	
                        <div class="col-sm-3 col-xs-4">
                            <div class="form-group">
                                <label for="mobile_country_code">Country Code *</label>
                                <select name="mobile_country_code" id="mobile_country_code" class="form-control" required="" aria-required="true">
                                    <option value="">Select Country Code</option>
                                    @foreach($countries as $country)
                                        @if (Input::old('mobile_country_code') == $country->id)
                                            <option value="{{$country->id}}" id="{{$country->id}}" selected>+{{$country->phonecode}}  ({{$country->name}})</option>
                                        @else
                                            <option value="{{$country->id}}" id="{{$country->id}}">+{{$country->phonecode}}  ({{$country->name}})</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('mobile_country_code'))
                                    <span id="mobile_country_code-error" class="help-block error-help-block">{{ $errors->first('mobile_country_code') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-9 col-xs-8">
                            <div class="form-group">
                                <label for="mobile_number">Mobile Number *</label>
                                <input type="text" name="mobile_number" id="mobile_number" class="form-control">
                                @if ($errors->has('mobile_number'))
                                    <span id="mobile_number-error" class="help-block error-help-block">{{ $errors->first('mobile_number') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Business location
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="address">Address *</label>
                                <input type="text" name="address" id="address" class="form-control">
                                @if ($errors->has('address'))
                                    <span id="address-error" class="help-block error-help-block">{{ $errors->first('address') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="country">Select Country *</label>
                                <select name="country" id="country" class="form-control load-dependent" required="" aria-required="true" dependent="state">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        @if (Input::old('country') == $country->id)
                                            <option value="{{$country->id}}" id="{{$country->id}}" selected>{{$country->name}}</option>
                                        @else
                                            <option value="{{$country->id}}" id="{{$country->id}}">{{$country->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('country'))
                                    <span id="country-error" class="help-block error-help-block">{{ $errors->first('country') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>	
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="state">Select State *</label>
                                <select name="state" id="state" class="form-control load-dependent" required="" dependent="city" aria-required="true">
                                    <option value="">Select State</option>
                                </select>
                                <i class='state-dropdown-loader' style="display: none;">loading...</i>
                                @if ($errors->has('state'))
                                    <span id="state-error" class="help-block error-help-block">{{ $errors->first('state') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="city">City/Town *</label>
                                <select name="city" id="city" class="form-control" required="" aria-required="true">
                                    <option value="">Select City</option>
                                </select>
                                <i class='city-dropdown-loader' style="display: none;">loading...</i>
                                @if ($errors->has('city'))
                                    <span id="city-error" class="help-block error-help-block">{{ $errors->first('city') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>	
                    <div class="row">	
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="zip">Zip/Pin/Area Code *</label>
                                <input type="text" name="zip" id="zip" class="form-control">
                                @if ($errors->has('zip'))
                                    <span id="zip-error" class="help-block error-help-block">{{ $errors->first('zip') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <label for="agree-checkbox" class="checkbox-inline">
                            <input type="checkbox" id="agree-checkbox" name="agree_checkbox" value="1"><span class="checkmark"></span> I agree to the Privacy &amp; Terms and Conditions
                            </label>
                            <div class="form-group has-error" id="agree_checkbox-error" style="display: none;">
                                <span style="color:#a94442;">Please select I agree to the Privacy &amp; Terms and Conditions.</span>
                            </div>
                        </div>
                    </div>	
                    <br>
                    <input type="hidden" name="type" value="vendor" />
                    <input type="hidden" name="vendor_type" value="wedding" />
                    <button type="submit" class="btn btn-default btn-block">ENTER</button>
                </form>
                <hr>
                <div class="text-center">
                    <h5>Already have an account? <a href="{{route('login')}}">Login Now</a></h5>
                    <h5> Are you a User? <a href="{{route('user-signup')}}">Sign Up Now</a></h5>
                    <p>You confirm that you accept the Terms of Service &amp; Privacy Policy</p>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
        var agree_checkbox = $("#agree-checkbox");
        $(document).ready(function() {
            $('.state-dropdown-loader, .city-dropdown-loader').hide();
            $("#registrationForm").on('submit', function() {
                if(agree_checkbox.is(':checked') == true) {
                    $("#agree_checkbox-error").hide();
                    $(this).val('1');
                    return true;
                } else {
                    $("#agree_checkbox-error").show();
                    $(this).val('');
                    return false;
                }
            });
            agree_checkbox.on('change', function() {
                if(agree_checkbox.is(':checked') == true) {
                    $("#agree_checkbox-error").hide();
                    $(this).val('1');
                } else {
                    $("#agree_checkbox-error").show();
                    $(this).val('');
                }
            });
        });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection