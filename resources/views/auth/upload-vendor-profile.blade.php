@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('dist/css/cropper.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/main.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="login_p signup">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 text-center">
                    <h3><i>PHOTO GALLERY</i></h3>
                </div>
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="photo-step">
                        <p>Add @if($status == "Free")at least @php echo (int) $public_photo_allowed @endphp @endif high-quality photos related to your business and services. Paid members can upload unlimited photos.</p>
                        <p>Storefronts with more photos typically receive more leads.</p>
                        <p>You must add at least <b>one <b>cover</b> and @if($status == "Free")at least @php echo (int) $public_photo_allowed @endphp @endif photos</b>.</p>
                        <div class="cover">
                            @if(count($coverImage) > 0)
                                <img src="{{Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$coverImage[0]->image_full)}}" class="img-responsive" />
                            @else
                                <img src="{{ asset('img/banner-img.jpg') }}" class="img-responsive">
                            @endif
                            <div class="cover-profile">
                                @if(@$vendorObj->getVendorProfilePic())
                                    <img src="{{$vendorObj->getVendorProfilePic()}}" class="img-responsive img-thumbnail" alt="" />
                                @endif
                            </div>
                        </div>
                        <p class="text-center"><b>Gif,PNG &amp; JPG Format (MAX 3MB)</b></p>
                        <br>
                        <p class="text-center">Select and drag images around to create a customized look and feel. By uploading a photo, you certify that you have the right to distribute the photo and it does not violate the User Agreement.</p>
                        <div class="tabbable-line">
                            <div class="crop-avatar text-center">
                                <div class="photo-panel">
                                    @if($existingImagesCount < config('constants.vendor_profile_photos_allowed'))
                                    <div class="avatar-view" data-original-title="" title="">
                                        <span id="fileselector">
                                            <button class="btn btn-default">
                                                <i class="fa fa-upload" aria-hidden="true"></i> Select Photo
                                            </button>
                                        </span>											
                                    </div>
                                    <!-- Cropping modal -->
                                    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <form class="avatar-form" id="imageVendorUploadForm" action="{{route('ajax-upload-vendor-profile')}}" enctype="multipart/form-data" method="post">
                                                    <input type="hidden" name="allowed_photos" value='<?php echo $public_photo_allowed; ?>'>
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                                                        <br>
                                                        <h4 class="text-center">Upload Photo</h4>
                                                        <hr>
                                                        <div class="avatar-body">
                                                            <div class="avatar-upload">
                                                                <input type="hidden" class="avatar-src" name="avatar_src">
                                                                <input type="hidden" class="avatar-data" name="avatar_data">
                                                                <input type="hidden" name="image_type" value="profile">
                                                                <label for="avatarInput">Select Photo</label>
                                                                <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    <div class="avatar-wrapper"></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="avatar-preview preview-lg hidden-xs"><img src="{{ asset('img/male-default.png') }}"></div>
                                                                    <div class="avatar-preview preview-md remove hidden hidden-xs"><img src="{{ asset('img/male-default.png') }}"></div>
                                                                    <div class="avatar-preview preview-sm remove hidden hidden-xs"><img src="{{ asset('img/male-default.png') }}"></div>
                                                                    <div class="avatar-btns">
                                                                        <div class="btn-group"><br>
                                                                            <button type="button" class="btn btn-default fa fa-search-plus" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                                            <button type="button" class="btn btn-default fa fa-search-minus" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <button type="submit" class="btn btn-default btn-block avatar-save">Add Photo</button>
                                                                    <div class="text-center">
                                                                        <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row avatar-btns panel-body">
                                                                <div class="col-md-9 text-center">
                                                                    <div class="btn-group">
                                                                        <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                                                    </div>
                                                                    <div class="btn-group">
                                                                        <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="avatar-view" title="" style="display:none" data-original-title="Change the profile Picture">
                                                                        <img src="{{ asset('img/profile-photo.jpg') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 text-center">
                    <div class="row" style="display:flex;flex-wrap:wrap;" id="profile-images">
                        @php
                            if($status == "Free") {
                                $limitBoxPhoto = $public_photo_allowed;
                            } else {
                                $limitBoxPhoto = $existingImagesCount;
                            }
                        @endphp
                        @for ($i = 0; $i < $limitBoxPhoto; $i++)
                            <div class="col-lg-3 col-sm-4 col-xs-6">
                                <div class="thumbnail">
                                    @if($existingImagesCount >= $i+1)
                                        <img id="img-{{$i+1}}" src="{{Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$existingImages[$i]->image_thumb)}}" class="img-responsive" style="width:100%;">
                                        <div class="panel_body">
                                            @if($existingImages[$i]->is_profile_image != 'Y' && $existingImages[$i]->is_approved == 'Y')
                                            <div class="dropdown pull-left">
                                                <a class="dropdown-toggle" href="javascript:void(0);" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="{{ route('set-vendor-profile-image', ['id' => $existingImages[$i]->id ])}}">set as profile photo</a></li>
                                                </ul>
                                            </div>
                                            @endif
                                            <p class="text-right"><a href="javascript:void(0);" class="remove-image delete" data-img-id="{{$existingImages[$i]->id}}" data-img-div="img-{{$i+1}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></p>
                                            @if($existingImages[$i]->is_approved == 'N')
                                                <p class="var"><small>Awaiting Approval</small></p>
                                            @elseif($existingImages[$i]->is_approved == 'D')
                                                @if($existingImages[$i]->disapprove_reason != NULL && !empty($existingImages[$i]->disapprove_reason))
                                                <p class="var"><small><strong>Reason of disapproval:</strong> {{$existingImages[$i]->disapprove_reason}}</small></p>
                                                @endif
                                            @endif
                                        </div>
                                    @else
                                        <img id="img-{{$i+1}}" src="{{ asset('img/dummy-profile.png') }}" class="img-responsive" style="width:100%;">
                                    @endif
                                </div>
                            </div>
                        @endfor
                    </div>
                    <a href="@if($vendorObj->is_paid == 0){{ route('paid-membership') }}@else{{ route('vendor-business-info') }}@endif" class="pull-right">Enter <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script src="{{asset('dist/js/cropper.min.js')}}"></script>
    <script src="{{asset('dist/js/main-profile.js')}}"></script>
    <script>
        $(".pimage").change(function(e) {
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                var file = e.originalEvent.srcElement.files[i];
                var img = document.createElement("img");
                var reader = new FileReader();
                reader.onloadend = function() {
                    img.src = reader.result;
                }
                reader.readAsDataURL(file);
                $("#aimage").after(img);
            }
        });
        
        $("#imageVendorUploadForm").on('submit', function(e){
            e.preventDefault();
            var image = $("#avatarInput")[0].files[0];
            if (image === undefined){
                var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
                $(".avatar-upload").after(msg);
                return false;
            }else{
                $("#loader").show();
                $("button.avatar-save").attr('disabled','disabled');
                var _token = $('input[name="_token"]').val();
                var thumb_data = $('input[name="avatar_data"]').val();
                var allowed_photos = '<?php echo $public_photo_allowed; ?>';
                var form = new FormData();
                form.append('image', image);
                form.append('_token', _token);
                form.append('thumb_data', thumb_data);
                form.append('allowed_photos', allowed_photos);
                $.ajax({
                    url: "{{route('ajax-upload-vendor-profile')}}",
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(data) {
                        $("#loader").hide();
                        var response = data[0];
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                        if(response.status === 0){
                            $(".avatar-upload").after(msg);
                            $("button.avatar-save").removeAttr('disabled');
                        } else {
                            $("#avatar-modal").modal('hide');
                            document.location.reload(true);
                            // $("#"+response.img_div).attr('src', response.thumb_path);
                            // $('<div class="panel_body"><div class="dropdown pull-left"><a class="dropdown-toggle" href="javascript:void(0);" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i></a><ul class="dropdown-menu"><li><a href="'+route('set-vendor-profile-image', response.image_id)+'">set as profile photo</a></li></ul></div><p class="text-right"><a href="javascript:void(0);" class="remove-image delete" data-img-id="'+response.image_id+'" data-img-div="'+response.img_div+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></p><p class="var"><small>Awaiting Approval</small></p></div>').insertAfter("#"+response.img_div);
                        }
                    },
                    error: function(xhr, status, error){
                        $("#loader").hide();
                        var errors = xhr.responseJSON;
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled');
                    }
                });
            }
        });

        // delete profile image
        $(".remove-image").on('click', function(e){
            e.preventDefault();

            var img_id = $(this).data("imgId");
            var img_div = $(this).data("imgDiv");
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('ajax-remove-vendor-profile-image') }}",
				method: "POST",
				data:{id:img_id, div:img_div, _token:_token},
				success:function(result) {
					$("#"+img_div).attr('src','{{ asset('img/dummy-profile.png') }}');
                    document.location.reload(true);
				},
                error: function(xhr, status, error){
                    var errors = xhr.responseJSON;
                    console.log(xhr.responseJSON);
                }
            });
        });

        $('#avatar-modal').on('hidden.bs.modal', function () {
            $("#loader").hide();
            $(".avatar-body .alert").remove();
            $("#imageVendorUploadForm")[0].reset();
            $("button.avatar-save").removeAttr('disabled');
        });
    </script>
@endsection