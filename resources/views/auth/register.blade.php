@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('canonical'){{$metafields['canonical']}}@endsection
@section('content')
@include('partials.reviews')
   <div class="container text-center panel-body mobile hidden-lg hidden-md hidden-sm">
	<a href="{{route('login')}}" class="btn btn-success" >LOGIN</a>

	<a href="{{route('join')}}" class="btn btn-warning">SIGN UP NOW!</a>
</div>
<!--End Mobile Banner-->

<br><br><br>
Home Page
<br><br><br>

@endsection

@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
