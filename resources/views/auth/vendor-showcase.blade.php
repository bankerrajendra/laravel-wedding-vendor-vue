@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                @if(!(Auth::user()->is_active()))
                    <div class="text-center">
                        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                    </div>
                @endif
                <div class="col-sm-4 hidden-xs">
                    @include('partials.vendor-profile-side')
                </div>
                <div class="col-sm-8 s_case">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Your Showcase
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <form method="post" action="{{ route('handle-vendor-showcase-submit') }}" name="frm-vendor-showcase-submit">
                            @csrf
                            <div class="col-lg-12 col-md-8 col-sm-12 col-xs-12">
                                <p>You can choose where to show your profile to promote your business.</p>
                                @if($satellites != "")
                                    @foreach ($satellites as $satellite)
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">
                                            {{$satellite->title}} : <a href="{{$satellite->url}}" target="_blank">(View Listing)</a>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="btn-group" id="btn-satellites-{{$satellite->id}}">
                                                <input type="hidden" @if(in_array($satellite->id, $showcase_sats))value="{{$satellite->id}}"@endif name="satellites[]" class="satellite-value" />
                                                <button type="button" class="show-btn satellite-btn btn btn-default btn-sm @if(in_array($satellite->id, $showcase_sats))locked_active @else unlocked_inactive @endif" data-satellite-id="{{$satellite->id}}">Show</button>
                                                <button type="button" class="hide-btn satellite-btn btn btn-default btn-sm @if(!in_array($satellite->id, $showcase_sats))locked_active @else unlocked_inactive @endif " data-satellite-id="{{$satellite->id}}">Hide</button>
                                            </div>
                                        </div>
                                    </div>	
                                    @endforeach
                                @else
                                    No sites has been added.
                                @endif
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <hr>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <p>Choose Your Category to display your store:</p>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="checkbox">
                                            <label><input name="ven_cat" type="checkbox" value="{{$vendor_category}}" checked="checked" disabled="disabled">{{$vendor_category_name}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    @if($categories)
                                        @php
                                            $k = 1;
                                        @endphp
                                        @foreach ($categories as $category)
                                            @if($vendor_category != $category->id)
                                                @if($k % 12 == 1)
                                                    <div class="col-sm-4">
                                                @endif
                                                <div class="checkbox">
                                                    <label><input name="categories[]" type="checkbox" value="{{$category->id}}" @if(!empty($showcase_cats) && in_array($category->id, $showcase_cats)) checked="checked" @endif>{{$category->name}} </label>
                                                </div>
                                                @if($k % 12 == 0)
                                                    </div>
                                                @endif
                                                @php
                                                    $k++;
                                                @endphp
                                            @endif
                                        @endforeach
                                        @if ($k % 12 != 1) </div> @endif
                                    @endif
                                    <div class="col-sm-12">
                                        <hr>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-default">SAVE</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
	<script type="text/javascript">
		$('.satellite-btn').on('click', function(){
            var sat_id = $(this).data("satelliteId");
            var action = '';
            if($(this).html() == "Show") {
                $("#btn-satellites-"+sat_id+" .show-btn").addClass("locked_active");
                $("#btn-satellites-"+sat_id+" .show-btn").removeClass("unlocked_inactive");
                $("#btn-satellites-"+sat_id+" .hide-btn").removeClass("locked_active");
                $("#btn-satellites-"+sat_id+" .hide-btn").addClass("unlocked_inactive");
                $("#btn-satellites-"+sat_id+" .satellite-value").val(sat_id);
                action = 'add';
            } else {
                $("#btn-satellites-"+sat_id+" .hide-btn").addClass("locked_active");
                $("#btn-satellites-"+sat_id+" .hide-btn").removeClass("unlocked_inactive");
                $("#btn-satellites-"+sat_id+" .show-btn").removeClass("locked_active");
                $("#btn-satellites-"+sat_id+" .show-btn").addClass("unlocked_inactive");
                $("#btn-satellites-"+sat_id+" .satellite-value").val('');
                action = 'remove';
            }
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: route('ajax-handle-vendor-satellites-submit'),
                method: "POST",
                data:{id:sat_id, action:action, _token:_token},
                success:function(result) {
                    // console(result);
                },
                error: function(xhr, status, error){
                    var errors = xhr.responseJSON;
                    // console.log(xhr.responseJSON);
                }
            });
        });
	</script>
@endsection