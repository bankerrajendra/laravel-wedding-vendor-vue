@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('dist/css/cropper.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/main.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="login_p signup">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 text-center">
                    <h1><span>PROFILE</span> PHOTO</h1>
                </div>
                <div class="col-sm-6 col-sm-offset-3">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="photo-step">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        <div class="row">
                        <div class="col-sm-4 col-xs-12 col-sm-offset-4">
                            <div class="thumbnail profile">
                                @if($existingImagesCount > 0)
                                    <img src="{{ config('constants.S3_WEDDING_IMAGES').$existingImages[0]->image_thumb}}" class="img-responsive" />
                                    <div class="panel_body">
                                        <p class="text-right"><a href="javascript:void(0);" class="remove-image" data-img-id="{{$existingImages[0]->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></p>
                                    </div>
                                @else
                                    <img src="{{ asset('img/dummy.png') }}" class="img-responsive" style="width:100%;">
                                @endif
                            </div>
                        </div>
                        </div>
                        <p class="text-center"><b>Gif, PNG &amp; JPG Format (MAX 3MB)</b></p>
                        <br>
                        <p class="text-center">Select and drag images around to create a customized look and feel. By uploading a photo, you certify that you have the right to distribute the photo and it does not violate the User Agreement.</p>
                        <div class="tabbable-line">
                            <div class="crop-avatar text-center">
                                <div class="photo-panel">
                                    <div class="avatar-view" data-original-title="" title="">
                                        <span id="fileselector">
                                            <button class="btn btn-default">
                                                <i class="fa fa-upload" aria-hidden="true"></i> Select Photo
                                            </button>
                                        </span>											
                                    </div>
                                    <!-- Cropping modal -->
                                    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <form class="avatar-form" id="imageUserUploadForm" action="{{route('ajax-user-upload-photo')}}" enctype="multipart/form-data" method="post">
                                                    @csrf
                                                    <input type="hidden" value="" name="codeprofile">
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                                                        <br>
                                                        <h4 class="text-center">Upload Photo</h4>
                                                        <hr>
                                                        <div class="avatar-body">
                                                            <div class="avatar-upload">
                                                                <input type="hidden" class="avatar-src" name="avatar_src">
                                                                <input type="hidden" class="avatar-data" name="avatar_data">
                                                                <label for="avatarInput">Select Photo</label>
                                                                <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                                            </div>
                                                            
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    <div class="avatar-wrapper"></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="avatar-preview preview-lg hidden-xs"><img src="{{ asset('img/profile-photo.jpg') }}"></div>
                                                                    <div class="avatar-preview preview-md remove hidden hidden-xs"><img src="{{ asset('img/profile-photo.jpg') }}"></div>
                                                                    <div class="avatar-preview preview-sm remove hidden hidden-xs"><img src="{{ asset('img/profile-photo.jpg') }}"></div>
                                                                    <div class="avatar-btns">
                                                                        <div class="btn-group"><br>
                                                                            <button type="button" class="btn btn-default fa fa-search-plus" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                                            <button type="button" class="btn btn-default fa fa-search-minus" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <button type="submit" class="btn btn-default btn-block avatar-save">Add Photo</button>
                                                                    <div class="text-center">
                                                                        <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none">
                                                                    </div>
                                                                </div>
                                                            </div>
    
                                                            <div class="row avatar-btns panel-body">
                                                                <div class="col-md-9 text-center">
                                                                    <div class="btn-group">
                                                                      <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                                                    </div>
                                                                    <div class="btn-group">
                                                                      <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="avatar-view" title="" style="display:none" data-original-title="Change the profile Picture">
                                                                        <img src="{{ asset('img/profile-photo.jpg') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            {{-- <a href="user-signup.php" style="font-size:18px;"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back</a> --}}
                        </div>
                        <div class="col-sm-6 col-xs-12 text-right">
                            {{-- <button type="button" class="btn btn-default">ENTER</button> --}}
                            <a href="{{ route('public.home') }}" class="btn btn-default">ENTER</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script src="{{asset('dist/js/cropper.min.js')}}"></script>
    <script src="{{asset('dist/js/main-profile.js')}}"></script>
    <script>
        $(".pimage").change(function(e) {
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                var file = e.originalEvent.srcElement.files[i];
                var img = document.createElement("img");
                var reader = new FileReader();
                reader.onloadend = function() {
                    img.src = reader.result;
                }
                reader.readAsDataURL(file);
                $("#aimage").after(img);
            }
        });
        
        $("#imageUserUploadForm").on('submit', function(e){
            e.preventDefault();
            var image = $("#avatarInput")[0].files[0];
            if (image === undefined){
                var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
                $(".avatar-upload").after(msg);
                return false;
            } else {
                $("#loader").show();
                $("button.avatar-save").attr('disabled','disabled');
                var _token = $('input[name="_token"]').val();
                var thumb_data = $('input[name="avatar_data"]').val();
                var form = new FormData();
                form.append('image', image);
                form.append('_token', _token);
                form.append('thumb_data', thumb_data);
                $.ajax({
                    url: route('ajax-user-upload-photo'),
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(data) {
                        $("#loader").hide();
                        var response = data[0];
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                        if(response.status === 0){
                            $(".avatar-upload").after(msg);
                            $("button.avatar-save").removeAttr('disabled');
                        } else {
                            $("#avatar-modal").modal('hide');
                            $(".profile .img-responsive").attr('src', response.thumb_path);
                        }
                    },
                    error: function(xhr, status, error){
                        $("#loader").hide();
                        var errors = xhr.responseJSON;
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled');
                    }
                });
            }
        });
        // delete profile image
        $(".remove-image").on('click', function(e){
            e.preventDefault();
            if(confirm('Are you sure you want to remove this image?')) {
                var img_id = $(this).data("imgId");
                var img_div = $(this).data("imgDiv");
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ route('ajax-remove-user-profile-image') }}",
                    method: "POST",
                    data:{id:img_id, div:img_div, _token:_token},
                    success:function(result) {
                        $("#"+img_div).attr('src','{{ asset('img/dummy-profile.png') }}');
                        document.location.reload(true);
                    },
                    error: function(xhr, status, error){
                        var errors = xhr.responseJSON;
                        console.log(xhr.responseJSON);
                    }
                });
            }
        });
        $('#avatar-modal').on('hidden.bs.modal', function () {
            $("#loader").hide();
            $(".avatar-body .alert").remove();
            $("#imageUserUploadForm")[0].reset();
            $("button.avatar-save").removeAttr('disabled');
        });

    </script>
@endsection