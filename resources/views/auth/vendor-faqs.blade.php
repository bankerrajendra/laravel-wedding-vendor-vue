@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                @if(!(Auth::user()->is_active()))
                    <div class="text-center">
                        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                    </div>
                @endif
                <div class="col-sm-4 hidden-xs">
                    @include('partials.vendor-profile-side')
                </div>
                <div class="col-sm-8">
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Frequently Asked Questions
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                            @if($hasAdminFaq == 1)<li class="active"><a data-toggle="tab" href="#faq" aria-expanded="true">FAQ</a></li>@endif
                            @if($hasCustomFaq == 1)<li @if($hasAdminFaq == 0 && $hasCustomFaq == 1) class="active" @else class="" @endif><a data-toggle="tab" href="#customfaq" aria-expanded="false">Manage Custom FAQ</a></li>@endif
                    </ul>
                    <div class="tab-content">
                        @if($hasAdminFaq == 1)
                            <div id="faq" class="tab-pane fade active in">
                                @if(session('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{session('success')}}
                                    </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <form name="frm-faq-answers" id="frm-faq-answers" method="post" action="{{ route('handle-submit-vendor-faq-answers') }}">
                                    @csrf
                                    @if(!empty($faqs))
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($faqs as $faq)
                                            <div class="row">	
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="field-{{$faq->id}}">{{$i}}. {{$faq->question}}</label>
                                                        @switch($faq->answer_type)
                                                            @case('text')
                                                                <input type="text" name="field-{{$faq->id}}" id="field-{{$faq->id}}" class="form-control" placeholder="{{@$faq->vendor_faq_answer_placeholder->placeholder}}" value="{{@$faq->answer}}">
                                                                @break
                                                            @case('text_area')
                                                                <textarea class="form-control" name="field-{{$faq->id}}" id="field-{{$faq->id}}" placeholder="{{@$faq->vendor_faq_answer_placeholder->placeholder}}" rows="5">{{@$faq->answer}}</textarea>
                                                                @break
                                                            @case('radio')
                                                                @php
                                                                    $options_string =  $faq->vendor_faq_answer_options->options;
                                                                    $options_arry = explode(",", $options_string);
                                                                @endphp
                                                                @foreach ($options_arry as $option)
                                                                    <br /><label class="radio-inline">
                                                                        <input type="radio" name="field-{{$faq->id}}" value="{{trim($option)}}" @if(@$faq->answer == trim($option)) checked="checked" @endif>{{trim($option)}}
                                                                    </label>    
                                                                @endforeach
                                                                @break
                                                            @case('checkbox')
                                                                @php
                                                                    $options_string =  $faq->vendor_faq_answer_options->options;
                                                                    $options_arry = explode(",", $options_string);
                                                                    $chk_answers = explode(",", @$faq->answer);
                                                                    $k = 1;
                                                                @endphp
                                                                    <div class="row">
                                                                    @foreach ($options_arry as $option)
                                                                        @if($k % 3 == 1)
                                                                            <div class="col-sm-3">
                                                                        @endif
                                                                                <div class="checkbox">
                                                                                    <label><input type="checkbox" value="{{trim($option)}}"name="field-{{$faq->id}}[]" @if(in_array(trim($option), $chk_answers)) checked="checked" @endif>{{trim($option)}}</label>
                                                                                </div>
                                                                        @if($k % 3 == 0)
                                                                            </div>
                                                                        @endif
                                                                        @php
                                                                            $k++;    
                                                                        @endphp
                                                                    @endforeach
                                                                    @if ($k % 3 != 1) </div> @endif
                                                                    </div>
                                                                @break
                                                        @endswitch
                                                    </div>
                                                </div>
                                            </div>      
                                        @php
                                            $i++;    
                                        @endphp
                                        @endforeach
                                    @endif	
                                    <button type="submit" class="btn btn-default pull-right">SAVE</button>
                                    <a href="{{ $userObj->getVendorProfileLink() }}" class="btn btn-default pull-right" style="margin-right:15px;">PREVIEW</a>
                                </form>
                            </div>
                        @endif
                        @if($hasCustomFaq == 1)
                            <div id="customfaq" class="tab-pane fade @if($hasAdminFaq == 0 && $hasCustomFaq == 1) active in @endif">
                                <div class="alert alert-danger show-custom-faq-error" style="display: none;"></div>
                                <div class="alert alert-success show-custom-faq-success" style="display: none;"></div>
                                <form method="post" name="frm-faq-custom" id="frm-faq-custom" action="{{ route('handle-submit-vendor-faq-custom') }}">
                                    @csrf
                                    @if(!empty($customFaqs))
                                        @php
                                            $j = 1;
                                        @endphp
                                        <div id="custom-faq-ajax-div">
                                        @foreach ($customFaqs as $custom_faq)
                                            <div class="row" id="cust-faq-{{$custom_faq->id}}">	
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="field-{{$custom_faq->id}}">Question</label>
                                                        <input type="text" name="edit_question[{{$custom_faq->id}}]" class="form-control custom-question" placeholder="Question" value="{{@$custom_faq->question}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="cname">Answer</label>
                                                        <textarea name="edit_answer[{{$custom_faq->id}}]" cols="50" rows="3" class="form-control custom-answer" placeholder="Answer">{{@$custom_faq->answer}}</textarea>
                                                    </div>
                                                </div>
                                                @if($custom_faq->is_approved == 'N')
                                                    <div class="col-sm-5" style="text-align: left;">
                                                        <span style='color:green;text-align: center;'>Awaiting Approval</span>
                                                    </div>
                                                @elseif($custom_faq->is_approved == 'D')
                                                    <div class="col-sm-5" style="text-align: left;">
                                                        @if($custom_faq->disapprove_reason != NULL && !empty($custom_faq->disapprove_reason))
                                                            <span style='color:red;text-align: center;'><strong>Reason of disapproval:</strong> {{$custom_faq->disapprove_reason}}</span>
                                                        @endif
                                                    </div>
                                                @endif
                                                <div class="col-sm-5" style="text-align:right;color:#000;display: block;float:right;"><a style="color:#000;" href="javascript:void(0);" onclick="javascript:delCustFaq({{$custom_faq->id}});"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></div>
                                            </div>
                                        @php
                                            $j++;    
                                        @endphp
                                        @endforeach
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div id="newFaq">
                                            <div>
                                                @if(empty($customFaqs))
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="cname">Question</label>
                                                        <input type="text" name="add_question[]" class="form-control custom-question" placeholder="Question">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="cname">Answer</label>
                                                        <textarea name="add_answer[]" cols="50" rows="3" class="form-control custom-answer" placeholder="Answer"></textarea>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <p id="addnew">
                                                <a href="javascript:void(0);" onclick="javascript:new_custom_faq();"> <i class="fa fa-plus" aria-hidden="true"></i> Add New </a>
                                            </p>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="submit" class="btn btn-default" name="submit-custom-faq">
                                            <a href="{{ route('vendor-faqs') }}" class="btn btn-default" style="margin-left:15px;">Reset</a>
                                        </div>
                                        <div class="col-sm-12" style="margin-left: 50px; margin-top: 10px;">
                                            <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none;">
                                        </div>
                                    </div>	
                                </form>
                                <div class="row">
                                    <div id="newfaqtpl" style="display:none">
                                        <div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="cname">Question</label>
                                                    <input type="text" name="add_question[]" class="form-control custom-question" placeholder="Question">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="cname">Answer</label>
                                                    <textarea name="add_answer[]" cols="50" rows="3" class="form-control custom-answer" placeholder="Answer"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script lang="javascript" type="text/javascript">
/*
This script is identical to the above JavaScript function.
*/
var ct = 1;
function new_custom_faq()
{
	ct++;
	var div1 = document.createElement('div');
	div1.id = ct;
	// link to delete extended form elements
	var delLink = '<div style="text-align:right;margin-right:15px;color:#000;display: block;clear: both;"><a style="color:#000;" href="javascript:void(0);" onclick="javascript:delFaq('+ ct +');"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></div>';
	div1.innerHTML = document.getElementById('newfaqtpl').innerHTML + delLink;
	document.getElementById('newFaq').appendChild(div1);
}
// function to delete the newly added set of elements
function delFaq(eleId)
{
	d = document;
	var ele = d.getElementById(eleId);
	var parentEle = d.getElementById('newFaq');
	parentEle.removeChild(ele);
}
function delCustFaq(faqId)
{
    if (confirm("Are you sure you want to delete this entry?")) {
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{ route('ajax-remove-vendor-custom-faq') }}",
            method: "POST",
            data:{id:faqId, _token:_token},
            success:function(result) {
                document.getElementById("cust-faq-"+faqId).remove();
            },
            error: function(xhr, status, error){
                var errors = xhr.responseJSON;
                console.log(xhr.responseJSON);
            }
        });
    } else {
        return false;
    }
}

    $(document).ready(function() {
        $('form#frm-faq-custom').on('submit', function(e) {
            e.preventDefault();
            $(".show-custom-faq-error, .show-custom-faq-success").hide();
            $(".show-custom-faq-error").html("");
            $(".show-custom-faq-success").html("");
            var submit_frm = true;
            $('form#frm-faq-custom .custom-question').each(function (index, value) {
                if($(this).val().length > 250) {
                    alert("Question should not be more than 250 characters in length.");
                    submit_frm = false;
                }
                if($(this).val().length == 0) {
                    alert("Question cann't be empty.");
                    submit_frm = false;
                }
            });
            $('form#frm-faq-custom .custom-answer').each(function (index, value) {
                if($(this).val().length > 500) {
                    alert("Answer should not be more than 500 characters in length.");
                    submit_frm = false;
                }
                if($(this).val().length == 0) {
                    alert("Answer cann't be empty.");
                    submit_frm = false;
                }
            });
            if(submit_frm == true) {
                var form = $("#frm-faq-custom");
                var url = form.attr('action');
                var data = new FormData(form[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    beforeSend: function () {
                        // show loading image and disable the submit button
                        $("input[name='submit-custom-faq']").attr('disabled','disabled');
                        $("#loader").show();
                    },
                    success: function(result) {
                        // show success message
                        if(result.status == 0) {
                            $(".show-custom-faq-error").html(result.message);
                            $(".show-custom-faq-error").show();
                        } else {
                            $(".show-custom-faq-success").html(result.message);
                            $(".show-custom-faq-success").show();
                        }
                        getVendorFaqsAjx();
                    },
                    error: function(xhr, status, error){
                        var errors = xhr.responseJSON;
                        $(".show-custom-faq-error").html(errors.error);
                        $(".show-custom-faq-error").show();
                    },
                    complete: function () {
                        // hide loading image and enable the submit button
                        $("#loader").hide();
                        $("input[name='submit-custom-faq']").removeAttr('disabled');
                    }
                });
                return false;
            } else {
                return false;
            }
        });
    });
    function getVendorFaqsAjx()
    {
        $.ajax({
            url: "{{ route('get-vendor-faqs-ajax') }}",
            method: "GET",
            success:function(result) {
                var html_faqs = '';
                $.each( result, function( key, custom_faq ) {
                    html_faqs +='<div class="row" id="cust-faq-'+custom_faq.id+'">';
                    html_faqs +='<div class="col-sm-12"><div class="form-group"><label for="field-'+custom_faq.id+'">Question</label><input type="text" name="edit_question['+custom_faq.id+']" class="form-control custom-question" placeholder="Question" value="'+custom_faq.question+'"></div></div>';
                    html_faqs += '<div class="col-sm-12"><div class="form-group"><label for="cname">Answer</label><textarea name="edit_answer['+custom_faq.id+']" cols="50" rows="3" class="form-control custom-answer" placeholder="Answer">'+custom_faq.answer+'</textarea></div></div>';
                    if(custom_faq.is_approved == 'N') {
                        html_faqs += '<div class="col-sm-5" style="text-align: left;"><span style="color:green;text-align: center;">Awaiting Approval</span></div>';
                    }
                    else if(custom_faq.is_approved == 'D') {
                        html_faqs += '<div class="col-sm-5" style="text-align: left;">';
                            if(custom_faq.disapprove_reason != null && custom_faq.disapprove_reason != '') {
                                html_faqs += '<span style="color:red;text-align: center;"><strong>Reason of disapproval:</strong> '+custom_faq.disapprove_reason+'</span>';
                            }
                        html_faqs += '</div>';
                    }
                    html_faqs += '<div class="col-sm-5" style="text-align:right;color:#000;display: block;float:right;"><a style="color:#000;" href="javascript:void(0);" onclick="javascript:delCustFaq('+custom_faq.id+');"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></div></div>';
                });

                $("#custom-faq-ajax-div").html(html_faqs);
                $("#newFaq div").html('');
                
            },
            error: function(xhr, status, error){
                var errors = xhr.responseJSON;
                console.log(xhr.responseJSON);
            }
        });
        
    }
</script>
@endsection