@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('dist/css/cropper.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/main.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                @if(!(Auth::user()->is_active()))
                    <div class="text-center">
                        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                    </div>
                @endif
                <div class="col-sm-4 hidden-xs">
                    @include('partials.vendor-profile-side')
                </div>
                <div class="col-sm-8">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Banner Upload
                            </div>
                        </div>
                    </div>
                    <div class="row">	
                        <div class="col-sm-12 panel-body cover">
                            <p>Add @if($status == "Free")at least @php echo (int) $public_photo_allowed+1 @endphp @endif high-quality photos related to your business and services. Paid members can upload unlimited photos.</p>
                            <p>Storefronts with more photos typically receive more leads.</p>
                            <p class="text-center"><b>Gif,PNG &amp; JPG Format (MAX 1455*375 pixel)</b></p>
                            @if($coverImageCount > 0)
                                <img src="{{Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$coverImage[0]->image_full)}}" class="img-responsive" />
                                @if($coverImage[0]->is_approved == 'N')
                                    <br />
                                    <span id="awaiting-appval-sec" style='color:green;text-align: center;position: absolute;'>Awaiting Approval</span>
                                @elseif($coverImage[0]->is_approved == 'D')
                                    @if($coverImage[0]->disapprove_reason != NULL && !empty($coverImage[0]->disapprove_reason))
                                        <br />
                                        <span id="reason-disapproval-sec" style='color:red;text-align: center;position: absolute;'><strong>Reason of disapproval:</strong> {{$coverImage[0]->disapprove_reason}}, <strong>Replace image by uploading new one.</strong></span>
                                    @endif
                                @endif
                            @else
                                <img src="{{ asset('img/banner-img.jpg') }}" class="img-responsive">
                            @endif
                        </div>
                    </div>
                    <div class="crop-avatar text-center">
                        <div class="photo-panel">
                            <div class="avatar-view" data-original-title="" title="">
                                <span id="fileselector">
                                    <button class="btn btn-default">
                                        <i class="fa fa-upload" aria-hidden="true"></i> Select Banner
                                    </button>
                                </span>											
                            </div>
                            <!-- Cropping modal -->
                            <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <form class="avatar-form" id="bannerVendorUploadForm" action="{{ route('ajax-upload-vendor-cover') }}" enctype="multipart/form-data" method="post">
                                            @csrf
                                            <input type="hidden" value="" name="codeprofile">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                                                <br>
                                                <h4 class="text-center">Upload Banner</h4>
                                                <hr>
                                                <div class="avatar-body">
                                                    <div class="avatar-upload">
                                                        <input type="hidden" class="avatar-src" name="avatar_src">
                                                        <input type="hidden" class="avatar-data" name="avatar_data">
                                                        <label for="avatarInput">Select Photo</label>
                                                        <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="avatar-wrapper"></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="avatar-preview preview-lg hidden-xs"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                                            <div class="avatar-preview preview-md remove hidden hidden-xs"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                                            <div class="avatar-preview preview-sm remove hidden hidden-xs"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                                            {{-- Zoom in out STARTED --}}
                                                            <div class="avatar-btns">
                                                                <div class="btn-group"><br>
                                                                    <button type="button" class="btn btn-default fa fa-search-plus" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                                    <button type="button" class="btn btn-default fa fa-search-minus" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                                                </div>
                                                            </div>
                                                            {{-- Zoom in out ENDED --}}
                                                            <br>
                                                            <button type="submit" class="btn btn-default btn-block avatar-save">Add Banner</button>
                                                            <div class="text-center">
                                                                <img src="{{asset('img/loader.gif')}}" id="loader-banner" width="20" style="display: none">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row avatar-btns panel-body">
                                                        <div class="col-md-9 text-center">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                                            </div>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="avatar-view" title="" style="display:none" data-original-title="Change the profile Picture">
                                                                <img src="{{ asset('img/banner-img.jpg') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Photo Upload
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 panel-body">
                            <p>Add @if($status == "Free")at least @php echo (int) $public_photo_allowed+1 @endphp @endif high-quality photos related to your business and services. Paid members can upload unlimited photos.</p>
                            <p>Storefronts with more photos typically receive more leads.</p>
                            <p>You must add at least one banner and @if($status == "Free"){{$public_photo_allowed}} @endif photos.</p>
                        </div>	
                        <div class="crop-avatar-another text-center">
                            <div class="photo-panel">
                                <div class="avatar-view-photo" data-original-title="" title="">
                                    <span id="fileselector">
                                        <button class="btn btn-default">
                                            <i class="fa fa-upload photo-upload" aria-hidden="true"></i> Select Photo
                                        </button>
                                    </span>											
                                </div>
                                <!-- Cropping modal -->
                                <div class="modal fade" id="avatar-modal-photo" aria-hidden="true" aria-labelledby="avatar-modal-photo-label" role="dialog" tabindex="-1" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <form class="avatar-form-photo" id="photoVendorUploadForm" action="{{ route('ajax-upload-vendor-profile') }}" enctype="multipart/form-data" method="post">
                                                @csrf
                                                <input type="hidden" value="" name="codeprofile">
                                                <input type="hidden" name="allowed_photos" value='<?php echo $public_photo_allowed; ?>'>
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                                                    <br>
                                                    <h4 class="text-center">Upload Photo</h4>
                                                    <hr>
                                                    <div class="avatar-body-photo">
                                                        <div class="avatar-upload-photo">
                                                            <input type="hidden" class="avatar-src-photo" name="avatar_src">
                                                            <input type="hidden" class="avatar-data-photo" name="avatar_data_photo">
                                                            <label for="avatarInputPhoto">Select Photo</label>
                                                            <input type="file" class="avatar-input-photo" id="avatarInputPhoto" name="avatar_file_photo">
                                                        </div>
                                                        
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <div class="avatar-wrapper-photo"></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="avatar-preview-photo preview-lg hidden-xs"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                                                <div class="avatar-preview-photo preview-md remove hidden hidden-xs"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                                                <div class="avatar-preview-photo preview-sm remove hidden hidden-xs"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                                                {{-- Zoom in out STARTED --}}
                                                                <div class="avatar-btns">
                                                                    <div class="btn-group"><br>
                                                                        <button type="button" class="btn btn-default fa fa-search-plus zoom-in-btn" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                                        <button type="button" class="btn btn-default fa fa-search-minus zoom-out-btn" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                                                    </div>
                                                                </div>
                                                                {{-- Zoom in out ENDED --}}
                                                                <br>
                                                                <button type="submit" class="btn btn-default btn-block avatar-save-photo">Add Photo</button>
                                                                <div class="text-center">
                                                                    <img src="{{asset('img/loader.gif')}}" id="loader-photo" width="20" style="display: none">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row avatar-btns-photo panel-body">
                                                            <div class="col-md-9 text-center">
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                                                </div>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="avatar-view-photo" title="" style="display:none" data-original-title="Change the profile Picture">
                                                                    <img src="{{ asset('img/dummy-profile.png') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row" style="display:flex;flex-wrap:wrap;" id="profile-images">
                        @php
                            if($status == "Free") {
                                $limitBoxPhoto = $public_photo_allowed;
                            } else {
                                $limitBoxPhoto = $profileImagesCount;
                            }
                        @endphp
                        @for ($i = 0; $i < $limitBoxPhoto; $i++)
                            <div class="col-lg-3 col-sm-4 col-xs-6">
                                <div class="thumbnail">
                                    @if($profileImagesCount >= $i+1)
                                        <img id="img-{{$i+1}}" src="{{Storage::disk('s3')->url('vendor_photos/thumb_cropped/'.$profileImages[$i]->image_thumb)}}" class="img-responsive" style="width:100%;">
                                        <div class="panel_body">
                                            @if($profileImages[$i]->is_profile_image != 'Y'  && $profileImages[$i]->is_approved == 'Y')
                                            <div class="dropdown pull-left">
                                                <a class="dropdown-toggle" href="javascript:void(0);" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="{{ route('set-vendor-profile-image', ['id' => $profileImages[$i]->id ])}}">set as profile photo</a></li>
                                                </ul>
                                            </div>
                                            @endif
                                            <p class="text-right"><a href="javascript:void(0);" class="remove-image delete" data-img-id="{{$profileImages[$i]->id}}" data-img-div="img-{{$i+1}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></p>
                                            @if($profileImages[$i]->is_approved == 'N')
                                                <p class="var"><small>Awaiting Approval</small></p>
                                            @elseif($profileImages[$i]->is_approved == 'D')
                                                @if($profileImages[$i]->disapprove_reason != NULL && !empty($profileImages[$i]->disapprove_reason))
                                                <p class="var" style="color: red; text-align: left;"><small><strong>Reason of disapproval:</strong> {{$profileImages[$i]->disapprove_reason}}</small></p>
                                                @endif
                                            @endif
                                        </div>
                                    @else
                                        <img id="img-{{$i+1}}" src="{{ asset('img/dummy-profile.png') }}" class="img-responsive" style="width:100%;">
                                    @endif
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script src="{{asset('dist/js/cropper.min.js')}}"></script>
    <script src="{{asset('dist/js/main-cover.js')}}"></script>
    <script src="{{asset('dist/js/main-photo.js')}}"></script>
    <script>
        $(".pimage").change(function(e) {
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                var file = e.originalEvent.srcElement.files[i];
                var img = document.createElement("img");
                var reader = new FileReader();
                reader.onloadend = function() {
                    img.src = reader.result;
                }
                reader.readAsDataURL(file);
                $("#aimage").after(img);
            }
        });
        /* For banner upload */
        $("#bannerVendorUploadForm").on('submit', function(e){
            e.preventDefault();
            var image = $("#avatarInput")[0].files[0];
            if (image === undefined){
                var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
                $(".avatar-upload").after(msg);
                return false;
            } else {
                $("#loader-banner").show();
                $("button.avatar-save").attr('disabled','disabled');
                var _token = $('input[name="_token"]').val();
                var thumb_data = $('input[name="avatar_data"]').val();
                var form = new FormData();
                form.append('image', image);
                form.append('_token', _token);
                form.append('thumb_data', thumb_data);
                $.ajax({
                    url: "{{route('ajax-upload-vendor-cover')}}",
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(data) {
                        $("#loader-banner").hide();
                        var response = data[0];
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                        if(response.status === 0){
                            $(".avatar-upload").after(msg);
                            $("button.avatar-save").removeAttr('disabled');
                        } else {
                            $("#avatar-modal").modal('hide');
                            $(".cover .img-responsive").attr('src', response.thumb_path);
                            if($("#reason-disapproval-sec").length > 0) {
                                $("#reason-disapproval-sec").remove();
                            }
                            if($("#awaiting-appval-sec").length == 0) {
                                $(".cover .img-responsive").after('<br /><span id="awaiting-appval-sec" style="color:green;text-align: center;position: absolute;">Awaiting Approval</span>');
                            }
                        }
                    },
                    error: function(xhr, status, error){
                        $("#loader-banner").hide();
                        var errors = xhr.responseJSON;
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled');
                    }
                });
            }
        });
        $('#avatar-modal').on('hidden.bs.modal', function () {
            $("#loader-banner").hide();
            $(".avatar-body .alert").remove();
            $("#bannerVendorUploadForm")[0].reset();
            $("button.avatar-save").removeAttr('disabled');
        });

        /* For photo upload */
        $("#photoVendorUploadForm").on('submit', function(e){
            e.preventDefault();
            var image = $("#avatarInputPhoto")[0].files[0];
            if (image === undefined){
                var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
                $(".avatar-upload-photo").after(msg);
                return false;
            } else {
                $("#loader-photo").show();
                $("button.avatar-save-photo").attr('disabled','disabled');
                var _token = $('input[name="_token"]').val();
                var thumb_data = $('input[name="avatar_data_photo"]').val();
                var allowed_photos = '<?php echo $public_photo_allowed; ?>';
                var form = new FormData();
                form.append('image', image);
                form.append('_token', _token);
                form.append('thumb_data', thumb_data);
                form.append('allowed_photos', allowed_photos);
                $.ajax({
                    url: "{{route('ajax-upload-vendor-profile')}}",
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(data) {
                        $("#loader-photo").hide();
                        var response = data[0];
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                        if(response.status === 0){
                            $(".avatar-upload-photo").after(msg);
                            $("button.avatar-save-photo").removeAttr('disabled');
                        } else {
                            document.location.reload(true);
                            // $("#avatar-modal-photo").modal('hide');
                            // $("#"+response.img_div).attr('src', response.thumb_path);
                            // $('<div class="panel_body"><div class="dropdown pull-left"><a class="dropdown-toggle" href="javascript:void(0);" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i></a><ul class="dropdown-menu"><li><a href="'+route('set-vendor-profile-image', response.image_id)+'">set as profile photo</a></li></ul></div><p class="text-right"><a href="javascript:void(0);" class="remove-image delete" data-img-id="'+response.image_id+'" data-img-div="'+response.img_div+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></p><p class="var"><small>Awaiting Approval</small></p></div>').insertAfter("#"+response.img_div);
                        }
                    },
                    error: function(xhr, status, error){
                        $("#loader-photo").hide();
                        var errors = xhr.responseJSON;
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled');
                    }
                });
            }
        });
        $('#avatar-modal-photo').on('hidden.bs.modal', function () {
            $("#loader-photo").hide();
            $(".avatar-body-photo .alert").remove();
            $("#photoVendorUploadForm")[0].reset();
            $("button.avatar-save-photo").removeAttr('disabled');
        });
        // delete profile image
        $(".remove-image").on('click', function(e){
            e.preventDefault();
            var img_id = $(this).data("imgId");
            var img_div = $(this).data("imgDiv");
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('ajax-remove-vendor-profile-image') }}",
				method: "POST",
				data:{id:img_id, div:img_div, _token:_token},
				success:function(result) {
					$("#"+img_div).attr('src','{{ asset('img/dummy-profile.png') }}');
                    document.location.reload(true);
				},
                error: function(xhr, status, error){
                    var errors = xhr.responseJSON;
                    console.log(xhr.responseJSON);
                }
            });
        });

    </script>
@endsection