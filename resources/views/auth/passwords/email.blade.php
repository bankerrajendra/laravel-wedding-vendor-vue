@extends('layouts.app-login')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection

@section('navclasses')
    navbar-fixed-top outer-nav scrolled
@endsection
@section('content')
    <!--login-->
    <div class="login_p">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 text-center">
                    <div class="tabbable-line">
                        <a href=""><img src="{{ asset('img/logo-password.png') }}" style="width:60%;"></a>
                        <hr>
                        <div class="tab-content">
                            <!--User Login-->
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <h4>Forgot Password</h4>
                            <form method="POST" action="{{ route('password.email') }}" novalidate>
                                @csrf
                                <div class="form-group login">
                                    <input type="email" name="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Email">
                                    <span class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" style="color:red">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <br>
                                <button type="submit" class="btn btn-default btn-block">SEND</button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <h5>Are you a User? <a href="{{ route('user-signup') }}"> Sign Up Now!</a></h5>
                                <h5> Are you a Vendor? <a href="{{ route('vendor-signup') }}"> Sign Up Now!</a></h5>
                            </div>
                            <!--End User Login-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Login-->
@endsection
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection


@role('admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@endrole