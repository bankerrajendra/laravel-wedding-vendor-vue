@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('dist/css/cropper.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/main.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')

        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="register">
                        <div class="panel">
                            <h3>Upload Your Photo</h3>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#menu1">Public Photo</a></li>
                            <li><a data-toggle="tab" href="#menu2">Private Photo</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="menu1" class="tab-pane fade in active">
                                <br>
                                <div id="successMessage"></div>
                                <div class="crop-avatar text-center">
                                    <div class="photo-panel">
                                        <h4>Upload Photos</h4>
                                        <div class="avatar-view" data-original-title="" title="" style="border:none !important;box-shadow:none;border-radius:0px">
                                            <span id="fileselector">
                                                <label class="btn btn-success">
                                                    <i class="fa fa-upload" aria-hidden="true"></i> Select Photo
                                                </label>
                                            </span>
                                        </div>
                                        <!-- Cropping modal -->
                                        <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
                                            <div class="modal-dialog modal-lg">

                                                <div class="modal-content">
                                                    <form class="avatar-form" id="imageUploadForm" action="{{route('ajax-upload-image')}}" enctype="multipart/form-data" method="post">

                                                        <input type="hidden" value="93e1b98e246c6ed64a9daf4336bed28e" name="codeprofile">

                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" id="resetPhoto" value="reset">×</button>
                                                            <h4 class="modal-title" id="avatar-modal-label">Upload Profile Photo</h4>
                                                        </div>

                                                        <div class="modal-body">
                                                            <div class="avatar-body">

                                                                <!-- Upload image and data -->
                                                                <div class="avatar-upload">
                                                                    <input type="hidden" class="avatar-src" name="avatar_src">
                                                                    <input type="hidden" class="avatar-data" name="avatar_data">
                                                                    <input type="hidden"  name="photo_type" value="public">
                                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                                                                    <label for="avatarInput">Select Photo</label>
                                                                    <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                                                </div>

                                                                <!-- Crop and preview -->
                                                                <div class="row">
                                                                    <div class="col-md-9">
                                                                        <div class="avatar-wrapper"></div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        @if(Auth::user()->gender == 'M')
                                                                            <div class="avatar-preview preview-lg"><img src="{{asset('img/male-default.png')}}" alt="Profile Picture" id="profileimage"  ></div>
                                                                        @else
                                                                            <div class="avatar-preview preview-lg"><img src="{{asset('img/female-default.png')}}" alt="Profile Picture" id="profileimage"></div>
                                                                        @endif
                                                                        <button type="submit" class="btn btn-primary btn-block avatar-save">Add Photo</button>
                                                                        <div class="text-center">
                                                                            <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row avatar-btns panel-body">
                                                                    <div class="col-md-9 text-center">
                                                                        <div class="btn-group">
                                                                            <button type="button" class="btn btn-success" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                                                        </div>
                                                                        <div class="btn-group">
                                                                            <button type="button" class="btn btn-success" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="avatar-view" title="Change the profile Picture" style="display:none">
                                                                            @if(Auth::user()->gender == 'M')
                                                                                <div class="avatar-preview preview-lg"><img src="{{asset('img/male-default.png')}}" alt="Profile Picture" id="profileimage"  ></div>
                                                                            @else
                                                                                <div class="avatar-preview preview-lg"><img src="{{asset('img/female-default.png')}}" alt="Profile Picture" id="profileimage"></div>
                                                                            @endif

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p>All photos are screened as per photo guidelines and gets activated within 24 to 48 hours.</p>
                                <div class="row">
                                    <div class="col-sm-offset-3 col-sm-6 panel-body">
                                        <div class="upload_photo">
                                            @if (count($existingImages) > 0)
                                                <img src="{{config('constants.S3_WEDDING_IMAGES').@$existingImages[0]->image_thumb}}" class="img-thumbnail">
                                                <div class="dropdown pull-right">
                                                    <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{route('ajax-delete-image',['id'=>@$existingImages[0]->id, 'activesection'=>'public'])}}" class="deleteButton" >Delete</a></li>
                                                    </ul>
                                                </div>
                                            @else
                                                @if(Auth::user()->gender == 'M')
                                                    <div class="avatar-preview preview-lg "><img src="{{asset('img/male-default.png')}}" alt="Profile Picture" id="profileimage"  ></div>
                                                @else
                                                    <div class="avatar-preview preview-lg "><img src="{{asset('img/female-default.png')}}" alt="Profile Picture" id="profileimage"></div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                    <span id="aimage"> </span>

                                </div>

                                <p class="text-justify"><b>Note:</b> Each photo must not be less than 440px 440px and you should be in the photo. The uploaded, images can take up to 48 hours to be reviewed and fully visible on your profile.</p>
                                <p class="text-justify"><b>Note:</b> Your face MUST be clearly visible in your MAIN IMAGE. All images MUST contain you.</p>
                                <p class="text-justify"><b>Note:</b> Any Images that doesn’t feature yourself will be removed.</p>
                                <div class="form-group">
                                    <input type="button" class="form-control btn btn-success" onclick="document.location='{{route('free-membership')}}'" value="COMPLETE PROFILE">
                                </div>
                                <a href="#" onclick="document.location='{{route('free-membership')}}'" class="btn btn-default text-center">Skip</a>
                            </div>

                            <div id="menu2" class="tab-pane fade">
                                <br>
                                <p class="text-justify">Muslim Wedding helps you share private photos with specific people of your choice. which means you can restrict who can see it.</p>
                                <p class="text-justify">You can make photos private by uploading them into "Private Photos.</p>
                                <div class="form-group">
                                    <input type="button" class="form-control btn btn-success" onclick="document.location='{{route('free-membership')}}'" value="COMPLETE PROFILE">
                                </div>
                                <a href="#" onclick="document.location='{{route('free-membership')}}'"  class="btn btn-default text-center">Skip</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
@section('footer_scripts')
    <script src="{{asset('dist/js/cropper.min.js')}}"></script>
    <script src="{{asset('dist/js/main.js')}}"></script>
    <script>
        $(".pimage").change(function(e) {
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                var file = e.originalEvent.srcElement.files[i];
                var img = document.createElement("img");
                var reader = new FileReader();
                reader.onloadend = function() {
                    img.src = reader.result;
                }
                reader.readAsDataURL(file);
                $("#aimage").after(img);
            }
        });

                @if(Auth::user()->gender == 'M')
        var default_image_path = "{{asset('img/male-default.png')}}";
                @else
        var default_image_path = "{{asset('img/female-default.png')}}";
        @endif

        $("#imageUploadForm").on('submit', function(e){
            e.preventDefault();
            var image = $("#avatarInput")[0].files[0];
            if (image === undefined){
                var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
                $(".avatar-upload").after(msg);
                return false;
            }else{
                $("#loader").show();
                $("button.avatar-save").attr('disabled','disabled');
                var _token = $('input[name="_token"]').val();
                var thumb_data = $('input[name="avatar_data"]').val();
                var form = new FormData();
                form.append('image', image);
                form.append('_token', _token);
                form.append('thumb_data', thumb_data);
                form.append('photo_type','public');
                form.append('set_as_profile','Y');
                $.ajax({
                    url: '{{route('ajax-upload-image')}}',
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(data) {
                        location.reload();
                        $("#loader").hide();
                        var response = data[0];
                        console.log(response);
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                        if(response.status === 0){
                            $(".avatar-upload").after(msg);
                            $("button.avatar-save").removeAttr('disabled');
                        } else {
                            $("#avatar-modal").modal('hide');
                            $(".default-image:first .img-thumbnail").attr('src','{{config('constants.upload_url_public')}}'+response.thumb_path);
                            $(".default-image:first .dropdown").show();
                            $(".default-image:first").attr('id', response.image_id);
                          // $(".default-image:first .dropdown .profilePicButton").attr('onClick', 'setProfileImage('+response.image_id+')');
                          // $(".default-image:first .dropdown .deleteButton").attr('onClick', 'deleteImage('+response.image_id+')');
                            $(".default-image:first").removeClass('default-image');
                        }
                    },
                    error: function(data){
                        $("#loader").hide();
                        var errors = data.responseJSON;
                        console.log(errors);
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[1]+'</div>';
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled');
                    }


                });
            }
        });
        $('#avatar-modal').on('hidden.bs.modal', function () {
            $("#loader").hide();
            $(".avatar-body .alert").remove();
            $("#imageUploadForm")[0].reset();
            $("button.avatar-save").removeAttr('disabled');
        });

    </script>
@endsection