@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    <div class="list-group mobile-sidebar">
                        <li class="list-group-item text-center">BUSINESS INFORMATION</li>
                        <a href="{{ route('vendor-business-info') }}" class="list-group-item">About Your Business <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('vendor-faqs') }}" class="list-group-item">Faqs <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('vendor-photo-upload') }}" class="list-group-item">Photo Upload <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('vendor-video') }}" class="list-group-item">Video <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('vendor-showcase') }}" class="list-group-item">Showcase <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('vendor-collect-reviews') }}" class="list-group-item">Collect Review <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('vendor-collect-reviews') }}?type=event" class="list-group-item">Event Collect Review <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('vendor-reviews') }}" class="list-group-item">Reviews <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        {{-- <a href="#" class="list-group-item">Manage Membership <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a> --}}
                        {{-- Show Vendors Events --}}
                        <a href="{{route('add-event')}}" class="list-group-item {{ Request::is('add-event') ? 'active' : '' }}"><i class="fa fa-plus" aria-hidden="true"></i> Post New Event</a>
                        <a href="javascript:void(0);" class="list-group-item {{ Request::is('event/edit/*') ? 'active' : '' }}">Events <i class="fa fa-angle-double-down"></i></a>
                        @if($events->count() > 0)
                            @foreach ($events as $event)
                                <a href="{{$event->getEditLink()}}" class="list-group-item"><i class="fa fa-angle-right"></i> {{$event->event_name}}</a>
                                <a class="list-group-item" style="padding:0px 0px 0px 30px" href="{{route('show-events-reviews', [$event->getEncryptedId()])}}"><small>View Reviews</small></a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
