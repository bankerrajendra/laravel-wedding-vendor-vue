@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            <div class="row step2 step_2">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-lg-6 col-sm-12">
                    {{--<h3>Step 2 of 3</h3>--}}
                    <form method="POST" action="{{ route('post-pending-information') }}" id="pendingInformationForm">
                        @csrf
                        <br>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                {{--<i class="fa fa-graduation-cap" aria-hidden="true"></i> --}}
                                Additional Information</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-12">
                                    <label for="gender">{{ __('Gender') }}</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <input type='radio' id="gender" name="gender" value="M" {{ old('gender')!='M' ? 'checked' : '' }}> Male
                                        <input type='radio' id="gender" name="gender" value="F" {{ old('gender')=='F' ? 'checked' : '' }}> Female
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="country">Email Address:</label>
                                    <input type="text" class="form-control" id="email" name="email" value="{{$email}}">
                                </div>
                                <div class="form-group">
                                    <label for="country">Country:</label>
                                    <select class="form-control dynamic" data-dependent="city" id="country" name="country">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="city">City:</label>
                                    <select class="form-control" id="city" name="city">
                                        <option value="">Select City</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="ethnicity">Ethnicity:</label>
                                    <select name="ethnicity" id="ethnicity" class="form-control" required>
                                        <option value="">Select Ethnicity</option>
                                        <option value="Indian">Indian</option>
                                        <option value="Middle Eastern">Middle Eastern</option>
                                        <option value="Asian">Asian</option>
                                        <option value="Mixed Race">Mixed Race</option>
                                        <option value="Other Ethnicity">Other Ethnicity</option>
                                    </select>
                                </div>

                                <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }} row">
                                    <div class="col-sm-12">
                                    <label for="day">Date of birth</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                                <select id="day" class="dob form-control" name="day">
                                                    <option value="">Day</option>
                                                    @for ($day = 1; $day <= 31; $day++)
                                                        <option value="{{ $day }}">{{ $day }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                                <select id="month" class="dob form-control" name="month">
                                                    <option value="">Month</option>
                                                    @for ($month = 1; $month <= 12; $month++)
                                                        <option value="{{ $month }}">{{ $month }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                                <select id="year" class="dob form-control" name="year">
                                                    <option value="">Year</option>
                                                    @for ($year =  (now()->year - 70); $year <= (now()->year - 18); $year++)
                                                        <option value="{{ $year }}">{{ $year }}</option>
                                                    @endfor
                                                </select>
                                            </div>

                                        </div>
                                </div>


                                        @if ($errors->has('dob'))
                                            <span class="help-block">
                                         <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                        <hr>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <div class="col-lg-4 col-sm-12 pull-right">
                    <br>
                    <div class="well well-sm panel-body">
                        <h4>Why Join Punjabigirl.com?</h4>
                        <br>
                        <div class="panel list-box">
                            <div class="circle"><i class="fa fa-comments-o" aria-hidden="true"></i></div> <span class="listed">Message With Members</span>
                        </div>
                        <div class="panel list-box">
                            <div class="circle1"><i class="fa fa-handshake-o" aria-hidden="true"></i></div> <span class="listed1">Partner Preference Matchmaking</span>
                        </div>
                        <div class="panel list-box">
                            <div class="circle2"><i class="fa fa-check-circle-o" aria-hidden="true"></i></div> <span class="listed2">Verified Profiles</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer_scripts')
    <script>
        $(document).ready(function(){

            $('.dynamic').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    var output;
                    $('#'+dependent).html('');
                    $("#phone-country-code option[selected='selected']").removeAttr('selected');
                    $("#phone-country-code option[id="+value+"]").attr("selected","selected");
                    $.ajax({
                        url:"{{ route('ajax.fetchLocation') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {
                            for(var i=0; i<result.length; i++){
                                output += "<option value="+result[i].id+">"+result[i].value+"</option>";
                            }
                            $('#'+dependent).append(output);
                        }

                    })
                }
            });
        });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection