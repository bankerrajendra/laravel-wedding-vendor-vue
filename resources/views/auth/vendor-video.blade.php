@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                @if(!(Auth::user()->is_active()))
                    <div class="text-center">
                        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                    </div>
                @endif
                <div class="col-sm-4 hidden-xs">
                    @include('partials.vendor-profile-side')
                </div>
                <div class="col-sm-8">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="post" action="{{ route('handle-vendor-video-submit') }}" name="submit-vendor-video">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    UPLOAD VIDEOS
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">	
                            <div class="col-sm-12 panel-body">
                                <p>Paid users can upload unlimited videos or audio clips and embed them.
                                    Including a video is a great way to promote your business. One of your videos will randomly appear on our home page featuring videos. Your videos will get views, and those views will lead to sales for your business.</p>
                            </div>
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">ADD NEW VIDEO</button>
                        </div>
                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                                        <br>
                                        <h4 class="text-center">ADD NEW VIDEO</h4>
                                        <hr>
                                        <div class="form-group">
                                            <label for="video_title">Video Title</label>
                                            <input type="text" name="video_title" id="video_title" class="form-control" placeholder="Video Title">
                                        </div>
                                        <div class="form-group">
                                            <label for="embed_code">Video URL:</label>
                                            <input type="text" name="embed_code" id="embed_code" class="form-control" placeholder="Enter youtube video url.">
                                            <small>Enter like: https://www.youtube.com/watch?v=VIDEOCODE OR https://youtu.be/VIDEOCODE</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Description:</label>
                                            <textarea class="form-control" rows="5" id="description" name="description" placeholder="Add description not more than 500 characters."></textarea>
                                        </div>
                                        <br>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-default" style="background:#e48aaa!important;border-color:#e48aaa!important;">ADD</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Your Videos
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if($videosCount > 0)
                            @foreach ($videos as $video)
                                <div class="col-sm-6 col-xs-6 panel-body">
                                    <div class="thum">
                                        <div class="embed-video" data-source="youtube" data-video-url="{{$video->embed_code}}" data-cc-policy="1"></div>
                                        <p class="text-right"><a href="{{ route('delete-vendor-video', ['id' => $video->id]) }}" class="delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></p>
                                    </div>
                                    @if($video->is_approved == 'N')
                                        <span style='color:green;text-align: center;position: absolute;'>Awaiting Approval</span>
                                    @elseif($video->is_approved == 'D')
                                        @if($video->disapprove_reason != NULL && !empty($video->disapprove_reason))
                                            <span style='color:red;text-align: center;position: absolute;'><strong>Reason of disapproval:</strong> {{$video->disapprove_reason}}</span>
                                        @endif
                                    @endif
                                </div>
                            @endforeach
                        @else
                            <div class="col-sm-6 col-xs-6 panel-body">
                                No videos found.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script src="{{asset('js/embed.videos.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.embed-video').embedVideo();
});
</script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection