@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">

                <div class="col-sm-4 col-sm-offset-4">
                    <div class="list-group mobile-sidebar">
                        <li class="list-group-item text-center">Setting</li>
                        <a href="{{ route('user-notification') }}" class="list-group-item">Notification <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('shortlisted-vendors') }}" class="list-group-item"> Shortlisted Vendors <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('finalized-vendors') }}" class="list-group-item"> Finalized Vendors <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('user-disable-account') }}" class="list-group-item">Delete/Deactivate Account <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('user-change-password') }}" class="list-group-item">Change Password <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
