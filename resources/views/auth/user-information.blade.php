@extends('layouts.app')

@section('content')	

        <div class="container">
            <div class="row step2">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
		<div class="container">
		<div class="row">
		<div class="col-sm-offset-3 col-sm-6">
			<div class="register">
				<div class="panel">
					<h3>2 of 3</h3>
				</div>
					<form method="POST" action="{{ route('post-additional-information') }}" id="userInformationForm">
                    @csrf
				<div class="form-group">
					<label for="pray">Do You Pray?</label>
					<select name="does_pray" id="does_pray" class="form-control">
						<option value="">Select</option>
						@foreach($do_you_pray as $rId=>$name)
							<option value="{{$rId}}">{{$name}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label>Born or Reverted?</label>
					<select class="form-control" name="is_born" id="is_born">
						<option value="">Select</option>
						@foreach($born_reverted as $rId=>$name)
							<option value="{{$rId}}">{{$name}}</option>
						@endforeach
					</select>
				</div>
                
				<div class="form-group">
					<label>Education</label>
					<select name="education" id="education" class="form-control">
						<option value="">Select</option>
						@foreach($educationOptions as $option)
							<option value="{{$option->id}}" @if(old('education') == $option->id ) selected="selected" @endif>{{$option->education}}</option>
						@endforeach
					</select>
					@if($errors->has('education'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('education') }}</strong>
						</span>
					@endif
				</div>
				
				<div class="form-group">
					<label for="comment">About Me</label>
					<textarea placeholder="Please write atleast 50 characters." class="form-control" style="Height:100px;" name="about" rows="5" id="about"></textarea>
					
					@if($errors->has('about'))
					<span class="invalid-feedback">
							<strong>{{ $errors->first('about') }}</strong>
						</span>
					@endif
				</div>
				
				<div class="form-group">
					<label for="comment">What I Am Looking For</label>
					<textarea placeholder="Please write atleast 50 characters" class="form-control" style="Height:100px;" name="looking_for" id="looking_for"></textarea>
					
					
					@if($errors->has('looking_for'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('looking_for') }}</strong>
						</span>
					@endif
					
				</div>
				
				<div class="form-group">
					<label>Food Preference?</label>
					<br>
					@foreach($food as $rId=>$name)
						<label class="radio-inline"><input required="" type="radio" name="food" id="food" value="{{$rId}}" aria-required="true">{{$name}}</label>
					@endforeach

					<span class="help-block food_err invalid-feedback"  style="font-weight:normal;color:#a94442;display:none">
						The food preference field is required
					</span>
				</div>
				
				<div class="form-group">
					<label>Smoke?</label>
					<br>
					@foreach($smoke as $rId=>$name)
						<label class="radio-inline"><input required="" type="radio" name="smoke" id="smoke" value="{{$rId}}" aria-required="true">{{$name}}</label>
					@endforeach

					<span class="help-block smoke_err invalid-feedback"  style="font-weight:normal;color:#a94442;display:none">
						The smoke field is required
					</span>

				</div>	
				
				<div class="form-group">
					<label>Drink?</label>
					<br>
					@foreach($drink as $rId=>$name)
						<label class="radio-inline"><input required="" type="radio" name="drink" id="drink" value="{{$rId}}" aria-required="true">{{$name}}</label>
					@endforeach

					<span class="help-block drink_err invalid-feedback"  style="font-weight:normal;color:#a94442;display:none">
						The drink field is required
					</span>
				</div>
				
				<div class="form-group">
					<input type="submit" class="form-control btn btn-warning" value="NEXT">

				</div>
					</form>
			</div>
			
		</div>
	</div>
	</div>
	
    </div>
    </div>

	@endsection
	@section('footer_scripts')
    <script src="{{asset('js/multi-select.js')}}"></script>
	
    <script type="text/javascript">
        $('select[multiple]').multiselect({
            columns: 4,
            placeholder: 'Select options'
        });
    </script>

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection