@extends('layouts.app')
@section('template_linked_css')
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                @if(!(Auth::user()->is_active()))
                    <div class="text-center">
                        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                    </div>
                @endif
                <div class="col-sm-4 hidden-xs">
                    @include('partials.vendor-profile-side')
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Reviews
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="jumbotron">
                                <h3>You have received {{ $totalReviews }} reviews</h3>
                                <div id="vendor-reviews-results" class="vendor-name">
                                    @include('pages.user.ajax.show-reviews')
                                </div>
                                
                                @if($totalReviews > $review_page_page)
                                <div class="media">
                                    <div class="media-body text-center">
                                        <a href="javascript:void(0);" id="next-page-review" data-next-page="2" class="btn btn-default">Show More </a>
                                        <img src="{{asset('img/loader.gif')}}" id="spinner-load" width="40" style="display: none;margin-top: 10px;">
                                   </div>
                                </div>
                                @endif								
                            </div>
                            <div class="jumbotron">							
                                <h3>Get reviews from your clients</h3>
                                <p>Reviews are critical when it comes time to choose a vendor. Encourage your past clients to leave a review about their experience with your business.</p>
                                <a href="{{ route('vendor-collect-reviews') }}" class="btn btn-default">Request reviews</a>
                            </div>
                            <div class="rev-rating">
                                @php
                                    $average_rating = $vendorObj->getAverageReviewRating();
                                    $total_review = $vendorObj->getTotalReviewVendor();
                                    if($total_review > 0) {
                                        $percentage_rating = ( $vendorObj->getSumReviewRating() * 100 / ($total_review*5) );
                                    } else {
                                        $percentage_rating = 0;
                                    }
                                @endphp
                                <h3>{{substr($average_rating, 0, -2)}} 
                                    @for($r=1;$r<=5;$r++)
                                    <i class="fa @if($r <= $average_rating) fa-star @elseif($r >= $average_rating-0.5 && $r <= $average_rating+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                                    @endfor 
                                <span>{{$percentage_rating}}% Positive ( {{$total_review}} @if( $total_review > 1) reviews @else review @endif)</span></h3>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            @for($br=1;$br<=5;$br++)
                                <div class="side">
                                    <div>{{$br}} star</div>
                                </div>
                                <div class="middle">
                                    <div class="bar-container">
                                        <div class="bar-{{$vendorObj->getSpcificStartBarPer($br)}}"></div>
                                    </div>
                                </div>
                                <div class="side right">
                                    <div>{{$vendorObj->getCountReviewStarSpecific($br)}}</div>
                                </div>
                            @endfor
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                        <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="rat-box">
                                <h3>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i> 
                                    <i class="fa fa-star-o" aria-hidden="true"></i> 
                                </h3>
                                <p class="text-center">You haven't received any reviews yet!</p>
                                <a href="{{ route('vendor-collect-reviews') }}" class="btn btn-default">Request reviews</a>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
var ajaxResultGetRequest = null
$("#next-page-review").on('click', function() {
    var url = route('get-vendor-reviews-ajax')+'?page='+$(this).data('next-page') + '&vendor_id={{$vendorObj->getEncryptedId()}}';
    
    ajaxResultGetRequest = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            // show loading image
            $('#next-page-review').hide();
            $('#spinner-load').show();
        },
        success: function(result) {
            // show results
            $('#vendor-reviews-results').append(result.reviews);
            $('#next-page-review').data('next-page', result.next_page);
            if(result.next_url == '') {
                $('#next-page-review').hide();
            } else {
                $('#next-page-review').show();
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            // hide loading image
            $('#spinner-load').hide();
            ajaxResultGetRequest = null;
        }
    });
});
</script>
@endsection