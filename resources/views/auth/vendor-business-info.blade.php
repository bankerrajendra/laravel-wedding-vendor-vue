@extends('layouts.app')
@section('template_linked_css')
<link href="{{asset('summernote-0.8.12-dist/summernote.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/summernote-bs3.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                @if(!(Auth::user()->is_active()))
                    <div class="text-center">
                        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                    </div>
                @endif
                <div class="col-sm-4 hidden-xs">
                    @include('partials.vendor-profile-side')
                </div>
                <div class="col-sm-8" id="vendor-information-main-bar">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="alert alert-danger show-business-info-error" style="display: none;"></div>
                    <div class="alert alert-success show-business-info-success" style="display: none;"></div>
                    <form name="vendor-business-frm" id="vendor-business-frm" action="{{ route('handle-post-vendor-business-info') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    Business Information
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" name="email" id="email" class="form-control" value="{{$userObj->email}}" readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="row">	
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="first_name">First Name *</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" value="{{$errors->any()?old('first_name'):($userObj->getFlagPropertyValue('first_name')?$userObj->getFlagPropertyValue('first_name')->property_value:$userObj->first_name)}}">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="last_name">Last Name *</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control" value="{{$errors->any()?old('last_name'):($userObj->getFlagPropertyValue('last_name')?$userObj->getFlagPropertyValue('last_name')->property_value:$userObj->last_name)}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="company_name">Company Name/Title *</label>
                                    <input type="text" name="company_name" id="company_name" class="form-control" value="{{$errors->any()?old('company_name'):($userObj->getFlagPropertyValue('company_name')?$userObj->getFlagPropertyValue('company_name')->property_value:$userObj->vendors_information->company_name)}}">
                                    <small>Note: characters length must not be more than 29</small>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="established_year">Established Year</label>
                                    <input type="text" name="established_year" id="established_year" maxlength="4" size="4" class="form-control" style="width: 60px;" value="{{$errors->any()?old('established_year'):($userObj->getFlagPropertyValue('established_year')?$userObj->getFlagPropertyValue('established_year')->property_value:$userObj->vendors_information->established_year)}}">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="keywords">Enter Keywords *</label>
                                    <textarea class="form-control " name="meta_keywords" id="meta_keywords" rows="5">{{$errors->any()?old('meta_keywords'):($userObj->getFlagPropertyValue('meta_keywords')?$userObj->getFlagPropertyValue('meta_keywords')->property_value:$userObj->vendors_information->meta_keywords)}}</textarea>
                                    <small>Note: content length must not be more than 500</small>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="tEditor">About Us *</label>
                                    @if($errors->any())
                                        <textarea name="business_information" id="tEditor" style="display: none;">{!!old("business_information")!!}</textarea>
                                    @else
                                        @if($userObj->getFlagPropertyValue('business_information'))
                                            <textarea name="business_information" id="tEditor" style="display: none;">{!!$userObj->getFlagPropertyValue("business_information")->property_value!!}</textarea>
                                        @else
                                            <textarea name="business_information" id="tEditor" style="display: none;">{!!$userObj->vendors_information->business_information!!}</textarea>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="tEditor1">Description *</label>
                                    @if($errors->any())
                                        <textarea name="specials" id="tEditor1" style="display: none;">{!!old("specials")!!}</textarea>
                                    @else
                                        @if($userObj->getFlagPropertyValue('specials'))
                                            <textarea name="specials" id="tEditor1" style="display: none;">{!!$userObj->getFlagPropertyValue("specials")->property_value!!}</textarea>
                                        @else
                                            <textarea name="specials" id="tEditor1" style="display: none;">{!!$userObj->vendors_information->specials!!}</textarea>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>	
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    Business location
                                </div>
                            </div>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="address">Address *</label>
                                    <input type="text" name="address" id="address" class="form-control" value="{{$errors->any()?old('address'):($userObj->getFlagPropertyValue('address')?$userObj->getFlagPropertyValue('address')->property_value:$userObj->address)}}">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <p>DISPLAY YOUR ADDRESS </p>
                                <label class="switch">
                                    <input type="checkbox" name="display_address" class="switch-btns" id="togBtn-add">
                                    <div class="slider round">
                                        <!--ADDED HTML-->
                                        <span class="on">ON</span>
                                        <span class="off">OFF</span>
                                        <!--END-->
                                    </div>
                                </label>
                            </div>
                        </div>	
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="country">Select Country *</label>
                                    <select name="country" id="country" class="form-control load-dependent" required="" dependent="state" aria-required="true">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{($errors->any()?old('country'):$userObj->country_id) == $country->id ? "selected":"" }} id="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="state">Select State *</label>
                                    <select name="state" id="state" class="form-control load-dependent" required="" dependent="city" aria-required="true">
                                        <option value="">Select State</option>
                                        @foreach(getStateByCountry($errors->any()?old('country'):$userObj->country_id) as $ct)
                                            <option value="{{ $ct->id }}" {{ ($errors->any()?old('state'):$userObj->state_id) == $ct->id ? "selected":"" }}>{{ $ct->name }}</option>
                                        @endforeach
                                    </select>
                                    <i class='state-dropdown-loader' style="display: none;">loading...</i>
                                </div>
                            </div>
                        </div>	
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="city">City/Town *</label>
                                    <select name="city" id="city" class="form-control" required="" aria-required="true">
                                        <option value="">Select City</option>
                                        @foreach(getCityByState(($errors->any()?old('state'):$userObj->state_id)) as $city)
                                            <option value="{{ $city->id }}" {{($errors->any()?old('city'):$userObj->city_id) == $city->id ? "selected":"" }}>{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                    <i class='city-dropdown-loader' style="display: none;">loading...</i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="zip">Zip/Pin/Area Code *</label>
                                    <input type="text" name="zip" id="zip" class="form-control" value="{{$errors->any()?old('zip'):($userObj->getFlagPropertyValue('zip')?$userObj->getFlagPropertyValue('zip')->property_value:$userObj->zip)}}">
                                </div>
                            </div>
                        </div>	
                        <div class="row">	
                            <div class="col-sm-3">
                                <div class="form-group">
                                        <label for="mobile_country_code">Country Code *</label>
                                        <select name="mobile_country_code" id="mobile_country_code" class="form-control" required="" aria-required="true">
                                            <option value="">Select Country Code</option>
                                            @foreach($countries as $country)
                                                <option {{($errors->any()?old('mobile_country_code'):$userObj->mobile_country_code) == $country->id ? "selected":"" }} value="{{$country->id}}" id="{{$country->id}}">+{{$country->phonecode}}  ({{$country->name}})</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('mobile_country_code'))
                                            <span id="mobile_country_code-error" class="help-block error-help-block">{{ $errors->first('mobile_country_code') }}</span>
                                        @endif
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label for="mobile_number">Mobile Number *</label>
                                    <input type="text" name="mobile_number" id="mobile_number" class="form-control" value="{{$errors->any()?old('mobile_number'):($userObj->getFlagPropertyValue('mobile_number')?$userObj->getFlagPropertyValue('mobile_number')->property_value:$userObj->mobile_number)}}">
                                    @if ($errors->has('mobile_number'))
                                        <span id="mobile_number-error" class="help-block error-help-block">{{ $errors->first('mobile_number') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <p>DISPLAY YOUR BUSINESS NUMBER</p>
                                <label class="switch">
                                    <input type="checkbox" name="display_business_number" class="switch-btns" id="togBtn-num">
                                    <div class="slider round">
                                        <!--ADDED HTML-->
                                        <span class="on">ON</span>
                                        <span class="off">OFF</span>
                                        <!--END-->
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    Website &amp; Social Links
                                </div>
                            </div>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="business_website">Business Website</label>
                                    <input type="text" name="business_website" id="business_website" value="{{$errors->any()?old('business_website'):($userObj->getFlagPropertyValue('business_website')?$userObj->getFlagPropertyValue('business_website')->property_value:$userObj->vendors_information->business_website)}}" class="form-control" placeholder="http://www.example.com">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="facebook">Facebook</label>
                                    <input type="text" name="facebook" id="facebook" class="form-control" value="{{$errors->any()?old('facebook'):($userObj->getFlagPropertyValue('facebook')?$userObj->getFlagPropertyValue('facebook')->property_value:$userObj->vendors_information->facebook)}}" placeholder="ie. facebook.com/xxxxxx">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="instagram">Instagram</label>
                                    <input type="text" name="instagram" id="instagram" class="form-control" value="{{$errors->any()?old('instagram'):($userObj->getFlagPropertyValue('instagram')?$userObj->getFlagPropertyValue('instagram')->property_value:$userObj->vendors_information->instagram)}}" placeholder="ie. instagram.com/xxxxxx">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="pinterest">Pinterest</label>
                                    <input type="text" name="pinterest" id="pinterest" class="form-control" value="{{$errors->any()?old('pinterest'):($userObj->getFlagPropertyValue('pinterest')?$userObj->getFlagPropertyValue('pinterest')->property_value:$userObj->vendors_information->pinterest)}}" placeholder="ie. pinterest.com/xxxxxx">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="twitter">Twitter</label>
                                    <input type="text" name="twitter" id="twitter" class="form-control" value="{{$errors->any()?old('twitter'):($userObj->getFlagPropertyValue('twitter')?$userObj->getFlagPropertyValue('twitter')->property_value:$userObj->vendors_information->twitter)}}" placeholder="ie. twitter.com/xxxxxx">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="linkedin">LinkedIn</label>
                                    <input type="text" name="linkedin" id="linkedin" class="form-control" value="{{$errors->any()?old('linkedin'):($userObj->getFlagPropertyValue('linkedin')?$userObj->getFlagPropertyValue('linkedin')->property_value:$userObj->vendors_information->linkedin)}}" placeholder="ie. linkedin.com/xxxxxx">
                                </div>
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-default pull-right" name="submit-business-info-btn">SAVE</button>
                        <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none;float: right;margin-right: 10px;margin-top: 5px;">
                        <a href="{{ $userObj->getVendorProfileLink() }}" target="_blank" class="btn btn-default pull-right" style="margin-right:15px;">PREVIEW</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

{{-- Un Approved Items --}}
<?php
if(count(getFlaggedUserDataAll(Auth::id())) > 0){ ?>

<input type="hidden" id="FeedbackStat">
<?php } ?>
<!-- Feedback message Modal -->
<div id="FeedbackModal" class="modal fade" role="dialog" style="display:none; ">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Some of the fields are incompatible with site policies.</h4>
            </div>
            <form method="post" id="updateFlagedUserre_entry_form" name="updateFlagedUserre_entry_form" action="{{route('ajax.updateFlagedUserre_entry')}}">
                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                <div class="modal-body">
                    {{ csrf_field() }}
                    @foreach(getFlaggedUserDataAll(Auth::id()) as $feedback)
                        <div class="form-group">
                            <label>
                                <?php echo ucwords(str_replace("_", " ",$feedback->property_name)); ?>
                            </label>
                            @if($feedback->property_name == 'business_information' || $feedback->property_name == 'specials' || $feedback->property_name == 'meta_keywords')
                                <textarea name="{{$feedback->property_name}}" rows="5" style="width:75%;">{{$feedback->property_value}}</textarea>
                            @else
                                <input name="{{$feedback->property_name}}" value="{{$feedback->property_value}}" class="form-control">
                            @endif

                            <p><strong style="color:red">Reason : </strong>{{$feedback->feedback}}</p>

                        </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Un Approved Items --}}
@endsection
@section('footer_scripts')
    <script src="{{asset('summernote-0.8.12-dist/summernote.min.js')}}"></script>
    <script>

    $(document).ready(function(){
        $("#tEditor").summernote({height: 300});
        $("#tEditor1").summernote({height: 300});
        
        $(".switch-btns").change(function(){
            if($(this).prop("checked") == true){
                $(this).val(1)
            }else{
                $(this).val(0)
            }
        });
        @if($userObj->vendors_information->display_address == 1)
            $("#togBtn-add").prop('checked', true);
            $("#togBtn-add").val(1);
        @else
            $("#togBtn-add").prop('checked', false);
            $("#togBtn-add").val(0);
        @endif
        @if($userObj->vendors_information->display_business_number == 1)
            $("#togBtn-num").prop('checked', true);
            $("#togBtn-num").val(1);
        @else
            $("#togBtn-num").prop('checked', false);
            $("#togBtn-num").val(0);
        @endif
        $("#vendor-business-frm").on('submit',function(e){
            e.preventDefault();
            $(".show-business-info-error, .show-business-info-success").hide();
            $(".show-business-info-error").html("");
            $(".show-business-info-success").html("");
            var submit_frm = true;
            var form = $("#vendor-business-frm");
            var url = form.attr('action');
            var data = new FormData(form[0]);
            if(submit_frm == true) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    beforeSend: function () {
                        // show loading image and disable the submit button
                        $("button[name='submit-business-info-btn']").attr('disabled','disabled');
                        $("#loader").show();
                    },
                    success: function(result) {
                        // show success message
                        if(result.status == 0) {
                            $(".show-business-info-error").html(result.message);
                            $(".show-business-info-error").show();
                        } else {
                            $(".show-business-info-success").html(result.message);
                            $(".show-business-info-success").show();
                        }
                    },
                    error: function(xhr, status, error){
                        var errors = xhr.responseJSON;
                        //console.log(errors);
                        var err_html = 'Errors<br />';
                        for (var key in errors) {
                            if (errors.hasOwnProperty(key)) {
                                var val = errors[key];
                                //console.log(val);
                                err_html += ' - '+val[0]+'<br />';
                            }
                        }
                        $(".show-business-info-error").html(err_html);
                        $(".show-business-info-error").show();
                    },
                    complete: function () {
                        // hide loading image and enable the submit button
                        $("#loader").hide();
                        $("button[name='submit-business-info-btn']").removeAttr('disabled');
                        scroll_to_div('vendor-information-main-bar');
                    }
                });
                return false;
            } else {
                scroll_to_div('vendor-information-main-bar');
                return false;
            }
        });
    });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection