@extends('layouts.app')
@section('template_linked_css')
<link href="{{asset('css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/timepicki.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('summernote-0.8.12-dist/summernote.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/summernote-bs3.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/multi-select.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dist/css/cropper.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dist/css/main.css')}}" rel="stylesheet" type="text/css">
<style type="text/css" media="screen">
.avatar-btns, .avatar-btns-poster { text-align: center; }
</style>
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                @if(!(Auth::user()->is_active()))
                    <div class="text-center">
                        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                    </div>
                @endif
                <div class="col-sm-4 hidden-xs">
                    @include('partials.vendor-profile-side')
                </div>
                <div class="col-sm-8" id="vendor-event-information-main-bar">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="alert alert-danger show-event-info-error" style="display: none;"></div>
                    <div class="alert alert-success show-event-info-success" style="display: none;"></div>
                    <form method="POST" action="{{ route('handle-event-add') }}" id="eventAddFrm" name="eventAddFrm">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    Events Information
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="text">Event Name *</label>
                                    <input type="text" name="event_name" id="event_name" value="{{$errors->any()?old('event_name'):''}}" class="form-control">
                                    @if ($errors->has('event_name'))
                                        <span id="event_name-error" class="help-block error-help-block">{{ $errors->first('event_name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="text">Address *</label>
                                    <input type="text" id="address" name="address" value="{{$errors->any()?old('address'):''}}" class="form-control">
                                    @if ($errors->has('address'))
                                        <span id="address-error" class="help-block error-help-block">{{ $errors->first('address') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="text">Venue *</label>
                                    <input type="text" id="venue" name="venue" value="{{$errors->any()?old('venue'):''}}" class="form-control">
                                    @if ($errors->has('venue'))
                                        <span id="venue-error" class="help-block error-help-block">{{ $errors->first('venue') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="text">Artists</label>&nbsp;&nbsp;<button class="add_artist_form_field"> 
                                        <span style="font-size:12px; font-weight: bold;">ADD + </span>
                                      </button>
                                    <div class="artist-container1">
                                        <div class="artist-div">
                                            <div style="float: left; width: 78%;position: relative;"><input class="form-control artist-text" style="margin-bottom: 10px;" type="text" name="artists[]" data-element-id="1" list="artist-select-1">
                                                <datalist id="artist-select-1">
                                                    @if($artists->count() > 0)
                                                        <select>
                                                            @foreach($artists as $artist)
                                                            <option value="{{$artist->name}}"></option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </datalist>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="country">Select Country *</label>
                                    <select name="country" id="country" class="form-control load-dependent" required="" aria-required="true" dependent="state">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{($errors->any()?old('country'):'') == $country->id ? "selected":"" }} id="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country'))
                                        <span id="country-error" class="help-block error-help-block">{{ $errors->first('country') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="state">Select State *</label>
                                    <select name="state" id="state" class="form-control load-dependent" dependent="city">
                                        <option value="">Select State</option>
                                        @foreach(getStateByCountry($errors->any()?old('country'):'') as $ct)
                                            <option value="{{ $ct->id }}" {{ ($errors->any()?old('state'):'') == $ct->id ? "selected":"" }}>{{ $ct->name }}</option>
                                        @endforeach
                                    </select>
                                    <i class='state-dropdown-loader' style="display: none;">loading...</i>
                                    @if ($errors->has('state'))
                                        <span id="state-error" class="help-block error-help-block">{{ $errors->first('state') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="city">City/Town *</label>
                                    <select name="city" id="city" class="form-control">
                                        <option value="">Select City</option>
                                        @foreach(getCityByState(($errors->any()?old('state'):'')) as $city)
                                            <option value="{{ $city->id }}" {{($errors->any()?old('city'):'') == $city->id ? "selected":"" }}>{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                    <i class='city-dropdown-loader' style="display: none;">loading...</i>
                                    @if ($errors->has('city'))
                                        <span id="city-error" class="help-block error-help-block">{{ $errors->first('city') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="zip">Zip/Pin/Area Code *</label>
                                    <input type="text" name="zip" id="zip" class="form-control" value="{{$errors->any()?old('zip'):''}}">
                                    @if ($errors->has('zip'))
                                        <span id="zip-error" class="help-block error-help-block">{{ $errors->first('zip') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="description">Description *</label>
                                    <textarea name="description" id="description" cols="30" rows="10" style="display: none;">{{$errors->any()?old('description'):''}}</textarea>
                                    @if ($errors->has('description'))
                                        <span id="description-error" class="help-block error-help-block">{{ $errors->first('description') }}</span>
                                    @endif
                                    <div class="form-group has-error" id="description-error" style="display: none;">
                                        <span style="color:#a94442;">This field cann't be empty</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="ticket_information">Ticket Information </label>
                                    <textarea name="ticket_information" id="ticket_information" cols="30" rows="10" style="display: none;">{{$errors->any()?old('ticket_information'):''}}</textarea>
                                    @if ($errors->has('ticket_information'))
                                        <span id="ticket_information-error" class="help-block error-help-block">{{ $errors->first('ticket_information') }}</span>
                                    @endif
                                    <div class="form-group has-error" id="ticket_information-error" style="display: none;">
                                        <span style="color:#a94442;">This field cann't be empty</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="categories">Category *</label>
                                    <div class="ne-search-option">
                                        <select data-placeholder="Select" class="chosen-select form-control" multiple tabindex="5" name="categories[]" id="categories">
                                            @if($events_categories != null)
                                                @foreach($events_categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('categories'))
                                            <span id="categories-error" class="help-block error-help-block">{{ $errors->first('categories') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="start-date">Start Date *</label>
                                    <div id="filterDate">
                                    <!-- Datepicker as text field -->         
                                        <div class="input-group date" data-date-format="dd.mm.yyyy">
                                            <input name="start_date" id="start-date" type="text" class="form-control" placeholder="dd.mm.yyyy" value="{{$errors->any()?old('start_date'):''}}" autocomplete="off">
                                            <div class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                            </div>
                                            @if ($errors->has('start_date'))
                                                <span id="start_date-error" class="help-block error-help-block">{{ $errors->first('start_date') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="end-date">End Date *</label>
                                    <div id="filterDate1">
                                    <!-- Datepicker as text field -->         
                                        <div class="input-group date" data-date-format="dd.mm.yyyy">
                                            <input name="end_date" id="end-date" type="text" class="form-control" placeholder="dd.mm.yyyy" value="{{$errors->any()?old('end_date'):''}}" autocomplete="off">
                                            <div class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                            </div>
                                            @if ($errors->has('end_date'))
                                                <span id="end_date-error" class="help-block error-help-block">{{ $errors->first('end_date') }}</span>
                                            @endif
                                            
                                        </div>
                                    </div>
                                    <div class="form-group has-error" id="start_end_date-error" style="display: none;">
                                        <span style="color:#a94442;">End date should be eqaul to OR grater than start date</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="start_time_picker">Start Time *</label>
                                    <div id="filterDate2"> 
                                        <div class="">
                                            <input type="text" class="form-control" id="start_time_picker" type="text" name="start_time" value="{{$errors->any()?old('start_time'):''}}" placeholder="hh:mm am">
                                            @if ($errors->has('start_time'))
                                                <span id="start_time-error" class="help-block error-help-block">{{ $errors->first('start_time') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="end_time_picker">End Time </label>
                                    <div id="filterDate3">
                                        <div class="">
                                            <input type="text" class="form-control" id='end_time_picker' type="text" name="end_time" value="{{$errors->any()?old('end_time'):''}}" placeholder="hh:mm am">
                                            @if ($errors->has('end_time'))
                                                <span id="end_time-error" class="help-block error-help-block">{{ $errors->first('end_time') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="crop-avatar-poster">
                                    <div class="poster-panel">
                                        <div class="poster-view" data-original-title="" title="" style="width:100%!important;margin: 0% auto 3%;">
                                            <label for="">Poster Image</label>
                                            <span id="fileselector-cover">
                                                <img src="{{ asset('img/banner-img1.png') }}" class="img-responsive">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="ticket_url">Ticket URL</label>
                                    <input type="text" name="ticket_url" id="ticket_url" class="form-control" value="{{$errors->any()?old('ticket_url'):''}}">
                                    @if ($errors->has('ticket_url'))
                                        <span id="ticket_url-error" class="help-block error-help-block">{{ $errors->first('ticket_url') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>	
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="meta_keywords">Keywords</label>
                                    <input type="text" name="meta_keywords" id="meta_keywords" class="form-control" value="{{$errors->any()?old('meta_keywords'):''}}">
                                    @if ($errors->has('meta_keywords'))
                                        <span id="meta_keywords-error" class="help-block error-help-block">{{ $errors->first('meta_keywords') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>	
                        
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="crop-avatar">
                                    <div class="photo-panel">
                                        <div class="avatar-view" data-original-title="" title="" style="width:100%!important;margin: 0% auto 3%;">
                                            <label for="">Cover Photo</label>
                                            <span id="fileselector-cover">
                                                <img src="{{ asset('img/banner-img1.png') }}" class="img-responsive">
                                            </span>											
                                            <small>The ideal dimension is 1320*350 pixels.</small>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="crop-avatar-another">
                                    <div class="photo-panel">
                                        <div class="avatar-view-photo" data-original-title="" title=""  style="width:100%!important;margin: 0% auto 3%;">
                                            <label for="">Logo</label>
                                            <span id="fileselector">
                                                <img src="{{ asset('img/dummy.png') }}" class="img-responsive">
                                            </span>	
                                            <small>Dimension is 500*500 pixels.</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="video_url">Video URL</label>
                                    <input type="text" name="video_url" id="video_url" class="form-control" value="{{$errors->any()?old('video_url'):''}}">
                                    @if ($errors->has('video_url'))
                                        <span id="video_url-error" class="help-block error-help-block">{{ $errors->first('video_url') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <p>Online Seat Selection</p>
                                <label class="switch">
                                    <input name="online_seat_selection" type="checkbox" class="switch-btns" id="togBtnOnlieSeat">
                                    <div class="slider round">
                                        <!--ADDED HTML-->
                                        <span class="on">ON</span>
                                        <span class="off">OFF</span>
                                        <!--END-->
                                    </div>
                                </label>
                            </div>
                            
                            <div class="col-sm-4">
                                <p>Parking</p>
                                <label class="switch">
                                    <input name="parking" type="checkbox" class="switch-btns" id="togBtnParking">
                                    <div class="slider round">
                                        <!--ADDED HTML-->
                                        <span class="on">ON</span>
                                        <span class="off">OFF</span>
                                        <!--END-->
                                    </div>
                                </label>
                            </div>
                            
                            <div class="col-sm-4">
                                <p>Food For Sale</p>
                                <label class="switch">
                                    <input name="food_for_sale" type="checkbox" class="switch-btns" id="togBtnFoodSale">
                                    <div class="slider round">
                                        <!--ADDED HTML-->
                                        <span class="on">ON</span>
                                        <span class="off">OFF</span>
                                        <!--END-->
                                    </div>
                                </label>
                            </div>
                            
                            <div class="col-sm-4">
                                <p>Drinks For Sale</p>
                                <label class="switch">
                                    <input name="drinks_for_sale" type="checkbox" class="switch-btns" id="togBtnDrinkSale">
                                    <div class="slider round">
                                        <!--ADDED HTML-->
                                        <span class="on">ON</span>
                                        <span class="off">OFF</span>
                                        <!--END-->
                                    </div>
                                </label>
                            </div>
                            
                            <div class="col-sm-4">
                                <p>BabySitting Services</p>
                                <label class="switch">
                                    <input name="babysitting_services" type="checkbox" class="switch-btns" id="togBtnBabySitting">
                                    <div class="slider round">
                                        <!--ADDED HTML-->
                                        <span class="on">ON</span>
                                        <span class="off">OFF</span>
                                        <!--END-->
                                    </div>
                                </label>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    Website &amp; Social Links
                                </div>
                            </div>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="business_website">Business Website</label>
                                    <input type="text" name="business_website" id="business_website" value="{{$errors->any()?old('business_website'):''}}" class="form-control" placeholder="http://www.example.com">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="facebook">Facebook</label>
                                    <input type="text" name="facebook" id="facebook" class="form-control" value="{{$errors->any()?old('facebook'):''}}" placeholder="ie. facebook.com/xxxxxx">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="instagram">Instagram</label>
                                    <input type="text" name="instagram" id="instagram" class="form-control" value="{{$errors->any()?old('instagram'):''}}" placeholder="ie. instagram.com/xxxxxx">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="pinterest">Pinterest</label>
                                    <input type="text" name="pinterest" id="pinterest" class="form-control" value="{{$errors->any()?old('pinterest'):''}}" placeholder="ie. pinterest.com/xxxxxx">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="twitter">Twitter</label>
                                    <input type="text" name="twitter" id="twitter" class="form-control" value="{{$errors->any()?old('twitter'):''}}" placeholder="ie. twitter.com/xxxxxx">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="linkedin">LinkedIn</label>
                                    <input type="text" name="linkedin" id="linkedin" class="form-control" value="{{$errors->any()?old('linkedin'):''}}" placeholder="ie. linkedin.com/xxxxxx">
                                </div>
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-default pull-right" name="submit-event-info-btn">SAVE</button>
                        <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none;float: right;margin-right: 10px;margin-top: 5px;">
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Cropping Model for Event Poster STARTED --}}
    <div class="crop-avatar-poster">
        <div class="modal fade" id="avatar-modal-poster" aria-hidden="true" aria-labelledby="avatar-modal-label-poster" role="dialog" tabindex="-1" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="avatar-form-poster" id="posterEventUploadForm" action="{{ route('ajax-upload-event-add-poster') }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <input type="hidden" value="" name="codeprofile">
                        <input type="hidden" name="allowed_photos" value='1'>
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                            <br>
                            <h4 class="text-center">Upload Poster</h4>
                            <hr>
                            <div class="avatar-body-poster">
                                <div class="avatar-upload-poster">
                                    <input type="hidden" class="avatar-src-poster" name="avatar_src_poster">
                                    <input type="hidden" class="avatar-data-poster" name="avatar_data_poster">
                                    <label for="avatarInputPoster">Select Photo</label>
                                    <input type="file" class="avatar-input-poster" id="avatarInputPoster" name="avatar_file_poster">
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper-poster"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="avatar-preview-poster preview-lg hidden-xs"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        <div class="avatar-preview-poster preview-md remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        <div class="avatar-preview-poster preview-sm remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        {{-- Zoom in out STARTED --}}
                                        <div class="avatar-btns-poster">
                                            <div class="btn-group"><br>
                                                <button type="button" class="btn btn-default fa fa-search-plus" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                <button type="button" class="btn btn-default fa fa-search-minus" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                            </div>
                                        </div>
                                        {{-- Zoom in out ENDED --}}
                                        <br>
                                        <button type="submit" class="btn btn-default btn-block avatar-save-poster">Add Poster</button>
                                        <div class="text-center">
                                            <img src="{{asset('img/loader.gif')}}" id="loader-poster" width="20" style="display: none">
                                        </div>
                                    </div>
                                </div>

                                <div class="row avatar-btns-poster panel-body">
                                    <div class="col-md-9 text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="avatar-view-poster" title="" style="display:none" data-original-title="Change the poster Picture">
                                            <img src="{{ asset('img/banner-img.jpg') }}" alt="Cover Picture" id="coverimage" style="height:auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Cropping Model for Event Poster ENDED --}}
    {{-- Cropping Model for Event Banner STARTED --}}
    <div class="crop-avatar">
        <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="avatar-form" id="bannerVendorUploadForm" action="{{ route('ajax-upload-event-add-cover') }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <input type="hidden" value="" name="codeprofile">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                            <br>
                            <h4 class="text-center">Upload Banner</h4>
                            <hr>
                            <div class="avatar-body">
                                <div class="avatar-upload">
                                    <input type="hidden" class="avatar-src" name="avatar_src">
                                    <input type="hidden" class="avatar-data" name="avatar_data">
                                    <label for="avatarInput">Select Photo</label>
                                    <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="avatar-preview preview-lg hidden-xs"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        <div class="avatar-preview preview-md remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        <div class="avatar-preview preview-sm remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        {{-- Zoom in out STARTED --}}
                                        <div class="avatar-btns">
                                            <div class="btn-group"><br>
                                                <button type="button" class="btn btn-default fa fa-search-plus" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                <button type="button" class="btn btn-default fa fa-search-minus" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                            </div>
                                        </div>
                                        {{-- Zoom in out ENDED --}}
                                        <br>
                                        <button type="submit" class="btn btn-default btn-block avatar-save">Add Banner</button>
                                        <div class="text-center">
                                            <img src="{{asset('img/loader.gif')}}" id="loader-banner" width="20" style="display: none">
                                        </div>
                                    </div>
                                </div>
    
                                <div class="row avatar-btns panel-body">
                                    <div class="col-md-9 text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="avatar-view" title="" style="display:none" data-original-title="Change the profile Picture">
                                            <img src="{{ asset('img/banner-img.jpg') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Cropping Model for Event Banner ENDED --}}
    {{-- Cropping Model for Event Photo STARTED --}}
    <div class="crop-avatar-another">
        <div class="modal fade" id="avatar-modal-photo" aria-hidden="true" aria-labelledby="avatar-modal-photo-label" role="dialog" tabindex="-1" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="avatar-form-photo" id="photoVendorUploadForm" action="{{ route('ajax-upload-event-add-profile') }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <input type="hidden" value="" name="codeprofile">
                        <input type="hidden" name="allowed_photos" value='1'>
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                            <br>
                            <h4 class="text-center">Upload Photo</h4>
                            <hr>
                            <div class="avatar-body-photo">
                                <div class="avatar-upload-photo">
                                    <input type="hidden" class="avatar-src-photo" name="avatar_src">
                                    <input type="hidden" class="avatar-data-photo" name="avatar_data_photo">
                                    <label for="avatarInputPhoto">Select Photo</label>
                                    <input type="file" class="avatar-input-photo" id="avatarInputPhoto" name="avatar_file_photo">
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper-photo"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="avatar-preview-photo preview-lg hidden-xs"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                        <div class="avatar-preview-photo preview-md remove hidden"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                        <div class="avatar-preview-photo preview-sm remove hidden"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                        {{-- Zoom in out STARTED --}}
                                        <div class="avatar-btns">
                                            <div class="btn-group"><br>
                                                <button type="button" class="btn btn-default fa fa-search-plus zoom-in-btn" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                <button type="button" class="btn btn-default fa fa-search-minus zoom-out-btn" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                            </div>
                                        </div>
                                        {{-- Zoom in out ENDED --}}
                                        <br>
                                        <button type="submit" class="btn btn-default btn-block avatar-save-photo">Add Photo</button>
                                        <div class="text-center">
                                            <img src="{{asset('img/loader.gif')}}" id="loader-photo" width="20" style="display: none">
                                        </div>
                                    </div>
                                </div>
    
                                <div class="row avatar-btns-photo panel-body">
                                    <div class="col-md-9 text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="avatar-view-photo" title="" style="display:none" data-original-title="Change the profile Picture">
                                            <img src="{{ asset('img/dummy-profile.png') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Cropping Model for Event Photo ENDED --}}
@endsection
@section('footer_scripts')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/timepicki.js')}}"></script>
<script src="{{asset('summernote-0.8.12-dist/summernote.min.js')}}"></script>
<script src="{{asset('js/multi-select.js')}}"></script>
<script src="{{asset('dist/js/cropper.min.js')}}"></script>
<script src="{{asset('dist/js/main-poster-event.js')}}"></script>
<script src="{{asset('dist/js/main-cover-event.js')}}"></script>
<script src="{{asset('dist/js/main-photo-event.js')}}"></script>
<script type="text/javascript">
    $('#start_time_picker').timepicki(); 
    $('#end_time_picker').timepicki();
    jQuery(document).ready(function($){
        // add Artists
        var max_fields = 20;
        var wrapper = $(".artist-container1");
        var add_button = $(".add_artist_form_field");

        var x = 1;
        $(add_button).click(function(e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append('<div class="artist-div"><div style="position: relative;float: left; width: 78%;"><input style="margin-bottom: 10px;" class="form-control artist-text" type="text" name="artists[]" data-element-id="'+x+'" list="artist-select-'+x+'" /><datalist id="artist-select-'+x+'">@if($artists->count() > 0)<select>@foreach($artists as $artist)<option value="{{$artist->name}}"></option>@endforeach</select>@endif</datalist></div><div style="position: relative;" id="artistList-'+x+'"></div>&nbsp;<div style="float: right; width: 20%;"><a href="javascript:void(0);" class="delete">Delete</a></div></div>'); //add input box
            } else {
                alert('You Reached the limits')
            }
        });

        $(wrapper).on("click", ".delete", function(e) {
            e.preventDefault();
            $(this).parent().parent('div.artist-div').remove();
            x--;
        })
        // artist ends
        $(".switch-btns").change(function(){
            if($(this).prop("checked") == true){
                $(this).val(1)
            }else{
                $(this).val(0)
            }
        });
        $.validator.setDefaults({ ignore: '' });
    });
    var config = {
	  '.chosen-select'           : {},
	  '.chosen-select-deselect'  : {allow_single_deselect:true},
	  '.chosen-select-no-single' : {disable_search_threshold:10},
	  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	  '.chosen-select-width'     : {width:"100%"}
	}
	for (var selector in config) {
	  $(selector).chosen(config[selector]);
	}
    //-------------New Added---------------
    $('.chosen-choices').click(function(){
    //	var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    //	if(isSafari){
            // setLiCenter();
    //	}
    });
    
    $(".pimage").change(function(e) {
        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
            var file = e.originalEvent.srcElement.files[i];
            var img = document.createElement("img");
            var reader = new FileReader();
            reader.onloadend = function() {
                img.src = reader.result;
            }
            reader.readAsDataURL(file);
            $("#aimage").after(img);
        }
    });
    /* For poster upload */
    $("#posterEventUploadForm").on('submit', function(e){
        e.preventDefault();
        var image = $("#avatarInputPoster")[0].files[0];
        if (image === undefined){
            var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
            $(".avatar-upload-poster").after(msg);
            return false;
        } else {
            $("#loader-poster").show();
            $("button.avatar-save-poster").attr('disabled','disabled');
            var _token = $('input[name="_token"]').val();
            var thumb_data = $('input[name="avatar_data_poster"]').val();
            var form = new FormData();
            form.append('image', image);
            form.append('_token', _token);
            form.append('thumb_data', thumb_data);
            $.ajax({
                url: route('ajax-upload-event-add-poster'),
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(data) {
                    $("#loader-poster").hide();
                    var response = data[0];
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                    if(response.status === 0) {
                        $(".avatar-upload-poster").after(msg);
                        $("button.avatar-save-poster").removeAttr('disabled');
                    } else {
                        $("#avatar-modal-poster").modal('hide');
                        $(".crop-avatar-poster .img-responsive").attr('src', response.thumb_path);
                    }
                },
                error: function(xhr, status, error) {
                    $("#loader-poster").hide();
                    var errors = xhr.responseJSON;
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                    $(".avatar-upload-poster").after(msg);
                    $("button.avatar-save-poster").removeAttr('disabled');
                }
            });
        }
    });
    $('#avatar-modal-poster').on('hidden.bs.modal', function () {
        $("#loader-poster").hide();
        $(".avatar-body-poster .alert").remove();
        $("#posterEventUploadForm")[0].reset();
        $("button.avatar-save-poster").removeAttr('disabled');
    });
    /* For banner upload */
    $("#bannerVendorUploadForm").on('submit', function(e){
        e.preventDefault();
        var image = $("#avatarInput")[0].files[0];
        if (image === undefined){
            var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
            $(".avatar-upload").after(msg);
            return false;
        } else {
            $("#loader-banner").show();
            $("button.avatar-save").attr('disabled','disabled');
            var _token = $('input[name="_token"]').val();
            var thumb_data = $('input[name="avatar_data"]').val();
            var form = new FormData();
            form.append('image', image);
            form.append('_token', _token);
            form.append('thumb_data', thumb_data);
            $.ajax({
                url: route('ajax-upload-event-add-cover'),
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(data) {
                    $("#loader-banner").hide();
                    var response = data[0];
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                    if(response.status === 0) {
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled');
                    } else {
                        $("#avatar-modal").modal('hide');
                        $(".crop-avatar .img-responsive").attr('src', response.thumb_path);
                    }
                },
                error: function(xhr, status, error) {
                    $("#loader-banner").hide();
                    var errors = xhr.responseJSON;
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                    $(".avatar-upload").after(msg);
                    $("button.avatar-save").removeAttr('disabled');
                }
            });
        }
    });
    $('#avatar-modal').on('hidden.bs.modal', function () {
        $("#loader-banner").hide();
        $(".avatar-body .alert").remove();
        $("#bannerVendorUploadForm")[0].reset();
        $("button.avatar-save").removeAttr('disabled');
    });
    /* For photo upload */
    $("#photoVendorUploadForm").on('submit', function(e){
        e.preventDefault();
        var image = $("#avatarInputPhoto")[0].files[0];
        if (image === undefined){
            var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
            $(".avatar-upload-photo").after(msg);
            return false;
        } else {
            $("#loader-photo").show();
            $("button.avatar-save-photo").attr('disabled','disabled');
            var _token = $('input[name="_token"]').val();
            var thumb_data = $('input[name="avatar_data_photo"]').val();
            var form = new FormData();
            form.append('image', image);
            form.append('_token', _token);
            form.append('thumb_data', thumb_data);
            $.ajax({
                url: route('ajax-upload-event-add-profile'),
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(data) {
                    $("#loader-photo").hide();
                    var response = data[0];
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                    if(response.status === 0){
                        $(".avatar-upload-photo").after(msg);
                        $("button.avatar-save-photo").removeAttr('disabled');
                    } else {
                        $("#avatar-modal-photo").modal('hide');
                        $(".crop-avatar-another .img-responsive").attr('src', response.thumb_path);
                    }
                },
                error: function(xhr, status, error){
                    $("#loader-photo").hide();
                    var errors = xhr.responseJSON;
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                    $(".avatar-upload").after(msg);
                    $("button.avatar-save").removeAttr('disabled');
                }
            });
        }
    });
    $('#avatar-modal-photo').on('hidden.bs.modal', function () {
        $("#loader-photo").hide();
        $(".avatar-body-photo .alert").remove();
        $("#photoVendorUploadForm")[0].reset();
        $("button.avatar-save-photo").removeAttr('disabled');
    });

    var date = new Date();
    date.setDate(date.getDate());
    
    $('.input-group.date').datepicker({format: "dd.mm.yyyy", startDate: date});

    $("#description").summernote({height: 300});
    $("#ticket_information").summernote({height: 300});
function compareDates()
{
    var date1 = $("#start-date").val();
    var date2 = $("#end-date").val();
    if(date1 != '' && date2 != '') {
        var date1Arr = date1.split(".");
        var newSDt = date1Arr[1]+'/'+date1Arr[0]+'/'+date1Arr[2];
        var d1 = new Date(newSDt);
        var date2Arr = date2.split(".");
        var newEDt = date2Arr[1]+'/'+date2Arr[0]+'/'+date2Arr[2];
        var d2 = new Date(newEDt);
        if(d1 <= d2) {
            $('#start_end_date-error').hide();
            return true;
        } else {
            $('#start_end_date-error').show();
            return false;
        }
    }
}
$(document).ready(function() {
    $('.state-dropdown-loader, .city-dropdown-loader').hide();
    // submit form
    $("#eventAddFrm").on('submit', function(e) {
        e.preventDefault();
        $(".show-event-info-error, .show-event-info-success").hide();
        $(".show-event-info-error").html("");
        $(".show-event-info-success").html("");
        var submit_frm = true;
        var form = $("#eventAddFrm");
        var url = form.attr('action');
        var data = new FormData(form[0]);
        // validate start and end dates
        var dateCmp = compareDates();
        if(dateCmp == false) {
            submit_frm = false;
        }
        if(submit_frm == true) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                data: data,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend: function () {
                    // show loading image and disable the submit button
                    $("button[name='submit-event-info-btn']").attr('disabled','disabled');
                    $("#loader").show();
                },
                success: function(result) {
                    // show success message
                    if(result.status == 0) {
                        $(".show-event-info-error").html(result.message);
                        $(".show-event-info-error").show();
                    } else {
                        $(".show-event-info-success").html(result.message);
                        // REMOVE form
                        $("#eventAddFrm").remove();
                        $(".show-event-info-success").show();
                        setTimeout(function() {window.location.reload(true);}, 2000);
                    }
                },
                error: function(xhr, status, error){
                    var errors = xhr.responseJSON;
                    //console.log(errors);
                    var err_html = 'Errors<br />';
                    for (var key in errors) {
                        if (errors.hasOwnProperty(key)) {
                            var val = errors[key];
                            //console.log(val);
                            err_html += ' - '+val[0]+'<br />';
                        }
                    }
                    $(".show-event-info-error").html(err_html);
                    $(".show-event-info-error").show();
                },
                complete: function () {
                    // hide loading image and enable the submit button
                    $("#loader").hide();
                    $("button[name='submit-event-info-btn']").removeAttr('disabled');
                    scroll_to_div('vendor-event-information-main-bar');
                }
            });
            return false;
        } else {
            scroll_to_div('vendor-event-information-main-bar');
            return false;
        }
    });
    
    $("#end-date").on('change', function() {
        compareDates();
    });
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection