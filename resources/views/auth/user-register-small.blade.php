@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('template_linked_css')
    <link href="{{asset('css/bootstrap-datepicker')}}.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="login_p signup">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3 text-center">
				<h1><span>Join,</span> it's Free!</h1>
				<h3>LET’S GET STARTED</h3>
				<hr>
				<p>Yay! You’re on your way to creating a New Vendor listing on the world’s biggest wedding resource.Simply fill out the below information.</p>
				<hr>
			</div>
			<div class="col-sm-6 col-sm-offset-3">
				<div class="tabbable-line">
                    <form method="POST" action="{{ route('register') }}" id="userRegistrationFrm" name="userRegistrationFrm">
                        @csrf
                        <input type="hidden" name="type" value="unverified" />
                        <div class="row">	
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="first_name">First Name *</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="last_name">Last Name *</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control">
                                </div>
                            </div>
                        </div>	
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Event Date</label>
                                    <div id="filterDate2">
                                    <!-- Datepicker as text field -->         
                                        <div class="input-group date" data-date-format="dd.mm.yyyy">
                                            <input type="text" name="event_date" class="form-control" autocomplete="off" placeholder="dd.mm.yyyy">
                                            <div class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="your-budget">Your Budget *</label>
                                    <select name="budget" id="your-budget" class="form-control" required="" aria-required="true">
                                        <option value="">Unknown</option>
                                        @foreach (config('constants.user_budget') as $budget_key => $budget_val)
                                            <option value="{{$budget_key}}">{{$budget_val}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>	
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <label for="agree-checkbox" class="checkbox-inline">
                                <input type="checkbox" id="agree-checkbox" name="agree_checkbox" value="1"><span class="checkmark"></span> I agree to the Privacy &amp; Terms and Conditions
                                </label>
                                <div class="form-group has-error" id="agree_checkbox-error" style="display: none;">
                                    <span style="color:#a94442;">Please select I agree to the Privacy &amp; Terms and Conditions.</span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row text-center">
                            <div class="col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-default">NEXT</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="text-center">
                        <h5>Already have an account? <a href="{{ route('login' )}}">Login Now</a></h5>
                        <h5> Are you a Vendor? <a href="{{ route('vendor-signup' )}}">Sign Up Now</a></h5>
                    </div>
			    </div>
		    </div>
	    </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    date.setDate(date.getDate()-1);
    $('.input-group.date').datepicker({format: "dd.mm.yyyy", startDate: date});
    var agree_checkbox = $("#agree-checkbox");
    $(document).ready(function() {
        $("form[name='userRegistrationFrm']").on('submit', function() {
            if(agree_checkbox.is(':checked') == true) {
                $("#agree_checkbox-error").hide();
                $(this).val('1');
                return true;
            } else {
                $("#agree_checkbox-error").show();
                $(this).val('');
                return false;
            }
        });
        agree_checkbox.on('change', function() {
            if(agree_checkbox.is(':checked') == true) {
                $("#agree_checkbox-error").hide();
                $(this).val('1');
            } else {
                $("#agree_checkbox-error").show();
                $(this).val('');
            }
        });
    });
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection