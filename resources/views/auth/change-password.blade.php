@extends('layouts.app')

@section('template_title')
    {{ Auth::user()->name }}'s' Setting
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection
@section('content')
    <div id="page-content-wrapper">
        <section>
            @include('partials.setting-navigation-mobile')
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-s " style="margin-top: 0px;">
                        <div class="row">
                            @include('partials.user-sidebar-navigation')
                            <div class="col-sm-8 col-sm-offset-4 panel-body menu">
                                @include('partials.setting-navigation')
                                @if (session('error'))
                                    <div class="alert alert-danger">
                                        {{ session('error') }}
                                    </div>
                                @endif
                                @if (session('success'))
                                    <div class="alert alert-success">
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <div class="panel panel-body partner">
                                    <h4>Change Password</h4>
                                    <hr>
                                    <form method="POST" action="{{ route('change-password') }}" id="changePasswordForm">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <label>Current password</label>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Password" name="current_password" id="current_password">
                                                    @if ($errors->has('current-password'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('current-password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <label>New password</label>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Password" name="new_password" id="new_password">
                                                    @if ($errors->has('new-password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('new-password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <label>Confirm New password</label>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Password" name="new_password_confirmation" id="new_password_confirmation">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <button type="submit" class="btn btn-warning btn-block">SAVE</button>
                                            </div>
                                        </div>

                                        <br>

                                    </form></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
