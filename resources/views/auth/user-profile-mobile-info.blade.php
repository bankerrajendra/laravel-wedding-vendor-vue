@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">

                <div class="col-sm-4 col-sm-offset-4">
                    <div class="list-group mobile-sidebar">
                        <li class="list-group-item text-center">PROFILE</li>
                        <a href="{{route('user-manage-profile')}}" class="list-group-item">My Profile<i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('user-manage-photo') }}" class="list-group-item {{ Request::is('myprofile/user-manage-photo', 'myprofile/user-manage-photo/*') ? 'active' : '' }}">Profile Photo</a>
                        <a href="{{ route('request-a-quote') }}" class="list-group-item"> Connect with Vendors <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{route('muslim-vendors')}}" class="list-group-item"> Browse Vendors <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
