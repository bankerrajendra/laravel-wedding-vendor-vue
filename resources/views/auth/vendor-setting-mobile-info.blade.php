@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    <div class="list-group mobile-sidebar">
                        <li class="list-group-item text-center">Settings</li>
                        <a href="{{ route('vendor-change-password') }}" class="list-group-item">Change Password<i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('vendor-notification') }}" class="list-group-item"> Notification <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{ route('my-membership') }}" class="list-group-item"><i class="fa fa-angle-right pull-right" aria-hidden="true"></i> My Membership</a>
                        <a href="{{ route('vendor-delete-account') }}" class="list-group-item">Delete/Deactivate Account <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
