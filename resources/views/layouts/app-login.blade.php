<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="{{ config('app.locale') }}" dir="ltr">
<!--<![endif]-->
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
{{-- CSRF Token --}}
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('template_title')</title>
<meta name="description" content="@yield('meta_description')">
<meta name="keyword" content="@yield('meta_keyword')">
<link rel="canonical" href="@yield('canonical')"/>

{{-- Fonts --}}
@yield('template_linked_fonts')

<!--favicon-->
<link href="{{ asset('img/fev.png') }}" rel="icon" type="image/png" />
<!--Style CSS-->
<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
<!--Responsive CSS-->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<!--Font Awesome CSS-->
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<!--Fonts-->
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700,700i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i,900,900i" rel="stylesheet">
@yield('template_linked_css')
<style>
body.login-body-bg {
    background: url('{{asset("img/vendor-bc.jpg")}}');
	background-size: 100%;
}
.error-help-block {
	text-align: left;
}
</style>
@yield('head')
</head>
<body class="login-body-bg">

@yield('content')

<!--End Footer-->
@routes

<!--Main JS-->
<script src="{{ asset('js/main.js') }}"></script>
<!--Bootstrap JS-->
<script src="{{ asset('js/bootstrap.js') }}"></script>
@yield('footer_scripts')
@stack('footer_scripts')

</body>
</html>
