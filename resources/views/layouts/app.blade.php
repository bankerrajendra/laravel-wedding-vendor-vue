<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html lang="{{ config('app.locale') }}" dir="ltr" class="ie8"><![endif]-->
<!--[if IE 9 ]><html lang="{{ config('app.locale') }}" dir="ltr" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="{{ config('app.locale') }}" dir="ltr">
<!--<![endif]-->
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
{{-- CSRF Token --}}
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('template_title')</title>
<meta name="description" content="@yield('meta_description')">
<meta name="keyword" content="@yield('meta_keyword')">
    @if(isset($index_page_flag) && $index_page_flag=='1')
        <link rel="canonical" href="@yield('canonical')"/>
    @endif

<script>
    window.App = {!! json_encode([
    'user' => Auth::user(),
    'signedIn' => Auth::check()
]) !!};
</script>

{{-- Fonts --}}
@yield('template_linked_fonts')

<!--Style CSS-->
<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
<!--Custom CSS-->
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css">
<!--Responsive CSS-->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<!--Font Awesome CSS-->
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/jquery.css') }}" rel="stylesheet" type="text/css">


    <link href="{{ asset('img/'.getGeneralSiteSetting('site_favicon')) }}" rel="icon" type="image/png" />
<!--Fonts-->
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700,700i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i,900,900i" rel="stylesheet">
@yield('template_linked_css')
<style>
.center {
    margin: auto;
    width: 60%;
    padding: 20px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.hideform {
    display: none;
}
</style>
@yield('head')
</head>
<body>
<!--header-->

    @guest

    @else
        {{-- @include('partials.mobile-nav') --}}
    @endguest
<header>
    @include('partials.nav')
	
</header>

@yield('content')

<!--Start Footer-->
@include('partials.footer')
<!--End Footer-->
@routes
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="{{asset('js/jquery.jscroll.min.js')}}"></script>

<!--Main JS-->
<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{ asset('js/main.js') }}"></script>
@if(Request::is('home','conversation/*'))
    <script src="{{ asset('js/app.js') }}"></script>
@endif

<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{asset('js/bootstrap-select.min.js')}}"></script>
<!--Bootstrap JS-->
<script src="{{ asset('js/bootstrap.js') }}"></script>
@yield('footer_scripts')
@stack('footer_scripts')
<script>
$(document).ready(function(){
    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
	
	$(document).on("mouseenter", "#profile-images .dropdown",
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        }
	);
	$(document).on("mouseleave", "#profile-images .dropdown",
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<script>
    $(function () {
        $(document).scroll(function () {
            var $nav = $(".navbar-fixed-top");
            $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        });
    });
</script>

{!!getGeneralSiteSetting('site_google_analytics_code')!!}
<script>
    function disablescroll(e,e1) {
        var value=document.getElementById('hdnF').value;
        if(value==0) {
            $("body").addClass("nav-open");
            document.getElementById('hdnF').value=1;
        } else {
            document.body.removeAttribute('class');
            document.getElementById('hdnF').value=0;
        }
    }
</script>
<script type="text/javascript">
    function openNav() {
        document.getElementById("mySidenav").style.width = "101%";
        document.body.style.overflowY = "hidden";
        document.body.style.position = "fixed";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.body.style.overflowY = "auto";
        document.body.style.position = "relative";
    }

    function openInNewTab(locationurl,protos='',protowww='',postfixpart='') {
        locationprotocol = "http";
        var win = window.open(locationprotocol+protos+'://'+protowww+locationurl+'.com/'+postfixpart, '_blank');
        win.focus();
    }


    $('form[name="subscribe-newsletter-frm"]').on('submit', function(e) {
        e.preventDefault();
        var email = $('form[name="subscribe-newsletter-frm"] input[name="newsletter-email"]').val();
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('ajax-subscribe-newsletter-action') }}",
            method:"POST",
            data:{
                email:email,
                _token:_token
            },
            success:function(result)
            {
                if(result.success) {
                    $('#newsletter-email-succ-msg').html(result.success);
                    $('#newsletter-email-succ-msg').show().delay(3000).fadeOut();
                    $('form[name="subscribe-newsletter-frm"] input[name="newsletter-email"]').val('')
                }
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            var responseMsg = jQuery.parseJSON(jqXHR.responseText);
            if(responseMsg.errors.email[0] != "") {
                $('#newsletter-email-blank-err').html(responseMsg.errors.email[0]);
                $('#newsletter-email-blank-err').show();
            }
        });
        return false;
    });
    $('form[name="subscribe-newsletter-frm"] input[name="newsletter-email"]').on('input',function(e){
        $('#newsletter-email-blank-err, #newsletter-email-succ-msg').hide();
        $('#newsletter-email-blank-err, #newsletter-email-succ-msg').html('');
    });
</script>
</body>
</html>
