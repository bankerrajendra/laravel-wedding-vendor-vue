<div class='movableContent'>
    <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
        <tr>
            <td width="100%" colspan="2">
                <hr style="height:1px;border:none;color:#333;background-color:#ddd; margin-bottom: 0px;" />
            </td>
        </tr>
        <tr style="background: #ccc;">
            <td width="100%" height="90" valign="middle">
                <div class="contentEditableContainer contentTextEditable">
                    <div class="contentEditable" align='center'>
                        <span style="padding:10px 0px;font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif; line-height: 20px;"><a href="{{getGeneralSiteSetting('site_url')}}">PunjabiWedding.com</a>. @if(!empty($unsubscribe_link))<a href="{{$unsubscribe_link}}">Unsubscribe</a>@endif<br> is committed to protecting your privacy and the confidentiality of your personal information. To ensure your privacy, do not share your personal information (e.g., account email, password ,  credit card information, address or phone number, etc.) with anyone.</span>
                        </span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td width="100%" height="50" valign="middle">
                <div class="contentEditableContainer contentTextEditable">
                    <div class="contentEditable" align='center'>
                        <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;    line-height: 20px;">{!!getGeneralSiteSetting('site_footer_text')!!}</span>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>