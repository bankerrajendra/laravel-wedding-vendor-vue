<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Welcome</title>
    <style type="text/css">

        @media screen and (max-width: 600px) {
            table[class="container"] {
                width: 95% !important;
            }
        }

        #outlook a {padding:0;}
        body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
        .ExternalClass {width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
        #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
        a img {border:none;}
        .image_fix {display:block;}
        p {margin: 1em 0;}
        h1, h2, h3, h4, h5, h6 {color: black !important;}

        h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

        h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
            color: red !important;
        }

        h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
            color: purple !important;
        }

        table td {border-collapse: collapse;}

        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }

        a {color: #000;}

        @media only screen and (max-device-width: 480px) {

            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: black; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }

            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: orange !important; /* or whatever your want */
                pointer-events: auto;
                cursor: default;
            }
        }


        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: blue; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }

            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: orange !important;
                pointer-events: auto;
                cursor: default;
            }
        }

        @media only screen and (-webkit-min-device-pixel-ratio: 2) {
            /* Put your iPhone 4g styles in here */
        }

        @media only screen and (-webkit-device-pixel-ratio:.75){
            /* Put CSS for low density (ldpi) Android layouts in here */
        }
        @media only screen and (-webkit-device-pixel-ratio:1){
            /* Put CSS for medium density (mdpi) Android layouts in here */
        }
        @media only screen and (-webkit-device-pixel-ratio:1.5){
            /* Put CSS for high density (hdpi) Android layouts in here */
        }
        /* end Android targeting */
        h2{
            color:#181818;
            font-family:Helvetica, Arial, sans-serif;
            font-size:22px;
            line-height: 22px;
            font-weight: normal;
        }
        a.link1{

        }
        a.link2{
            color:#fff;
            text-decoration:none;
            font-family:Helvetica, Arial, sans-serif;
            font-size:16px;
            color:#fff;border-radius:4px;
        }
        p{
            color:#555;
            font-family:Helvetica, Arial, sans-serif;
            font-size:14px;
            line-height:160%;
        }
    </style>

    <script type="colorScheme" class="swatch active">
  {
    "name":"Default",
    "bgBody":"ffffff",
    "link":"fff",
    "color":"555555",
    "bgItem":"ffffff",
    "title":"181818"
  }
</script>

</head>
<body>
<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
<table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class='bgBody' >
    <tr>
        <td>
            <table cellpadding="0" width="620" class="container" align="center" cellspacing="0" border="0">
                <tr>
                    <td>
                        <!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->


                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container" style="border:1px solid #ccc; padding: 20px;">
                            <tr>
                                <td class='movableContentContainer bgItem'>

                                    <div class='movableContent'>
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" bgcolor="#000" class="container">
                                            <tr >
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" valign="top">&nbsp;</td>
                                                <td width="200" valign="top" align="center">
                                                    <div class="contentEditableContainer contentImageEditable">
                                                        <div class="contentEditable" align='center'>
                                                            <img src="{{asset('img/logo.png')}}"   alt='Logo'  data-default="placeholder" />
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="200" valign="top">&nbsp;</td>
                                            </tr>
                                            <tr height="5" style="border-bottom: 2px solid #cf0404;;">
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div class='movableContent'>
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                            <tr>
                                                <td colspan="3">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align='left' style="padding-left: 10px; padding-right: 10px;"  >
                                                            <h3>Hi {{$user->name}}, Welcome to Punjabi Girl</h3>
                                                            <p>Thank you for registering with Punjabi Girl.</p>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="20" align="left"><img src="{{asset('img/circle.png')}}" style="padding: 10px; width:40px;"></td>
                                                <td align="center">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align='left' >
                                                            <p>Complete your profile.</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="20" align="left"><img src="{{asset('img/circle.png')}}" style="padding: 10px; width:40px;"></td>
                                                <td align="center">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align='left' >
                                                            <p>A complete and in-depth profile makes it easier for you to connect with the right person.</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="20" align="left"><img src="{{asset('img/circle.png')}}" style="padding: 10px; width:40px;"></td>
                                                <td align="center">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align='left' >
                                                            <p>Get more responses by uploading up to 5 photos on your profile.</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="20">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td width="20" align="left"><img src="{{asset('img/circle.png')}}" style="padding: 10px; width:40px;"></td>
                                                <td align="center">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align='left' >
                                                            <p>To view your matches Login regularly and check your inbox for new messages.</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="20">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td width="20" align="left"><img src="{{asset('img/circle.png')}}" style="padding: 10px; width:40px;"></td>
                                                <td align="center">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align='left' >
                                                            <p>At Punjabi Girl, your safety is a priority. To stay safe report All suspicious behavior immediately.</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="20">&nbsp;</td>
                                            </tr>

                                        </table>
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable" align='left' style="padding-left: 10px; padding-right: 10px;"  >
                                                <h4>We look forward to serving you,</h4>
                                                <p>The Punjabi Girl Team</p>
                                            </div>
                                        </div>
                                    </div>


                                    <div class='movableContent'>
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                            <tr>
                                                <td width="100%" colspan="2">
                                                    <hr style="height:1px;border:none;color:#333;background-color:#ddd; margin-bottom: 0px;" />
                                                </td>
                                            </tr>
                                            <tr style="background: #ccc;">
                                                <td width="100%" height="90" valign="middle">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align='center'>
										<span style="padding:10px 0px;font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif; line-height: 20px;">To ensure your privacy, do not share your personal information with anyone.<br>
										You have received this email because you are a member of PunjabiGirlcom. <a href="https://www.punjabigirl.com/unsubscribe/{{@$user->token}}" target="_blank">Unsubscribe</a><br>
										If you have a question, or need assistance, please contact <a href="http://www.punjabigirl.com/">Customer Service</a>
										</span>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" height="50" valign="middle">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align='center'>
                                                            <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif; line-height: 20px;">Copyright &copy; 2018 <a href="https://www.punjabigirl.com/">www.punjabigirl.com</a> - All rights reserved.</span>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>


                                </td>
                            </tr>
                        </table>




                    </td></tr></table>

        </td>
    </tr>
</table>
<!-- End of wrapper table -->
</body>
</html>
