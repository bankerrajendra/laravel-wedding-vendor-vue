@extends('adminlte::page')
@section('title', 'Religion | New')
@section('content_header')
    <h1>Create Religion</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

{{--@section('template_title')--}}
{{--@lang('usersmanagement.editing-user', ['name' => $user->name])--}}
{{--@endsection--}}

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }		
    </style>
@endsection

@section('content')
    {{--<div class="container">--}}
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>



                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('saveReligion'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}


							<!--<div class="form-group has-feedback row">

                                <label for="title" class="col-md-3 control-label"> Site | Religion ID</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('title', $nextSiteId, array('id' => 'title', 'readonly'=>'readonly', 'class' => 'form-control', 'placeholder' => 'Religion Title')) !!}
										<small class="smallText">Default Generated</small>
                                    </div>
                                </div>
                            </div> -->

                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> Title</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('religion_title', '', array('id' => 'title','class' => 'form-control', 'placeholder' => 'Religion Title')) !!}										
                                    </div>
                                </div>
                            </div> 


                            <div class="form-group has-feedback row">

                                <label for="description" class="col-md-3 control-label">Description </label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::textarea('religion_description', '', array('id' => 'description', 'class' => 'form-control', 'placeholder' => 'Religion Description')) !!}

                                    </div>

                                </div>
                            </div>

                            
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}

    @include('modals.modal-save')
    @include('modals.modal-delete')

@endsection

@section('js')
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @include('scripts.check-changed')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js" type="text/javascript"></script>
    <script>
        CKEDITOR.config.allowedContent=true;
        CKEDITOR.replace( 'description' );
    </script>


    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
