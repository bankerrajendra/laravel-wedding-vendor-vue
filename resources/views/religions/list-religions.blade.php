{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title', 'Manage Religions')
@section('content_header')
    <h1>Religions | List All 
	 {{--<a class="btn btn-sm btn-info" href="{{ route('createReligions')}}" data-toggle="tooltip" title="Create Religion">	 <i class="fa fa-plus"></i> Create New</a> --}}
    </h1>
@stop
{{--@section('template_title')--}}
    {{--@lang('usersmanagement.showing-all-users')--}}
{{--@endsection--}}

{{--@section('template_linked_css')--}}
@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">

    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="box">
                    <div class="box-header"></div>
                    <div class="box-body">                       
                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table">
                             
                                <thead class="thead">
                                <tr>
                                    <th>#</th>
                                    <th class="hidden-xs" >Religion Title</th>

                                    {{--<th class="hidden-sm hidden-xs hidden-md">created On</th>--}}
                                    {{--<th class="hidden-sm hidden-xs hidden-md">Updated On</th>--}}
                                    {{--<th>Action</th>--}}
                                    {{--<th class="no-search no-sort"></th>--}}
                                    {{--<th class="no-search no-sort"></th>--}}
                                </tr>
                                </thead>
                                <tbody id="cmscitystate_table">
                                {{$i=1}}
                                @foreach($religionLists as $religion)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$religion->religion}}</td>

                                        {{--<td class="hidden-sm hidden-xs hidden-md">{{$religion->created_at}}</td>--}}
                                        {{--<td class="hidden-sm hidden-xs hidden-md">{{$religion->updated_at}}</td>--}}
                                        {{--<td>--}}
										{{----}}
                                            {{--<a class="btn btn-sm btn-info" href="{{ route('editReligions', ['id'=>$religion->id]) }}" data-toggle="tooltip" title="Edit">--}}
                                                {{--<i class="fa fa-edit"></i>--}}
                                            {{--</a>--}}
										 {{----}}
											{{--<a class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')" href="{{ route('deleteReligion', ['id'=>$religion->id]) }}" data-toggle="tooltip" title="Edit">--}}
                                                {{--<i class="fa fa-remove "></i>--}}
                                            {{--</a>--}}
											{{----}}
                                        {{--</td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                                {{--<tbody id="search_results"></tbody>--}}
                                {{--@if(config('usersmanagement.enableSearchUsers'))--}}
                                    {{--<tbody id="search_results"></tbody>--}}
                                {{--@endif--}}

                            </table>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--@include('modals.modal-delete')--}}

@endsection
