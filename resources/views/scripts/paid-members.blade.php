<script type="text/javascript">
$('.view-payments').on('click', function() {
        var user_id = $(this).data("userId");
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('member-payment-history') }}",
            method:"POST",
            data:{uid:user_id, _token:_token},
            success:function(result)
            {
                $('#membershipHistoryModal').modal('show');
                $("#membershipHistoryModal .user-name").html(result.user_name);
                var history_html = ""
                var data = result.membership_history;
                $.each(data, function(key, item) 
                {
                    history_html += "<tr><td>"+item.duration+" "+item.duration_type+" "+item.plan_name+"</td><td>"+formatDate(item.created_at)+"</td><td>"+item.currency+" "+item.plan_amount+"</td><td>"+item.payment_gateway_name+"</td></tr>";
                });
                $("#membershipHistoryModal #table-records").html(history_html);
            }
        })
		
});
$('.edit-plan').on('click', function() {
    $('#model_body_common, #model_body_common_err').hide();
    $('#model_body_common, #model_body_common_err').html('');
    var user_id = $(this).data("userId");
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:"{{ route('member-info') }}",
        method:"POST",
        data:{uid:user_id, _token:_token},
        success:function(result)
        {
            $('#membershipEdiPlanModal').modal('show');
            $("#membershipEdiPlanModal .user-id").html(result.user_id);
            $("#membershipEdiPlanModal .user-name").html(result.user_name);
            $("#membershipEdiPlanModal .user-email").html(result.user_email);
        }
    })	
});
function formatDate(date) {
   
    var d = new Date(date);
    var curr_day = d.getDate();
    var curr_month = d.getMonth();
    var curr_year = d.getFullYear();
    var curr_hour = d.getHours();
    var curr_min = d.getMinutes();
    var curr_sec = d.getSeconds();

    var month_name = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

    curr_month++ ; // In js, first month is 0, not 1
    
    return month_name[curr_month-1] + " " + curr_day + ", "+ curr_year + " "+curr_hour+":"+curr_min+":"+curr_sec;
}

function update_payment_member() {
	var package_id = $("#package_id").val();
	var payment_note = $("#payment_note").val();
	var hidd_user_id = $("#membershipEdiPlanModal .user-id").html();
	var payment_mode = $("#payment_mode").val();
	if(package_id =='')
	{
		alert('Please Select Plan');
		return false;
	}
	if(payment_mode == '')
	{
		alert('Please Select Payment mode');
		return false;
	}
    $("#updt-mem-submit-btn").attr("disabled", true);
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:"{{ route('update-member-plan') }}",
        method:"POST",
        data:{ 
            uid:hidd_user_id, 
            _token:_token, 
            package_id:package_id, 
            payment_mode:payment_mode, 
            payment_note:payment_note 
        },
        success:function(result)
        {
            if(result.status == 'success')
			{	
				$('#model_body_common').html(result.message);
                $('#model_body_common').slideDown();
                // clean up the messages
                $("#package_id").val('');
                $("#payment_note").val('');
	            $("#membershipEdiPlanModal .user-id").html('');
	            $("#payment_mode").val('');
			}
			else
			{
				$('#model_body_common_err').html(result.message);
				$('#model_body_common_err').slideDown();
			}
            $("#updt-mem-submit-btn").attr("disabled", false);
        }
    })
	return false;
}

</script>