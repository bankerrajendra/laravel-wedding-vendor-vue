<script type="text/javascript">
$('.send-email').on('click', function() {
    $('#model_body_common_email, #model_body_common_email_err').hide();
    $('#model_body_common_email, #model_body_common_email_err').html('');
    var user_id = $(this).data("userId");
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:"{{ route('member-info') }}",
        method:"POST",
        data:{uid:user_id, _token:_token},
        success:function(result)
        {
            $('#sendEmail').modal('show');
            $("#sendEmail .user-id").html(result.user_id);
            $("#sendEmail .user-name").html(result.user_name);
            $("#sendEmail .user-email").html(result.user_email);
            $("#sendEmail .user-mobile-number").html("+"+result.country_code+"-"+result.mobile_number);
            $("#sendEmail #user-email").html(result.user_email);
        }
    })	
});

function send_email() {
	var email = $("#sendEmail #user-email").html();
	var subject = $("#sendEmail input[name='subject']").val();
	var message = $("#sendEmail #email-message").val();
	if(email =='')
	{
		alert('Please add email address');
		return false;
	}
    if(subject == '')
	{
		alert('Please add subject');
		return false;
	}
	if(message == '')
	{
		alert('Please add message');
		return false;
	}
    $("#updt-send-email-submit-btn").attr("disabled", true);
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:"{{ route('send-email-member') }}",
        method:"POST",
        data:{ 
            email:email, 
            _token:_token, 
            message_body:message,
            subject:subject,
            ajax: true
        },
        success:function(result)
        {
            if(result.status == 'success')
			{
				$('#model_body_common_email').html(result.message);
                $('#model_body_common_email').slideDown();
                $("#sendEmail input[name='subject']").val('');
                $("#sendEmail #email-message").val('');
			}
			else
			{
				$('#model_body_common_email_err').html(result.message);
				$('#model_body_common_email_err').slideDown();
			}
            $("#updt-send-email-submit-btn").attr("disabled", false);
        }
    })
	return false;
}
</script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script>
    $('#email-message').ckeditor();
</script>