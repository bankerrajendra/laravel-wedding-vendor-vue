<script type="text/javascript">
$('.send-sms').on('click', function() {
    $('#model_body_common_sms, #model_body_common_sms_err').hide();
    $('#model_body_common_sms, #model_body_common_sms_err').html('');
    var user_id = $(this).data("userId");
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:"{{ route('member-info') }}",
        method:"POST",
        data:{uid:user_id, _token:_token},
        success:function(result)
        {
            $('#sendSMS').modal('show');
            $("#sendSMS .user-id").html(result.user_id);
            $("#sendSMS .user-name").html(result.user_name);
            $("#sendSMS .user-email").html(result.user_email);
            $("#sendSMS .user-mobile-number").html("+"+result.country_code+"-"+result.mobile_number);
            $("#sendSMS #user-mobile-number").html("+"+result.country_code+result.mobile_number);
        }
    })	
});

function send_sms() {
	var mobile_number = $("#sendSMS #user-mobile-number").html();
	var user_name = $("#sendSMS .user-name").html();
	var sms = $("#sendSMS #sms").val();
	if(mobile_number =='')
	{
		alert('Please add mobile number');
		return false;
	}
	if(sms == '')
	{
		alert('Please add message');
		return false;
	}
    $("#updt-send-sms-submit-btn").attr("disabled", true);
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:"{{ route('send-sms-member') }}",
        method:"POST",
        data:{ 
            contact_number:mobile_number, 
            _token:_token, 
            message_body:sms,
            user_name:user_name,
            ajax: true
        },
        success:function(result)
        {
            if(result.status == 'success')
			{
				$('#model_body_common_sms').html(result.message);
                $('#model_body_common_sms').slideDown();
                $("#sendSMS #sms").val('');
			}
			else
			{
				$('#model_body_common_sms_err').html(result.message);
				$('#model_body_common_sms_err').slideDown();
			}
            $("#updt-send-sms-submit-btn").attr("disabled", false);
        }
    })
	return false;
}

</script>