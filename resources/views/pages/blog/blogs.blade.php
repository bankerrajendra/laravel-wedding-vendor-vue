@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('navclasses') background-black outer-nav  @endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 text-center bb">
                <h2>Our Blog</h2>
                <img src="{{asset('img/title-img.png')}}">
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8">

                @foreach($bloglist as $blogRec)

                    <div class="blog-p">
                        <img src="{{config('constants.upload_url_public'). config('constants.blogimage_upload_path_rel').$blogRec->blog_image}}" class="img-responsive">
                        <div class="media-body">
                            <h3>{{$blogRec->title}}</h3>
                            <p><i class="fa fa-calendar" aria-hidden="true"></i> <i>Posted on {{date_format($blogRec->created_at,'l, F d, Y')}}</i></p>
                            <p>{!! substr(html_entity_decode($blogRec->description),0,330) !!}</p>
                            <a href="{{route('blog-detail',['slug'=>$blogRec->slug])}}" class="read_more">Read More</a>
                            <a href="{{route('blog-detail',['slug'=>$blogRec->slug])}}#commentss" class="read_more"><i class="fa fa-pencil" aria-hidden="true"></i> Comments ({{get_blog_comments_count($blogRec->id)}})</a>
                        </div>
                    </div>

                @endforeach

                    @if(config('usersmanagement.enableCmsPagination'))
                        {{ $bloglist->links() }}
                    @endif

            </div>

            <div class="col-sm-4 latest-post">

                <div class="panel panel-default">
                    <div class="panel-heading panel-head">
                        <h4>Latest Posts</h4>
                    </div>
                    <ul class="list-group lis">
                        @foreach($bloglistsidebar as $blogRec)
                            <li class="list-group-item">
                                <a href="{{route('blog-detail',['slug'=>$blogRec->slug])}}">{{$blogRec->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>

        </div>
    </div>
@endsection
