@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('navclasses') background-black outer-nav  @endsection

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 text-center bb">
                <h2>Our Blog</h2>
                <img src="{{asset('img/title-img.png')}}">
            </div>
        </div>




        <div class="row">
            <div class="col-sm-8">
                <div class="blog-p">
                    <img class="img-responsive" alt="{{$blogRec->title}}" src="{{config('constants.upload_url_public'). config('constants.blogimage_upload_path_rel').$blogRec->blog_image}}">
                    <div class="media-body">
                        <h3>{{$blogRec->title}}</h3>
                        <p><i class="fa fa-calendar" aria-hidden="true"></i> <i>Posted on {{date_format($blogRec->created_at,'l, F d, Y')}}</i></p>
                        <p>{!! $blogRec->description !!}</p>
                    </div>
                </div>




                {{--//added by bindu for comments--}}
                <a name="commentss"></a>
                <div id="comments"><br></div>

                <?php if(isset($blogcomments) && count($blogcomments)>0) { ?><br>
                <div class="media1"  >
                    <?php foreach($blogcomments as $commentKey => $commentValue):
                    //echo $commentKey; ?>
                    <div class="media">
                        <div class="media-left">
                            <img src="/img/blogcomment_icon.png" class="media-object" style="width:45px" alt="">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $commentValue->name; ?> <small><i>Posted on <?php echo date("M d, Y",strtotime($commentValue['created_at'])); ?></i></small></h4>
                            <p><?php echo $commentValue['messages']; ?></p>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php }	?>

                <div id="reply"><br style="clear:both"><br style="clear:both"></div>
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => 'blog-comments', 'method' => 'post', 'role' => 'form', 'class' => 'needs-validation', 'style' => 'display:inline')) !!}


                <h3 style="margin-bottom: 15px;">Leave Reply</h3>
                <div class="form-group">
                    <input type="hidden" name="blog_id" value="{{$blogRec->id}}">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{old('name')}}"   >
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{old('email')}}"   >
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile Number" value="{{old('mobile_number')}}"   >
                </div>
                <div class="form-group">
                    <textarea class="form-control" type="textarea" id="messages" name="messages" placeholder="Message" maxlength="140" rows="7" required>{{old('messages')}}</textarea>
                </div>
                <button type="submit" id="submit" name="submit" class="btn btn-danger">Submit Form</button>
                </form>

                {{--//END: added by Bindu for comments--}}
                <br><br>



            </div>

            <div class="col-sm-4 latest-post">

                <div class="panel panel-default">
                    <div class="panel-heading panel-head">
                        <h4>Latest Posts</h4>
                    </div>
                    <ul class="list-group lis">
                        @foreach($bloglistsidebar as $blogRec)
                            <li class="list-group-item">
                                <a href="{{route('blog-detail',['slug'=>$blogRec->slug])}}">{{$blogRec->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>

        </div>
    </div>


@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection