@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('navclasses')background-black outer-nav  @endsection
@section('content')
    <!--Start Banner-->
    <section>
      
<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 text-center panel-body">
			<h2>{{$country_name}} {{$pagetypes}} </h2>
			<img src="img/title-img.png"/>	
		</div>
	</div>
	<div class="row">	
		<div class="col-sm-8 col-sm-offset-2 panel-body">
			<ul class="list-inline brands-list">
				<?php 
					for($i=65;$i<=90;$i++) {
						echo '<li><a href="#'.lcfirst(chr($i)).'">'.chr($i).'</a></li>';
					}
				?>

			</ul>
			<div class="panel panel-default panel-body">
				
				
                        <?php for($i= 65; $i<=90; $i++){ ?>
                        <div class="community-heading" id="<?php echo lcfirst(chr($i)); ?>"><?php echo chr($i); ?></div>
                        <div class="row community-content">
                            <?php if(array_key_exists(chr($i), $statelist)){ ?>
                                <?php foreach($statelist[chr($i)] as $state){ ?>
                                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                                    {{--{{route('get-language', ['type_id'=>$lang['type_id'],'slug'=>$lang['slug']])}}--}}
                                    <?php if ($country_name=='Languages') { ?>
                                        <p><a href="{{route('get-language', ['slug'=>$state->slug])}}"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> {{$state->type_name}}</a></p>
                                    <?php } else { ?>
                                        <p><a href="{{ route('get-'.strtolower($pagetype),['cityname'=>$state->slug]) }}"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> {{$state->type_name}}</a></p>
                                    <?php } ?>
                                </div>
                            <?php }
                            } ?>
                        </div>
                        <?php } ?>
			</div>
		</div>
	</div>
</div>
 
    </section>
@endsection