@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('navclasses') background-black outer-nav  @endsection

@section('content')
    <!--Start Section-->

    <div class="container sec_tion">
        <div class="row sitemap">
            <div class="col-sm-10 col-sm-offset-1 text-center">
                <h3>Site Map</h3>
            </div>

            <div class="col-sm-10 col-sm-offset-1">
                <div class="panel">
                    <hr style="margin-top:0px;" />


                    <h4>Vendor Categories</h4>

                    <hr />
                    <div class="colum_map">
                        <ul>
                            @foreach($vendorCat as $vcatArr)
                                <li> <a href="{{route('search-vendor', ['category' => $vcatArr['slug']])}}">{{$vcatArr['name']}}</a></li>

                            @endforeach

                        </ul>
                    </div>

                    <h4>Event Categories</h4>

                    <hr />
                    <div class="colum_map">
                        <ul>
                            @foreach($eventCat as $ecatArr)

                                <li> <a href="{{route('search-events', ['category' => $ecatArr['slug']])}}">{{$ecatArr['name']}}</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <h4>Other Info</h4>

                    <hr />
                    <div class="colum_map">
                        <ul>
                            <li><a href="{{ route('search-events') }}">Events</a></li>
                            <li><a href="{{route('muslim-vendors')}}" > WEDDING VENDORS</a></li>
                            <li><a href="{{route('search-vendor', ['category' => 'marriage-bureau'])}}">MARRIAGE BUREAU</a></li>
                            <li><a href="{{ route('request-a-quote') }}">CONNECT WITH VENDORS</a></li>

                            <li><a href="{{route('contact')}}">Contact Us</a></li>
                            <li><a href="{{route('vendor-event-signup')}}">Post An Event</a> </li>
                            <li><a href="{{route('vendor-signup')}}">Free Business Listing</a> </li>
                            <li><a href="{{route('cmsroots', ['slug'=>'privacy'])}}">Privacy Policy</a></li>
                            <li><a href="{{route('cmsroots', ['slug'=>'term-condition'])}}">Terms &amp; Conditions</a></li>
                            <li><a href="{{route('cmsroots',['slug'=>'aboutus'])}}">About Us</a></li>
                            <li><a href="{{route('cmsroots',['slug'=>'disclaimers'])}}">Disclaimers</a></li>
                            <li><a href="{{route('cmsroots',['slug'=>'faqs'])}}">FAQ's</a></li>
                            <li><a href="{{route('cmsroots', ['slug'=>'howitworks'])}}">How It Works</a></li>
                            <li><a href="{{route('blogs')}}">Blog</a></li>
                            <li><a href="{{route('login')}}">Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Section-->
@endsection