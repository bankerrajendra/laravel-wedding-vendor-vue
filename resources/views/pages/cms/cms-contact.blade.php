@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('navclasses')background-black outer-nav  @endsection
{!! NoCaptcha::renderJs() !!}
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center aboout panel-body">
                <h2>CONTACT US</h2>
                <img src="{{asset('img/title-img.png')}}">
            </div>

        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 panel-body contact-form">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(array('route' => 'send-contact', 'method' => 'post', 'role' => 'form', 'class' => 'needs-validation', 'style' => 'display:inline')) !!}

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{old('username')}}"  >
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" id="email" name="email" type="email" value="{{old('email')}}" >
                    </div>
                    <div class="form-group">
                        <label for="name">Subject</label>
                        <input class="form-control" id="subject" name="subject" type="text" value="{{old('subject')}}" >
                    </div>
                    <div class="form-group">
                        <label for="name">Mobile</label>
                        <input class="form-control" id="mobile" name="mobile" maxlength="16" type="text" value="{{old('mobile')}}" >
                    </div>

                    <div class="form-group">
                        <label for="name">Message</label>
                        <textarea class="form-control" id="message" name="message" rows="5" >{{old('message')}}</textarea>
                    </div>

                    {!! NoCaptcha::display() !!}

<br>
                    <button class="btn btn-success" name="submitContact"  type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>



@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection