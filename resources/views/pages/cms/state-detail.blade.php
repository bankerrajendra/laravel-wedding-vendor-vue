@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('navclasses')background-black outer-nav  @endsection
@section('content')
    <section class="city-date">
        <div class="container">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 hidden-xs">
                            <h3>Indian Matrimony In {{$cmsdetail[0]->type_name}}</h3>
                            <h5><b>Free Online Matrimony Site for Indian Men & Women</b></h5>
                            <p class="text-justify">{{$cmsdetail[0]->page_description}}</p>
                        </div>
                        <div class="col-sm-6 col-sm-offset-3">
                            {{--@include('partials.register')--}}
                        </div>
                        <div class="col-sm-8 col-sm-offset-2 hidden-lg hidden-md hidden-sm panel-body">
                            <h3>Indian Matrimony In {{$cmsdetail[0]->type_name}}</h3>
                            <h5><b>Free Online Matrimony Site for Indian Men & Women</b></h5>
                            <p class="text-justify">{{$cmsdetail[0]->page_description}}</p>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection