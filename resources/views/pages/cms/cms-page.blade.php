@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('navclasses') background-black outer-nav  @endsection

@section('content')
    <!--Start Banner-->

                    {!! $pageinfo->page_description !!}

    @if ($slug=="about-us")
        <!--Start Section-->
        <section class="block_2 hidden-xs con">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="slog_txt text-center">
                            <h2>Latest Profiles</h2>
                            <img src="img/line.png">
                        </div>
                        <div class="text-center">
                            <p>Please log in to find out more and search for your perfect partner.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @php $count = 0; @endphp
                    @foreach($featuredUsers as $featuredUser)
                        <div class="col-sm-3 panel-body">
                            <a href="#">
                                <div class="success">
                                    <img src="{{$featuredUser->getVendorProfilePic()}}" alt="" class="image">
                                    <div class="overlay">
                                        <div class="text">{{$featuredUser->name}} {{$featuredUser->getAge()}} {{$featuredUser->city->name}} <br> @if( $featuredUser->gender=='M') Male @else Female @endif</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @php $count++; @endphp
                    @endforeach
                    @if($count < 4)
                        @for($i = $count; $i < 4; $i++)
                            @if($i==0)
                                <div class="col-sm-3 panel-body">
                                    <a href="#">
                                        <div class="success">
                                            <img src="{{asset('img/demo.jpg')}}" alt="" class="image">
                                            <div class="overlay">
                                                <div class="text">Jason 35 Vancouver <br> Male</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                            @if($i==1)
                                <div class="col-sm-3 panel-body">
                                    <a href="#">
                                        <div class="success">
                                            <img src="{{asset('img/demo1.jpg')}}" alt="" class="image">
                                            <div class="overlay">
                                                <div class="text">Harish 25 Delhi <br> Male</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                            @if($i==2)
                                <div class="col-sm-3 panel-body">
                                    <a href="#">
                                        <div class="success">
                                            <img src="{{asset('img/demo2.jpg')}}" alt="" class="image">
                                            <div class="overlay">
                                                <div class="text">Kevin 30 Toronto <br> Male</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                            @if($i==3)
                                <div class="col-sm-3 panel-body">
                                    <a href="#">
                                        <div class="success">
                                            <img src="{{asset('img/demo3.jpg')}}" alt="" class="image">
                                            <div class="overlay">
                                                <div class="text">Merry 25 Texas <br> Female</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        @endfor
                    @endif

                </div>
            </div>
        </section>
        <!--End Section-->

    @endif
        {{--if($slug=="about-us") {--}}
    {{--$featuredUsers = $userInfoRepo->getFeaturedUsers();--}}
    {{--}--}}


@endsection
