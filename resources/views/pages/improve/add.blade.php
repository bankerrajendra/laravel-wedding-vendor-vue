@extends('layouts.app')
@section('template_title')
    How can we improve?
@endsection
@section('head')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6">
                <div class="register">
                    <div class="panel">
                        <h3>Provide Us Your Suggestions</h3>
                        @if(session('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <form role="form" method="POST" action="{{ route('handle-improve-submit') }}" name="add-improve" id="add-improve">
                    @csrf
                        <div class="form-group ">
                            <label>Subject</label>
                            <input type="text" name="subject" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" name="message" style="height:100px;"></textarea>
                        </div>
                    
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-warning" value="SUBMIT">
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection