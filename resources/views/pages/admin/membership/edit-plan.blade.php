@extends('adminlte::page')
@section('title', 'Edit Plan Record')
@section('content_header')
    <h1>Edit Plan Record</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

{{--@section('template_title')--}}
{{--@lang('usersmanagement.editing-user', ['name' => $user->name])--}}
{{--@endsection--}}

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection
<style type="text/css">
    table.add-package-tbl input.form-control {
        min-width: 100px;
        width: 60px;
    }
    .has-feedback table.add-package-tbl .form-control {
        padding-right: 5px;
    }
</style>

@section('content')
    {{--<div class="container">--}}
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>



                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('post-edit-plan'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline', 'id' => 'frm-edit-membership-plan')) !!}

                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> Plan Name</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="hidden" name="id" id="id" value="<?php echo $planRec['id']; ?>">
                                        <input type="text" name="plan_name" id="plan_name" value="<?php echo $planRec['plan_name']; ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="status" class="col-md-3 control-label"> Plan Type</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="plan_type" id="plan_type" >
                                            <option value="Free" <?php echo ($planRec['plan_type']=='Free'?'selected':''); ?>>Free</option>
                                            <option value="Paid" <?php echo ($planRec['plan_type']=='Paid'?'selected':''); ?>>Paid</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group has-feedback row paid-plan" <?php echo ($planRec['plan_type'] == 'Paid' ? 'style="display:block;"' : 'style="display:none;"'); ?>>
                                <label for="status" class="col-md-3 control-label"> Packages</label>
                                <div class="col-md-9">
                                    <!-- Add Packages -->
                                    <table class="table table-bordered add-package-tbl" id="package_item_table">
                                        <tr>
                                            <th>Amount</th>
                                            <th>Duration</th>
                                            <th>Type</th>
                                            <th>With Banner</th>
                                            <th>Status</th>
                                            <th>
                                                <button type="button" name="add-pckg" class="btn btn-success btn-sm add-pckg"><span class="glyphicon glyphicon-plus"></span></button>
                                            </th>
                                        </tr>
                                        @if(!empty($planPack))
                                            @foreach($planPack as $package)
                                                <tr>
                                                    <td><input type="text" name="package_amount[]" class="form-control package_amount" value="{{$package->amount}}" /></td>
                                                    <td><input type="text" name="package_duration[]" class="form-control package_duration" value="{{$package->duration}}" /></td>
                                                    <td>
                                                        <select name="package_type[]" class="form-control package_type">
                                                            <option value="Days" @if($package->type == "Days") selected="selected" @endif >Days</option>
                                                            <option @if($package->type == "Month") selected="selected" @endif  value="Month">Month</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="with_banner[]" class="form-control with_banner">
                                                            <option value="0" @if($package->with_banner == "0") selected="selected" @endif >No</option>
                                                            <option @if($package->with_banner == "1") selected="selected" @endif  value="1">Yes</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="package_status[]" class="form-control package_status">
                                                            <option value="A" @if($package->status == "A") selected="selected" @endif >Active</option>
                                                            <option value="ADMIN" @if($package->status == "ADMIN") selected="selected" @endif >Admin</option>
                                                            <option @if($package->status == "I") selected="selected" @endif  value="I">Inactive</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <button type="button" name="remove-pckg" class="btn btn-danger btn-sm remove-pckg"><span class="glyphicon glyphicon-minus"></span></button>
                                                    </td>
                                                    <input type="hidden" name="package_id[]" value="{{$package->id}}" />
                                                </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                    <!-- Add Packages -->
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> Photos Allowed</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="text" name="public_photos_allowed" id="public_photos_allowed" value="<?php echo $planRec['public_photos_allowed']; ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> Message Allowed</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="text" name="msg_allowed" id="msg_allowed" value="<?php echo $planRec['msg_allowed']; ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> Membership Benefits</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <?php $membership_benefitsArr=explode(',',$planRec['membership_benefits']);  ?>
                                        @foreach($benefits as $benefit)
                                            <input type="checkbox" name="membership_benefits[]" id="membership_benefits" <?php echo in_array($benefit['id'], $membership_benefitsArr)?"checked":""; ?> value="{{@$benefit['id']}}" > {{@$benefit['name']}} <br>
                                        @endforeach
                                        <br>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="status" class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="status" id="status" >
                                            <option value="A" <?php echo ($planRec['status']=='A'?'selected':''); ?> >Active</option>
                                            <option value="I" <?php echo ($planRec['status']=='I'?'selected':''); ?> >Inactive</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}

    @include('modals.modal-save')
    @include('modals.modal-delete')

@endsection

@section('js')
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @include('scripts.check-changed')
    <script type="text/javascript" language="javascript">
        $(document).on('click', '.add-pckg', function(){
            var html = '';
            html += '<tr>';
            html += '<td><input type="text" name="package_amount[]" class="form-control package_amount" /></td>';
            html += '<td><input type="text" name="package_duration[]" class="form-control package_duration" /></td>';
            html += '<td><select name="package_type[]" class="form-control package_type"><option value="Days">Days</option><option value="Month">Month</option></select></td>';
            html += '<td><select name="with_banner[]" class="form-control with_banner"><option value="0">No</option><option value="1">Yes</option></select></td>';
            html += '<td><select name="package_status[]" class="form-control package_status"><option value="A">Active</option><option value="ADMIN">Admin</option><option value="I">Inactive</option></select></td>';
            html += '<td><button type="button" name="remove-pckg" class="btn btn-danger btn-sm remove-pckg"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
            $('#package_item_table').append(html);
        });

        $(document).on('click', '.remove-pckg', function(){
            $(this).closest('tr').remove();
        });
        $('#plan_type').on('change', function(event){
            if($(this).val() == 'Free') {
                // hide paid
                $('.paid-plan').hide();
            } else {
                // show paid
                $('.paid-plan').show();
            }
        });

    </script>


    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
