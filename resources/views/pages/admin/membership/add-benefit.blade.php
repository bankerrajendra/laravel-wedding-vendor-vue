@extends('adminlte::page')
@section('title', 'Add Benefit Record')
@section('content_header')
    <h1>Add Benefit Record</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>



                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('update-benefit'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> Benefit Detail</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('name', '', array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Benefit Description')) !!}
                                    </div>
                                </div>
                            </div>

                            <!-- Image -->
                            <div class="form-group has-feedback row ">
                                <label for="status" class="col-md-3 control-label"> Icon Image</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="file" class="form-control" name="icon_image" />
                                    </div>
                                    <small>allowed extensions: <i>jpeg,jpg,png,gif,ico</i>, Image size max: <i>1 MB</i>, Dimensions max: <i>100px x 100px</i></small>

                                </div>
                            </div>
                            <!-- Image -->

                            <div class="form-group has-feedback row ">

                                <label for="status" class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="status" id="status" >
                                            <option value="A" selected="selected">Active</option>
                                            <option value="I" >Inactive</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
