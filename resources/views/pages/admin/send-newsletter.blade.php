@extends('adminlte::page')
@section('title', 'Send News Letter to Members')
@section('content_header')
    <h1>Send News Letter to Members</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <link href="{{ asset('css/select-box.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-send-newsletter-submit'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            <div class="form-group has-feedback row ">
                                <label for="title" class="col-md-3 control-label"> Status</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="status" value="All" checked="checked" />
                                            All
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="status" value="Active" />
                                            Active
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="status" value="Inactive" />
                                            In Active
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="status" value="Paid" />
                                            Paid
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="status" value="Expired" />
                                            Expired
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="status" value="Subscribers" />
                                            Subscribers
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Members -->
                            <div class="form-group has-feedback row ">
                                <label for="members" class="col-md-3 control-label"> Members</label>
                                <div class="col-md-9">
                                    <i class='chosen_loader'>loading...</i>
                                    <select style="display:none;" name="members[]" multiple class="chosen-select form-control" id="members">
                                        <option value="">Select</option>
                                        <option value="all">All</option>
                                        @if(count($all_users) > 0)
                                            @foreach($all_users as $user)
                                                <option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}} ({{$user->email}})</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <!-- Members -->

                            <!-- News Letters -->
                            <div class="form-group has-feedback row ">
                                <label for="newsletter" class="col-md-3 control-label"> News Letters</label>
                                <div class="col-md-9">
                                    <select name="newsletter" class="form-control" id="newsletter">
                                        <option value="">Select</option>
                                        @if(count($news_letters) > 0)
                                            @foreach($news_letters as $news_letter)
                                                <option value="{{$news_letter->id}}">{{$news_letter->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <!-- News Letters -->

                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-3">
                                    {!! Form::button('Submit', array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2','type' => 'submit')) !!}
                                </div>
                                <div class="col-md-3">
                                    <a href="/admin/templates/news-letter/list" class="btn btn-info margin-bottom-1 mt-3 mb-2" title="">Back</a>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('js/select-box.js') }}"></script>
    <script type="text/javascript">
    jQuery(document).ready(function(jQ) {
        $(".chosen_loader").hide();
        $('input[name="status"]').on('change', function() {
            if($(this).val() != '')
            {
                $(".chosen_loader").show();
                $(".chosen-choices").hide();
                var status = $(this).val();
                var dependent = 'members';
                var _token = $('input[name="_token"]').val();
                var output;
                $.ajax({
                    url:"{{ route('ajax-show-members-newsletter-send') }}",
                    method:"POST",
                    data:{status:status, _token:_token},
                    success:function(result)
                    {   
                        // enable the dropdown
                        jQ('#'+dependent).prop('disabled', false);
                        // remove the old options
                        jQ('#'+dependent+' option:gt(0)').remove();
                        // add all
                        if(result.length > 1) {
                            jQ('#'+dependent).append(jQ('<option></option>').attr('value', 'all').text("All"));
                        }
                        for(var i=0; i<result.length; i++){
                            if(status == "Subscribers") {
                                jQ('#'+dependent).append(jQ('<option></option>').attr('value', result[i].id).text(result[i].email));
                            } else {
                                jQ('#'+dependent).append(jQ('<option></option>').attr('value', result[i].id).text(result[i].first_name+" "+result[i].last_name+" ("+result[i].email+")"));
                            }
                        }
                        // update Chosen
                        jQ('#'+dependent).trigger("chosen:updated");
                        $(".chosen_loader").hide();
                        $(".chosen-choices").show();
                    }
                });
            }
        });
        jQ('.chosen-select').chosen( {
            width : '400px'
        } );

        /** updating select box if current value = DM **/
        $(".chosen-select").on("change", function(e, val){
				
                var target = $(this);
                var value = $(this).val();
				/** 
					@ earlier used no-filter attribute is no longer functional here
					@ chosen lib will fire event on every multi dropdown now
					@ including lifestyles, country/city/state etc
				**/
				var qg = [];
				var ng = [];
				
				/** exclude data other than all **/
				if(value!==null) {
                    qg = value.filter(function (value, index, arr) {
                        return value !== "all";
                    });
                }
				
				/** Make all only in selection **/
                if(value!==null) {
                    ng = value.filter(function (value, index, arr) {
                        return value == "all";
                    });
                }
				/** 
				  @ val.selected returns only single value of item clicked recently 
				  @ Logic : val.selected gives last item checked and if its all then we simply put {ng} else{qg}
				**/
				if(val.selected == "all"){
					var filtered = ng;
				}else{
					var filtered = qg;
				}
  
				/** Updating choosen dropdown with modified data **/
                $(target).val(filtered);
                $(target).trigger("chosen:updated");
            
        })
        /** @end **/


    }); 
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
