@extends('adminlte::page')
@section('title', 'Update Logo & Favicon')
@section('content_header')
    <h1>Update Logo & Favicon</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-logo-favicon-submit'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline', 'enctype' => 'multipart/form-data')) !!}
                            <!-- Logo -->
                            <div class="form-group has-feedback row ">

                                <label for="site_logo" class="col-md-3 control-label"> Site Logo</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="file" class="form-control" name="site_logo" id="site_logo" />
                                    </div>
                                    <small>allowed extensions: <i>jpeg, jpg, png, gif, bmp</i> Image size max: <i>2 MB</i>, Dimensions max: <i>500px x 500px</i></small>

                                    @if($logo != "")
                                        <p>
                                            <img src="{{asset('img/'.$logo->value)}}" alt="logo" width="240" border="0" />
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <!-- Logo -->
                            <!-- Favicon -->
                            <div class="form-group has-feedback row ">

                                <label for="site_favicon" class="col-md-3 control-label"> Site Favicon</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="file" class="form-control" name="site_favicon" id="site_favicon" />
                                    </div>
                                    <small>allowed extensions: <i>jpeg, jpg, png, gif, bmp, ico</i> Image size max: <i>1 MB</i>, Dimensions max: <i>100px x 100px</i></small>

                                    @if($favicon != "")
                                        <p>
                                            <img src="{{asset('img/'.$favicon->value)}}" alt="icon" width="20" border="0" />
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
