@extends('adminlte::page')
@section('title', 'Site Email')
@section('content_header')
    <h1>Site Email</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-site-email-submit'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            <div class="form-group row ">

                                <label for="site_email" class="col-md-3 control-label"> Site Email</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('site_email', $site_email, array('id' => 'site_email', 'class' => 'form-control', 'placeholder' => 'Email')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_from_email" class="col-md-3 control-label"> From Email</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('site_from_email', $site_from_email, array('id' => 'site_from_email', 'class' => 'form-control', 'placeholder' => 'Email')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_to_email" class="col-md-3 control-label"> To Email</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('site_to_email', $site_to_email, array('id' => 'site_to_email', 'class' => 'form-control', 'placeholder' => 'Email')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
