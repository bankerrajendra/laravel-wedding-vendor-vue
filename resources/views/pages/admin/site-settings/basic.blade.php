@extends('adminlte::page')
@section('title', 'Site Basic Settings')
@section('content_header')
    <h1>Site Basic Settings</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
        .disp-blc {
            display: block;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-site-basic-settings-submit'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            <div class="form-group row ">

                                <label for="site_url" class="col-md-3 control-label"> Web Name (Website URL)</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('site_url', $site_url, array('id' => 'site_url', 'class' => 'form-control', 'placeholder' => 'Website URL')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_contact_telephone" class="col-md-3 control-label"> Website Contact Telephone number</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('site_contact_telephone', $site_contact_telephone, array('id' => 'site_contact_telephone', 'class' => 'form-control', 'placeholder' => 'Site Contact Telephone')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_name" class="col-md-3 control-label"> Website Name</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('site_name', $site_name, array('id' => 'site_name', 'class' => 'form-control', 'placeholder' => 'Website Name')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_title" class="col-md-3 control-label"> Website Title</label>

                                <div class="col-md-9">
                                    <div class="input-group" style="display: block;">
                                        {!! Form::text('site_title', $site_title, array('id' => 'site_title', 'class' => 'form-control', 'placeholder' => 'Website Title')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_keywords" class="col-md-3 control-label"> Website Keywords</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <textarea class="form-control" placeholder="Website Keywords" cols="105" rows="7" name="site_keywords" id="site_keywords">{{$site_keywords}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_description" class="col-md-3 control-label"> Website Description</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <textarea class="form-control" placeholder="Website Description" cols="105" rows="7" name="site_description" id="site_description">{{$site_description}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_footer_text" class="col-md-3 control-label"> Footer Text</label>

                                <div class="col-md-9">
                                    <div class="input-group " style="display: block;">
                                        {!! Form::text('site_footer_text', $site_footer_text, array('id' => 'site_footer_text', 'class' => 'form-control', 'placeholder' => 'Footer Text')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_contact_no" class="col-md-3 control-label"> Contact No</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('site_contact_no', $site_contact_no, array('id' => 'site_contact_no', 'class' => 'form-control', 'placeholder' => 'Contact No')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_postal_address" class="col-md-3 control-label"> Full Address</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <textarea class="form-control" placeholder="Full Address" cols="105" rows="7" name="site_postal_address" id="site_postal_address">{{$site_postal_address}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_google_analytics_code" class="col-md-3 control-label"> Google Analytics Code</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <textarea class="form-control" placeholder="Google Analytics Code" cols="105" rows="7" name="site_google_analytics_code" id="site_google_analytics_code">{{$site_google_analytics_code}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label class="col-md-3 control-label"> Tax Applicabel</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="site_tax_applicable" value="1" @if($site_tax_applicable == "1")checked="checked"@endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="site_tax_applicable" value="0" @if($site_tax_applicable == "0")checked="checked"@endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">

                                <label for="site_tax_name" class="col-md-3 control-label"> Tax Name</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('site_tax_name', $site_tax_name, array('id' => 'site_tax_name', 'class' => 'form-control', 'placeholder' => 'Tax Name')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_tax_percentage" class="col-md-3 control-label"> Service Tax (%)</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('site_tax_percentage', $site_tax_percentage, array('id' => 'site_tax_percentage', 'class' => 'form-control', 'placeholder' => 'Service Tax (%)')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_timezone" class="col-md-3 control-label"> Timezone</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="form-control" id="site_timezone" name="site_timezone">
                                            <option value="">Select Timezone</option>
                                            @include('partials.admin.timezone-dd-options')
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
