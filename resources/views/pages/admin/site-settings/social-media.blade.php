@extends('adminlte::page')
@section('title', 'Social Media Link')
@section('content_header')
    <h1>Social Media Link</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-social-media-submit'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            <div class="form-group row ">

                                <label for="site_facebook_link" class="col-md-3 control-label"> Facebook Link</label>

                                <div class="col-md-9">
                                    <div class="input-group" style="display: block;">
                                        {!! Form::text('site_facebook_link', $site_facebook_link, array('id' => 'site_facebook_link', 'class' => 'form-control', 'placeholder' => 'Facebook Link')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="site_twitter_link" class="col-md-3 control-label"> Twitter Link</label>

                                <div class="col-md-9">
                                    <div class="input-group" style="display: block;">
                                        {!! Form::text('site_twitter_link', $site_twitter_link, array('id' => 'site_twitter_link', 'class' => 'form-control', 'placeholder' => 'Twitter Link')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">

                                <label for="site_linkedin_link" class="col-md-3 control-label"> Linkedin Link</label>

                                <div class="col-md-9">
                                    <div class="input-group" style="display: block;">
                                        {!! Form::text('site_linkedin_link', $site_linkedin_link, array('id' => 'site_linkedin_link', 'class' => 'form-control', 'placeholder' => 'Linkedin Link')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">

                                <label for="site_google_link" class="col-md-3 control-label"> Google Link</label>

                                <div class="col-md-9">
                                    <div class="input-group" style="display: block;">
                                        {!! Form::text('site_google_link', $site_google_link, array('id' => 'site_google_link', 'class' => 'form-control', 'placeholder' => 'Google Link')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">

                                <label for="site_youtube_link" class="col-md-3 control-label"> Youtube Link</label>

                                <div class="col-md-9">
                                    <div class="input-group" style="display: block;">
                                        {!! Form::text('site_youtube_link', $site_youtube_link, array('id' => 'site_youtube_link', 'class' => 'form-control', 'placeholder' => 'Youtube Link')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">

                                <label for="site_instagram_link" class="col-md-3 control-label"> Instagram Link</label>

                                <div class="col-md-9">
                                    <div class="input-group" style="display: block;">
                                        {!! Form::text('site_instagram_link', $site_instagram_link, array('id' => 'site_instagram_link', 'class' => 'form-control', 'placeholder' => 'Instagram Link')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
