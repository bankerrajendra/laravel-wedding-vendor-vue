@extends('adminlte::page')
@section('title')
    Showing Event's Reviews
@endsection
@section('content_header')
    <h1>Event's Reviews</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
        .trf {
            transform: rotate(61deg);
            -ms-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            /* -webkit-transform: rotate(90deg); */
            -o-transform: rotate(90deg);
            margin-top: 20px;
        }
        .mb15 {
            margin-bottom: 15px;
        }
        .mb25 {
            margin-bottom: 25px;
        }
        .ml15 {
            margin-left: 15px;
        }
    </style>
@endsection

@section('content')
<div class="main-content">
	<div class="panel mb25">
    	<div class="panel-body">
        	<div class="row">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            	<form method="get" action="{{ route('admin-events-reviews-list') }}" name="search-action-frm">
                    {!! Form::hidden('status', $status) !!}
                    <div class="panel col-sm-12">
                        <a href="{{ route('admin-events-reviews-list') }}" class="btn btn-primary mb15 ml15"><i class="fa fa-list fa-fw"></i>&nbsp;&nbsp;All ({{$all_records}})&nbsp;</a>
                        <a href="{{ route('admin-events-reviews-list') }}?status=1" class="btn btn-success mb15 ml15"><i class=" fa fa-thumbs-up text-success "></i>&nbsp;&nbsp;Approved List ({{$approved_records}})&nbsp;</a>
                        <a href="{{ route('admin-events-reviews-list') }}?status=0" class="btn btn-warning mb15 ml15"><i class=" fa fa-thumbs-down text-warning "></i>&nbsp;&nbsp;Un Approved List ({{$unaprroved_records}})&nbsp;</a>
                        <hr style="border:1px solid rgba(0,0,0,.06);height:0px;margin-bottom:0px">
                    </div>
                    <div class=" col-sm-5 mb15 ml15">
                        <label for="cat-fltr">Filter by Event</label>&nbsp;
                        <select name="event" class="form-control" id="cat-fltr">
                            <option value="">All</option>
                            @if($events != null)
                                @foreach ($events as $event_single)
                                    @if($event_single->id  == $event)
                                        <option value="{{$event_single->id}}" selected="selected">{{$event_single->event_name}}</option>
                                    @else
                                        <option value="{{$event_single->id}}">{{$event_single->event_name}}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <a id="action-delete" href="#" class="btn btn-danger mb15 ml15"><i class="fa fa-trash fa-fw"></i>&nbsp;&nbsp;Delete&nbsp;</a>
                            
                        <a id="action-active" href="#" class="btn btn-success mb15 ml15"><i class="fa fa-thumbs-up text-success"></i>&nbsp;&nbsp;Make Approved&nbsp;</a>
                                    
                        <a id="action-inactive" href="#" class="btn btn-warning mb15 ml15"><i class="fa fa-thumbs-down text-warning"></i>&nbsp;&nbsp;Make Un Approved&nbsp;</a>
                    </div>
                    <div class="col-sm-3">
                        <label>
                            Show
                        </label>
                        <label>
                            <select name="per_page" id="per_page" class="form-control">
                                <option @if($per_page == "1")selected="selected"@endif value="1">1</option>
                                <option @if($per_page == "2")selected="selected"@endif value="2">2</option>
                                <option @if($per_page == "3")selected="selected"@endif value="3">3</option>
                                <option @if($per_page == "5")selected="selected"@endif value="5">5</option>
                                <option @if($per_page == "10")selected="selected"@endif value="10">10</option>
                                <option @if($per_page == "20")selected="selected"@endif value="20">20</option>
                                <option @if($per_page == "50")selected="selected"@endif value="50">50</option>
                                <option @if($per_page == "100")selected="selected"@endif value="100">100</option>
                            </select>
                        </label>
                        <label>
                            entries
                        </label>                    
                    </div>
                    <div class="col-sm-2">
                        
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-4 input-group mb15">
                        <input type="search" name="search" id="search_field" class="form-control clearable x" value="{{$search}}" placeholder="Search here..">
                        <span class="input-group-btn pr10">
                        <button type="submit" class="btn btn-primary">Search</button>
                        </span>
                    </div>
                </form>
            </div>
            
            <div class="row">
            	<div class="col-sm-12">
                    <div class="table-responsive">
                    {!! Form::open(array('route' => array('handle-manage-bulk-event-reviews'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'name' => 'bulk-action-frm')) !!}
                    {!! Form::hidden('action', '', array('id' => 'action')) !!}
                        <table class="table table-bordered bordered table-striped table-condensed datatable">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="cb-checkbox mb0" id="all_check"><span class="cb-inner"><i><input class="all_check " type="checkbox" id="all"></i></span>
                                        </label>
                                    </th>
                                    <th>View</th>
                                    <th>Edit</th>
                                    <th>Status</th>
                                    <th style="width:40%;">Message</th>
                                    <th style="max-width:20%;min-width:15%">Rating</th>
                                    <th style="max-width:20%;min-width:15%">User</th>
                                    <th style="max-width:20%;min-width:15%">Event</th>
                                    <th style="max-width:20%;min-width:15%">Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @if($reviews->total() > 0 && !empty($reviews))
                                        @foreach($reviews as $review)
                                        <tr>
                                            <td class="checkbox-row"><label class="cb-checkbox"><span class="cb-inner"><i><input type="checkbox" class="review_ids" name="review_ids[]" value="{{$review->id}}"></i></span></label>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin-events-reviews-view', [$review->id]) }}"><i class="fa fa-eye fa-fw"></i></a>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin-events-reviews-edit', [$review->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                                            </td>
                                            <td>
                                                <i style="font-size: 25px;" class="fa fa-thumbs-<?php if($review->approved == "1"): echo 'up text-success'; else: echo 'down text-warning'; endif?>"></i>
                                            </td>
                                            <td>
                                                {{$review->message}}
                                            </td>
                                            <td>{{$review->rating}}</td>
                                            <td>
                                                @if($review->user != null)
                                                    <a href="{{ URL::to('users/' . $review->user_id . '/edit') }}"><img src="{{$review->user->getVendorProfilePic()}}" alt="" width="100"></a><br />{{$review->user->first_name}} {{$review->user->last_name}}
                                                @else
                                                    UserID: {{$review->user_id}}
                                                @endif
                                            </td>
                                            <td><a href="{{ URL::to('events/' . $review->event_id . '/edit') }}"><img src="{{@$review->event->getProfilePic()}}" alt="" width="100"></a><br /><a href="{{ URL::to('events/' . $review->event_id . '/edit') }}">{{@$review->event->event_name}}</a></td>
                                            <td>{{ date("F d, Y h:i A", strtotime($review->created_at))}}</td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                        </table>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div>
            	<div class="pull-left">
                    <div id="show_record_message">
                        Showing {{$reviews->total()}} entries
                    </div>
                </div>
                <div class="pull-right">
                    {{ $reviews->appends(['search' => $search, 'per_page' => $per_page, 'status' => $status, 'event' => $event])->links() }}               
                </div>
    	    </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript">
        $('select[name="event"]').on('change', function() {
            window.location.href = "{{route('admin-events-reviews-list')}}?event="+this.value;
        });
        $('#per_page').on('change', function() {
            $("form[name='search-action-frm']").submit();
        });
        $('.all_check').click(function () {    
            $('input:checkbox').prop('checked', this.checked);    
        });
        $('#action-delete').on('click', function() {
            bulkActionTemplateSubmit('delete','Are you sure you want to delete selected entries!');
        });
        $('#action-active').on('click', function() {
            bulkActionTemplateSubmit('1','Are you sure you want to make approve selected entries!');
        });
        $('#action-inactive').on('click', function() {
            bulkActionTemplateSubmit('0','Are you sure you want to make un approve selected entries!');
        });
        function bulkActionTemplateSubmit(action_val, message) {
            if($('input[name="review_ids[]"]:checked').length > 0) {
                if(confirm(message)) {
                    $('#action').val(action_val);
                    $("form[name='bulk-action-frm']").submit();
                }
            } else {
                alert('Select atleast one entry.');
            }
        }
    </script>
@endsection
