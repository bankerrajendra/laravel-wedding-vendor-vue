@extends('adminlte::page')
@section('title', 'Edit Review')
@section('content_header')
    <h1>Edit Review</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-edit-event-review'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            {!! Form::hidden('id', $review->id) !!}
                            <div class="form-group has-feedback row ">

                                <label for="message" class="col-md-3 control-label"> Message</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {{$review->message}}
                                    </div>
                                </div>
                            </div>
                            <!-- Rating -->
                            <div class="form-group has-feedback row ">
                                <label for="rating" class="col-md-3 control-label"> Rating</label>
                                <div class="col-md-9"><strong>{{$review->rating}}</strong></div>
                            </div>
                            <!-- Rating -->

                            <!-- Status -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="approved" value="1" @if($review->approved == "1")checked="checked"@endif>
                                            Approve
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="approved" value="0" @if($review->approved == "0")checked="checked"@endif>
                                            Un Approve
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- Status -->

                            <!-- User -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> User</label>

                                <div class="col-md-9">
                                    <a href="{{ URL::to('users/' . $review->user_id . '/edit') }}"><img src="{{$review->user->getVendorProfilePic()}}" alt="" width="100"></a><br />
                                    <strong>{{$review->user->first_name}} {{$review->user->last_name}}</strong>
                                </div>
                            </div>
                            <!-- User -->

                            <!-- Event -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> Event</label>

                                <div class="col-md-9">
                                    <a href="{{ URL::to('events/' . $review->event_id . '/edit') }}"><img src="{{@$review->event->getProfilePic()}}" alt="" width="100"></a><br />
                                    <strong><a href="{{ URL::to('events/' . $review->event_id . '/edit') }}">{{@$review->event->event_name}}</a></strong>
                                </div>
                            </div>
                            <!-- Event -->

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
