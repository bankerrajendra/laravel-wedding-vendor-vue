@extends('adminlte::page')
@section('title', 'Add Vendor FAQ')
@section('content_header')
    <h1>Add Vendor FAQ</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-add-vendor-faq'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}

                            <div class="form-group row ">

                                <label for="question" class="col-md-3 control-label"> Question <span class="text-danger">*</span></label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('question', '', array('id' => 'question', 'class' => 'form-control', 'placeholder' => 'Question')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">
                                <label for="categories" class="col-md-3 control-label"> Select Categories <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="categories-select2" id="satellite_ids" style="width:600px" name="categories[]" multiple="multiple">
                                            @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Answer Type <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="answer_type" value="text" checked="checked">
                                            Textbox
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="answer_type" value="text_area">
                                            Textarea
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="answer_type" value="radio">
                                            Radio
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="answer_type" value="checkbox">
                                            Checkbox
                                        </label>
                                    </div>
                                </div>
                            </div>
                            {{--Order--}}
                            <div class="form-group row ">

                                <label for="order" class="col-md-3 control-label"> Order <span class="text-danger">*</span></label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('order', '', array('id' => 'order', 'class' => 'form-control', 'placeholder' => 'Order')) !!}
                                    </div>
                                </div>
                            </div>
                            {{--Order--}}
                            {{-- For Radio and Checkbox options --}}
                            <div class="form-group row" style="display: none;" id="for-options">
                                <label for="answer_options" class="col-md-3 control-label"> Answer Options <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <textarea name="options" id="answer_options" cols="90" rows="5" placeholder="Enter Multiple Option comma seperated like Option1, Option2, Option3..."></textarea>
                                    </div>
                                </div>
                            </div>
                            {{-- For Text and Textarea placeholder --}}
                            <div class="form-group row" style="display: block;" id="for-placeholder">
                                <label for="answer_placeholder" class="col-md-3 control-label"> Answer Placeholder </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('placeholder', '', array('id' => 'answer_placeholder', 'class' => 'form-control', 'placeholder' => 'Placeholder')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Status <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="status" value="1" checked="checked">
                                            Active
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="status" value="0">
                                            Inactive
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('input[type="radio"]').on('click', function() {
                if($(this).val() == 'radio' || $(this).val() == "checkbox") {
                    $("#for-options").show();
                    $("#for-placeholder").hide();
                } else {
                    $("#for-options").hide();
                    $("#for-placeholder").show();
                }
            });
            $('.categories-select2').select2({
                placeholder: "Select Category",
                allowClear: true
            });
        });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
