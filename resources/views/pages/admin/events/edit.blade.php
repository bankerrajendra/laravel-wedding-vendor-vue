@extends('adminlte::page')
@section('title', 'Edit Event >> '.$event->event_name)
@section('content_header')
<h1>Edit Event >> {{$event->event_name}}</h1>
@stop
@section('css')
    <link href="{{asset('css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/timepicki.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/cropper.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/main.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .avatar-btns, .avatar-btns-poster { text-align: center; }
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-admin-edit-event'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            {!! Form::hidden('id', $event->id) !!}
                            {!! Form::hidden('event_id', $event->getEncryptedId()) !!}
                            <div class="form-group row ">
                                <label for="event_name" class="col-md-3 control-label"> Name <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('event_name', $event->event_name, array('id' => 'event_name', 'class' => 'form-control', 'placeholder' => 'Name')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="venue" class="col-md-3 control-label"> Venue <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('venue', $event->venue, array('id' => 'venue', 'class' => 'form-control', 'placeholder' => 'Venue')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="artist" class="col-md-3 control-label"> Artist(s) </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <button class="add_artist_form_field"> 
                                            <span style="font-size:12px; font-weight: bold;">ADD + </span>
                                          </button>
                                        <div class="artist-container1">
                                            @php
                                                $artists_cnt = 0;
                                            @endphp
                                            @if($event->artists != '')
                                                @php
                                                    $expld_arts = explode(',', $event->artists);
                                                    $artists_cnt = count($expld_arts);
                                                @endphp
                                                @if(count($expld_arts) > 0)
                                                    @php
                                                        $i = 0;
                                                    @endphp
                                                    @foreach ($expld_arts as $art_id)
                                                        <div class="artist-div">
                                                            <div style="float: left; width: 78%;position: relative;"><input class="form-control artist-text" style="margin-bottom: 10px;" type="text" value="{{@$event->getArtistInfoBy('id', $art_id)->name}}" name="artists[]" data-element-id="{{$i+1}}" list="artist-select-{{$i+1}}">
                                                                <datalist id="artist-select-{{$i+1}}">
                                                                    @if($artists->count() > 0)
                                                                        <select>
                                                                            @foreach($artists as $artist)
                                                                            <option value="{{$artist->name}}"></option>
                                                                            @endforeach
                                                                        </select>
                                                                    @endif
                                                                </datalist>
                                                            </div>
                                                            &nbsp;<div style="float: right; width: 20%;"><a href="javascript:void(0);" class="delete">Delete</a></div>
                                                        </div>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @else
                                                <div class="artist-div">
                                                    <div style="float: left; width: 78%;position: relative;"><input class="form-control artist-text" style="margin-bottom: 10px;" type="text" name="artists[]" data-element-id="1" list="artist-select-1">
                                                        <datalist id="artist-select-1">
                                                            @if($artists->count() > 0)
                                                                <select>
                                                                    @foreach($artists as $artist)
                                                                    <option value="{{$artist->name}}"></option>
                                                                    @endforeach
                                                                </select>
                                                            @endif
                                                        </datalist>
                                                    </div>
                                                </div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="address" class="col-md-3 control-label"> Address <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('address', $event->address, array('id' => 'address', 'class' => 'form-control', 'placeholder' => 'Address')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="country" class="col-md-3 control-label"> Country <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="selectpicker form-control dynamic" data-dependent='state' data-show-subtext="true" data-live-search="true" id="country"  name="country">
                                            <option disabled="" >-- Please select an option --</option>
                                            @foreach (@$countryList as $country)
                                                <option value="{{ $country->id }}" {{ ( $country->id == @$event->country_id) ? 'selected' : '' }}> {{ $country->name }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="state" class="col-md-3 control-label"> State <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select name="state" id="state" class="form-control dynamic" data-dependent='city'>
                                            @foreach (getStateByCountry($event->country) as $state)
                                                <option value="{{ $state->id }}" {{ ( $state->id == @$event->state_id) ? 'selected' : '' }}> {{ $state->name }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="city" class="col-md-3 control-label"> City <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select name="city" id="city" class="selectpicker  form-control dynamic" required="" aria-required="true">
                                            <option value="">Select City</option>
                                            @foreach(getCityByState($event->state) as $city)
                                                <option value="{{ $city->id }}" {{ $event->city_id == $city->id ? "selected":"" }}>{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="zip" class="col-md-3 control-label"> Zip <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('zip', $event->zip, array('id' => 'zip', 'class' => 'form-control', 'placeholder' => 'Zip Code')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="description" class="col-md-3 control-label"> Description <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="">
                                        <textarea class="form-control" name="description" id="description" rows="5">{{$event->description}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="ticket_information" class="col-md-3 control-label"> Ticket Information </label>
                                <div class="col-md-9">
                                    <div class="">
                                        <textarea class="form-control" name="ticket_information" id="ticket_information" rows="5">{{$event->ticket_information}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="categories" class="col-md-3 control-label"> Select Categories <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="form-group row ">
                                        @php
                                            $categories_id = unserialize($event->categories);
                                        @endphp
                                        @if(!empty($events_categories))
                                            @php
                                                $k = 1;
                                            @endphp
                                            @foreach ($events_categories as $category)
                                                @if($k % 12 == 1)
                                                    <div class="col-sm-4">
                                                @endif
                                                <div class="checkbox">
                                                    <label><input name="categories[]" type="checkbox" value="{{$category->id}}" @if(in_array($category->id, $categories_id)) checked="checked" @endif>{{$category->name}} </label>
                                                </div>
                                                @if($k % 12 == 0)
                                                    </div>
                                                @endif
                                                @php
                                                    $k++;
                                                @endphp
                                            @endforeach
                                            @if ($k % 12 != 1) </div> @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @php
                                $expl_event_sdate = explode('-', $event->start_date);
                                $event_start_date = strtotime(date('Y-m-d H:i:s', strtotime($event->start_date) ) );
                                $expl_event_edate = explode('-', $event->end_date);
                                $expld_end_time = explode(' ', $expl_event_edate[2]);
                                $haveEndTime = true;
                                if($expld_end_time[1] != "00:00:00") {
                                    $event_end_date = strtotime(date('Y-m-d H:i:s', strtotime($event->end_date) ) );
                                } else {
                                    $event_end_date = strtotime(date('Y-m-d', strtotime($event->end_date) ) );
                                    $haveEndTime = false;
                                }
                            @endphp
                            <div class="form-group row ">
                                <label for="start_date" class="col-md-3 control-label"> Start Date <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div id="filterDate">
                                        <!-- Datepicker as text field -->         
                                            <div class="input-group date" data-date-format="dd.mm.yyyy">
                                                <input name="start_date" id="start-date" type="text" class="form-control" placeholder="dd.mm.yyyy" value="{{$errors->any()?old('start_date'):date('d.m.Y', $event_start_date)}}" autocomplete="off">
                                                <div class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                                </div>
                                                @if ($errors->has('start_date'))
                                                    <span id="start_date-error" class="help-block error-help-block">{{ $errors->first('start_date') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="start_time_picker" class="col-md-3 control-label"> Start Time <span class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <div id="filterDate2"> 
                                        <div class="">
                                            <input type="text" class="form-control" id="start_time_picker" type="text" name="start_time" value="{{$errors->any()?old('start_time'):date('h:i A', $event_start_date)}}" placeholder="hh:mm am">
                                            @if ($errors->has('start_time'))
                                                <span id="start_time-error" class="help-block error-help-block">{{ $errors->first('start_time') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="end_date" class="col-md-3 control-label"> End Date <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div id="filterDate1">
                                        <!-- Datepicker as text field -->         
                                            <div class="input-group date" data-date-format="dd.mm.yyyy">
                                                <input name="end_date" id="end-date" type="text" class="form-control" placeholder="dd.mm.yyyy" value="{{$errors->any()?old('end_date'):date('d.m.Y', $event_end_date)}}" autocomplete="off">
                                                <div class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                                </div>
                                                @if ($errors->has('end_date'))
                                                    <span id="end_date-error" class="help-block error-help-block">{{ $errors->first('end_date') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="end_time_picker" class="col-md-3 control-label"> End Time </label>
                                <div class="col-md-4">
                                    <div id="filterDate2">
                                        <input type="text" class="form-control" id="end_time_picker" type="text" name="end_time" value="{{$errors->any()?old('end_time'):($haveEndTime ? date('h:i A', $event_end_date): '')}}" placeholder="hh:mm am">
                                        @if ($errors->has('end_time'))
                                            <span id="end_time-error" class="help-block error-help-block">{{ $errors->first('end_time') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="poster_image" class="col-md-3 control-label"> Poster Image </label>
                                <div class="col-md-9">
                                    <div class="crop-avatar-poster">
                                        <div class="poster-panel">
                                            <div class="poster-view" data-original-title="" title="" style="width:100%!important;margin: 0% auto 3%;">
                                                <span id="fileselector-poster">
                                                    @if($event->getPosterPic(true) != null)
                                                        <img src="{{ $event->getPosterPic(true) }}" class="img-responsive">
                                                    @else
                                                        <img src="{{ asset('img/banner-img1.png') }}" class="img-responsive">
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    @if($posterImg != null)
                                    <ul class="list-unstyled list-inline margin-top10">
                                        <?php if($posterImg->is_approved=='Y') { ?>
                                            <li><strong style="color:green">Approved</strong></li> 
                                        <?php } else if($posterImg->is_approved=='N') { ?>
                                        <li>
                                            <button class="btn btn-success btn-xs approve_photo" data-photoid="{{$posterImg->id}}">Approve</button>
                                        </li>
                                        <li>
                                            <button class="btn btn-warning btn-xs disapprove_photo" data-photoid="{{$posterImg->id}}">Disapprove</button>
                                        </li>
                                        <?php } else if($posterImg->is_approved=='D') { ?>
                                        <li>
                                            <button class="btn btn-warning btn-xs disapprove_photo" disabled>Disapproved</button><br> <strong>Reason: <?php echo $posterImg->disapprove_reason; ?></strong>
                                        </li>
                                        <?php } ?>
                                        <li>
                                            <button type="button" class="btn btn-danger btn-xs deleteImage" data-type="poster" data-id="{{$posterImg->id}}" >Delete</button>
                                        </li>
                                    </ul>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="poster_image" class="col-md-3 control-label"> Banner Image </label>
                                <div class="col-md-9">
                                    <div class="crop-avatar">
                                        <div class="photo-panel">
                                            <div class="avatar-view" data-original-title="" title="" style="width:100%!important;margin: 0% auto 3%;">
                                                <span id="fileselector-cover">
                                                    @if($event->getCoverPic(true) != null)
                                                        <img src="{{ $event->getCoverPic(true) }}" class="img-responsive">
                                                    @else
                                                        <img src="{{ asset('img/banner-img1.png') }}" class="img-responsive">
                                                    @endif
                                                </span>											
                                                <small>The ideal dimension is 1320*350 pixels.</small>
                                            </div>
                                        </div>
                                    </div>
                                    @if($coverImg != null)
                                    <ul class="list-unstyled list-inline margin-top10">
                                        <?php if($coverImg->is_approved=='Y') { ?>
                                            <li><strong style="color:green">Approved</strong></li> 
                                        <?php } else if($coverImg->is_approved=='N') { ?>
                                        <li>
                                            <button class="btn btn-success btn-xs approve_photo" data-photoid="{{$coverImg->id}}">Approve</button>
                                        </li>
                                        <li>
                                            <button class="btn btn-warning btn-xs disapprove_photo" data-photoid="{{$coverImg->id}}">Disapprove</button>
                                        </li>
                                        <?php } else if($coverImg->is_approved=='D') { ?>
                                        <li>
                                            <button class="btn btn-warning btn-xs disapprove_photo" disabled>Disapproved</button><br> <strong>Reason: <?php echo $coverImg->disapprove_reason; ?></strong>
                                        </li>
                                        <?php } ?>
                                        <li>
                                            <button type="button" class="btn btn-danger btn-xs deleteImage" data-type="cover" data-id="{{$coverImg->id}}" >Delete</button>
                                        </li>
                                    </ul>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="poster_image" class="col-md-3 control-label"> Logo Image </label>
                                <div class="col-md-9">
                                    <div class="crop-avatar-another">
                                        <div class="photo-panel">
                                            <div class="avatar-view-photo" data-original-title="" title=""  style="width:100%!important;margin: 0% auto 3%;">
                                                <span id="fileselector-profile">
                                                    @if($event->getProfilePic(true) != null)
                                                        <img src="{{ $event->getProfilePic(true) }}" class="img-responsive">
                                                    @else
                                                        <img src="{{ asset('img/dummy.png') }}" class="img-responsive">
                                                    @endif
                                                </span>	
                                                <small>Dimension is 500*500 pixels.</small>									
                                            </div>
                                        </div>
                                    </div>
                                    @if($profileImg != null)
                                    <ul class="list-unstyled list-inline margin-top10">
                                        <?php if($profileImg->is_approved=='Y') { ?>
                                            <li><strong style="color:green">Approved</strong></li> 
                                        <?php } else if($profileImg->is_approved=='N') { ?>
                                        <li>
                                            <button class="btn btn-success btn-xs approve_photo" data-photoid="{{$profileImg->id}}">Approve</button>
                                        </li>
                                        <li>
                                            <button class="btn btn-warning btn-xs disapprove_photo" data-photoid="{{$profileImg->id}}">Disapprove</button>
                                        </li>
                                        <?php } else if($profileImg->is_approved=='D') { ?>
                                        <li>
                                            <button class="btn btn-warning btn-xs disapprove_photo" disabled>Disapproved</button><br> <strong>Reason: <?php echo $profileImg->disapprove_reason; ?></strong>
                                        </li>
                                        <?php } ?>
                                        <li>
                                            <button type="button" class="btn btn-danger btn-xs deleteImage"  data-id="{{$profileImg->id}}" data-type="profile" >Delete</button>
                                        </li>
                                    </ul>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="ticket_url" class="col-md-3 control-label"> Ticket Url </label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('ticket_url', $event->ticket_url, array('id' => 'ticket_url', 'class' => 'form-control', 'placeholder' => 'Ticket Url')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="meta_keywords" class="col-md-3 control-label"> Meta Keywords </label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('meta_keywords', $event->meta_keywords, array('id' => 'meta_keywords', 'class' => 'form-control', 'placeholder' => 'Meta Keywords')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="video_url" class="col-md-3 control-label"> Video URL </label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('video_url', $event->video_url, array('id' => 'video_url', 'class' => 'form-control', 'placeholder' => 'Video URL')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="business_website" class="col-md-3 control-label"> Business Website </label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('business_website', $event->business_website, array('id' => 'business_website', 'class' => 'form-control', 'placeholder' => 'Business Website')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="facebook" class="col-md-3 control-label"> Facebook </label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('facebook', $event->facebook, array('id' => 'facebook', 'class' => 'form-control', 'placeholder' => 'Facebook')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="pinterest" class="col-md-3 control-label"> Pinterest </label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('pinterest', $event->pinterest, array('id' => 'pinterest', 'class' => 'form-control', 'placeholder' => 'Pinterest')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="twitter" class="col-md-3 control-label"> Twitter </label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('twitter', $event->twitter, array('id' => 'twitter', 'class' => 'form-control', 'placeholder' => 'Twitter')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="linkedin" class="col-md-3 control-label"> Linked In </label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('linkedin', $event->linkedin, array('id' => 'linkedin', 'class' => 'form-control', 'placeholder' => 'Linked In')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="instagram" class="col-md-3 control-label"> Instagram </label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('instagram', $event->instagram, array('id' => 'instagram', 'class' => 'form-control', 'placeholder' => 'Instagram')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Online Seat Selection </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="online_seat_selection" value="1" @if($event->online_seat_selection == '1') checked="checked" @endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="online_seat_selection" value="0" @if($event->online_seat_selection == '0') checked="checked" @endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Parking </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="parking" value="1" @if($event->parking == '1') checked="checked" @endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="parking" value="0" @if($event->parking == '0') checked="checked" @endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Food For Sale </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="food_for_sale" value="1" @if($event->food_for_sale == '1') checked="checked" @endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="food_for_sale" value="0" @if($event->food_for_sale == '0') checked="checked" @endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Drinks For Sale </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="drinks_for_sale" value="1" @if($event->drinks_for_sale == '1') checked="checked" @endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="drinks_for_sale" value="0" @if($event->drinks_for_sale == '0') checked="checked" @endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> BabySitting Services </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="babysitting_services" value="1" @if($event->babysitting_services == '1') checked="checked" @endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="babysitting_services" value="0" @if($event->babysitting_services == '0') checked="checked" @endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Status <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="is_approved" value="1" @if($event->is_approved == '1') checked="checked" @endif>
                                            Active
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="is_approved" value="0" @if($event->is_approved == '0') checked="checked" @endif>
                                            Inactive
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Cropping Model for Event Poster STARTED --}}
    <div class="crop-avatar-poster">
        <div class="modal fade" id="avatar-modal-poster" aria-hidden="true" aria-labelledby="avatar-modal-label-poster" role="dialog" tabindex="-1" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="avatar-form-poster" id="posterEventUploadForm" action="{{ route('ajax-upload-event-edit-poster') }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <input name="event_id" value="{{$event->getEncryptedId()}}" type="hidden" />
                        <input type="hidden" value="" name="codeprofile">
                        <input type="hidden" name="allowed_photos" value='1'>
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                            <br>
                            <h4 class="text-center">Upload Poster</h4>
                            <hr>
                            <div class="avatar-body-poster">
                                <div class="avatar-upload-poster">
                                    <input type="hidden" class="avatar-src-poster" name="avatar_src_poster">
                                    <input type="hidden" class="avatar-data-poster" name="avatar_data_poster">
                                    <label for="avatarInputPoster">Select Photo</label>
                                    <input type="file" class="avatar-input-poster" id="avatarInputPoster" name="avatar_file_poster">
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper-poster"></div>
                                    </div>
                                    <div class="col-md-3">
                                        {{-- <div class="avatar-preview-poster preview-lg hidden-xs"><img src="{{ asset('img/banner-img.jpg') }}"></div> --}}
                                        <div class="avatar-preview-poster preview-md remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        <div class="avatar-preview-poster preview-sm remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        {{-- Zoom in out STARTED --}}
                                        <div class="avatar-btns-poster">
                                            <div class="btn-group"><br>
                                                <button type="button" class="btn btn-default fa fa-search-plus" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                <button type="button" class="btn btn-default fa fa-search-minus" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                            </div>
                                        </div>
                                        {{-- Zoom in out ENDED --}}
                                        <br>
                                        <button type="submit" class="btn btn-default btn-block avatar-save-poster">Add Poster</button>
                                        <div class="text-center">
                                            <img src="{{asset('img/loader.gif')}}" id="loader-poster" width="20" style="display: none">
                                        </div>
                                    </div>
                                </div>

                                <div class="row avatar-btns-poster panel-body">
                                    <div class="col-md-9 text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="avatar-view-poster" title="" style="display:none" data-original-title="Change the poster Picture">
                                            <img src="{{ asset('img/banner-img.jpg') }}" alt="Cover Picture" id="coverimage" style="height:auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Cropping Model for Event Poster ENDED --}}
    {{-- Cropping Model for Event Banner STARTED --}}
    <div class="crop-avatar">
        <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="avatar-form" id="bannerVendorUploadForm" action="{{ route('ajax-upload-event-edit-cover') }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <input name="event_id" value="{{$event->getEncryptedId()}}" type="hidden" />
                        <input type="hidden" value="" name="codeprofile">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                            <br>
                            <h4 class="text-center">Upload Banner</h4>
                            <hr>
                            <div class="avatar-body">
                                <div class="avatar-upload">
                                    <input type="hidden" class="avatar-src" name="avatar_src">
                                    <input type="hidden" class="avatar-data" name="avatar_data">
                                    <label for="avatarInput">Select Photo</label>
                                    <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper"></div>
                                    </div>
                                    <div class="col-md-3">
                                        {{-- <div class="avatar-preview preview-lg hidden-xs"><img src="{{ asset('img/banner-img.jpg') }}"></div> --}}
                                        <div class="avatar-preview preview-md remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        <div class="avatar-preview preview-sm remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                        {{-- Zoom in out STARTED --}}
                                        <div class="avatar-btns">
                                            <div class="btn-group"><br>
                                                <button type="button" class="btn btn-default fa fa-search-plus" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                <button type="button" class="btn btn-default fa fa-search-minus" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                            </div>
                                        </div>
                                        {{-- Zoom in out ENDED --}}
                                        <br>
                                        <button type="submit" class="btn btn-default btn-block avatar-save">Add Banner</button>
                                        <div class="text-center">
                                            <img src="{{asset('img/loader.gif')}}" id="loader-banner" width="20" style="display: none">
                                        </div>
                                    </div>
                                </div>
    
                                <div class="row avatar-btns panel-body">
                                    <div class="col-md-9 text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="avatar-view" title="" style="display:none" data-original-title="Change the profile Picture">
                                            <img src="{{ asset('img/banner-img.jpg') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Cropping Model for Event Banner ENDED --}}
    {{-- Cropping Model for Event Photo STARTED --}}
    <div class="crop-avatar-another">
        <div class="modal fade" id="avatar-modal-photo" aria-hidden="true" aria-labelledby="avatar-modal-photo-label" role="dialog" tabindex="-1" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="avatar-form-photo" id="photoVendorUploadForm" action="{{ route('ajax-upload-event-edit-profile') }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <input name="event_id" value="{{$event->getEncryptedId()}}" type="hidden" />
                        <input type="hidden" value="" name="codeprofile">
                        <input type="hidden" name="allowed_photos" value='1'>
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                            <br>
                            <h4 class="text-center">Upload Photo</h4>
                            <hr>
                            <div class="avatar-body-photo">
                                <div class="avatar-upload-photo">
                                    <input type="hidden" class="avatar-src-photo" name="avatar_src">
                                    <input type="hidden" class="avatar-data-photo" name="avatar_data_photo">
                                    <label for="avatarInputPhoto">Select Photo</label>
                                    <input type="file" class="avatar-input-photo" id="avatarInputPhoto" name="avatar_file_photo">
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="avatar-wrapper-photo"></div>
                                    </div>
                                    <div class="col-md-3">
                                        {{-- <div class="avatar-preview-photo preview-lg hidden-xs"><img src="{{ asset('img/dummy-profile.png') }}"></div> --}}
                                        <div class="avatar-preview-photo preview-md remove hidden"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                        <div class="avatar-preview-photo preview-sm remove hidden"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                        {{-- Zoom in out STARTED --}}
                                        <div class="avatar-btns">
                                            <div class="btn-group"><br>
                                                <button type="button" class="btn btn-default fa fa-search-plus zoom-in-btn" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                                <button type="button" class="btn btn-default fa fa-search-minus zoom-out-btn" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                            </div>
                                        </div>
                                        {{-- Zoom in out ENDED --}}
                                        <br>
                                        <button type="submit" class="btn btn-default btn-block avatar-save-photo">Add Photo</button>
                                        <div class="text-center">
                                            <img src="{{asset('img/loader.gif')}}" id="loader-photo" width="20" style="display: none">
                                        </div>
                                    </div>
                                </div>
    
                                <div class="row avatar-btns-photo panel-body">
                                    <div class="col-md-9 text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="avatar-view-photo" title="" style="display:none" data-original-title="Change the profile Picture">
                                            <img src="{{ asset('img/dummy-profile.png') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- Cropping Model for Event Photo ENDED --}}
    @include('modals.modal-delete')
@endsection

@section('js')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/timepicki.js')}}"></script>
<script src="{{asset('dist/js/cropper.min.js')}}"></script>
<script src="{{asset('dist/js/main-poster-event.js')}}"></script>
<script src="{{asset('dist/js/main-cover-event.js')}}"></script>
<script src="{{asset('dist/js/main-photo-event.js')}}"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript">
    var deleteImage = false;
    var imageId = 0;
    var imageType = '';
    jQuery(".deleteImage").on('click', function() {
        jQuery("#confirmDelete .modal-title").html("Delete Image");
        jQuery("#confirmDelete .modal-body").html("<p>Are you sure you want to delete this Image ?</p>");
        jQuery("#confirmDelete").modal('show');
        deleteImage = true;
        imageId = jQuery(this).data('id');
        imageType = jQuery(this).data('type');
    });
	
    $('#confirmDelete').on('hidden.bs.modal', function() {
        deleteImage = false;
    });
	
    $('#confirmDelete .modal-footer #confirm').on('click', function() {
        if (deleteImage) {
            var _token = $('input[name="_token"]').val();
            var form = new FormData();
            form.append('_token', _token);
            form.append('id', imageId);
            jQuery.ajax({
                url: "{{ route('admin-delete-event-image') }}",
                data: form,
                method: "post",
                processData: false,
                contentType: false
            }).done(function(response) {
                jQuery("#fileselector-" + imageType).remove();
                jQuery("#confirmDelete").modal('hide');
                imageId = 0;
                imageType = '';
                deleteImage = false;
            });

        }
    })
    $(".pimage").change(function(e) {
        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
            var file = e.originalEvent.srcElement.files[i];
            var img = document.createElement("img");
            var reader = new FileReader();
            reader.onloadend = function() {
                img.src = reader.result;
            }
            reader.readAsDataURL(file);
            $("#aimage").after(img);
        }
    });
    /* For poster upload */
    $("#posterEventUploadForm").on('submit', function(e){
        e.preventDefault();
        var image = $("#avatarInputPoster")[0].files[0];
        if (image === undefined){
            var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
            $(".avatar-upload-poster").after(msg);
            return false;
        } else {
            $("#loader-poster").show();
            $("button.avatar-save-poster").attr('disabled','disabled');
            var _token = $('input[name="_token"]').val();
            var thumb_data = $('input[name="avatar_data_poster"]').val();
            var event_id = '{{$event->getEncryptedId()}}';
            var form = new FormData();
            form.append('image', image);
            form.append('_token', _token);
            form.append('event_id', event_id);
            form.append('thumb_data', thumb_data);
            $.ajax({
                url: "{{route('ajax-upload-event-edit-poster')}}",
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(data) {
                    $("#loader-poster").hide();
                    var response = data[0];
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                    if(response.status === 0) {
                        $(".avatar-upload-poster").after(msg);
                        $("button.avatar-save-poster").removeAttr('disabled');
                    } else {
                        $("#avatar-modal-poster").modal('hide');
                        $(".crop-avatar-poster .img-responsive").attr('src', response.thumb_path);
                    }
                },
                error: function(xhr, status, error) {
                    $("#loader-poster").hide();
                    var errors = xhr.responseJSON;
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                    $(".avatar-upload-poster").after(msg);
                    $("button.avatar-save-poster").removeAttr('disabled');
                }
            });
        }
    });
    $('#avatar-modal-poster').on('hidden.bs.modal', function () {
        $("#loader-poster").hide();
        $(".avatar-body-poster .alert").remove();
        $("#posterEventUploadForm")[0].reset();
        $("button.avatar-save-poster").removeAttr('disabled');
    });
    /* For banner upload */
    $("#bannerVendorUploadForm").on('submit', function(e){
        e.preventDefault();
        var image = $("#avatarInput")[0].files[0];
        if (image === undefined){
            var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
            $(".avatar-upload").after(msg);
            return false;
        } else {
            $("#loader-banner").show();
            $("button.avatar-save").attr('disabled','disabled');
            var _token = $('input[name="_token"]').val();
            var thumb_data = $('input[name="avatar_data"]').val();
            var event_id = '{{$event->getEncryptedId()}}';
            var form = new FormData();
            form.append('image', image);
            form.append('event_id', event_id);
            form.append('_token', _token);
            form.append('thumb_data', thumb_data);
            $.ajax({
                url: "{{route('ajax-upload-event-edit-cover')}}",
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(data) {
                    $("#loader-banner").hide();
                    var response = data[0];
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                    if(response.status === 0) {
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled');
                    } else {
                        $("#avatar-modal").modal('hide');
                        $(".crop-avatar .img-responsive").attr('src', response.thumb_path);
                    }
                },
                error: function(xhr, status, error) {
                    $("#loader-banner").hide();
                    var errors = xhr.responseJSON;
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                    $(".avatar-upload").after(msg);
                    $("button.avatar-save").removeAttr('disabled');
                }
            });
        }
    });
    $('#avatar-modal').on('hidden.bs.modal', function () {
        $("#loader-banner").hide();
        $(".avatar-body .alert").remove();
        $("#bannerVendorUploadForm")[0].reset();
        $("button.avatar-save").removeAttr('disabled');
    });
    /* For photo upload */
    $("#photoVendorUploadForm").on('submit', function(e){
        e.preventDefault();
        var image = $("#avatarInputPhoto")[0].files[0];
        if (image === undefined){
            var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
            $(".avatar-upload-photo").after(msg);
            return false;
        } else {
            $("#loader-photo").show();
            $("button.avatar-save-photo").attr('disabled','disabled');
            var _token = $('input[name="_token"]').val();
            var event_id = '{{$event->getEncryptedId()}}';
            var thumb_data = $('input[name="avatar_data_photo"]').val();
            var form = new FormData();
            form.append('image', image);
            form.append('event_id', event_id);
            form.append('_token', _token);
            form.append('thumb_data', thumb_data);
            $.ajax({
                url: "{{route('ajax-upload-event-edit-profile')}}",
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(data) {
                    $("#loader-photo").hide();
                    var response = data[0];
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                    if(response.status === 0){
                        $(".avatar-upload-photo").after(msg);
                        $("button.avatar-save-photo").removeAttr('disabled');
                    } else {
                        $("#avatar-modal-photo").modal('hide');
                        $(".crop-avatar-another .img-responsive").attr('src', response.thumb_path);
                    }
                },
                error: function(xhr, status, error){
                    $("#loader-photo").hide();
                    var errors = xhr.responseJSON;
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                    $(".avatar-upload").after(msg);
                    $("button.avatar-save").removeAttr('disabled');
                }
            });
        }
    });
    $('#avatar-modal-photo').on('hidden.bs.modal', function () {
        $("#loader-photo").hide();
        $(".avatar-body-photo .alert").remove();
        $("#photoVendorUploadForm")[0].reset();
        $("button.avatar-save-photo").removeAttr('disabled');
    });

    $('#start_time_picker').timepicki(); 
    $('#end_time_picker').timepicki();
    var date = new Date();
    date.setDate(date.getDate());
    
    $('.input-group.date').datepicker({format: "dd.mm.yyyy", startDate: date});
    CKEDITOR.config.allowedContent=true;
    CKEDITOR.replace( 'description' );
    CKEDITOR.replace( 'ticket_information' );
    $(document).ready(function() {
        $.validator.setDefaults({ ignore: '' });

        $(".approve_photo").on("click", function(e){
            e.preventDefault();
            //var message = prompt("Reason of Disappr","");
            //approve_photo
            var photo_id = $(this).data('photoid');
            var token = $('input[name="_token"]').val();

            var elm = $(this);

            $.ajax({
                url:"{{ route('ajax.approveEventPhoto') }}",
                method:"POST",
                data:"photo_id="+photo_id+"&_token="+token,
                success:function(resp)
                {
                    elm.parent().parent().find("li .disapprove_photo").remove();
                    elm.text("Approved");
                    elm.prop("disabled", true);
                }
            })
        });

        $(".disapprove_photo").on("click", function(e){
            e.preventDefault();
            var message = prompt("Reason of Disapprove","");
            var photo_id = $(this).data('photoid');
            var token = $('input[name="_token"]').val();

            var elm = $(this);

            $.ajax({
                url:"{{ route('ajax.disapproveEventPhoto') }}",
                method:"POST",
                data:"message="+message+"&photo_id="+photo_id+"&_token="+token,
                success:function(resp)
                {
                    elm.parent().parent().find("li .approve_photo").remove();
                    elm.text("Disapproved");
                    elm.prop("disabled", true);
                }
            })
        });

        $('.dynamic').change(function() {
            if ($(this).val() != '') {
                var select = $(this).attr("id");
                var value = $(this).val();
                var dependent = $(this).data('dependent');
                var _token = $('input[name="_token"]').val();
                var output;
                if(dependent=="state") {
                    $('#city').html('<option value="">Select City</option>');
                }
                $('#' + dependent).html('');
                $("#phone-country-code option[selected='selected']").removeAttr('selected');
                $("#phone-country-code option[id=" + value + "]").attr("selected", "selected");
                $.ajax({
                    url: "{{ route('ajax.fetchLocation') }}",
                    method: "POST",
                    data:{
                        select: select,
                        value: value,
                        _token: _token,
                        dependent: dependent
                    },
                    success: function(result) {
                        for (var i = 0; i < result.length; i++) {
                            output += "<option value=" + result[i].id + ">" + result[i].value + "</option>";
                        }
                        $('#' + dependent).append(output);
                    }

                })
            }
        });

        // add Artists
        @if($artists_cnt > 0)
        var max_fields = 20-{{$artists_cnt}};
        @else
        var max_fields = 20;
        @endif
        var wrapper = $(".artist-container1");
        var add_button = $(".add_artist_form_field");
        @if($artists_cnt > 0)
        var x = {{$artists_cnt}};
        @else
        var x = 1;
        @endif
        
        $(add_button).click(function(e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append('<div class="artist-div"><div style="position: relative;float: left; width: 78%;"><input style="margin-bottom: 10px;" class="form-control artist-text" type="text" name="artists[]" data-element-id="'+x+'" list="artist-select-'+x+'" /><datalist id="artist-select-'+x+'">@if($artists->count() > 0)<select>@foreach($artists as $artist)<option value="{{$artist->name}}"></option>@endforeach</select>@endif</datalist></div><div style="position: relative;" id="artistList-'+x+'"></div>&nbsp;<div style="float: right; width: 20%;"><a href="javascript:void(0);" class="delete">Delete</a></div></div>'); //add input box
            } else {
                alert('You Reached the limits')
            }
        });

        $(wrapper).on("click", ".delete", function(e) {
            e.preventDefault();
            $(this).parent().parent('div.artist-div').remove();
            x--;
        });
        // artist ends
        $('input[type="radio"]').on('click', function() {
            if($(this).val() == 'radio' || $(this).val() == "checkbox") {
                $("#for-options").show();
                $("#for-placeholder").hide();
            } else {
                $("#for-options").hide();
                $("#for-placeholder").show();
            }
        });
        $('.categories-select2').select2({
            placeholder: "Select Category",
            allowClear: true
        });
    });
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection
