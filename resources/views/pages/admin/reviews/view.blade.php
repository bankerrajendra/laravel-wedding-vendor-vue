@extends('adminlte::page')
@section('title', 'View Review')
@section('content_header')
    <h1>View Review</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> Message</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {{$review->message}}
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Rating -->
                            <div class="form-group has-feedback row ">
                                <label for="rating" class="col-md-3 control-label"> Rating</label>
                                <div class="col-md-9">
                                    {{$review->rating}}
                                </div>
                            </div>
                            <!-- Rating -->

                            <!-- Status -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @if($review->approved == "1")
                                            Approved
                                        @elseif($review->approved == "0")
                                            Un Approved
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- Status -->
                            
                            <!-- User -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> User</label>

                                <div class="col-md-9">
                                    <a href="{{ URL::to('users/' . $review->user_id . '/edit') }}"><img src="{{$review->user->getVendorProfilePic()}}" alt="" width="100"></a><br />
                                    <strong>{{$review->user->first_name}} {{$review->user->last_name}}</strong>
                                </div>
                            </div>
                            <!-- User -->

                            <!-- Vendor -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> Vendor</label>

                                <div class="col-md-9">
                                    <a href="{{ URL::to('vendors/' . $review->vendor_id . '/edit') }}"><img src="{{$review->vendor()->getVendorProfilePic()}}" alt="" width="100"></a><br />
                                    <strong>{{$review->vendor()->first_name}} {{$review->vendor()->last_name}}</strong>
                                </div>
                            </div>
                            <!-- Vendor -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection
