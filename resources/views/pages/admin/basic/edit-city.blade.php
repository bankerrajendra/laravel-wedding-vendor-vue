@extends('adminlte::page')
@section('title', 'Edit City Record')
@section('content_header')
    <h1>Edit City Record</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

{{--@section('template_title')--}}
{{--@lang('usersmanagement.editing-user', ['name' => $user->name])--}}
{{--@endsection--}}

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    {{--<div class="container">--}}
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>



                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('post-edit-city'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}

                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> City Detail</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="hidden" name="id" id="id" value="<?php echo $cityRec['id']; ?>">
                                        <input type="text" name="name" id="name" value="<?php echo $cityRec['name']; ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="status" class="col-md-3 control-label"> State</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="state_id" id="state_id" >
                                            @foreach($states as $state)
                                                <option value="{{$state->id}}" <?php echo ($state->id==$cityRec['state_id']?'selected':''); ?> >{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}

    @include('modals.modal-save')
    @include('modals.modal-delete')

@endsection

@section('js')
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @include('scripts.check-changed')



    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
