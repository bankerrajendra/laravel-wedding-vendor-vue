{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title', 'Countries List')
@section('content_header')
    <h1>Countries List</h1>
@stop


@section('css')

    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box">

                    <div class="box-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                Showing All <strong>Countries</strong> List
                            </span>

                            {{--<div class="btn-group pull-right btn-group-xs">--}}
                                {{--<a href="{{route('add-plan')}}"   style="font-size: 16px">Add Plans</a>--}}

                            {{--</div>--}}
                        </div>
                    </div>




                    <div class="box-body">

                        {{--@if(config('usersmanagement.enableSearchUsers'))--}}
                        {{--@include('partials.search-users-form')--}}
                        {{--@endif--}}

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table">
                                {{--<caption id="user_count">--}}
                                {{--{{$blogvar->count()}} records total--}}
                                {{--</caption>--}}
                                <thead class="thead">
                                <tr>
                                    <th>Id</th>
                                    <th>Country Name</th>
                                    <th>Country Code</th>
                                    <th>Country number code</th>
                                    {{--<th>Action</th>--}}
                                </tr>
                                </thead>
                                <tbody id="cmscitystate_table">
                                <?php $pageNo=1; ?>
                                @foreach($countryLists as $country)
                                    <tr>
                                        <td>{{$pageNo++}}</td>
                                        <td>{{$country->name}}</td>
                                        <td>{{$country->code}}</td>
                                        <td>{{$country->phonecode}}</td>
                                        {{--<td>--}}
                                            {{--<a class="btn btn-sm btn-info " href="{{route('edit-plan',['id'=>$plan->id])}}" data-toggle="tooltip" title="Edit Plan">--}}
                                                {{--Edit--}}
                                            {{--</a>--}}
                                            {{--<a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this plan?');" href="{{route('delete-plan',['id'=>$plan->id])}}" data-toggle="tooltip" title="Delete Plan">--}}
                                                {{--Delete Plan--}}
                                            {{--</a>--}}
                                        {{--</td>--}}
                                    </tr>
                                @endforeach
                                </tbody>


                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection