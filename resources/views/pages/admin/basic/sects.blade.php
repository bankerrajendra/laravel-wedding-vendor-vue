{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title', 'Sects List')
@section('content_header')
    <h1>Sects List</h1>
@stop


@section('css')

    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box">

                    <div class="box-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                Showing All <strong>Sects</strong> List
                            </span>

                            <div class="btn-group pull-right btn-group-xs">
                                <a href="{{route('add-sects')}}"   style="font-size: 16px; font-weight: bold; text-decoration: underline">Add Sects</a><br><br>
                                <form method="get" action="#">
                                @if($search!="")
                                    <a href="{{route('admin-sects')}}"   style="font-size: 16px; text-decoration: underline">Clear Search</a> &nbsp;
                                @endif
                                    <input type="text" name="search" value="{{@$search}}"><input type="submit" value="Search ">
                                </form>

                            </div>
                        </div>
                    </div>




                    <div class="box-body">


                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table">
                                {{--<caption id="user_count">--}}
                                {{--{{$blogvar->count()}} records total--}}
                                {{--</caption>--}}
                                <thead class="thead">
                                <tr>
                                    <th>Id</th>
                                    <th>Sect Name</th>
                                    <th>Religion </th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="cmscitystate_table">
                                <?php //$pageNo=1; ?>
                                @foreach($sectvar as $sect)
                                    <tr>
                                        <td>{{$pageNo++}}</td>
                                        <td>{{$sect->name}}</td>
                                        <td>{{$sect->religion->religion}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-info " href="{{route('edit-sects',['id'=>$sect->id])}}" data-toggle="tooltip" title="Edit Sect">
                                                Edit
                                            </a>
                                            <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this sect?');" href="{{route('delete-sects',['id'=>$sect->id])}}" data-toggle="tooltip" title="Delete Sect">
                                                Delete Sect
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>


                            </table>
                            @if(config('usersmanagement.enableCmsPagination'))
                                {{ $sectvar->links() }}
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection