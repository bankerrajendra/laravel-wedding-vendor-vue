@extends('adminlte::page')
@section('title', 'Manage Payment Gateways')
@section('content_header')
    <h1>Manage Payment Gateways</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection
<style type="text/css">
.payment-gateway-frm .has-feedback .form-control {
    padding-right: 5px;
}
</style>
@section('content')
    <div class="row">
        <div class="col-lg-12 offset-lg-1 payment-gateway-frm">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">
                        
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-payment-gateways'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            @csrf
                            <!-- Paypal -->
                            <div class="form-group has-feedback row ">
                                <h3 class="col-md-12 control-label"> Paypal</h3>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="paypal_client_id" class="col-md-3 control-label"> Client ID</label>
                                
                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($paypal['client_id'])) {
                                                $paypal_client_id = $paypal['client_id'];
                                            } else {
                                                $paypal_client_id = "";
                                            }
                                        @endphp
                                        {!! Form::text('paypal[client_id]', $paypal_client_id, array('id' => 'paypal_client_id', 'class' => 'form-control', 'placeholder' => 'Client ID')) !!}
                                    </div>
                                    <div class="show-creds">
                                        <b>Sandbox</b>: AVyBTdC50Ins52MgCccQPJ7VE0vvx3PCM3jTFra5RdDRwiafRMQRvZifzN9-9GpYLd0Ij5xqbqLgiA-g
                                        <br />
                                        <b>Live</b>: AfpKUi8myQmhilhscm_cqRn0YUwM5J69knCU9tvIInI4xajVX2U-IvtCv9cv_fwaX0eTKPKGzRKlB0Lf
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="paypal_secret_key" class="col-md-3 control-label"> Secret Key</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($paypal['secret_key'])) {
                                                $paypal_secret_key = $paypal['secret_key'];
                                            } else {
                                                $paypal_secret_key = "";
                                            }
                                        @endphp
                                        {!! Form::text('paypal[secret_key]', $paypal_secret_key, array('id' => 'paypal_secret_key', 'class' => 'form-control', 'placeholder' => 'Secret Key')) !!}
                                    </div>
                                    <div class="show-creds">
                                        <b>Sandbox</b>: ECYrratltOol1gijWMUVavxCAj1VbBhM8psFfqNRGQ5zNR5qeAuT8MrOh-oKdmEgxdy6pxWAHGPy65VZ
                                        <br />
                                        <b>Live</b>: EO00Lsf2Ee05DlUu84NU-lb9yE05HqIkNZbrluGBWSV54uEQeAopsDUuRg-sP-FU02Jcrzf9Yp81TpYu
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="paypal_mode" class="col-md-3 control-label"> Mode</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($paypal['mode'])) {
                                                $paypal_mode = $paypal['mode'];
                                            } else {
                                                $paypal_mode = "";
                                            }
                                        @endphp
                                        <select class="custom-select form-control" name="paypal[mode]" id="paypal_mode" >
                                            <option @if($paypal_mode == 'sandbox') selected="selected" @endif value="sandbox">Sandbox</option>
                                            <option @if($paypal_mode == 'live') selected="selected" @endif value="live">Live</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="paypal_connection_timeout" class="col-md-3 control-label"> Connection Timeout</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($paypal['connection_timeout'])) {
                                                $paypal_connection_timeout = $paypal['connection_timeout'];
                                            } else {
                                                $paypal_connection_timeout = "";
                                            }
                                        @endphp
                                        {!! Form::text('paypal[connection_timeout]', $paypal_connection_timeout, array('id' => 'paypal_connection_timeout', 'class' => 'custom-select form-control', 'placeholder' => 'Connection Timeout in seconds')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="paypal_log_enabled" class="col-md-3 control-label"> Log Enabled</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($paypal['log_enabled'])) {
                                                $paypal_log_enabled = $paypal['log_enabled'];
                                            } else {
                                                $paypal_log_enabled = "";
                                            }
                                        @endphp
                                        <select class="custom-select form-control" name="paypal[log_enabled]" id="paypal_log_enabled" >
                                            <option @if($paypal_log_enabled == 'true') selected="selected" @endif value="true">True</option>
                                            <option @if($paypal_log_enabled == 'false') selected="selected" @endif value="false">False</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="paypal_log_filename" class="col-md-3 control-label"> Log FileName</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($paypal['log_filename'])) {
                                                $paypal_log_filename = $paypal['log_filename'];
                                            } else {
                                                $paypal_log_filename = "";
                                            }
                                        @endphp
                                        {!! Form::text('paypal[log_filename]', $paypal_log_filename, array('id' => 'paypal_log_filename', 'class' => 'form-control', 'placeholder' => 'Log Filename')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="paypal_log_level" class="col-md-3 control-label"> Log Level</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($paypal['log_level'])) {
                                                $paypal_log_level = $paypal['log_level'];
                                            } else {
                                                $paypal_log_level = "";
                                            }
                                        @endphp
                                        {!! Form::text('paypal[log_level]', $paypal_log_level, array('id' => 'paypal_log_level', 'class' => 'form-control', 'placeholder' => 'Log Level')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="paypal_status" class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="paypal_status" id="paypal_status" >
                                            <option @if($paypal_status == '1') selected="selected" @endif value="1">Active</option>
                                            <option @if($paypal_status == '0') selected="selected" @endif value="0">In Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- Paypal -->

                            <!-- Helcim -->
                            <div class="form-group has-feedback row ">
                                <h3 class="col-md-12 control-label"> Helcim</h3>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="helcim_api_url" class="col-md-3 control-label"> Api URL</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($helcim['api_url'])) {
                                                $helcim_api_url = $helcim['api_url'];
                                            } else {
                                                $helcim_api_url = "";
                                            }
                                        @endphp
                                        {!! Form::text('helcim[api_url]', $helcim_api_url, array('id' => 'helcim_api_url', 'class' => 'form-control', 'placeholder' => 'Api URL')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="helcim_account_id" class="col-md-3 control-label"> Account ID</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($helcim['account_id'])) {
                                                $helcim_account_id = $helcim['account_id'];
                                            } else {
                                                $helcim_account_id = "";
                                            }
                                        @endphp
                                        {!! Form::text('helcim[account_id]', $helcim_account_id, array('id' => 'helcim_account_id', 'class' => 'form-control', 'placeholder' => 'Account ID')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group has-feedback row ">

                                <label for="helcim_api_token" class="col-md-3 control-label"> Api Token</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($helcim['api_token'])) {
                                                $helcim_api_token = $helcim['api_token'];
                                            } else {
                                                $helcim_api_token = "";
                                            }
                                        @endphp    
                                        {!! Form::text('helcim[api_token]', $helcim_api_token, array('id' => 'helcim_api_token', 'class' => 'form-control', 'placeholder' => 'Api Token')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="helcim_transaction_type" class="col-md-3 control-label"> Transaction Type</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($helcim['transaction_type'])) {
                                                $helcim_transaction_type = $helcim['transaction_type'];
                                            } else {
                                                $helcim_transaction_type = "";
                                            }
                                        @endphp    
                                        {!! Form::text('helcim[transaction_type]', $helcim_transaction_type, array('id' => 'helcim_transaction_type', 'class' => 'form-control', 'placeholder' => 'Transaction Type')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="helcim_terminal_id" class="col-md-3 control-label"> Terminam Id</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($helcim['terminal_id'])) {
                                                $helcim_terminal_id = $helcim['terminal_id'];
                                            } else {
                                                $helcim_terminal_id = "";
                                            }
                                        @endphp   
                                        {!! Form::text('helcim[terminal_id]', $helcim_terminal_id, array('id' => 'helcim_terminal_id', 'class' => 'form-control', 'placeholder' => 'Terminam Id')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="helcim_test_mode" class="col-md-3 control-label"> Test Mode</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($helcim['test_mode'])) {
                                                $helcim_test_mode = $helcim['test_mode'];
                                            } else {
                                                $helcim_test_mode = "";
                                            }
                                        @endphp
                                        <select class="custom-select form-control" name="helcim[test_mode]" id="helcim_test_mode" >
                                            <option @if($helcim_test_mode == '1') selected="selected" @endif value="1">Yes</option>
                                            <option @if($helcim_test_mode == '0') selected="selected" @endif value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="helcim_status" class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="helcim_status" id="helcim_status" >
                                            <option @if($helcim_status == '1') selected="selected" @endif value="1">Active</option>
                                            <option @if($helcim_status == '0') selected="selected" @endif value="0">In Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- Helcim -->

                            <!-- Merrco -->
                            <div class="form-group has-feedback row ">
                                <h3 class="col-md-12 control-label"> Merrco</h3>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="merrco_account_id" class="col-md-3 control-label"> Account ID</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($merrco['account_id'])) {
                                                $merrco_account_id = $merrco['account_id'];
                                            } else {
                                                $merrco_account_id = "";
                                            }
                                        @endphp
                                        {!! Form::text('merrco[account_id]', $merrco_account_id, array('id' => 'merrco_account_id', 'class' => 'form-control', 'placeholder' => 'Account ID')) !!}
                                    </div>
                                    <div>
                                        <b>Test</b>: 1001376730
                                        <br />
                                        <b>Live</b>: 1002533064
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group has-feedback row ">

                                <label for="merrco_basic_auth_key" class="col-md-3 control-label"> Basic Authorization Key</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($merrco['basic_auth_key'])) {
                                                $merrco_basic_auth_key = $merrco['basic_auth_key'];
                                            } else {
                                                $merrco_basic_auth_key = "";
                                            }
                                        @endphp
                                        {!! Form::text('merrco[basic_auth_key]', $merrco_basic_auth_key, array('id' => 'merrco_basic_auth_key', 'class' => 'form-control', 'placeholder' => 'Basic Authorization Key')) !!}
                                    </div>
                                    <div>
                                        <b>Test</b>: <span style="word-wrap: break-word;">dGVzdF9iYW5rZXJyYWplbmRyYTpCLXFhMi0wLTVjYjRiMjVjLTAtMzAyYjAyMTQ1NTZjYTAxNDBmZWM1MTY3Y2U3NzE1MzcyYjlmMDlhYWYzYWIzMmMzMDIxMzc0YzQxNDBkMDY2Y2M4Njc4NGQ0ZDZkNDBhNWY0ZjliM2VhYTk4</span>
                                        <br />
                                        <b>Live</b>: <span style="word-wrap: break-word;">cG1sZS0yNDU0MDQ6Qi1wMS0wLTVhM2FjOTA2LTAtMzAyYzAyMTQ2YzEyODU2ZDdjNmJiZmYxMDIzODAxNzMyNzA1YmFkZWYzZGNlODM1MDIxNDJmMjk5Y2I4NDY4YWI4NTAwNjYxY2ZjZDliYmJmYmIxMzhkNTQwY2Y=</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="merrco_test_mode" class="col-md-3 control-label"> Test Mode</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            if(isset($merrco['test_mode'])) {
                                                $merrco_test_mode = $merrco['test_mode'];
                                            } else {
                                                $merrco_test_mode = "";
                                            }
                                        @endphp
                                        <select class="custom-select form-control" name="merrco[test_mode]" id="merrco_test_mode" >
                                            <option @if($merrco_test_mode == '1') selected="selected" @endif value="1">Yes</option>
                                            <option @if($merrco_test_mode == '0') selected="selected" @endif value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group has-feedback row ">

                                <label for="merrco_status" class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="merrco_status" id="merrco_status" >
                                            <option value="1" @if($merrco_status == '1') selected="selected" @endif>Active</option>
                                            <option value="0" @if($merrco_status == '0') selected="selected" @endif>In Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- Merrco -->

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript" language="javascript">

</script>

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
