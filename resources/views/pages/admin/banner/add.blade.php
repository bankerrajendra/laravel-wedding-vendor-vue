@extends('adminlte::page')
@section('title', 'Add Banner')
@section('content_header')
    <h1>Add Banner</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/css/auth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-add-banner'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}

                            <div class="form-group row ">

                                <label for="title" class="col-md-3 control-label"> Banner Title</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('title', '', array('id' => 'title', 'class' => 'form-control', 'placeholder' => 'Banner Title')) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row ">
                                <label for="status" class="col-md-3 control-label"> Banner Type</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        @include('partials.admin.banner-types')
                                    </div>
                                </div>
                            </div>
                                
                            <div class="form-group row ">

                                <label for="banner_image" class="col-md-3 control-label"> Banner Image </label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="file" class="avatar-input" id="banner_image" name="banner_image" >
                                        <small>allowed extensions: <i>jpeg, jpg, png, gif, bmp</i></small>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row ">

                                <label for="meta_keyword" class="col-md-3 control-label"> Banner Link</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('banner_link', '', array('id' => 'banner_link', 'class' => 'form-control', 'placeholder' => 'Banner Link')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="status" class="col-md-3 control-label"> Status</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="status" id="status" >
                                            <option value="1" selected="selected">Active</option>
                                            <option value="0" >Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
