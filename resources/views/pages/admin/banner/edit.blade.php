@extends('adminlte::page')
@section('title', 'Edit Banner')
@section('content_header')
    <h1>Edit Banner</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/css/auth.css">
@endsection
@section('navclasses') background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-edit-banner'), 'method' => 'post', 'role' => 'form', 'enctype' => 'multipart/form-data', 'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            {!! Form::hidden('id', $banner->id) !!}
                            <div class="form-group row ">

                                <label for="title" class="col-md-3 control-label"> Banner Title</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('title', $banner->title, array('id' => 'title', 'class' => 'form-control', 'placeholder' => 'Banner Title')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">
                                <label for="status" class="col-md-3 control-label"> Banner Type</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        @include('partials.admin.banner-types')
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="banner_image" class="col-md-3 control-label"> Banner Image </label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="file" class="avatar-input" id="banner_image" name="banner_image" >
                                        <small>allowed extensions: <i>jpeg, jpg, png, gif, bmp</i></small>
                                        @if($banner->banner_image != "")
                                        <p>
                                        @php
                                            switch ($banner->banner_type) {
                                                case 'Free Membership':
                                                    $banner_width = 350;
                                                    break;
                                                case 'Dashboard':
                                                    $banner_width = 900;
                                                    break;
                                                case 'Search':
                                                    $banner_width = 1000;
                                                    break;
                                                case 'View Profile':
                                                    $banner_width = 263;
                                                    break;
                                                case 'Connect - 1':
                                                case 'Connect - 2':
                                                case 'Connect - 3':
                                                    $banner_width = 360;
                                                    break;
                                                default:
                                                    $banner_width = 400;
                                                    break;
                                            }
                                        @endphp
                                        <img src="{{asset('img/banners/'.$banner->banner_image)}}" alt="logo" width="{{$banner_width}}" border="0" />
                                            <br />
                                            <a href="{{asset('img/banners/'.$banner->banner_image)}}" target="_blank">View Full Image</a>
                                        </p>
                                    @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                <label for="meta_keyword" class="col-md-3 control-label"> Banner Link</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('banner_link', $banner->banner_link, array('id' => 'banner_link', 'class' => 'form-control', 'placeholder' => 'Banner Link')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="status" class="col-md-3 control-label"> Status</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="status" id="status" >
                                            <option value="1" @if($banner->status == 1) selected="selected" @endif>Active</option>
                                            <option value="0" @if($banner->status == 0) selected="selected" @endif>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
