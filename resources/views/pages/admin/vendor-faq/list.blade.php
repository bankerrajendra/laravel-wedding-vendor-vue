@extends('adminlte::page')
@section('title')
    Showing Vendor's Faqs
@endsection
@section('content_header')
    <h1>Vendor's Faqs</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
        .trf {
            transform: rotate(61deg);
            -ms-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            /* -webkit-transform: rotate(90deg); */
            -o-transform: rotate(90deg);
            margin-top: 20px;
        }
        .mb15 {
            margin-bottom: 15px;
        }
        .mb25 {
            margin-bottom: 25px;
        }
        .ml15 {
            margin-left: 15px;
        }
    </style>
@endsection

@section('content')
<div class="main-content">
	<div class="panel mb25">
    	<div class="panel-body">
        	<div class="row">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            	<form method="get" action="{{ route('admin-vendor-faqs') }}" name="search-action-frm">
                    {!! Form::hidden('status', $status) !!}
                    <div class="panel col-sm-12">
                        <a id="new" href="{{ route('add-vendor-faq') }}" class="btn btn-default mb15 ml15"><i class="fa fa-plus fa-fw"></i>&nbsp;&nbsp;Add New&nbsp;</a>
                        <a href="{{ route('admin-vendor-faqs') }}" class="btn btn-primary mb15 ml15"><i class="fa fa-list fa-fw"></i>&nbsp;&nbsp;All ({{$all_records}})&nbsp;</a>
                        <a href="{{ route('admin-vendor-faqs') }}?status=1" class="btn btn-success mb15 ml15"><i class=" fa fa-thumbs-up text-success "></i>&nbsp;&nbsp;Active List ({{$active_records}})&nbsp;</a>
                        <a href="{{ route('admin-vendor-faqs') }}?status=0" class="btn btn-warning mb15 ml15"><i class=" fa fa-thumbs-down text-warning "></i>&nbsp;&nbsp;In Active List ({{$inactive_records}})&nbsp;</a>
                    </div>
                    <div class=" col-sm-5 mb15 ml15">
                        <label for="cat-fltr">Filter by Category</label>&nbsp;
                        <select name="category" class="form-control" id="cat-fltr">
                            <option value="">All</option>
                            @if($all_categories != null)
                                @foreach ($all_categories as $category_val)
                                    @if($category_val->id  == $category)
                                        <option value="{{$category_val->id}}" selected="selected">{{$category_val->name}}</option>
                                    @else
                                        <option value="{{$category_val->id}}">{{$category_val->name}}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-12">
                        <a id="action-delete" href="#" class="btn btn-danger mb15 ml15"><i class="fa fa-trash fa-fw"></i>&nbsp;&nbsp;Delete&nbsp;</a>
                            
                        <a id="action-active" href="#" class="btn btn-success mb15 ml15"><i class="fa fa-thumbs-up text-success"></i>&nbsp;&nbsp;Make Active&nbsp;</a>
                                    
                        <a id="action-inactive" href="#" class="btn btn-warning mb15 ml15"><i class="fa fa-thumbs-down text-warning"></i>&nbsp;&nbsp;Make In Active&nbsp;</a>

                        <a id="action-order" href="#" class="btn btn-warning mb15 ml15"><i class="fa fa-sort"></i>&nbsp;&nbsp;Change Order&nbsp;</a>
                    </div>
                    <div class="col-sm-3">
                        <label>
                            Show
                        </label>
                        <label>
                            <select name="per_page" id="per_page" class="form-control">
                                <option @if($per_page == "1")selected="selected"@endif value="1">1</option>
                                <option @if($per_page == "2")selected="selected"@endif value="2">2</option>
                                <option @if($per_page == "3")selected="selected"@endif value="3">3</option>
                                <option @if($per_page == "5")selected="selected"@endif value="5">5</option>
                                <option @if($per_page == "10")selected="selected"@endif value="10">10</option>
                                <option @if($per_page == "20")selected="selected"@endif value="20">20</option>
                                <option @if($per_page == "50")selected="selected"@endif value="50">50</option>
                                <option @if($per_page == "100")selected="selected"@endif value="100">100</option>
                            </select>
                        </label>
                        <label>
                            entries
                        </label>                    
                    </div>
                    <div class="col-sm-2">
                        
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-4 input-group mb15">
                        <input type="search" name="search" id="search_field" class="form-control clearable x" value="{{$search}}" placeholder="Search here..">
                        <span class="input-group-btn pr10">
                        <button type="submit" class="btn btn-primary">Search</button>
                        </span>
                    </div>
                </form>
            </div>
            
            <div class="row">
            	<div class="col-sm-12">
                    <div class="table-responsive">
                    {!! Form::open(array('route' => array('handle-manage-bulk-vendor-faqs'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'name' => 'bulk-action-frm')) !!}
                    {!! Form::hidden('action', '', array('id' => 'action')) !!}
                        <table class="table table-bordered bordered table-striped table-condensed datatable">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="cb-checkbox mb0" id="all_check"><span class="cb-inner"><i><input class="all_check " type="checkbox" id="all"></i></span>
                                        </label>
                                    </th>
                                    <th>Edit</th>
                                    <th style="cursor: pointer;max-width:20%;min-width:15%">Status</th>
                                    <th style="cursor: pointer;max-width:20%;min-width:15%">
                                    Question
                                    </th>
                                    <th style="cursor: pointer;max-width:10%;min-width:5%">
                                    Order
                                    </th>
                                    <th style="cursor: pointer;max-width:15%;min-width:10pt">
                                    Answer Type 
                                    </th>
                                    <th style="cursor: pointer;max-width:15%;min-width:10%">
                                    Category
                                    </th>
                                    <th style="cursor: pointer;max-width:20%;min-width:15%">Created On</th>
                                    <th style="cursor: pointer;max-width:20%;min-width:15%">Modified On</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @if($records->total() > 0 && !empty($records))
                                        @foreach($records as $record)
                                        <tr>
                                            <td class="checkbox-row"><label class="cb-checkbox"><span class="cb-inner"><i><input type="checkbox" class="ids" name="ids[]" value="{{$record->id}}"></i></span></label>
                                            </td>
                                            <td>
                                                <a class="btn btn-info" href="{{ route('edit-vendor-faq', [$record->id]) }}"><i class="fa fa-pencil fa-fw"></i>&nbsp;&nbsp;Edit</a>
                                            </td>
                                            <td>
                                                <i style="font-size: 25px;" class="fa fa-thumbs-<?php if($record->status == 1): echo 'up text-success'; else: echo 'down text-warning'; endif?>"></i>
                                            </td>
                                            <td>{{$record->question}}</td>
                                            <td><input type="text" style="width: 50px;" name="orders[{{$record->id}}]"  value="{{$record->order}}" id="orders" /></td>
                                            <td>
                                                @switch($record->answer_type)
                                                    @case('text')
                                                        Textbox
                                                        @break
                                                
                                                    @case('text_area')
                                                        Textarea
                                                        @break

                                                    @case('radio')
                                                        Radio
                                                        @break

                                                    @case('checkbox')
                                                        Checkbox
                                                        @break
                                                    
                                                @endswitch
                                            </td>
                                            <td>
                                                @php
                                                    $categories_id = unserialize($record->categories);
                                                @endphp
                                                @foreach ($categories_id as $category_id)
                                                    <strong>{{$record->getCategoryName($category_id)->name}}</strong>
                                                    <br />
                                                @endforeach
                                            </td>
                                            <td>{{ date("F d, Y h:i A", strtotime($record->created_at))}}</td>
                                            <td>{{ date("F d, Y h:i A", strtotime($record->updated_at))}}</td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                        </table>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div>
            	<div class="pull-left">
                    <div id="show_record_message">
                        Showing {{$records->total()}} entries
                    </div>
                </div>
                <div class="pull-right">
                    {{ $records->appends(['search' => $search, 'per_page' => $per_page, 'status' => $status, 'category' => $category])->links() }}               
                </div>
    	    </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript">
        $('select[name="category"]').on('change', function() {
            window.location.href = "{{route('admin-vendor-faqs')}}?category="+this.value;
        });
        $('#per_page').on('change', function() {
            $("form[name='search-action-frm']").submit();
        });
        $('.all_check').click(function () {    
            $('input:checkbox').prop('checked', this.checked);    
        });
        $('#action-delete').on('click', function() {
            bulkActionSubmit('delete','Are you sure you want to delete selected entries!');
        });
        $('#action-active').on('click', function() {
            bulkActionSubmit('1','Are you sure you want to make active selected entries!');
        });
        $('#action-inactive').on('click', function() {
            bulkActionSubmit('0','Are you sure you want to make in active selected entries!');
        });
        $('#action-order').on('click', function() {
            bulkActionSubmit('sort','Are you sure you want to change order of all entries!');
        });
        function bulkActionSubmit(action_val, message) {
            if(action_val == 'sort') {
                if(confirm(message)) {
                        $('#action').val(action_val);
                        $("form[name='bulk-action-frm']").submit();
                }
            } else {
                if($('input[name="ids[]"]:checked').length > 0) {
                    if(confirm(message)) {
                        $('#action').val(action_val);
                        $("form[name='bulk-action-frm']").submit();
                    }
                } else {
                    alert('Select atleast one entry.');
                }
            }
        }
    </script>
@endsection
