@extends('adminlte::page')
@section('title', 'Edit Vendor FAQ >> '.$record->question)
@section('content_header')
<h1>Edit Vendor FAQ >> {{$record->question}}</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-edit-vendor-faq'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            {!! Form::hidden('id', $record->id) !!}
                            <div class="form-group row ">

                                <label for="question" class="col-md-3 control-label"> Question <span class="text-danger">*</span></label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('question', $record->question, array('id' => 'question', 'class' => 'form-control', 'placeholder' => 'Question')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="categories" class="col-md-3 control-label"> Select Categories <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            $categories_id = unserialize($record->categories)
                                        @endphp
                                        <select class="categories-select2" id="category_ids" style="width:600px" name="categories[]" multiple="multiple">
                                            @foreach($categories as $category)
                                            <option @if(in_array($category->id, $categories_id)) selected="selected" @endif value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Answer Type <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="answer_type" value="text" @if($record->answer_type == "text") checked="checked" @endif>
                                            Textbox
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="answer_type" value="text_area" @if($record->answer_type == 'text_area') checked="checked" @endif>
                                            Textarea
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="answer_type" value="radio" @if($record->answer_type == 'radio') checked="checked" @endif>
                                            Radio
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="answer_type" value="checkbox" @if($record->answer_type == 'checkbox') checked="checked" @endif>
                                            Checkbox
                                        </label>
                                    </div>
                                </div>
                            </div>
                            {{--Order--}}
                            <div class="form-group row ">

                                <label for="order" class="col-md-3 control-label"> Order <span class="text-danger">*</span></label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('order', $record->order, array('id' => 'order', 'class' => 'form-control', 'placeholder' => 'Order')) !!}
                                    </div>
                                </div>
                            </div>
                            {{--Order--}}
                            {{-- For Radio and Checkbox options --}}
                            <div class="form-group row" @if($record->answer_type == 'text' || $record->answer_type == 'text_area')style="display: none;"@endif id="for-options">
                                <label for="answer_options" class="col-md-3 control-label"> Answer Options <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                    <textarea name="options" id="answer_options" cols="90" rows="5" placeholder="Enter Multiple Option comma seperated like Option1, Option2, Option3...">{{@$record->vendor_faq_answer_options->options}}</textarea>
                                    </div>
                                </div>
                            </div>

                            {{-- For Text and Textarea placeholder --}}
                            <div class="form-group row" @if($record->answer_type == 'text' || $record->answer_type == 'text_area') style="display: block;" @else style="display: none;" @endif id="for-placeholder">
                                <label for="answer_placeholder" class="col-md-3 control-label"> Answer Placeholder </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('placeholder', @$record->vendor_faq_answer_placeholder->placeholder, array('id' => 'answer_placeholder', 'class' => 'form-control', 'placeholder' => 'Placeholder')) !!}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Status <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="status" value="1" @if($record->status == 1) checked="checked" @endif>
                                            Active
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="status" value="0" @if($record->status == 0) checked="checked" @endif>
                                            Inactive
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('input[type="radio"]').on('click', function() {
                if($(this).val() == 'radio' || $(this).val() == "checkbox") {
                    $("#for-options").show();
                    $("#for-placeholder").hide();
                } else {
                    $("#for-options").hide();
                    $("#for-placeholder").show();
                }
            });
            $('.categories-select2').select2({
                placeholder: "Select Category",
                allowClear: true
            });
        });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
