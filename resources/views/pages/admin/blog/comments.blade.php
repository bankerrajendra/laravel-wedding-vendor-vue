{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title', 'Blog Comments')
@section('content_header')
    <h1>Comments List</h1>
@stop


@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">

    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="box">

                    <div class="box-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                Showing All <strong>Comments</strong> Records
                            </span>

                        </div>
                    </div>




                    <div class="box-body">

                        {{--@if(config('usersmanagement.enableSearchUsers'))--}}
                        {{--@include('partials.search-users-form')--}}
                        {{--@endif--}}

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table">
                                <caption id="user_count">
                                    {{$blogvar->count()}} records total
                                </caption>
                                <thead class="thead">
                                <tr>
                                    <th>Id</th>
                                    <th>Blog Title</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile Number</th>

                                    <th class="hidden-xs">Comment</th>
                                    <th>Status</th>
                                    <th class="hidden-sm hidden-xs hidden-md">created On</th>
                                    <th class="hidden-sm hidden-xs hidden-md">Updated On</th>
                                    <th>Action</th>
                                    {{--<th class="no-search no-sort"></th>--}}
                                    {{--<th class="no-search no-sort"></th>--}}
                                </tr>
                                </thead>
                                <tbody id="cmscitystate_table">
                                @foreach($blogvar as $blog)
                                    <tr>
                                        <td>{{$pageNo++}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-info " href="{{route('edit-blog',['id'=>$blog->id])}}" data-toggle="tooltip" title="Blog">
                                                {{$blog->blog->title}}
                                            </a>
                                            </td>
                                        <td>{{$blog->name}}</td>
                                        <td>{{$blog->email}}</td>
                                        <td>{{$blog->mobile_number}}</td>

                                        <td class="hidden-xs">{!!nl2br($blog->messages)!!}</td>

                                        <td>	@if($blog->status=="1")
                                                Active
                                            @else
                                                Inactive
                                            @endif</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{date_format($blog->created_at,'l, F d, Y')}}</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{date_format($blog->updated_at,'l, F d, Y')}}</td>
                                        <td>

                                                @if($blog->status=="1")
                                                <a class="btn btn-sm btn-info " href="{{route('approve_comment',['id'=>$blog->id,'status'=>'0'])}}" data-toggle="tooltip" >
                                                    Inactivate
                                                </a>
                                                @else
                                                <a class="btn btn-sm btn-info " href="{{route('approve_comment',['id'=>$blog->id,'status'=>'1'])}}" data-toggle="tooltip" >
                                                    Activate
                                                </a>
                                                @endif

                                            <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this comment?');" href="{{route('delete-comment',['id'=>$blog->id])}}" data-toggle="tooltip" title="Delete Comment">
                                                Delete Comment
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>


                            </table>
                            @if(config('usersmanagement.enableCmsPagination'))
                                {{ $blogvar->links() }}
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--@include('modals.modal-delete')--}}

@endsection

@section('js')
    @if ((count($blogvar) > config('usersmanagement.datatablesJsStartCount')) && config('usersmanagement.enabledDatatablesJs'))
        @include('scripts.datatables')
    @endif
    {{--@if(config('usersmanagement.enableSearchUsers'))--}}
    {{--@include('scripts.search-users')--}}
    {{--@endif--}}
@endsection