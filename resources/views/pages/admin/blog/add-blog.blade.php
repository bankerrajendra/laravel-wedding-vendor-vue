@extends('adminlte::page')
@section('title', 'Add Blog Record')
@section('content_header')
    <h1>Add Blog Record</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

{{--@section('template_title')--}}
{{--@lang('usersmanagement.editing-user', ['name' => $user->name])--}}
{{--@endsection--}}

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    {{--<div class="container">--}}
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>



                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('update-blog'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}




                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> Blog Title</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('title', '', array('id' => 'title', 'class' => 'form-control', 'placeholder' => 'Blog Title')) !!}
                                    </div>
                                </div>
                            </div>




                            <div class="form-group has-feedback row">

                                <label for="description" class="col-md-3 control-label">Blog Description </label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::textarea('description', '', array('id' => 'description', 'class' => 'form-control', 'placeholder' => 'Blog Description')) !!}

                                    </div>

                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="slug" class="col-md-3 control-label"> Slug </label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('slug', '', array('id' => 'slug', 'class' => 'form-control', 'placeholder' => 'Slug')) !!}
                                    </div>

                                </div>
                            </div>


                                <div class="form-group has-feedback row ">

                                    <label for="blog_image" class="col-md-3 control-label"> Blog Image (Suggested size: 718 X 428) </label>

                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <input type="file" class="avatar-input" id="blog_image" name="blog_image" >
                                        </div>
                                    </div>
                                </div>


                            <div class="form-group has-feedback row ">

                                <label for="meta_keyword" class="col-md-3 control-label"> Meta Keywords</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('meta_keyword', '', array('id' => 'meta_keyword', 'class' => 'form-control', 'placeholder' => 'Meta Description')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="meta_title" class="col-md-3 control-label">Meta Title</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('meta_title', '', array('id' => 'meta_title', 'class' => 'form-control', 'placeholder' => 'Meta Title')) !!}
                                    </div>

                                </div>
                            </div>

                            <div class="form-group has-feedback row ">

                                <label for="meta_description" class="col-md-3 control-label">Meta Description</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::textarea('meta_description', '', array('id' => 'meta_description', 'class' => 'form-control', 'placeholder' => 'Meta Description')) !!}
                                    </div>

                                </div>
                            </div>



                            <div class="form-group has-feedback row ">

                                <label for="status" class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="status" id="status" >
                                            <option value="1" selected="selected">Active</option>
                                            <option value="0" >Inactive</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}

    @include('modals.modal-save')
    @include('modals.modal-delete')

@endsection

@section('js')
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @include('scripts.check-changed')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js" type="text/javascript"></script>
    <script>
        CKEDITOR.config.allowedContent=true;
        CKEDITOR.replace( 'description' );
    </script>


    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
