@extends('adminlte::page')
@section('title', 'Viewing Vendor Category >> '.$record->name)
@section('content_header')
<h1>Viewing Vendor Category >> {{$record->name}}</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
@php
    $satellites_comma = implode(",", unserialize($record->satellites));
@endphp
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Name: </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {{$record->name}}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Vendor Type: </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {{$record->get_vendor_type()->name}}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Display in Footer: </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        @if($record->display_in_footer == 1) Yes @else No @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Status: </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <div class="input-group">
                                            @if($record->status == 1) Active @else Inactive @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Associated Satellites: </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            $satellites_ids = unserialize($record->satellites)
                                        @endphp
                                        @foreach ($satellites_ids as $satellites_id)
                                            <strong>{{$record->getSatelliteName($satellites_id)->title}}</strong>
                                            <br />
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div>
                                @foreach ($record->vendor_category_satellite_information as $satellite_data)
                                    <div class="row">
                                        <label class="col-md-3 col-lg-2 control-label form-group">&nbsp;</label><div class="col-md-9 col-lg-7">
                                            <div class="row">
                                                <div class="col-md-12 col-lg-12 form-group">
                                                <h4>{{$record->getSatelliteName($satellite_data->satellite_id)->title}}</h4>
                                                </div>
                                                <div class="col-md-12 col-lg-12 form-group">
                                                    <label class="control-label" for="">Title: </label>
                                                    {{$satellite_data->title}}
                                                </div>
                                                <div class="col-md-12 col-lg-12 form-group">
                                                    <label class="control-label" for="">Keywords: </label>
                                                    {{$satellite_data->keywords}}
                                                </div>
                                                <div class="col-md-12 col-lg-12 form-group">
                                                    <label class="control-label" for="">Description: </label>
                                                    {{$satellite_data->description}}
                                                </div>
                                                <div class="col-md-12 col-lg-12 form-group">
                                                    <label class="control-label" for="">Meta Description: </label>
                                                    {{$satellite_data->meta_description}}
                                                </div>
                                                <div class="col-md-6 col-lg-12 form-group">
                                                    <label class="control-label" for="">Icon Image</label>
                                                    @if($satellite_data->icon_image != "")
                                                    <p>
                                                    <img src="{{config('constants.S3_WEDDING_IMAGES_CATEGORY').$satellite_data->icon_image}}" alt="icon_image" width="200" border="0" />
                                                        <br />
                                                        <a href="{{config('constants.S3_WEDDING_IMAGES_CATEGORY').$satellite_data->icon_image}}" target="_blank">View Full</a>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

