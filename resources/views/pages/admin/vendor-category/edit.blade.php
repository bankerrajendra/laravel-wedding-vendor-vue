@extends('adminlte::page')
@section('title', 'Edit Vendor Category >> '.$record->name)
@section('content_header')
<h1>Edit Vendor Category >> {{$record->name}}</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
@php
    $satellites_comma = implode(",", unserialize($record->satellites));
@endphp
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-edit-vendor-category'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            {!! Form::hidden('id', $record->id) !!}
                            <div class="form-group row ">

                                <label for="name" class="col-md-3 control-label"> Name <span class="text-danger">*</span></label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('name', $record->name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">

                                    <label for="vendor_type" class="col-md-3 control-label"> Vendor Type <span class="text-danger">*</span></label>
    
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <select class="form-control" name="vendor_type" id="vendor_type">
                                                <option value=""></option>
                                                @if($vendor_types != null)
                                                    @foreach ($vendor_types as $ven_type)
                                                        <option value="{{$ven_type->id}}" @if($record->vendor_type == $ven_type->id) selected="selected" @endif>{{$ven_type->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            <div class="form-group row ">
                                <label for="display_in_footer" class="col-md-3 control-label"> Display in Footer <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="display_in_footer" value="1" @if($record->display_in_footer == 1) checked="checked" @endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="display_in_footer" value="0" @if($record->display_in_footer == 0) checked="checked" @endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="has_admin_faqs" class="col-md-3 control-label"> Admin Specific FAQ<span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="has_admin_faqs" value="1" @if($record->has_admin_faqs == 1) checked="checked" @endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="has_admin_faqs" value="0" @if($record->has_admin_faqs == 0) checked="checked" @endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="has_custom_faqs" class="col-md-3 control-label"> Custom FAQ <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="has_custom_faqs" value="1" @if($record->has_custom_faqs == 1) checked="checked" @endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="has_custom_faqs" value="0"  @if($record->has_custom_faqs == 0) checked="checked" @endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Status <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="status" value="1" @if($record->status == 1) checked="checked" @endif>
                                            Active
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="status" value="0" @if($record->status == 0) checked="checked" @endif>
                                            Inactive
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="satellites" class="col-md-3 control-label"> Select Satellites <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        @php
                                            $satellites_us = unserialize($record->satellites)
                                        @endphp
                                        <select class="satellites-select2" id="satellite_ids" style="width:600px" name="satellites[]" multiple="multiple">
                                            @foreach($satellites as $satellite)
                                            <option @if(in_array($satellite->id, $satellites_us)) selected="selected" @endif value="{{$satellite->id}}">{{$satellite->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="add_more_category_details" style="display: block;">
                                @php
                                    $i = 1;    
                                @endphp
                                @foreach ($record->vendor_category_satellite_information as $satellite_data)
                                    <div id="satellite_seo_content_{{$satellite_data->satellite_id}}">
                                        <div class="row">
                                            <label class="col-md-3 col-lg-2 control-label form-group">&nbsp;</label><div class="col-md-9 col-lg-7">
                                                <div class="row">
                                                    <div class="col-md-12 col-lg-12 form-group">
                                                    <h4>{{$record->getSatelliteName($satellite_data->satellite_id)->title}}</h4>
                                                    </div>
                                                    <div class="col-md-12 col-lg-12 form-group">
                                                        <label class="control-label" for="title_{{$satellite_data->satellite_id}}">Title <span class="text-danger">*</span></label>
                                                        <input type="text" name="title_{{$satellite_data->satellite_id}}" id="title_{{$satellite_data->satellite_id}}" class="form-control" required="required" value="{{$satellite_data->title}}" placeholder="Title">
                                                    </div>
                                                    <div class="col-md-12 col-lg-12 form-group">
                                                        <label class="control-label" for="keywords_{{$satellite_data->satellite_id}}">Keywords <span class="text-danger">*</span></label>
                                                        <textarea name="keywords_{{$satellite_data->satellite_id}}" id="keywords_{{$satellite_data->satellite_id}}" class="form-control" required="required" placeholder="Keywords" rows="4">{{$satellite_data->keywords}}</textarea>
                                                    </div>
                                                    <div class="col-md-12 col-lg-12 form-group">
                                                        <label class="control-label" for="description_{{$satellite_data->satellite_id}}">Description <span class="text-danger">*</span></label>
                                                        <textarea name="description_{{$satellite_data->satellite_id}}" id="description_{{$satellite_data->satellite_id}}" class="form-control" required="required" rows="4" placeholder="Description">{{$satellite_data->description}}</textarea>
                                                    </div>
                                                    <div class="col-md-12 col-lg-12 form-group">
                                                        <label class="control-label" for="meta_description_{{$satellite_data->satellite_id}}">Meta Description <span class="text-danger">*</span></label>
                                                        <textarea name="meta_description_{{$satellite_data->satellite_id}}" id="meta_description_{$satellite_data->satellite_idi}}" class="form-control" rows="4" placeholder="Meta Description">{{$satellite_data->meta_description}}</textarea>
                                                    </div>
                                                    <div class="col-md-6 col-lg-12 form-group">
                                                        <label class="control-label" for="icon_image_{{$satellite_data->satellite_id}}">Icon Image</label>
                                                        <input filesize="10" extension="jpg|png|jpeg|gif|bmp" type="file" name="icon_image_{{$satellite_data->satellite_id}}" id="icon_image_{{$satellite_data->satellite_id}}" class="form-control">
                                                        <p class="help-block">Allowed File type jpg | png | jpeg | gif | bmp.</p>
                                                        @if($satellite_data->icon_image != "")
                                                        <p>
                                                        <img src="{{config('constants.S3_WEDDING_IMAGES_CATEGORY').$satellite_data->icon_image}}" alt="icon_image" width="200" border="0" />
                                                            <br />
                                                            <a href="{{config('constants.S3_WEDDING_IMAGES_CATEGORY').$satellite_data->icon_image}}" target="_blank">View Full</a>
                                                        </p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $i++;    
                                    @endphp
                                @endforeach
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}
                                </div>
                            </div>
                            <input type="hidden" name="store_satellite_ids" id="store_satellite_ids" value="{{$satellites_comma}}"  />
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.satellites-select2').select2({
                placeholder: "Select Satellite",
                allowClear: true
            });
            // handling the satellite select
            $('.satellites-select2').change(function(){
                var curr_id = $(this).val();
                var new_c_id = curr_id.toString();
                var sats_arr = new_c_id.split(',');
                if(curr_id != '' && curr_id != null){
                    var store_satellite_ids = $('#store_satellite_ids').val();
                    if(curr_id.length > 0) {
                        var opt = '';
                        var i;
                        for (i = 0; i < sats_arr.length; ++i) {
                            var check_id = $('#add_more_category_details').find('#satellite_seo_content_'+sats_arr[i]);
                            //alert(check_id.length);
                            if(store_satellite_ids.search(sats_arr[i])!='-1'){
                                //var content = $('#satellite_seo_content_'+id).html();
                                //opt += '<div id="satellite_seo_content_'+id+'">'+content+'</div>';
                            }else{
                                var title_heading = $('#satellite_ids option[value="'+sats_arr[i]+'"]').text();
                                opt += '<div id="satellite_seo_content_'+sats_arr[i]+'"><div class="row"><label class="col-md-3 col-lg-2 control-label form-group">&nbsp;</label><div class="col-md-9 col-lg-7"><div class="row"><div class="col-md-12 col-lg-12 form-group"><h4>'+title_heading+'</h4></div><div class="col-md-12 col-lg-12 form-group"><label class="control-label" for="title_'+sats_arr[i]+'">Title <span class="text-danger">*</span></label><input type="text" name="title_'+sats_arr[i]+'" id="title_'+sats_arr[i]+'" class="form-control" required="required" placeholder="Title" /></div><div class="col-md-12 col-lg-12 form-group"><label class="control-label" for="keywords_'+sats_arr[i]+'">Keywords <span class="text-danger">*</span></label><textarea name="keywords_'+sats_arr[i]+'" id="keywords_'+sats_arr[i]+'" class="form-control" required="required" placeholder="Keywords" rows="4"></textarea></div><div class="col-md-12 col-lg-12 form-group"><label class="control-label" for="description_'+sats_arr[i]+'">Description <span class="text-danger">*</span></label><textarea name="description_'+sats_arr[i]+'" id="description_'+sats_arr[i]+'" class="form-control" required="required" rows="4" placeholder="Description"></textarea></div><div class="col-md-12 col-lg-12 form-group"><label class="control-label" for="meta_description_'+sats_arr[i]+'">Meta Description <span class="text-danger">*</span></label><textarea name="meta_description_'+sats_arr[i]+'" id="meta_description_'+sats_arr[i]+'" class="form-control" rows="4" placeholder="Meta Description"></textarea></div><div class="col-md-6 col-lg-12 form-group"><label class="control-label" for="icon_image_'+sats_arr[i]+'">Icon Image</label><input filesize="10" extension="jpg|png|jpeg|gif|bmp" type="file" name="icon_image_'+sats_arr[i]+'" id="icon_image_'+sats_arr[i]+'" class="form-control"><p class="help-block">Allowed File type jpg | png | jpeg | gif | bmp.</p></div></div></div></div></div>';
                            }
                        }
                        
                        $('#add_more_category_details').show();
                        $('#add_more_category_details').append(opt);
                        if(store_satellite_ids!=''){
                            store_satellite_ids = store_satellite_ids.split(',');
                            curr_id = curr_id.toString();
                            for(var i=0;i<store_satellite_ids.length;i++){
                                if(curr_id.search(store_satellite_ids[i])=='-1'){
                                    $('#satellite_seo_content_'+store_satellite_ids[i]).remove();
                                }
                            }
                        }
                        $('#store_satellite_ids').val($(this).val());
                    } else {
                        $('#add_more_category_details').hide();
                        $('#add_more_category_details').html('');
                        $('#store_satellite_ids').val('');
                    }
                    
                } else {
                    $('#add_more_category_details').hide();
                    $('#add_more_category_details').html('');
                    $('#store_satellite_ids').val('');
                }
            });
        });
function myFunction(item, index, store_satellite_ids) {
    alert(store_satellite_ids);
    var check_id = $('#add_more_category_details').find('#satellite_seo_content_'+item);
    //alert(check_id.length);
    // if(store_satellite_ids.search(item)!='-1'){
    //     //var content = $('#satellite_seo_content_'+id).html();
    //     //opt += '<div id="satellite_seo_content_'+id+'">'+content+'</div>';
    // }else{
        var title_heading = $('#satellite_ids option[value="'+curr_id[i]+'"]').text();
        opt += '<div id="satellite_seo_content_'+item+'"><div class="row"><label class="col-md-3 col-lg-2 control-label form-group">&nbsp;</label><div class="col-md-9 col-lg-7"><div class="row"><div class="col-md-12 col-lg-12 form-group"><h4>'+title_heading+'</h4></div><div class="col-md-12 col-lg-12 form-group"><label class="control-label" for="title_'+item+'">Title <span class="text-danger">*</span></label><input type="text" name="title_'+item+'" id="title_'+item+'" class="form-control" required="required" placeholder="Title" /></div><div class="col-md-12 col-lg-12 form-group"><label class="control-label" for="keywords_'+item+'">Keywords <span class="text-danger">*</span></label><textarea name="keywords_'+item+'" id="keywords_'+item+'" class="form-control" required="required" placeholder="Keywords" rows="4"></textarea></div><div class="col-md-12 col-lg-12 form-group"><label class="control-label" for="description_'+item+'">Description <span class="text-danger">*</span></label><textarea name="description_'+item+'" id="description_'+item+'" class="form-control" required="required" rows="4" placeholder="Description"></textarea></div><div class="col-md-12 col-lg-12 form-group"><label class="control-label" for="meta_description_'+item+'">Meta Description <span class="text-danger">*</span></label><textarea name="meta_description_'+item+'" id="meta_description_'+item+'" class="form-control" rows="4" placeholder="Meta Description"></textarea></div><div class="col-md-6 col-lg-12 form-group"><label class="control-label" for="icon_image_'+item+'">Icon Image</label><input filesize="10" extension="jpg|png|jpeg|gif|bmp" type="file" name="icon_image_'+item+'" id="icon_image_'+item+'" class="form-control"><p class="help-block">Allowed File type jpg | png | jpeg | gif | bmp.</p></div></div></div></div></div>';
    //}
    return opt;
}
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
