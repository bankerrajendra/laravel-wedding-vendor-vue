@extends('adminlte::page')
@section('title', 'SMS Configuration')
@section('content_header')
    <h1>SMS Configuration</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-sms-config'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            
                            <!-- Twillio Account Sid -->
                            <div class="form-group has-feedback row ">
                                <label for="twilio_account_sid" class="col-md-3 control-label"> Twillio Account SID</label>
                                <div class="col-md-9">
                                    {!! Form::text('twilio_account_sid', $twilio_account_sid, array('id' => 'twilio_account_sid', 'class' => 'form-control', 'placeholder' => 'Account SID')) !!}
                                </div>
                            </div>
                            <!-- Twillio Account Sid -->

                            <!-- Twillio Auth Token -->
                            <div class="form-group has-feedback row ">
                                <label for="twilio_auth_token" class="col-md-3 control-label"> Twillio Auth Token</label>
                                <div class="col-md-9">
                                    {!! Form::text('twilio_auth_token', $twilio_auth_token, array('id' => 'twilio_auth_token', 'class' => 'form-control', 'placeholder' => 'Auth Token')) !!}
                                </div>
                            </div>
                            <!-- Twillio Auth Token -->

                            <!-- From Number -->
                            <div class="form-group has-feedback row ">
                                <label for="twilio_from_mobile_number" class="col-md-3 control-label"> Twillio From Mobile Number</label>
                                <div class="col-md-9">
                                    {!! Form::text('twilio_from_mobile_number', $twilio_from_mobile_number, array('id' => 'twilio_from_mobile_number', 'class' => 'form-control', 'placeholder' => 'From Mobile Number')) !!}
                                </div>
                            </div>
                            <!-- From Number -->

                            <!-- Status -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" @if($sms_status == "A")checked="checked"@endif name="sms_status" value="A">
                                            Active
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" @if($sms_status == "I")checked="checked"@endif name="sms_status" value="I">
                                            In Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- Status -->

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
