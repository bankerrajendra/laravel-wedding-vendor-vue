@extends('adminlte::page')
@section('title', 'Edit Artist >> '.$record->name)
@section('content_header')
<h1>Edit Artist >> {{$record->name}}</h1>
@stop
@section('css')
   <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .avatar-btns, .avatar-btns-poster { text-align: center; }
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-admin-edit-artist'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            {!! Form::hidden('id', $record->id) !!}
                            <div class="form-group row ">
                                <label for="name" class="col-md-3 control-label"> Name <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="">
                                        {!! Form::text('name', $record->name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="image" class="col-md-3 control-label"> Image </label>
                                <div class="col-md-9">
                                    <input filesize="10" extension="jpg|png|jpeg|gif|bmp" type="file" name="image_full" id="image_full" class="form-control">
                                    <p class="help-block">Allowed File type jpg | png | jpeg | gif | bmp.</p>
                                    @if($record->image_full != "")
                                    <p id="artist-image">
                                        <img src="{{config('constants.S3_WEDDING_IMAGES_ARTIST').$record->image_full}}" alt="image_full" width="200" border="0" />
                                        <br />
                                        <a href="{{config('constants.S3_WEDDING_IMAGES_ARTIST').$record->image_full}}" target="_blank">View Full</a>
                                    </p>
                                    <ul class="list-unstyled list-inline margin-top10" id="deleteimg-section">
                                        <li>
                                            <button type="button" class="btn btn-danger btn-xs deleteImage" data-id="{{$record->id}}">Delete</button>
                                        </li>
                                    </ul>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-3 control-label"> Approved <span class="text-danger">*</span></label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="is_approved" value="Y" @if($record->is_approved == 'Y') checked="checked" @endif>
                                            Yes
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="is_approved" value="N" @if($record->is_approved == 'N') checked="checked" @endif>
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modals.modal-delete')
@endsection

@section('js')
<script type="text/javascript">
    var deleteImage = false;
    jQuery(".deleteImage").on('click', function() {
        jQuery("#confirmDelete .modal-title").html("Delete Image");
        jQuery("#confirmDelete .modal-body").html("<p>Are you sure you want to delete this Image ?</p>");
        jQuery("#confirmDelete").modal('show');
        deleteImage = true;
    });
	
    $('#confirmDelete').on('hidden.bs.modal', function() {
        deleteImage = false;
    });
    $('#confirmDelete .modal-footer #confirm').on('click', function() {
        if (deleteImage) {
            var _token = $('input[name="_token"]').val();
            var form = new FormData();
            form.append('_token', _token);
            form.append('id', '{{$record->id}}');
            jQuery.ajax({
                url: "{{ route('admin-delete-artist-image') }}",
                data: form,
                method: "post",
                processData: false,
                contentType: false
            }).done(function(response) {
                jQuery("#artist-image").remove();
                jQuery("#deleteimg-section").remove();
                jQuery("#confirmDelete").modal('hide');
                deleteImage = false;
            });
        }
    });
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection
