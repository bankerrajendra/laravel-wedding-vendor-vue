@extends('adminlte::page')
@section('title', 'Viewing How to improve')
@section('content_header')
    <h1>Viewing How to improve</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback row ">

                                <label for="" class="col-md-3 control-label"> Subject</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {{$improve->subject}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group has-feedback row ">

                                <label for="" class="col-md-3 control-label"> Message</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {{$improve->message}}
                                    </div>
                                </div>
                            </div>
                            
                            <!-- User -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> User</label>

                                <div class="col-md-9">
                                    <a href="{{ URL::to('users/' . $improve->user_id . '/edit') }}"><img src="{{$improve->user->getVendorProfilePic()}}" alt="" width="100"></a><br />
                                    <strong>{{$improve->user->first_name}} {{$improve->user->last_name}}</strong>
                                </div>
                            </div>
                            <!-- User -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection
