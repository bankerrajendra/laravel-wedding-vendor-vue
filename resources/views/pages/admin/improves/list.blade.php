@extends('adminlte::page')
@section('title')
    Showing How we can improve records.
@endsection
@section('content_header')
    <h1>How we can improve?</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
        .trf {
            transform: rotate(61deg);
            -ms-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            /* -webkit-transform: rotate(90deg); */
            -o-transform: rotate(90deg);
            margin-top: 20px;
        }
        .mb15 {
            margin-bottom: 15px;
        }
        .mb25 {
            margin-bottom: 25px;
        }
        .ml15 {
            margin-left: 15px;
        }
    </style>
@endsection

@section('content')
<div class="main-content">
	<div class="panel mb25">
    	<div class="panel-body">
        	<div class="row">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            	<form method="get" action="{{ route('admin-improves-list') }}" name="search-action-frm">
                    <div class="col-sm-12">
                        <a id="action-delete" href="#" class="btn btn-danger mb15 ml15"><i class="fa fa-trash fa-fw"></i>&nbsp;&nbsp;Delete&nbsp;</a>
                    </div>
                    <div class="col-sm-3">
                        <label>
                            Show
                        </label>
                        <label>
                            <select name="per_page" id="per_page" class="form-control">
                                <option @if($per_page == "1")selected="selected"@endif value="1">1</option>
                                <option @if($per_page == "2")selected="selected"@endif value="2">2</option>
                                <option @if($per_page == "3")selected="selected"@endif value="3">3</option>
                                <option @if($per_page == "5")selected="selected"@endif value="5">5</option>
                                <option @if($per_page == "10")selected="selected"@endif value="10">10</option>
                                <option @if($per_page == "20")selected="selected"@endif value="20">20</option>
                                <option @if($per_page == "50")selected="selected"@endif value="50">50</option>
                                <option @if($per_page == "100")selected="selected"@endif value="100">100</option>
                            </select>
                        </label>
                        <label>
                            entries
                        </label>                    
                    </div>
                    <div class="col-sm-2">
                        
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-4 input-group mb15">
                        <input type="search" name="search" id="search_field" class="form-control clearable x" value="{{$search}}" placeholder="Search here..">
                        <span class="input-group-btn pr10">
                        <button type="submit" class="btn btn-primary">Search</button>
                        </span>
                    </div>
                </form>
            </div>
            
            <div class="row">
            	<div class="col-sm-12">
                    <div class="table-responsive">
                    {!! Form::open(array('route' => array('handle-manage-bulk-improves'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'name' => 'bulk-action-frm')) !!}
                    {!! Form::hidden('action', '', array('id' => 'action')) !!}
                        <table class="table table-bordered bordered table-striped table-condensed datatable">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="cb-checkbox mb0" id="all_check"><span class="cb-inner"><i><input class="all_check " type="checkbox" id="all"></i></span>
                                        </label>
                                    </th>
                                    <th>View</th>
                                    <th style="max-width:20%;min-width:15%">Subject</th>
                                    <th style="width:40%;">Message</th>
                                    <th style="max-width:20%;min-width:15%">User</th>
                                    <th style="max-width:20%;min-width:15%">Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @if($improves->total() > 0 && !empty($improves))
                                        @foreach($improves as $improve)
                                        <tr>
                                            <td class="checkbox-row"><label class="cb-checkbox"><span class="cb-inner"><i><input type="checkbox" class="improve_ids" name="improve_ids[]" value="{{$improve->id}}"></i></span></label>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin-improves-view', [$improve->id]) }}"><i class="fa fa-eye fa-fw"></i></a>
                                            </td>
                                            <td>
                                                {{$improve->subject}}
                                            </td>
                                            <td>
                                                {{$improve->message}}
                                            </td>
                                            <td><a href="{{ URL::to('users/' . $improve->user_id . '/edit') }}"><img src="{{$improve->user->getVendorProfilePic()}}" alt="" width="100"></a><br />{{$improve->user->first_name}} {{$improve->user->last_name}}</td>
                                            <td>{{ date("F d, Y h:i A", strtotime($improve->created_at))}}</td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                        </table>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div>
            	<div class="pull-left">
                    <div id="show_record_message">
                        Showing {{$improves->total()}} entries
                    </div>
                </div>
                <div class="pull-right">
                    {{ $improves->appends(['search' => $search, 'per_page' => $per_page])->links() }}               
                </div>
    	    </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript">
        $('#per_page').on('change', function() {
            $("form[name='search-action-frm']").submit();
        });
        $('.all_check').click(function () {    
            $('input:checkbox').prop('checked', this.checked);    
        });
        $('#action-delete').on('click', function() {
            bulkActionSubmit('delete','Are you sure you want to delete selected entries!');
        });
        function bulkActionSubmit(action_val, message) {
            if($('input[name="improve_ids[]"]:checked').length > 0) {
                if(confirm(message)) {
                    $('#action').val(action_val);
                    $("form[name='bulk-action-frm']").submit();
                }
            } else {
                alert('Select atleast one entry.');
            }
        }
    </script>
@endsection
