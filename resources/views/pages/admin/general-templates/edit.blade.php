@extends('adminlte::page')
@section('title', 'Edit '.$page_title)
@section('content_header')
    <h1>Edit {{$page_title}}</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-edit-template'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            {!! Form::hidden('type', $type) !!}
                            {!! Form::hidden('id', $template->id) !!}
                            <div class="form-group has-feedback row ">

                                <label for="name" class="col-md-3 control-label"> {{$page_title}} Title</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('name', $template->name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => $page_title . ' title')) !!}
                                    </div>
                                </div>
                            </div>
                            @if($type == "email")
                            <!-- Subject -->
                            <div class="form-group has-feedback row ">

                                <label for="subject" class="col-md-3 control-label"> Subject</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('subject', $template->subject()->subject, array('id' => 'subject', 'class' => 'form-control', 'placeholder' => 'Add Subject')) !!}
                                    </div>
                                </div>
                            </div>
                            <!-- Subject -->
                            @endif

                            <!-- Description -->
                            <div class="form-group has-feedback row ">
                                <label for="description" class="col-md-3 control-label"> Description</label>
                                <div class="col-md-9">
                                    <textarea id="description" name="description" cols="60" rows="10">{{$template->description}}</textarea>
                                </div>
                            </div>
                            <!-- Description -->

                            <!-- Status -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <label>
                                            <input type="radio" name="status" value="A" @if($template->status == "A")checked="checked"@endif>
                                            Active
                                        </label>
                                        &nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="status" value="I" @if($template->status == "I")checked="checked"@endif>
                                            In Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- Status -->

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    @if($type != "sms")
        <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
        <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
        <script>
            CKEDITOR.config.allowedContent=true;
            $('textarea').ckeditor();
        </script>
    @endif
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
