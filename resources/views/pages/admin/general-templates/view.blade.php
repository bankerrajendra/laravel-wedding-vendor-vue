@extends('adminlte::page')
@section('title', 'View '.$page_title)
@section('content_header')
    <h1>View {{$page_title}}</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback row ">

                                <label for="title" class="col-md-3 control-label"> {{$page_title}} Title</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {{$template->name}}
                                    </div>
                                </div>
                            </div>
                            @if($template->type == "email")
                            <div class="form-group has-feedback row ">
                                <label for="status" class="col-md-3 control-label"> Subject</label>
                                <div class="col-md-9">
                                    {{$template->subject()->subject}}
                                </div>
                            </div>
                            @endif

                            <!-- Description -->
                            <div class="form-group has-feedback row ">
                                <label for="status" class="col-md-3 control-label"> Description</label>
                                <div class="col-md-9">
                                    {!! $template->description !!}
                                </div>
                            </div>
                            <!-- Description -->

                            <!-- Status -->
                            <div class="form-group has-feedback row ">

                                <label class="col-md-3 control-label"> Status</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        @if($template->status == "A")
                                            Active
                                        @elseif($template->status == "I")
                                            In Active
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- Status -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection
