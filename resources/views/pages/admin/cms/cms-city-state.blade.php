{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title', 'CMS City State')
@section('content_header')
    <h1>CMS City State Records</h1>
@stop
{{--@section('template_title')--}}
    {{--@lang('usersmanagement.showing-all-users')--}}
{{--@endsection--}}

{{--@section('template_linked_css')--}}
@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">

    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="box">
                    <div class="box-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                Showing All <strong>{{$page_type}}</strong> Records
                            </span>

                            <div class="btn-group pull-right btn-group-xs">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 18px">
                                    <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                    <span class="sr-only">
                                        @lang('usersmanagement.users-menu-alt')
                                    </span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{route('cms-city-states',['name'=>config('constants.cms.country')])}}">
                                        <i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>Country</a>
                                    <a class="dropdown-item" href="{{route('cms-city-states',['name'=>config('constants.cms.city')])}}">
                                        <i class="fa fa-fw fa-group" aria-hidden="true"></i>City</a>
                                    <a class="dropdown-item" href="{{route('cms-city-states',['name'=>config('constants.cms.state')])}}">
                                        <i class="fa fa-fw fa-group" aria-hidden="true"></i>State</a>
                                    <a class="dropdown-item" href="{{route('cms-city-states',['name'=>config('constants.cms.language')])}}">
                                        <i class="fa fa-fw fa-group" aria-hidden="true"></i>Language</a>
                                    <a class="dropdown-item" href="{{route('cms-city-states',['name'=>config('constants.cms.religion')])}}">
                                        <i class="fa fa-fw fa-group" aria-hidden="true"></i>Religion</a>
                                    <a class="dropdown-item" href="{{route('cms-city-states',['name'=>config('constants.cms.community')])}}">
                                        <i class="fa fa-fw fa-group" aria-hidden="true"></i>Sect</a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="box-body">

                        {{--@if(config('usersmanagement.enableSearchUsers'))--}}
                            {{--@include('partials.search-users-form')--}}
                        {{--@endif--}}

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table">
                                <caption id="user_count">
                                    {{ trans_choice('usersmanagement.users-table.caption', 1, ['userscount' => $cmscitystate->count()]) }}
                                </caption>
                                <thead class="thead">
                                <tr>
                                    <th>Id</th>
                                    <th class="hidden-xs" >Page Type</th>
                                    <th>Page Name</th>
                                    <th class="hidden-xs">Slug</th>
                                    <th class="hidden-xs">Page Description</th>
                                    <th>Status</th>
                                    <th class="hidden-sm hidden-xs hidden-md">created On</th>
                                    <th class="hidden-sm hidden-xs hidden-md">Updated On</th>
                                    <th>Action</th>
                                    {{--<th class="no-search no-sort"></th>--}}
                                    {{--<th class="no-search no-sort"></th>--}}
                                </tr>
                                </thead>
                                <tbody id="cmscitystate_table">
                                {{$i=1}}
                                @foreach($cmscitystate as $cms)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$cms->page_type}}</td>
                                        <td>{{$cms->type_name}}</td>
                                        <td class="hidden-xs">{{$cms->slug}}</td>
                                        <td class="hidden-xs">{{$cms->page_description}}</td>
                                        <td>@if($cms->status=="1")
                                                Active
                                            @else
                                                Inactive
                                            @endif</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{$cms->created_at}}</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{$cms->updated_at}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ route('set-cms-city-states', ['id'=>$cms->id]) }}" data-toggle="tooltip" title="Edit">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                {{--<tbody id="search_results"></tbody>--}}
                                {{--@if(config('usersmanagement.enableSearchUsers'))--}}
                                    {{--<tbody id="search_results"></tbody>--}}
                                {{--@endif--}}

                            </table>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--@include('modals.modal-delete')--}}

@endsection

@section('js')
    @if ((count($cmscitystate) > config('usersmanagement.datatablesJsStartCount')) && !config('usersmanagement.enableCmsCityPagination'))
        @include('scripts.datatables')
    @endif
    {{--@if(config('usersmanagement.enableSearchUsers'))--}}
        {{--@include('scripts.search-users')--}}
    {{--@endif--}}
@endsection
