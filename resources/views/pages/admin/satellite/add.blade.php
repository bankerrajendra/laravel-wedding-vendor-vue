@extends('adminlte::page')
@section('title', 'Add Satellite')
@section('content_header')
    <h1>Add Satellite</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
@endsection
@section('navclasses')background-black outer-nav  @endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-add-satellite'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}

                            <div class="form-group row ">

                                <label for="title" class="col-md-3 control-label"> Title</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('title', '', array('id' => 'title', 'class' => 'form-control', 'placeholder' => 'Title')) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row ">
                                <label for="url" class="col-md-3 control-label"> URL</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('url', '', array('id' => 'url', 'class' => 'form-control', 'placeholder' => 'URL')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="logo" class="col-md-3 control-label"> Logo </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="file" class="avatar-input" id="logo" name="logo" >
                                        <small>allowed extensions: <i>jpeg, jpg, png, gif, bmp</i> Image size max: <i>2 MB</i>, Dimensions max: <i>500px x 500px</i></small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="favicon" class="col-md-3 control-label"> Favicon </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="file" class="avatar-input" id="favicon" name="favicon" >
                                        <small>allowed extensions: <i>jpeg, jpg, png, gif, bmp</i> Image size max: <i>1 MB</i>, Dimensions max: <i>100px x 100px</i></small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="status" class="col-md-3 control-label"> Status</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="status" id="status" >
                                            <option value="1" selected="selected">Active</option>
                                            <option value="0" >Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'submit')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
