{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title')
    Reports
@endsection
@section('content_header')
    <h1>Reports</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                               User Reports
                            </span>
                        </div>
                    </div>

                    <div class="box-body">

                        <div class="">
                            <table class="table table-striped table-sm ">

                                <thead class="thead">
                                <tr>
                                    <th>ID</th>
                                    <th>From User</th>
                                    <th>To User</th>
                                    <th>Reason</th>
                                    <th>Description</th>
                                    <th>Reply</th>
                                    <th class="no-search no-sort">Action</th>
                                </tr>
                                </thead>
                                <tbody id="users_table">
                                @foreach($reports as $report)
                                    @php
                                    $from_user = $report->from_user();
                                    $to_user = $report->to_user();
                                    @endphp
                                    <tr>
                                        <td>{{$pageNo++}}</td>
                                        <td><a href="/users/{{$from_user->id}}/edit"  target="_blank"><img src="{{$from_user->getVendorProfilePic()}}" width="100"></a><br/><strong>{{$from_user->name}}</strong>
                                            @if($from_user->deleted_at !== null)  <span style="color: red;"><i class="fa fa-remove"></i> Deleted </span> @endif</td>
                                        <td><a href="/users/{{$to_user->id}}/edit"  target="_blank"><img src="{{$to_user->getVendorProfilePic()}}" width="100"></a><br/><strong>{{$to_user->name}}</strong>
                                            @if($to_user->deleted_at !== null)  <span style="color: red;"><i class="fa fa-remove"></i> Deleted </span> @endif</td>
                                        <td>{{$report->reason}}</td>
                                        <td>{{$report->description}}</td>
                                        <td>
                                            <b>Admin</b>:
                                            <br /> 
                                            @if($report->admin_reply != "")
                                                {{$report->admin_reply}}
                                            @else
                                                <div id="adminReplySec-{{$report->id}}">
                                                <textarea cols="40" rows="4" name="admin_reply" id="admin-reply-{{$report->id}}"></textarea>
                                                <br /><input type="button" class="btn btn-small btn-success submitAdminReportReply" name="submit-admin-reply" id="adminReply-{{$report->id}}" value="Submit" data-report-id="{{$report->id}}" />
                                                </div>
                                            @endif
                                            @if($report->to_user_reply != "")
                                                <br />
                                                <b>User</b>: 
                                                <br />
                                                {{$report->to_user_reply}}
                                            @endif
                                        </td>
                                        <td>
                                            <a onclick="javascript:return confirm('Are you sure you want to delete this item?');" href="{{route('report-report-trash', [ $report->id ])}}" class="btn btn-sm btn-danger btn-block">Delete</a>
                                            <br />
                                            @if($to_user->banned == 0)
                                            <a class="btn btn-sm btn-warning btn-block" href="{{ URL::to('users/' . $to_user->id . '/ban') }}" data-toggle="tooltip" title="Ban User">Ban User
                                            </a>
                                            @else
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('users/' . $to_user->id . '/unban') }}" data-toggle="tooltip" title="UnBan User">UnBan User
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @if(config('usersmanagement.enablePagination'))
                                {{ $reports->links() }}
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')
@csrf
@endsection

@section('js')
<script type="text/javascript">
jQuery(".submitAdminReportReply").on('click', function(e) {
    var url = '{{route("report-admin-reply")}}';
    var report_id = $(this).data('reportId');
    var admin_reply = $("#admin-reply-"+report_id).val();
    var _token = $('input[name="_token"]').val();
    if(admin_reply == "") {
        alert("Admin Reply cann't blank");
        return false;
    }
    if(report_id == "") {
        alert("Report ID cann't blank");
        return false;
    }
    $.ajax({
        url:url,
        method:"POST",
        data:{ 
            report_id: report_id,
            admin_reply: admin_reply,
            _token:_token
        },
        success:function(result)
        {
           if(result.status == 1) {
                $("#admin-reply-"+report_id).before('<span class="text text-success">'+result.message+'</span><br />');
                $("#adminReplySec-"+report_id).empty();
                $("#adminReplySec-"+report_id).html(admin_reply);
           } else {
                $("#admin-reply-"+report_id).before('<span class="text text-danger">'+result.message+'</span><br />');
           }
        }
    })
});
</script>
@endsection
