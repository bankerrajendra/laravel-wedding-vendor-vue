@extends('layouts.app')
@section('template_title')
    Unsubscribed
@endsection
@section('head')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center aboout panel-body">
                <h2>Unsubscribed</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 panel-body">
                @if($success != "")
                    <div class="alert alert-success" role="alert">
                        {{$success}}
                    </div>
                @endif
                @if($error != "")
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endif
            </div>
        </div>
    </div>



@endsection
@section('footer_scripts')
@endsection