@extends('layouts.app')

@section('template_title')
    Payment Cancel
@endsection
@section('navclasses')

@endsection
@section('template_fastload_css')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 pay-action failed text-center panel-body">
            <img src="{{asset('img/failed.png')}}">
            <h3>Payment Canceled</h3>
            <a href="{{route('my-membership')}}" class="btn btn-warning">Continue</a>
		</div>
    </div>
</div>
@endsection
@section('footer_scripts')
@endsection