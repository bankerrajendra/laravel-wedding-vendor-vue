@extends('layouts.app')
@section('template_title')
    Reviews
@endsection
@section('head')
@endsection
@section('content')
<section>
	<div class="container review-add" id="add-review-section">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="modal-content border-theme reg_modal">
					<div class="panel-body w_rite" id="write">
                        <h3>Write a review</h3>
                        @if(session('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form role="form" method="POST" action="{{ route('handle-review-submit') }}" name="userRating" id="userRating">
                        @csrf
							<div class="form-group">
								<textarea class="form-control" id="message" name="message" value="" placeholder="Message" minlength="10" rows="40" style="height:200px;"></textarea>					
							</div>
							<div class="form-group" id="rating-ability-wrapper">
								<h4 class="bold rating-header" style=""><span class="selected-rating">0</span> / 5</h4>
								<button type="button" class="btnrating btn btn-default btn-sm" data-attr="1" id="rating-star-1">
									<i class="fa fa-star" aria-hidden="true"></i>
								</button>
								<button type="button" class="btnrating btn btn-default btn-sm" data-attr="2" id="rating-star-2">
									<i class="fa fa-star" aria-hidden="true"></i>
								</button>
								<button type="button" class="btnrating btn btn-default btn-sm" data-attr="3" id="rating-star-3">
									<i class="fa fa-star" aria-hidden="true"></i>
								</button>
								<button type="button" class="btnrating btn btn-default btn-sm" data-attr="4" id="rating-star-4">
									<i class="fa fa-star" aria-hidden="true"></i>
								</button>
								<button type="button" class="btnrating btn btn-default btn-sm" data-attr="5" id="rating-star-5">
									<i class="fa fa-star" aria-hidden="true"></i>
                                </button>
                                <input type="hidden" id="selected_rating" name="selected_rating" value="" />
							</div>
							<button type="submit" id="submit" class="btn btn-success">Submit</button>
                            <a href="{{ route('reviews') }}"><button type="button" class="btn btn-default">Back</button></a>
						</form>
					  </div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('footer_scripts')
<script type="text/javascript">
    jQuery(document).ready(function($){
	    $.validator.setDefaults({ ignore: '' });
        $(".btnrating").on('click',(function(e) {
        
            var previous_value = $("#selected_rating").val();
            
            var selected_value = $(this).attr("data-attr");
            $("#selected_rating").val(selected_value);
            
            $(".selected-rating").empty();
            $(".selected-rating").html(selected_value);
            
            for (i = 1; i <= selected_value; ++i) {
                $("#rating-star-"+i).toggleClass('btn-warning');
                $("#rating-star-"+i).toggleClass('btn-default');
            }
            
            for (ix = 1; ix <= previous_value; ++ix) {
                $("#rating-star-"+ix).toggleClass('btn-warning');
                $("#rating-star-"+ix).toggleClass('btn-default');
            }
        
        }));	
    });
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection