@extends('layouts.app')
@section('template_title')
    User's All Reviews
@endsection
@section('head')
@endsection
@section('content')
<section>
    <div class="container">
        <div class="col-sm-10 col-sm-offset-1">
			<div class="row">
				@if(session('success'))
					<div class="alert alert-success" role="alert">
						{{session('success')}}
					</div>
				@endif
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
			<div class="row">
				<div class="col-sm-6 col-xs-6 re_view">
					<h3>All Reviews</h3>
				</div>
				<div class="col-sm-6 col-xs-6">
					<a href="{{ route('add-review') }}" class="btn btn-danger pull-right"><i class="fa fa-pencil"></i> Write a review</a>
				</div>
			</div>
			@if($reviews->count() > 0)
				@foreach ($reviews as $review)
				<div class="media">
					<a class="pull-left" href="{{$review->user->profileLink()}}">
						<img class="media-object img-circle" src="{{$review->user->getProfilePicForGuest()}}" width="80" height="80" alt="">
					</a>
					<div class="pull-right">
						<strong>
						@if($review->approved == 1)
							Approved
						@else
							Pending for approval
						@endif
						</strong>
					</div>
					<h4 class="media-heading">{{$review->user->first_name}} {{$review->user->last_name}}</h4>
					<ul class="list-inline list-unstyled">
						<li>
							@for ($i = 1; $i <= 5; $i++)
								@php
									$star_state = 'fa-star-o';
									if($review->rating >= $i) :
										$star_state = 'fa-star';
									endif
								@endphp
								<span class="fa {{$star_state}}"></span>	
							@endfor
						</li>
						<li>|</li>
					<li><span><i class="glyphicon glyphicon-calendar"></i> {{$review->created_at->diffForHumans()}} </span></li>
					</ul>
				<p>{{$review->message}}</p>
				</div>
				@endforeach
				<div class="col-sm-12">{{ $reviews->links() }}</div>
			@endif
		</div>
    </div>
</section>
@endsection
@section('footer_scripts')
@endsection