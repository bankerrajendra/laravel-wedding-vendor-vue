@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.lightbox')}}.css">
    <link href="{{asset('css/bootstrap-datepicker')}}.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/lighbox-gallery.css')}}">
    <link rel="stylesheet" href="{{asset('css/YouTubePopUp.css')}}">
    <style type="text/css">
    .sim-p a h5 {
        color: #000;
        text-align: center;
        padding-top: 0px;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
        padding-left: 10px;
        padding-right: 10px;
    }
    </style>
@endsection
@section('content')
    <div class="vendor-detail">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{route('search-events', ['category' => $event->getEventCategoryDetail($category_slug)->slug])}}">{{$event->getEventCategoryDetail($category_slug)->name}}</a></li>
                <li class="active text-dark">
                    {{$event->event_name}}
                </li>
            </ol>
            <div class="row">
                <div class="col-sm-12">
                    @if($selfEvent == true && $event->is_approved == 0)
                        <div class="text-center">
                            <div class="alert alert-info">Event is pending for approval.</div>
                        </div>
                    @endif
                    <div class="vendor-banner">
                        @if(@$event->getCoverPic($selfEvent))
                            <img src="{{$event->getCoverPic($selfEvent)}}" class="img-responsive img-thumbnail" style="width:100%;" alt="" />
                        @endif
                    </div>
                </div>
                <div class="col-sm-8">	
                    <div class="vendor-name">
                        <h1>
                            <i>
                                {{$event->event_name}}, {{$event->venue}}
                                @if($vendorObj->getProfileFieldExistsLoggedVendor('company_name', 'vi', $selfEvent))
                                    <br> By - {{$vendorObj->getProfileFieldExistsLoggedVendor('company_name', 'vi', $selfEvent)}}
                                @endif
                            </i>
                        </h1>
                        <p>
                            @php
                                $average_rating = $event->getAverageReviewRating();
                                $total_review = $event->getTotalReview();
                            @endphp

                            @for($r=1;$r<=5;$r++)
                            <i class="fa @if($r <= $average_rating) fa-star @elseif($r >= $average_rating-0.5 && $r <= $average_rating+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                            @endfor
                        </p>
                        <div class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" style="color:#000;">( {{$total_review}} @if( $total_review > 1)Reviews @else Review @endif)</a>
                            <div class="dropdown-menu text-center" style="width: 330px;">
                                <div class="col-sm-12 col-xs-12 panel-body">
                                    @for($br=1;$br<=5;$br++)
                                        <div class="side">
                                            <div class="">
                                                <a href="{{ route('show-events-reviews', ['eventId' => $event->getEncryptedId(), 'rate' => $br]) }}" onclick="javascript:getPerRatingReviews({{$br}});">{{$br}} star</a>
                                            </div>
                                        </div>
                                        <div class="middle">
                                            <div class="bar-container">
                                                <div class="bar-{{$event->getSpcificStartBarPer($br)}}"></div>
                                            </div>
                                        </div>
                                        <div class="side right">
                                            <div><a href="{{ route('show-events-reviews', ['eventId' => $event->getEncryptedId(), 'rate' => $br] )}}" onclick="javascript:getPerRatingReviews({{$br}});">{{$event->getCountReviewStarSpecific($br)}}</a></div>
                                        </div>
                                    @endfor
                                </div>
                                <div class="col-sm-12 col-xs-12 text-center">
                                    <a href="{{ route('show-events-reviews', ['eventId' => $event->getEncryptedId()] )}}" class="btn btn-default btn-rev">Read More</a>
                                </div>
                            </div>
                        </div>
                        @if($selfEvent == false)<span class="hidden-lg hidden-md hidden-sm"><a href="{{ route('write-event-review', [$event->getEncryptedId()]) }}">Write a Review</a></span>
                        @endif
                        <p></p>  
                        @if($selfEvent == false)<p class="hidden-xs"><span><a href="{{ route('write-event-review', [$event->getEncryptedId()]) }}">Write a Review</a></span></p>@endif
                        <ul class="add-list">
                            <li><a href="#location"> <i class="fa fa-map-marker" aria-hidden="true"></i> Map</a></li>
                            @if($selfEvent == true)
                                @if($vendorObj->vendors_information->display_business_number == 1)
                                <li class="hidden-xs"><a href="javascript:void(0);" data-toggle="popover" data-placement="bottom" data-content="+{{getPhoneCode($vendorObj->mobile_country_code)}} {{$vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', $selfEvent)}}"> <i class='fa fa-phone' aria-hidden='true'></i> Phone Number</a></li>
                                <li class="hidden-md hidden-sm hidden-lg"><a href="tel:+{{getPhoneCode($vendorObj->mobile_country_code)}} {{$vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', $selfEvent)}}"> <i class='fa fa-phone' aria-hidden='true'></i> Phone Number</a></li>
                                @endif
                            @else
                                @if($vendorObj->is_paid == 1 && $vendorObj->vendors_information->display_business_number == 1)
                                    @if($vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', false))
                                        <li class="hidden-xs"><a href="javascript:void(0);" data-toggle="popover" data-placement="bottom" data-content="+{{getPhoneCode($vendorObj->mobile_country_code)}} {{$vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', false)}}"> <i class='fa fa-phone' aria-hidden='true'></i> Phone Number</a></li>
                                        <li class="hidden-md hidden-sm hidden-lg"><a href="tel:+{{getPhoneCode($vendorObj->mobile_country_code)}} {{$vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', $selfEvent)}}"> <i class='fa fa-phone' aria-hidden='true'></i> Phone Number</a></li>
                                    @endif
                                @endif
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">	
                    <div class="vendor-name pull_right">
                        <ul class="v_social">
                            @if($event->facebook != "")
                            <li class="fb"><a target="_blank" href="{{$event->facebook}}"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            @endif
                            @if($event->twitter != "")
                            <li class="tw"><a target="_blank" href="{{$event->twitter}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            @endif
                            @if($event->linkedin != "")
                            <li class="lin"><a target="_blank" href="{{$event->linkedin}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            @endif
                            @if($event->pinterest != "")
                            <li class="pin"><a target="_blank" href="{{$event->pinterest}}"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                            @endif
                            @if($event->instagram != "")
                            <li class="inst"><a target="_blank" href="{{$event->instagram}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="event-date">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="eventd">
                                    @php
                                        $start_date = new DateTime($event->start_date);
                                        $end_date = new DateTime($event->end_date);
                                        $start_date_timestamp = $start_date->getTimestamp();
                                        $end_date_timestamp = $end_date->getTimestamp();

                                        $expl_event_edate = explode('-', $event->end_date);
                                        $expld_end_time = explode(' ', $expl_event_edate[2]);
                                        $haveEndTime = true;
                                        if($expld_end_time[1] == "00:00:00") {
                                            $haveEndTime = false;
                                        }

                                    @endphp
                                    <p> 
                                        @if(date('Y-m-d', $start_date_timestamp) == date('Y-m-d', $end_date_timestamp))
                                            {{date('j F, Y', $start_date_timestamp)}}
                                        @else
                                            {{date('j F, Y', $start_date_timestamp)}} - 
                                            {{date('j F, Y', $end_date_timestamp)}}
                                        @endif
                                        - Timing <span>{{date('h:i A', $start_date_timestamp)}}@if($haveEndTime == true) - {{date('h:i A', $end_date_timestamp)}}@endif</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <br class="hidden-xs">
                <div class="col-sm-8 our-gal">
                    <div class="btn_group">
                        @if($selfEvent == false && $allowSendMessage == true)
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#sendMessageRequest"><i class="fa fa-envelope-o" aria-hidden="true"></i> Message</button>
                        @endif
                    </div>
                    @if($selfEvent == false && $allowSendMessage == true)
                        <!-- Modal -->
                        <div id="sendMessageRequest" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h3 class="modal-title" style="font-family: 'Merienda One', cursive!important;">MESSAGE EVENT VENDOR <br><span style="color: #ed38a0!important;">{{$event->event_name}}</span></h3>
                                        <hr>
                                        <div class="alert alert-danger show-message-request-error" style="display: none;"></div>
                                        <div class="alert alert-success show-message-request-success" style="display: none;"></div>

                                        <form name="submit-new-message-request" id="submit-new-message-request" action="{{ route('handle-event-message-request') }}" method="post">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>First Name *</label>
                                                        <input type="text" class="form-control" placeholder="First Name" id="first-name-msg" name="first_name" @if(!empty($form_prepopulate) && $form_prepopulate['first_name'] != "")value="{{$form_prepopulate['first_name']}}"@endif>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Last Name *</label>
                                                        <input type="text" class="form-control" placeholder="Last Name" id="last-name-msg" name="last_name" @if(!empty($form_prepopulate) && $form_prepopulate['last_name'] != "")value="{{$form_prepopulate['last_name']}}"@endif >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Phone Number</label>
                                                        <input type="text" class="form-control" placeholder="Phone Number" id="mobile-number-msg" name="mobile_number" @if(!empty($form_prepopulate) && $form_prepopulate['mobile_number'] != "")value="{{$form_prepopulate['mobile_number']}}"@endif>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(Auth::check() == false)
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Email *</label>
                                                        <input type="email" id="email-msg" name="email" class="form-control" placeholder="Email" @if(!empty($form_prepopulate) && $form_prepopulate['email'] != "")value="{{$form_prepopulate['email']}}"@endif >
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Password *</label>
                                                        <input type="password" id="password-msg" name="password" class="form-control" placeholder="Password">
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                                <label class="lb">Preferred Contact Method *</label>
                                                <br>
                                                <label class="checkbox-inline"><input type="checkbox" id="check_email_contact" name="check_email_contact" value="1" checked="checked"><span class="checkmark email-chk"></span> Email</label>
                                                <label class="checkbox-inline"><input type="checkbox" id="check_phone_contact" name="check_phone_contact" value="1"><span class="checkmark phone-chk"></span> Phone</label>
                                                <label class="checkbox-inline"><input type="checkbox" id="check_message_contact" name="check_message_contact" value="1"><span class="checkmark message-chk"></span> Text Message</label>
                                            </div>
                                            <div class="form-group">
                                                <label for="message">Message:</label>
                                                <textarea id="message" name="message" class="form-control" style="height: 100px;"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-default" name="submit-btn-message-request">SUBMIT</button>
                                            <div class="row">
                                                <div class="col-sm-12" style="margin-left: 50px; margin-top: 10px;">
                                                    <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none;">
                                                </div>
                                            </div>
                                            <input type="hidden" name="vendor_id" value="{{base64_encode($event->vendor_id)}}" />
                                            <input type="hidden" name="event_id" value="{{$event->getEncryptedId()}}" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <!-- Mobile Videos Starts -->
                    @if($event->video_url != '')
                    <div class="row hidden-sm hidden-lg hidden-md">
                        <div class="col-sm-12">
                            <ul class="video-sec">
                                <li><a class="bla-1" href="{{$event->video_url}}"><img src="{{ asset('img/video-img.jpg') }}" class="img-responsive"></a></li>
                            </ul>
                        </div>
                    </div>
                    @endif
                    <!-- Mobile Videos Ends -->
                </div>
                @if(strip_tags($event->description) != '')
                <div class="col-sm-12 ab_content" style="margin-top:0px;border-top:none;">
                    <div class="panel-heading"><h3><span>Description</span></h3></div>
                    <p>{!!$event->description!!}</p>
                </div>
                @endif
            </div>
            <div class="row event-o-detail">
                <div class="col-sm-6">
                <h3>Current Events</h3>
                    <span class="h-line"></span>
                    @if($event->getPosterPic($selfEvent) != null)
                        <img src="{{ $event->getPosterPic(true) }}" class="img-responsive" style="margin-top:20px;">
                    @endif
                    <ul class="f-event">
                        @if($event->online_seat_selection == 1) <li><img src="{{asset('img/chair.png')}}"></li> @endif
                        @if($event->drinks_for_sale == 1) <li><img src="{{asset('img/glass.png')}}"></li> @endif
                        @if($event->parking == 1) <li><img src="{{asset('img/parking.png')}}"></li> @endif
                        @if($event->food_for_sale == 1) <li><img src="{{asset('img/fork.png')}}"></li> @endif
                        @if($event->babysitting_services == 1) <li><img src="{{asset('img/baby-sitter.png')}}"></li> @endif
                    </ul>
                </div>	
                <div class="col-sm-6">
                    @if($event->video_url != '')
                    <h3>Video</h3>
                        <span class="h-line"></span>
                        <br>
                        <iframe style="width:100%; height: 227px;" src="https://www.youtube.com/embed/{{GetYouTubeId($event->video_url)}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    @endif
                </div>
                @if(strip_tags($event->ticket_information) != '')
                <div class="col-sm-12 ab_content" style="margin-top:20px;">
                    <div class="panel-heading"><h3><span>Ticket Information</span></h3></div>
                    <p>{!!$event->ticket_information!!}</p>
                </div>
                @endif
            </div>
            
            <div class="v_address hidden-xs">
                <div class="row">
                    <div class="vl"></div>
                    <div class="col-sm-4 panel-body">
                        <h2>Contact Info</h2>
                        @if($selfEvent == false && $allowSendMessage == true)
                            <p> <i class="fa fa-envelope" aria-hidden="true"></i> <a data-toggle="modal" href="#sendMessageRequest">Send A Message</a></p>
                        @endif

                        @if($event->business_website != '')
                        <p><i class="fa fa-globe" aria-hidden="true"></i> <a href="{{$event->business_website}}" target="_blank">{{$event->business_website}}</a></p>
                        @endif

                        @if($event->address != '')<p><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="#location">Location</a></p>@endif
                    </div>
                    <div class="vl1"></div>
                    <div class="col-sm-4 panel-body">
                        <h2>Request Message</h2>
                        @if($selfEvent == false && $allowSendMessage == true)<a data-toggle="modal" href="#sendMessageRequest" class="btn btn-default">SEND MESSAGE</a>@endif
                    </div>
                    @if($event->ticket_url != '')
                    <div class="col-sm-4 panel-body">
                        <h2>Ticket</h2>
                          <a href="{{$event->ticket_url}}" target="_blank" class="btn btn-warning ">BUY TICKETS</a>                
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="v_reviews" id="reviews">
            @if(isset($reviews['reviews']) && count($reviews['reviews']) > 0)
                <div class="container">
                    <div class="row" id="reviews">
                        <div class="col-sm-12">
                            <h3>Reviews </h3>
                            @if($selfEvent == false)
                                <a href="{{ route('write-event-review', [$event->getEncryptedId()]) }}" class="review-btn pull-right"><i class="fa fa-pencil" aria-hidden="true"></i> Write a Review</a>
                            @endif
                        </div>
                        <div class="col-sm-12">
                            <div id="events-reviews-results">
                                @include('pages.user.ajax.show-events-reviews')
                            </div>
                            <div class="media">
                                <div class="media-body text-center">
                                    <a href="{{route('show-events-reviews', ['eventId' => $event->getEncryptedId()])}}" class="btn btn-default">Show More </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="no-review">
                    <h4>No Review @if($selfEvent == false)<b><a href="{{ route('write-event-review', [$event->getEncryptedId()]) }}">Write a Review</a></b>@endif</h4>
                </div>
            @endif
        </div>
        <div class="v_videos">
            <div class="container">
                <div class="row" id="location">
                    <div class="col-sm-12">
                        <h3>Map</h3>
                        <p class="text-center">{{$event->address}}, {{$event->city->name}}, {{$event->state->name}}, {{$event->country->name}} {{$event->zip}}</p>
                    </div>
                    <div class="col-sm-offset-1 col-sm-10">
                        @php
                        // prepare query for address
                            $address_map = urlencode($event->address).', '.urlencode($event->city->name).', '.urlencode($event->state->name).', '.urlencode($event->country->name);
                        @endphp
                        <div class="mapouter"><div class="gmap_canvas"><iframe style="width:100%;" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q={{$address_map}}&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{position:relative;text-align:right;height:400px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:100%;}</style></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Similar event starts --}}
        @if(count($similarEvents) > 0)
        <div class="v_reviews">
            <div class="container">
                <div class="row event-o-detail">
                    <div class="col-sm-12">
                        <h3>Current Events</h3>
                        <span class="h-line"></span>
                        <br>
                        <div class="row">
                            @foreach($similarEvents as $similarEvent)
                            <div class="col-sm-4">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="{{$similarEvent['profile']}}"><img src="{{$similarEvent['image']}}" alt="" class="media-object" style="width:100px;height:80px;"></a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><span>{{$similarEvent['date']}}</span> {{$similarEvent['city']}}</h4>
                                        <p>{{showReadMore($similarEvent['title'], 35, '')}}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        {{-- Similar events ends --}}
    </div>
@endsection
@section('footer_scripts')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/jquery.lightbox.js')}}"></script>
<script src="{{asset('js/embed.videos.js')}}"></script>
<script src="{{asset('js/YouTubePopUp.jquery.js')}}"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
    jQuery(function(){
        jQuery("a.bla-1").YouTubePopUp();
        jQuery("a.bla-2").YouTubePopUp( { autoplay: 0 } ); // Disable autoplay
    });
    $(document).ready(function() {
        $('.embed-video').embedVideo();
    });
    var email_chkbox = $("#check_email_contact");
    var phone_chkbox = $("#check_phone_contact");
    var message_chkbox = $("#check_message_contact");
    
    $(document).ready(function() {
        email_chkbox.on('change', function() {
            if(email_chkbox.is(':checked') == true) {
                phone_chkbox.prop("checked", false);
                message_chkbox.prop("checked", false);
            }
        });
        phone_chkbox.on('change', function() {
            if(phone_chkbox.is(':checked') == true) {
                email_chkbox.prop("checked", false);
                message_chkbox.prop("checked", false);
            }
        });
        message_chkbox.on('change', function() {
            if(message_chkbox.is(':checked') == true) {
                email_chkbox.prop("checked", false);
                phone_chkbox.prop("checked", false);
            }
        });
        // submit request price
        $('form#submit-new-message-request').on('submit', function(e) {
            e.preventDefault();
            $(".show-message-request-error, .show-message-request-success").hide();
            $(".show-message-request-error").html("");
            $(".show-message-request-success").html("");
            var submit_frm = true;
            if(email_chkbox.is(':checked') == false && phone_chkbox.is(':checked') == false && message_chkbox.is(':checked') == false) {
                submit_frm = false;
                $(".show-price-request-error").show();
                $(".show-price-request-error").html("Please select atleast one Preferred Contact Method.");
            } else {
                submit_frm = true;
                $(".show-price-request-error").hide();
                $(".show-price-request-error").html("");
            }
            if(phone_chkbox.is(':checked') == true) {
                if($('form#submit-new-price-request input[name="mobile_number"]').val() == '') {
                        submit_frm = false;
                        $(".show-price-request-error").show();
                        $(".show-price-request-error").html("Please enter Phone Number.");
                } else {
                        submit_frm = true;
                        $(".show-price-request-error").hide();
                        $(".show-price-request-error").html("");
                }
            }
            if(submit_frm == true) {
                var form = $("#submit-new-message-request");
                var url = form.attr('action');
                var data = new FormData(form[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    beforeSend: function () {
                        // show loading image and disable the submit button
                        $("button[name='submit-btn-message-request']").attr('disabled','disabled');
                        $("#loader").show();
                    },
                    success: function(result) {
                        // show success message
                        if(result.status == 0) {
                            $(".show-message-request-error").html(result.message);
                            $(".show-message-request-error").show();
                        } else {
                            $(".show-message-request-success").html(result.message);
                            $(".show-message-request-success").show();
                            // reset the form
                            resetMessageRequestFrm();
                            setTimeout(function() {$('#sendMessageRequest').modal('hide');window.location.reload(true);}, 2000);
                        }
                    },
                    error: function(xhr, status, error){
                        var errors = xhr.responseJSON;
                        $(".show-message-request-error").html(errors.error);
                    },
                    complete: function () {
                        // hide loading image and enable the submit button
                        $("#loader").hide();
                        $("button[name='submit-btn-message-request']").removeAttr('disabled');
                    }
                });
                return false;
            } else {
                return false;
            }
        });
    });
    function resetMessageRequestFrm()
    {
        var message = $('form#submit-new-message-request textarea[name="message"]');
        var first_name = $('form#submit-new-price-request input[name="first_name"]');
        var last_name = $('form#submit-new-price-request input[name="last_name"]');
        var mobile_number = $('form#submit-new-price-request input[name="mobile_number"]');
        @if(Auth::check() == false)
        var email = $('form#submit-new-price-request input[name="email"]');
        var password = $('form#submit-new-price-request input[name="password"]');
        @endif
        email_chkbox.prop("checked", true);
        phone_chkbox.prop("checked", false);
        phone_chkbox.prop("checked", false);

        first_name.val('')
        last_name.val('')
        mobile_number.val('')
        @if(Auth::check() == false)
        email.val('')
        password.val('')
        @endif
        message.val('');
    }
var ajaxResultGetRequest = null
$("#next-page-review").on('click', function() {
    var url = route('get-event-reviews-ajax')+'?page='+$(this).data('next-page') + '&event_id={{$event->getEncryptedId()}}';
    
    ajaxResultGetRequest = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            // show loading image
            $('#next-page-review').hide();
            $('#spinner-load').show();
        },
        success: function(result) {
            // show results
            $('#events-reviews-results').append(result.reviews);
            $('#next-page-review').data('next-page', result.next_page);
            if(result.next_url == '') {
                $('#next-page-review').hide();
            } else {
                $('#next-page-review').show();
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            // hide loading image
            $('#spinner-load').hide();
            ajaxResultGetRequest = null;
        }
    });
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection