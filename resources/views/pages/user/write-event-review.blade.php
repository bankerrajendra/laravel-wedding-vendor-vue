@extends('layouts.app')
@section('template_title')
    Write A Event Review
@endsection
@section('content')

<div class="login_p signup">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3 text-center">
				<h1><span>Write</span> A Event Review</h1>
				<h3>Your Overall Experience </h3>
				<hr>
			</div>
			<div class="col-sm-6 col-sm-offset-3">
				<div class="tabbable-line">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form role="form" method="POST" action="{{ route('handle-event-review-submit') }}" name="userRating" id="userRating">
                        @csrf	
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>How would you rate your overall experience with {{$eventInfo->event_name}}?</label>
                                    <textarea class="form-control" name="message" style="height: 100px;">@if($user_review != ''){{@$user_review->message}}@endif</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group rating-btn">
                                    <label for="scountry">Your Rating</label>
                                    <span class="selected-rating">@if($user_review != ''){{@$user_review->rating}}@else 0 @endif</span> / 5
                                    <br>
                                    @for($i=1;$i<=5;$i++)
                                        <button type="button" class="btnrating btn @if($user_review != '' && @$user_review->rating >= $i) btn-warning @else btn-default @endif btn-sm" data-attr="{{$i}}" id="rating-star-{{$i}}">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </button>
                                    @endfor
                                    <input type="hidden" id="selected_rating" name="selected_rating" value="@if($user_review != ''){{@$user_review->rating}}@endif" required="required">
                                    <input type="hidden" id="event_id" name="event_id" value="{{$event_id}}" required="required">
                                </div>
                            </div>
                        </div>	
                        <div class="row text-center">
                            <div class="col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-default btn-block">SUBMIT</button>
                            </div>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
	    </div>
    </div>
</div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
jQuery(document).ready(function($){
    // may be this is for hidden field show error
    $.validator.setDefaults({ ignore: '' });
    $(".btnrating").on('click',(function(e) {
    
        var previous_value = $("#selected_rating").val();
        
        var selected_value = $(this).attr("data-attr");
        $("#selected_rating").val(selected_value);
        
        $(".selected-rating").empty();
        $(".selected-rating").html(selected_value);
        
        for (i = 1; i <= selected_value; ++i) {
            $("#rating-star-"+i).toggleClass('btn-warning');
            $("#rating-star-"+i).toggleClass('btn-default');
        }
        
        for (ix = 1; ix <= previous_value; ++ix) {
            $("#rating-star-"+ix).toggleClass('btn-warning');
            $("#rating-star-"+ix).toggleClass('btn-default');
        }
    
    }));	
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection