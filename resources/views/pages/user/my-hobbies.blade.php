@extends('layouts.app')
@section('template_title')
   Edit Profile
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection

@section('content')
<div class="edit-profile">
	<div class="container">
	@if(session('success'))
		<div class="alert alert-success" role="alert">
			{{session('success')}}
		</div>
	@endif
	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
		<div class="row">
			@include('partials.my-profile-navigation')

			<div class="col-sm-8 panel-body">
				<form method="post" action="{{ route('updateHobbiesFrontend') }}">
					{{ csrf_field()}}
					<div class="panel-group" id="accordion">
						<!--Hobbies-->
						<div class="panel panel-success">
							<div class="panel-heading mobile-panel">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Hobbies  <i class="more-less glyphicon glyphicon-minus"></i></a>
								</h4>
							</div>
							<div id="collapse1" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">
										@php
											$hobbies = explode(",", @$userProfile->users_information->hobbies);											 
										@endphp
                                        
										<div class="checkbox hbs">
											<?php  foreach ($hobbies_interests['hobbies'] as $hkey=>$hval) { $h =0;?>
												<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"><label><input type="checkbox" value="<?php echo $hkey; ?>" name="hobbies[]" <?php if(in_array($hkey, $hobbies)){echo "checked";} ?>><?php echo $hval; ?></label></div>
											<?php $h++;} ?>
											{{--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"><label><input type="checkbox" value="{{config('constants.doesnt_matter')}}" name="hobbies[]">Doesn't matter</label></div>--}}
											
										</div>
                                        
									</div>
										
								</div>
							</div>
						</div>
						<!--End Hobbies-->
						
						<!--Interests-->
						<div class="panel panel-warning">
							<div class="panel-heading mobile-panel">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Interests <i class="more-less glyphicon glyphicon-plus"></i></a>
								</h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="row">
										@php 
											$interests = explode(",", @$userProfile->users_information->interests);											 
										@endphp
										
										<div class="checkbox">
											<?php  foreach ($hobbies_interests['interests'] as $hkey=>$hval) { $h =0;?>
												<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"><label><input type="checkbox" value="<?php echo $hkey; ?>" name="interests[]" <?php if(in_array($hkey, $interests)){echo "checked";} ?>><?php echo $hval; ?></label></div>
											<?php $h++;} ?>
										</div>
                                        					
									</div>
								</div>
							</div>
						</div>
						<!--End Interests-->
						
						<!--Favourite Music-->
						<div class="panel panel-success">
							<div class="panel-heading mobile-panel">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Favourite Music <i class="more-less glyphicon glyphicon-plus"></i></a>
								</h4>
							</div>
							<div id="collapse3" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="row">
										@php 
											$favourite_music = explode(",", @$userProfile->users_information->favourite_music);											 
										@endphp
                                        
										<div class="checkbox">
											<?php  foreach ($hobbies_interests['favourite_music'] as $hkey=>$hval) { $h =0;?>
											   <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"> <label><input type="checkbox" value="<?php echo $hkey; ?>" name="favourite_music[]" <?php if(in_array($hkey, $favourite_music)){echo "checked";} ?>><?php echo $hval; ?></label></div>
											<?php $h++;} ?>
										</div>
                                       				
									</div>
								</div>
							</div>
						</div>
						<!--End Favourite Music-->
						
						<!--Favourite Read-->
						<div class="panel panel-warning">
							<div class="panel-heading mobile-panel">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Favourite Reads <i class="more-less glyphicon glyphicon-plus"></i></a>
								</h4>
							</div>
							<div id="collapse4" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="row">
										@php 
											$favourite_reads = explode(",", @$userProfile->users_information->favourite_reads);											 
										@endphp
										
										<div class="checkbox">
											<?php  foreach ($hobbies_interests['favourite_reads'] as $hkey=>$hval) { $h =0;?>
												<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"><label><input type="checkbox" value="<?php echo $hkey; ?>" name="favourite_reads[]" <?php if(in_array($hkey, $favourite_reads)){echo "checked";} ?>><?php echo $hval; ?></label></div>
											<?php $h++;} ?>
										</div>
                                        					
									</div>
								</div>
							</div>
						</div>
						<!--End Favourite Read-->
						
						<!--Movies-->
						<div class="panel panel-success">
							<div class="panel-heading mobile-panel">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Preferred Movies <i class="more-less glyphicon glyphicon-plus"></i></a>
								</h4>
							</div>
							<div id="collapse5" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="row">
										@php
											$preferred_movies = explode(",", @$userProfile->users_information->preferred_movies);											 
										@endphp

										<div class="checkbox">
											<?php  foreach ($hobbies_interests['preferred_movies'] as $hkey=>$hval) { $h =0;?>
												<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"><label><input type="checkbox" value="<?php echo $hkey; ?>" name="preferred_movies[]" <?php if(in_array($hkey, $preferred_movies)){echo "checked";} ?>><?php echo $hval; ?></label></div>
											<?php $h++;} ?>
										</div>
                                        					
									</div>
								</div>
							</div>
						</div>
						<!--End Movies-->
						
						<!--Sports-->
						<div class="panel panel-warning">
							<div class="panel-heading mobile-panel">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse6">Sports <i class="more-less glyphicon glyphicon-plus"></i></a>
								</h4>
							</div>
							<div id="collapse6" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="row">
										@php 
											$sports = explode(",", @$userProfile->users_information->sports);											 
										@endphp
										
										<div class="checkbox">
											<?php  foreach ($hobbies_interests['sports'] as $hkey=>$hval) { $h =0;?>
											   <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"> <label><input type="checkbox" value="<?php echo $hkey; ?>" name="sports[]" <?php if(in_array($hkey, $sports)){echo "checked";} ?>><?php echo $hval; ?></label></div>
											<?php $h++;} ?>
										</div>
                                        				
									</div>
								</div>
							</div>
						</div>
						<!--End Sports-->
						
						<!--Languages-->
						<div class="panel panel-success">
							<div class="panel-heading mobile-panel">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse7">Spoken Languages <i class="more-less glyphicon glyphicon-plus"></i></a>
								</h4>
							</div>
							
							<div id="collapse7" class="panel-collapse collapse">
								<div class="panel-body">
									<div class="row">
										@php 
											$language_spoken = explode(",", @$userProfile->users_information->spoken_languages);	 
										@endphp
										
										<div class="checkbox">											 
											<?php  foreach ($language as $lang) { ?>
												<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6"><label><input type="checkbox" value="<?php echo $lang->id; ?>" name="language_spoken[]" <?php if(in_array($lang->id, $language_spoken)){echo "checked";} ?>><?php echo $lang->language; ?></label></div>
											<?php  } ?>
										</div>
                                        				
									</div>
								</div>
						</div>
						<!--End Languages-->
					</div>
					</br>
					<button type="submit" class="btn btn-warning">Save Changes</button>
						<a href="{{ route('self-profile') }}" class="btn btn-success">Back</a>
				</form>
			</div>
		</div>
	</div>
</div>
<!--End Login-->

@endsection
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

$(document).ready(function(){
	var cb = $(".hbs").find("input[type='checkbox']");
	$(cb).on("click", function(){
	  if($(this).val() == "DM"){
		$(cb).not(this).removeAttr('checked');
	  }
	})
})
</script>