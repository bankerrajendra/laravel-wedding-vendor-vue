@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('content')
<div class="login_p signup">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h1>What <span>You Need?</span></h1>
                <p>Yay! You’re on your way to creating a New Vendor listing on the world’s biggest wedding resource.Simply fill out the below information.</p>
            </div>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="tabbable-line">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form name="frm-submit-user-vendor-message" method="POST" action="{{ route('user-vendor-mail') }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    VENDOR MESSAGE
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                @if(!empty($chosen_cats))
                                    @foreach ($chosen_cats as $chosen_cat)
                                        <div class="form-group">
                                            <label>{{$chosen_cat['name']}}</label>
                                            <textarea name="description[{{$chosen_cat['id']}}]" class="form-control description-text" style="height: 70px;" id="description-{{$chosen_cat['id']}}">{{@Session::get('description_request_a_quote')[$chosen_cat['id']]}}</textarea>
                                        </div>        
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <a href="{{route('user-choose-vendor')}}?wedding_type={{$wedding_type}}&no_of_guest={{$no_of_guest}}&country={{$country}}&state={{$state}}&city={{$city}}" style="font-size:18px;"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back</a>
                            </div>
                            <div class="col-sm-6 col-xs-12 text-right">	
                                <button type="submit" name="submit" class="btn btn-default btn-block">NEXT</button>
                            </div>
                        </div>
                        <input name="categories" value="{{implode(',', $categories)}}" type="hidden" />
                        <input name="wedding_type" value="{{$wedding_type}}" type="hidden" />
                        <input name="no_of_guest" value="{{$no_of_guest}}" type="hidden" />
                        <input name="mobile_number" value="{{$mobile_number}}" type="hidden" />
                        <input name="preferred_contact_method" value="{{$preferred_contact_method}}" type="hidden" />
                        <input name="event_date" value="{{$event_date}}" type="hidden" />
                        <input name="country" value="{{$country}}" type="hidden" />
                        <input name="state" value="{{$state}}" type="hidden" />
                        <input name="city" value="{{$city}}" type="hidden" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
$(document).on('input propertychange', ".description-text", function () {
    if($(this).val().length > 300) {
        error  = true;
        if($($(this)).next('span').length == 0) {
            $('<span style="color: #a94442;">Characters should be less than 300.</span>').insertAfter($(this));
        }
    } else {
        $($(this)).next('span').remove();
    }
});
$(document).ready(function() {
    $('form[name="frm-submit-user-vendor-message"]').submit(function (e) {
        var error = false;
        $(".description-text").each(function() {
            if($(this).val().length > 300) {
                error  = true;
                if($($(this)).next('span').length == 0) {
                    $('<span style="color: #a94442;">Characters should be less than 300.</span>').insertAfter($(this));
                }
            } else {
                $($(this)).next('span').remove();
            }
        });
        if(error == false) {
            return true;
        } else {
            return false;
        }
    });
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection