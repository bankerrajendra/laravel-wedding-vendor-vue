@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('content')
<div class="login_p signup">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h1>Select <span>Vendors</span></h1>
                <p>We'll introduce you to several professional interested and available wedding vendors near your wedding location. You'll be able to Search, compare, choose the price that's right for you & book the professional (s) you need.</p>
                <hr>
            </div>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="tabbable-line">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form name="frm-submit-choosed-vendor" method="POST" action="{{ route('user-vendor-message') }}">
                        @csrf
                        <div class="row">
                            <div class="form-group">
                                @if($categories->count() > 0)
                                    @php
                                    $session_cats = explode(',', Request::session()->get('comma_sep_categories', ''));
                                    @endphp
                                    @foreach ($categories as $category)
                                        <div class="col-sm-6 panel_body">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="categories[]" value="{{$category->id}}" @if(in_array($category->id, $session_cats)) checked="checked" @endif ><span class="checkmark"></span> {{$category->name}}
                                            </label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>	
                        <hr>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <a href="{{route('request-a-quote')}}" style="font-size:18px;"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back</a>
                            </div>
                            <div class="col-sm-6 col-xs-12 text-right">
                                <button type="submit" name="submit" class="btn btn-default">NEXT</button>
                            </div>
                        </div>
                        <input name="wedding_type" value="{{$wedding_type}}" type="hidden" />
                        <input name="no_of_guest" value="{{$no_of_guest}}" type="hidden" />
                        <input name="mobile_number" value="{{$mobile_number}}" type="hidden" />
                        <input name="preferred_contact_method" value="{{$preferred_contact_method}}" type="hidden" />
                        <input name="event_date" value="{{$event_date}}" type="hidden" />
                        <input name="country" value="{{$country}}" type="hidden" />
                        <input name="state" value="{{$state}}" type="hidden" />
                        <input name="city" value="{{$city}}" type="hidden" />
                    </form>
                <br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
jQuery(document).ready(function($){
    $.validator.setDefaults({ ignore: '' });
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection