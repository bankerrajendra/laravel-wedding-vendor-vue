@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.lightbox')}}.css">
    <link href="{{asset('css/bootstrap-datepicker')}}.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/lighbox-gallery.css')}}">
    <link rel="stylesheet" href="{{asset('css/YouTubePopUp.css')}}">
    <style type="text/css">
    .sim-p a h5 {
        color: #000;
        text-align: center;
        padding-top: 0px;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
        padding-left: 10px;
        padding-right: 10px;
    }
    </style>
@endsection
@section('content')
    <div class="vendor-detail">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{route('search-vendor', ['category' => $catInfo->slug])}}">{{$catInfo->name}}</a></li>
                <li class="active text-dark">
                    @if($vendorObj->getProfileFieldExistsLoggedVendor('company_name', 'vi', $selfProfile))
                        {{$vendorObj->getProfileFieldExistsLoggedVendor('company_name', 'vi', $selfProfile)}}
                    @endif
                </li>
            </ol>
            <div class="row">
                <div class="col-sm-12">
                    @if(Auth::check())
                        @if(!(Auth::user()->is_active()) && $selfProfile == true)
                            <div class="text-center">
                                <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                            </div>
                        @endif
                    @endif
                    <div class="vendor-banner">
                        @if(@$vendorObj->getVendorCoverPic($selfProfile))
                            <img src="{{$vendorObj->getVendorCoverPic($selfProfile)}}" class="img-responsive img-thumbnail" style="width:100%;" alt="" />
                        @endif
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="vendor-photo">
                        @if(@$vendorObj->getVendorProfilePic($selfProfile))
                            <img src="{{$vendorObj->getVendorProfilePic($selfProfile)}}" class="img-responsive img-thumbnail" alt="" />
                        @endif
                    </div>
                </div>	
                <div class="col-sm-6">	
                    <div class="vendor-name">
                        <h1>
                            <i>
                                @if($vendorObj->getProfileFieldExistsLoggedVendor('company_name', 'vi', $selfProfile))
                                    {{$vendorObj->getProfileFieldExistsLoggedVendor('company_name', 'vi', $selfProfile)}}
                                @endif
                            </i>
                        </h1>
                        <p>
                            @php
                                $average_rating = $vendorObj->getAverageReviewRating();
                                $total_review = $vendorObj->getTotalReviewVendor();
                            @endphp

                            @for($r=1;$r<=5;$r++)
                            <i class="fa @if($r <= $average_rating) fa-star @elseif($r >= $average_rating-0.5 && $r <= $average_rating+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                            @endfor
                        </p>
                        <div class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" style="color:#000;">( {{$total_review}} @if( $total_review > 1)Reviews @else Review @endif)</a>
                            <div class="dropdown-menu text-center" style="width: 330px;">
                                <div class="col-sm-12 col-xs-12 panel-body">
                                    @for($br=1;$br<=5;$br++)
                                        <div class="side">
                                            <div class="">
                                                <a href="{{ route('show-reviews', ['vendorId' => $vendorObj->getEncryptedId(), 'rate' => $br]) }}" onclick="javascript:getPerRatingReviews({{$br}});">{{$br}} star</a>
                                            </div>
                                        </div>
                                        <div class="middle">
                                            <div class="bar-container">
                                                <div class="bar-{{$vendorObj->getSpcificStartBarPer($br)}}"></div>
                                            </div>
                                        </div>
                                        <div class="side right">
                                            <div><a href="{{ route('show-reviews', ['vendorId' => $vendorObj->getEncryptedId(), 'rate' => $br] )}}" onclick="javascript:getPerRatingReviews({{$br}});">{{$vendorObj->getCountReviewStarSpecific($br)}}</a></div>
                                        </div>
                                    @endfor
                                </div>
                                <div class="col-sm-12 col-xs-12 text-center">
                                    <a href="{{ route('show-reviews', ['vendorId' => $vendorObj->getEncryptedId()] )}}" class="btn btn-default btn-rev">Read More</a>
                                </div>
                            </div>
                        </div>
                        @if($selfProfile == false)<span class="hidden-lg hidden-md hidden-sm"><a href="{{ route('write-review', [$vendorObj->getEncryptedId()]) }}">Write a Review</a></span>
                        @endif
                        <p></p>  
                        @if($selfProfile == false)<p class="hidden-xs"><span><a href="{{ route('write-review', [$vendorObj->getEncryptedId()]) }}">Write a Review</a></span></p>@endif
                        <ul class="add-list">
                            @if($vendorObj->vendors_information->display_address == 1)
                                @if($selfProfile == true)
                                    <li><a href="#location"> <i class="fa fa-map-marker" aria-hidden="true"></i> Map</a></li>
                                @else
                                    @if($vendorObj->getProfileFieldExistsLoggedVendor('address', 'u', $selfProfile))
                                        <li><a href="#location"> <i class="fa fa-map-marker" aria-hidden="true"></i> Map</a></li>
                                    @endif
                                @endif
                            @endif

                            @if($selfProfile == true)
                                @if($vendorObj->vendors_information->display_business_number == 1)
                                <li class="hidden-xs"><a href="javascript:void(0);" data-toggle="popover" data-placement="bottom" data-content="+{{getPhoneCode($vendorObj->mobile_country_code)}} {{$vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', $selfProfile)}}"> <i class='fa fa-phone' aria-hidden='true'></i> Phone Number</a></li>
                                <li class="hidden-md hidden-sm hidden-lg"><a href="tel:+{{getPhoneCode($vendorObj->mobile_country_code)}} {{$vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', $selfProfile)}}"> <i class='fa fa-phone' aria-hidden='true'></i> Phone Number</a></li>
                                @endif
                            @else
                                @if($vendorObj->is_paid == 1 && $vendorObj->vendors_information->display_business_number == 1)
                                    @if($vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', false))
                                        <li class="hidden-xs"><a href="javascript:void(0);" data-toggle="popover" data-placement="bottom" data-content="+{{getPhoneCode($vendorObj->mobile_country_code)}} {{$vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', false)}}"> <i class='fa fa-phone' aria-hidden='true'></i> Phone Number</a></li>
                                        <li class="hidden-md hidden-sm hidden-lg"><a href="tel:+{{getPhoneCode($vendorObj->mobile_country_code)}} {{$vendorObj->getProfileFieldExistsLoggedVendor('mobile_number', 'u', $selfProfile)}}"> <i class='fa fa-phone' aria-hidden='true'></i> Phone Number</a></li>
                                    @endif
                                @endif
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">	
                    <div class="vendor-name pull_right">
                        <ul class="v_social">
                            @if($vendorObj->getProfileFieldExistsLoggedVendor('facebook', 'vi', $selfProfile))
                            <li><a target="_blank" href="{{$vendorObj->getProfileFieldExistsLoggedVendor('facebook', 'vi', $selfProfile)}}"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            @endif
                            @if($vendorObj->getProfileFieldExistsLoggedVendor('twitter', 'vi', $selfProfile))
                            <li><a target="_blank" href="{{$vendorObj->getProfileFieldExistsLoggedVendor('twitter', 'vi', $selfProfile)}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            @endif
                            @if($vendorObj->getProfileFieldExistsLoggedVendor('linkedin', 'vi', $selfProfile))
                            <li><a target="_blank" href="{{$vendorObj->getProfileFieldExistsLoggedVendor('linkedin', 'vi', $selfProfile)}}"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            @endif
                            @if($vendorObj->getProfileFieldExistsLoggedVendor('pinterest', 'vi', $selfProfile))
                            <li><a target="_blank" href="{{$vendorObj->getProfileFieldExistsLoggedVendor('pinterest', 'vi', $selfProfile)}}"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                            @endif
                            @if($vendorObj->getProfileFieldExistsLoggedVendor('instagram', 'vi', $selfProfile))
                            <li><a target="_blank" href="{{$vendorObj->getProfileFieldExistsLoggedVendor('instagram', 'vi', $selfProfile)}}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <br class="hidden-xs">
                <div class="col-sm-8 our-gal">
                    <div class="btn_group">
                        @if($selfProfile == false && $allowSendMessage == true)
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#sendPriceRequest"><i class="fa fa-envelope-o" aria-hidden="true"></i> Message</button>
                        @endif
                        @if(empty($faqs) && empty($customFaqs))
                        @else
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myFaq">Faqs</button>
                        @endif
                        @if($selfProfile == false && $allowShortList == true)<button type="button" class="btn btn-default shortlist" id="show_hide_bt" onclick="javascript:;"><i class="fa fa-heart" aria-hidden="true" @if($shortListed == true) style="color:red;" @else style="color:white;" @endif></i> @if($shortListed == true) Shortlisted @else Shortlist @endif</button>@endif
                        @if(!Auth::check())
                        <a href="{{ route('login') }}?redirect={{ urlencode($vendorObj->getVendorProfileLink()) }}" class="btn btn-default shortlist" id="show_hide_bt" onclick="javascript:;"><i class="fa fa-heart" aria-hidden="true" style="color:white;"></i> Shortlist</a>
                        @endif
                    </div>
                    @if($selfProfile == false && $allowSendMessage == true)
                        <!-- Modal -->
                        <div id="sendPriceRequest" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h3 class="modal-title" style="font-family: 'Merienda One', cursive!important;">MESSAGE VENDOR <br><span style="color: #ed38a0!important;">{{$vendorObj->vendors_information->company_name}}</span></h3>
                                        <p>Tell us about your wedding and let us do all the work.</p>
                                        <hr>
                                        <div class="alert alert-danger show-price-request-error" style="display: none;"></div>
                                        <div class="alert alert-success show-price-request-success" style="display: none;"></div>

                                        <form name="submit-new-price-request" id="submit-new-price-request" action="{{ route('handle-new-price-request') }}" method="post">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label for="first-name">First Name *</label>
                                                        <input type="text" name="first_name" class="form-control" id="first-name" placeholder="First Name" @if(!empty($form_prepopulate) && $form_prepopulate['first_name'] != "")value="{{$form_prepopulate['first_name']}}"@endif />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label for="last-name">Last Name *</label>
                                                        <input type="text" id="last-name" name="last_name" class="form-control" placeholder="Last Name" @if(!empty($form_prepopulate) && $form_prepopulate['last_name'] != "")value="{{$form_prepopulate['last_name']}}"@endif />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label for="mobile-number">Phone Number</label>
                                                        <input type="text" id="mobile-number" name="mobile_number" class="form-control" placeholder="Phone Number" @if(!empty($form_prepopulate) && $form_prepopulate['mobile_number'] != "")value="{{$form_prepopulate['mobile_number']}}"@endif />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label for="event-date">Event Date *</label>
                                                        <div id="filterDate2">
                                                        <!-- Datepicker as text field -->         
                                                            <div class="input-group date" data-date-format="dd.mm.yyyy">
                                                                <input type="text" id="event-date" class="form-control" placeholder="dd.mm.yyyy" name="event_date" autocomplete="off">
                                                                <div class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(Auth::check() == false)
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label for="email">Email *</label>
                                                        <input type="email" id="email" name="email" class="form-control" placeholder="Email" @if(!empty($form_prepopulate) && $form_prepopulate['email'] != "")value="{{$form_prepopulate['email']}}"@endif />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <div class="form-group">
                                                        <label for="password">Password *</label>
                                                        <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                                <label class="lb">Preferred Contact Method *</label>
                                                <br>
                                                <label class="checkbox-inline"><input type="checkbox" id="check_email_contact" name="check_email_contact" value="1" checked="checked"><span class="checkmark email-chk"></span> Email</label>
                                                <label class="checkbox-inline"><input type="checkbox" id="check_phone_contact" name="check_phone_contact" value="1"><span class="checkmark phone-chk"></span> Phone</label>
                                                <label class="checkbox-inline"><input type="checkbox" id="check_message_contact" name="check_message_contact" value="1"><span class="checkmark message-chk"></span> Text Message</label>
                                            </div>
                                            <div class="form-group">
                                                <label for="about-extra-needs">Tell us about your {{$catInfo->name}} needs?</label>
                                                <textarea id="about-extra-needs" name="about_extra_needs" class="form-control" style="height: 100px;"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-default" name="submit-btn-price-request">SUBMIT</button>
                                            <div class="row">
                                                <div class="col-sm-12" style="margin-left: 50px; margin-top: 10px;">
                                                    <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none;">
                                                </div>
                                            </div>
                                            <input type="hidden" name="vendor_id" value="{{$vendorObj->getEncryptedId()}}" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <!-- Faqs1-->
                    @if(!empty($faqs) || !empty($customFaqs))
                    <div id="myFaq" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h3 class="modal-title" style="font-family: 'Merienda One', cursive!important;">FAQs</h3>
                                    <br>
                                    @if(!empty($faqs))
                                        @foreach ($faqs as $faq)
                                            @if($faq->answer != '')
                                                <p><b>{{$faq->question}}</b></p>
                                                <p>{{$faq->answer}}</p>
                                            @endif
                                        @endforeach
                                    @endif
                                    @if(!empty($customFaqs))
                                        @foreach ($customFaqs as $cust_faq)
                                            <p><b>{{$cust_faq->question}}</b></p>
                                            <p>{{$cust_faq->answer}}</p>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!-- End Faqs1-->
                    <!-- Mobile Videos Starts -->
                    @if($videosCount > 0)
                    <div class="row hidden-sm hidden-lg hidden-md">
                        <div class="col-sm-12">
                            <ul class="video-sec">
                                @foreach ($videos as $video)
                                <li><a class="bla-1" href="{{$video->embed_code}}"><img src="{{ asset('img/video-img.jpg') }}" class="img-responsive"></a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    <!-- Mobile Videos Ends -->
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="gallery">
                                @if(!empty($vendorImages))
                                    @foreach ($vendorImages as $vendorImage)
                                        <li class="vendor-imgs" style="display:none;"><a href="{{config('constants.S3_WEDDING_IMAGES_ORIGINAL').$vendorImage->image_full}}"><img src="{{config('constants.S3_WEDDING_IMAGES').$vendorImage->image_thumb}}" alt="Image" style="width:100%;"></a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        @if($vendorImages->count() > $imgLimit)
                            <div class="col-sm-11 panel-body text-center">
                                <a href="javascript:void(0);" class="btn btn-default show-more-img-vendor">SHOW MORE</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-sm-4 ab_content">
                    @if($vendorObj->getProfileFieldExistsLoggedVendor('business_information', 'vi', $selfProfile))
                    <div class="panel-heading"><h3><span>About Us</span></h3></div>
                    <p>{!!$vendorObj->getProfileFieldExistsLoggedVendor('business_information', 'vi', $selfProfile)!!}</p>
                    @endif
                </div>
            </div>
            <div class="v_address hidden-xs">
                <div class="row">
                    <div class="vl"></div>
                    <div class="col-sm-4 panel-body">
                        <h2>Contact Info</h2>
                        @if($selfProfile == false && $allowSendMessage == true)
                            <p> <i class="fa fa-envelope" aria-hidden="true"></i> <a data-toggle="modal" href="#sendPriceRequest">Send A Message</a></p>
                        @endif

                        @if($vendorObj->getProfileFieldExistsLoggedVendor('business_website', 'vi', $selfProfile))
                        <p><i class="fa fa-globe" aria-hidden="true"></i> <a href="{{$vendorObj->getProfileFieldExistsLoggedVendor('business_website', 'vi', $selfProfile)}}" target="_blank">{{$vendorObj->getProfileFieldExistsLoggedVendor('business_website', 'vi', $selfProfile)}}</a></p>
                        @endif

                        @if($vendorObj->vendors_information->display_address == 1 && $vendorObj->getProfileFieldExistsLoggedVendor('address', 'u', $selfProfile))<p><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="#location">Location</a></p>@endif
                    </div>
                    <div class="vl1"></div>
                    <div class="col-sm-4 panel-body">
                        <h2>Request Pricing</h2>
                        @if($selfProfile == false && $allowSendMessage == true)<a data-toggle="modal" href="#sendPriceRequest" class="btn btn-default">SEND MESSAGE</a>@endif
                    </div>
                    <div class="col-sm-4 panel-body">
                        <h2>Established</h2>
                        <div class="varified">
                            <div class="v-content">@if($vendorObj->getProfileFieldExistsLoggedVendor('established_year', 'vi', $selfProfile)){{$vendorObj->getProfileFieldExistsLoggedVendor('established_year', 'vi', $selfProfile)}}@endif</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($vendorObj->getProfileFieldExistsLoggedVendor('specials', 'vi', $selfProfile))
        <div class="sp_content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Specials</h3>
                    </div>
                    <div class="col-sm-12">
                        <div class="media-body">
                        <p>{!!$vendorObj->getProfileFieldExistsLoggedVendor('specials', 'vi', $selfProfile)!!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="v_reviews" id="reviews">
            @if(isset($reviews['reviews']) && count($reviews['reviews']) > 0)
                <div class="container">
                    <div class="row" id="reviews">
                        <div class="col-sm-12">
                            <h3>Reviews </h3>
                            @if($selfProfile == false)
                                <a href="{{ route('write-review', [$vendorObj->getEncryptedId()]) }}" class="review-btn pull-right"><i class="fa fa-pencil" aria-hidden="true"></i> Write a Review</a>
                            @endif
                        </div>
                        <div class="col-sm-12">
                            <div id="vendor-reviews-results">
                                @include('pages.user.ajax.show-reviews')
                            </div>
                            <div class="media">
                                <div class="media-body text-center">
                                    <a href="{{route('show-reviews', ['vendorId' => $vendorObj->getEncryptedId()])}}" class="btn btn-default">Show More </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="no-review">
                    <h4>No Review @if($selfProfile == false)<b><a href="{{ route('write-review', [$vendorObj->getEncryptedId()]) }}">Write a Review</a></b>@endif</h4>
                </div>
            @endif
        </div>
        @if($videosCount > 0)
        <div class="v_videos hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12r">
                        <h3>Videos</h3>
                    </div>
                    <div class="col-sm-offset-1 col-sm-10">
                        <div class="row">
                            @foreach ($videos as $video)
                                <div class="col-sm-6 panel-body">
                                    {{-- <div class="embed-video" data-source="youtube" data-video-url="{{$video->embed_code}}" data-cc-policy="1"></div> --}}
                                    <iframe style="width:100%; height: 227px;" src="https://www.youtube.com/embed/{{GetYouTubeId($video->embed_code)}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if($vendorObj->vendors_information->display_address == 1)
        <div class="v_videos">
            <div class="container">
                <div class="row" id="location">
                    <div class="col-sm-12">
                        <h3>Map</h3>
                        <p class="text-center">{{$vendorObj->getProfileFieldExistsLoggedVendor('address', 'u', $selfProfile)}}, {{$vendorObj->city->name}}, {{$vendorObj->state->name}}, {{$vendorObj->country->name}} {{$vendorObj->zip}}</p>
                    </div>
                    <div class="col-sm-offset-1 col-sm-10">
                        @php
                        // prepare query for address
                            $address_map = urlencode($vendorObj->getProfileFieldExistsLoggedVendor('address', 'u', $selfProfile)).', '.urlencode($vendorObj->city->name).', '.urlencode($vendorObj->state->name).', '.urlencode($vendorObj->country->name);
                        @endphp
                        <div class="mapouter"><div class="gmap_canvas"><iframe style="width:100%;" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q={{$address_map}}&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{position:relative;text-align:right;height:400px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:100%;}</style></div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        {{-- Similar vendors starts --}}
        @if(count($similarVendors) > 0)
        <div class="sim_vendors">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3>Similar Vendors</h3>
                    </div>
                    @foreach($similarVendors as $similarVendor)
                    <div class="col-sm-3 col-xs-12">
                        <div class="sim-p">
                            <a href="{{$similarVendor['vendor_profile']}}"><img src="{{$similarVendor['vendor_image']}}" alt="" class="img-responsive" style="width:100%;">
                            <h4>{{$similarVendor['vendor_title']}}</h4>
                            <h5>{{$similarVendor['city']}}, {{$similarVendor['country']}}</h5>
                            </a>
                            @if($similarVendor['average_rating'] != '')
                            <div class="rating">{{substr($similarVendor['average_rating'], 0, -2)}}</div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        {{-- Similar vendors ends --}}
    </div>
@endsection
@section('footer_scripts')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/jquery.lightbox.js')}}"></script>
<script src="{{asset('js/embed.videos.js')}}"></script>
<script src="{{asset('js/YouTubePopUp.jquery.js')}}"></script>
<script>
//* for show additional images STARTED *//
var totalImagesToShow = {{$vendorImages->count()}};
var imageLimitPerPage = {{$imgLimit}};
// how many to show
var display_count = 0;
// function to show
function showImagesVendor(start, end){  
    for(var i=start; i<end ;i++) {  
        $('.vendor-imgs').eq(i).css('display', 'block')
    }
}
// bind click event to show between n and n+3
$('.show-more-img-vendor').click(function(event){
    showImagesVendor(display_count, display_count+imageLimitPerPage);
    display_count += imageLimitPerPage;
    if(display_count > totalImagesToShow) {
        $('.show-more-img-vendor').hide();
    }
});

//* for show additional images ENDED *//
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
    showImagesVendor(display_count, display_count+imageLimitPerPage);
    display_count += imageLimitPerPage;
});
    // $(".bla-1").each(function() {
    //     var url = $(this).attr("href")
    //     var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    //     var match = url.match(regExp);
    //     if (match && match[2].length == 11) {
    //         var id = match[2];
    //         var path = 'http://img.youtube.com/vi/'+id+'/0.jpg';
    //         $(this).html('<img src="'+path+'" class="img-responsive">');
    //     } 
    // });
    jQuery(function(){
        jQuery("a.bla-1").YouTubePopUp();
        jQuery("a.bla-2").YouTubePopUp( { autoplay: 0 } ); // Disable autoplay
    });
    $(document).ready(function() {
        $('.embed-video').embedVideo();
    });
    var email_chkbox = $("#check_email_contact");
    var phone_chkbox = $("#check_phone_contact");
    var message_chkbox = $("#check_message_contact");
    // Initiate Lightbox
    $(function() {
        $('.gallery a').lightbox();
    });
    var date = new Date();
    date.setDate(date.getDate());
    $('.input-group.date').datepicker({format: "dd.mm.yyyy", startDate: date});
    $(document).ready(function() {
        email_chkbox.on('change', function() {
            if(email_chkbox.is(':checked') == true) {
                phone_chkbox.prop("checked", false);
                message_chkbox.prop("checked", false);
            }
        });
        phone_chkbox.on('change', function() {
            if(phone_chkbox.is(':checked') == true) {
                email_chkbox.prop("checked", false);
                message_chkbox.prop("checked", false);
            }
        });
        message_chkbox.on('change', function() {
            if(message_chkbox.is(':checked') == true) {
                email_chkbox.prop("checked", false);
                phone_chkbox.prop("checked", false);
            }
        });
        // submit request price
        $('form#submit-new-price-request').on('submit', function(e) {
            e.preventDefault();
            $(".show-price-request-error, .show-price-request-success").hide();
            $(".show-price-request-error").html("");
            $(".show-price-request-success").html("");
            var submit_frm = true;
            if(email_chkbox.is(':checked') == false && phone_chkbox.is(':checked') == false && message_chkbox.is(':checked') == false) {
                submit_frm = false;
                $(".show-price-request-error").show();
                $(".show-price-request-error").html("Please select atleast one Preferred Contact Method.");
            } else {
                submit_frm = true;
                $(".show-price-request-error").hide();
                $(".show-price-request-error").html("");
            }
            if(phone_chkbox.is(':checked') == true) {
                if($('form#submit-new-price-request input[name="mobile_number"]').val() == '') {
                        submit_frm = false;
                        $(".show-price-request-error").show();
                        $(".show-price-request-error").html("Please enter Phone Number.");
                } else {
                        submit_frm = true;
                        $(".show-price-request-error").hide();
                        $(".show-price-request-error").html("");
                }
            }
            if(submit_frm == true) {
                var form = $("#submit-new-price-request");
                var url = form.attr('action');
                var data = new FormData(form[0]);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    beforeSend: function () {
                        // show loading image and disable the submit button
                        $("button[name='submit-btn-price-request']").attr('disabled','disabled');
                        $("#loader").show();
                    },
                    success: function(result) {
                        // show success message
                        if(result.status == 0) {
                            $(".show-price-request-error").html(result.message);
                            $(".show-price-request-error").show();
                        } else {
                            $(".show-price-request-success").html(result.message);
                            $(".show-price-request-success").show();
                            // reset the form
                            resetPriceRequestFrm();
                            setTimeout(function() {$('#sendPriceRequest').modal('hide');window.location.reload(true);}, 2000);
                        }
                    },
                    error: function(xhr, status, error){
                        var errors = xhr.responseJSON;
                        $(".show-price-request-error").html(errors.error);
                    },
                    complete: function () {
                        // hide loading image and enable the submit button
                        $("#loader").hide();
                        $("button[name='submit-btn-price-request']").removeAttr('disabled');
                    }
                });
                return false;
            } else {
                return false;
            }
        });
    });
    function resetPriceRequestFrm()
    {
        var first_name = $('form#submit-new-price-request input[name="first_name"]');
        var last_name = $('form#submit-new-price-request input[name="last_name"]');
        var mobile_number = $('form#submit-new-price-request input[name="mobile_number"]');
        var event_date = $('form#submit-new-price-request input[name="event_date"]');
        @if(Auth::check() == false)
        var email = $('form#submit-new-price-request input[name="email"]');
        var password = $('form#submit-new-price-request input[name="password"]');
        @endif
        var about_extra_needs = $('form#submit-new-price-request textarea[name="about_extra_needs"]');

        email_chkbox.prop("checked", true);
        phone_chkbox.prop("checked", false);
        phone_chkbox.prop("checked", false);

        first_name.val('')
        last_name.val('')
        mobile_number.val('')
        event_date.val('')
        @if(Auth::check() == false)
        email.val('')
        password.val('')
        @endif
        about_extra_needs.val('')
        
    }
var ajaxResultGetRequest = null
$("#next-page-review").on('click', function() {
    var url = route('get-vendor-reviews-ajax')+'?page='+$(this).data('next-page') + '&vendor_id={{$vendorObj->getEncryptedId()}}';
    
    ajaxResultGetRequest = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            // show loading image
            // $("#loader-search").show();
            $('#next-page-review').hide();
            $('#spinner-load').show();
        },
        success: function(result) {
            // show results
            $('#vendor-reviews-results').append(result.reviews);
            $('#next-page-review').data('next-page', result.next_page);
            if(result.next_url == '') {
                $('#next-page-review').hide();
            } else {
                $('#next-page-review').show();
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            // hide loading image
            $('#spinner-load').hide();
            ajaxResultGetRequest = null;
        }
    });
});
var ajaxShortList = null;
$('#show_hide_bt').on('click', function(e) {
    var shortList;
    if ($(this).text().trim() == 'Shortlist') {
        shortList = 1;
    } else {
        shortList = 0;
    }

    var url = route('short-list-vendor-ajax')+'?vendor_id={{$vendorObj->getEncryptedId()}}'+'&shortlist='+shortList;
    
    ajaxShortList = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            $('#show_hide_bt').attr('disabled', true);
        },
        success: function(result) {
            $('#show_hide_bt').find('i').remove();
            if(shortList == 1) {
                $('#show_hide_bt').html($('<i/>',{class:'fa fa-heart'}).css('color', 'red')).append(' Shortlisted');
            } else {
                $('#show_hide_bt').html($('<i/>',{class:'fa fa-heart'}).css('color', 'white')).append(' Shortlist');
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            $('#show_hide_bt').attr('disabled', false);
            ajaxShortList = null;
        }
    });
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection