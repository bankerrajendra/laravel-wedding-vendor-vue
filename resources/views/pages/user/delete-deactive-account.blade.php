@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 hidden-xs">
                    @include('partials.user-side-menu')
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Delete/Deactivate Account
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Please bear in mind that deactivating or deleting your Account removes all information associated with your account from search and will no longer be viewable by other members.</p>
                            <button data-toggle="modal" data-target="#deleteAccountModal" class="btn btn-default btn-sm">Deactivate / Delete Account</button>
                            <div id="deleteAccountModal" class="modal fade" role="dialog" style="display: none;">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                            <h4 class="modal-title">Deactivate or Delete Account</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>If you're just hoping to take a break, we recommend disabling your account, not deleting it.</p>
                                            <p><b>Disable your account</b></p>
                                            <p>Your profile and photos will be removed from the site, but we'll keep everything for you in case you return so that you can start right where you left off.</p>
                                            <p>To reactivate your account, simply log back in at any time.</p>
                                            <p><a href="{{ route('user-deactivate-account') }}" class="btn btn-default btn-sm">Deactivate Account</a></p>
                                            <p><b>Delete your account</b></p>
                                            <hr>
                                            <p>This is PERMANENT</p>
                                            <p>If you want to use MuslimWedding again, you'll need to create a new account and fill out your profile again.</p>
                                            <p><b>Note:</b> Any messages you've sent will still be in the recipients' inboxes, even if you delete your account.</p>
                                            <p><a href="{{ route('user-delete-account') }}" class="btn btn-default btn-sm">Delete Account</a></p>
                                            <p>Once you delete your account, your profile will be removed from search and will no longer be viewable by other members and you must sign up again as a new customer. if you want to use our website. We will retain certain data for analytical purposes and record-keeping integrity, as well as to prevent fraud, collect any fees owed, enforce our terms and conditions, take actions we deem necessary to protect the integrity of our Services or our users, or take other actions otherwise permitted by law.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
@endsection