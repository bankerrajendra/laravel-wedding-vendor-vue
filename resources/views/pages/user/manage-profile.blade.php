@extends('layouts.app')
@section('template_linked_css')
<link href="{{asset('css/bootstrap-datepicker')}}.css" rel="stylesheet" type="text/css">
@endsection
@section('template_title')
    Manage Profile
@endsection
@section('content')
<div class="vendor-setting min_h">
    <div class="container">
        <div class="row">
            @if(!(Auth::user()->is_active()))
                <div class="text-center">
                    <div class="alert alert-info">Your account is currently <strong>under review</strong>!<br>Upon approval you will have full access.</div>
                </div>
            @endif
            <div class="col-sm-4 hidden-xs">
                @include('partials.user-manage-profile-side')
            </div>
            <div class="col-sm-8">
                <div class="alert alert-danger show-manage-profile-error" style="display: none;"></div>
                <div class="alert alert-success show-manage-profile-success" style="display: none;"></div>
                <form name="submit-manage-profile-frm" id="submit-manage-profile-frm" action="{{ route('handle-submit-manage-profile') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Manage Profile
                            </div>
                        </div>
                    </div>
                    <div class="row">	
                        <div class="col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="first_name">First Name *</label>
                                <input type="text" name="first_name" id="first_name" value="{{$userObj->first_name}}" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="last_name">Last Name *</label>
                                <input type="text" name="last_name" id="last_name" value="{{$userObj->last_name}}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Event Date</label>
                                <div id="filterDate2">
                                <!-- Datepicker as text field -->         
                                    <div class="input-group date" data-date-format="dd.mm.yyyy">
                                    <input type="text" name="event_date" class="form-control" placeholder="dd.mm.yyyy" value="{{$event_date}}" autocomplete="off">
                                        <div class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="budget">Your Budget *</label>
                                <select name="budget" id="budget" class="form-control" required="" aria-required="true">
                                    <option value="">Unknown</option>
                                    @foreach (config('constants.user_budget') as $budget_key => $budget_val)
                                        <option @if($budget == $budget_key) selected="selected" @endif value="{{$budget_key}}">{{$budget_val}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel_head text-center">
                                Choose Vendors
                            </div>
                        </div>
                    </div>
                    <div class="tabbable-line" style="padding:0px;">
                        <div class="row cat-list">
                            <div class="form-group">
                                @if($categories->count() > 0)
                                    @foreach ($categories as $category)
                                        <div class="col-sm-4 col-xs-6 panel_body">
                                            <label class="checkbox-inline">
                                                <input name="vendor_categories[]" type="checkbox" value="{{$category->id}}" @if(in_array($category->id, $vendor_categories)) checked="checked" @endif><span class="checkmark"></span> {{$category->name}}
                                            </label>
                                        </div>        
                                    @endforeach
                                @endif
                            </div>
                        </div>	
                        <hr>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 text-right">
                                <button type="submit" name="submit-btn-frm-manage-profile" class="btn btn-default">SAVE</button>
                                <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none;float: right;margin-left: 10px;margin-top: 5px;">
                            </div>
                        </div>
                        <br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_scripts')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    date.setDate(date.getDate()-1);
    $('.input-group.date').datepicker({format: "dd.mm.yyyy", startDate: date});

    $("#submit-manage-profile-frm").on('submit',function(e){
        e.preventDefault();
        $(".show-manage-profile-error, .show-manage-profile-success").hide();
        $(".show-manage-profile-error").html("");
        $(".show-manage-profile-success").html("");
        var form = $("#submit-manage-profile-frm");
        var url = form.attr('action');
        var data = new FormData(form[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                // show loading image and disable the submit button
                $("button[name='submit-btn-frm-manage-profile']").attr('disabled','disabled');
                $("#loader").show();
            },
            success: function(result) {
                // show success message
                if(result.status == 0) {
                    $(".show-manage-profile-error").html(result.message);
                    $(".show-manage-profile-error").show();
                } else {
                    $(".show-manage-profile-success").html(result.message);
                    $(".show-manage-profile-success").show();
                }
            },
            error: function(xhr, status, error){
                var errors = xhr.responseJSON;
                //console.log(errors);
                var err_html = 'Errors<br />';
                for (var key in errors) {
                    if (errors.hasOwnProperty(key)) {
                        var val = errors[key];
                        //console.log(val);
                        err_html += ' - '+val[0]+'<br />';
                    }
                }
                $(".show-manage-profile-error").html(err_html);
                $(".show-manage-profile-error").show();
            },
            complete: function () {
                // hide loading image and enable the submit button
                $("#loader").hide();
                $("button[name='submit-btn-frm-manage-profile']").removeAttr('disabled');
            }
        });
    });

</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection