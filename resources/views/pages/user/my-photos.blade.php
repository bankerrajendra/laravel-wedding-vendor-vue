<?php use Illuminate\Support\Facades\Session; ?>
@extends('layouts.app')
@section('template_linked_css')
	<link href="{{asset('dist/css/cropper.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('dist/css/main.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('template_title')
   Edit Profile
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection

@section('content')
    <div class="d_account">
<div class="container">

		<?php
			$activesession="public";
			if (\Session::has('active_section')) {
                $activesession=Session::get('active_section');
			}
		?>



		<div class="row">
			@include('partials.my-profile-navigation')

			<div class="col-sm-8 panel-body pub_pri_photo">
				<h4 class="text-center"> Become a Diamond Member &amp; get more responses by uploading upto 10 photos on your profile.</h4>
				<br>
				@if(session('success'))

					<div class="alert alert-success avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>{{session('success')}}</div>

				@endif

				@if(session('error'))
					<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>{{session('error')}}</div>
				@endif



				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<ul class="nav nav-tabs">
					<li class="@if($activesession=='public') active @endif"><a data-toggle="tab" href="#menu1">Public Photo</a></li>
					<li class="@if($activesession=='private') active @endif"><a data-toggle="tab" href="#menu2">Private Photo</a></li>
				</ul>
				<div class="tab-content">
					<div id="menu1" class="tab-pane fade @if($activesession=='public') in active @endif">
						<br>
						<div class="crop-avatar text-center">
							<div class="photo-panel">
								<h4>Upload Public Photos</h4>
								<div class="avatar-view" data-original-title="" title="">
									<span id="fileselector">
										<label class="btn btn-success">
											<i class="fa fa-upload" aria-hidden="true"></i> Select Photo
										</label>
									</span>
								</div>
								<!-- Cropping modal -->
								<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
									<div class="modal-dialog modal-lg">

										<div class="modal-content">


											<form class="avatar-form" id="imageUploadForm" action="{{ route('ajax-upload-image') }}" enctype="multipart/form-data" method="post">
												{{--<input type="hidden" value="93e1b98e246c6ed64a9daf4336bed28e" name="codeprofile">--}}

												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" id="resetPhoto" value="reset">×</button>
													<h4 class="modal-title" id="avatar-modal-label">Upload Public Photo</h4>
												</div>

												<div class="modal-body">
													<div class="avatar-body">

														<!-- Upload image and data -->
														<div class="avatar-upload">
															<input type="hidden" class="avatar-src" name="avatar_src">
															<input type="hidden" class="avatar-data" name="avatar_data">
															<input type="hidden" name="_token" value="{{csrf_token()}}">
															<input type="hidden"  name="photo_type" value="public">
															<input type="hidden"  name="allowed_photos" value='<?php echo $public_photo_allowed; ?>'>
															<label for="avatarInput">Select Photo</label>
															<input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
														</div>

														<!-- Crop and preview -->
														<div class="row">
															<div class="col-md-9">
																<div class="avatar-wrapper"></div>
															</div>
															<div class="col-md-3">
																<div class="avatar-preview preview-lg hidden-xs">@if($user->gender == 'M')
																		<img src="{{asset('img/male-default.png')}}">
																	@else
																		<img src="{{asset('img/female-default.png')}}">
																	@endif</div>
																<div class="avatar-preview preview-md remove hidden hidden-xs">@if($user->gender == 'M')
																		<img src="{{asset('img/male-default.png')}}">
																	@else
																		<img src="{{asset('img/female-default.png')}}">
																	@endif</div>
																<div class="avatar-preview preview-sm remove hidden hidden-xs">@if($user->gender == 'M')
																		<img src="{{asset('img/male-default.png')}}">
																	@else
																		<img src="{{asset('img/female-default.png')}}">
																	@endif</div>
																<br>
																<button type="submit" id="public" class="btn btn-primary btn-block avatar-save" style="background-color:#f0ad4e; border-color:#eea236">Add Photo</button>
															</div>
														</div>

														<div class="row avatar-btns panel-body">
															<div class="col-md-9 text-center">
																<div class="btn-group">
																	<button type="button" class="btn btn-success" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
																</div>
																<div class="btn-group">
																	<button type="button" class="btn btn-success" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
																</div>
															</div>
															<div class="col-md-3">
																<div class="avatar-view" title="" style="display:none" data-original-title="Change the profile Picture">
																	@if($user->gender == 'M')
																		<img src="{{asset('img/male-default.png')}}" alt="Profile Picture" id="profileimage" style="height:auto;">
																	@else
																		<img src="{{asset('img/female-default.png')}}" alt="Profile Picture" id="profileimage" style="height:auto;">
																	@endif
																</div>
															</div>
														</div>

													</div>
												</div>
												<!-- modal-body -->
											</form>
											<!-- form -->
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<p class="text-center">Note: Each photo must be in jpg, png and gif format. </p>
						<p class="text-center">Note: All photos are screened as per photo guidelines and gets activated within 24 to 48 hours. </p>
						<div id="successMessage"></div>
						<br>
						<div class="row">
							@php
								if($status=="Free") {
									$canUploadImages = ($public_photo_allowed - $existingPublicImagesCount);
									$canUploadPrivateImages = ($private_photo_allowed - $existingPrivateImagesCount);
								} else {
									$thumbShow=config('constants.photo_thumb_show_for_paid_members');
									$canUploadImages = ($existingPublicImagesCount>=$thumbShow)?0:($thumbShow-$existingPublicImagesCount);
									$canUploadPrivateImages = ($existingPrivateImagesCount>=$thumbShow)?0:($thumbShow-$existingPrivateImagesCount);
								}

							@endphp

							@if($existingPublicImagesCount > 0)
								@foreach($existingPublicImages as $existingImage)
									@if($existingImage->is_approved=='Y')
										<div class="col-sm-6 col-xs-12 col-lg-4 panel-body">
											<div class="upload_photo" id="{{$existingImage->id}}">
												<img src="{{config('constants.S3_WEDDING_IMAGES').$existingImage->image_thumb}}" class="img-thumbnail">
												<div class="dropdown pull-right">
													<button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown"  style="padding:6px 12px!important; margin-right:0px!important;"><i class="fa fa-cog" aria-hidden="true"></i></button>
													<ul class="dropdown-menu">
														@if($existingImage->is_profile_image == 'N')
														<li><a href="{{route('set-profile-image',['id'=>$existingImage->id])}}" class="profilePicButton" onclick="">Set as Profile</a></li>
														@endif
															<li><a href="{{route('ajax-movetoprivate-image',['id'=>$existingImage->id])}}" class="profilePicButton" onclick="">Move to Private</a></li>

														<li><a href="{{route('ajax-delete-image',['id'=>$existingImage->id, 'activesection'=>'public'])}}" class="deleteButton" onclick="">Delete</a></li>
													</ul>
												</div>
											</div>
										</div>
									@else
										<div class="col-sm-6 col-xs-12 col-lg-4 panel-body">
											<div class="upload_photo" id="{{$existingImage->id}}">
												<img src="{{config('constants.S3_WEDDING_IMAGES').$existingImage->image_thumb}}" class="img-thumbnail">
												@if($existingImage->disapprove_reason!=NULL && !empty($existingImage->disapprove_reason))
													<span style='color:Red; text-align: center; font-size: x-small'><strong>Reason of disapproval:</strong> {{$existingImage->disapprove_reason}}</span>
												@else
													<span style='color:green; text-align: center'>Awaiting Approval</span>
												@endif
												<div class="dropdown pull-right">
													<button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown"  style="padding:6px 12px!important; margin-right:0px!important;"><i class="fa fa-cog" aria-hidden="true"></i></button>
													<ul class="dropdown-menu">
														<li><a href="{{route('ajax-delete-image',['id'=>$existingImage->id, 'activesection'=>'public'])}}" class="deleteButton" onclick="">Delete</a></li>
													</ul>
												</div>
											</div>
										</div>
									@endif
								@endforeach
							@endif
							@for($i = 0; $i < $canUploadImages; $i++)
								<div class="col-sm-6 col-xs-12 col-lg-4 panel-body">
									<div class="upload_photo default-image">
										@if($user->gender == 'M')
											<img src="{{asset('img/male-default.png')}}" class="img-thumbnail">
										@else
											<img src="{{asset('img/female-default.png')}}" class="img-thumbnail">
										@endif
										<div class="dropdown pull-right" style="display: none">
											<button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown"  style="padding:6px 12px!important; margin-right:0px!important;"><i class="fa fa-cog" aria-hidden="true"></i></button>
											<ul class="dropdown-menu">
												<li><a href="javascript:;" class="profilePicButton">Set as Profile</a></li>
												<li><a href="javascript:;" class="deleteButton">Delete</a></li>
											</ul>
										</div>
									</div>
								</div>
							@endfor





							<span id="aimage"> </span>
							<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.js"></script>
							<script>
                                $(".pimage").change(function(e) {
                                    for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                                        var file = e.originalEvent.srcElement.files[i];
                                        var img = document.createElement("img");
                                        var reader = new FileReader();
                                        reader.onloadend = function() {
                                            img.src = reader.result;
                                        }
                                        reader.readAsDataURL(file);
                                    //    $("#aimage").after(img);
                                    }
                                });
							</script>
						</div>
						<hr>
						<p class="text-justify"><b>Note:</b> Each photo must not be less than 440px 440px and you should be in th\e photo. The uploaded, images can take up to 48 hours to be reviewed and fully visible on your profile.</p>
						<p class="text-justify"><b>Note:</b> Your face MUST be clearly visible in your MAIN IMAGE. All images MUST contain you.</p>
						<p class="text-justify"><b>Note:</b> Any Images that doesn’t feature yourself will be removed.</p>
					</div>

					<div id="menu2" class="tab-pane fade @if($activesession=='private') in active @endif">
						<br>
						<div class="crop-avatarpri text-center">
							<div class="photo-panel">
								<h4>Upload Private Photos</h4>
								<div class="avatar-view" data-original-title="" title="">
									<span id="fileselector">
										<label class="btn btn-success">
											<i class="fa fa-upload" aria-hidden="true"></i> Select Photo
										</label>
									</span>
								</div>
								<!-- Cropping modal -->
								<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
									<div class="modal-dialog modal-lg">

										<div class="modal-content">

											<form class="avatar-form" id="imagePrivateUploadForm" action="{{ route('ajax-upload-image') }}" enctype="multipart/form-data" method="post">


												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" id="resetPhoto" value="reset">×</button>
													<h4 class="modal-title" id="avatar-modal-label">Upload Private Photo</h4>
												</div>

												<div class="modal-body">
													<div class="avatar-body">

														<!-- Upload image and data -->
														<div class="avatar-upload">
															<input type="hidden" class="avatar-src" name="avatar_src">
															<input type="hidden" class="avatar-data" name="avatar_data">
															<input type="hidden" name="_token" value="{{csrf_token()}}">
															<input type="hidden"  name="photo_type" value="private">
															<input type="hidden"  name="allowed_photos" value='<?php echo $private_photo_allowed; ?>'>
															<label for="avatarInput1">Select Photo</label>
															<input type="file" class="avatar-input" id="avatarInput1" name="avatar_file">
														</div>

														<!-- Crop and preview -->
														<div class="row">
															<div class="col-md-9">
																<div class="avatar-wrapper"></div>
															</div>
															<div class="col-md-3">
																<div class="avatar-preview preview-lg hidden-xs">@if($user->gender == 'M')
																		<img src="{{asset('img/male-default.png')}}">
																	@else
																		<img src="{{asset('img/female-default.png')}}">
																	@endif</div>
																<div class="avatar-preview preview-md remove hidden hidden-xs">@if($user->gender == 'M')
																		<img src="{{asset('img/male-default.png')}}">
																	@else
																		<img src="{{asset('img/female-default.png')}}">
																	@endif</div>
																<div class="avatar-preview preview-sm remove hidden hidden-xs">@if($user->gender == 'M')
																		<img src="{{asset('img/male-default.png')}}">
																	@else
																		<img src="{{asset('img/female-default.png')}}">
																	@endif</div>
																<br>
																<button type="submit" class="btn btn-primary btn-block avatar-save" style="background-color:#f0ad4e; border-color:#eea236">Add Photo</button>
															</div>
														</div>

														<div class="row avatar-btns panel-body">
															<div class="col-md-9 text-center">
																<div class="btn-group">
																	<button type="button" class="btn btn-success" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
																</div>
																<div class="btn-group">
																	<button type="button" class="btn btn-success" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
																</div>
															</div>
															<div class="col-md-3">
																<div class="avatar-view" title="" style="display:none" data-original-title="Change the profile Picture">
																	@if($user->gender == 'M')
																		<img src="{{asset('img/male-default.png')}}" alt="Profile Picture" id="profileimage" style="height:auto;">
																	@else
																		<img src="{{asset('img/female-default.png')}}" alt="Profile Picture" id="profileimage" style="height:auto;">
																	@endif
																</div>
															</div>
														</div>

													</div>
												</div>
												<!-- modal-body -->
											</form>
											<!-- form -->
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<p class="text-center">Note: Each photo must be in jpg, png and gif format. </p>
						<p class="text-center">Note: All photos are screened as per photo guidelines and gets activated within 24 to 48 hours. </p>
						<br>
						<div class="row">


							@if($existingPrivateImagesCount > 0)
								@foreach($existingPrivateImages as $existingImage)
									@if($existingImage->is_approved=='Y')
										<div class="col-sm-6 col-xs-12 col-lg-4 panel-body">
											<div class="upload_photo" id="{{$existingImage->id}}">
												<img src="{{config('constants.S3_WEDDING_IMAGES').$existingImage->image_thumb}}" class="img-thumbnail">
												<div class="dropdown pull-right">
													<button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown" style="padding:6px 12px!important; margin-right:0px!important;"><i class="fa fa-cog" aria-hidden="true"></i></button>
													<ul class="dropdown-menu">
														<li><a href="{{route('ajax-movetopublic-image',['id'=>$existingImage->id])}}" class="profilePicButton" onclick="">Move to Public</a></li>
														<li><a href="{{route('ajax-delete-image',['id'=>$existingImage->id, 'activesection'=>'private'])}}" class="deleteButton" onclick="">Delete</a></li>
													</ul>
												</div>
											</div>
										</div>
									@else
										<div class="col-sm-6 col-xs-12 col-lg-4 panel-body">
											<div class="upload_photo" id="{{$existingImage->id}}">
												<img src="{{config('constants.S3_WEDDING_IMAGES').$existingImage->image_thumb}}" class="img-thumbnail">
												@if($existingImage->disapprove_reason!=NULL && !empty($existingImage->disapprove_reason))
													<span style='color:Red; text-align: center; font-size: x-small'><strong>Reason of disapproval:</strong> {{$existingImage->disapprove_reason}}</span>
												@else
													<span style='color:green; text-align: center'>Awaiting Approval</span>
												@endif

												<div class="dropdown pull-right">
													<button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown"  style="padding:6px 12px!important; margin-right:0px!important;"><i class="fa fa-cog" aria-hidden="true"></i></button>
													<ul class="dropdown-menu">
														<li><a href="{{route('ajax-delete-image',['id'=>$existingImage->id, 'activesection'=>'private'])}}" class="deleteButton" onclick="">Delete</a></li>
													</ul>
												</div>
											</div>
										</div>
									@endif
								@endforeach
							@endif
							@for($i = 0; $i < $canUploadPrivateImages; $i++)
								<div class="col-sm-6 col-xs-12 col-lg-4 panel-body">
									<div class="upload_photo default-image">
										@if($user->gender == 'M')
											<img src="{{asset('img/male-default.png')}}" class="img-thumbnail">
										@else
											<img src="{{asset('img/female-default.png')}}" class="img-thumbnail">
										@endif
										<div class="dropdown pull-right" style="display: none">
											<button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown"  style="padding:6px 12px!important; margin-right:0px!important;"><i class="fa fa-cog" aria-hidden="true"></i></button>
											<ul class="dropdown-menu">
												<li><a href="javascript:;" class="profilePicButton">Set as Profile</a></li>
												<li><a href="javascript:;" class="deleteButton">Delete</a></li>
											</ul>
										</div>
									</div>
								</div>
							@endfor


							<span id="aimage"> </span>
							<script type="text/javascript" src="//code.jquery.com/jquery-1.7.1.js"></script>
							<script>
                                $(".pimage").change(function(e) {
                                    for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                                        var file = e.originalEvent.srcElement.files[i];
                                        var img = document.createElement("img");
                                        var reader = new FileReader();
                                        reader.onloadend = function() {
                                            img.src = reader.result;
                                        }
                                        reader.readAsDataURL(file);
                                       // $("#aimage").after(img);
                                    }
                                });
							</script>
						</div>
						<hr>
						<p class="text-justify"><b>Note:</b> Each photo must not be less than 440px 440px and you should be in the photo. The uploaded, images can take up to 48 hours to be reviewed and fully visible on your profile.</p>
						<p class="text-justify"><b>Note:</b> Your face MUST be clearly visible in your MAIN IMAGE. All images MUST contain you.</p>
						<p class="text-justify"><b>Note:</b> Any Images that doesn’t feature yourself will be removed.</p>
					</div>

				</div>
			</div>
		</div>
</div>
</div>
@endsection
@section('footer_scripts')
	<script src="{{asset('dist/js/cropper.min.js')}}"></script>
	<script src="{{asset('dist/js/main.js')}}"></script>
	<script>
				@if(Auth::user()->gender == 'M')
        var default_image_path = "{{asset('img/male-default.png')}}";
				@else
        var default_image_path = "{{asset('img/female-default.png')}}";
		@endif




        $(".pimage").change(function(e) {
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                var file = e.originalEvent.srcElement.files[i];
                var img = document.createElement("img");
                var reader = new FileReader();
                reader.onloadend = function() {
                    img.src = reader.result;
                }
                reader.readAsDataURL(file);
//                $("#aimage").after(img);
            }
        });

        $("#imagePrivateUploadForm").on('submit', function(e){
            e.preventDefault();
            var image = $("#avatarInput1")[0].files[0];
//            alert("hello1");
            if (image === undefined){
                var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
                $(".avatar-upload").after(msg);
                return false;
            } else {
                $("#loader").show();
                $("button.avatar-save").attr('disabled','disabled');
                var _token = $('input[name="_token"]').val();
                var thumb_data = $('input[name="avatar_data"]').val();
                var photo_type = 'private'; //$('input[name="photo_type"]').val();
                var allowed_photos = '<?php echo $private_photo_allowed; ?>'; //'$('input[name="allowed_photos"]').val();
                 //alert(photo_type +':'+allowed_photos);
                var form = new FormData();
                form.append('image', image);
                form.append('_token', _token);
                form.append('thumb_data', thumb_data);
                form.append('photo_type', photo_type);
                form.append('allowed_photos', allowed_photos);
                $.ajax({
                    url: '{{route('ajax-upload-image')}}',
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(data) {
                        //location.reload();
                        $("#loader").hide();
                        var response = data[0];
                        //console.log(response);
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                        if(response.status === 0){
                            $(".avatar-upload").after(msg);

                            $("button.avatar-save").removeAttr('disabled');
                        } else {
                            location.reload();
                            {{--$("#avatar-modalpri").modal('hide');--}}
                            {{--$(".default-image:first .img-thumbnail").attr('src','{{config('constants.upload_url_public')}}'+response.thumb_path);--}}
                            {{--$(".default-image:first .dropdown").show();--}}
                            {{--$(".default-image:first").attr('id', response.image_id);--}}
                            {{--$(".default-image:first .dropdown .profilePicButton").attr('onClick', 'setProfileImage('+response.image_id+')');--}}
                            {{--$(".default-image:first .dropdown .deleteButton").attr('onClick', 'deleteImage('+response.image_id+')');--}}
                            {{--$(".default-image:first").removeClass('default-image');--}}
                        }
                    },
                    error: function(data){
                        $("#loader").hide();
                        var errors = data.responseJSON;
                        //console.log(errors);
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[1]+'</div>';
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled');
                    }

                });

            }
        });

        $("#imageUploadForm").on('submit', function(e){
            e.preventDefault();
            var image = $("#avatarInput")[0].files[0];
            //alert("hello");
            if (image === undefined){
                var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
                $(".avatar-upload").after(msg);
                return false;
            }else{
                $("#loader").show();
                $("button.avatar-save").attr('disabled','disabled');
                var _token = $('input[name="_token"]').val();
                var thumb_data = $('input[name="avatar_data"]').val();
               // var photo_type = $('input[name="photo_type"]').val();
               // var allowed_photos = $('input[name="allowed_photos"]').val();
                var photo_type = 'public'; //$('input[name="photo_type"]').val();
                var allowed_photos = '<?php echo $public_photo_allowed; ?>'; //'$('input[name="allowed_photos"]').val();
               // alert(photo_type);
                var form = new FormData();
                form.append('image', image);
                form.append('_token', _token);
                form.append('thumb_data', thumb_data);
                form.append('photo_type', photo_type);
                form.append('allowed_photos', allowed_photos);
                $.ajax({
                    url: '{{route('ajax-upload-image')}}',
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(data) {
                       //location.reload();
                       // location.reload();

                        $("#loader").hide();
                        var response = data[0];
                        //console.log(response);
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                        if(response.status === 0){
                            $(".avatar-upload").after(msg);

                            $("button.avatar-save").removeAttr('disabled');
                           // return false;
                        } else {
                            location.reload();
                            {{--$("#avatar-modal").modal('hide');--}}
                            {{--$(".default-image:first .img-thumbnail").attr('src','{{config('constants.upload_url_public')}}'+response.thumb_path);--}}
                            {{--$(".default-image:first .dropdown").show();--}}
                            {{--$(".default-image:first").attr('id', response.image_id);--}}
                            {{--$(".default-image:first .dropdown .profilePicButton").attr('onClick', 'setProfileImage('+response.image_id+')');--}}
                            {{--$(".default-image:first .dropdown .deleteButton").attr('onClick', 'deleteImage('+response.image_id+')');--}}
                            {{--$(".default-image:first").removeClass('default-image');--}}
                        }
                    },
                    error: function(data){
                        $("#loader").hide();
                        var errors = data.responseJSON;
                        //console.log(errors);
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[1]+'</div>';
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled'); return false;
                    }


                });

            }
        });
        $('#avatar-modal').on('hidden.bs.modal', function () {
            $("#loader").hide();
            $(".avatar-body .alert").remove();
            $("#imageUploadForm")[0].reset();
            $("button.avatar-save").removeAttr('disabled');

        });


        $('#avatar-modalpri').on('hidden.bs.modal', function () {
            $("#loader").hide();
            $(".avatar-body .alert").remove();
            $("#imagePrivateUploadForm")[0].reset();
            $("button.avatar-save").removeAttr('disabled');

        });


        {{--function setProfileImage(id){--}}
            {{--var _token = $('input[name="_token"]').val();--}}
            {{--var form = new FormData();--}}
            {{--form.append('id', id);--}}
            {{--form.append('_token', _token);--}}
            {{--$.ajax({--}}
                {{--url: '{{route('set-profile-image')}}',--}}
                {{--data: form,--}}
                {{--cache: false,--}}
                {{--contentType: false,--}}
                {{--processData: false,--}}
                {{--type: 'POST',--}}
                {{--success: function (data) {--}}
                    {{--location.reload();--}}
                    {{--var msg = '<div class="alert alert-success avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+data.message+'</div>';--}}
                    {{--$("#successMessage").html(msg);--}}
                {{--}--}}
            {{--});--}}
        {{--}--}}

        {{--function deleteImage(id){--}}
            {{--var _token = $('input[name="_token"]').val();--}}
            {{--var form = new FormData();--}}
            {{--form.append('id', id);--}}
            {{--form.append('_token', _token);--}}
            {{--$.ajax({--}}
                {{--url: '{{route('ajax-delete-image')}}',--}}
                {{--data: form,--}}
                {{--cache: false,--}}
                {{--contentType: false,--}}
                {{--processData: false,--}}
                {{--type: 'POST',--}}
                {{--success:function(data) {--}}
                    {{--location.reload();--}}
                    {{--$(".upload_photo .img-thumbnail").attr('src', default_image_path);--}}
                    {{--$(".upload_photo .dropdown ").hide();--}}
                    {{--$(".upload_photo").addClass('default-image');--}}

                {{--}--}}
            {{--});--}}
        {{--}--}}

        {{--function moveImageToPrivate(id) {--}}
            {{--var _token = $('input[name="_token"]').val();--}}
            {{--var form = new FormData();--}}
            {{--form.append('id', id);--}}
            {{--form.append('_token', _token);--}}
            {{--$.ajax({--}}
                {{--url: '{{route('ajax-movetoprivate-image')}}',--}}
                {{--data: form,--}}
                {{--cache: false,--}}
                {{--contentType: false,--}}
                {{--processData: false,--}}
                {{--type: 'POST',--}}
                {{--success:function(data) {--}}
                    {{--location.reload();--}}
                  {{--//  alert(data.message);--}}

                    {{--$(".upload_photo .img-thumbnail").attr('src', default_image_path);--}}
                    {{--$(".upload_photo .dropdown ").hide();--}}
                    {{--$(".upload_photo").addClass('default-image');--}}

                {{--}--}}
            {{--});--}}
        {{--}--}}
		{{--function moveImageToPublic(id) {--}}
			{{--var _token = $('input[name="_token"]').val();--}}
			{{--var form = new FormData();--}}
			{{--form.append('id', id);--}}
			{{--form.append('_token', _token);--}}
			{{--$.ajax({--}}
				{{--url: '{{route('ajax-movetopublic-image')}}',--}}
				{{--data: form,--}}
				{{--cache: false,--}}
				{{--contentType: false,--}}
				{{--processData: false,--}}
				{{--type: 'POST',--}}
				{{--success:function(data) {--}}
                    {{--location.reload();--}}
					{{--$(".upload_photo .img-thumbnail").attr('src', default_image_path);--}}
					{{--$(".upload_photo .dropdown ").hide();--}}
					{{--$(".upload_photo").addClass('default-image');--}}

				{{--}--}}
			{{--});--}}
		{{--}--}}

	</script>
@endsection