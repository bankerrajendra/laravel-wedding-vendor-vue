@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('content')
<div class="login_p signup">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h1><span>Email</span></h1>
                <p class="text-center">@if(!Auth::check())What email address would you like to use to receive vendor quotes?@else We will send updates on your registered email: <strong>{{$email}}</strong> @endif</p>
            </div>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="tabbable-line">
                    <div class="alert alert-danger show-price-request-error" style="display: none;"></div>
                    <div class="alert alert-success show-price-request-success" style="display: none;"></div>
                    <form name="frm-submit-user-vendor-mail" action="{{ route('handle-submit-user-vendor-mail') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    Email Address
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="first-name">First Name *</label>
                                    <input type="text" id="first-name" name="first_name" class="form-control" value="{{$first_name}}" placeholder="First Name">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="last-name">Last Name *</label>
                                    <input type="text" id="last-name" name="last_name" class="form-control" value="{{$last_name}}" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        @if(!Auth::check())
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="email">Email *</label>
                                    <input type="email" value="{{$email}}" name="email" class="form-control" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="password">Password *</label>
                                    <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                                </div>
                            </div>
                        </div>	
                        @endif
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <p>Please add "info@muslimwedding.com" to your address bookwedding to make sure you receive our messages.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <a href="{{route('user-vendor-message')}}" style="font-size:18px;"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back</a>
                            </div>
                            <div class="col-sm-6 col-xs-12 text-right">
                                <button type="submit" name="submit-btn-price-request" class="btn btn-default btn-block">SUBMIT</button>
                            </div>
                        </div>
                        <input name="categories" value="{{$categories}}" type="hidden" />
                        <input name="wedding_type" value="{{$wedding_type}}" type="hidden" />
                        <input name="no_of_guest" value="{{$no_of_guest}}" type="hidden" />
                        <input name="mobile_number" value="{{$mobile_number}}" type="hidden" />
                        <input name="preferred_contact_method" value="{{$preferred_contact_method}}" type="hidden" />
                        <input name="event_date" value="{{$event_date}}" type="hidden" />
                        <input name="country" value="{{$country}}" type="hidden" />
                        <input name="state" value="{{$state}}" type="hidden" />
                        <input name="city" value="{{$city}}" type="hidden" />
                        @if($description != '')
                            @foreach ($description as $msg_key => $msg_val)
                            <input name="description[{{$msg_key}}]" value="{{$msg_val}}" type="hidden" />     
                            @endforeach
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
$(document).ready(function() {
    // submit request price
    $('form[name="frm-submit-user-vendor-mail"]').on('submit', function(e) {
        e.preventDefault();
        $(".show-price-request-error, .show-price-request-success").hide();
        $(".show-price-request-error").html("");
        $(".show-price-request-success").html("");
        var form = $('form[name="frm-submit-user-vendor-mail"]');
        var url = form.attr('action');
        var data = new FormData(form[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
                // show loading image and disable the submit button
                $("button[name='submit-btn-price-request']").attr('disabled','disabled');
                $("#loader").show();
            },
            success: function(result) {
                // show success message
                if(result.status == 0) {
                    $(".show-price-request-error").html(result.message);
                    $(".show-price-request-error").show();
                } else {
                    $(".show-price-request-success").html(result.message);
                    $(".show-price-request-success").show();
                    setTimeout(function() {window.location.href=route('muslim-vendors');}, 1000);
                }
            },
            error: function(xhr, status, error){
                var errors = xhr.responseJSON;
                var err_msg = '';
                err_msg += ' - '+visit(errors)+'<br>';
                //$(".show-price-request-error").html(err_msg);
                $(".show-price-request-error").show();
            },
            complete: function () {
                // hide loading image and enable the submit button
                $("#loader").hide();
                $("button[name='submit-btn-price-request']").removeAttr('disabled');
            }
        });
    });
});
function visit(object) {
    if (isIterable(object)) {
        forEachIn(object, function (accessor, child) {
            visit(child);
        });
    }
    else {
        var value = object;
        $(".show-price-request-error").append(' - '+value+'<br>');
    }
}
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection