@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('template_linked_css')
@endsection
@section('content')
    <div class="vendor-head-content" id="header-sec-search-events">
        <div class="container text-center hidden-xs">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    {{-- Category Title --}}
                    <h2 id="page-category-title">@if($category_name == ''){{$metafields['heading']}}@else{{$category_name}}@endif</h2>
                    {{-- Category Description --}}
                    <p id="page-category-description">@if($metafields['cat_description'] == '') {{config('constants.default_category_description_for_search_event')}} @else{{$metafields['cat_description']}}@endif</p>
                </div>
            </div>
        </div>
    </div>
    <div class="search-vendor">
        <div class="container hidden-xs">
            <div class="row">
                @if(Auth::check())
                    @if(!(Auth::user()->is_active()))
                        <div class="text-center">
                            <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                        </div>
                    @endif
                @endif
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                    <div class="se-ven-form">
                        <div class="row">
                            <form action="{{ route('search-events') }}" method="GET" name="search-event-frm" id="search-event-frm">
                                @csrf
                                <div class="col-lg-5 no-padding">
                                    <input name="category_name" type="text" id="category_search" class="form-control" placeholder="Find Events" value="{{$category_name}}" autocomplete="off">
                                    <div id="categoryList">
                                    </div>
                                </div>
                                <div class="col-lg-5 no-padding">
                                    <input name="city_zip" value="{{$city_zip}}" id="city_state_country" type="text" class="form-control" placeholder="City/State/Country" autocomplete="off">
                                    <div id="locationList">
                                    </div>
                                </div>
                                <div class="col-lg-2 no-padding">
                                    <input type="submit" class="btn btn-default" value="SEARCH">
                                    <img src="{{asset('img/loader.gif')}}" id="loader-search" width="20" style="display: none;margin-left: 50px; margin-top: 10px;">
                                </div>
                                <input type="hidden" name="location_type" value="{{$location_type}}" />
                                <input type="hidden" name="location_id" value="{{$location_id}}" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--End search form-->
        <div class="m-head hidden-lg hidden-md">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h5>EVENTS</h5>
                        <h3 id="mobile-cat-name">@if($category_name == ''){{config('constants.default_category_title_for_search_event')}}@else{{$category_name}}@endif</h3>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#changeLocMob">Change Location</a> |  <a href="javascript:void(0);" data-toggle="modal" data-target="#changeCatsModal">Change Category</a>
                        <div id="changeLocMob" class="collapse">
                            <div class="form-group">
                                <div id="custom-search-input" style="padding: 0px 0px;">
                                    <div class="input-group">
                                        <input type="text" value="{{$city_zip}}" class="form-control" placeholder="LOCATION" autocomplete="off" name="city_zip_mobile_modal" id="city_zip_mobile_modal">
                                        <span class="input-group-btn">
                                            <button class="btn btn-danger" name="mobile-location-search-modal" type="button" onclick="javascript:callAjxGetRecordSet();">
                                                <span class="fa fa-search"></span>
                                            </button>
                                        </span>
                                        <div id="locationMobileModalList">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="changeCatsModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Select Category</h4>
                    </div>
                    <div class="modal-body">
                        @if(count($categories) > 0)
                        <ul class="listing">
                            @foreach ($categories as $category)
                                <li><a href="javascript:void(0);" onclick="javascript:SearchCatByLink('{{$category->name}}');">{{$category->name}}</a></li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    
        <div class="m-filter hidden-lg hidden-md">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-6">
                        <p id="show-total-records">{{$events->total()}} event(s)</p>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <p class="text-right"><a href="javascript:void(0);" data-toggle="modal" data-target="#searchLocfilter"><i class="fa fa-filter" aria-hidden="true"></i> Filter</a></p>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- filter -->
        <div id="searchLocfilter" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Filters &nbsp;&nbsp;&nbsp;&nbsp;<small><a href="{{ route('search-events') }}">Clear All</a></small></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Location</label>
                            <div id="custom-search-input">
                                <div class="input-group col-md-12">
                                    <input type="text" value="{{$city_zip}}" class="search-query form-control" placeholder="LOCATION" name="city_zip_mobile_filter" id="city_zip_mobile_filter">
                                    <span class="input-group-btn">
                                        <button class="btn btn-danger" type="button">
                                            <span class="fa fa-search"></span>
                                        </button>
                                    </span>
                                    <div id="locationMobileFilterList">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-default btn-block" onclick="javascript:callAjxGetRecordSet();">Apply</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container hidden-xs">
            <div class="row">
                <div class="col-sm-12">
                    <span id="show-total-records-big" class="r-result">{{$events->total()}} results:</span>
                    <ul id="myDIV">
                        @if($category_name != '' || $city_zip != '')
                            @if($category_name != '')
                            <li id="category-flt">{{$category_name}} <span class="close-btn" onclick="javascript:removeCatFltr();">&times;</span></li>
                            @endif
                            @if($city_zip != '')
                            <li id="city-flt">{{$city_zip}} <span class="close-btn" onclick="javascript:removeLocFltr();">&times;</span></li>
                            @endif
                        @endif
                    </ul>
                    @if($category_name != '' || $city_zip != '')
                    <button onclick="javascript:clearAllFilter();" class="clear-btn clear-all-fltr-btn">Clear All</button>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="search-listing">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 hidden-xs">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        All Category <span class="pull-right"><b><i class="more-less fa fa-angle-up" style="font-weight:bold;"></i></b></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    @if(count($categories) > 0)
                                    <ul class="v-list">
                                        @foreach ($categories as $category)
                                        <li style="display: block;"><a href="javascript:void(0);" onclick="javascript:SearchCatByLink('{{$category->name}}');">{{$category->name}}</a></li>
                                        @endforeach
                                    </ul>
                                    @endif
                                    <button id="next" style="display: none;"><i class="fa fa-plus" aria-hidden="true"></i> Show More</button>
                                </div>
                            </div>
                        </div>
                        <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" value="{{$city_zip}}" class="search-query form-control" placeholder="LOCATION" name="city_zip_side" id="city_state_country_side" autocomplete="off">
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" name="side-location-search" onclick="javascript:callAjxGetRecordSet();" type="button">
                                        <span class="fa fa-search"></span>
                                    </button>
                                </span>
                                <div id="locationSideList">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        {{-- Result Row --}}
                        <div class="results" id="event-results" data-next-page="@if($events->nextPageUrl() != null){{ $events->nextPageUrl().'&category='.urlencode($category_name).'&city='.urlencode($city_zip).'&location_type='.$location_type.'&location_id='.$location_id }}@endif">
                        @include('pages.user.ajax.show-events')
                        </div>
                        <div id='loading-results' style="display: none; text-align: center;"><img src="{{asset('img/loader-2.gif')}}" width="64" style="margin-top: 10px;"></div>
                        @if($allowSendMessage == true)
                            <!-- Modal -->
                            <div id="req-msg" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title" style="font-family: 'Merienda One', cursive!important;">MESSAGE EVENT VENDOR <br><span id="message-header" style="color: #ed38a0!important;"></span></h3>
                                            <hr>
                                            <div class="alert alert-danger show-message-request-error" style="display: none;"></div>
                                            <div class="alert alert-success show-message-request-success" style="display: none;"></div>
                                            <form name="submit-message-request" id="submit-message-request" action="{{ route('handle-event-message-request') }}" method="post">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-6">
                                                        <div class="form-group">
                                                            <label>First Name *</label>
                                                            <input type="text" class="form-control" placeholder="First Name" id="first-name-msg" name="first_name">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-6">
                                                        <div class="form-group">
                                                            <label>Last Name *</label>
                                                            <input type="text" class="form-control" placeholder="Last Name" id="last-name-msg" name="last_name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-6">
                                                        <div class="form-group">
                                                            <label>Phone Number</label>
                                                            <input type="text" class="form-control" placeholder="Phone Number" id="mobile-number-msg" name="mobile_number">
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(Auth::check() == false)
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-6">
                                                        <div class="form-group">
                                                            <label>Email *</label>
                                                            <input type="email" id="email-msg" name="email" class="form-control" placeholder="Email">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-6">
                                                        <div class="form-group">
                                                            <label>Password *</label>
                                                            <input type="password" id="password-msg" name="password" class="form-control" placeholder="Password">
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="form-group">
                                                    <label class="lb">Preferred Contact Method *</label>
                                                    <br>
                                                    <label class="checkbox-inline"><input type="checkbox" id="check_email_contact" name="check_email_contact" value="1" checked="checked"><span class="checkmark email-chk"></span> Email</label>
                                                    <label class="checkbox-inline"><input type="checkbox" id="check_phone_contact" name="check_phone_contact" value="1"><span class="checkmark phone-chk"></span> Phone</label>
                                                    <label class="checkbox-inline"><input type="checkbox" id="check_message_contact" name="check_message_contact" value="1"><span class="checkmark message-chk"></span> Text Message</label>
                                                </div>
                                                <div class="form-group">
                                                    <label id="message-event-vendor">Message:</label>
                                                    <textarea id="message-event-vendor" name="message" class="form-control" style="height: 100px;"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-default" name="submit-btn-price-request">SUBMIT</button>
                                                <div class="row">
                                                    <div class="col-sm-12" style="margin-left: 25px; margin-top: 10px;">
                                                        <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none;">
                                                    </div>
                                                </div>
                                                <input type="hidden" name="vendor_id" value="" />
                                                <input type="hidden" name="event_id" value="" />
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<!--Bootstrap JS-->
<script type='text/javascript'>
function SearchBySpec(type, id)
{
    if(type != '' && id != '') {
        $("input[name='location_type']").val(type);
        $("input[name='location_id']").val(id);
    }
}
function SearchCatByLink(category) 
{
    $("input[name='category_name']").val(category);
    callAjxGetRecordSet();
}
var ajaxResultGetRequest = null;
var currentCategoryRequest = null;
function callAjxGetRecordSet() {
    var form = $("#search-event-frm");
    var url = form.attr('action');
    // hide mobile filter modals
    $('#changeCatsModal').modal('hide');
    $('#searchLocfilter').modal('hide');
    var category_name = $("input[name='category_name']").val();
    if($('#city_state_country').hasClass('active')) {
        $('#city_state_country_side').val($('#city_state_country').val())
        $('#city_zip_mobile_modal').val($('#city_state_country').val())
        $('#city_zip_mobile_filter').val($('#city_state_country').val())
    }
    if($('#city_state_country_side').hasClass('active')) {
        $('#city_state_country').val($('#city_state_country_side').val())
        $('#city_zip_mobile_modal').val($('#city_state_country_side').val())
        $('#city_zip_mobile_filter').val($('#city_state_country_side').val())
    }
    if($('#city_zip_mobile_modal').hasClass('active')) {
        $('#city_state_country').val($('#city_zip_mobile_modal').val())
        $('#city_state_country_side').val($('#city_zip_mobile_modal').val())
        $('#city_zip_mobile_filter').val($('#city_zip_mobile_modal').val())
    }
    if($('#city_zip_mobile_filter').hasClass('active')) {
        $('#city_state_country').val($('#city_zip_mobile_filter').val())
        $('#city_state_country_side').val($('#city_zip_mobile_filter').val())
        $('#city_zip_mobile_modal').val($('#city_zip_mobile_filter').val())
    }
    var city_zip = $("input[name='city_zip']").val();
    if(city_zip == '') {
        $("input[name='location_type']").val('');
        $("input[name='location_id']").val('');
    }
    var location_type = $("input[name='location_type']").val();
    var location_id = $("input[name='location_id']").val();
    var filtr_txt = '';
    
    // remove class from location
    $('#city_state_country').removeClass('active');
    $('#city_state_country_side').removeClass('active');
    $('#city_zip_mobile_modal').removeClass('active');
    $('#city_zip_mobile_filter').removeClass('active');
    ajaxResultGetRequest = $.ajax({
        url: url,
        data: { category: category_name, city: city_zip, location_type: location_type, location_id: location_id },
        method: 'GET',
        beforeSend: function () {
            // show loading image and disable the submit button
            $("form#search-event-frm input[type='submit']").attr('disabled','disabled');
            $("#loader-search").show();
        },
        success: function(result) {
            // show results
            $('.results').html(result.events);
            $('#event-results').data('next-page', result.next_page);
            $('#show-total-records').html(result.total_records+' events');
            $('#show-total-records-big').html(result.total_records+' records:');
            if(result.category_name == '') {
                $('#page-category-title').html(result.metafields.heading);
                $("#mobile-cat-name").html(result.metafields.heading);
            } else {
                $('#page-category-title').html(result.category_name);
                $("#mobile-cat-name").html(result.category_name);
            }
            $('#page-category-description').html(result.category_description);
            $('#mobile-cat-description').html(result.category_description);
            // update meta tags
            $('title').html(result.metafields.title);
            $('meta[name="description"]').attr('content', result.metafields.description);
            $('meta[name="keyword"]').attr('content', result.metafields.keyword);
            // update the main url for category slug
            if(result.category_slug != '') {
                var url=window.location.href;
                if(url.indexOf('?') != -1) {
                    var arr=url.split('?')[1];
                    if(arr !== undefined) {
                        var urlToReplce = result.category_slug+'?'+arr;
                    } else {
                        var urlToReplce = result.category_slug;
                    }
                } else {
                    if(url.indexOf('events/') != -1) {
                        var urlToReplce = result.category_slug;
                    } else {
                        var urlToReplce = 'events/'+result.category_slug;
                    }
                    
                }
                window.history.pushState("","", urlToReplce);
            } else {
                var url=window.location.href;
                if(url.indexOf('?') != -1) {
                    var arr=url.split('?')[1];
                    if(arr !== undefined) {
                        var urlToReplce = '?'+arr;
                    } else {
                        var urlToReplce = '';
                    }
                } else {
                    var urlToReplce = 'all';
                }
                window.history.pushState("","", urlToReplce);
            }

            // set the filter for search
            if(result.category_name != '') {
                filtr_txt += '<li id="category-flt">'+$("input[name='category_name']").val()+' <span class="close-btn" onclick="javascript:removeCatFltr();">&times;</span></li>';
            }
            if(result.location_name != '') {
                filtr_txt += '<li id="city-flt">'+$("input[name='city_zip']").val()+' <span class="close-btn" onclick="javascript:removeLocFltr();">&times;</span></li>';
            }
            $("#myDIV").html(filtr_txt);
            document.getElementById("myDIV").style.display = "inline-flex";
            if($('.clear-all-fltr-btn').length == 0 ) {
                if(result.category_name != '' || result.location_name != '') {
                    $('<button onclick="javascript:clearAllFilter();" class="clear-btn clear-all-fltr-btn">Clear All</button>').insertAfter("#myDIV");
                }
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
            $(".show-message-request-error").html(errors.error);
        },
        complete: function () {
            // hide loading image and enable the submit button
            $("#loader-search").hide();
            $("form#search-event-frm input[type='submit']").removeAttr('disabled');
            ajaxResultGetRequest = null;
            $('html, body').animate({scrollTop: 0 }, 'slow');
        }
    });
}
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-angle-down fa-angle-up');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
$(document).ready(function(){

        var list = $(".v-list li");
        var numToShow = 10;
        var button = $("#next");
        var numInList = list.length;
        list.hide();
        if (numInList > numToShow) {
            button.show();
        }
        list.slice(0, numToShow).show();

        button.click(function(){
            var showing = list.filter(':visible').length;
            list.slice(showing - 1, showing + numToShow).fadeIn();
            var nowShowing = list.filter(':visible').length;
            if (nowShowing >= numInList) {
            button.hide();
            }
        });
});
var email_chkbox = $("#check_email_contact");
var phone_chkbox = $("#check_phone_contact");
var message_chkbox = $("#check_message_contact");
$(document).ready(function() {
    var processing = false;
    if(processing == false) {
        $(window).scroll(fetchResultSet);
    }
    function fetchResultSet() {
        var page = $('#event-results').data('next-page');
        if(page != '') {
            clearTimeout( $.data( this, "scrollCheck" ) );
            $.data( this, "scrollCheck", setTimeout(function() {
                var scroll_position_for_posts_load = $(window).height() + $(window).scrollTop() + 100;
                if(scroll_position_for_posts_load >= $(document).height()) {
                    if(processing == false) {
                        processing = true;
                        if(processing == true) {
                            $("#loading-results").show();
                        }
                        $.get(page, function(data){
                            $('.results').append(data.events);
                            $('#event-results').data('next-page', data.next_page);
                            processing = false;
                            $("#loading-results").hide();
                        });
                    }
                }
            }, 350))
        }
    }
    email_chkbox.on('change', function() {
        if(email_chkbox.is(':checked') == true) {
            phone_chkbox.prop("checked", false);
            message_chkbox.prop("checked", false);
        }
    });
    phone_chkbox.on('change', function() {
        if(phone_chkbox.is(':checked') == true) {
            email_chkbox.prop("checked", false);
            message_chkbox.prop("checked", false);
        }
    });
    message_chkbox.on('change', function() {
        if(message_chkbox.is(':checked') == true) {
            email_chkbox.prop("checked", false);
            phone_chkbox.prop("checked", false);
        }
    });
    // submit request message
    $('form#submit-message-request').on('submit', function(e) {
        e.preventDefault();
        $(".show-message-request-error, .show-message-request-success").hide();
        $(".show-message-request-error").html("");
        $(".show-message-request-success").html("");
        var submit_frm = true;
        if(email_chkbox.is(':checked') == false && phone_chkbox.is(':checked') == false && message_chkbox.is(':checked') == false) {
            submit_frm = false;
            $(".show-price-request-error").show();
            $(".show-price-request-error").html("Please select atleast one Preferred Contact Method.");
        } else {
            submit_frm = true;
            $(".show-price-request-error").hide();
            $(".show-price-request-error").html("");
        }
        if(phone_chkbox.is(':checked') == true) {
            if($('form#submit-new-price-request input[name="mobile_number"]').val() == '') {
                    submit_frm = false;
                    $(".show-price-request-error").show();
                    $(".show-price-request-error").html("Please enter Phone Number.");
            } else {
                    submit_frm = true;
                    $(".show-price-request-error").hide();
                    $(".show-price-request-error").html("");
            }
        }
        if(submit_frm == true) {
            var form = $("#submit-message-request");
            var url = form.attr('action');
            var data = new FormData(form[0]);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                data: data,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend: function () {
                    // show loading image and disable the submit button
                    $("button[name='submit-btn-price-request']").attr('disabled','disabled');
                    $("#loader").show();
                },
                success: function(result) {
                    // show success message
                    if(result.status == 0) {
                        $(".show-message-request-error").html(result.message);
                        $(".show-message-request-error").show();
                    } else {
                        $(".show-message-request-success").html(result.message);
                        $(".show-message-request-success").show();
                        // reset the form
                        resetPriceRequestFrm();
                        setTimeout(function() {$('#req-msg').modal('hide');window.location.reload(true);}, 2000);
                    }
                },
                error: function(xhr, status, error){
                    var errors = xhr.responseJSON;
                    $(".show-message-request-error").html(errors.error);
                },
                complete: function () {
                    // hide loading image and enable the submit button
                    $("#loader").hide();
                    $("button[name='submit-btn-price-request']").removeAttr('disabled');
                }
            });
            return false;
        } else {
            return false;
        }
    });
    // handle search event form
    $('form#search-event-frm').on('submit', function(e) {
        e.preventDefault();
        callAjxGetRecordSet();
    });

});
function resetPriceRequestFrm() {
    var first_name = $('form#submit-new-price-request input[name="first_name"]');
    var last_name = $('form#submit-new-price-request input[name="last_name"]');
    var mobile_number = $('form#submit-new-price-request input[name="mobile_number"]');
    var email = $('form#submit-new-price-request input[name="email"]');
    var password = $('form#submit-new-price-request input[name="password"]');
    var message = $('form#submit-message-request textarea[name="message"]');

    email_chkbox.prop("checked", true);
    phone_chkbox.prop("checked", false);
    phone_chkbox.prop("checked", false);

    first_name.val('')
    last_name.val('')
    mobile_number.val('')
    email.val('')
    password.val('')
    message.val('');
}

$(function() {
    
    $("#category_search").on('keyup', function() {
        var value = $("#category_search").val();
        currentCategoryRequest = $.ajax({
            type:"get",
            dataType: 'html',
            url: route('get-event-search-auto-suggestions'),
            data :{
                'key' : value               
            },
            beforeSend : function()    {           
                if(currentCategoryRequest != null) {
                    currentCategoryRequest.abort();
                }
            },
            success: function(response) {
                if(ajaxResultGetRequest == null) {
                    $('#categoryList').fadeIn();  
                    $('#categoryList').html(response);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // Error handling
            },
            complete: function () {
                currentCategoryRequest = null;
            }
        });
    });

    var currentLocationRequest = null;
    $("#city_state_country").on('keyup', function() {
        $(this).addClass('active');
        var value = $(this).val();
        currentLocationRequest = $.ajax({
            type:"get",
            dataType: 'html',
            url: route('get-location-auto-suggestions'),
            data :{
                'key' : value               
            },
            beforeSend : function()    {           
                if(currentLocationRequest != null) {
                    currentLocationRequest.abort();
                }
            },
            success: function(response) {
                if(ajaxResultGetRequest == null) {
                    $('#locationList').fadeIn();  
                    $('#locationList').html(response);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // Error handling
            }
        });
    });

    var currentLocationSideRequest = null;
    $("#city_state_country_side").on('keyup', function() {
        $(this).addClass('active');
        var value = $(this).val();
        currentLocationSideRequest = $.ajax({
            type:"get",
            dataType: 'html',
            url: route('get-location-auto-suggestions'),
            data :{
                'key' : value               
            },
            beforeSend : function() {
                if(currentLocationSideRequest != null) {
                    currentLocationSideRequest.abort();
                }
            },
            success: function(response) {
                if(ajaxResultGetRequest == null) {
                    $('#locationSideList').fadeIn();  
                    $('#locationSideList').html(response);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // Error handling
            }
        });
    });

    var currentLocationMobModRequest = null;
    $("#city_zip_mobile_modal").on('keyup', function() {
        $(this).addClass('active');
        var value = $(this).val();
        currentLocationMobModRequest = $.ajax({
            type:"get",
            dataType: 'html',
            url: route('get-location-auto-suggestions'),
            data :{
                'key' : value               
            },
            beforeSend : function() {
                if(currentLocationMobModRequest != null) {
                    currentLocationMobModRequest.abort();
                }
            },
            success: function(response) {
                if(ajaxResultGetRequest == null) {
                    $('#locationMobileModalList').fadeIn();  
                    $('#locationMobileModalList').html(response);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // Error handling
            }
        });
    });

    var currentLocationMobFltRequest = null;
    $("#city_zip_mobile_filter").on('keyup', function() {
        $(this).addClass('active');
        var value = $(this).val();
        currentLocationMobFltRequest = $.ajax({
            type:"get",
            dataType: 'html',
            url: route('get-location-auto-suggestions'),
            data :{
                'key' : value               
            },
            beforeSend : function() {
                if(currentLocationMobFltRequest != null) {
                    currentLocationMobFltRequest.abort();
                }
            },
            success: function(response) {
                if(ajaxResultGetRequest == null) {
                    $('#locationMobileFilterList').fadeIn();  
                    $('#locationMobileFilterList').html(response);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                // Error handling
            }
        });
    });
});
$(document).on('click', 'ul.category-event-autosuggestion li', function(){
    if($(this).hasClass('category')) {
        $('#category_search').val($(this).text());
        callAjxGetRecordSet();
        $('#categoryList').fadeOut();
    }
});
$(document).on("focusout","#category_search",function(){
    if (!catListItems.hasClass("active")) {
        $('#categoryList').fadeOut();
        callAjxGetRecordSet();
    }
});

$(document).on("focusout","#city_state_country",function(){
    $('#locationList').fadeOut();
    callAjxGetRecordSet();
});
$(document).on("focusout","#city_state_country_side",function(){
    $('#locationSideList').fadeOut();
    callAjxGetRecordSet();
});
$(document).on("focusout","#city_zip_mobile_modal",function(){
    $('#locationMobileModalList').fadeOut();
    callAjxGetRecordSet();
});
$(document).on("focusout","#city_zip_mobile_filter",function(){
    $('#locationMobileFilterList').fadeOut();
    callAjxGetRecordSet();
});

var catListItems = $('#categoryList')
.hide()
.bind({
    "mouseenter" : function() {
        $(this).addClass("active");   
    },
    "mouseleave" : function() {
        $(this).removeClass("active");
    }
});

$(document).on('click', 'ul.location-vendor-autosuggestion li', function(){
    $('#city_state_country').val($(this).text());
    $('#city_state_country_side').val($(this).text());
    $('#city_zip_mobile_modal').val($(this).text());
    callAjxGetRecordSet();
    $('#locationList').fadeOut();
    $('#locationSideList').fadeOut();
    $('#locationMobileModalList').fadeOut();
    $('#locationMobileFilterList').fadeOut();
});

var categoryOnClickSearch = null;
$(document).on("click","#category_search",function(){
    var value = $("#category_search").val();
    categoryOnClickSearch = $.ajax({
        type:"get",
        dataType: 'html',
        url: route('get-event-search-auto-suggestions'),
        data :{
            'key' : value               
        },
        success: function(response) {
            if(ajaxResultGetRequest == null) {
                $('#categoryList').fadeIn();  
                $('#categoryList').html(response);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // Error handling
        },
        complete: function() {
            categoryOnClickSearch = null
        }
    });
});

function showMessageModal(vendor_id, event_id, message_header, first_name, last_name, email, mobile_number) {
    $("input[name='vendor_id']").val(vendor_id);
    $("input[name='event_id']").val(event_id);
    if(message_header != '') {
        $("#message-header").html(message_header);
    } else {
        $("#message-header").html('');
    }
    if(first_name != '') {
        $("#first-name-msg").val(first_name);
    } else {
        $("#first-name-msg").val('');
    }
    if(last_name != '') {
        $("#last-name-msg").val(last_name);
    } else {
        $("#last-name-msg").val('');
    }
    if(email != '') {
        $("#email-msg").val(email);
    } else {
        $("#email-msg").val('');
    }
    if(mobile_number != '') {
        $("#mobile-number-msg").val(mobile_number);
    } else {
        $("#mobile-number-msg").val('');
    }
    $("#req-msg").modal({show:true});
}
function clearAllFilter() {
    if($('.clear-all-fltr-btn').length > 0) {
        $('.clear-all-fltr-btn').remove();
    }
    document.getElementById("myDIV").style.display = "none";
    $("input[name='category_name']").val('');
    $("input[name='city_zip']").val('');
    $('#city_state_country_side').val('');
    callAjxGetRecordSet();
}
function removeCatFltr()
{
    $("input[name='category_name']").val('');
    $("#category-flt").remove();
    if($("input[name='category_name']").val() == '' && $("input[name='city_zip']").val() == '') {
        if($('.clear-all-fltr-btn').length > 0) {
            $('.clear-all-fltr-btn').remove();
        }
    }
    callAjxGetRecordSet();
}
function removeLocFltr()
{
    $("input[name='city_zip']").val('');
    $('#city_state_country_side').val('');
    $("#city-flt").remove();
    if($("input[name='category_name']").val() == '' && $("input[name='city_zip']").val() == '') {
        if($('.clear-all-fltr-btn').length > 0) {
            $('.clear-all-fltr-btn').remove();
        }
    }
    callAjxGetRecordSet();
}
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection