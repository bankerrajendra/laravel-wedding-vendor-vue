@extends('layouts.app')

@section('template_title')
    Edit Profile
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
    .alert-dangers {
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
    }

@endsection

@section('content')
    <div class="edit-profile">
        <div class="container">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-dangers">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">

                @include('partials.my-profile-navigation')
                <div class="col-sm-8 panel-body">
                    <form action="{{ route('updateUserFrontend') }}" method="post" id="registrationForm">

                        {{ csrf_field() }}

                        <div class="panel-group" id="accordion">
                            <!--Personal Profile-->
                            <div class="panel panel-success">
                                <div class="panel-heading mobile-panel">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="basicinfo">Personal Profile  <i class="more-less glyphicon glyphicon-minus"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Name</label>
                                            </div>
                                            <input type="hidden" class="form-control" name="name" value="{{$userProfile->name}}">
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{$errors->any()?old('first_name'):$userProfile->first_name}}">
                                                    {{--@if ($errors->has('first_name'))--}}
                                                        {{--<span class="invalid-feedback" style="color: #dc3545;">--}}
                                                            {{--<strong>{{ $errors->first('first_name') }}</strong>--}}
                                                        {{--</span>--}}
                                                    {{--@endif--}}
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="last_name" value="{{$errors->any()?old('last_name'):$userProfile->last_name}}">
                                                    {{--@if ($errors->has('last_name'))--}}
                                                        {{--<span class="invalid-feedback" style="color: #dc3545;">--}}
                                                            {{--<strong>{{ $errors->first('last_name') }}</strong>--}}
                                                        {{--</span>--}}
                                                    {{--@endif--}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Email</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" disabled value="{{$userProfile->email}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Phone Number</label>
                                            </div>

                                            <div class="col-sm-3 col-xs-5">
                                                <div class="form-group">
                                                    <select name="phone_country_code" id="phone_country_code" class="selectpicker form-control  {{ $errors->has('phone_country_code') ? ' is-invalid' : '' }}" data-show-subtext="true" data-live-search="true">

                                                        {{--<option value="">Select</option>--}}
                                                        @foreach($countries as $country)
                                                                <option value="{{$country->id}}" id="{{$country->id}}" {{(($errors->any()?old('phone_country_code'):@$userProfile->users_information['country_code'])==$country->id)?'selected':''}}>+{{$country->phonecode}}  ({{$country->name}})</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block phone_country_code_err invalid-feedback"  style="font-weight:normal;color:#a94442;display:none">
                                                        Phone country code field is required
                                                    </span>


                                                </div>
                                            </div>
                                            <div class="col-sm-6  col-xs-7">
                                                <div class="form-group">
                                                    <input type="text" name="mobile_number" value="{{ $errors->any()?old('mobile_number'):$userProfile->users_information['mobile_number'] }}" class="form-control" id="mobile_number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Date of Birth</label>
                                            </div>
                                            @php
                                                $date_of_birth = explode('-',$userProfile->date_of_birth);
                                                $dob_year = $date_of_birth[0];
                                                $dob_month = $date_of_birth[1];
                                                $dob_day = $date_of_birth[2];
                                            @endphp
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="form-group">
                                                    <select name="day" id="day" class="form-control">
                                                        {{--<option value="">DD</option>--}}

                                                        @for ($day = 1; $day <= 31; $day++)
                                                            <option value="{{ $day }}" @if(($errors->any()?old('day'):$dob_day) == $day) selected="selected" @endif>{{ $day }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="form-group">
                                                    <select name="month" id="month" class="form-control">
                                                        {{--<option value="">MM</option>--}}
                                                        @for ($month = 1; $month <= 12; $month++)
                                                            <option value="{{ $month }}" @if(($errors->any()?old('month'):$dob_month) == $month) selected="selected" @endif>{{ $month }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-xs-4">
                                                <div class="form-group">
                                                    <select name="year" id="year" class="form-control">
                                                        {{--<option value="">YYYY</option>--}}
                                                        @for ($year =  (now()->year - 70); $year <= (now()->year - 18); $year++)
                                                            <option value="{{ $year }}" @if(($errors->any()?old('year'):$dob_year) == $year) selected="selected" @endif>{{ $year }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row f_padding">
                                            <div class="col-sm-3">
                                                <label>Gender</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="hidden" value="{{ $userProfile->gender }}" name="gender">
                                                    @foreach(config('constants.gender') as $key=>$val)
                                                        <label class="radio-inline"><input type="radio" disabled    {{ $userProfile->gender==$key? "checked":"" }}>{{ $val }}</label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Marital Status</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="marital_status" id="marital_status" class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach($marital_status as $key=>$val)
                                                            <option value="{{ $key }}" {{ ($errors->any()?old('marital_status'):$userProfile->users_information['marital_status'])==$key? "selected":"" }}>{{ $val }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Profile Created By </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="profile_created_by" class="form-control">
                                                        <option value="">Please Select</option>
                                                        @foreach(config('constants.profile_created_by') as $key=>$profle_created)
                                                            <option value="{{ $key }}" {{ ($errors->any()?old('profile_created_by'):$userProfile->users_information['profile_created_by']) == $key ? "selected":"" }}>{{ $profle_created }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row f_padding">
                                            <div class="col-sm-3">
                                                <label>Body Type</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    @foreach(config('constants.body_type') as $key=>$body)

                                                        <label class="radio-inline"><input type="radio" name="body_type" id="body_type" value="{{ $key }}" {{ ($errors->any()?old('body_type'):$userProfile->users_information['body_type']) == $body ? "checked":"" }}>{{ $body }}</label>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Body Weight</label>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <input type="number" min="0" name="weight" id="weight" value="{{ $errors->any()?old('weight'):@$userProfile->users_information['weight'] }}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <select name="weight_measure" id="weight_measure" class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach(config('constants.weight_measure') as $rId=>$name)
                                                            <option value="{{ $rId }}" {{ ($errors->any()?old('weight_measure'):$userProfile->users_information['weight_measure']) == $rId?'selected':'' }}>{{$name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Eye Color </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control" name="eye_color">
                                                        <option value="">Select</option>
                                                        @foreach(config('constants.eye_color') as $key=>$color)
                                                            <option value="{{ $key }}" {{ ($errors->any()?old('eye_color'):$userProfile->users_information['eye_color']) == $key ? "selected":"" }}>{{ $color }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Height </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control" name="height">
                                                        <option value="">Select</option>
                                                        @foreach($heights as $key=>$height)
                                                            <option value="{{ $key }}" {{($errors->any()?old('height'):$userProfile->users_information['height']) == $key ? "selected":"" }}>{{ $height }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Complexion </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control" name="complexion">
                                                        <option value="">Select</option>
                                                        @foreach(config('constants.skin_tone') as $key=>$cpl)
                                                            <option value="{{ $key }}" {{($errors->any()?old('complexion'):$userProfile->users_information['complexion']) == $key ? "selected":"" }}>{{ $cpl }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Personal Profile-->

                            <!--Family Background-->
                            <div class="panel panel-warning">
                                <div class="panel-heading mobile-panel">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="familybackgroud">Family Background <i class="more-less glyphicon glyphicon-plus"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Religion </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="religion" id="religion" class="form-control religionMap" dependent="sect" disabled="disabled">
                                                        <option value="">Select </option>
                                                        @foreach($religions as $name=>$rId)
                                                                <option value="{{$rId}}" {{ ( $rId == @$userProfile->users_information['religion_id']) ? 'selected' : '' }}>{{ ucfirst($name) }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                        <?php
                                        $arr=array();
                                            if( $userProfile->users_information['sub_cast_id'] == "45" || $userProfile->users_information['sect_id'] == "8"){
                                                $otherInfo = json_decode(@getOtherReligionInfo($userProfile->id));
                                                if(count($otherInfo)) {
                                                    for($i=0; $i<count($otherInfo);$i++) {
                                                    //echo $otherInfo[$i]->others_info_source;
                                                        $arr[$otherInfo[$i]->others_info_source]=$otherInfo[$i]->name;
                                                    }
                                                }
                                            }
                                        ?>


                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Sect/Math-hab</label>
                                            </div>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="sect" id="sect"  dependent="sub_cast" data-val="{{@$arr['community_or_sect']}}" class="form-control religionMap implementOthers">
                                                        {{--<option value="">Select </option>--}}
                                                        @foreach($communities as $comunity)
                                                                <option value="{{$comunity->id}}" {{ ( $comunity->id == ($errors->any()?old('sect'):@$userProfile->users_information['sect_id'])) ? 'selected' : '' }}>{{ ucfirst($comunity->name) }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                {{--if($arr['sub_cast'])--}}
                                                {{--$arr['community_or_sect']--}}
                                                {{--<div class="sct_sect form-group others"><input type="text" name="other_sect" class="form-control" placeholder="Please specify Sect"></div>--}}
                                                @if($userProfile->users_information['sect_id'] == "8" ) <?php //&& isset($arr['community_or_sect']) && $arr['community_or_sect']!="" ?>
                                                    <div class="sct_sect form-group others"><input type="text" name="other_sect" value="{{($errors->any()?old('other_sect'):@$arr['community_or_sect'])}}" class="form-control" placeholder="Please specify Sect"></div>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Sub Caste</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="sub_cast" id="sub_cast" class="form-control implementOthers selectpicker {{ $errors->has('sub_cast') ? ' is-invalid' : '' }}" data-show-subtext="true" data-live-search="true">
                                                        {{--<option value="">Select Community</option>--}}
                                                        @foreach($subcastes as $subcast)
                                                            <option value="{{$subcast->id}}" {{ ( $subcast->id == ($errors->any()?old('sub_cast'):@$userProfile->users_information['sub_cast_id'])) ? 'selected' : '' }}>{{ ucfirst($subcast->name) }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                @if($userProfile->users_information['sub_cast_id'] == "45") <?php // && isset($arr['sub_cast']) && $arr['sub_cast']!="" ?>
                                                    <div class="sct_sub_cast form-group others"><input type="text" name="other_sub_cast" value="{{($errors->any()?old('other_sub_cast'):@$arr['sub_cast'])}}" class="form-control" placeholder="Please specify Sub-caste"></div>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Religious History?</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="is_born" class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach($born_reverted as $key=>$br)
                                                            <option value="{{ $key }}" {{($errors->any()?old('is_born'):$userProfile->users_information['is_born']) == $key ? "selected":"" }}>{{ $br }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Language </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="language_id" class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach(getLanguages() as $lang)
                                                            <option value="{{ $lang->id }}" {{($errors->any()?old('language_id'):$userProfile->users_information['language_id']) == $lang->id ? "selected":"" }}>{{ $lang->language }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Horoscope</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="horoscope" class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach(config('constants.horoscope') as $key=>$hrs)
                                                            <option value="{{ $key }}" {{($errors->any()?old('horoscope'):$userProfile->users_information['horoscope']) == $key ? "selected":"" }}>{{ $hrs }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Native Place</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" name="native_place" value="{{($errors->any()?old('native_place'):@$userProfile->users_information['native_place']) }}" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Family Background-->

                            <!--Family Details-->
                            <div class="panel panel-success">
                                <div class="panel-heading mobile-panel">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapse3">Family Details <i class="more-less glyphicon glyphicon-plus"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Father Occupation</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control" name="father_occupation_id">
                                                        <option value="">Select</option>
                                                        @foreach(getProfession() as $prf)
                                                            <option value="{{ $prf->id }}" {{ ($errors->any()?old('father_occupation_id'):$userProfile->users_information['father_occupation_id']) == $prf->id ? "selected":"" }}>{{ $prf->profession }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Mother Occupation</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control" name="mother_occupation_id">
                                                        <option value="">Select</option>
                                                        @foreach(getProfession() as $mprf)
                                                            <option value="{{ $mprf->id }}" {{ ($errors->any()?old('mother_occupation_id'):$userProfile->users_information['mother_occupation_id']) == $mprf->id ? "selected":"" }}>{{ $mprf->profession }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>No of Brothers</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control" name="number_of_brother">
                                                        <option value="">Select</option>
                                                        @for($i =1; $i<20; $i++)
                                                            <option value="{{ $i }}" {{ ($errors->any()?old('number_of_brother'):$userProfile->users_information['number_of_sister']) == $i ? "selected":"" }}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>No of Sisters</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control" name="number_of_sister">
                                                        <option value="">Select</option>
                                                        @for($i =1; $i<20; $i++)
                                                            <option value="{{ $i }}" {{ ($errors->any()?old('number_of_sister'):$userProfile->users_information['number_of_brother']) == $i ? "selected":"" }}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Family Location</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" name="family_location" value="{{ ($errors->any()?old('family_location'):$userProfile->users_information['family_location']) }}" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row f_padding">
                                            <div class="col-sm-3">
                                                <label>Family Type</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <label class="radio-inline"><input type="radio"  name="family_type" value="joint" {{ ($errors->any()?old('family_type'):@$userProfile->users_information['family_type']) == "joint"?'checked':'' }}>Joint</label>
                                                    <label class="radio-inline"><input type="radio" name="family_type" value="nuclear" {{ ($errors->any()?old('family_type'):@$userProfile->users_information['family_type']) == "nuclear"?'checked':'' }}>Nuclear</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Family Details-->

                            <!--Education & Career-->
                            <div class="panel panel-warning">
                                <div class="panel-heading mobile-panel">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="education">Education & Career <i class="more-less glyphicon glyphicon-plus"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Education</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="education" id="education" class="form-control">
                                                        {{--<option value="">Select</option>--}}
                                                        @foreach($educationOptions as $option)
                                                            <option value="{{$option->id}}" {{ ($errors->any()?old('education'):$userProfile->users_information['education_id']) == $option->id ? "selected":"" }}>{{$option->education}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Employment</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="profession_id" class="form-control">
                                                        {{--<option value="">Select</option>--}}
                                                        @foreach(getProfession() as $prf)
                                                            <option value="{{ $prf->id }}" {{ ($errors->any()?old('profession_id'):$userProfile->users_information['profession_id']) == $prf->id ? "selected":"" }}>{{ $prf->profession }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Annual Income</label>
                                            </div>
                                            <div class="col-sm-3 col-xs-5">
                                                <div class="form-group">
                                                    <select name="currency" class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach(@getCurrency() as $rId=>$name)
                                                            <option value="{{ $name }}" {{ ( $name == ($errors->any()?old('currency'):@$userProfile->users_information['currency'])) ? 'selected' : '' }}> {{ $name }} </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-7">
                                                <div class="form-group">
                                                    <select name="annual_income"class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach(config('constants.annual_income') as $key=>$income)
                                                            <option value="{{ $key }}" {{($errors->any()?old('annual_income'):$userProfile->users_information['annual_income']) == $key ? "selected":"" }}>{{ $income }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>



                                    </div>
                                </div>
                            </div>
                            <!--End Education & Career-->

                            <!--Location-->
                            <div class="panel panel-success">
                                <div class="panel-heading mobile-panel">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="address">Location <i class="more-less glyphicon glyphicon-plus"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Country</label>
                                            </div>

                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select name="country_code" class="form-control selectpicker dynamic_differ" dependent="state" id="country" data-show-subtext="true" data-live-search="true" >
                                                        <option value="">Select</option>
                                                        @foreach($countries as $ctr)
                                                            <option value="{{ $ctr->id }}" {{($errors->any()?old('country_code'):$userProfile->country_id) == $ctr->id ? "selected":"" }}>{{ $ctr->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block country_code_err invalid-feedback"  style=";font-weight:normal;color:#a94442;display:none">
                                                        Country field is required
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>State</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control selectpicker dynamic_differ" dependent="city" id="state" data-show-subtext="true" data-live-search="true" name="state_id">
                                                        <option value="">Select</option>
                                                        @foreach(getStateByCountry(($errors->any()?old('country_code'):$userProfile->country_id)) as $ct)
                                                            <option value="{{ $ct->id }}" {{ ($errors->any()?old('state_id'):$userProfile->state_id) == $ct->id ? "selected":"" }}>{{ $ct->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block state_code_err invalid-feedback"  style=";font-weight:normal;color:#a94442;display:none">
                                                        State field is required
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>City</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  id="city" name="city_id">
                                                        <option value="">Select</option>
                                                        @foreach(getCityByState(($errors->any()?old('state_id'):$userProfile->state_id)) as $ct)
                                                            <option value="{{ $ct->id }}" {{($errors->any()?old('city_id'):$userProfile->city_id) == $ct->id ? "selected":"" }}>{{ $ct->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block city_code_err invalid-feedback"  style=";font-weight:normal;color:#a94442;display:none">
                                                        City field is required
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Zip/Pin/Area Code </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <input type="text" name="zipcode" value="{{ ($errors->any()?old('zipcode'):$userProfile->users_information['zipcode']) }}" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Location-->

                            <!--Lifestyle-->
                            <div class="panel panel-warning">
                                <div class="panel-heading mobile-panel">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="lifestyle">Lifestyle <i class="more-less glyphicon glyphicon-plus"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row f_padding">
                                            <div class="col-sm-3">
                                                <label>Food Preference?</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">

                                                    @foreach($food as $rId=>$name)
                                                        <label class="radio-inline"><input type="radio" name="food" id="food" value="{{$rId}}"   {{ ( $rId == ($errors->any()?old('food'):@$userProfile->users_information['food'])) ? 'checked' : '' }}>{{$name}}</label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row f_padding">
                                            <div class="col-sm-3">
                                                <label>Drink?</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    @foreach($drink as $rId=>$name)
                                                        <label class="radio-inline"><input type="radio" name="drink" id="drink" value="{{$rId}}"   {{ ( $rId == ($errors->any()?old('drink'):@$userProfile->users_information['drink'])) ? 'checked' : '' }}>{{$name}}</label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row f_padding">
                                            <div class="col-sm-3">
                                                <label>Smoke?</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    @foreach($smoke as $rId=>$name)
                                                        <label class="radio-inline"><input type="radio" name="smoke"  id="smoke" value="{{$rId}}"   {{ ( $rId == ($errors->any()?old('smoke'):@$userProfile->users_information['smoke'])) ? 'checked' : '' }}>{{$name}}</label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Do You Pray? </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <select class="form-control" name="does_pray">
                                                        <option value="">Select Do You Pray</option>
                                                        @foreach($do_you_pray as $rId=>$name)
                                                            <option  value="{{$rId}}" {{ ($errors->any()?old('does_pray'):@$userProfile->users_information['does_pray'])==$rId? "selected":"" }}>{{$name}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Lifestyle-->

                            <!--About Myself-->
                            <div class="panel panel-success">
                                <div class="panel-heading mobile-panel">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" class="aboutme">About Myself <i class="more-less glyphicon glyphicon-plus"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>This section will help you make a positive impression on your potential partner</label>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="about" name="about" style="height: 150px;">{{ ($errors->any()?old('about'):$userProfile->users_information['about']) }}</textarea>
                                                    <small>(Please write atleast 50 characters.)</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End About Myself-->
                            <!--What I'm looking For-->
                            <div class="panel panel-warning">
                                <div class="panel-heading mobile-panel">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" class="lookingfor" >What I'm looking For <i class="more-less glyphicon glyphicon-plus"></i></a>
                                    </h4>
                                </div>
                                <div id="collapse8" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="looking_for" id="looking_for" style="height: 150px;">{{ ($errors->any()?old('looking_for'):$userProfile->users_information['looking_for']) }}</textarea>
                                                    <small>(Please write atleast 50 characters.)</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End About Myself-->
                        </div>
                        <button type="submit" class="btn btn-warning" id="prsave">Save Changes</button>
                        <a href="{{ route('self-profile') }}" class="btn btn-success">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer_scripts')
    <script>
        $(document).ready(function(){
            $("#term_and_conditions-error").css({"color":"rgb(169, 68, 66) !important"});

            $('.dynamic').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    var output;
                    $('#'+dependent).html('');
                    $("#phone-country-code option[selected='selected']").removeAttr('selected');
                    $("#phone-country-code option[id="+value+"]").attr("selected","selected");
                    $.ajax({
                        url:"{{ route('ajax.fetchLocation') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {
                            for(var i=0; i<result.length; i++){
                                output += "<option value="+result[i].id+">"+result[i].value+"</option>";
                            }
                            $('#'+dependent).append(output);
                        }

                    })
                }
            });

            var url = document.location.toString();
            if ( url.match('#') ) {
                if(url.split('#')[1]!="collapse1") {
                    $('.' + url.split('#')[1]).trigger('click');
                }
            }
        });

    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
