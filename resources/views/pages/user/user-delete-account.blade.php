@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="login_p signup">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 text-center">
                    <h3><i>DELETE ACCOUNT</i></h3>
                    <p><b>Note:</b> We do not refund memberships. By permanently deleting your account, you will lose any unused time left on your Platinum / Diamond account.</p>
                </div>
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="tabbable-line">
                        <a href="{{route('deleting-account')}}" class="btn btn-default btn-block">DELETE ACCOUNT</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
@endsection