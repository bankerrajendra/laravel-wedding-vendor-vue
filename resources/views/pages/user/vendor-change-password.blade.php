@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting min_h">
        <div class="container">
            <div class="row">
                @if(!(Auth::user()->is_active()))
                    <div class="text-center">
                        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
                    </div>
                @endif
                <div class="col-sm-4 hidden-xs">
                    @include('partials.vendor-side-menu')
                </div>
                <div class="col-sm-8 update-password">
                    <span class="text-info2"></span>
                    <form name="frm-change-user-password" id="changePasswordForm" method="POST" action="{{ route('handle-vendor-change-password') }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    YOUR ACCOUNT INFORMATION
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input disabled="disabled" type="email" name="email" id="email" class="form-control" value="{{$user->email}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="current_password">Current Password</label>
                                    <input type="password" name="current_password" id="current_password" class="form-control" value="">
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="new_password">New Password</label>
                                    <input type="password" name="new_password" id="new_password" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="new_password_confirmation">Confirm Password</label>
                                    <input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control">
                                </div>
                            </div>
                        </div>
                        <br>
                        <button type="submit" name="password-chng-btn" class="btn btn-default pull-right">SAVE</button>
                        <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none;float: right;margin-right: 10px;margin-top: 5px;">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
$("#changePasswordForm").on('submit', function(e) {

    e.preventDefault();
    var _token = $('input[name="_token"]').val();
    var url = route('handle-vendor-change-password');

    $.ajax({
        url: url,
        method: "POST",
        data: $("#changePasswordForm").serialize(),
        beforeSend: function () {
            // show loading image and disable the submit button
            $("button[name='password-chng-btn']").attr('disabled','disabled');
            $("#loader").show();
        },
        success: function (result) {
            if(typeof(result.error)!="undefined") {
                var el = document.createElement("div");
                el.setAttribute("class","alert alert-danger avatar-alert alert-dismissable");
                el.innerHTML =  result.error;
                setTimeout(function(){
                    el.parentNode.removeChild(el);
                },2000);
                $("#changePasswordForm").closest('.update-password').prepend(el);
            } else if(typeof(result.success)!="undefined") {
                var el = document.createElement("div");
                el.setAttribute("class","alert alert-success avatar-alert alert-dismissable");
                el.innerHTML =  result.success;
                setTimeout(function(){
                    el.parentNode.removeChild(el);
                },2000);
                $("#changePasswordForm").closest('.update-password').prepend(el);
                $('.text-info2').show();
            }
        },
        complete: function () {
            // hide loading image and enable the submit button
            $("#loader").hide();
            $("button[name='password-chng-btn']").removeAttr('disabled');
        }
    });
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection