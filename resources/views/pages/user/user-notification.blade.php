@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 hidden-xs">
                    @include('partials.user-side-menu')
                </div>
                <div class="col-sm-8">
                    @if(session('success'))
						<div class="alert alert-success" role="alert">
							{{session('success')}}
						</div>
					@endif
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
                    <form method="post" name="submit-email-notification" action="{{ route('handle-email-notification') }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    Email Notifications
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="checkbox">
                                    <label><input type="checkbox" value="Y" name="notify_sends_message" @if(isset($user_info) && $user_info->notify_sends_message == "Y")checked="checked"@endif> If someone sends me a message</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" value="Y" name="notify_news_letter" @if(isset($user_info) && $user_info->notify_news_letter == "Y")checked="checked"@endif> Email newsletters</label>
                                </div>	
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-default">SAVE</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
@endsection