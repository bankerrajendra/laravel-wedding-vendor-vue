@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('dist/css/cropper.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('dist/css/main.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('template_title')
    Manage Photos
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection

@section('content')
    <div id="page-content-wrapper">
        <section>
            @include('partials.edit-profile-navigation-mobile')
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-s " style="margin-top: 0px;">
                        <div class="row">
                            @include('partials.user-sidebar-navigation')
                            <div class="col-sm-8 col-sm-offset-4 panel-body menu menu1">
                                @include('partials.edit-profile-navigation')
                                @if(session('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{session('success')}}
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                    <div class="text-center">
                                        <p>GET MORE RESPONSES BY UPLOADING UPTO {{config('constants.image_upload_limit')}} PHOTOS ON YOUR PROFILE.</p>
                                    </div>
                                    <div class="crop-avatar text-center">
                                        <div class="photo-panel">
                                            <h3>Upload Photos</h3>
                                            <div class="avatar-view" data-original-title="" title="">
							<span id="fileselector">
								<label class="btn btn-warning">
									<i class="fa fa-upload" aria-hidden="true"></i> Select Photo
								</label>
							</span>
                                            </div>
                                            <!-- Cropping modal -->
                                            <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
                                                <div class="modal-dialog modal-lg">

                                                    <div class="modal-content">

                                                        <form class="avatar-form" id="imageUploadForm" action="{{ route('ajax-upload-image') }}" enctype="multipart/form-data" method="post">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" id="resetPhoto" value="reset">×</button>
                                                                <h4 class="modal-title" id="avatar-modal-label">Upload Image</h4>
                                                            </div>

                                                            <div class="modal-body">
                                                                <div class="avatar-body">

                                                                    <!-- Upload image and data -->
                                                                    <div class="avatar-upload">
                                                                        <input type="hidden" class="avatar-src" name="avatar_src">
                                                                        <input type="hidden" class="avatar-data" name="avatar_data">
                                                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                                        <label for="avatarInput">Select Photo</label>
                                                                        <input type="file" class="avatar-input" id="avatarInput" name="avatar_file" >
                                                                    </div>

                                                                    <!-- Crop and preview -->
                                                                    <div class="row">
                                                                        <div class="col-md-9">
                                                                            <div class="avatar-wrapper"></div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="avatar-preview preview-lg">
                                                                                @if(Auth::user()->gender == 'M')
                                                                                    <img src="{{asset('img/male-default.png')}}">
                                                                                @else
                                                                                    <img src="{{asset('img/female-default.png')}}">
                                                                                @endif
                                                                            </div>
                                                                            <div class="avatar-preview preview-md remove hidden"> @if(Auth::user()->gender == 'M')
                                                                                    <img src="{{asset('img/male-default.png')}}">
                                                                                @else
                                                                                    <img src="{{asset('img/female-default.png')}}">
                                                                                @endif</div>
                                                                            <div class="avatar-preview preview-sm remove hidden"> @if(Auth::user()->gender == 'M')
                                                                                    <img src="{{asset('img/male-default.png')}}">
                                                                                @else
                                                                                    <img src="{{asset('img/female-default.png')}}">
                                                                                @endif</div>
                                                                            <br>
                                                                            <button type="submit" class="btn btn-primary btn-block avatar-save">Add Photo</button>
                                                                            <br/>
                                                                            <div class="text-center">
                                                                                <img src="{{asset('img/loader.gif')}}" id="loader" width="20" style="display: none">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row avatar-btns panel-body">
                                                                        <div class="col-md-9 text-center">
                                                                            <div class="btn-group">
                                                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                                                            </div>
                                                                            <div class="btn-group">
                                                                                <button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="avatar-view" title="Change the profile Picture" style="display:none">
                                                                                @if(Auth::user()->gender == 'M')
                                                                                    <img src="{{asset('img/male-default.png')}}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                                                                @else
                                                                                    <img src="{{asset('img/female-default.png')}}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- modal-body -->
                                                        </form>
                                                        <!-- form -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <p class="text-center">Note: Each photo must be in jpg, png and gif format. </p>
                                    <p class="text-center">Note: All photos are screened as per photo guidelines. </p>
                                    <div id="successMessage"></div>
                                    <div class="row">
                                        @php
                                            $canUploadImages = (config('constants.image_upload_limit') - $existingImagesCount);
                                        @endphp
                                        @if($existingImagesCount > 0)
                                            @foreach($existingImages as $existingImage)
                                                <div class="col-sm-6 col-xs-12 col-lg-4 panel-body">
                                                    <div class="upload_photo" id="{{$existingImage->id}}">
                                                        <img src="{{config('constants.upload_url_public').$existingImage->image_thumb}}" class="img-thumbnail">
                                                        <div class="dropdown pull-right">
                                                            <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="javascript:;" class="profilePicButton" onclick="setProfileImage({{$existingImage->id}})">Set as Profile</a></li>
                                                                <li><a href="javascript:;" class="deleteButton" onclick="deleteImage({{$existingImage->id}})">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                        @for($i = 0; $i < $canUploadImages; $i++)
                                            <div class="col-sm-6 col-xs-12 col-lg-4 panel-body">
                                                <div class="upload_photo default-image">
                                                    @if(Auth::user()->gender == 'M')
                                                        <img src="{{asset('img/male-default.png')}}" class="img-thumbnail">
                                                    @else
                                                        <img src="{{asset('img/female-default.png')}}" class="img-thumbnail">
                                                    @endif
                                                    <div class="dropdown pull-right" style="display: none">
                                                        <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="javascript:;" class="profilePicButton">Set as Profile</a></li>
                                                            <li><a href="javascript:;" class="deleteButton">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                        <span id="aimage"> </span>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 panel-body">
                                            <br>
                                            <p class="notice"><b>Note:</b> Any Images that doesn’t feature yourself will be removed.</p>
                                            <p class="notice"><b>Note:</b> Any blurred images will be denied.</p>
                                        </div>
                                        <br>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer_scripts')
    <script src="{{asset('dist/js/cropper.min.js')}}"></script>
    <script src="{{asset('dist/js/main.js')}}"></script>
    <script>
        $(".pimage").change(function(e) {
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                var file = e.originalEvent.srcElement.files[i];
                var img = document.createElement("img");
                var reader = new FileReader();
                reader.onloadend = function() {
                    img.src = reader.result;
                }
                reader.readAsDataURL(file);
                $("#aimage").after(img);
            }
        });

                @if(Auth::user()->gender == 'M')
        var default_image_path = "{{asset('img/male-default.png')}}";
                @else
        var default_image_path = "{{asset('img/female-default.png')}}";
        @endif

        $("#imageUploadForm").on('submit', function(e){
            e.preventDefault();
            var image = $("#avatarInput")[0].files[0];
            if (image === undefined){
                var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
                $(".avatar-upload").after(msg);
                return false;
            }else{
                $("#loader").show();
                $("button.avatar-save").attr('disabled','disabled');
                var _token = $('input[name="_token"]').val();
                var thumb_data = $('input[name="avatar_data"]').val();
                var form = new FormData();
                form.append('image', image);
                form.append('_token', _token);
                form.append('thumb_data', thumb_data);
                $.ajax({
                    url: '{{route('ajax-upload-image')}}',
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(data) {
                        $("#loader").hide();
                        var response = data[0];
                        console.log(response);
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                        if(response.status === 0){
                            $(".avatar-upload").after(msg);
                            $("button.avatar-save").removeAttr('disabled');
                        } else {
                            $("#avatar-modal").modal('hide');
                            $(".default-image:first .img-thumbnail").attr('src','{{config('constants.upload_url_public')}}'+response.thumb_path);
                            $(".default-image:first .dropdown").show();
                            $(".default-image:first").attr('id', response.image_id);
                            $(".default-image:first .dropdown .profilePicButton").attr('onClick', 'setProfileImage('+response.image_id+')');
                            $(".default-image:first .dropdown .deleteButton").attr('onClick', 'deleteImage('+response.image_id+')');
                            $(".default-image:first").removeClass('default-image');
                        }
                    },
                    error: function(data){
                        $("#loader").hide();
                        var errors = data.responseJSON;
                        console.log(errors);
                        var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[1]+'</div>';
                        $(".avatar-upload").after(msg);
                        $("button.avatar-save").removeAttr('disabled');
                    }
                });
            }
        });
        $('#avatar-modal').on('hidden.bs.modal', function () {
            $("#loader").hide();
            $(".avatar-body .alert").remove();
            $("#imageUploadForm")[0].reset();
            $("button.avatar-save").removeAttr('disabled');
        });


        function setProfileImage(id){
            var _token = $('input[name="_token"]').val();
            var form = new FormData();
            form.append('id', id);
            form.append('_token', _token);
            $.ajax({
                url: '{{route('set-profile-image')}}',
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (data) {
                    var msg = '<div class="alert alert-success avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+data.message+'</div>';
                    $("#successMessage").html(msg);
                }
            });
        }

        function deleteImage(id){
            var _token = $('input[name="_token"]').val();
            var form = new FormData();
            form.append('id', id);
            form.append('_token', _token);
            $.ajax({
                url: '{{route('ajax-delete-image')}}',
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(data) {
                    $(".upload_photo#"+id+" .img-thumbnail").attr('src', default_image_path);
                    $(".upload_photo#"+id+" .dropdown ").hide();
                    $(".upload_photo#"+id).addClass('default-image');
                    $(".upload_photo#"+id).attr('id','');
                }
            });
        }

    </script>
@endsection