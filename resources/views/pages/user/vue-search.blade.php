@extends('layouts.app')
@section('template_title')
    Search Profile
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection
@section('content')
    <link href="{{ asset('css/select-box.css') }}" rel="stylesheet" type="text/css">
    <div class="dashboard">
        <div class="container" id="app">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <search-filter 
                    :search-text="'@if(isset($search_arry['keywords'])) {{$search_arry['keywords']}} @endif'"
                    :county-name="'{{$userProfile->country->name}}'"
                    :countries='{{$countries}}'
                    :religions='{{$religions_vue}}'
                    :sect='{{$sect}}'
                    :languages='{{$languages}}'
                    :educations='{{$educations}}'
                    :body-types='{{$body_types}}'
                    :foods='{{$foods}}'
                    :smokes='{{$smokes}}'
                    :drinks='{{$drinks}}'
                    :marital-statuses='{{$marital_status}}'
                    :age-start="{{config('constants.search_defaults.start_age')}}"
                    :age-end="{{config('constants.search_defaults.end_age')}}"
                    :height-start="{{config('constants.search_defaults.start_height')}}"
                    :height-end="{{config('constants.search_defaults.end_height')}}"
                    age-range="{{config('constants.search_defaults.start_age')}} - {{config('constants.search_defaults.end_age')}}"
                    height-range="{{config('constants.search_defaults.start_height')}} cm - {{config('constants.search_defaults.end_height')}} cm"
                    :result-set="{{$results}}"
                    :pagination="{{$pagination}}"
                    doesnt-matter="{{config('constants.doesnt_matter')}}"
                    :unlock-image-path="{{json_encode(URL::asset('img/unlock.png'))}}"
            >
            </search-filter>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script>

        $('.collapse').on('shown.bs.collapse',function(){
            $(this).parent().find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
        }).on('hidden.bs.collapse',function(){
            $(this).parent().find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
        });

    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
