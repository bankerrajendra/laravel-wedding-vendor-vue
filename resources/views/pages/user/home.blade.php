@extends('layouts.app')

@section('template_title')
	{{ Auth::user()->name }}'s' Homepage
@endsection

@section('template_fastload_css')
	.help-block1, .error-help-block1{
	margin-top: 5px;
	margin-bottom: 10px;
	color: #a94442
	}

@endsection
@section('navclasses')
	navbar-fixed-top
@endsection

@section('content')
	<div id="page-content-wrapper">
		<div class="dashboard dashes">
			<div class="container">
				<div class="row">
					User Dashboard
				</div>
			</div>
		</div>
	</div>

	<?php
if(count(getFlaggedUserDataAll(Auth::id())) > 0){ ?>

<input type="hidden" id="FeedbackStat">
<?php } ?>
<!-- Feedback message Modal -->
<div id="FeedbackModal" class="modal fade" role="dialog" style="display:none; ">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Some of the fields are incompatible with site policies.</h4>
			</div>
			<form method="post" id="updateFlagedUserre_entry_form" name="updateFlagedUserre_entry_form" action="{{route('ajax.updateFlagedUserre_entry')}}">
				<div class="modal-body">
					{{ csrf_field() }}
					@foreach(getFlaggedUserDataAll(Auth::id()) as $feedback)
						<div class="form-group">
							<label>
								<?php echo ucwords(str_replace("_", " ",$feedback->property_name)); ?>
							</label>
							<input type="hidden" name="user_id" value="{{ Auth::id() }}">
							@if($feedback->property_name == 'business_information' || $feedback->property_name == 'specials')
								<textarea name="{{$feedback->property_name}}" rows="5" style="width:75%;">{{$feedback->property_value}}</textarea>
							@else
								<input name="{{$feedback->property_name}}" value="{{$feedback->property_value}}" class="form-control">
							@endif

							<p><strong style="color:red">Reason : </strong>{{$feedback->feedback}}</p>

						</div>
					@endforeach
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Update</button>
				</div>
			</form>
		</div>

	</div>
</div>

@endsection

@section('footer_scripts')

	<script type="text/javascript" src="{{ e(asset('vendor/jsvalidation/js/jsvalidation.js'))}}"></script>

	{!! $validator !!}
@endsection


