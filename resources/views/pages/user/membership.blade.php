@extends('layouts.app')

@section('template_title')
    Membership
@endsection
@section('navclasses')

@endsection
@section('template_fastload_css')
@endsection
@section('content')
    <div class="mem">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="row">
                        <div class="col-sm-4 col-xs-5">
                            @if(!empty($featured_users))
                                @foreach($featured_users as $featuredUser)
                                    <small class="memimg"><cite><img src="{{$featuredUser->getVendorProfilePic()}}" alt="featured photo" draggable="false" width="50" /></cite></small>

                                @endforeach
                            @endif
                        </div>
                        <div class="col-sm-8 col-xs-7">
                            <p><b>Chat and send messages instantly!</b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container pay-free">
        <div class="row">
            <div class="col-lg-8 col-sm-offset-2">
                <div class="row">
                    <div class="membser-list">
                        <div class="col-sm-8 panel-body">
                            <h4><b>Muslim Wedding Plus Benefits</b></h4>
                            <span></span>
                            @if(!empty($free_member_benefits))
                                <ul class="list-group">
                                    @foreach($free_member_benefits as $benefit)
                                        <li class="list-group-item">
                                        @if($benefit['image'] != "")
                                            @if (file_exists(public_path().'/img/membership-icons/'.$benefit['image']))
                                                <img src="{{asset('img/membership-icons/'.$benefit['image'])}}" alt="" width="20" border="0" />
                                            @endif
                                        @endif
                                        {{$benefit['name']}}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                        <div class="col-sm-4">

                            @if(getBannerByType('Free Membership') != "")
                                {!!getBannerByType('Free Membership')!!} 
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-8 col-sm-offset-2">
                <br>
                <h4><b>Share this page to get plus membership free</b></h4>
                <hr>
                <div class="sharethis-inline-share-buttons" data-url="https://www.muslimwedding.com" data-title="Join Muslimwedding.com"></div>
                <hr>
            </div>
            <div class="col-sm-8 col-sm-offset-2 panel-body">
                <a href="{{route('public.home')}}" class="btn btn-warning btn-lg pull-right">Enter</a>
            </div>
        </div>


        </div>
    </div>

@endsection