@extends('layouts.app')

@section('template_title')
    Deactivate Account
@endsection

@section('template_fastload_css')
    .help-block1, .error-help-block1{
    margin-top: 5px;
    margin-bottom: 10px;
    color: #a94442
    }

    hr{
    border-top: 1px solid #ccc;
    }


@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6">
                <div class="register">
                    <form method="POST" action="{{route('deactivating-account')}}" id="deactivateAccount">
                        @csrf
                        <div class="panel">
                            <h3>Deactivate Account</h3>
                        </div>
                        <p>Deactivating your account will remove your profile and photos from the site and prevent users from messaging you. To reactivate simply log in your account later,</p>
                        <p>Reason for deactivation</p>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rdReason" value="I found the ideal match" checked="checked"> I found the ideal match
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rdReason" value="I'm no longer searching for my other half">I'm no longer searching for my other half
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rdReason" value="I'm taking a break from the site"> I'm taking a break from the site
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rdReason" value="I didn't find my ideal match"> I didn't find my ideal match
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rdReason" value="This is not working for me"> This is not working for me
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rdReason" value="Other"> Other
                                </label>
                            </div>
                        </div>

                        <p>Is there anything that we could do better?</p>
                        <div class="form-group">
                            <label>Please enter any additional comments :</label>
                            <textarea class="form-control" id="description" name="description" style="height:100px;"></textarea>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-warning"  name="submitUser" value="DEACTIVATE ACCOUNT">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
