@extends('layouts.app')

@section('template_title')
    {{ $userProfile->first_name }} Profile
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
    .help-block1, .error-help-block1{
    margin-top: 5px;
    margin-bottom: 10px;
    color: #a94442
    }

@endsection

@section('template_linked_css')
    <link href="{{asset('css/zoom.css')}}" rel="stylesheet" type="text/css" />
@endsection


@section('content')

    <div class="navbar-fixed-bottom hidden-lg hidden-sm hidden-md nav-footer">
        @if($authHasPrivatePhoto && ($blockedFlag<1 && $skippedFlag<1))
            @if($authPhotoShareStatus)
                <a href="javascript:;" class="unsharePhotos btn btn-warning button" data-profileid="{{$userProfile->getEncryptedId()}}">Unshare Photos</a>
            @else
                <a href="javascript:;" class="sharePhotos btn btn-warning button" data-profileid="{{$userProfile->getEncryptedId()}}">Share Photos</a>
            @endif
        @endif
        <a href="#send_message" class="btn btn-success button">Message</a>&nbsp;
        <div class="dropdown pull-right" style="display:inline-block; padding-left:5px;">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" data-target="drop"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal" data-target="#reportUser" href="#" >Report  User</a></li>
                @if($blockedFlag>0)
                    <li><a href="javascript:;" class="unblockProfile" data-profileid="{{$userProfile->getEncryptedId()}}">UnBlock User</a></li>
                @else
                    <li><a data-toggle="modal" onclick="blockUserData();" data-target="#demo-4">Block User</a></li>
                @endif
            </ul>
        </div>
        @if($blockedFlag<=0 || $skippedFlag<=0) <a id="likeProfile" data-profileid="{{$userProfile->getEncryptedId()}}" data-reloadpage="true" class="btn btn-default button click-fav  pull-right likeProfile"><i class="fa @if(!(!$userProfile->isSkipped() && !$userProfile->isLiked() && !$userProfile->isLikedBack())) fa-heart @else  fa-heart-o @endif" @if(!(!$userProfile->isSkipped() && !$userProfile->isLiked() && !$userProfile->isLikedBack())) style="color: red" @endif  aria-hidden="true"></i></a>@endif
    </div>

    <div id="page-content-wrapper">
        <section>
            <div class="container view-prof">
                <div class="row">
                    @if(session('success'))
                        <div class="alert alert-success avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>{{session('success')}}</div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>{{session('error')}}</div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="col-sm-3 col-xs-12">
                        <div class="main-img">
                            <img src="{{$userProfile->getProfilePic('profile-image')}}" style="width:100%;" title="" alt="">
                            <div class="user_details">
                                @if($userProfile->users_images->isEmpty())
                                    @if($requestToUploadPhotoStatus>0)
                                        <div class="btn-success" style="text-align: center">Request Already Sent</div>
                                    @else
                                        <a href="javascript:;" class="sendPhotoRequest btn btn-warning btn-block" data-profileid="{{$userProfile->getEncryptedId()}}">Send Photo Request</a>
                                    @endif
                                @endif
                                <h4 class="text-center">
                                    {{$userProfile->first_name}}
                                    {{--{{config('constants.site_code').$userProfile->id}}--}}
                                </h4>
                            </div>
                        </div>

                        <div class="new-profile-match">
                            <br>
                            <div class="panel panel-default hidden-xs">
                                <div class="panel-heading">Member Status</div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-9 col-sm-9 col-xs-8">
                                            This member is verified by Muslim Wedding.
                                        </div>
                                        <div class="col-lg-3 col-sm-3  col-xs-4">
                                            <img src="{{asset('img/verified.png')}}" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(isset($newProfiles) && count($newProfiles)>0)
                            <div class="panel panel-default hidden-xs">
                                <div class="panel-heading">
                                    New Profile
                                </div>

                                <div class="panel-body" id="testDiv2">
                                @foreach($newProfiles as $newProfile)
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="{{$newProfile->profileLink()}}">
                                                <img src="{{$newProfile->getProfilePic()}}" class="media-object img-thumbnail" style="width:70px">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <a href="{{$newProfile->profileLink()}}" class="msg-name">
                                                    {{$newProfile->first_name}}
{{--                                                    {{config('constants.site_code').$newProfile->id}}--}}
                                                </a>
                                            </h4>
                                            <p>{{$newProfile->getAge()}}, {{config('constants.height.'.$newProfile->users_information->height)}} , {{$newProfile->users_information->religion->religion}}, @if($newProfile->users_information->sect_show=='Y') {{$newProfile->users_information->get_community()}}, @endif @if(!is_null($newProfile->users_information->profession)) {{$newProfile->users_information->profession->profession}} @endif,  {{$newProfile->city->name}}, {{$newProfile->country->name}}</p>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                            @endif

                            @if(isset($matchesProfiles) && count($matchesProfiles)>0)
                            <div class="panel panel-default hidden-xs">
                                <div class="panel-heading">
                                    Matches Profile
                                </div>
                                <div class="panel-body" id="testDiv3">
                                    @foreach($matchesProfiles as $newProfile)
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="{{$newProfile->profileLink()}}">
                                                    <img src="{{$newProfile->getProfilePic()}}" class="media-object img-thumbnail" style="width:70px">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="{{$newProfile->profileLink()}}" class="msg-name">
                                                        {{$newProfile->first_name}}
                                                        {{--{{config('constants.site_code').$newProfile->id}}--}}
                                                    </a>
                                                </h4>
                                                <p>{{$newProfile->getAge()}}, {{config('constants.height.'.$newProfile->users_information->height)}} , {{$newProfile->users_information->religion->religion}}, @if($newProfile->users_information->sect_show=='Y') {{$newProfile->users_information->get_community()}}, @endif @if(!is_null($newProfile->users_information->profession)) {{$newProfile->users_information->profession->profession}} @endif,  {{$newProfile->city->name}}, {{$newProfile->country->name}}</p>
                                            </div>
                                        </div>

                                    @endforeach

                                </div>
                            </div>
                            @endif
                        </div>
                        @if(getBannerByType('View Profile') != "")
                            {!!getBannerByType('View Profile','img-thumbnail hidden-xs')!!}
                        @endif
                    </div>

                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                        <div class="user-pr-details">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-lg-6 hidden-sm hidden-lg hidden-md">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td><b>Age</b></td>
                                            <td>{{$userProfile->getAge()}} Years</td>
                                        </tr>

                                        <tr>
                                            <td><b>Religion</b></td>
                                            <td>{{$userProfile->users_information->religion->religion}} @if($userProfile->users_information->sect_show=='Y') , {{$userProfile->users_information->get_community()}} @endif    @if($userProfile->users_information->subcaste_show=='Y') , {{$userProfile->users_information->get_subcaste()}} @endif </td>
                                        </tr>

                                        @if(!is_null($userProfile->users_information->marital_status))
                                            <tr>
                                                <td><b>Marital Status</b></td>
                                                <td>{{config('constants.marital_status.'.$userProfile->users_information->marital_status)}}</td>
                                            </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->gender))
                                            <tr>
                                                <td><b>Gender</b></td>
                                                <td>{{config('constants.gender.'.$userProfile->gender)}}</td>
                                            </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->education))
                                            <tr>
                                                <td><b>Education</b></td>
                                                <td>{{$userProfile->users_information->education->education}}</td>
                                            </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->profession))
                                            <tr>
                                                <td><b>Profession</b></td>
                                                <td>{{$userProfile->users_information->profession->profession}}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <td><b>Location</b></td>
                                            <td>{{$userProfile->city->name}}, {{$userProfile->country->name}}</td>
                                        </tr>

                                        @if(!is_null($userProfile->users_information->horoscope) && $userProfile->users_information->horoscope_show=='Y')
                                            <tr>
                                                <td><b>Horoscope</b></td>
                                                <td>{{$userProfile->users_information->horoscope}}</td>
                                            </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->body_type) && $userProfile->users_information->body_type_show=='Y')
                                            <tr>
                                                <td><b>Body Type</b></td>
                                                <td>{{$userProfile->users_information->body_type}}</td>
                                            </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->weight)  && $userProfile->users_information->weight_show=='Y')
                                            <tr>
                                                <td><b>Body Weight</b></td>
                                                <td>{{$userProfile->users_information->weight}} {{$userProfile->users_information->weight_measure}}</td>
                                            </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->eye_color))
                                            <tr>
                                                <td><b>Eye Color</b></td>
                                                <td>{{$userProfile->users_information->eye_color}}</td>
                                            </tr>
                                        @endif
                                        @if(!is_null($userProfile->users_information->height))
                                            <tr>
                                                <td><b>Height</b></td>
                                                <td>{{config('constants.height.'.$userProfile->users_information->height)}}</td>

                                            </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->complexion) && $userProfile->users_information->complexion_show=='Y')
                                            <tr>
                                                <td><b>Complexion </b></td>
                                                <td>{{$userProfile->users_information->complexion}}</td>
                                            </tr>
                                        @endif

                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-lg-6 hidden-xs">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td><b>Age</b></td>
                                            <td>{{$userProfile->getAge()}}  Years</td>
                                        </tr>
                                        <tr>
                                            <td><b>Religion</b></td>
                                            <td>{{$userProfile->users_information->religion->religion}} @if($userProfile->users_information->sect_show=='Y') , {{$userProfile->users_information->get_community()}} @endif  @if($userProfile->users_information->subcaste_show=='Y') , {{$userProfile->users_information->get_subcaste()}} @endif </td>
                                        </tr>

                                        @if(!is_null($userProfile->users_information->marital_status))
                                        <tr>
                                            <td><b>Marital Status</b></td>
                                            <td>{{config('constants.marital_status.'.$userProfile->users_information->marital_status)}}</td>
                                        </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->gender))
                                        <tr>
                                            <td><b>Gender</b></td>
                                            <td>{{config('constants.gender.'.$userProfile->gender)}}</td>
                                        </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->education))
                                        <tr>
                                            <td><b>Education</b></td>
                                            <td>{{$userProfile->users_information->education->education}}</td>
                                        </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->profession))
                                        <tr>
                                            <td><b>Profession</b></td>
                                            <td>{{$userProfile->users_information->profession->profession}}</td>
                                        </tr>
                                        @endif

                                        <tr>
                                            <td><b>Location</b></td>
                                            <td>{{$userProfile->city->name}}, {{$userProfile->country->name}}</td>
                                        </tr>

                                        @if(!is_null($userProfile->users_information->horoscope) && $userProfile->users_information->horoscope_show=='Y')
                                        <tr>
                                            <td><b>Horoscope</b></td>
                                            <td>{{$userProfile->users_information->horoscope}}</td>
                                        </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-lg-6 hidden-xs">
                                    <table class="table">
                                        <tbody>
                                        @if(!is_null($userProfile->users_information->body_type) && $userProfile->users_information->body_type_show=='Y')
                                        <tr>
                                            <td><b>Body Type</b></td>
                                            <td>{{$userProfile->users_information->body_type}}</td>
                                        </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->weight) && $userProfile->users_information->weight_show=='Y')
                                        <tr>
                                            <td><b>Body Weight</b></td>
                                            <td>{{$userProfile->users_information->weight}} {{$userProfile->users_information->weight_measure}}</td>
                                        </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->eye_color))
                                        <tr>
                                            <td><b>Eye Color</b></td>
                                            <td>{{$userProfile->users_information->eye_color}}</td>
                                        </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->height))
                                        <tr>
                                            <td><b>Height</b></td>
                                            <td>{{config('constants.height.'.$userProfile->users_information->height)}}</td>

                                        </tr>
                                        @endif

                                        @if(!is_null($userProfile->users_information->complexion) && $userProfile->users_information->complexion_show=='Y')
                                        <tr>
                                            <td><b>Complexion </b></td>
                                            <td>{{$userProfile->users_information->complexion}}</td>
                                        </tr>
                                        @endif

                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-xs-12">
                                    <hr>
                                </div>

                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="panel-heading partner_pref" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-target="#demo" class="" aria-expanded="true">
                                                        <i class="fa fa-plus"></i> &nbsp;&nbsp;{{$userProfile->first_name}}'s Partner Preference Click Here
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text_right hidden-xs">
                                            @if($authHasPrivatePhoto  && ($blockedFlag<1 && $skippedFlag<1))
                                                @if($authPhotoShareStatus)
                                                    <a href="javascript:;" class="unsharePhotos btn btn-warning button" data-profileid="{{$userProfile->getEncryptedId()}}">Unshare Photos</a>
                                                @else
                                                    <a href="javascript:;" class="sharePhotos btn btn-warning button" data-profileid="{{$userProfile->getEncryptedId()}}">Share Photos</a>
                                                @endif
                                            @endif

                                            <a href="#send_message" class="btn btn-success button">Message</a>&nbsp;
                                                <?php
//                                                echo "<br>Is Liked ".$userProfile->isLiked();
//                                                echo "<br>Is Liked back ".$userProfile->isLikedBack();
//                                                echo "<br>Is Skipped ".$userProfile->isSkipped();
                                                ?>
                                                {{--@if(!$userProfile->isSkipped() && !$userProfile->isLiked() && !$userProfile->isLikedBack())--}}
                                                @if($blockedFlag<=0 && $skippedFlag<=0)<a id="likeProfile" data-profileid="{{$userProfile->getEncryptedId()}}" data-reloadpage="true" class="btn btn-default button click-fav likeProfile"><i class="fa @if(!(!$userProfile->isSkipped() && !$userProfile->isLiked() && !$userProfile->isLikedBack())) fa-heart @else  fa-heart-o @endif" @if(!(!$userProfile->isSkipped() && !$userProfile->isLiked() && !$userProfile->isLikedBack())) style="color: red" @endif aria-hidden="true"></i>&nbsp;{{$userProfile->getLikeText()}} </a>@endif
                                                {{--<button type="button" class="btn btn-primary btn-block likeProfile" id="likeProfile" data-profileid="{{$userProfile->getEncryptedId()}}" data-reloadpage="true"><i class="fa fa-check" aria-hidden="true"></i> {{$userProfile->getLikeText()}}</button>--}}

                                            <div class="dropdown pull-right" style="display:inline-block; padding-left:5px;">
                                                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" data-target="drop" ><i class="fa fa-cog" aria-hidden="true"></i></button>

                                                <ul class="dropdown-menu">
                                                    <li><a data-toggle="modal" data-target="#reportUser" href="#">Report  User</a></li>
                                                    @if($blockedFlag>0)
                                                        <li><a href="javascript:;" class="unblockProfile" data-profileid="{{$userProfile->getEncryptedId()}}">UnBlock User</a></li>
                                                    @else
                                                        <li><a data-toggle="modal" onclick="blockUserData();" data-target="#demo-4">Block User</a></li>
                                                    @endif

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel-group -->

                                <div class="modal fade head_popup" id="reportUser" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" id="resetPublic" value="reset">×</button>
                                                <h4 class="modal-title" id="avatar-modal-label">Are you sure you want to report this user ?</h4>
                                            </div>

                                            <div class="modal-body report">
                                                <br>
                                                <form id="reportUserForm" name="reportUserForm" method="post" class="needs-validation" role="form" action="" >
                                                    <div class="row">
                                                        <div class="col-md-12 form-group">
                                                            <input class="form-control" id="reason" name="reason" placeholder="Reason" type="text"  />
                                                            <span id="reason-error" class="help-block1 error-help-block1" style="display: none">The reason field is required.</span>
                                                        </div>
                                                        <div class="col-md-12 form-group">
                                                            <input type="hidden" id="userId" name="userId" value="{{$userProfile->getEncryptedId()}}">
                                                            <textarea class="form-control" id="description" name="description" placeholder="Description" style="height:100px;"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 form-group">
                                                            <button type="submit" class="btn btn-success"> Submit</button>&nbsp;&nbsp;
                                                            <button type="button" class="btn btn-setting" class="close" data-dismiss="modal"> Cancel</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade" id="demo-4" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body text-center report">
                                                <h6 class="modal-title"></h6>
                                                <h4>Are you sure you want to block this user ?</h4>
                                                <br>
                                                <a href="javascript:;" class="btn btn-success blockProfile" data-profileid="{{$userProfile->getEncryptedId()}}"> Ok</a>&nbsp;
                                                <button type="button" class="btn btn-setting" data-dismiss="modal"> Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade text-center" id="reportSuccess">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" id='resetPublic1' value='reset'>&times;</button>
                                        <h4 class="modal-title" style="color:#fff;">Thank You!</h4>
                                    </div>

                                    <div class="modal-body report">
                                        <br>
                                        <p>Your report has been submitted successfully. Admin will review it and will take required action.</p>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 form-group">
                                                <button type="button" class="btn btn-setting" class="close" data-dismiss="modal">Okay</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div id="demo" class="panel-collapse collapse" aria-expanded="true" style="">
                            <div class="user-pr-details">
                                <div class="p_result text-center">{{$userProfile->first_name}}'s Preference Results</div>
                                <div class="match text-center">You have {{count($similarities)}} similarities in common</div>

                                @foreach($similarities as $simKey=>$simVal)
                                    <div class="compatibility-list row">
                                        <div class="col-md-3 col-md-offset-2 col-xs-5">
                                            <h5>{{ucwords(str_replace("_"," ", $simKey))}}</h5>
                                        </div>
                                        <div class="col-sm-3 col-xs-5">
                                            <h5>{{$simVal}}</h5>
                                        </div>
                                        <div class="col-sm-2 col-xs-2">
                                            <h4 class="pull-right icon"><i class="fa fa-check" aria-hidden="true"></i></h4>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                        @if(count($existingPublicImages)>0)
                            <div class="user-pr-details">
                                <div class="profile-heading">Public Photos</div>
                                <hr>
                                <div class="row gallery" id="1">
                                    @foreach($existingPublicImages as $existingImage)
                                        <div class="col-xs-6 col-sm-4 col-lg-2 panel-body gal_g">
                                            <a href="{{config('constants.S3_WEDDING_IMAGES_ORIGINAL').$existingImage->image_full}}"><img src="{{config('constants.S3_WEDDING_IMAGES').$existingImage->image_thumb}}" class="img-thumbnail" alt=""></a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        @if($existingPrivateImagesCount>0)
                        <div class="user-pr-details">
                            <div class="profile-heading">Private Photos</div>
                            <div class="para">
                                <hr>
                                @if($existingPrivateImagesCount>0)
                                    @if($userHasPhotoSharedAccess || $priPhotoRequestStatus=='A')
                                        <div class="row gallery" id="2">
                                            @foreach($existingPrivateImages as $existingImage)
                                                <div class="col-xs-6 col-sm-4 col-lg-2 panel-body gal_g1">
                                                    <a href="{{config('constants.S3_WEDDING_IMAGES_ORIGINAL').$existingImage->image_full}}"><img src="{{config('constants.S3_WEDDING_IMAGES').$existingImage->image_thumb}}" class="img-thumbnail" alt=""></a>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <p style="display: inline-block;">{{$userProfile->first_name}} have {{$existingPrivateImagesCount}} private photo(s), You can send a private photo request to view them.</p> &nbsp;&nbsp;
                                        @if($priPhotoRequestStatus=='P')
                                            <div class="btn btn-success" style="text-align: center" >Request Already Sent</div>
                                        @else
                                            <a href="javascript:;" class="requestToView btn btn-warning" data-profileid="{{$userProfile->getEncryptedId()}}">Request to View</a>
                                        @endif
                                        <div class="row" id="2">
                                            @foreach($existingPrivateImages as $existingImage)
                                                <div class="col-xs-6 col-sm-4 col-lg-2 panel-body gal_g1">
                                                    <?php $lockImg=($userProfile->gender == 'M')?asset('img/male_unlock.jpg'):asset('img/female_unlock.jpg'); ?>
                                                    <a href="{{$lockImg}}"><img src="{{$lockImg}}" class="img-thumbnail" alt=""></a>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                            {{--We will restrict this area when the current user is not allowed to see this user's private pictures. In that case "gallery class of above div tag will be removed, so that it doesn't become the part of photo gallery--}}
                                        {{--<div class="col-xs-6 col-sm-4 col-lg-2 panel-body gal_g1">--}}
                                            {{--<a href="img/lock_images.jpg"><img src="{{asset('img/lock_images.jpg')}}" class="img-thumbnail" alt=""></a>--}}
                                        {{--</div>--}}
                                        {{----}}

                                @endif
                            </div>
                        </div>
                        @endif

                        @if(!is_null($userProfile->users_information->about))
                        <div class="user-pr-details">
                            <div class="profile-heading">About Me</div>
                            <hr>
                            <p>{{$userProfile->users_information->about}}</p>
                        </div>
                        @endif

                        @if(!is_null($userProfile->users_information->looking_for))
                        <div class="user-pr-details">
                            <div class="profile-heading">What I'm Looking For</div>
                            <hr>
                            <p>{{$userProfile->users_information->looking_for}}</p>
                        </div>
                        @endif

                        <div class="user-pr-details" id="send_message">
                            <div class="profile-heading">Send a Quick Message</div>
                            @if($blockedFlag>0 || $skippedFlag>0)<div id="error_msg" style="color: #a94442; ">User blocked/hide by you. Please unblock/unhide user first.</div>@endif
                            <div class="para">
                                <div class="row">

                                    <form action="javascript:void(0);">

                                    <input type="hidden" name="receiver_id" value="{{$userProfile->id}}"><input type="hidden" name="pageType" value="inbox">
                                        <div class="col-sm-12">
                                            <span id="returnSpan{{$userProfile->id}}" style="color: red;"></span>
                                            <div class="form-group">

                                                <textarea class="form-control sendMessageInput" id="sendMessageInput{{$userProfile->id}}" name="msg_description" @if($blockedFlag>0 || $skippedFlag>0) disabled="disabled" @endif  style="box-shadow:none;" data-profileid="{{$userProfile->getEncryptedId()}}" data-id="{{$userProfile->id}}" placeholder="Write Your Message.."  required="" cols="54" rows="5" ></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 pull-right">

                                            <input type="submit" id="sendMessage{{$userProfile->id}}" @if($blockedFlag>0 || $skippedFlag>0) disabled="disabled" @endif data-profileid="{{$userProfile->getEncryptedId()}}" data-id="{{$userProfile->id}}"  class="btn btn-success sendMessage" value="Send Message">
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        <div class="user-pr-details">
                            <div class="profile-heading">Life Style</div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <h5> Drink :</h5>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-6">
                                    <h5>{{config('constants.drink.'.$userProfile->users_information->drink)}}</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <h5>Smoke :</h5>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-6">
                                    <h5>{{config('constants.smoke.'.$userProfile->users_information->smoke)}}</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-6">
                                    <h5>Food Preference :</h5>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-6">
                                    <h5>{{$userProfile->users_information->food}}</h5>
                                </div>
                            </div>
                        </div>






                        <?php $hobbies=$userProfile->users_information->hobbies; ?>
                        @if(!is_null($hobbies))
                            <?php $hobbiesArr=explode(',',$hobbies); ?>
                            <div class="user-pr-details">
                                <div class="profile-heading">Hobbies</div>
                                <hr>
                                <div class="row">
                                @foreach ($hobbiesArr as $key=>$hobby)
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <h5>{{config('constants.hobbies.'.trim($hobby))}}</h5>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        @endif

                        <?php $interests=$userProfile->users_information->interests; ?>
                        @if(!is_null($interests))
                            <?php $interestsArr=explode(',',$interests); ?>
                            <div class="user-pr-details">
                                <div class="profile-heading">Interests</div>
                                <hr>
                                <div class="row">
                                    @foreach ($interestsArr as $key=>$interest)
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <h5>{{config('constants.interests.'.trim($interest))}}</h5>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        <?php $favourite_music=$userProfile->users_information->favourite_music; ?>
                        @if(!is_null($favourite_music))
                            <?php $favourite_musicArr=explode(',',$favourite_music); ?>
                            <div class="user-pr-details">
                                <div class="profile-heading">Favourite Music</div>
                                <hr>
                                <div class="row">
                                    @foreach ($favourite_musicArr as $key=>$favourite_msc)
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <h5>{{config('constants.favourite_music.'.trim($favourite_msc))}}</h5>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        <?php $favourite_reads=$userProfile->users_information->favourite_reads; ?>
                        @if(!is_null($favourite_reads))
                            <?php $favourite_readsArr=explode(',',$favourite_reads); ?>
                            <div class="user-pr-details">
                                <div class="profile-heading">Favourite Reads</div>
                                <hr>
                                <div class="row">
                                    @foreach ($favourite_readsArr as $key=>$favourite_read)
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <h5>{{config('constants.favourite_reads.'.trim($favourite_read))}}</h5>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        <?php $preferred_movies=$userProfile->users_information->preferred_movies; ?>
                        @if(!is_null($preferred_movies))
                            <?php $preferred_moviesArr=explode(',',$preferred_movies); ?>
                            <div class="user-pr-details">
                                <div class="profile-heading">Preferred  Movies</div>
                                <hr>
                                <div class="row">
                                    @foreach ($preferred_moviesArr as $key=>$preferred_movie)
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <h5>{{config('constants.preferred_movies.'.trim($preferred_movie))}}</h5>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif


                        <?php $sports=$userProfile->users_information->sports; ?>
                        @if(!is_null($sports))
                            <?php $sportsArr=explode(',',$sports); ?>
                            <div class="user-pr-details">
                                <div class="profile-heading">Sports</div>
                                <hr>
                                <div class="row">
                                    @foreach ($sportsArr as $key=>$sport)
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <h5>{{config('constants.sports.'.trim($sport))}}</h5>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif




                        <?php $spoken_languages=$userProfile->users_information->spoken_languages; ?>
                        @if(!is_null($spoken_languages))
                            <?php  $spoken_languagesArr=get_spoken_languages($spoken_languages);  //print_r($spoken_languages); ?>
                            <div class="user-pr-details">
                                <div class="profile-heading">Spoken Languages</div>
                                <hr>
                                <div class="row">
                                    @foreach ($spoken_languagesArr as $language)
                                        <div class="col-md-3 col-sm-6 col-xs-6">
                                            <h5>{{$language->language}}</h5>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif


                        <div class="new-profile-match hidden-lg hidden-md hidden-sm">
                            <div class="row">
                                <div class="col-sm-12 text-left">
                                    <br>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Member Status</div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-9 col-sm-9 col-xs-8">
                                                    This member is verified by Muslim Wedding.
                                                </div>
                                                <div class="col-lg-3 col-sm-3  col-xs-4">
                                                    <img src="{{asset('img/verified.png')}}" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if(isset($newProfiles) && count($newProfiles)>0)
                                    <div class="col-sm-12 text-left">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            New Profile
                                        </div>
                                        <div class="panel-body" id="testDiv4">
                                            @foreach($newProfiles as $newProfile)
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a href="{{$newProfile->profileLink()}}">
                                                            <img src="{{$newProfile->getProfilePic()}}" class="media-object img-thumbnail" style="width:70px">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            <a href="{{$newProfile->profileLink()}}" class="msg-name">
                                                                {{$newProfile->first_name}}
                                                                {{--{{config('constants.site_code').$newProfile->id}}--}}
                                                            </a>
                                                        </h4>
                                                        <p>{{$newProfile->getAge()}}, {{config('constants.height.'.$newProfile->users_information->height)}} , {{$newProfile->users_information->religion->religion}}, @if($newProfile->users_information->sect_show=='Y') {{$newProfile->users_information->get_community()}}, @endif  @if(!is_null($newProfile->users_information->profession)) {{$newProfile->users_information->profession->profession}} @endif,  {{$newProfile->city->name}}, {{$newProfile->country->name}}</p>
                                                    </div>
                                                </div>

                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                                @endif

                                @if(isset($matchesProfiles) && count($matchesProfiles)>0)
                                    <div class="col-sm-12 text-left">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Matches Profile
                                        </div>
                                        <div class="panel-body" id="testDiv5">
                                            @foreach($matchesProfiles as $newProfile)
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a href="{{$newProfile->profileLink()}}">
                                                            <img src="{{$newProfile->getProfilePic()}}" class="media-object img-thumbnail" style="width:70px">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            <a href="{{$newProfile->profileLink()}}" class="msg-name">
                                                                {{$newProfile->first_name}}
                                                                {{--{{config('constants.site_code').$newProfile->id}}--}}
                                                            </a>
                                                        </h4>
                                                        <p>{{$newProfile->getAge()}}, {{config('constants.height.'.$newProfile->users_information->height)}}, {{$newProfile->users_information->religion->religion}}, @if($newProfile->users_information->sect_show=='Y') {{$newProfile->users_information->get_community()}}, @endif  @if(!is_null($newProfile->users_information->profession)) {{$newProfile->users_information->profession->profession}} @endif,  {{$newProfile->city->name}}, {{$newProfile->country->name}}</p>
                                                    </div>
                                                </div>

                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>


                        @if(getBannerByType('View Profile') != "")
                            {!!getBannerByType('View Profile','img-thumbnail hidden-md hidden-sm hidden-lg')!!}
                        @endif
                        <br><br>

                    </div>
                </div>
            </div>

        </section>
    </div>

    {{--<section>--}}
    {{--<div class="container">--}}
    {{--<div class="col-12 col-lg-10 offset-lg-1" style="min-height: 600px;">--}}

    {{--@include('panels.welcome-panel')--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

@endsection

@section('footer_scripts')
    <script src="{{asset('js/zoom.min.js')}}"></script>
    <script src="{{asset('js/jquery.slimscroll.min.js')}}"></script>

    <script>
        $(function(){
            $('#testDiv2').slimScroll({
                height: '250px',
                color: '#000',
                size: '10px',
                borderRadius:'2px',
                railVisible:true
            });
        });

        $(function(){
            $('#testDiv3').slimScroll({
                height: '250px',
                color: '#000',
                size: '10px',
                borderRadius:'2px',
                railVisible:true
            });
        });
        $(function(){
            $('#testDiv4').slimScroll({
                height: '250px',
                color: '#000',
                size: '10px',
                borderRadius:'2px',
                railVisible:true
            });
        });

        $(function(){
            $('#testDiv5').slimScroll({
                height: '250px',
                color: '#000',
                size: '10px',
                borderRadius:'2px',
                railVisible:true
            });
        });
    </script>

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
