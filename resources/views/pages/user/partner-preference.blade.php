@extends('layouts.app')
@section('template_title')
	Edit Profile
@endsection
@section('navclasses')
	navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection

@section('content')

	<link href="{{ asset('css/select-box.css') }}" rel="stylesheet" type="text/css">
	<div class="edit-profile">
		<div class="container">
			@if(session('success'))
				<div class="alert alert-success" role="alert">
					{{session('success')}}
				</div>
			@endif
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="row">
				@include('partials.my-profile-navigation')

				<div class="col-sm-8 panel-body">
					<form method="post" id="updatePartnerPreferenceFrontend" action="{{ route('updatePartnerPreferenceFrontend') }}">
						{{ csrf_field()}}
						<div class="panel-group" id="accordion">
							<!--Personal Profile-->
							<div class="panel panel-success">
								<div class="panel-heading mobile-panel">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="edit_partner">Personal Profile  <i class="more-less glyphicon glyphicon-minus"></i></a>
									</h4>
								</div>
								<div id="collapse1" class="panel-collapse collapse in">
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-3">
												<label>Age</label>
											</div>
											<div class="col-sm-4 col-xs-5">
												<div class="form-group">
													<select name="age_from" class="form-control age_from">
														@for ($age = 18; $age <= 50; $age++)
															<option value="{{ $age }}" @if(@$ppr->age_from == $age) selected="selected" @endif >{{ $age }} Year</option>
														@endfor
													</select>
												</div>
											</div>
											<div class="col-sm-1 col-xs-2 text-center">
												<b>To</b>
											</div>
											<div class="col-sm-4 col-xs-5">
												<div class="form-group">
													<select name="age_to" class="form-control age_to">
														@for ($age = 18; $age <= 50; $age++)
															<option value="{{ $age }}"  @if(@$ppr->age_to == $age) selected="selected" @endif>{{ $age }} Year</option>
														@endfor
													</select>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-sm-3">
												<label>Height</label>
											</div>
											<div class="col-sm-4 col-xs-5">
												<div class="form-group">
													<select name="height_from" class="form-control height_from">
														@foreach(config('constants.height') as $key=>$height)
															<option value="{{ $key }}"  @if(@$ppr->height_from == $key) selected="selected" @endif>{{ $height }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-sm-1 col-xs-2 text-center">
												<b>To</b>
											</div>
											<div class="col-sm-4 col-xs-5">
												<div class="form-group">
													<select name="height_to"  class="form-control height_to">
														@foreach(config('constants.height') as $key=>$height)
															<option value="{{ $key }}" @if(@$ppr->height_to == $key) selected="selected" @endif >{{ $height }}</option>
														@endforeach
													</select>
													</select>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-sm-3">
												<label>Marital Status</label>
											</div>
											<div class="col-sm-9">
												<div class="form-group">
													<select name="marital_status[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" >
														<option value="">Select</option>
														<option value="{{config('constants.doesnt_matter')}}" @if(@$ppr->marital_status == "DM") selected="selected" @endif>Doesn't matter</option>
														@foreach(config('constants.marital_status') as $key=>$val)
															<option value="{{ $key }}" @if(@$ppr->marital_status == $key) selected="selected" @endif>{{ $val }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label>Religion </label>
											</div>
											<div class="col-sm-9">
												<div class="form-group">
													<select name="religion" id="religion" class="form-control">
														@foreach($religions as $name=>$rId)
															<option value="{{$rId}}">{{ ucfirst($name) }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label>Sect/Math-hab</label>
											</div>
											@php
												$sect = explode(",", @$ppr->community);
											@endphp
											<div class="col-sm-9">
												<div class="form-group">
													<div class="ne-search-option">
														<select name="sect[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" >
															<option value="{{config('constants.doesnt_matter')}}" @if(@$ppr->community == "DM") selected="selected" @endif>Doesn't matter</option>
															@foreach(getSubcastes() as $sct)
																<option value="{{ $sct->id }}" <?php if(in_array($sct->id, $sect)){echo "selected";} ?>>{{ $sct->name }}</option>
															@endforeach
														</select>
														<i class='chosen_loader'>loading...</i>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label>Language </label>
											</div>
											@php
												$language = explode(",", @$ppr->languages);
											@endphp
											<div class="col-sm-9">
												<div class="form-group">
													<select data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" name="language[]">
														<option value="{{config('constants.doesnt_matter')}}" @if(@$ppr->languages == "DM") selected="selected" @endif>Doesn't matter</option>
														@foreach(getLanguages() as $lang)
															<option value="{{ $lang->id }}" <?php if(in_array($lang->id, $language)){echo "selected";} ?>>{{ $lang->language }}</option>
														@endforeach

													</select>
													<i class='chosen_loader'>loading...</i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label>Eye Color </label>
											</div>
											<div class="col-sm-9">
												@php
													$eye_color = explode(",", @$ppr->eye_color);
												@endphp
												<div class="form-group">
													<select  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" name="eye_color[]">

														<option value="{{config('constants.doesnt_matter')}}" @if(@$ppr->eye_color == "DM") selected="selected" @endif >Doesn't matter</option>
														@foreach(config('constants.eye_color') as $key=>$color)
															<option value="{{ $key }}" <?php if(in_array($key, $eye_color)){echo "selected";} ?> >  {{ $color }}</option>
														@endforeach


													</select>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-sm-3">
												<label>Complexion </label>
											</div>
											<div class="col-sm-9">
												@php
													$st = explode(",", @$ppr->skin_tone);
												@endphp
												<div class="form-group">
													<select  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" name="complexion[]">
														<option value="{{config('constants.doesnt_matter')}}" @if(@$ppr->skin_tone == "DM") selected="selected" @endif>Doesn't matter</option>
														@foreach(config('constants.skin_tone') as $key=>$cpl)
															<option value="{{ $key }}"   <?php if(in_array($key, $st)){echo "selected";} ?>>{{ $cpl }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>


										<div class="row">
											<div class="col-sm-3">
												<label>Body Type </label>
											</div>
											<div class="col-sm-9">
												@php
													$bt = explode(",", @$ppr->body_type);
												@endphp
												<div class="form-group">
													<select  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" name="body_type[]">
														<option value="{{config('constants.doesnt_matter')}}" @if(@$ppr->skin_tone == "DM") selected="selected" @endif>Doesn't matter</option>


														@foreach(config('constants.body_type') as $key=>$cpl)
															<option value="{{ $key }}"   <?php if(in_array($key, $bt)){echo "selected";} ?>>{{ $cpl }}</option>
														@endforeach


													</select>
												</div>
											</div>
										</div>


									</div>
								</div>
							</div>
							<!--End Personal Profile-->

							<!--Education & Career-->
							<div class="panel panel-warning">
								<div class="panel-heading mobile-panel">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="edit_partner-education">Education & Career <i class="more-less glyphicon glyphicon-plus"></i></a>
									</h4>
								</div>
								<div id="collapse4" class="panel-collapse collapse">
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-3">
												<label>Education</label>
											</div>
											@php
												$education = explode(",", @$ppr->education);
											@endphp

											<div class="col-sm-9">
												<div class="form-group">
													<select data-placeholder="Select" class="chosen-select form-control" multiple tabindex="5" name="education_id[]">

														<option value="{{config('constants.doesnt_matter')}}" @if(@$ppr->education == "DM") selected="selected" @endif>Doesn't matter</option>
														@foreach(getEducation() as $ed)
															<option value="{{ $ed->id }}" <?php if(in_array($ed->id, $education)){echo "selected";} ?>>{{ $ed->education }}</option>
														@endforeach

													</select>
													<i class='chosen_loader'>loading...</i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label>Employment</label>
											</div>
											<div class="col-sm-9">
												@php
													$professional_area = explode(",", @$ppr->professional_area);
												@endphp
												<div class="form-group">
													<select data-placeholder="Select" class="chosen-select form-control" multiple tabindex="5" name="profession_id[]">

														<option value="{{config('constants.doesnt_matter')}}"  @if(@$ppr->professional_area == "DM") selected="selected" @endif>Doesn't matter</option>
														@foreach(getProfession() as $prf)
															<option value="{{ $prf->id }}"  <?php if(in_array($prf->id, $professional_area)){echo "selected";} ?>>{{ $prf->profession }}</option>
														@endforeach

													</select>
													<i class='chosen_loader'>loading...</i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label>Annual Income</label>
											</div>
											<div class="col-sm-3 col-xs-5">
												<div class="form-group">
													<select name="currency_code" class="form-control">
														@foreach(@$currency as $rId=>$name)
															<option value="{{ $name }}" {{ ( $name == @$ppr->currency) ? 'selected' : '' }}> {{ $name }} </option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-sm-6 col-xs-7">
												<div class="form-group">
													<select name="annual_income"class="form-control">

														<option value="{{config('constants.doesnt_matter')}}"  @if(@$ppr->annual_income == "DM") selected="selected" @endif>Doesn't matter</option>
														@foreach(config('constants.annual_income') as $key=>$income)
															<option value="{{ $key }}"  @if(@$ppr->annual_income == $key) selected="selected" @endif >{{ $income }}</option>
														@endforeach

													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End Education & Career-->

							<!--Location-->
							<div class="panel panel-success">
								<div class="panel-heading mobile-panel">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Location <i class="more-less glyphicon glyphicon-plus"></i></a>
									</h4>
								</div>
								<div id="collapse5" class="panel-collapse collapse">
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-3">
												<label>Country</label>
											</div>
											<div class="col-sm-9">
												@php
													$cl = explode(",", @$ppr->country_living);
												@endphp

												<div class="form-group">
													<select no-filter="1" name="country_code[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control dynamics" multiple tabindex="5" id="country" dependent="state" >

														<option value="{{config('constants.doesnt_matter')}}"  <?php if( @$ppr->country_living == "DM"){echo "selected"; } ?>   value="DM" >Doesn't matter</option>
														@foreach(getCountry() as $ctr)
															<option value="{{ $ctr->id }}"  <?php if(in_array($ctr->id, $cl)){echo "selected";} ?>>{{ $ctr->name }}</option>
														@endforeach

													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label>State</label>
											</div>
											<div class="col-sm-9">
												@php
													$sl = explode(",", @$ppr->state_living);
                                                    $cll = explode(",", @$ppr->country_living);
												@endphp
												<div class="form-group">
													<select  no-filter="1"  name="state_id[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control dynamics" id="state" dependent="city" multiple tabindex="5">

														<option value="{{config('constants.doesnt_matter')}}" <?php if( @$ppr->state_living == "DM"){echo "selected"; } ?>  value="DM" >Doesn't matter</option>
														@foreach(getStateByCountry($cll) as $stt)
															<option value="{{ $stt->id }}" <?php if(in_array($stt->id, $sl)){echo "selected";} ?>>{{ $stt->name }}</option>
														@endforeach

													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-3">
												<label>City</label>
											</div>
											<div class="col-sm-9">
												@php
													$ctl = explode(",", @$ppr->city_district);
                                                    $sll = explode(",", @$ppr->state_living);
												@endphp
												<div class="form-group">
													<select  no-filter="1"  name="city_id[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple id="city" tabindex="5">
														<option value="">Select</option>
														<option value="{{config('constants.doesnt_matter')}}"  <?php if( @$ppr->city_district == "DM"){echo "selected"; } ?>  value="DM" >Doesn't matter</option>
														@foreach(getCityByState($sll) as $ct)
															<option value="{{ $ct->id }}" <?php if(in_array($ct->id, $ctl)){echo "selected";} ?>>{{ $ct->name }}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End Location-->

							<!--Lifestyle-->
							<div class="panel panel-warning">
								<div class="panel-heading mobile-panel">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="edit_partner-lifestyle">Lifestyle <i class="more-less glyphicon glyphicon-plus"></i></a>
									</h4>
								</div>
								<div id="collapse6" class="panel-collapse collapse">
									<div class="panel-body">


										<div class="row f_padding">
											<div class="col-sm-3">
												<label>Food Preference</label>
											</div>
											<div class="col-sm-9">
												@php
													$dt = explode(",", @$ppr->diet);
												@endphp
												<div class="form-group">
													<select name="food[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">

														<option value="{{config('constants.doesnt_matter')}}" <?php if( @$ppr->diet == "DM"){echo "selected"; } ?> value="DM" >Doesn't matter</option>
														@foreach(config('constants.food') as $rId=>$name)
															<option value="{{ $rId }}" <?php if(in_array($rId, $dt)){echo "selected";} ?>>{{ $name }}</option>
														@endforeach

													</select>
												</div>
											</div>
										</div>

										<div class="row f_padding">
											<div class="col-sm-3">
												<label>Drink</label>
											</div>
											<div class="col-sm-9">
												@php
													$dr = explode(",", @$ppr->drink);
												@endphp
												<div class="form-group">
													<select name="drink[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">

														<option value="{{config('constants.doesnt_matter')}}" <?php if( @$ppr->drink == "DM"){echo "selected"; } ?> value="DM" >Doesn't matter</option>
														@foreach(config('constants.drink') as $rId=>$name)
															<option value="{{ $rId }}" <?php if(in_array($rId, $dr)){echo "selected";} ?>>{{ $name }}</option>
														@endforeach

													</select>
												</div>
											</div>
										</div>

										<div class="row f_padding">
											<div class="col-sm-3">
												<label>Smoke</label>
											</div>

											<div class="col-sm-9">
												@php
													$sm = explode(",", @$ppr->smoke);
												@endphp
												<div class="form-group">
													<select name="smoke[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">

														<option value="{{config('constants.doesnt_matter')}}" <?php if( @$ppr->smoke == "DM"){echo "selected"; } ?> value="DM" >Doesn't matter</option>
														@foreach(config('constants.smoke') as $rId=>$name)
															<option value="{{ $rId }}" <?php if(in_array($rId, $sm)){echo "selected";} ?>>{{ $name }}</option>
														@endforeach

													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--End Lifestyle-->
						</div>
						<button type="submit" class="btn btn-warning">Save Changes</button>
						<a href="{{ route('self-profile') }}" class="btn btn-success">Back</a>
					</form>
				</div>
			</div>
		</div>
	</div>
		<!--End Login-->
@endsection

@section('footer_scripts')
	<script>
	    var url = document.location.toString();
		if ( url.match('#') ) {
			if(url.split('#')[1]!="partnerbasic") {
				$('.' + url.split('#')[1]).trigger('click');
			}
		}

	</script>

@endsection
