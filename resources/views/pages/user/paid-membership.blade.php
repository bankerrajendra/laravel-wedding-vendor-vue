@extends('layouts.app')

@section('template_title')
    Paid Membership
@endsection
@section('navclasses')

@endsection
@section('template_fastload_css')
@endsection
@section('content')
<div class="vendor-membership">
	<div class="container">
		<div class="row">
			<div class="membser-list">
				<div class="col-sm-4 panel-body hidden-lg hidden-md hidden-sm">
					<h3>Choose Your Pack</h3>
					<span class="line"></span>
					@if(!empty($packages))
						@foreach($packages as $package)
							<div class="frb-group">
								<div class="frb frb-primary">
									<input type="radio" id="radio-button-mb-{{$package->id}}" name="pack-radio-button" data-package-type="{{$package->type}}" data-with-banner="{{$package->with_banner}}" data-package-duration="{{$package->duration}}" data-package-amount="{{$package->amount}}" value="{{$package->id}}">
									<label for="radio-button-mb-{{$package->id}}">
										<span class="frb-title"> {{$currency}} {{$package->amount}} / <small>{{$package->duration}} {{$package->type}}</small></span>
									</label>
								</div>
							</div>
						@endforeach
					@endif
				</div>
				<div class="col-sm-8 panel-body">
					<h3>Muslim Wedding Plus Benefits</h3>
					<span class="line"></span>
					@if(!empty($paid_member_benefits))
						@foreach($paid_member_benefits as $benefit)
							<p style="text-align: left;">
							@if($benefit['image'] != "")
								@if (file_exists(public_path().'/img/membership-icons/'.$benefit['image']))
									<img src="{{asset('img/membership-icons/'.$benefit['image'])}}" alt="" width="24" border="0" />
								@endif
							@endif
							{{$benefit['name']}}
							</p>
						@endforeach
					@endif
				</div>
				<div class="col-sm-4 panel-body hidden-xs">
					<h3>Choose Your Pack</h3>
					<span class="line"></span>
					@if(!empty($packages))
						@foreach($packages as $package)
						<div class="frb-group">
							<div class="frb frb-primary">
								<input type="radio" id="radio-button-{{$package->id}}" name="pack-radio-button" data-package-type="{{$package->type}}" data-with-banner="{{$package->with_banner}}" data-package-duration="{{$package->duration}}" data-package-amount="{{$package->amount}}" value="{{$package->id}}">
								<label for="radio-button-{{$package->id}}">
									<span class="frb-title"> {{$currency}} {{$package->amount}} / <small>{{$package->duration}} {{$package->type}}@if($package->with_banner == "1") with Banner @endif</small></span>
								</label>
							</div>
						</div>
						@endforeach
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="membser-list" id="payment-section" style="display: none;">
				<br>
				<div class="col-sm-8 panel-body">
					<h3>Select Payment Options</h3>
					<span class="line"></span>
					<ul class="nav nav-pills">
						@if($helcim_status == 1)
						<li>
							<a href="#helcim-payment" data-toggle="modal">Credit/Debit card</a>
						</li>
						<!-- Modal -->
						<div id="helcim-payment" class="modal fade" role="dialog" style="background: #fff;display: none;">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content" style="box-shadow:none; border:none;">
									<div class="modal-body">
										<div class="row">
											<div class="col-sm-10 col-sm-offset-1">
												<button type="button" class="close" style="font-size: 34px; text-shadow:none;opacity:1;background: none;color: #000;" data-dismiss="modal">×</button>
												<br>
												<br>
												<form method="post" action="{{route('handle-helcim-payment-submission')}}" name="frm-handle-helcim-payment-submission">
													@csrf
													<input type="hidden" name="plan_name" class="plan-name-hidden" value="{{$plan_name}}" />
													<input type="hidden" name="plan_id" class="plan-id-hidden" value="{{$plan_id}}" />
													<input type="hidden" name="package_amount" class="package-amount-hidden" value="" />
													<input type="hidden" name="package_duration" class="package-duration-hidden" value="" />
													<input type="hidden" name="package_duration_type" class="package-duration-type-hidden" value="" />
													<input type="hidden" name="package_with_banner" class="package-with-banner-hidden" value="" />
													<h3>Credit/Debit Card</b>  <img src="{{ asset('img/payment.png') }}" class="pull-right" style="display:inline-block;"></h4>
													<div class="row">
														<div class="col-xs-12 form-group">
															<label class="control-label" for="country">Country</label>
															<select class="form-control dynamic_diff" name="country" dependent="state" id="country">
																@foreach($countries as $ctr)
																	<option value="{{ $ctr->id }}" {{($errors->any()?old('country_code'):$userProfile->country_id) == $ctr->id ? "selected":"" }}>{{$ctr->name}}</option>
																@endforeach
															</select>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 form-group">
															<input name="card-number" class="form-control card-number" type="text" placeholder="Card Number">
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 form-group ">
															<label class="control-label">Expiry Date</label>
															<input name="card-expiry-month-yy" class="form-control card-expiry-month-yy" placeholder="MM/YY" type="text">
														</div>
														<div class="col-xs-6 form-group">
															<label class="control-label">&nbsp;</label>
															<div class="input-group">
															<input name="cvv-cvc" type="tel" class="form-control" placeholder="CVV/CVC">
															<span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
														</div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 form-group">
															<input name="first-name" class="form-control first-name" placeholder="First Name">
														</div>
														<div class="col-xs-6 form-group">
															<input name="last-name" class="form-control last-name" placeholder="Last Name">
														</div>
													</div>

													<div class="row">
														<div class="col-xs-5 form-group">
															<label class="control-label" for="phone-country-code">Country Code</label>
															<select class="form-control" id="phone-country-code" name="phone-country-code">
															@foreach($countries as $ctr)
																<option value="{{ $ctr->id }}" {{(($errors->any()?old('phone_country_code'):@$userProfile->users_information['country_code']) == $ctr->id)?'selected':''}}>+{{$ctr->phonecode}}  ({{$ctr->name}})</option>
															@endforeach
															</select>
														</div>
														<div class="col-xs-7 form-group">
															<label class="control-label">&nbsp;</label>
															<input name="mobile-number" class="form-control mobile-number" placeholder="Mobile Number" type="text" value="{{ $errors->any()?old('mobile_number'):$userProfile->mobile_number }}">
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 form-group">
															<label class="control-label">Billing Address</label>
															<input name="billing-add-1" class="form-control billing-add-1" type="text" placeholder="Address Line 1">
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 form-group">
															<input name="billing-add-2" class="form-control billing-add-2" type="text" placeholder="Address Line 2">
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 form-group">
															<input name="billing-city" class="form-control billing-city" type="text" placeholder="City">
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 form-group">
															<select class="form-control billing-state" name="billing-state" id="state">
															@foreach(getStateByCountry(($errors->any()?old('country_code'):$userProfile->country_id)) as $ct)
																<option value="{{ $ct->id }}" {{ ($errors->any()?old('state_id'):$userProfile->state_id) == $ct->id ? "selected":"" }}>{{ $ct->name }}</option>
															@endforeach
															</select>
															<i class='state-dropdown-loader' style="display: none;">loading...</i>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 form-group">
															<input name="zip-code" class="form-control zip-code" size="20" type="text" placeholder="Zip/Postal Code">
														</div>
													</div>
													<div class="row">
														<div class="col-md-12 form-group">
															<div class="checkbox">
																<label><input name="conf-complete-purchase" type="checkbox" value="1">By Clicking on "Complete Purchase" you agree to the SNV Wedding Gallery Terms of Service and Refund Policy.</label>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12 form-group">
															<button class="btn btn-default" type="submit">Complete Purchase »</button>
															<br>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endif
						@if($merrco_status == 1)
						<li>
							<a href="#merrco-payment" data-toggle="modal">Credit card</a>
						</li>
						<!-- Modal -->
						<div id="merrco-payment" class="modal fade" role="dialog" style="background: #fff;display: none;">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content" style="box-shadow:none; border:none;">
									<div class="modal-body">
										<div class="row">
											<div class="col-sm-10 col-sm-offset-1">
												<button type="button" class="close" style="font-size: 34px; text-shadow:none;opacity:1;background: none;color: #000;" data-dismiss="modal">×</button>
												<br>
												<br>
												<form method="post" action="{{route('handle-merrco-payment-submission')}}" name="frm-handle-merrco-payment-submission">
												@csrf
												<input type="hidden" name="plan_name" class="plan-name-hidden" value="{{$plan_name}}" />
												<input type="hidden" name="plan_id" class="plan-id-hidden" value="{{$plan_id}}" />
												<input type="hidden" name="package_amount" class="package-amount-hidden" value="" />
												<input type="hidden" name="package_duration" class="package-duration-hidden" value="" />
												<input type="hidden" name="package_duration_type" class="package-duration-type-hidden" value="" />
												<input type="hidden" name="package_with_banner" class="package-with-banner-hidden" value="" />
												<h3>Credit Card</b>  <img src="{{ asset('img/payment1.png') }}" class="pull-right" style="display:inline-block;"></h4>
												<div class="row">
													<div class="col-xs-12 form-group card">
														<label class="control-label" for="country_merrco">Country</label>
														<select class="form-control dynamic_diff" name="country" dependent="state_merrco" id="country_merrco">
															@foreach($countries as $ctr)
																<option value="{{ $ctr->id }}" {{($errors->any()?old('country_code'):$userProfile->country_id) == $ctr->id ? "selected":"" }}>{{$ctr->name}}</option>
															@endforeach
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 form-group">
														<input class="form-control card-number" name="card-number" size="20" type="text" placeholder="Card Number">
													</div>
												</div>
												<div class="row">
													<div class="col-xs-6 form-group">
														<label class="control-label">Expiry Date</label>
														<input name="card-expiry-month-yy" class="form-control card-expiry-month-yy" placeholder="MM/YY" type="text">
													</div>
													<div class="col-xs-6 form-group">
														<label class="control-label">&nbsp;</label>
														<div class="input-group">
															<input name="cvv-cvc" type="tel" class="form-control" placeholder="CVV/CVC">
															<span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-6 form-group">
														<input name="first-name" class="form-control first-name" placeholder="First Name">
													</div>
													<div class="col-xs-6 form-group">
														<input name="last-name" class="form-control last-name" placeholder="Last Name">
													</div>
												</div>

												<div class="row">
													<div class="col-xs-5 form-group">
														<label class="control-label" for="phone-country-code-merrco">Country Code</label>
														<select class="form-control" id="phone-country-code-merrco" name="phone-country-code">
														@foreach($countries as $ctr)
															<option value="{{ $ctr->id }}" {{(($errors->any()?old('phone_country_code'):@$userProfile->users_information['country_code']) == $ctr->id)?'selected':''}}>+{{$ctr->phonecode}}  ({{$ctr->name}})</option>
														@endforeach
														</select>
													</div>
													<div class="col-xs-7 form-group">
														<label class="control-label">&nbsp;</label>
														<input name="mobile-number" class="form-control mobile-number" placeholder="Mobile Number" type="text" value="{{ $errors->any()?old('mobile_number'):$userProfile->mobile_number }}">
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 form-group">
														<label class="control-label">Billing Address</label>
														<input name="billing-add-1" class="form-control billing-add-1" type="text" placeholder="Address Line 1">
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 form-group">
														<input name="billing-add-2" class="form-control billing-add-2" type="text" placeholder="Address Line 1">
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 form-group">
														<input name="billing-city" class="form-control billing-city" type="text" placeholder="City">
													</div>
												</div>
												<div class="row">	
													<div class="col-xs-12 form-group">
														<select class="form-control billing-state" name="billing-state" id="state_merrco">
														@foreach(getStateByCountry(($errors->any()?old('country_code'):$userProfile->country_id)) as $ct)
															<option value="{{ $ct->id }}" {{ ($errors->any()?old('state_id'):$userProfile->state_id) == $ct->id ? "selected":"" }}>{{ $ct->name }}</option>
														@endforeach
														</select>
														<i class='state_merrco-dropdown-loader' style="display: none;">loading...</i>
													</div>
												</div>
												<div class="row">	
													<div class="col-xs-12 form-group">
													<input name="zip-code" class="form-control zip-code" size="20" type="text" placeholder="Zip/Postal Code">
													</div>
												</div>
												<div class="row">
													<div class="col-md-12 form-group">
														<div class="checkbox">
															<label><input name="conf-complete-purchase" type="checkbox" value="1">By Clicking on "Complete Purchase" you agree to the SNV Wedding Gallery Terms of Service and Refund Policy.</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12 form-group">
														<button class="btn btn-default" type="submit">Complete Purchase »</button>
														<br>
													</div>
												</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endif
						@if($paypal_status == 1)
						<li>
							<a href="javascript:;" onclick="javascript:jQuery('#pay-with-paypal-frm').submit();" title="">Paypal</a>
							<form name="pay-with-paypal" id="pay-with-paypal-frm" action="{{route('handle-paypal-payment-submission')}}" method="post">
								@csrf
								<input type="hidden" name="plan_name" class="plan-name-hidden" value="{{$plan_name}}" />
								<input type="hidden" name="plan_id" class="plan-id-hidden" value="{{$plan_id}}" />
								<input type="hidden" name="package_amount" class="package-amount-hidden" value="" />
								<input type="hidden" name="package_duration" class="package-duration-hidden" value="" />
								<input type="hidden" name="package_duration_type" class="package-duration-type-hidden" value="" />
								<input type="hidden" name="package_with_banner" class="package-with-banner-hidden" value="" />
							</form>
						</li>
						@endif
					</ul>
				</div>
				<div class="col-sm-4 panel-body">
					<h3>Order Information</h3>
					<span class="line"></span>
					<table class="table">
						<tbody>
							<tr>
								<td><b>Membership Type</b></td>
								<td>{{$plan_name}}</td>
							</tr>
							<tr>
								<td><b>Duration</b></td>
								<td id="pack_duration"></td>
							</tr>
							<tr>
								<td><b>Total Amount</b></td>
								<td>{{$currency}}<span id="pack_amount"></span>/-</td>
							</tr>		
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="text-center col-md-4 panel-body col-md-offset-4">
				<h4><a href="{{$userProfile->getVendorProfileLink()}}">Skip &gt;&gt;</a></h4>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footer_scripts')
<script>
$(function(){
	$('input[name="pack-radio-button"]').on('click', function() {
		$("#payment-section").show();
		if($(this).data("withBanner") == 1) {
			$("#pack_duration").html($(this).data("packageDuration")+' '+$(this).data("packageType")+' with Banner');
		} else {
			$("#pack_duration").html($(this).data("packageDuration")+' '+$(this).data("packageType"));
		}
		$("#pack_amount").html($(this).data("packageAmount"));
		// Place the amount, duration, type and with banner to hidden field of payment form
		$('.package-amount-hidden').val($(this).data("packageAmount"));
		$('.package-duration-hidden').val($(this).data("packageDuration"));
		$('.package-duration-type-hidden').val($(this).data("packageType"));
		$('.package-with-banner-hidden').val($(this).data("withBanner"));
	});
});
$(document).ready(function(){
	$("#term_and_conditions-error").css({"color":"rgb(169, 68, 66) !important"});

	$('.dynamic_diff').change(function(){
		if($(this).val() != '')
		{
			var select = $(this).attr("id");
			var value = $(this).val();
			var dependent = $(this).attr('dependent');
			var _token = $('input[name="_token"]').val();
			var output;
			if (dependent == "state_merrco") {
				$('#state_merrco').html('<option value="">Select State</option>');
			}
			if (dependent == "state") {
				$('#state').html('<option value="">Select State</option>');
			} 
			if (dependent == "city") {
				$('#city').html('<option value="">Select City</option>');
			} 
			$.ajax({
				url:"{{ route('ajax.fetchLocation') }}",
				method:"POST",
				data:{select:select, value:value, _token:_token, dependent:dependent},
				beforeSend: function () {
					// show loading image and disable the submit button
					if($('.'+dependent+'-dropdown-loader').length > 0) {
						$('.'+dependent+'-dropdown-loader').show();
					}
					$('#'+dependent).attr('disabled', true);
				},
				success:function(result)
				{
					for(var i=0; i<result.length; i++){
						output += "<option value="+result[i].id+">"+result[i].value+"</option>";
					}
					$('#'+dependent).append(output);
				},
				complete: function () {
					// show loading image and disable the submit button
					if($('.'+dependent+'-dropdown-loader').length > 0) {
						$('.'+dependent+'-dropdown-loader').hide();
					}
					$('#'+dependent).removeAttr('disabled');
				}

			})
		}
	});
	
});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection