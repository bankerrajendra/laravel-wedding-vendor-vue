@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('template_linked_css')
    
@endsection
@section('content')
    <div class="container">
        @if($topBanners != null)
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
            <ol class="carousel-indicators">
                @php
                    $b = 0;
                    $totalBanner = $topBanners->count();
                @endphp
                @for($b=0;$b<$totalBanner;$b++)
                    <li data-target="#myCarousel" data-slide-to="{{$b}}" @if($b == 0)class="active"@endif></li>
                @endfor
            </ol>
            
            <!-- Wrapper for slides -->
            
            <div class="carousel-inner">
                @php
                    $ban = 0;    
                @endphp
                @foreach ($topBanners as $banner)
                    <div class="item @if($ban == 0) active @endif">
                        <img src="{{$banner->getCoverPic()}}" class="img-responsive" alt="{{$banner->event_name}}">
                    </div>
                @php
                    $ban++;   
                @endphp
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-sm-10">
                <h3>Featured Artists</h3>
                <span class="h-line"></span>
            </div>
            <div class="col-sm-2 text-right">
                <div class="form-group">
                  <select class="form-control" id="country-select" name="country">
                    <option>Select Country</option>
                    @foreach($countries as $country)
                        <option value="{{$country->id}}" {{$country_id == $country->id ? "selected":"" }} id="{{$country->id}}">{{$country->name}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
        </div>
        <br>
        @if($artists != null)
        <div class="row">
            @foreach ($artists as $artist)
                <div class="col-sm-3">
                    {{-- <a href=""> --}}
                        <div class="img-text">
                            <img src="{{$artist->getImage()}}" class="img-responsive">
                            <div class="ven_head">
                                <h5>{{$artist->name}}</h5>
                            </div>
                        </div>
                    {{-- </a> --}}
                </div>
            @endforeach
        </div>
        @endif
    </div>
    
    <div class="container f-artist">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-8">
                        <h3>Featured Events</h3>
                        <span class="h-line"></span>
                    </div>
                    <div class="col-sm-4 text-right">
                        <h5><a href="{{route('search-events')}}"><b>View All Featured Events >></b></a></h5>
                    </div>
                </div>
                @if($recentFeaturedEvents != null)
                <div class="row">
                    @foreach ($recentFeaturedEvents as $feaEvent)
                    @php
                        $start_date = new \DateTime($feaEvent->start_date);
                    @endphp
                        <div class="col-sm-3">
                            <div class="sim-p e-column">
                                <a href="{{$feaEvent->getProfileLink()}}"><img src="{{$feaEvent->getProfilePic()}}" class="img-responsive" style="width:100%;height: 108px;">
                                <h3>{{date('M d', $start_date->getTimestamp())}},  {{showReadMore($feaEvent->city->name, 8, '')}}</h3>
                                <h4>{{showReadMore($feaEvent->event_name, 22, '')}}</h4>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
$("#country-select").on('change', function(){
    window.location.href = "{{route('event-landing')}}/"+$(this).val();
});
</script>
@endsection