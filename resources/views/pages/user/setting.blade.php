@extends('layouts.app')

@section('template_title')
    {{ $user->name }}'s' Setting
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection
@section('content')
    <div id="page-content-wrapper">
        <section>
        @include('partials.setting-navigation-mobile')
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-s " style="margin-top: 0px;">
                        <div class="row">
                            @include('partials.user-sidebar-navigation')
                            <div class="col-sm-8 col-sm-offset-4 panel-body menu">
                                @include('partials.setting-navigation')
                                @if(session('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{session('success')}}
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="panel panel-body partner">
                                    <h4>Partner Preferences</h4>
                                    <hr>

                                    <form method="post" action="{{route('save-partner-preferences')}}" id="partnerPreferencesForm">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Age :</label>
                                            </div>
                                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-5">
                                                <div class="form-group">
                                                    <select class="form-control" name="age_from">
                                                        @for($i=config('constants.partner_preference.start_age'); $i<=config('constants.partner_preference.end_age'); $i++)
                                                            <option value="{{$i}}" @if($current_preferences) @if($current_preferences->age_from == $i) selected="selected" @endif  @elseif(old('age_from') == $i ) selected="selected" @endif >{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-4 col-sm-4 col-xs-2">
                                                <div class="form-group text-center">
                                                    <b>To</b>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-5">
                                                <div class="form-group">
                                                    <select class="form-control" name="age_to">
                                                        @for($i=config('constants.partner_preference.start_age'); $i<=config('constants.partner_preference.end_age'); $i++)
                                                            <option value="{{$i}}" @if($current_preferences) @if($current_preferences->age_to == $i) selected="selected" @endif @elseif(old('age_to') == $i ) selected="selected" @elseif($i == config('constants.partner_preference.end_age')) selected="selected" @endif >{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label for="city">City :</label>
                                                    <select class="form-control" name="city" id="city">
                                                        <option style="display: none;" value="">Select City</option>
                                                        @foreach($cities as $city)
                                                        <option value="{{$city->id}}" @if($current_preferences) @if($current_preferences->city_id == $city->id) selected="selected" @endif @elseif(old('city') == $city->id ) selected="selected" @endif >{{$city->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <button type="submit" class="btn btn-warning btn-block">SAVE</button>
                                            </div>
                                        </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p>When you run out of matches from your preferred city, you will be shown matches from other cities as well.</p>
                                            </div>
                                        </div>
                                        <hr>
                                        <h4>Notification</h4>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6 pull-left">
                                                <span class="notifi">Activity</span>
                                            </div>
                                            <div class="col-sm-6 col-xs-6 pull-right">
                                                <label class="switch">
                                                    <input type="checkbox" @if($user->get_user_information()->activity_notification == 1) checked="checked" @endif name="activity_notification" id="activity_notification" data-profileid="{{$user->getEncryptedId()}}">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>

                                        </div>
                                        <p>One email daily containing unread messages, viewed me and activities on your profile of new members when you are away.</p>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6 pull-left">
                                                <span class="notifi">Instant Alerts</span>
                                            </div>
                                            <div class="col-sm-6 col-xs-6 pull-right">
                                                <label class="switch">
                                                    <input type="checkbox" @if($user->get_user_information()->instant_alert_notification == 1) checked="checked" @endif id="instant_alert_notification" name="instant_alert_notification" data-profileid="{{$user->getEncryptedId()}}">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>

                                        </div>
                                        <p>We notify you when a member sends a message & adds you to their favorite list. </p>

                                        {{--<hr>--}}
                                        {{--<h4>Subscriptions</h4>--}}
                                        {{--<br>--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-sm-6 col-xs-6 pull-left">--}}
                                                {{--<span class="notifi">Newsletters / Special Offers</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-sm-6 col-xs-6 pull-right">--}}
                                                {{--<label class="switch">--}}
                                                    {{--<input type="checkbox">--}}
                                                    {{--<span class="slider round"></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<br>--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-sm-6 col-xs-6 pull-left">--}}
                                                {{--<span class="notifi">Attend alerts</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-sm-6 col-xs-6 pull-right">--}}
                                                {{--<label class="switch">--}}
                                                    {{--<input type="checkbox">--}}
                                                    {{--<span class="slider round"></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </form>
                                    <a class="btn btn-danger hidden-xs" href="{{'deactivate-account'}}">Deactivate Account</a>
                                </div>

                                <div class="list-group hidden-lg hidden-sm hidden-md">
                                    <a href="{{route('change-password')}}" class="list-group-item">Change Password</a>
                                    <a href="{{route('deactivate-account')}}" class="list-group-item">Deactivate Account</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
