@extends('layouts.app')

@section('template_title')
    Deactivate Account
@endsection

@section('template_fastload_css')
    .help-block1, .error-help-block1{
    margin-top: 5px;
    margin-bottom: 10px;
    color: #a94442
    }

    hr{
    border-top: 1px solid #ccc;
    }
@endsection

@section('navclasses')
    navbar-fixed-top
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6">
                <div class="register">
                    <div class="panel">
                        <h3>Delete Account</h3>
                    </div>
                    <p><b>Note:</b> We do not refund memberships. By permanently deleting your account, you will lose any unused time left on your Platinum / Diamond account.</p>
                    <div class="form-group">
                        <a href="{{route('deleting-account')}}" class="form-control btn btn-warning">DELETE ACCOUNT</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
