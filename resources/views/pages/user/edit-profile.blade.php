@extends('layouts.app')

@section('template_title')
   Edit Profile
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection

@section('content')
    <div id="page-content-wrapper">
        <section>
            @include('partials.edit-profile-navigation-mobile')
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-s " style="margin-top: 0px;">
                        <div class="row">
                            @include('partials.user-sidebar-navigation')
                            <div class="col-sm-8 col-sm-offset-4 panel-body menu menu1">
                                @include('partials.edit-profile-navigation')
                                @if(session('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{session('success')}}
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form id="edit-profile" name="edit-profile" method="post" action="{{route('post-edit-profile')}}">
                                    @csrf
                                        <div class="panel-success">
                                            <div class="panel-heading mobile-panel">
                                                <h4 class="panel-title">Basic Info </h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label">Username :</label>
                                                        <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="username" id="username" class="email form-control" value="{{$user->name}}" readonly="">
                                                            <span class="span_email errorcls" id="span_email"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label">Email :</label>
                                                        <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="email" id="email" class="email form-control" value="{{$user->email}}" readonly="">
                                                            <span class="span_email errorcls" id="span_email"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label">Ethnicity:</label>
                                                        <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="ethnicity" id="ethnicity" class="email form-control" value="{{$userInformation->ethnicity}}" readonly="">
                                                            <span class="span_email errorcls" id="span_email"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label">Phone Number :</label>
                                                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
                                                            <select class="form-control" id="phone_country_code" name="phone_country_code">
                                                                @foreach($locationRepo->getCountries() as $country)
                                                                    <option value="{{$country->phonecode}}" id="{{$country->id}}" @if($userInformation->country_code == $country->phonecode) selected="selected" @endif>+{{$country->phonecode}} ({{$country->name}})</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-9">
                                                            <input type="text" name="phone_number" id="phone_number" class="form-control" value="{{$userInformation->mobile_number}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                $date_of_birth = explode('-',$user->date_of_birth);
                                                $dob_year = $date_of_birth[0];
                                                $dob_month = $date_of_birth[1];
                                                $dob_day = $date_of_birth[2];
                                                @endphp
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label"><span class="star">*</span>Date of Birth :</label>
                                                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4">
                                                            <select name="day" id="day" class="form-control"><option value="">DD</option>
                                                                @for ($day = 1; $day <= 31; $day++)
                                                                    <option value="{{ $day }}" @if($dob_day == $day) selected="selected" @endif>{{ $day }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4">
                                                            <select name="month" id="month" class="form-control">
                                                                <option value="">MM</option>
                                                                @for ($month = 1; $month <= 12; $month++)
                                                                    <option value="{{ $month }}" @if($dob_month == $month) selected="selected" @endif>{{ $month }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4">
                                                            <select name="year" id="year" class="form-control">
                                                                <option value="">YYYY</option>
                                                                @for ($year =  (now()->year - 70); $year <= (now()->year - 18); $year++)
                                                                    <option value="{{ $year }}" @if($dob_year == $year) selected="selected" @endif>{{ $year }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-sm-4 col-xs-4 control-label">Gender : </label>
                                                        <div class="col-sm-6 col-xs-4">
                                                            <strong >@if($user->gender == 'M') Male @else Female @endif</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel-success">
                                            <div class="panel-heading mobile-panel">
                                                <h4 class="panel-title">Education &amp; Career</h4>
                                            </div>

                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-4 col-md-3 col-sm-3 col-xs-12  control-label"><span class="star">*</span>Education :</label>
                                                        <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                            <select name="education" id="education" class="form-control">
                                                                @foreach($userInfoRepo->getEducationOptions() as $option)
                                                                    <option value="{{$option->id}}" @if($userInformation->education_id == $option->id ) selected="selected" @endif>{{$option->education}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-4 col-md-3 col-sm-3 col-xs-12  control-label"><span class="star">*</span>Employment :</label>
                                                        <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                            <select name="profession" id="profession" class="form-control">
                                                                @foreach($userInfoRepo->getProfessionOptions() as $option)
                                                                    <option value="{{$option->id}}" @if($userInformation->profession_id == $option->id ) selected="selected" @endif>{{$option->profession}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel-success">
                                            <div class="panel-heading mobile-panel">
                                                <h4 class="panel-title">Location</h4>
                                            </div>

                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label">Country :</label>
                                                        <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                            <select name="country" id="country" class="form-control dynamic" data-dependent="city">
                                                                @foreach($locationRepo->getCountries() as $country)
                                                                    <option value="{{$country->id}}" @if($user->country_id == $country->id ) selected="selected" @endif>{{$country->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-4 col-md-3 col-sm-3 col-xs-12  control-label"><span class="star">*</span>City  :</label>
                                                        <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                            <select name="city" id="city" class="form-control">
                                                                @foreach($locationRepo->getCountryCities($user->country_id) as $city)
                                                                    <option value="{{$city->id}}" @if($user->city_id == $city->id ) selected="selected" @endif>{{$city->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel-success">
                                            <div class="panel-heading mobile-panel">
                                                <h4 class="panel-title">Lifestyle</h4>
                                            </div>

                                            <div class="panel-body">

                                                    <div class="form-group">
                                                        <div class="row">
                                                        <label for="food" class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label">Food Preference</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                                <select class="form-control" name="food" id="food">
                                                                    <option value="Veg" @if($userInformation->food == "Veg" ) selected="selected" @endif>Veg</option>
                                                                    <option value="Non-Veg" @if($userInformation->food == "Non-Veg" ) selected="selected" @endif>Non-Veg</option>
                                                                    <option value="Vegan" @if($userInformation->food == "Vegan" ) selected="selected" @endif>Vegan</option>
                                                                    <option value="Others" @if($userInformation->food == "Others" ) selected="selected" @endif>Others</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                        <label for="drink" class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label">Drinking</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                                <select name="drink" id="drink" class="form-control">
                                                                    <option value="Y" @if($userInformation->drink == "Y" ) selected="selected" @endif>Yes</option>
                                                                    <option value="N" @if($userInformation->drink == "N" ) selected="selected" @endif>No</option>
                                                                    <option value="S" @if($userInformation->drink == "S" ) selected="selected" @endif>Occasionally</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label for="smoke" class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label">Smoking</label>
                                                            <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                                <select name="smoke" id="smoke" class="form-control">
                                                                    <option value="Y" @if($userInformation->smoke == "Y" ) selected="selected" @endif>Yes</option>
                                                                    <option value="N" @if($userInformation->smoke == "N" ) selected="selected" @endif>No</option>
                                                                    <option value="S" @if($userInformation->smoke == "S" ) selected="selected" @endif>Occasionally</option>
                                                                </select>
                                                            </div>
                                                         </div>
                                                    </div>
                                                </div>

                                        </div>
                                    <div class="panel-success">
                                        <div class="panel-heading mobile-panel">
                                            <h4 class="panel-title">Interests</h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label for="interests" class="col-lg-4 col-md-3 col-sm-3 col-xs-12 control-label">Interests</label>
                                                    <div class="col-lg-6 col-md-9 col-sm-9 col-xs-12">
                                                        <select name="interest[]" id="interest" multiple="multiple" class="form-control">
                                                            @foreach($userInfoRepo->getInterestOptions() as $option)
                                                                <option value="{{$option->id}}" @if(in_array($option->id, $userInterests)) selected="selected" @endif>{{$option->interest}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="panel-success">
                                            <div class="panel-heading mobile-panel">
                                                <h4 class="panel-title">About Myself</h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-sm-12 control-label"><small>This section will help you make a positive impression on your potential partner</small></label>
                                                        <div class="col-sm-12">
                                                            <textarea class="form-control" rows="5" name="about" id="about" >{{$userInformation->about}}</textarea>
                                                            <small class="char_wr">(Please write atleast 50 characters.)</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-warning btn-lg"> Save Changes</button>
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer_scripts')
    <script src="{{asset('js/multi-select.js')}}"></script>
    <script type="text/javascript">
        $('select[multiple]').multiselect({
            columns: 4,
            placeholder: 'Select options'
        });
    </script>
    <script>
        $(document).ready(function(){

            $('.dynamic').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    var output;
                    $('#'+dependent).html('');
                    $("#phone-country-code option[selected='selected']").removeAttr('selected');
                    $("#phone-country-code option[id="+value+"]").attr("selected","selected");
                    $.ajax({
                        url:"{{ route('ajax.fetchLocation') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {
                            for(var i=0; i<result.length; i++){
                                output += "<option value="+result[i].id+">"+result[i].value+"</option>";
                            }
                            $('#'+dependent).append(output);
                        }

                    })
                }
            });
        });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection