@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 hidden-xs">
                    @include('partials.user-side-menu')
                </div>
                <div class="col-sm-8 msg-section">
                    <div class="row hidden-xs">
                        <div class="col-sm-12 col-xs-12"><h4>Finalized Vendors</h4></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 no_padding">
                            <div id="results-finalized-vendors">
                            @include('pages.user.ajax.show-finalized')
                            </div>
                            @if($finalized->total() > $per_page)
                            <div class="text-center" style="margin-top: 10px;">
                                <a href="javascript:void(0);" id="next-page" data-next-page="2" class="btn btn-default">Show More </a>
                                <img src="{{asset('img/loader.gif')}}" id="spinner-load" width="40" style="display: none;">
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
var ajaxResultGetRequest = null
$("#next-page").on('click', function() {
    var url = route('finalized-vendors')+'?page='+$(this).data('next-page');
    ajaxResultGetRequest = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            // show loading image
            $('#next-page').hide();
            $('#spinner-load').show();
        },
        success: function(result) {
            // show results
            $('#results-finalized-vendors').append(result.finalized);
            $('#next-page').data('next-page', result.next_page);
            if(result.next_url == '') {
                $('#next-page').hide();
            } else {
                $('#next-page').show();
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            // hide loading image
            $('#spinner-load').hide();
            ajaxResultGetRequest = null;
        }
    });
});
</script>
@endsection