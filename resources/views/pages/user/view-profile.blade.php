<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-s ">
			<div class="row">
				@include('partials.user-sidebar-navigation')
					<div class="col-sm-8 col-sm-offset-4">
						<div id="themeSlider" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								@if($userProfile->users_images->isEmpty())
									<li data-target="#themeSlider" data-slide-to="0" class="active"></li>
								@else
									@foreach($userProfile->users_images as $image)
										<li data-target="#themeSlider" data-slide-to="{{$loop->index}}" class="{{($loop->index == 0)?'active':''}}"></li>
									@endforeach
								@endif
							</ol>

							<div class="carousel-inner">
								@if($userProfile->users_images->isEmpty())
									<div class="item active">
										<img src="{{$userProfile->getVendorProfilePic()}}" alt="slide" class="img-responsive;">
									</div>
								@else
									@foreach($userProfile->users_images as $image)
										<div class="item {{($loop->index == 0)?'active':''}}">
											<img src="{{config('constants.upload_url_public').$image->image_thumb}}" alt="slide" class="img-responsive;">
										</div>
									@endforeach
								@endif


							</div>
							<!-- Left and right controls -->
							<a class="left carousel-control" href="#themeSlider" data-slide="prev">
								<span class="fa fa-chevron-left"></span>
							</a>
							<a class="right carousel-control" href="#themeSlider" data-slide="next">
								<span class="fa fa-chevron-right"></span>
							</a>
						</div>
						<div class="panel panel-body personal-detail">
							<div class="row">
								<div class="col-sm-6">
									<h4>{{$userProfile->name}} <span>{{$userProfile->getAge()}},</span> <span>{{$userProfile->city->name}}</span></h4>
									<h5>Working as {{$userProfile->users_information->profession->profession}}</h5>
									<h5>Studied {{$userProfile->users_information->education->education}}</h5>
								</div>
								<div class="col-sm-6 text-right">
									<a href="#send_message" class="btn btn-danger"> <i class="fa fa-envelope" aria-hidden="true"></i> Message</a>
									{{--<a id="afav" class="btn btn-warning"><i class="fa fa-heart-o" aria-hidden="true"></i>&nbsp;Favorite </a>--}}
								</div>
							</div>
							<a data-toggle="modal" class="btn btn-warning" data-target="#reportUser" style="margin:7px 0px;" href="#">Report  User</a>
							<div class="modal fade text-center" id="reportUser">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" id='resetPublic' value='reset'>&times;</button>
											<h4 class="modal-title" style="color:#fff;">Are you sure you want to report this user ?</h4>
										</div>

										<div class="modal-body report">
											<br>
											<form id="reportUserForm" method="post" class="form" role="form" action="" >
												<div class="row">
													<div class="col-md-12 form-group">
														<input class="form-control" id="reason" name="reason" placeholder="Reason" type="text" required autofocus />
														<span id="reason-error" class="help-block1 error-help-block1" style="display: none">The reason field is required.</span>
													</div>
													<div class="col-md-12 form-group">
														<input type="hidden" id="userId" name="userId" value="{{$userProfile->getEncryptedId()}}">
														<textarea class="form-control" id="description" name="description" placeholder="Description" style="height:100px;"></textarea>
													</div>
												</div>
												<div class="row">
													<div class="col-xs-12 col-md-12 form-group">
														<button type="submit" class="btn btn-danger"> Submit</button>&nbsp;&nbsp;
														<button type="button" class="btn btn-setting" class="close" data-dismiss="modal"> Cancel</button>
													</div>
												</div>
											</form>
										</div>

									</div>
								</div>
							</div>
							<div class="modal fade text-center" id="reportSuccess">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" id='resetPublic1' value='reset'>&times;</button>
											<h4 class="modal-title" style="color:#fff;">Thank You!</h4>
										</div>

										<div class="modal-body report">
											<br>
											<p>Your report has been submitted successfully. Admin will review it and will take required action.</p>
											<div class="row">
												<div class="col-xs-12 col-md-12 form-group">
													<button type="button" class="btn btn-setting" class="close" data-dismiss="modal">Okay</button>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
							<br>
							<h6>About Me</h6>
							<hr>
							<p>{{$userProfile->users_information->about}}<p>
								<br>
							<h6>Interests</h6>
							<hr>
							<div class="row">
								@foreach($userProfile->interests as $interest)
									<div class="col-sm-3">
										<h5>{{$interest->interest}}</h5>
									</div>
								@endforeach
							</div>
							<br>
							<h6>LIFE STYLE</h6>
							<hr>
							<div class="row">
								<div class="col-md-3 col-sm-6 col-xs-6">
									<h5> Drink :</h5>
								</div>
								<div class="col-md-2 col-sm-6 col-xs-6">
									<h5>{{(($userProfile->users_information->drink=='Y')?'Yes':(($userProfile->users_information->drink=='N')?'No':'Occasionally'))}}</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-sm-6 col-xs-6">
									<h5> Smoke:</h5>
								</div>
								<div class="col-md-2 col-sm-6 col-xs-6">
									<h5>{{(($userProfile->users_information->smoke=='Y')?'Yes':(($userProfile->users_information->smoke=='N')?'No':'Occasionally'))}}</h5>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3 col-sm-6 col-xs-6">
									<h5> Food Preference  :</h5>
								</div>
								<div class="col-md-2 col-sm-6 col-xs-6">
									<h5>{{$userProfile->users_information->food}}</h5>
								</div>
							</div>
						</div>
						<div class="panel panel-body personal-detail" id="send_message" style="margin-bottom: 85px;">
							<h6>SEND A QUICK MESSAGE</h6>
							<form method="POST" action="" id="messageSendForm">
								<div class="form-group">
									<textarea cols="54" rows="5" class="form-control sendMessageInput" name="message"  id="sendMessageInput{{$userProfile->id}}" @if($userProfile->banned == 1 || $userProfile->deactivated == 1) disabled="disabled" @endif  placeholder="Write Your Message.." data-profileid="{{$userProfile->getEncryptedId()}}" data-id="{{$userProfile->id}}" ></textarea>
								</div>
								<div class="pull-right">
									<button type="submit" id="sendMessage{{$userProfile->id}}" @if($userProfile->banned == 1 || $userProfile->deactivated == 1) disabled="disabled" @endif   class="btn btn-danger sendMessage" data-profileid="{{$userProfile->getEncryptedId()}}" data-id="{{$userProfile->id}}">SEND</button>
								</div>
								<br>
								<br>
								<br>
								<br>
							</form>
						</div>
						<div class="popuptext" id="myPopup">
							<div class="personal-detail-button2 text-center">
								<span><i class="fa fa-close"></i></span>
							</div>
						</div>
						<div class="popuptext1" id="myPopup1">
							<div class="personal-detail-button3 text-center">
								<span><i class="fa fa-check"></i></span>
							</div>
						</div>
						<div class="personal-detail-button">
							<div class="container">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-1">
										<div class="row">
											<div class="col-sm-4">
											</div>
											@if(!$userProfile->isSkipped() && !$userProfile->isLiked() && !$userProfile->isLikedBack())
												<div class="col-sm-8">
													<div class="personal-detail-button1">
														<div class="row">
															<div class="col-sm-4  col-xs-5">
																<button type="button" class="btn btn-success btn-block" id="shortlistProfile" data-profileid="{{$userProfile->getEncryptedId()}}" @if($userProfile->isShortlisted()) disabled="disabled" @endif>@if($userProfile->isShortlisted())Shortlisted @else Shortlist  @endif</button>
															</div>
															<div class="col-sm-4  col-xs-2">
																&nbsp;
															</div>
															<div class="col-sm-4  col-xs-5">
																<button type="button" class="btn btn-primary btn-block likeProfile" id="likeProfile" data-profileid="{{$userProfile->getEncryptedId()}}" data-reloadpage="true"><i class="fa fa-check" aria-hidden="true"></i> {{$userProfile->getLikeText()}}</button>
															</div>
														</div>
													</div>
												</div>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
