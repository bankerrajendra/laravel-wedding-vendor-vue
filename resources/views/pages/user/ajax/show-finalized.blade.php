@if($finalized->total() > 0)
    @foreach ($finalized as $vendor)
        <div class="media" id="shortlist-id-{{$vendor->to_user()->getEncryptedId()}}">
            <div class="media-left">
                <a href="{{$vendor->to_user()->getVendorProfileLink()}}">
                    <img src="{{$vendor->to_user()->getVendorProfilePic()}}" alt="" class="media-object img-thumbnail" style="width:80px">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">
                    <a href="{{$vendor->to_user()->getVendorProfileLink()}}" class="msg-name">{{$vendor->to_user()->first_name}} {{$vendor->to_user()->last_name}}</a>
                    <div class="dropdown pull-right">
                        <button class="btn btn-note dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Send Message</a></li>
                            <li><a href="javascript:void(0);" onclick="javascript:doDeleteShortListed('{{$vendor->to_user()->getEncryptedId()}}');">Delete</a></li>
                        </ul>
                    </div>
                </h4> 
                <p><small>{{$vendor->to_user()->state->name}}, {{$vendor->to_user()->country->name}}</small></p>
                <p class="rating">
                    @php
                        $average_rating = $vendor->to_user()->getAverageReviewRating();
                        $total_review = $vendor->to_user()->getTotalReviewVendor();
                    @endphp
                    
                    @for($r=1;$r<=5;$r++)
                        <i class="fa @if($r <= $average_rating) fa-star @elseif($r >= $average_rating-0.5 && $r <= $average_rating+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                    @endfor
                    {{$total_review}} @if( $total_review > 1) Rating @else Rating @endif
                </p>
            </div>
        </div>    
    @endforeach
@endif