@if(isset($reviews['reviews']) && count($reviews['reviews']) > 0)
    @foreach ($reviews['reviews'] as $review)
        <div class="media">
            <img class="media-object img-circle" src="{{$review['profile_picture']}}" width="80" height="80" alt="">
            <h4 class="media-heading">{{$review['name']}}</h4>
            <ul class="list-inline list-unstyled">
                <li>
                    @for($rt = 1;$rt <= 5; $rt++)
                        <span class="fa @if($rt <= $review['rating']) fa-star @else fa-star-o @endif" aria-hidden="true"></span>
                    @endfor
                </li>
                <li>|</li>
                <li><span><i class="fa fa-clock-o" aria-hidden="true"></i> {{$review['created_at']}} </span></li>
            </ul>
            <p>{{$review['message']}}</p>
        </div>
    @endforeach
@endif