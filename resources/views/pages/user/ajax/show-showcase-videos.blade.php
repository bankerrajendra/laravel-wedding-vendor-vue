@if(isset($showcaseVideosArray) && count($showcaseVideosArray) > 0)
    @foreach ($showcaseVideosArray as $record)
        <div class="col-sm-4 panel-body">
            <div class="border-s">
                <iframe style="width:100%; height: 227px;" src="https://www.youtube.com/embed/{{$record['embed_code']}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                {{-- <a href="{{$record['vendor_profile_link']}}"><div class="embed-video" data-source="youtube" data-video-url="{{$record['embed_code']}}" data-cc-policy="1"></div></a> --}}
                <h3 class="p_name"><a href="{{$record['vendor_profile_link']}}">{{$record['video_title']}}</a></h3>
                <span>
                    @for($r=1;$r<=5;$r++)
                    <i class="fa @if($r <= $record['average_rating']) fa-star @elseif($r >= $record['average_rating']-0.5 && $r <= $record['average_rating']+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                    @endfor
                </span>
                <span class="pull-right">
                    @if($record['allow_shortlist'] == 1)
                    @if($record['shortlisted'] == 0)<a id="short-list-link-{{$record['vendor_enc_id']}}" @if(!Auth::check()) href="{{ route('login') }}?redirect={{urlencode($record['vendor_profile_link'])}}" @else href="javascript:void(0);" onclick="javascript:shortListVendor('{{$record['vendor_enc_id']}}');" @endif class="heart-o">@endif<i class="fa @if($record['shortlisted'] == 0)fa-heart-o @else fa-heart @endif" aria-hidden="true" @if($record['shortlisted'] == 1) style="color:red;" @endif></i>@if($record['shortlisted'] == 0)</a>@endif
                    @endif
                </span>
                <p class="p_content">
                    {{$record['description']}}
                </p>
            </div>
        </div>
    @endforeach
@endif