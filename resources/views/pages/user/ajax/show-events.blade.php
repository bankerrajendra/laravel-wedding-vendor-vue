@if($events->total() > 0)
    @foreach ($events as $event)
        <div class="s-result">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media">
                        <div class="media-left">
                            <a href="{{$event->getProfileLink()}}">
                                @if(@$event->getProfilePic())
                                    <img src="{{$event->getProfilePic()}}" class="img-rec" alt="" />
                                @endif
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="{{$event->getProfileLink()}}">{{$event->event_name}}</a></h4>
                            <div class="item-subtitle">
                                @php
                                    $average_rating = $event->getAverageReviewRating();
                                    $total_review = $event->getTotalReview();
                                @endphp
                                
                                @for($r=1;$r<=5;$r++)
                                    <i class="fa @if($r <= $average_rating) fa-star @elseif($r >= $average_rating-0.5 && $r <= $average_rating+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                                @endfor
                                {{$total_review}} @if( $total_review > 1) Reviews @else Review @endif
                            </div>
                            
                            <div class="item-subtitle">
                            <span>{{$event->city->name}},</span> <span>{{$event->state->name}},</span> <span>{{$event->country->name}}</span> - <span>{{$event->zip}}</span></div>
                            
                            <p class="text-justify">{{showReadMore(strip_tags($event->description), 200, '')}}</p>
                            @php
                                $messageHeader = $firstName = $lastName = $emailFld = $mobileNumber = '';
                                $messageHeader = $event->event_name;
                                if(!empty($form_prepopulate) && $form_prepopulate['first_name'] != "") {
                                    $firstName = $form_prepopulate['first_name'];
                                }
                                if(!empty($form_prepopulate) && $form_prepopulate['last_name'] != "") {
                                    $lastName = $form_prepopulate['last_name'];
                                }
                                if(!empty($form_prepopulate) && $form_prepopulate['mobile_number'] != "") {
                                    $mobileNumber = $form_prepopulate['mobile_number'];
                                }
                                if(!empty($form_prepopulate) && $form_prepopulate['email'] != "") {
                                    $emailFld = $form_prepopulate['email'];
                                }
                                $vendor_id = $event->vendor->getEncryptedId();
                                $event_id = $event->getEncryptedId();
                            @endphp
                            @if($allowSendMessage == true)
                            <a href="javascript:void(0);" onclick="javascript:showMessageModal( '{{$vendor_id}}','{{$event_id}}', '{{$messageHeader}}', '{{$firstName}}', '{{$lastName}}', '{{$emailFld}}', '{{$mobileNumber}}' );" class="btn btn-default pull-right req-btn">Message</a>
                            @endif
                        </div>
                        @if($allowSendMessage == true)
                        <a href="javascript:void(0);" onclick="javascript:showMessageModal( '{{$vendor_id}}','{{$event_id}}', '{{$messageHeader}}', '{{$firstName}}', '{{$lastName}}', '{{$emailFld}}', '{{$mobileNumber}}' );" class="btn btn-default btn-block mob-req-btn">Message</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="no-result">
        <div class="text-center">
            <i class="fa fa-frown-o" aria-hidden="true"></i>
            <h2>Oops!</h2>
            <h1>No results, try another search.</h1>
        </div>
    </div>
@endif