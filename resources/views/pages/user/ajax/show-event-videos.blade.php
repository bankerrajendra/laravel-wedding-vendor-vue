@if($eventVids->total() > 0)
    @foreach ($eventVids as $record)
        @php
            $embed_code = GetYouTubeId($record->video_url);
            $average_rating = $record->getAverageReviewRating();
        @endphp
        <div class="col-sm-4 panel-body">
            <div class="border-s">
                <iframe style="width:100%; height: 227px;" src="https://www.youtube.com/embed/{{$embed_code}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                
                <h3 class="p_name"><a href="{{$record->getProfileLink()}}">{{showReadMore($record->event_name, 22, '')}}</a></h3>
                <span>
                    @for($r=1;$r<=5;$r++)
                    <i class="fa @if($r <= $average_rating) fa-star @elseif($r >= $average_rating-0.5 && $r <= $average_rating+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                    @endfor
                </span>
                <p class="p_content">
                    {{showReadMoreApi(strip_tags($record->description), 80, '')}}
                </p>
            </div>
        </div>
    @endforeach
@endif