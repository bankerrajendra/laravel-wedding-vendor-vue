@if($vendors->total() > 0)
    @foreach ($vendors as $vendor)
        <div class="s-result">
            <div class="row">
                <div class="col-sm-12">
                    <div class="media">
                        <div class="media-left">
                            <a href="{{$vendor->getVendorProfileLink()}}">
                                @if(@$vendor->getVendorProfilePic())
                                    <img src="{{$vendor->getVendorProfilePic()}}" class="img-rec" alt="" />
                                @endif
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="{{$vendor->getVendorProfileLink()}}">{{$vendor->vendors_information->company_name}}</a></h4>
                            <div class="item-subtitle">
                                @php
                                    $average_rating = $vendor->getAverageReviewRating();
                                    $total_review = $vendor->getTotalReviewVendor();
                                @endphp
                                
                                @for($r=1;$r<=5;$r++)
                                    <i class="fa @if($r <= $average_rating) fa-star @elseif($r >= $average_rating-0.5 && $r <= $average_rating+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                                @endfor
                                {{$total_review}} @if( $total_review > 1) Reviews @else Review @endif
                            </div>
                            
                            <div class="item-subtitle">
                            <span>{{$vendor->city->name}},</span> <span>{{$vendor->state->name}},</span> <span>{{$vendor->country->name}}</span> - <span>{{$vendor->zip}}</span></div>
                            
                            <p class="text-justify">{{showReadMore(strip_tags($vendor->vendors_information->business_information), 200, '')}}</p>
                            @php
                                $messageHeader = $firstName = $lastName = $emailFld = $mobileNumber = $vendorCategory = '';
                                $messageHeader .= $vendor->vendors_information->company_name;
                                if(!empty($form_prepopulate) && $form_prepopulate['first_name'] != "") {
                                    $firstName = $form_prepopulate['first_name'];
                                }
                                if(!empty($form_prepopulate) && $form_prepopulate['last_name'] != "") {
                                    $lastName = $form_prepopulate['last_name'];
                                }
                                if(!empty($form_prepopulate) && $form_prepopulate['mobile_number'] != "") {
                                    $mobileNumber = $form_prepopulate['mobile_number'];
                                }
                                if(!empty($form_prepopulate) && $form_prepopulate['email'] != "") {
                                    $emailFld = $form_prepopulate['email'];
                                }
                                if($vendor->vendors_information->vendor_category != '') {
                                    $vendorCategory = $vendor->getCategoryVendor()->name;
                                }
                            @endphp
                            @if($allowSendMessage == true)
                            <a href="javascript:void(0);" onclick="javascript:showMessageModal( '{{$vendor->getEncryptedId()}}','{{$messageHeader}}', '{{$firstName}}', '{{$lastName}}', '{{$emailFld}}', '{{$mobileNumber}}', '{{$vendorCategory}}' );" class="btn btn-default pull-right req-btn">Request Pricing</a>
                            @endif
                        </div>
                        @if($allowSendMessage == true)
                        <a href="javascript:void(0);" onclick="javascript:showMessageModal( '{{$vendor->getEncryptedId()}}','{{$messageHeader}}', '{{$firstName}}', '{{$lastName}}', '{{$emailFld}}', '{{$mobileNumber}}', '{{$vendorCategory}}' );" class="btn btn-default btn-block mob-req-btn">Request Pricing</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="no-result">
        <div class="text-center">
            <i class="fa fa-frown-o" aria-hidden="true"></i>
            <h2>Oops!</h2>
            <h1>No results, try another search.</h1>
        </div>
    </div>
@endif