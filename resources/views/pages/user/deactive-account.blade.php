@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="login_p signup">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h3 class="text-center"><i>DEACTIVATE ACCOUNT</i></h3>
                    <p>Deactivating your account will remove your profile and photos from the site and prevent users from messaging you. To reactivate simply log in your account later,</p>
                    <p>Reason for deactivation</p>
                    <form method="POST" action="{{route('deactivating-account')}}" id="deactivateAccount">
                        @csrf
                        <div class="form-group">
                            <div class="radio">
                                <label><input type="radio" name="rdReason" value="I found the ideal match" checked="checked"> I found the ideal match</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label><input type="radio" name="rdReason" value="I'm no longer searching for my other half"> I'm no longer searching for my other half</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label><input type="radio" name="rdReason" value="I'm taking a break from the site"> I'm taking a break from the site</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label><input type="radio" name="rdReason" value="I didn't find my ideal match"> I didn't find my ideal match</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label><input type="radio" name="rdReason" value="This is not working for me"> This is not working for me</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label><input type="radio" name="rdReason" value="Other"> Other</label>
                            </div>
                        </div>
                        <p><small>Is there anything that we could do better?</small></p>
                        <div class="form-group">
                            <label>Please enter any additional comments :</label>
                            <textarea class="form-control" id="description" name="description" style="height:100px;"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default btn-block">Delete Account</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
@endsection