@extends('layouts.app')

@section('template_title')
    Search User
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
    .modal {
        margin-top: 0px;
        background: rgba(0, 0, 0, 0.8588235294117647);
        height: 100%;
        {{--z-index: 999;--}}
    }

    .personal-detail-button{
        display: none;
        z-index: 98;
    }
    .show_b{
        display:block;
    }
@endsection
@section('template_linked_css')
    <link href="{{asset('css/select-box.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div id="page-content-wrapper">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-s " style="margin-top: 0px;">
                        <div class="row">
                            @include('partials.user-sidebar-navigation')
                            <div class="col-sm-8 col-sm-offset-4 panel-body menu">

                                @if(session('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{session('success')}}
                                    </div>
                                @endif
                                @if ($errors->any())
                                        <br/><br/>
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}. Please try again.</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                        <div class="col-md-12 col-xs-12 panel-body">
                                            <button type="button" class="btn btn-primary btn-block btn-lg launch-modal">Search by City and Age</button>
                                        </div>
                                        <!-- Modal -->
                                        <div class="modal fade search text-center" id="myModal" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Search Partner</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form name="searchForm" id="searchForm" method="post" action="{{route('search')}}">
                                                            @csrf
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Age :</label>
                                                                </div>
                                                                <div class="col-lg-5 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="form-group">
                                                                        <select class="form-control" name="age_from">
                                                                            @for($i=config('constants.partner_preference.start_age'); $i<=config('constants.partner_preference.end_age'); $i++)
                                                                                <option value="{{$i}}" @if($request->input('age_from') == $i) selected="selected" @endif  >{{$i}}</option>
                                                                            @endfor
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="form-group text-center">
                                                                        <b>To</b>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 col-md-4 col-sm-4 col-xs-4">
                                                                    <div class="form-group">
                                                                        <select class="form-control" name="age_to">
                                                                            @for($i=config('constants.partner_preference.start_age'); $i<=config('constants.partner_preference.end_age'); $i++)
                                                                                <option value="{{$i}}" @if($request->input('age_to'))  @if($request->input('age_to') == $i) selected="selected" @endif @else @if($i == config('constants.partner_preference.end_age')) selected="selected" @endif @endif >{{$i}}</option>
                                                                            @endfor
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Country :</label>
                                                                        <div class="ne-search-option">
                                                                            <select data-placeholder="Select Country" name="country[]" id="country" class="form-control chosen-select dynamic" multiple="" data-dependent="cities">
                                                                                @foreach($locationRepo->getCountries() as $country)
                                                                                    <option value="{{$country->id}}" @if($request->input('country')) @if(in_array($country->id, $request->input('country'))) selected="selected" @endif @else @if($user->country_id == $country->id ) selected="selected" @endif @endif >{{$country->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>City :</label>
                                                                        <div class="ne-search-option">
                                                                            <select data-placeholder="Select City" name="cities[]" id="cities" multiple="" class="form-control chosen-select">
                                                                                @foreach($allCities as $city)
                                                                                    <option value="{{$city->id}}" @if($request->input('cities')) @if(in_array($city->id, $request->input('cities'))) selected="selected" @endif @else @if($user->city_id == $city->id ) selected="selected" @endif @endif >{{$city->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{--<div class="row">--}}
                                                                {{--<div class="col-sm-12">--}}
                                                                    {{--<label>Show Me On Top:</label>--}}
                                                                    {{--<div class="form-group">--}}
                                                                        {{--<label class="radio-inline">--}}
                                                                            {{--<input type="radio" name="optradio" checked>Most active profiles--}}
                                                                        {{--</label>--}}
                                                                        {{--<label class="radio-inline">--}}
                                                                            {{--<input type="radio" name="optradio">Newest profiles--}}
                                                                        {{--</label>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Search</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @if(!is_null($results))
                                        @if(!$results->isEmpty())
                                         @foreach($results as $result)
                                        <div class="col-md-6 col-xs-6 panel-body">
                                            <a href="javascript:void(0)" id="addClass" class="addClass" data-profilelink="{{$result->popupProfileLink()}}">
                                                <div class="like-img" style="overflow: hidden">
                                                    <img src="{{$result->getVendorProfilePic()}}" class="img-responsive">
                                                    <h4 style="overflow: hidden; text-overflow: ellipsis;white-space: nowrap;"><span style="white-space: nowrap">{{$result->name}},</span> <small style="white-space: nowrap">{{$result->getAge()}} {{$result->city->name}}</small></h4>
                                                </div>
                                            </a>
                                        </div>
                                       @endforeach
                                             @if(config('usersmanagement.enableCmsPagination') && count($results)<=config('usersmanagement.frontEndSize'))
                                                 <div  style="clear:both;">
                                                 </div>
                                                     <div class="col-sm-12 panel-body">
                                                         {{ $results->links() }}
                                                     </div>
                                             @endif
                                        @else
                                            <div class="col-md-12 col-xs-12 panel-body">
                                                <div class="alert alert-info" role="alert">
                                                    No User found.
                                                </div>
                                            </div>
                                        @endif
                                    @else
                                        <div class="col-md-12 col-xs-12 panel-body">
                                            <div class="alert alert-info" role="alert">
                                                No User found.
                                            </div>
                                        </div>
                                    @endif
                                    <div class="popup-box chat-popup search-popup" id="qnimate">
                                        <div class="popup-head">
                                            <div class="popup-head-right pull-right">
                                                <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                            <div id="popup-body">

                                            </div>
                                    </div>

                                    <div class="personal-detail-button" id="shortlistt">
                                    </div>

                                    <span id="reportuseroutside">

                                    </span>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer_scripts')
    <script src="{{asset('js/select-box.js')}}"></script>
    <script type="text/javascript">
        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
            '.chosen-select-width'     : {width:"100%"}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    </script>
    {{--<script>--}}
        {{--$("#menu-toggle").click(function(e) {--}}
            {{--e.preventDefault();--}}
            {{--$("#wrapper").toggleClass("toggled");--}}
            {{--$(this).find('i').toggleClass('fa-navicon fa-times')--}}

        {{--});--}}
    {{--</script>--}}
    <script type="text/javascript">
        $(document).ready(function(){
            $('.launch-modal').click(function(){
                $('#myModal').modal({
                    backdrop: 'static'
                });
            });
        });
    </script>
    <script>
        $(function(){
            $(".addClass").click(function () {
                var loader = "<div class=\"text-center\">\n" +
                    "<img src=\"{{asset('img/loader.gif')}}\" id=\"loader\" width=\"20\" style='margin-top:50%' >\n" +
                    "</div>";
                $('#qnimate #popup-body').html(loader);
                $('#qnimate').addClass('popup-box-on');
                $('.personal-detail-button').addClass('show_b');
                var link = $(this).data('profilelink');
                $.ajax({
                    url: link,
                    method:"get",
                    success:function(result)
                    {
                        var substriing=result; //substriing.substring(substriing.indexOf('skipbuttonDiv'),2000);

                        //alert(substriing.search('@skipbuttonDiv'));
                        //alert(substriing.search('@endskipbuttonDiv'));
                        //console.log(substriing);
                        var startString="@skipbuttonDiv";
                        var endString="@endskipbuttonDiv";

                        var firstpos=substriing.indexOf(startString)+0;
                        var endpos=substriing.indexOf(endString)+0;


                        var secondpart= (substriing.substring(substriing.indexOf(startString)+0));
                        var secondpart1= (secondpart.substring(14,secondpart.indexOf(endString)+0));
                        var finalpopuphtml=(substriing.substring(0,substriing.indexOf(startString)+0)) +" "+ substriing.substring(substriing.indexOf(endString)+17)
                       // console.log(finalpopuphtml);
                      //  console.log(secondpart1);




                        var startString1="@reportuserDiv";
                        var endString1="@endreportuserDiv";

                        var firstpos=finalpopuphtml.indexOf(startString1)+0;
                        var endpos=finalpopuphtml.indexOf(endString1)+0;

                        var secondpart11= (finalpopuphtml.substring(finalpopuphtml.indexOf(startString1)+0));
                        var secondpart2= (secondpart11.substring(14,secondpart11.indexOf(endString1)+0));
                        var finalpopuphtml1=(finalpopuphtml.substring(0,finalpopuphtml.indexOf(startString1)+0)) +" "+ finalpopuphtml.substring(finalpopuphtml.indexOf(endString1)+17)




                        $('#reportuseroutside').html(secondpart2);
                        $('#shortlistt').html(secondpart1);
                        $('#qnimate #popup-body').html(finalpopuphtml1);


                        //$('#qnimate #popup-body').html(result);
                    }

                })
            });

            $("#removeClass").click(function () {
                $('#qnimate').removeClass('popup-box-on');
                $('.personal-detail-button').removeClass('show_b');
            });
        })







    </script>
    <script>
        $(document).ready(function(){

            $('.dynamic').change(function(){

                if($(this).val() != '' && $(this).val() != null)
                {

                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    var output;
                    $('#'+dependent).html('');
                    $.ajax({
                        url:"{{ route('ajax.fetchLocation') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {
                            for(var i=0; i<result.length; i++){
                                output += "<option value="+result[i].id+">"+result[i].value+"</option>";
                            }
                            $('#'+dependent).append(output);
                            $('#'+dependent).trigger("chosen:updated");
                        }

                    })
                } else {
                    var dependent = $(this).data('dependent');
                    $('#'+dependent).html('');
                    $('#'+dependent).trigger("chosen:updated");
                }
            });
        });

    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection