@extends('layouts.app')
@section('template_linked_css')
@endsection
@section('content')
@php
    $average_rating = $vendorObj->getAverageReviewRating();
    $total_review = $vendorObj->getTotalReviewVendor();
    if($total_review > 0) {
        $percentage_rating = ( $vendorObj->getSumReviewRating() * 100 / ($total_review*5) );
    } else {
        $percentage_rating = 0;
    }
@endphp
    <div class="login_p signup ">
        <div class="container all-reviews">
            <div class="row">
                <div class="col-sm-10 col-xs-12 col-sm-offset-1">
                    <div class="rev-rating">
                        <h3>@if($vendorObj->checkProfileFieldApproved('company_name')) {{$vendorObj->vendors_information->company_name}} @endif by @if($vendorObj->checkProfileFieldApproved('first_name')) {{$vendorObj->first_name}} @endif</h3>
                        <h4>{{substr($average_rating, 0, -2)}} 
                            @for($r=1;$r<=5;$r++)
                            <i class="fa @if($r <= $average_rating) fa-star @elseif($r >= $average_rating-0.5 && $r <= $average_rating+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                            @endfor <span>{{$percentage_rating}}% Positive ( {{$total_review}} @if( $total_review > 1) reviews @else review @endif)</span></h4>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            @for($br=1;$br<=5;$br++)
                                <div class="side">
                                    <div id="star-rating-{{$br}}" class="style-bold-rate" @if($per_rate == $br) style="font-weight: bold;" @endif><a href="javascript:void(0);" onclick="javascript:getPerRatingReviews({{$br}});">{{$br}} star</a></div>
                                </div>
                                <div class="middle">
                                    <div class="bar-container">
                                        <div class="bar-{{$vendorObj->getSpcificStartBarPer($br)}}"></div>
                                    </div>
                                </div>
                                <div class="side right">
                                    <div id="total-rating-{{$br}}" class="style-bold-rate" @if($per_rate == $br) style="font-weight: bold;" @endif><a href="javascript:void(0);" onclick="javascript:getPerRatingReviews({{$br}});">{{$vendorObj->getCountReviewStarSpecific($br)}}</a></div>
                                </div>
                            @endfor
                        </div>
                        <div class="col-sm-8 col-xs-12 text-right">
                            <a href="{{ route('write-review', [$vendorObj->getEncryptedId()]) }}" class="btn btn-default">Write a review</a>
                        </div>
                    </div>
                    <hr>
                    <div id="vendor-reviews-results">
                    @include('pages.user.ajax.show-reviews')
                    </div>
                    @if(isset($reviews['reviews']) && count($reviews['reviews']) > 0)
                    <div class="media">
                        <div class="media-body text-center">
                            <a href="javascript:void(0);" id="next-page-review" data-next-page="2" class="btn btn-default">Show More </a>
                            <img src="{{asset('img/loader.gif')}}" id="spinner-load" width="40" style="display: none;margin-top: 10px;">
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div id="rate-per-page" data-per-rate="{{$per_rate}}"></div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
var ajaxPerRateGetRequest = null;
function getPerRatingReviews(per_rate)
{
    // bold out the text
    $('.style-bold-rate').css('font-weight', '');
    $("#star-rating-"+per_rate).css('font-weight', 'bold');
    $("#total-rating-"+per_rate).css('font-weight', 'bold');
    

    var url = route('get-vendor-reviews-ajax')+'?page=1&vendor_id={{$vendorObj->getEncryptedId()}}'+'&per_rate='+per_rate;
    
    ajaxPerRateGetRequest = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            // show loading image
            $('#next-page-review').hide();
            $('#spinner-load').show();
        },
        success: function(result) {
            // show results
            $('#vendor-reviews-results').html(result.reviews);
            $('#next-page-review').data('next-page', result.next_page);
            if(result.next_url == '') {
                $('#next-page-review').hide();
            } else {
                $('#next-page-review').show();
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            // hide loading image
            $('#spinner-load').hide();
            ajaxPerRateGetRequest = null;
        }
    });
}
var ajaxResultGetRequest = null
$("#next-page-review").on('click', function() {
    var per_rate = $('#rate-per-page').data('per-rate');
    var url = route('get-vendor-reviews-ajax')+'?page='+$(this).data('next-page') + '&vendor_id={{$vendorObj->getEncryptedId()}}'+'&per_rate='+per_rate;
    
    ajaxResultGetRequest = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            // show loading image
            $('#next-page-review').hide();
            $('#spinner-load').show();
        },
        success: function(result) {
            // show results
            $('#vendor-reviews-results').append(result.reviews);
            $('#next-page-review').data('next-page', result.next_page);
            if(result.next_url == '') {
                $('#next-page-review').hide();
            } else {
                $('#next-page-review').show();
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            // hide loading image
            $('#spinner-load').hide();
            ajaxResultGetRequest = null;
        }
    });
});
</script>
@endsection