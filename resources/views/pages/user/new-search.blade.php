@extends('layouts.app')
@section('template_title')
    Search Profile
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection

@section('content')

    <link href="{{ asset('css/select-box.css') }}" rel="stylesheet" type="text/css">
    <div class="dashboard">
        <div class="container">
            @if(session('success'))
                <div class="alert alert-success" role="alert">
                    {{session('success')}}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 search-panel hidden-xs">
                    <hr>
                    <div class="left-tab-heading">
                        <h4>New Search</h4>
                    </div>
                    <div class="panel-group" id="accordion">
                        <div class="panel">
                            <div class="panel-body">
                                <form method="get" id="newSearchProfileKw" action="{{ route('new-search') }}">
                                    <div class="input-group stylish-input-group">
                                        <input type="text" name="keywordstop" id="keywordstop" value="<?php if(isset($search_arry['keywords'])) { echo $search_arry['keywords']; } ?>" class="form-control" placeholder="Enter Keywords">
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                    <a href="{{ route('new-search') }}" class="btn reset-result pull-right">Reset all</a>
                                </form>
                            </div>
                            <!--Location-->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#collapse0" aria-expanded="true" class="">Location<span class="fa pull-right fa-minus"></span></a>
                                </h4>
                            </div>

                            <div id="collapse0" class="panel-collapse collapse in" aria-expanded="true" style="">
                                <div class="panel-body">
                                    <div class="check">
                                        <p>
                                            Current Country : {{$userProfile->country->name}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--Location-->
                        </div>
                        <form method="get" id="newSearchProfileFlt" action="{{ route('new-search') }}">
                            <!--Country-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse17" aria-expanded="true" class="">Country<span class="fa pull-right fa-minus"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse17" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body ne-search-option">
                                        <select no-filter="1" name="country_code[]" data-placeholder="Select Country" style="display:none" class="chosen-select form-control dynamics" multiple tabindex="5" id="country" dependent="state" >
                                            <option value="{{config('constants.doesnt_matter')}}" <?php if( !empty($search_arry['country_code']) &&in_array(config('constants.doesnt_matter'), $search_arry['country_code'])) { echo "selected"; } ?> >Doesn't matter</option>
                                            @foreach(getCountry() as $ctr)
                                                <option <?php if( !empty($search_arry['country_code']) &&in_array($ctr->id, $search_arry['country_code'])) { echo "selected"; } ?> value="{{ $ctr->id }}">{{ $ctr->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--Country-->
                            <!--State-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse18" aria-expanded="true" class="">State<span class="fa pull-right fa-minus"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse18" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    @php
                                        if( !empty($search_arry['state_id']) ) {
                                            $sl = $search_arry['state_id'];
                                        } else {
                                            $sl = [];
                                        }
                                        if( !empty($search_arry['country_code']) ) {
                                            $cll = $search_arry['country_code'];
                                        } else {
                                            $cll = [];
                                        }
                                    @endphp
                                    <div class="panel-body ne-search-option">
                                        <select  no-filter="1"  name="state_id[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control dynamics" id="state" dependent="city" multiple tabindex="5">
                                            <option <?php if( in_array(config('constants.doesnt_matter'), $sl) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                            @foreach(getStateByCountry($cll) as $stt)
                                                <option value="{{ $stt->id }}" <?php if(in_array($stt->id, $sl)){echo "selected";} ?>>{{ $stt->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--State-->
                            <!--City-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse19" aria-expanded="true" class="">City<span class="fa pull-right fa-minus"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse19" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    @php
                                        if( !empty($search_arry['city_id']) ) {
                                            $ctl = $search_arry['city_id'];
                                        } else {
                                            $ctl = [];
                                        }
                                        if( !empty($search_arry['state_id']) ) {
                                            $sll = $search_arry['state_id'];
                                        } else {
                                            $sll = [];
                                        }
                                    @endphp
                                    <div class="panel-body ne-search-option">
                                        <select  no-filter="1"  name="city_id[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple id="city" tabindex="5">
                                            <option <?php if( in_array(config('constants.doesnt_matter'), $ctl) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                            @foreach(getCityByState($sll) as $ct)
                                                <option value="{{ $ct->id }}" <?php if(in_array($ct->id, $ctl)){echo "selected";} ?>>{{ $ct->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--City-->
                            <!--Religion-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse20" aria-expanded="<?php if(!empty($search_arry['religion'])) : echo "true"; else: echo "false"; endif; ?>"  class="">Religion<span class="fa pull-right fa-<?php if(!empty($search_arry['religion'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse20" class="panel-collapse collapse <?php if(!empty($search_arry['religion'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['religion'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                    <div class="panel-body ne-search-option">
                                        <select name="religion" id="religion" class="form-control">
                                            @foreach($religions as $name=>$rId)
                                                <option value="{{$rId}}">{{ ucfirst($name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--Religion-->
                            <!--Sect-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse21" aria-expanded="<?php if(!empty($search_arry['sect'])) : echo "in"; endif; ?>" class="">Sect<span class="fa pull-right fa-<?php if(!empty($search_arry['sect'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse21" class="panel-collapse collapse <?php if(!empty($search_arry['sect'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['sect'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                    @php
                                        if( !empty($search_arry['sect']) ) {
                                            $sect = $search_arry['sect'];
                                        } else {
                                            $sect = [];
                                        }
                                    @endphp
                                    <div class="panel-body ne-search-option">
                                        <div class="ne-search-option">
                                            <select name="sect[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" >
                                                <option <?php if( in_array(config('constants.doesnt_matter'), $sect) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                @foreach(getSects() as $sct)
                                                    <option <?php if( in_array($sct->id, $sect) ) { echo "selected"; } ?> value="{{ $sct->id }}">{{ $sct->name }}</option>
                                                @endforeach
                                            </select>
                                            <i class='chosen_loader'>loading...</i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Sect-->
                            <!--Language-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse22" aria-expanded="<?php if(!empty($search_arry['language'])) : echo "true"; else: echo "false"; endif; ?>" class="">Language<span class="fa pull-right fa-plus"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse22" class="panel-collapse collapse <?php if(!empty($search_arry['language'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['language'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                    <div class="panel-body ne-search-option">
                                        <select data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" name="language[]">
                                            <option <?php if( !empty($search_arry['language']) && in_array(config('constants.doesnt_matter'), $search_arry['language']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                            @foreach(getLanguages() as $lang)
                                                <option <?php if( !empty($search_arry['language']) && in_array($lang->id, $search_arry['language']) ) { echo "selected"; } ?> value="{{ $lang->id }}">{{ $lang->language }}</option>
                                            @endforeach
                                        </select>
                                        <i class='chosen_loader'>loading...</i>
                                    </div>
                                </div>
                            </div>
                            <!--Language-->
                            <!--Education-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse23" aria-expanded="<?php if(!empty($search_arry['education_id'])) : echo "true"; else: echo "false"; endif; ?>" class="">Education<span class="fa pull-right fa-<?php if(!empty($search_arry['education_id'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse23" class="panel-collapse collapse <?php if(!empty($search_arry['education_id'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['education_id'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                    <div class="panel-body ne-search-option">
                                        <select data-placeholder="Select" class="chosen-select form-control" multiple tabindex="5" name="education_id[]">

                                            <option <?php if( !empty($search_arry['education_id']) && in_array(config('constants.doesnt_matter'), $search_arry['education_id']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                            @foreach(getEducation() as $ed)
                                                <option <?php if( !empty($search_arry['education_id']) && in_array($ed->id, $search_arry['education_id']) ) { echo "selected"; } ?> value="{{ $ed->id }}">{{ $ed->education }}</option>
                                            @endforeach

                                        </select>
                                        <i class='chosen_loader'>loading...</i>
                                    </div>
                                </div>
                            </div>
                            <!--Education-->
                            <!--Body Type-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse24" aria-expanded="<?php if(!empty($search_arry['body_type'])) : echo "true"; else: echo "false"; endif; ?>" class="">Body Type<span class="fa pull-right fa-<?php if(!empty($search_arry['body_type'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse24" class="panel-collapse collapse <?php if(!empty($search_arry['body_type'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['body_type'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                    <div class="panel-body ne-search-option">
                                        <select  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" name="body_type[]">
                                            <option <?php if( !empty($search_arry['body_type']) && in_array(config('constants.doesnt_matter'), $search_arry['body_type']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                            @foreach(config('constants.body_type') as $key=>$cpl)
                                                <option <?php if( !empty($search_arry['body_type']) && in_array($key, $search_arry['body_type']) ) { echo "selected"; } ?> value="{{ $key }}">{{ $cpl }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--Body Type-->
                            <!--Age-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse"  href="#collapse25">Age<span class="fa fa-minus pull-right"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse25" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>
                                            <input type="text" name="ageRange" id="age"  value="0" readonly class="input-filter">
                                        </p>
                                        <div id="slider-age"></div>
                                    </div>
                                </div>
                            </div>
                            <!--Age-->
                            <!--Height-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse"  href="#collapse26">Height<span class="fa fa-minus pull-right"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse26" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>
                                            <input type="text" name="heightRange" id="height"  value="0" readonly class="input-filter">
                                        </p>
                                        <div id="slider-height"></div>
                                    </div>
                                </div>
                            </div>
                            <!--Height-->
                            <!--Food Preference-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse27" aria-expanded="<?php if(!empty($search_arry['food'])) : echo "true"; else: echo "false"; endif; ?>" class="">Food Preference<span class="fa pull-right fa-<?php if(!empty($search_arry['food'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse27" class="panel-collapse collapse <?php if(!empty($search_arry['food'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['food'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                    <div class="panel-body ne-search-option">
                                        <select name="food[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">

                                            <option <?php if( !empty($search_arry['food']) && in_array(config('constants.doesnt_matter'), $search_arry['food']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                            @foreach(config('constants.food') as $rId=>$name)
                                                <option <?php if( !empty($search_arry['food']) && in_array($rId, $search_arry['food']) ) { echo "selected"; } ?> value="{{ $rId }}">{{ $name }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--Food Preference-->
                            <!--Smoking-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse28" aria-expanded="<?php if(!empty($search_arry['smoke'])) : echo "true"; else: echo "false"; endif; ?>" class="">Smoking<span class="fa pull-right fa-<?php if(!empty($search_arry['smoke'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse28" class="panel-collapse collapse <?php if(!empty($search_arry['smoke'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['smoke'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                    <div class="panel-body ne-search-option">
                                        <select name="smoke[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">

                                            <option <?php if( !empty($search_arry['smoke']) && in_array(config('constants.doesnt_matter'), $search_arry['smoke']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                            @foreach(config('constants.smoke') as $rId=>$name)
                                                <option <?php if( !empty($search_arry['smoke']) && in_array($rId, $search_arry['smoke']) ) { echo "selected"; } ?> value="{{ $rId }}">{{ $name }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--Smoking-->
                            <!--Driking-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse29" aria-expanded="<?php if(!empty($search_arry['drink'])) : echo "true"; else: echo "false"; endif; ?>" class="">Driking<span class="fa pull-right fa-<?php if(!empty($search_arry['drink'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse29" class="panel-collapse collapse <?php if(!empty($search_arry['drink'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['drink'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                    <div class="panel-body ne-search-option">
                                        <select name="drink[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">

                                            <option <?php if( !empty($search_arry['drink']) && in_array(config('constants.doesnt_matter'), $search_arry['drink']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                            @foreach(config('constants.drink') as $rId=>$name)
                                                <option <?php if( !empty($search_arry['drink']) && in_array($rId, $search_arry['drink']) ) { echo "selected"; } ?> value="{{ $rId }}">{{ $name }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--Driking-->
                            <!--Marital Status-->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse30" aria-expanded="<?php if(!empty($search_arry['marital_status'])) : echo "true"; else: echo "false"; endif; ?>" class="">Marital Status<span class="fa pull-right fa-<?php if(!empty($search_arry['marital_status'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse30" class="panel-collapse collapse <?php if(!empty($search_arry['marital_status'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['marital_status'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                    <div class="panel-body ne-search-option">
                                        <select name="marital_status[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" >
                                            <option <?php if( !empty($search_arry['marital_status']) && in_array(config('constants.doesnt_matter'), $search_arry['marital_status']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                            @foreach(config('constants.marital_status') as $key=>$val)
                                                <option <?php if( !empty($search_arry['marital_status']) && in_array($key, $search_arry['marital_status']) ) { echo "selected"; } ?> value="{{ $key }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--Marital Status-->
                            <div class="panel">
                                <button style="button" class="btn btn-warning">Search</button>
                                <a href="{{ route('new-search') }}" class="btn reset-result pull-right">Reset all</a>
                            </div>
                        </form>
                        <form method="get" id="newSearchProfileKwDn" action="{{ route('new-search') }}">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse31">Search Text<span class="fa fa-minus pull-right"></span></a>
                                    </h4>
                                </div>
                                <div id="collapse31" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="input-group stylish-input-group">
                                            <input type="text" name="keywordsdown" id="keywordsdown" value="<?php if(isset($search_arry['keywords'])) { echo $search_arry['keywords']; } ?>" class="form-control" placeholder="Enter Keywords">
                                            <span class="input-group-btn">
                                                <button class="btn btn-success" id="bottom-keyword-sbmt" type="submit"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-8 search_profile">
                    <div class="row">
                        <div class="filter-fixed">
                            <div class="col-sm-12 hidden-xs">
                                <hr>
                            </div>

                            <div class="col-xs-2 hidden-sm hidden-md hidden-lg text-center">
                                <a data-toggle="modal" data-target="#mobile-search-modal" style="color:#eea236;font-size:24px;margin-top: -8px;display: block;" class=" filter"><i class="fa fa-filter" aria-hidden="true"></i></a>
                                <!-- Modal -->
                            </div>

                            <div class="col-sm-8 col-xs-4 padding-right">

                                    @if($results->total() > 1)
                                        About {{ $results->total() }} Results...
                                    @else
                                        About {{ $results->total() }} Result...
                                    @endif

                            </div>
                            <div class="col-sm-2 col-xs-3 padding-right">
                                <div class="dropdown pull-right">
                                    <a class="dropdown-toggle sort" data-toggle="dropdown" aria-expanded="false">Sort By
                                        <span class="fa fa-sort"></span></a>
                                    <ul class="dropdown-menu">
                                        @php
                                            $order_by_lnk_arry = $search_arry;
                                        @endphp
                                        @php
                                            $order_by_lnk_arry['order_by'] = 'latest';
                                        @endphp
                                        <li <?php if($search_arry['order_by'] == 'latest') : ?>style="background-color: #4cae4c;"<?php endif; ?>><a href="{{ route('new-search', $order_by_lnk_arry) }}">New Users</a></li>
                                        @php
                                            $order_by_lnk_arry['order_by'] = 'online';
                                        @endphp
                                        <li <?php if($search_arry['order_by'] == 'online') : ?>style="background-color: #4cae4c;"<?php endif; ?>><a href="{{ route('new-search', $order_by_lnk_arry) }}">Online Users</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-2 col-xs-3 padding-right">
                                <div class="dropdown pull-right">
                                    <a class="dropdown-toggle sort" href="#" data-toggle="dropdown" aria-expanded="false">Per Page
                                        <span class="caret"></span></a>
                                    @php
                                        $per_page_lnk_arry = $search_arry;
                                    @endphp
                                    <ul class="dropdown-menu text-right">
                                        @php
                                            $per_page_lnk_arry['per_page'] = 10;
                                        @endphp
                                        <li <?php if($search_arry['per_page'] == 10) : ?>style="background-color: #4cae4c;"<?php endif; ?>><a href="{{ route('new-search', $per_page_lnk_arry) }}">10 Per Page</a></li>
                                        @php
                                            $per_page_lnk_arry['per_page'] = 20;
                                        @endphp
                                        <li <?php if($search_arry['per_page'] == 20) : ?>style="background-color: #4cae4c;"<?php endif; ?>><a href="{{ route('new-search', $per_page_lnk_arry) }}">20 Per Page</a></li>
                                        @php
                                            $per_page_lnk_arry['per_page'] = 30;
                                        @endphp
                                        <li <?php if($search_arry['per_page'] == 30) : ?>style="background-color: #4cae4c;"<?php endif; ?>><a href="{{ route('new-search', $per_page_lnk_arry) }}">30 Per Page</a></li>
                                        @php
                                            $per_page_lnk_arry['per_page'] = 40;
                                        @endphp
                                        <li <?php if($search_arry['per_page'] == 40) : ?>style="background-color: #4cae4c;"<?php endif; ?>><a href="{{ route('new-search', $per_page_lnk_arry) }}">40 Per Page</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {{--Mobile Search --}}
                        <div id="mobile-search-modal" class="modal fade" role="dialog">
                            <div class="modal-dialog" style="margin:0px;">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <div class="left-tab-heading">
                                            <h4>New Search</h4>
                                        </div>

                                        <div class="panel-group" id="accordion-mobile">
                                            <div class="panel">
                                                <div class="panel-body">
                                                    <form method="get" id="newMobSearchProfileKw" action="{{ route('new-search') }}">
                                                        <div class="input-group stylish-input-group">
                                                            <input type="text" name="keywordstop" name="keywordstop-mob" value="<?php if(isset($search_arry['keywords'])) { echo $search_arry['keywords']; } ?>" class="form-control"  placeholder="Enter Keywords" >
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
                                                            </span>
                                                        </div>
                                                        <a href="{{ route('new-search') }}" class="btn reset-result pull-right">Reset all</a>
                                                    </form>
                                                </div>
                                                <!--Location-->
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" href="#collapse00">Location<span class="fa fa-minus pull-right"></span></a>
                                                    </h4>
                                                </div>
                                                <div id="collapse00" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div class="check">
                                                            <p>
                                                                Current Country : {{$userProfile->country->name}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Location-->
                                            </div>
                                            <form method="get" id="newMobSearchProfileFlt" action="{{ route('new-search') }}">
                                                <!-- Country  -->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" aria-expanded="true"  href="#collapse170">Country<span class="fa fa-minus pull-right"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse170" class="panel-collapse collapse in" aria-expanded="true">
                                                        <div class="panel-body ne-search-option">
                                                            <select no-filter="1" name="country_code[]" data-placeholder="Select Country" style="display:none" class="chosen-select form-control dynamic_diff" multiple tabindex="5" id="country-mob" dependent="state-mob">
                                                                <option value="{{config('constants.doesnt_matter')}}" <?php if( !empty($search_arry['country_code']) &&in_array(config('constants.doesnt_matter'), $search_arry['country_code'])) { echo "selected"; } ?> >Doesn't matter</option>
                                                                @foreach(getCountry() as $ctr)
                                                                    <option <?php if( !empty($search_arry['country_code']) &&in_array($ctr->id, $search_arry['country_code'])) { echo "selected"; } ?> value="{{ $ctr->id }}">{{ $ctr->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Country  -->

                                                <!-- State  -->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" aria-expanded="true" href="#collapse180">State<span class="fa fa-minus pull-right"></span></a>
                                                        </h4>
                                                    </div>
                                                    @php
                                                        if( !empty($search_arry['state_id']) ) {
                                                            $sl = $search_arry['state_id'];
                                                        } else {
                                                            $sl = [];
                                                        }
                                                        if( !empty($search_arry['country_code']) ) {
                                                            $cll = $search_arry['country_code'];
                                                        } else {
                                                            $cll = [];
                                                        }
                                                    @endphp
                                                    <div id="collapse180" class="panel-collapse collapse in" aria-expanded="true">
                                                        <div class="panel-body ne-search-option">
                                                            <select no-filter="1" name="state_id[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control dynamic_diff" id="state-mob" dependent="city-mob" multiple tabindex="5">
                                                                <option <?php if( in_array(config('constants.doesnt_matter'), $sl) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(getStateByCountry($cll) as $stt)
                                                                    <option value="{{ $stt->id }}" <?php if(in_array($stt->id, $sl)){echo "selected";} ?>>{{ $stt->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- City  -->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" aria-expanded="true" href="#collapse190">City<span class="fa fa-minus pull-right"></span></a>
                                                        </h4>
                                                    </div>
                                                    @php
                                                        if( !empty($search_arry['city_id']) ) {
                                                            $ctl = $search_arry['city_id'];
                                                        } else {
                                                            $ctl = [];
                                                        }
                                                        if( !empty($search_arry['state_id']) ) {
                                                            $sll = $search_arry['state_id'];
                                                        } else {
                                                            $sll = [];
                                                        }
                                                    @endphp
                                                    <div id="collapse190" class="panel-collapse collapse in" aria-expanded="true">
                                                        <div class="panel-body ne-search-option">
                                                            <select no-filter="1" name="city_id[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple id="city-mob" tabindex="5">
                                                                <option <?php if( in_array(config('constants.doesnt_matter'), $ctl) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(getCityByState($sll) as $ct)
                                                                    <option value="{{ $ct->id }}" <?php if(in_array($ct->id, $ctl)){echo "selected";} ?>>{{ $ct->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--Religion-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapsemrel" aria-expanded="<?php if(!empty($search_arry['religion'])) : echo "true"; else: echo "false"; endif; ?>">Religion<span class="fa pull-right fa-<?php if(!empty($search_arry['religion'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapsemrel" class="panel-collapse collapse <?php if(!empty($search_arry['religion'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['religion'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        <div class="panel-body">
                                                            <select name="religion" id="religion-mob" class="form-control">
                                                                @foreach($religions as $name=>$rId)
                                                                    <option value="{{$rId}}">{{ ucfirst($name) }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Religion-->

                                                <!-- Sect -->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapse150"aria-expanded="<?php if(!empty($search_arry['sect'])) : echo "in"; endif; ?>" class="">Sect<span class="fa pull-right fa-<?php if(!empty($search_arry['sect'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse150" class="panel-collapse collapse <?php if(!empty($search_arry['sect'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['sect'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        @php
                                                            if( !empty($search_arry['sect']) ) {
                                                                $sect = $search_arry['sect'];
                                                            } else {
                                                                $sect = [];
                                                            }
                                                        @endphp
                                                        <div class="panel-body ne-search-option">
                                                            <select name="sect[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">
                                                                <option <?php if( in_array(config('constants.doesnt_matter'), $sect) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(getSects() as $sct)
                                                                    <option <?php if( in_array($sct->id, $sect) ) { echo "selected"; } ?> value="{{ $sct->id }}">{{ $sct->name }}</option>
                                                                @endforeach
                                                            </select>
                                                            <i class='chosen_loader'>loading...</i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Community-->

                                                <!--Language-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapse1130" aria-expanded="<?php if(!empty($search_arry['language'])) : echo "true"; else: echo "false"; endif; ?>" class="">Language<span class="fa pull-right fa-<?php if(!empty($search_arry['sect'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse1130" class="panel-collapse collapse <?php if(!empty($search_arry['language'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['language'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        <div class="panel-body ne-search-option">
                                                            <select data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" name="language[]">
                                                                <option <?php if( !empty($search_arry['language']) && in_array(config('constants.doesnt_matter'), $search_arry['language']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(getLanguages() as $lang)
                                                                    <option <?php if( !empty($search_arry['language']) && in_array($lang->id, $search_arry['language']) ) { echo "selected"; } ?> value="{{ $lang->id }}">{{ $lang->language }}</option>
                                                                @endforeach
                                                            </select>
                                                            <i class='chosen_loader'>loading...</i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Language-->

                                                <!--Education-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse1110" aria-expanded="<?php if(!empty($search_arry['education_id'])) : echo "true"; else: echo "false"; endif; ?>" class="">Education<span class="fa pull-right fa-<?php if(!empty($search_arry['education_id'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse1110" class="panel-collapse collapse <?php if(!empty($search_arry['education_id'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['education_id'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        <div class="panel-body">
                                                            <select data-placeholder="Select" class="chosen-select form-control" multiple tabindex="5" name="education_id[]">

                                                                <option <?php if( !empty($search_arry['education_id']) && in_array(config('constants.doesnt_matter'), $search_arry['education_id']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(getEducation() as $ed)
                                                                    <option <?php if( !empty($search_arry['education_id']) && in_array($ed->id, $search_arry['education_id']) ) { echo "selected"; } ?> value="{{ $ed->id }}">{{ $ed->education }}</option>
                                                                @endforeach

                                                            </select>
                                                            <i class='chosen_loader'>loading...</i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Education-->

                                                <!--Body Type -->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse1140" aria-expanded="<?php if(!empty($search_arry['body_type'])) : echo "true"; else: echo "false"; endif; ?>" class="">Body Type<span class="fa pull-right fa-<?php if(!empty($search_arry['body_type'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse1140" class="panel-collapse collapse <?php if(!empty($search_arry['body_type'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['body_type'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        <div class="panel-body">
                                                            <select  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" name="body_type[]">
                                                                <option <?php if( !empty($search_arry['body_type']) && in_array(config('constants.doesnt_matter'), $search_arry['body_type']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(config('constants.body_type') as $key=>$cpl)
                                                                    <option <?php if( !empty($search_arry['body_type']) && in_array($key, $search_arry['body_type']) ) { echo "selected"; } ?> value="{{ $key }}">{{ $cpl }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Body Type -->

                                                <!--Age-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapse1150">Age<span class="fa fa-minus pull-right"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse1150" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <p>
                                                                <input type="text" name="ageRange" id="age-mob"  value="0" readonly class="input-filter">
                                                            </p>
                                                            <div id="slider-age-mob"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Age-->

                                                <!--Height-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse1160">Height<span class="fa fa-minus pull-right"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse1160" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <p>
                                                                <input type="text" name="heightRange" id="height-mob"  value="0" readonly class="input-filter">
                                                            </p>
                                                            <div id="slider-height-mob"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Height-->

                                                <!--Food Prefrence-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse290" aria-expanded="<?php if(!empty($search_arry['food'])) : echo "true"; else: echo "false"; endif; ?>">Food Preference<span class="fa pull-right fa-<?php if(!empty($search_arry['food'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse290" class="panel-collapse collapse <?php if(!empty($search_arry['food'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['food'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        <div class="panel-body">
                                                            <select name="food[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">

                                                                <option <?php if( !empty($search_arry['food']) && in_array(config('constants.doesnt_matter'), $search_arry['food']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(config('constants.food') as $rId=>$name)
                                                                    <option <?php if( !empty($search_arry['food']) && in_array($rId, $search_arry['food']) ) { echo "selected"; } ?> value="{{ $rId }}">{{ $name }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Food Prefrence-->

                                                <!--Smoking-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse1190" aria-expanded="<?php if(!empty($search_arry['smoke'])) : echo "true"; else: echo "false"; endif; ?>" class="">Smoking<span class="fa pull-right fa-<?php if(!empty($search_arry['smoke'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse1190" class="panel-collapse collapse <?php if(!empty($search_arry['smoke'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['smoke'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        <div class="panel-body">
                                                            <select name="smoke[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">

                                                                <option <?php if( !empty($search_arry['smoke']) && in_array(config('constants.doesnt_matter'), $search_arry['smoke']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(config('constants.smoke') as $rId=>$name)
                                                                    <option <?php if( !empty($search_arry['smoke']) && in_array($rId, $search_arry['smoke']) ) { echo "selected"; } ?> value="{{ $rId }}">{{ $name }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Smoking-->

                                                <!--Drinking-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse140" aria-expanded="<?php if(!empty($search_arry['drink'])) : echo "true"; else: echo "false"; endif; ?>" class="">Driking<span class="fa pull-right fa-<?php if(!empty($search_arry['drink'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse140" class="panel-collapse collapse <?php if(!empty($search_arry['drink'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['drink'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        <div class="panel-body">
                                                            <select name="drink[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">

                                                                <option <?php if( !empty($search_arry['drink']) && in_array(config('constants.doesnt_matter'), $search_arry['drink']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(config('constants.drink') as $rId=>$name)
                                                                    <option <?php if( !empty($search_arry['drink']) && in_array($rId, $search_arry['drink']) ) { echo "selected"; } ?> value="{{ $rId }}">{{ $name }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Drinking-->

                                                <!--Marital Status-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse100" aria-expanded="<?php if(!empty($search_arry['marital_status'])) : echo "true"; else: echo "false"; endif; ?>" class="">Marital Status<span class="fa pull-right fa-<?php if(!empty($search_arry['marital_status'])) : echo "minus"; else: echo "plus"; endif; ?>"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse100" class="panel-collapse collapse <?php if(!empty($search_arry['marital_status'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['marital_status'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        <div class="panel-body">
                                                            <select name="marital_status[]"  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" >
                                                                <option <?php if( !empty($search_arry['marital_status']) && in_array(config('constants.doesnt_matter'), $search_arry['marital_status']) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(config('constants.marital_status') as $key=>$val)
                                                                    <option <?php if( !empty($search_arry['marital_status']) && in_array($key, $search_arry['marital_status']) ) { echo "selected"; } ?> value="{{ $key }}">{{ $val }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Marital Status-->

                                                <div class="panel">
                                                    <button style="button" class="btn btn-warning">Search</button>
                                                    <a href="{{ route('new-search') }}" class="btn reset-result pull-right">Reset all</a>
                                                </div>
                                            </form>
                                            <form method="get" id="newMobSearchProfileKwDn" action="{{ route('new-search') }}">
                                                <!--Profile Text  -->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse160">Search Text<span class="fa fa-minus pull-right"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse160" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="input-group stylish-input-group">
                                                                <input type="text" name="keywordstop" id="keywordsdown-mob" value="<?php if(isset($search_arry['keywords'])) { echo $search_arry['keywords']; } ?>" class="form-control" placeholder="Enter Keywords" >
                                                                <span class="input-group-btn">
                                                                        <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
                                                                    </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--Mobile Search --}}

                        <div style="clear:both;"></div>
                        {{--<div class="infinite-scroll">--}}
                        <div class="col-sm-12">
                            @php
                                $counter = 0;
                                $ad_after_records = 2;
                            @endphp
                            @foreach($results as $user_profile)
                                <?php $unlockImg=''; ?>
                                <?php if($user_profile->privatePhotoPermissionCnt()>0) { $unlockImg='<div class="un-lock"><img src="'.URL::asset('img/unlock.png').'"></div>'; } ?>
                                <?php  $disabled_status=($user_profile->banned == 1 || $user_profile->deactivated == 1 || $user_profile->deleted_at != null || $user_profile->account_show=='N' || Auth::user()->account_show=='N' || $user_profile->isSkipped() || $user_profile->isSkippedMe() || $user_profile->isBlockedMe() || $user_profile->isBlocked())?'Y':'N'; ?>
                                <div class="profile-box">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-4 can_panel">
                                            {!! $unlockImg !!}
                                            <a href="{{$user_profile->profileLink()}}" target="_blank">
                                                <img src="{{$user_profile->getVendorProfilePic()}}" class="img-responsive img-thumbnail" alt="">
                                            </a>
                                        </div>
                                        <div class="col-sm-9 col-xs-8 can_panel">
                                            <h4><a href="{{$user_profile->profileLink()}}" target="_blank">{{$user_profile->getFirstname()}}</a> <span class="pull-right @if($user_profile->isOnline()) online @else offline @endif"><i class="fa fa-circle" aria-hidden="true"></i></span></h4>
                                            <p>{{$user_profile->getAge()}}, {{$user_profile->city->name}}, {{$user_profile->state->name}}, {{$user_profile->country->name}}</p>
                                            @if($user_profile->hasConversation())
                                                <a href="{{route('conversation', ['user_id' => $user_profile->getEncryptedId()])}}" class="s_msg"><i class="fa fa-eye" aria-hidden="true"></i> View Message</a>
                                            @else
                                               <span id="span-show-view-msg-{{$user_profile->id}}" style="display:none"><a href="{{route('conversation', ['user_id' => $user_profile->getEncryptedId()])}}" class="s_msg" ><i class="fa fa-eye" aria-hidden="true"></i> View Message</a></span>
                                               <span id="span-show-send-msg-{{$user_profile->id}}">
                                                    <a href="javascript:void(0)" id="show-send-msg-{{$user_profile->id}}" class="s_msg cls-show-snd-msg"><i class="fa fa-envelope"></i> Send Message</a>
                                                    <!-- Message Popup -->
                                                    <div class="popup-box chat-popup" id="qnimate-{{$user_profile->id}}">
                                                        <div class="popup-head">
                                                            <div class="popup-head-left pull-left">New Message :</div>
                                                            <div class="popup-head-right pull-right">
                                                                <button data-widget="remove" id="remove-msg-box-{{$user_profile->id}}" class="chat-header-button pull-right close-msg-popup" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                            </div>
                                                        </div>
                                                        <div class="popup-messages">
                                                            <div class="row">

                                                                <div class="col-sm-12">
                                                                    <div class="media">
                                                                        <div class="media-left">
                                                                            {!! $unlockImg !!}
                                                                            <a href="{{$user_profile->profileLink()}}" target="_blank"><img src="{{$user_profile->getVendorProfilePic()}}" class="media-object img-thumbnail" style="width:60px"></a>
                                                                        </div>
                                                                        <div class="media-body">
                                                                            <h4 class="media-heading">{{$user_profile->getFirstname()}}</h4>
                                                                            <p>{{$user_profile->getAge()}}, {{$user_profile->city->name}}, {{$user_profile->state->name}}, {{$user_profile->country->name}}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <span id="returnSpan{{$user_profile->id}}" style="color: red;"></span>
                                                                    <form action="javascript:void(0);" id="msg-frm-{{$user_profile->id}}" class="needs-validation" role="form" name="messageFrm" method="post">
                                                                        @csrf
                                                                        <div class="form-group">
                                                                            <textarea class="form-control sendMessageInput" id="sendMessageInput{{$user_profile->id}}" name="msg_description" @if($disabled_status=='Y') disabled="disabled" @endif  style="box-shadow:none;" data-profileid="{{$user_profile->getEncryptedId()}}" data-id="{{$user_profile->id}}" placeholder="Message" maxlength="140" rows="3" required=""></textarea>
                                                                        </div>
                                                                        <input type="hidden" name="receiver_id" value="{{$user_profile->id}}" />
                                                                        <input type="hidden" name="pageType" value="inbox">
                                                                        <input type="submit" id="sendMessage{{$user_profile->id}}" @if($disabled_status=='Y')  disabled="disabled" @endif data-profileid="{{$user_profile->getEncryptedId()}}" data-id="{{$user_profile->id}}"  class="btn btn-warning pull-right sendMessage" value="Send">
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Message Popup -->
                                                </span>
                                            @endif
                                            <p>{{$user_profile->users_information->religion->religion}}, {{config('constants.height.'.$user_profile->users_information->height)}}, @if(!is_null($user_profile->users_information->education)) {{$user_profile->users_information->education->education}}@if(!is_null($user_profile->users_information->body_type) ),@endif
                                                @endif @if(!is_null($user_profile->users_information->body_type) ) {{$user_profile->users_information->body_type}}@endif</p>
                                            <p class="hidden-xs">@if(!is_null($user_profile->users_information->about)){{ showReadMore($user_profile->users_information->about, 90, $user_profile->profileLink()) }}@endif</p>
                                        </div>
                                    </div>
                                </div>
                                @if(($counter + 1) % $ad_after_records == 0)
                                    @if(getBannerByType('Search') != "")
                                        <div class="profile-box">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    {!!getBannerByType('Search')!!}
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                @php
                                    $counter++;   
                                @endphp
                            @endforeach
                        </div>
                        <div class="col-sm-12">{{ $results->appends($search_arry)->links() }}</div>
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script>

        var url = document.location.toString();
        if ( url.match('#') ) {
            if(url.split('#')[1]!="partnerbasic") {
                $('.' + url.split('#')[1]).trigger('click');
            }
        }

        $('.collapse').on('shown.bs.collapse',function(){
            $(this).parent().find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
        }).on('hidden.bs.collapse',function(){
            $(this).parent().find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
        });

        $(function(){
            // Age slider
            $( "#slider-age" ).slider({
                range: true,
                min: <?php echo config('constants.search_defaults.start_age'); ?>,
                max: <?php echo config('constants.search_defaults.end_age'); ?>,
                values: [<?php echo (int) $age_from; ?>, <?php echo (int) $age_to; ?>],
                slide: function( event, ui ) {
                    $( "#age" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ]);
                },
                stop : function(event, ui)
                {
                    $( "#age" ).val($( "#slider-age" ).slider( "values", 0 ) + " - " + $( "#slider-age" ).slider( "values", 1 ) );

                }
            });
            $( "#age" ).val($( "#slider-age" ).slider( "values", 0 ) + " - " + $( "#slider-age" ).slider( "values", 1 ) );
            // Mobile age
            $( "#slider-age-mob" ).slider({
                range: true,
                min: <?php echo config('constants.search_defaults.start_age'); ?>,
                max: <?php echo config('constants.search_defaults.end_age'); ?>,
                values: [<?php echo (int) $age_from; ?>, <?php echo (int) $age_to; ?>],
                slide: function( event, ui ) {
                    $( "#age-mob" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ]);
                },
                stop : function(event, ui)
                {
                    $( "#age-mob" ).val($( "#slider-age-mob" ).slider( "values", 0 ) + " - " + $( "#slider-age-mob" ).slider( "values", 1 ) );

                }
            });
            $( "#age-mob" ).val($( "#slider-age-mob" ).slider( "values", 0 ) + " - " + $( "#slider-age-mob" ).slider( "values", 1 ) );
            // Height slider
            $( "#slider-height" ).slider({
                range: true,
                min: <?php echo config('constants.search_defaults.start_height'); ?>,
                max: <?php echo config('constants.search_defaults.end_height'); ?>,
                values: [<?php echo (int) $height_from; ?>, <?php echo (int) $height_to; ?>],
                slide: function( event, ui ) {
                    $("#height").val(ui.values[ 0 ] + " cm " + " - " + ui.values[ 1 ] + " cm"  );
                },
                stop : function(event, ui)
                {
                    $( "#height" ).val($( "#slider-height" ).slider( "values", 0 ) + " cm " + " - " + $( "#slider-height" ).slider( "values", 1 )+ " cm " );

                }
            });
            $( "#height" ).val($( "#slider-height" ).slider( "values", 0 ) + " cm " + " - " + $( "#slider-height" ).slider( "values", 1 )+ " cm " );
            // Mobile Height slider
            $( "#slider-height-mob" ).slider({
                range: true,
                min: <?php echo config('constants.search_defaults.start_height'); ?>,
                max: <?php echo config('constants.search_defaults.end_height'); ?>,
                values: [<?php echo (int) $height_from; ?>, <?php echo (int) $height_to; ?>],
                slide: function( event, ui ) {
                    $("#height-mob").val(ui.values[ 0 ] + " cm " + " - " + ui.values[ 1 ] + " cm"  );
                },
                stop : function(event, ui)
                {
                    $( "#height-mob" ).val($( "#slider-height-mob" ).slider( "values", 0 ) + " cm " + " - " + $( "#slider-height-mob" ).slider( "values", 1 )+ " cm " );

                }
            });
            $( "#height-mob" ).val($( "#slider-height-mob" ).slider( "values", 0 ) + " cm " + " - " + $( "#slider-height-mob" ).slider( "values", 1 )+ " cm " );
            // Message Popupbox
            $(".cls-show-snd-msg").click(function () {
                var link_div_str = this.id;
                var user_id = link_div_str.substring(14);
                $('.chat-popup').removeClass('popup-box-on');
                $('#qnimate-'+user_id).addClass('popup-box-on');
            });

            $(".close-msg-popup").click(function () {
                var close_div_str = this.id;
                var user_id = close_div_str.substring(15);
                $('#qnimate-'+user_id).removeClass('popup-box-on');
            });
        });

        {{--/** For infinite scroll pagination */--}}
        {{--$('ul.pagination').hide();--}}
        {{--$(function() {--}}
        {{--$('.infinite-scroll').jscroll({--}}
        {{--autoTrigger: true,--}}
        {{--loadingHtml: '<img class="center-block" src="{{asset('img/loader.gif')}}" alt="Loading..." />',--}}
        {{--padding: 0,--}}
        {{--nextSelector: '.pagination li.active + li a',--}}
        {{--contentSelector: 'div.infinite-scroll',--}}
        {{--callback: function() {--}}
        {{--$('ul.pagination').remove();--}}
        {{--}--}}
        {{--});--}}
        {{--});--}}
        $("#mobile-search-modal").on("show", function () {
            $("body").addClass("modal-open");
        }).on("hidden", function () {
            $("body").removeClass("modal-open")
        });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
