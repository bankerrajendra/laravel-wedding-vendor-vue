@extends('layouts.app')

@section('template_title')
    Deactivate Account
@endsection

@section('template_fastload_css')
    .help-block1, .error-help-block1{
    margin-top: 5px;
    margin-bottom: 10px;
    color: #a94442
    }

    hr{
        border-top: 1px solid #ccc;
    }


@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div id="page-content-wrapper">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 ">
                        <div class="row">
                            @include('partials.user-sidebar-navigation')
                               <div class="col-sm-8 col-sm-offset-4 panel-body menu">
                                <br>
                                <div class="panel panel-body partner ">
                                    <h4>Deactivate Account</h4>
                                    <hr>
                                    <p>Please bear in mind that deactivating your Account removes all information associated with your account from search and will no longer be viewable by other members.</p>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Deactivate Account</button>
                                    <br>
                                    <div id="myModal" class="modal fade text-center" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" id="resetPublic" value="reset">×</button>
                                                    <h4 class="modal-title" id="avatar-modal-label">Deactivate Account</h4>
                                                </div>

                                                <div class="modal-body">
                                                    <p>If you're just hoping to take a break, we recommend disabling your account, not deleting it.</p>
                                                    <p><b>Disable your account</b></p>
                                                    <p>Your profile and photos will be removed from the site, but we'll keep everything for you in case you return so that you can start right where you left off.</p>
                                                    <p>To reactivate your account, simply log back in at any time.</p>
                                                    <a class="btn btn-danger" href="{{route('disable-account')}}">Disable Account</a>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>








                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{--<section>--}}
    {{--<div class="container">--}}
    {{--<div class="col-12 col-lg-10 offset-lg-1" style="min-height: 600px;">--}}

    {{--@include('panels.welcome-panel')--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

@endsection
