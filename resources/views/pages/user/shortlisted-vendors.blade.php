@extends('layouts.app')
@section('template_linked_css')
    <link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 hidden-xs">
                    @include('partials.user-side-menu')
                </div>
                <div class="col-sm-8 msg-section">
                    <div class="row hidden-xs">
                        <div class="col-sm-12 col-xs-12"><h4>Shortlist Vendors</h4></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 no_padding">
                            <div id="results-shortlisted-vendors">
                            @include('pages.user.ajax.show-shortlisted')
                            </div>
                            @if($shortlisted->total() > $per_page)
                            <div class="text-center" style="margin-top: 10px;">
                                <a href="javascript:void(0);" id="next-page" data-next-page="2" class="btn btn-default">Show More </a>
                                <img src="{{asset('img/loader.gif')}}" id="spinner-load" width="40" style="display: none;">
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
var ajaxResultGetRequest = null
$("#next-page").on('click', function() {
    var url = route('shortlisted-vendors')+'?page='+$(this).data('next-page');
    ajaxResultGetRequest = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            // show loading image
            $('#next-page').hide();
            $('#spinner-load').show();
        },
        success: function(result) {
            // show results
            $('#results-shortlisted-vendors').append(result.shortlisted);
            $('#next-page').data('next-page', result.next_page);
            if(result.next_url == '') {
                $('#next-page').hide();
            } else {
                $('#next-page').show();
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            // hide loading image
            $('#spinner-load').hide();
            ajaxResultGetRequest = null;
        }
    });
});
var ajaxFinalize = null;
function doFinalize(vendor_id)
{
    var url = route('short-list-finalize-vendor-ajax')+'?vendor_id='+vendor_id+'&finalize=1';
    ajaxFinalize = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {},
        success: function(result) {
            if(result.status == 1) {
                $('#shortlist-id-'+vendor_id).fadeOut(300, function(){ 
                    $(this).remove();
                });
            } else {
                console.log(result.message);
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            ajaxFinalize = null;
        }
    });
}
var ajaxShortList = null;
function doDelete(vendor_id)
{
    var url = route('short-list-vendor-ajax')+'?vendor_id='+vendor_id+'&shortlist=0';
    ajaxShortList = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
        },
        success: function(result) {
            if(result.status == 1) {
                $('#shortlist-id-'+vendor_id).fadeOut(300, function(){ 
                    $(this).remove();
                });
            } else {
                console.log(result.message);
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            ajaxShortList = null;
        }
    });
}
</script>
@endsection