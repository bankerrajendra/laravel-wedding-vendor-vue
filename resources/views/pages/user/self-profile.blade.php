@extends('layouts.app')

@section('template_title')
    Self Profile
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_fastload_css')
@endsection

@section('content')
    <div class="self-profile">
        <div class="container">
            <div class="row">
               @include('partials.my-profile-navigation')

                <div class="col-sm-8 panel-body">
                    <div class="row">
                        <div class="col-sm-12 profile_n hidden-sm hidden-lg hidden-md text-center">
                            <h3>{{$userProfile->first_name.", ".$userProfile->getAge()." ". $userProfile->city->name}}</h3>
                        </div>
                        <div class="col-sm-4 text-center">
                            <div class="profile-img">
                                <img src="{{$userProfile->getVendorProfilePic()}}" class="img-responsive img-thumbnail" style="width:100%;">
                                <a href="{{route('my-photos')}}" class="photo-icon">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <h3 class="hidden-xs">{{$userProfile->first_name}}</h3>
                            <div class="row hidden-sm hidden-lg hidden-md">
                                <div class="col-xs-6 panel-body">
                                    <a href="{{route('my-profile')}}" class="btn btn-warning btn-block"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp; Edit Personal</a>
                                </div>
                                <div class="col-xs-6 panel-body">
                                    <a href="{{route('partner-preference')}}" class="btn btn-success btn-block"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp; Edit Partner</a>
                                </div>
                            </div>
                            <hr class="mm">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td><b>Age/Height :</b></td>
                                    <td>{{$userProfile->getAge()}} / {{config('constants.height.'.$userProfile->users_information->height)}}</td>
                                </tr>

                                @if(!is_null($userProfile->users_information->marital_status))
                                    <tr>
                                        <td><b>Marital Status</b></td>
                                        <td>{{config('constants.marital_status.'.$userProfile->users_information->marital_status)}}</td>
                                    </tr>
                                @endif

                                @if(!is_null($userProfile->users_information->profile_created_by))
                                    <tr>
                                        <td><b>Posted by :</b></td>
                                        <td>{{config('constants.profile_created_by.'.$userProfile->users_information->profile_created_by)}}</td>
                                    </tr>
                                @endif

                                <tr>
                                    <td><b>Religion  :</b></td>
                                    <td>{{$userProfile->users_information->religion->religion}}</td>
                                </tr>

                                <tr>
                                    <td><b>Location :</b></td>
                                    <td>{{$userProfile->country->name}}, {{$userProfile->city->name}}</td>
                                </tr>

                                @if(!is_null($userProfile->users_information->language_id))
                                    <tr>
                                        <td><b>Language :</b></td>
                                        <td>{{$userProfile->users_information->get_own_language()}}</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-12">
                            <hr>
                        </div>
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Personal Profile</a></li>
                                <li><a data-toggle="tab" href="#menu1">Partner Preference</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="col-sm-12 panel-body">

                                    @if(!is_null($userProfile->users_information->about))
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">About Me <a href="{{route('my-profile')}}#aboutme"  class="pull-right">Edit</a></h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                   <p> {{$userProfile->users_information->about}} </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(!is_null($userProfile->users_information->looking_for))
                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">What I am looking For <a href="{{route('my-profile')}}#lookingfor" data-toggle="" class="pull-right">Edit</a></h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <p> {{$userProfile->users_information->looking_for}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Basic Info <a href="{{route('my-profile')}}" data-toggle="" class="pull-right">Edit</a></h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12 col-lg-8">
                                                    <table class="table">
                                                        <tbody>
                                                        <tr>
                                                            <td>Date of Birth :</td>
                                                            <td>{{date('Y-m-d', strtotime($userProfile->date_of_birth))}}</td>
                                                        </tr>

                                                        @if(!is_null($userProfile->users_information->gender))
                                                        <tr>
                                                            <td>Gender :</td>
                                                            <td>{{config('constants.gender.'.$userProfile->gender)}}</td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userProfile->users_information->marital_status))
                                                            <tr>
                                                                <td>Marital Status</td>
                                                                <td>{{config('constants.marital_status.'.$userProfile->users_information->marital_status)}}</td>
                                                            </tr>
                                                        @endif

                                                        @if(!is_null($userProfile->users_information->profile_created_by))
                                                            <tr>
                                                                <td>Profile Created By :</td>
                                                                <td>{{config('constants.profile_created_by.'.$userProfile->users_information->profile_created_by)}}</td>
                                                            </tr>
                                                        @endif

                                                        @if(!is_null($userProfile->users_information->body_type))
                                                        <tr>
                                                            <td>Body Type :</td>
                                                            <td>{{$userProfile->users_information->body_type}}</td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userProfile->users_information->weight))
                                                        <tr>
                                                            <td>Body Weight :</td>
                                                            <td>{{$userProfile->users_information->weight}} {{$userProfile->users_information->weight_measure}}</td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userProfile->users_information->complexion))
                                                        <tr>
                                                            <td>Complexion :</td>
                                                            <td>{{$userProfile->users_information->complexion}}</td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userProfile->users_information->eye_color))
                                                        <tr>
                                                            <td>Eye Color :</td>
                                                            <td>{{$userProfile->users_information->eye_color}}</td>
                                                        </tr>
                                                        @endif

                                                        <tr>
                                                            <td>Country :</td>
                                                            <td>{{$userProfile->country->name}}</td>
                                                        </tr>

                                                        <tr>
                                                            <td>State :</td>
                                                            <td>{{$userProfile->state->name}}</td>
                                                        </tr>

                                                        <tr>
                                                            <td>City :</td>
                                                            <td>{{$userProfile->city->name}}</td>
                                                        </tr>

                                                        @if(!is_null($userProfile->users_information->zipcode))
                                                        <tr>
                                                            <td>Pincode :</td>
                                                            <td>{{$userProfile->users_information->zipcode}}</td>
                                                        </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Lifestyle <a href="{{route('my-profile')}}#lifestyle" data-toggle="" class="pull-right">Edit</a></h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12 col-lg-5">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>Food Preference :</td>
                                                                <td>{{config('constants.food.'.$userProfile->users_information->food)}} </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Drink :</td>
                                                                <td>{{config('constants.drink.'.$userProfile->users_information->drink)}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td>Smoke :</td>
                                                                <td>{{config('constants.smoke.'.$userProfile->users_information->smoke)}}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Family Details &amp; Religious Background <a href="{{route('my-profile')}}#familybackgroud" data-toggle="" class="pull-right">Edit</a></h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-9 col-xs-9 col-lg-8">
                                                    <table class="table">
                                                        <tbody><tr>
                                                            <td>Religion :</td>
                                                            <td>{{$userProfile->users_information->religion->religion}}</td>
                                                        </tr>

                                                        @if(!is_null($userProfile->users_information->language_id))
                                                            <tr>
                                                                <td>Language :</td>
                                                                <td>{{$userProfile->users_information->get_own_language()}}</td>
                                                            </tr>
                                                        @endif

                                                        <tr>
                                                            <td>Sect/Math-hab :</td>
                                                            <td>{{$userProfile->users_information->get_community()}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Sub Caste :</td>
                                                            <td>{{$userProfile->users_information->get_subcaste()}}</td>
                                                        </tr>

                                                        @if(!is_null($userProfile->users_information->does_pray))
                                                        <tr>
                                                            <td>Do You Pray? :</td>
                                                            <td>{{config('constants.do_you_pray.'.$userProfile->users_information->does_pray)}}</td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userProfile->users_information->is_born))
                                                        <tr>
                                                            <td>Religious History? :</td>
                                                            <td>{{config('constants.born_reverted.'.$userProfile->users_information->is_born)}}</td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userProfile->users_information->horoscope))
                                                        <tr>
                                                            <td>Horoscope:</td>
                                                            <td>{{$userProfile->users_information->horoscope}}</td>
                                                        </tr>
                                                        @endif

                                                        </tbody></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Education &amp; Career <a href="{{route('my-profile')}}#education" data-toggle="" class="pull-right">Edit</a></h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12 col-lg-10">
                                                    <table class="table">
                                                        <tbody>
                                                        @if(!is_null($userProfile->users_information->profession))
                                                            <tr>
                                                                <td>Employment</td>
                                                                <td>{{$userProfile->users_information->profession->profession}}</td>
                                                            </tr>
                                                        @endif

                                                        @if(!is_null($userProfile->users_information->annual_income))
                                                            <tr>
                                                                <td>Annual income :</td>
                                                                <td>@if($userProfile->users_information->annual_income!='Dont want to specify') {{$userProfile->users_information->currency}} @endif {{ config('constants.annual_income.'.$userProfile->users_information->annual_income)}}</td>
                                                            </tr>
                                                        @endif
                                                        @if(!is_null($userProfile->users_information->education))
                                                            <tr>
                                                                <td>Education</td>
                                                                <td>{{$userProfile->users_information->education->education}}</td>
                                                            </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Hobbies, Interests &amp; more <a href="{{route('my-hobbies')}}" data-toggle="" class="pull-right">Edit</a></h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table">
                                                        <tbody>

                                                        <?php $hobbies=$userProfile->users_information->hobbies; ?>
                                                        @if(!is_null($hobbies))
                                                            <?php $hobbiesArr=explode(',',$hobbies);
                                                                $arr=array();
                                                                foreach ($hobbiesArr as $key=>$hobby) {
                                                                    array_push($arr,config('constants.hobbies.'.trim($hobby)));
                                                                }
                                                            ?>
                                                            <tr>
                                                                <td>Hobbies  :</td>
                                                                <td>{{implode(",",$arr)}}</td>
                                                            </tr>
                                                        @endif

                                                        <?php $interests=$userProfile->users_information->interests; ?>
                                                        @if(!is_null($interests))
                                                            <?php $interestsArr=explode(',',$interests);
                                                            $arr=array();
                                                            foreach ($interestsArr as $key=>$hobby) {
                                                                array_push($arr,config('constants.interests.'.trim($hobby)));
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td>Interests  :</td>
                                                                <td>{{implode(",",$arr)}}</td>
                                                            </tr>
                                                        @endif

                                                        <?php $favourite_music=$userProfile->users_information->favourite_music; ?>
                                                        @if(!is_null($favourite_music))
                                                            <?php $favourite_musicArr=explode(',',$favourite_music);
                                                            $arr=array();
                                                            foreach ($favourite_musicArr as $key=>$hobby) {
                                                                array_push($arr,config('constants.favourite_music.'.trim($hobby)));
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td>Favourite Music  :</td>
                                                                <td>{{implode(",",$arr)}}</td>
                                                            </tr>
                                                        @endif

                                                        <?php $favourite_reads=$userProfile->users_information->favourite_reads; ?>
                                                        @if(!is_null($favourite_reads))
                                                            <?php $favourite_readsArr=explode(',',$favourite_reads);
                                                            $arr=array();
                                                            foreach ($favourite_readsArr as $key=>$hobby) {
                                                                array_push($arr,config('constants.favourite_reads.'.trim($hobby)));
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td>Favourite Reads  :</td>
                                                                <td>{{implode(",",$arr)}}</td>
                                                            </tr>
                                                        @endif

                                                        <?php $preferred_movies=$userProfile->users_information->preferred_movies; ?>
                                                        @if(!is_null($preferred_movies))
                                                            <?php $preferred_moviesArr=explode(',',$preferred_movies);
                                                            $arr=array();
                                                            foreach ($preferred_moviesArr as $key=>$hobby) {
                                                                array_push($arr,config('constants.preferred_movies.'.trim($hobby)));
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td>Preferred Movies  :</td>
                                                                <td>{{implode(",",$arr)}}</td>
                                                            </tr>
                                                        @endif

                                                        <?php $sports=$userProfile->users_information->sports; ?>
                                                        @if(!is_null($sports))
                                                            <?php $sportsArr=explode(',',$sports);
                                                            $arr=array();
                                                            foreach ($sportsArr as $key=>$hobby) {
                                                                array_push($arr,config('constants.sports.'.trim($hobby)));
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td>Sports  :</td>
                                                                <td>{{implode(",",$arr)}}</td>
                                                            </tr>
                                                        @endif

                                                        <?php $spoken_languages=$userProfile->users_information->spoken_languages; ?>
                                                        @if(!is_null($spoken_languages))
                                                            <?php  $spoken_languagesArr=get_spoken_languages($spoken_languages);
                                                            $arr=array();
                                                            foreach ($spoken_languagesArr as $language) {
                                                                array_push($arr,$language->language);
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td>Sports  :</td>
                                                                <td>{{implode(",",$arr)}}</td>
                                                            </tr>
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php $userPref = $userProfile->users_preferences();?>
                            <div id="menu1" class="tab-pane fade">
                                <div class="col-sm-12 panel-body">
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Basic Info<a href="{{route('partner-preference')}}" data-toggle="" class="pull-right">Edit</a></h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12 col-lg-8">
                                                    <table class="table">
                                                        <tbody>
                                                        <tr>
                                                            <td>Age:</td>
                                                            <td>{{$userPref['age_from']}} to {{$userPref['age_to']}} Years</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Height :</td>
                                                            <td>{{config('constants.height.'.$userPref['height_from'])}} to {{config('constants.height.'.$userPref['height_to'])}}</td>
                                                        </tr>

                                                        @if(!is_null($userPref['marital_status']))
                                                        <tr>
                                                            <td>Marital Status  :</td>
                                                            <td><?php if($userPref['marital_status']=='DM') { echo "Doesn't Matter"; } else {
                                                                $arr=array();
                                                                foreach(explode(",",$userPref['marital_status']) as $val) {
                                                                    array_push($arr,config('constants.marital_status.'.$val));
                                                                }
                                                                echo implode(", ", $arr);
                                                              } ?>
                                                            </td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['religion']))
                                                        <tr>
                                                            <td>Religion :</td>
                                                            <td><?php if($userPref['religion']=='DM') { echo "Doesn't Matter"; } else {
                                                                  echo   getReligions()[$userPref['religion']];
                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['languages']))
                                                        <tr>
                                                            <td>Language :</td>
                                                            <td><?php if($userPref['languages']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $language = explode(",", $userPref['languages']);
                                                                    $arr=array();
                                                                    foreach(getLanguages() as $ed) {
                                                                        if(in_array($ed->id, $language)) {
                                                                            array_push($arr,$ed->language);
                                                                        }
                                                                    }
                                                                    echo implode(", ", $arr);

                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['community']))
                                                        <tr>
                                                            <td>Sect :</td>
                                                            <td><?php if($userPref['community']=='DM') { echo "Doesn't Matter"; } else {

                                                                    $sect = explode(",", $userPref['community']);
                                                                    $arr=array();
                                                                    foreach(getSubcastes() as $ed) {
                                                                        if(in_array($ed->id, $sect)) {
                                                                            array_push($arr,$ed->name);
                                                                        }
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['body_type']))
                                                        <tr>
                                                            <td>Body Type :</td>
                                                            <td><?php if($userPref['body_type']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $arr=array();
                                                                    foreach(explode(",",$userPref['body_type']) as $val) {
                                                                        array_push($arr,config('constants.body_type.'.$val));
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['eye_color']))
                                                        <tr>
                                                            <td>Eye Color :</td>
                                                            <td><?php if($userPref['eye_color']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $arr=array();
                                                                    foreach(explode(",",$userPref['eye_color']) as $val) {
                                                                        array_push($arr,config('constants.eye_color.'.$val));
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['skin_tone']))
                                                        <tr>
                                                            <td>Complexion :</td>
                                                            <td><?php if($userPref['skin_tone']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $arr=array();
                                                                    foreach(explode(",",$userPref['skin_tone']) as $val) {
                                                                        array_push($arr,config('constants.skin_tone.'.$val));
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['country_living']))
                                                        <tr>
                                                            <td>Country :</td>
                                                            <td><?php if($userPref['country_living']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $country = explode(",", $userPref['country_living']);
                                                                    $arr=array();
                                                                    foreach(getCountry() as $ed) {
                                                                        if(in_array($ed->id, $country)) {
                                                                            array_push($arr,$ed->name);
                                                                        }
                                                                    }
                                                                    echo implode(", ", $arr);

                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['state_living']))
                                                        <tr>
                                                            <td>State :</td>
                                                            <td><?php if($userPref['state_living']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $state = explode(",", $userPref['state_living']);
                                                                    $arr=array();
                                                                    foreach(getStateByCountry($country) as $ed) {
                                                                        if(in_array($ed->id, $state)) {
                                                                            array_push($arr,$ed->name);
                                                                        }
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['city_district']))
                                                        <tr>
                                                            <td>City :</td>
                                                            <td><?php if($userPref['city_district']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $city = explode(",", $userPref['state_living']);
                                                                    $arr=array();
                                                                    foreach(getCityByState($state) as $ed) {
                                                                        if(in_array($ed->id, $city)) {
                                                                            array_push($arr,$ed->name);
                                                                        }
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?></td>
                                                        </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if(!is_null($userPref['diet']) || !is_null($userPref['drink']) || !is_null($userPref['smoke']))
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Lifestyle <a href="{{route('partner-preference')}}#edit_partner-lifestyle" data-toggle="" class="pull-right">Edit</a></h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12 col-lg-5">
                                                    <table class="table">
                                                        <tbody>

                                                        @if(!is_null($userPref['diet']))
                                                        <tr>
                                                            <td>Food Preference :</td>
                                                            <td><?php if($userPref['diet']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $arr=array();
                                                                    foreach(explode(",",$userPref['diet']) as $val) {
                                                                        array_push($arr,config('constants.food.'.$val));
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['drink']))
                                                        <tr>
                                                            <td>Drink :</td>
                                                            <td><?php if($userPref['drink']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $arr=array();
                                                                    foreach(explode(",",$userPref['drink']) as $val) {
                                                                        array_push($arr,config('constants.drink.'.$val));
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?></td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['smoke']))
                                                        <tr>
                                                            <td>Smoke :</td>
                                                            <td><?php if($userPref['smoke']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $arr=array();
                                                                    foreach(explode(",",$userPref['smoke']) as $val) {
                                                                        array_push($arr,config('constants.smoke.'.$val));
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?></td>
                                                        </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(!is_null($userPref['professional_area']) || !is_null($userPref['annual_income']) || !is_null($userPref['education']))
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Education &amp; Career <a href="{{route('partner-preference')}}#edit_partner-education" data-toggle="" class="pull-right">Edit</a></h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12 col-lg-8">
                                                    <table class="table">
                                                        <tbody>

                                                        @if(!is_null($userPref['professional_area']))
                                                        <tr>
                                                            <td>Employment :</td>
                                                            <td><?php if($userPref['professional_area']=='DM') { echo "Doesn't Matter"; } else {

                                                                    $profession = explode(",", $userPref['professional_area']);
                                                                    $arr=array();
                                                                    foreach(getProfession() as $ed) {
                                                                        if(in_array($ed->id, $profession)) {
                                                                            array_push($arr,$ed->profession);
                                                                        }
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['annual_income']))
                                                        <tr>
                                                            <td>Annual income :</td>
                                                            <td><?php
                                                                if($userPref['annual_income']=='DM') { echo "Doesn't Matter"; } else {
                                                                   echo $userPref['currency']. " ". config('constants.annual_income.'.$userPref['annual_income']);
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                        @endif

                                                        @if(!is_null($userPref['education']))
                                                        <tr>
                                                            <td>Education :</td>
                                                            <td><?php if($userPref['education']=='DM') { echo "Doesn't Matter"; } else {
                                                                    $education = explode(",", $userPref['education']);
                                                                    $arr=array();
                                                                    foreach(getEducation() as $ed) {
                                                                        if(in_array($ed->id, $education)) {
                                                                            array_push($arr,$ed->education);
                                                                        }
                                                                    }
                                                                    echo implode(", ", $arr);
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer_scripts')

@endsection