@extends('layouts.app')

@section('template_title')
    Event Not Found
@endsection

@section('template_fastload_css')
    .help-block1, .error-help-block1{
    margin-top: 5px;
    margin-bottom: 10px;
    color: #a94442
    }

@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div id="page-content-wrapper">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-sm-offset-1 col-s margin-top-0-conditional">
                        <div class="row">
                            <div class="col-sm-10 panel-body">
                                <div class="alert alert-info" role="alert">
                                    <div style="text-align:center; padding-top:4px; color:#31708f">

                                        <h2>Event Not Found</h2>

                                        <p>This Event cannot be found.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection