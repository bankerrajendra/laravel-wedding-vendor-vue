@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('template_linked_css')
<link href="{{asset('css/bootstrap-datepicker')}}.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="login_p signup">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h1><span>Request</span> a Quote</h1>
                <p>Yay! You’re on your way to creating a New Vendor listing on the world’s biggest wedding resource.Simply fill out the below information.</p>
            </div>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="tabbable-line">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form name="frm-submit-request-a-quote" method="POST" action="{{ route('user-choose-vendor') }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    Describe Your Wedding
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="tabbable-line">
                                    <div class="row">
                                        <div class="form-group" style="margin-top:-15px;">
                                            <div class="col-sm-3 col-xs-6 panel_body">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="wedding_type_hindu" value="1" @if(session('wedding_type') == 'Hindu')checked="checked"@endif><span class="checkmark"></span> Hindu
                                            </label>
                                            </div>
                                            
                                            <div class="col-sm-3 col-xs-6 panel_body">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="wedding_type_muslim" value="1" @if(session('wedding_type') == 'Muslim')checked="checked"@endif><span class="checkmark"></span> Muslim
                                            </label>
                                            </div>
                                            
                                            <div class="col-sm-3 col-xs-6 panel_body">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="wedding_type_sikh" value="1" @if(session('wedding_type') == 'Sikh')checked="checked"@endif><span class="checkmark"></span> Sikh
                                            </label>
                                            </div>
                                            <div class="col-sm-3 col-xs-6 panel_body">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="wedding_type_christian" value="1" @if(session('wedding_type') == 'Christian')checked="checked"@endif><span class="checkmark"></span> Christian
                                            </label>
                                            </div>
                                            <span id="select-wedding-type-error" style="display: none; color: #a94442;padding-left: 15px;">The Wedding Type field is required.</span>
                                        </div>
                                    </div>	
                                </div>
                            </div>
                        </div>
                            
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    EVENT INFORMATION
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="no_of_guest">Number Of Guests</label>
                                    <input type="text" name="no_of_guest" id="no_of_guest" value="{{session('no_of_guest')}}" class="form-control" placeholder="Number Of Guests">
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="event-date">Event Date</label>
                                    <div id="filterDate2">
                                    <!-- Datepicker as text field -->         
                                        <div class="input-group date" data-date-format="dd.mm.yyyy">
                                            <input name="event_date" id="event-date" type="text" class="form-control" placeholder="dd.mm.yyyy" value="{{session('event_date')}}" autocomplete="off">
                                            <div class="input-group-addon" >
                                                <span class="fa fa-calendar"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="mobile-number">Phone Number</label>
                                    <input type="text" id="mobile-number" name="mobile_number" class="form-control" placeholder="Phone Number" value="{{session('mobile_number')}}" />
                                    <span id="mobile-number-error" style="display: none; color: #a94442;">Please enter Phone Number.</span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Preferred Contact Method *</label>
                                    <br />
                                    <label class="checkbox-inline"><input type="checkbox" id="check_email_contact" name="check_email_contact" value="1" @if(session('preferred_contact_method') == 'email')checked="checked"@endif><span class="checkmark email-chk"></span> Email</label>
                                    <label class="checkbox-inline"><input type="checkbox" id="check_phone_contact" name="check_phone_contact" value="1" @if(session('preferred_contact_method') == 'phone')checked="checked"@endif><span class="checkmark phone-chk"></span> Phone</label>
                                    <label class="checkbox-inline"><input type="checkbox" id="check_message_contact" name="check_message_contact" value="1" @if(session('preferred_contact_method') == 'message')checked="checked"@endif><span class="checkmark message-chk"></span> Text Message</label>
                                    <div id="preferred-contact-error" style="display: none; color: #a94442;float: left">Please select atleast one Preferred Contact Method.</div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel_head text-center">
                                    User location
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="country">Select Country *</label>
                                    <select name="country" id="country" class="form-control load-dependent" required="" aria-required="true" dependent="state">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            @if (session('country') == $country->id)
                                                <option value="{{$country->id}}" id="{{$country->id}}" selected>{{$country->name}}</option>
                                            @else
                                                <option value="{{$country->id}}" id="{{$country->id}}">{{$country->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="scountry">Select State *</label>
                                    <select name="state" id="state" class="form-control load-dependent" required="" dependent="city" aria-required="true">
                                        <option value="">Select State</option>
                                        @if(session('country') != '' && session('state') != '')
                                            @foreach(getStateByCountry(session('country')) as $ct)
                                                <option value="{{ $ct->id }}" {{ session('state') == $ct->id ? "selected":"" }}>{{ $ct->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <i class='state-dropdown-loader' style="display: none;">loading...</i>
                                </div>
                            </div>
                        </div>	
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="city">City/Town *</label>
                                    <select name="city" id="city" class="form-control" required="" aria-required="true">
                                        <option value="">Select City</option>
                                        @if(session('country') != '' && session('state') != '' && session('city') != '')
                                            @foreach(getCityByState(session('state')) as $city)
                                                <option value="{{ $city->id }}" {{session('city') == $city->id ? "selected":"" }}>{{ $city->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <i class='city-dropdown-loader' style="display: none;">loading...</i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                {{-- <div class="form-group">
                                    <label for="city">Zip/Pin/Area Code *</label>
                                    <input type="text" name="Zip Code" id="Zip Code" class="form-control">
                                </div> --}}
                            </div>
                        </div>
                        <button type="submit" name="submit" class="btn btn-default btn-block">NEXT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_scripts')
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
var wedding_type_hindu = $("input[name='wedding_type_hindu']");
var wedding_type_muslim = $("input[name='wedding_type_muslim']");
var wedding_type_sikh = $("input[name='wedding_type_sikh']");
var wedding_type_christian = $("input[name='wedding_type_christian']");

var email_chkbox = $("#check_email_contact");
var phone_chkbox = $("#check_phone_contact");
var message_chkbox = $("#check_message_contact");
$(document).ready(function() {
    wedding_type_hindu.on('change', function() {
        $("#select-wedding-type-error").hide();
        if(wedding_type_hindu.is(':checked') == true) {
            wedding_type_muslim.prop("checked", false);
            wedding_type_sikh.prop("checked", false);
            wedding_type_christian.prop("checked", false);
        }
    });
    wedding_type_muslim.on('change', function() {
        $("#select-wedding-type-error").hide();
        if(wedding_type_muslim.is(':checked') == true) {
            wedding_type_hindu.prop("checked", false);
            wedding_type_sikh.prop("checked", false);
            wedding_type_christian.prop("checked", false);
        }
    });
    wedding_type_sikh.on('change', function() {
        $("#select-wedding-type-error").hide();
        if(wedding_type_sikh.is(':checked') == true) {
            wedding_type_hindu.prop("checked", false);
            wedding_type_muslim.prop("checked", false);
            wedding_type_christian.prop("checked", false);
        }
    });
    wedding_type_christian.on('change', function() {
        $("#select-wedding-type-error").hide();
        if(wedding_type_christian.is(':checked') == true) {
            wedding_type_hindu.prop("checked", false);
            wedding_type_muslim.prop("checked", false);
            wedding_type_sikh.prop("checked", false);
        }
    });

    email_chkbox.on('change', function() {
        $("#preferred-contact-error").hide();
        if(email_chkbox.is(':checked') == true) {
            phone_chkbox.prop("checked", false);
            message_chkbox.prop("checked", false);
        }
    });
    phone_chkbox.on('change', function() {
        $("#preferred-contact-error").hide();
        if(phone_chkbox.is(':checked') == true) {
            email_chkbox.prop("checked", false);
            message_chkbox.prop("checked", false);
        }
    });
    message_chkbox.on('change', function() {
        $("#preferred-contact-error").hide();
        if(message_chkbox.is(':checked') == true) {
            email_chkbox.prop("checked", false);
            phone_chkbox.prop("checked", false);
        }
    });
    $("#mobile-number").on('keyup', function() {
        $("#mobile-number-error").hide();
    });
    $('form[name="frm-submit-request-a-quote"]').submit(function (e) {
        var error = false;
        $("#select-wedding-type-error, #mobile-number-error, #preferred-contact-error").hide();
        if(wedding_type_hindu.is(':checked') == false && wedding_type_muslim.is(':checked') == false && wedding_type_sikh.is(':checked') == false && wedding_type_christian.is(':checked') == false) {
            $("#select-wedding-type-error").show();
            error = true;
        } else {
            $("#select-wedding-type-error").hide();
        }
        if(email_chkbox.is(':checked') == false && phone_chkbox.is(':checked') == false && message_chkbox.is(':checked') == false) {
            $("#preferred-contact-error").show();
            error = true;
        } else {
            $("#preferred-contact-error").hide();
        }
        if(phone_chkbox.is(':checked') == true) {
            if($('form[name="frm-submit-request-a-quote"] input[name="mobile_number"]').val() == '') {
                    $("#mobile-number-error").show();
                    error = true;
            } else {
                    $("#mobile-number-error").hide();
            }
        }
        if(error == false) {
            return true;
        } else {
            return false;
        }
    });
});
var date = new Date();
date.setDate(date.getDate());
$('.input-group.date').datepicker({format: "dd.mm.yyyy", startDate: date});
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $validator !!}
@endsection