@extends('layouts.app')

@section('template_title')
    My Activities
@endsection

@section('template_fastload_css')
    .help-block1, .error-help-block1{
    margin-top: 5px;
    margin-bottom: 10px;
    color: #a94442
    }
@endsection

@section('navclasses')
    navbar-fixed-top
@endsection

@section('content')
    <div id="page-content-wrapper">
        <div class="dashboard">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 activity">

                        <span id="app">
							<activity :result-data="{{$viewedUsers}}"
                                      :paginate="{{$pagination}}"  :tabf="{{$tab}}"></activity>

						</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<section>--}}
    {{--<div class="container">--}}
    {{--<div class="col-12 col-lg-10 offset-lg-1" style="min-height: 600px;">--}}

    {{--@include('panels.welcome-panel')--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

@endsection

@section('footer_scripts')

    <script type="text/javascript">
        {{----}}
        {{--$('ul.pagination').hide();--}}
        {{--$(function() {--}}
            {{--$('.infinite-scroll').jscroll({--}}
                {{--autoTrigger: true,--}}
                {{--loadingHtml: '<img class="center-block" src="{{asset('img/loader.gif')}}" alt="Loading..." />',--}}
                {{--padding: 0,--}}
                {{--nextSelector: '.pagination li.active + li a',--}}
                {{--contentSelector: 'div.infinite-scroll',--}}
                {{--callback: function() {--}}
                    {{--$('ul.pagination').remove();--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
    </script>
@endsection
