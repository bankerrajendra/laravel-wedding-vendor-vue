@extends('layouts.app')

@section('template_title')
    Viewed Me
@endsection

@section('template_fastload_css')
    .help-block1, .error-help-block1{
    margin-top: 5px;
    margin-bottom: 10px;
    color: #a94442
    }

@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div id="page-content-wrapper">
        <div class="dashboard">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 activity">

                        @include('partials.activity-navigation')

                        <div class="tab-content ">
                            <!--Viewed Me-->
                            <div id="home" class="tab-pane tab-scrollable fade in active">

                                @if(!is_null($viewedUsers))
                                    @if(!$viewedUsers->isEmpty())

                                        {{--<div class="infinite-scroll">--}}

                                        @foreach($viewedUsers as $viewEntry)
                                            <?php $from_user=$viewEntry->from_user();

                                                $disabled_status=($from_user->banned == 1 || $from_user->deactivated == 1 || $from_user->deleted_at != null || $from_user->account_show=='N' || $viewEntry->to_user()->account_show=='N' || $from_user->isSkipped() || $from_user->isSkippedMe() || $from_user->isBlockedMe() || $from_user->isBlocked())?'Y':'N';
                                            ?>
                                            <div class="row activity-list" id="view-{{$viewEntry->id}}">
                                                <div class="col-sm-2 col-xs-4 can_panel">
                                                    @if($from_user->privatePhotoPermissionCnt()>0) <div class="un-lock"><img src="{{URL::asset('img/unlock.png')}}"></div>  @endif
                                                    <a href="{{$from_user->profileLink()}}"><img src="{{$from_user->getVendorProfilePic()}}" class="img-responsive img-thumbnail" style="width:100%;"></a>
                                                </div>
                                                <div class="col-sm-6 col-xs-8 can_panel">
                                                    <h4>{{$from_user->getFirstname()}} <span class="pull-right hidden-lg hidden-sm hidden-md online"><i class="fa fa-circle" aria-hidden="true"></i></span><span style="color: red; font-size: x-small; font-weight: bold;font-style: italic">{{$viewEntry->updated_at->diffForHumans()}}</span></h4>
                                                    <p>@if(!is_null($from_user->users_information->about)){{ showReadMore($from_user->users_information->about, 90, $from_user->profileLink()) }}@endif</p>
                                                    <p class="in-line">
                                                        @if($from_user->hasConversation())
                                                            <a href="{{route('conversation', ['userId'=>$from_user->getEncryptedId()])}}"><i class="fa fa-eye" aria-hidden="true"></i> View Message</a></p>
                                                        @else
                                                            <span id="span-show-view-msg-{{$from_user->id}}" style="display:none"><a href="{{route('conversation', ['userId'=>$from_user->getEncryptedId()])}}"><i class="fa fa-eye" aria-hidden="true"></i> View Message</a> </span>
                                                            <span id="span-show-send-msg-{{$from_user->id}}" style="display: inline-block; padding-right: 10px;">
                                                                <a data-toggle="modal" data-target="#myModal{{$viewEntry->id}}" class="sendMessageButton" data-profileid="{{$from_user->getEncryptedId()}}"  data-viewid="{{$viewEntry->id}}"><i class="fa fa-comment" aria-hidden="true"></i> Send Message</a></p>
                                                            </span>
                                                            <div class="modal fade" id="myModal{{$viewEntry->id}}" role="dialog" >
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title" style="color:#fff;">Message</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form action="javascript:void(0);">
                                                                            <span id="returnSpan{{$from_user->id}}" style="color: red;"></span>
                                                                            <div class="form-group">
                                                                                @if($viewEntry->to_user()->account_show=='N')
                                                                                    <div class="alert alert-info" role="alert">
                                                                                        You cannot send message as your profile is hidden. First change your profile status to "Show" (under Settings->Show Profile)
                                                                                    </div>
                                                                                @endif
                                                                                <textarea class="form-control sendMessageInput" id="sendMessageInput{{$from_user->id}}" name="msg_description" @if($disabled_status=='Y') disabled="disabled" @endif  style="box-shadow:none;" data-profileid="{{$from_user->getEncryptedId()}}" data-id="{{$from_user->id}}" placeholder="Message" maxlength="140" rows="5" required=""></textarea>
                                                                            </div>
                                                                            <input type="submit" id="sendMessage{{$from_user->id}}" @if($disabled_status=='Y')  disabled="disabled" @endif data-profileid="{{$from_user->getEncryptedId()}}" data-id="{{$from_user->id}}"  class="btn btn-success sendMessage" value="Send Message">
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif

                                                    <p class="in-line"><a href="javascript:;" class="remove deleteViewedMe" data-profileid="{{$from_user->getEncryptedId()}}" data-viewid="{{$viewEntry->id}}">Remove</a></p>
                                                </div>
                                                <div class="col-sm-4 hidden-xs">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>Age :</td>
                                                                <td>{{$from_user->getAge()}} Years</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Religion :</td>
                                                                <td>{{$from_user->users_information->religion->religion}}</td>
                                                            </tr>

                                                            @if($from_user->users_information->sect_show=='Y')
                                                            <tr>
                                                                <td>Sect :</td>
                                                                <td>{{$from_user->users_information->get_community()}}</td>
                                                            </tr>
                                                            @endif

                                                            <tr>
                                                                <td>Location :</td>
                                                                <td>{{$from_user->city->name}}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endforeach
                                        @if(config('usersmanagement.enableMsgPagination'))
                                            {{ $viewedUsers->links() }}
                                        @endif
                                        {{--</div>--}}


                                    @else
                                        <div class="col-sm-12 panel-body">
                                            <div class="alert alert-info" role="alert">
                                                No user found.
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-sm-12 panel-body">
                                        <div class="alert alert-info" role="alert">
                                            No user found.
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <!--End Viewed Me-->

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    {{--<section>--}}
    {{--<div class="container">--}}
    {{--<div class="col-12 col-lg-10 offset-lg-1" style="min-height: 600px;">--}}

    {{--@include('panels.welcome-panel')--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

@endsection

@section('footer_scripts')

    <script type="text/javascript">
        {{----}}
        {{--$('ul.pagination').hide();--}}
        {{--$(function() {--}}
            {{--$('.infinite-scroll').jscroll({--}}
                {{--autoTrigger: true,--}}
                {{--loadingHtml: '<img class="center-block" src="{{asset('img/loader.gif')}}" alt="Loading..." />',--}}
                {{--padding: 0,--}}
                {{--nextSelector: '.pagination li.active + li a',--}}
                {{--contentSelector: 'div.infinite-scroll',--}}
                {{--callback: function() {--}}
                    {{--$('ul.pagination').remove();--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
    </script>
@endsection
