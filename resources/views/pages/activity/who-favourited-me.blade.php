@extends('layouts.app')

@section('template_title')
    Favorited Me
@endsection

@section('template_fastload_css')
    .help-block1, .error-help-block1{
    margin-top: 5px;
    margin-bottom: 10px;
    color: #a94442
    }

@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div id="page-content-wrapper">
        <div class="dashboard">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 activity">

                        @include('partials.activity-navigation')

                        <div class="tab-content ">
                            <!--Favorited Me-->

                                <div id="menu2" class="tab-pane tab-scrollable fade  in active">
                                @if(!is_null($likedMeUsers))
                                    @if(!$likedMeUsers->isEmpty())

                                        <div class="infinite-scroll">
                                            @foreach($likedMeUsers as $viewEntry)
                                                @php
                                                    $from_user=$viewEntry->from_user();
                                                    $disabled_status=($from_user->banned == 1 || $from_user->deactivated == 1 || $from_user->deleted_at != null || $from_user->account_show=='N' || $viewEntry->to_user()->account_show=='N')?'Y':'N';
                                                @endphp

                                                <div class="row activity-list" id="favouritedme-{{$viewEntry->id}}">
                                                    <div class="col-sm-2 col-xs-4 can_panel">
                                                        @if($from_user->privatePhotoPermissionCnt()>0) <div class="un-lock"><img src="{{URL::asset('img/unlock.png')}}"></div>  @endif
                                                        <a href="{{$from_user->profileLink()}}"><img src="{{$from_user->getVendorProfilePic()}}" class="img-responsive img-thumbnail" style="width:100%;"></a>
                                                        @if($viewEntry->to_user_status == 1)
                                                            <span class="new_ribbon green_ribbon">Mutual Match</span>
                                                        @endif
                                                    </div>
                                                    <div class="col-sm-6 col-xs-8 can_panel">
                                                        <h4>{{$from_user->getFirstname()}} <span class="pull-right hidden-lg hidden-sm hidden-md online"><i class="fa fa-circle" aria-hidden="true"></i></span></h4>
                                                        <p>@if(!is_null($from_user->users_information->about)){{ showReadMore($from_user->users_information->about, 90, $from_user->profileLink()) }}@endif</p>
                                                        @if(!$from_user->isBlockedMe() && !$from_user->isBlocked())  <p class="in-line"><a href="javascript:void(0)" class="likeBack" @if($disabled_status=='Y' || $viewEntry->to_user_status == 1) disabled="disabled" @endif data-profileid="{{$from_user->getEncryptedId()}}" data-favouritedmeid="{{$viewEntry->id}}" ><i class="fa fa-heart-o" aria-hidden="true"></i> Favorite Back</a></p> @endif
                                                        <p class="in-line"><a href="javascript:;" class="remove skipProfile" data-profileid="{{$from_user->getEncryptedId()}}" data-favouritedmeid="{{$viewEntry->id}}">Hide</a></p>
                                                    </div>
                                                    <div class="col-sm-4 hidden-xs">
                                                        <table class="table">
                                                            <tbody>
                                                            <tr>
                                                                <td>Age :</td>
                                                                <td>{{$from_user->getAge()}} Years</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Religion :</td>
                                                                <td>{{$from_user->users_information->religion->religion}}</td>
                                                            </tr>

                                                            @if($from_user->users_information->sect_show=='Y')
                                                                <tr>
                                                                    <td>Sect :</td>
                                                                    <td>{{$from_user->users_information->get_community()}}</td>
                                                                </tr>
                                                            @endif

                                                            <tr>
                                                                <td>Location :</td>
                                                                <td>{{$from_user->city->name}}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endforeach
                                            @if(config('usersmanagement.enableMsgPagination'))
                                                {{ $likedMeUsers->links() }}
                                            @endif
                                        </div>

                                    @else
                                        <div class="col-sm-12 panel-body">
                                            <div class="alert alert-info" role="alert">
                                                No record found.
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-sm-12 panel-body">
                                        <div class="alert alert-info" role="alert">
                                            No record found.
                                        </div>
                                    </div>
                                @endif
                                {{--<div class="row activity-list">--}}
                                {{--<div class="col-sm-2 col-xs-4 can_panel">--}}
                                {{--<a href=""><img src="img/demo2.jpg" class="img-responsive img-thumbnail" style="width:100%;"></a>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-4 col-xs-8 can_panel">--}}
                                {{--<h4>MW231234 <span class="pull-right hidden-lg hidden-sm hidden-md online"><i class="fa fa-circle" aria-hidden="true"></i></span></h4>--}}
                                {{--<p>Muslim Wedding may be your most trusted brand that your friends and family likes..</p>--}}
                                {{--<p class="in-line"><a href="javascript:void(0)"><i class="fa fa-heart" aria-hidden="true"></i>  Favorite Back</a></p>--}}
                                {{--<p class="in-line"><a href="" class="remove">Hide</a></p>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3 hidden-xs">--}}
                                {{--<table class="table">--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                {{--<td>Age :</td>--}}
                                {{--<td>57 Years</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Religion :</td>--}}
                                {{--<td>Islamic</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Sect :</td>--}}
                                {{--<td>Relli</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Location :</td>--}}
                                {{--<td>Luau</td>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                                {{--</table>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3 tx_right hidden-xs">--}}
                                {{--<p><small>2:34pm, 31 October 2017</small></p>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="row activity-list">--}}
                                {{--<div class="col-sm-2 col-xs-4 can_panel">--}}
                                {{--<a href=""><img src="img/demo3.jpg" class="img-responsive img-thumbnail" style="width:100%;"></a>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-4 col-xs-8 can_panel">--}}
                                {{--<h4>MW231234 <span class="pull-right hidden-lg hidden-sm hidden-md online"><i class="fa fa-circle" aria-hidden="true"></i></span></h4>--}}
                                {{--<p>Muslim Wedding may be your most trusted brand that your friends and family likes..</p>--}}
                                {{--<p class="in-line"><a href="javascript:void(0)"><i class="fa fa-heart" aria-hidden="true"></i>  Favorite Back</a></p>--}}
                                {{--<p class="in-line"><a href="" class="remove">Hide</a></p>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3 hidden-xs">--}}
                                {{--<table class="table">--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                {{--<td>Age :</td>--}}
                                {{--<td>57 Years</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Religion :</td>--}}
                                {{--<td>Islamic</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Sect :</td>--}}
                                {{--<td>Relli</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Location :</td>--}}
                                {{--<td>Luau</td>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                                {{--</table>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3 tx_right hidden-xs">--}}
                                {{--<p><small>2:34pm, 31 October 2017</small></p>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="row activity-list">--}}
                                {{--<div class="col-sm-2 col-xs-4 can_panel">--}}
                                {{--<a href=""><img src="img/demo1.jpg" class="img-responsive img-thumbnail" style="width:100%;"></a>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-4 col-xs-8 can_panel">--}}
                                {{--<h4>MW231234 <span class="pull-right hidden-lg hidden-sm hidden-md offline"><i class="fa fa-circle" aria-hidden="true"></i></span></h4>--}}
                                {{--<p>Muslim Wedding may be your most trusted brand that your friends and family likes..</p>--}}
                                {{--<p class="in-line"><a href="javascript:void(0)"><i class="fa fa-heart" aria-hidden="true"></i>  Favorite Back</a></p>--}}
                                {{--<p class="in-line"><a href="" class="remove">Hide</a></p>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3 hidden-xs">--}}
                                {{--<table class="table">--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                {{--<td>Age :</td>--}}
                                {{--<td>57 Years</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Religion :</td>--}}
                                {{--<td>Islamic</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Sect :</td>--}}
                                {{--<td>Relli</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Location :</td>--}}
                                {{--<td>Luau</td>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                                {{--</table>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3 tx_right hidden-xs">--}}
                                {{--<p><small>2:34pm, 31 October 2017</small></p>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="row activity-list">--}}
                                {{--<div class="col-sm-2 col-xs-4 can_panel">--}}
                                {{--<a href=""><img src="img/demo2.jpg" class="img-responsive img-thumbnail" style="width:100%;"></a>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-4 col-xs-8 can_panel">--}}
                                {{--<h4>MW231234 <span class="pull-right hidden-lg hidden-sm hidden-md offline"><i class="fa fa-circle" aria-hidden="true"></i></span></h4>--}}
                                {{--<p>Muslim Wedding may be your most trusted brand that your friends and family likes..</p>--}}
                                {{--<p class="in-line"><a href="javascript:void(0)"><i class="fa fa-heart" aria-hidden="true"></i>  Favorite Back</a></p>--}}
                                {{--<p class="in-line"><a href="" class="remove">Hide</a></p>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3 hidden-xs">--}}
                                {{--<table class="table">--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                {{--<td>Age :</td>--}}
                                {{--<td>57 Years</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Religion :</td>--}}
                                {{--<td>Islamic</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Sect :</td>--}}
                                {{--<td>Relli</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>Location :</td>--}}
                                {{--<td>Luau</td>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                                {{--</table>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-3 tx_right hidden-xs">--}}
                                {{--<p><small>2:34pm, 31 October 2017</small></p>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <!--End Favorited Me-->

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    {{--<section>--}}
    {{--<div class="container">--}}
    {{--<div class="col-12 col-lg-10 offset-lg-1" style="min-height: 600px;">--}}

    {{--@include('panels.welcome-panel')--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

@endsection

@section('footer_scripts')

    <script type="text/javascript">


        $('ul.pagination').hide();
        $(function() {
            $('.infinite-scroll').jscroll({
                autoTrigger: true,
                loadingHtml: '<img class="center-block" src="{{asset('img/loader.gif')}}" alt="Loading..." />',
                padding: 0,
                nextSelector: '.pagination li.active + li a',
                contentSelector: 'div.infinite-scroll',
                callback: function() {
                    $('ul.pagination').remove();
                }
            });
        });
    </script>
@endsection
