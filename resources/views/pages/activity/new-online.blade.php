@extends('layouts.app')
@section('template_title')
    New & Online Users
@endsection
@section('template_fastload_css')
@endsection

@section('navclasses')
    navbar-fixed-top
@endsection
@section('css')
    @if(config('usermanagement.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('usermanagement.datatablesCssCDN') }}">

    @endif
@endsection
@section('content')

    <div id="page-content-wrapper">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="row">
                            @include('partials.user-sidebar-navigation')
                            <div class="col-sm-8 col-sm-offset-4 panel-body container-pad">
                            @if(!is_null($viewedUsers))
                                @if(!$viewedUsers->isEmpty())
                                    @foreach($viewedUsers as $viewEntry)
                                        <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing" id="view-{{$viewEntry->id}}">
                                            <div class="media">
                                                <a class="pull-left" href="{{$viewEntry->profileLink()}}" target="_parent">
                                                    <img alt="image" src="{{$viewEntry->getVendorProfilePic()}}">
                                                </a>
                                                <div class="clearfix visible-sm"></div>

                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                        <a href="{{$viewEntry->profileLink()}}" >{{$viewEntry->name}} <small>{{$viewEntry->getAge()}}, {{$viewEntry->city->name}}</small></a>
                                                    </h4>

                                                    <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
                                                        @if($viewEntry->hasConversation())
                                                            <li><a href="{{route('conversation', ['userId'=>$viewEntry->getEncryptedId()])}}"><i class="fa fa-eye" aria-hidden="true"></i> View Messages</a></li>
                                                        @else
                                                            <li><a href="javascript:;" class="sendMessageButton" data-viewid="{{$viewEntry->id}}"><i class="fa fa-envelope" aria-hidden="true"></i>  Send Message</a></li>
                                                            <div class="popup-box chat-popup" id="messageDiv{{$viewEntry->id}}">
                                                                <div class="popup-head">
                                                                    <div class="popup-head-left pull-left">New Message :</div>
                                                                    <div class="popup-head-right pull-right">
                                                                        <button data-widget="remove" id="removeClass"  class="chat-header-button pull-right messagePopUpClose" type="button" data-viewid="{{$viewEntry->id}}"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="popup-messages">
                                                                    <div class="row">
                                                                        <div class="col-sm-2 col-xs-4">
                                                                            <a href="{{$viewEntry->profileLink()}}"><img src="{{$viewEntry->getVendorProfilePic()}}" alt=""></a>
                                                                        </div>
                                                                        <div class="col-sm-9 col-xs-8">
                                                                            <h5>{{$viewEntry->name}}</h5>
                                                                            <p>{{$viewEntry->getAge()}},{{$viewEntry->city->name}}</p>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <form action="">
                                                                                <div class="form-group">
                                                                                    <textarea class="form-control sendMessageInput" id="sendMessageInput{{$viewEntry->id}}" @if($viewEntry->banned == 1 || $viewEntry->deactivated == 1) disabled="disabled" @endif  name="msg_description" placeholder="Message" maxlength="140" rows="5" required="" data-profileid="{{$viewEntry->getEncryptedId()}}" data-id="{{$viewEntry->id}}" data-reloadpage="1"></textarea>
                                                                                </div>
                                                                                <input type="button" class="btn btn-danger pull-right sendMessage" id="sendMessage{{$viewEntry->id}}" @if($viewEntry->banned == 1 || $viewEntry->deactivated == 1) disabled="disabled" @endif  data-profileid="{{$viewEntry->getEncryptedId()}}" data-id="{{$viewEntry->id}}" data-reloadpage="1" value="Send Message">
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif


                                                        <li style="list-style: none">|</li>


                                                        @if($viewEntry->isOnline())
                                                            <li class="online"><i class="fa fa-circle" aria-hidden="true"></i> Online</li>
                                                        @else
                                                            <li class="offline"><i class="fa fa-circle" aria-hidden="true"></i> Offline</li>
                                                        @endif

                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!-- End Listing-->




                                        @endforeach
                                        @if(config('usersmanagement.enableCmsPagination'))
                                            {{ $viewedUsers->links() }}
                                        @endif
                                    @else
                                        <div class="col-sm-12 panel-body">
                                            <div class="alert alert-info" role="alert">
                                                No user found.
                                            </div>
                                        </div>
                                @endif
                                @else
                                    <div class="col-sm-12 panel-body">
                                        <div class="alert alert-info" role="alert">
                                            No user found.
                                        </div>
                                    </div>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer_scripts')
    <script>
        $(function(){
            $(".sendMessageButton").on('click',function () {
                var id=$(this).data('viewid');
                $('#messageDiv'+id).addClass('popup-box-on');
            });

            $(".messagePopUpClose").on('click', function () {
                var id=$(this).data('viewid');
                $('#messageDiv'+id).removeClass('popup-box-on');
            });
        });

    </script>
@endsection

@section('js')
    @if ((count($viewedUsers) > config('usersmanagement.datatablesJsStartCount')) && config('usersmanagement.enabledDatatablesJs'))
        @include('scripts.datatables')
    @endif
    {{--@if(config('usersmanagement.enableSearchUsers'))--}}
    {{--@include('scripts.search-users')--}}
    {{--@endif--}}
@endsection