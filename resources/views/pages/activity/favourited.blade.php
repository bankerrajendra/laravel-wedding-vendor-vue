@extends('layouts.app')

@section('template_title')
    My Favorites
@endsection

@section('template_fastload_css')
    .help-block1, .error-help-block1{
    margin-top: 5px;
    margin-bottom: 10px;
    color: #a94442
    }

@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div id="page-content-wrapper">
        <div class="dashboard">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 activity">

                        @include('partials.activity-navigation')

                        <div class="tab-content ">
                            <!--My Favorites-->
                            <div id="menu1" class="tab-pane tab-scrollable fade  in active">
                                @if(!is_null($likedUsers))
                                    @if(!$likedUsers->isEmpty())

                                        <div class="infinite-scroll">

                                            @foreach($likedUsers as $viewEntry)
                                                @php
                                                    $likedTo = $viewEntry->to_user();
                                                    $likedFrom = $viewEntry->from_user();
                                                    if($likedTo->id == Auth::user()->id)
                                                        $from_user = $likedFrom;
                                                    else
                                                        $from_user = $likedTo;

                                                    $disabled_status=($from_user->banned == 1 || $from_user->deactivated == 1 || $from_user->deleted_at != null || $from_user->account_show=='N' || $viewEntry->to_user()->account_show=='N')?'Y':'N';
                                                @endphp

                                                <div class="row activity-list" id="like-{{$viewEntry->id}}">
                                                    <div class="col-sm-2 col-xs-4 can_panel">
                                                        @if($from_user->privatePhotoPermissionCnt()>0) <div class="un-lock"><img src="{{URL::asset('img/unlock.png')}}"></div>  @endif
                                                        <a href="{{$from_user->profileLink()}}"><img src="{{$from_user->getVendorProfilePic()}}" class="img-responsive img-thumbnail" style="width:100%;"></a>

                                                    </div>
                                                    <div class="col-sm-6 col-xs-8 can_panel">
                                                        <h4>{{$from_user->getFirstname()}} <span class="pull-right hidden-lg hidden-sm hidden-md online"><i class="fa fa-circle" aria-hidden="true"></i></span></h4>
                                                        <p>@if(!is_null($from_user->users_information->about)){{ showReadMore($from_user->users_information->about, 90, $from_user->profileLink()) }}@endif</p>
                                                        <p class="in-line"><a href="javascript:void(0)" class="deleteLiked" data-profileid="{{$from_user->getEncryptedId()}}" data-likeid="{{$viewEntry->id}}"><i class="fa fa-heart" aria-hidden="true"></i> Unfavorite</a></p>

                                                        @if($viewEntry->to_user_status == 1)
                                                            <p class="mutaul-match">It's a mutual match</p>
                                                        @endif
                                                        {{--<p class="in-line"><a href="javascript:;" class="remove deleteLiked" data-profileid="{{$from_user->getEncryptedId()}}" data-likeid="{{$viewEntry->id}}">Remove</a></p>--}}
                                                        {{--<button type="button" class="btn btn-warning" disabled="disabled"><i class="fa fa-check" aria-hidden="true"></i></button>--}}
                                                        {{--<button type="button" class="btn btn-default deleteLiked"  data-profileid="{{$likedUser->getEncryptedId()}}" data-likeid="{{$likedEntry->id}}" ><i class="fa fa-times"></i></button>--}}
                                                    </div>
                                                    <div class="col-sm-4 hidden-xs">
                                                        <table class="table">
                                                            <tbody>
                                                            <tr>
                                                                <td>Age :</td>
                                                                <td>{{$from_user->getAge()}} Years</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Religion :</td>
                                                                <td>{{$from_user->users_information->religion->religion}}</td>
                                                            </tr>

                                                            @if($from_user->users_information->sect_show=='Y')
                                                                <tr>
                                                                    <td>Sect :</td>
                                                                    <td>{{$from_user->users_information->get_community()}}</td>
                                                                </tr>
                                                            @endif
                                                            <tr>
                                                                <td>Location :</td>
                                                                <td>{{$from_user->city->name}}</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endforeach
                                            @if(config('usersmanagement.enableMsgPagination'))
                                                {{ $likedUsers->links() }}
                                            @endif
                                        </div>


                                    @else
                                        <div class="col-sm-12 panel-body">
                                            <div class="alert alert-info" role="alert">
                                                No record found.
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-sm-12 panel-body">
                                        <div class="alert alert-info" role="alert">
                                            No record found.
                                        </div>
                                    </div>
                                @endif

                            </div>
                            <!--End My Favorites-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $('ul.pagination').hide();
        $(function() {
            $('.infinite-scroll').jscroll({
                autoTrigger: true,
                loadingHtml: '<img class="center-block" src="{{asset('img/loader.gif')}}" alt="Loading..." />',
                padding: 0,
                nextSelector: '.pagination li.active + li a',
                contentSelector: 'div.infinite-scroll',
                callback: function() {
                    $('ul.pagination').remove();
                }
            });
        });
    </script>
@endsection
