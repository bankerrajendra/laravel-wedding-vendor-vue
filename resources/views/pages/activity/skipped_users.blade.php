@extends('layouts.app')
@section('template_title')
    Skipped Users
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div id="page-content-wrapper">
        <section>
            @include('partials.setting-navigation-mobile')
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="row">
                            @include('partials.user-sidebar-navigation')
                            <div class="col-sm-8 col-sm-offset-4 panel-body container-pad menu">
                                @include('partials.setting-navigation')
                                @if(!is_null($skippedUsers))
                                    @if(!$skippedUsers->isEmpty())
                                        @foreach($skippedUsers as $skipEntry)
                                            <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing" id="skip-{{$skipEntry->id}}">
                                                <div class="media">
                                                    <a class="pull-left" href="{{$skipEntry->to_user()->profileLink()}}" target="_parent">
                                                        <img alt="image" src="{{$skipEntry->to_user()->getVendorProfilePic()}}">
                                                    </a>
                                                    <div class="clearfix visible-sm"></div>

                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            <a href="{{$skipEntry->to_user()->profileLink()}}" >{{$skipEntry->to_user()->name}} <small>{{$skipEntry->to_user()->getAge()}}, {{$skipEntry->to_user()->city->name}}</small></a>
                                                        </h4>

                                                        <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
                                                            {{--<li><a href=""><i class="fa fa-eye" aria-hidden="true"></i> View Message</a></li>--}}
                                                            {{--<li style="list-style: none">|</li>--}}
                                                            <li><a href="javascript:;" class="deleteSkippedProfile" data-profileid="{{$skipEntry->to_user()->getEncryptedId()}}" data-skipid="{{$skipEntry->id}}" ><i class="fa fa-refresh"></i> Reset</a></li>

                                                            <li style="list-style: none">|</li>
                                                            @if($skipEntry->to_user()->isOnline())
                                                                <li class="online"><i class="fa fa-circle" aria-hidden="true"></i> Online</li>
                                                            @else
                                                                <li class="offline"><i class="fa fa-circle" aria-hidden="true"></i> Offline</li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><!-- End Listing-->
                                        @endforeach
                                            @if(config('usersmanagement.enableCmsPagination'))
                                                {{ $skippedUsers->links() }}
                                            @endif
                                    @else
                                        <div class="col-sm-12 panel-body">
                                            <div class="alert alert-info" role="alert">
                                                No user found.
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-sm-12 panel-body">
                                        <div class="alert alert-info" role="alert">
                                            No user found.
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer_scripts')
@endsection