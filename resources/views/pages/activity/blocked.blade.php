@extends('layouts.app')
@section('template_title')
    Blocked Users
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div id="page-content-wrapper">
        <section>
            @include('partials.setting-navigation-mobile')
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="row">
                            @include('partials.user-sidebar-navigation')
                            <div class="col-sm-8 col-sm-offset-4 panel-body container-pad menu">
                                @include('partials.setting-navigation')
                                @if(!is_null($blockedProfiles))
                                    @if(!$blockedProfiles->isEmpty())
                                        @foreach($blockedProfiles as $blockEntry)
                                            <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing" id="block-{{$blockEntry->id}}">
                                                <div class="media">
                                                    <a class="pull-left" href="{{$blockEntry->to_user()->profileLink()}}" target="_parent">
                                                        <img alt="image" src="{{$blockEntry->to_user()->getVendorProfilePic()}}">
                                                    </a>
                                                    <div class="clearfix visible-sm"></div>

                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            <a href="{{$blockEntry->to_user()->profileLink()}}" >{{$blockEntry->to_user()->name}} <small>{{$blockEntry->to_user()->getAge()}}, {{$blockEntry->to_user()->city->name}}</small></a>
                                                        </h4>

                                                        <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
                                                            {{--<li><a href=""><i class="fa fa-eye" aria-hidden="true"></i> View Message</a></li>--}}

                                                            {{--<li style="list-style: none">|</li>--}}

                                                            <li><a href="javascript:;" class="unblockProfile" data-profileid="{{$blockEntry->to_user()->getEncryptedId()}}" data-blockid="{{$blockEntry->id}}" ><i class="fa fa-ban"></i> Unblock</a></li>

                                                            <li style="list-style: none">|</li>
                                                            @if($blockEntry->to_user()->isOnline())
                                                                <li class="online"><i class="fa fa-circle" aria-hidden="true"></i> Online</li>
                                                            @else
                                                                <li class="offline"><i class="fa fa-circle" aria-hidden="true"></i> Offline</li>
                                                            @endif

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><!-- End Listing-->
                                        @endforeach
                                            @if(config('usersmanagement.enableCmsPagination'))
                                                {{ $blockedProfiles->links() }}
                                            @endif
                                    @else
                                        <div class="col-sm-12 panel-body">
                                            <div class="alert alert-info" role="alert">
                                                No user found.
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-sm-12 panel-body">
                                        <div class="alert alert-info" role="alert">
                                            No user found.
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer_scripts')
@endsection