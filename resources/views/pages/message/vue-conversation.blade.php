@extends('layouts.app')
@section('template_title')
    Messages
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">


                @include('partials.message-navigation')


                <div class="col-sm-8 msg-section no_padding chat-section" id="app">
                    <conversation-component
                            :other-user="{{json_encode($otherUser)}}"
                            :auth-user="{{json_encode($authUser)}}"
                            :receiver-id="{{json_encode($receiver_id)}}"
                            :page-type="{{json_encode($page_type)}}"
                            :page-link="{{json_encode($page_link)}}"
                            :conversation-page-link="{{json_encode(route('conversation', ['userId' => $otherUser['getEncryptedId']]))}}"
                            :filter-conversation-page-link="{{json_encode(route('filter-conversation', ['userId' => $otherUser['getEncryptedId']]))}}"
                    >
                    </conversation-component>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
@endsection