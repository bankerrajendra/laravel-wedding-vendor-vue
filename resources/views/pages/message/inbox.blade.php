@extends('layouts.app')
@section('template_title')
    Messages - Inbox
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">
                @include('partials.message-navigation')

                <div class="col-sm-8 msg-section">
                    <div class="row hidden-xs">
                        <div class="col-sm-6 col-xs-6"><h4>Inbox</h4></div>
                        <div class="col-sm-6 col-xs-6">
                            <div class="checkbox pull-right unread"><label><input type="checkbox" class="show_unread" id="show_unread" @if($showunread=="unread") checked @endif value=""> Show Unread</label></div>
                        </div>
                    </div>

                    @include('partials.message-navigation-mobile')

                    <div class="row">
                        <div class="col-sm-12 col-xs-12 no_padding">
                            @if(!is_null($conversations))
                                @foreach($conversations as $conversation)
                                    <div class="media">
                                        <div class="media-left">
                                            @if($validateUserCanRead == false && $conversation['old_conversation_exists'] == false)
                                                <a href="{{route('paid-membership')}}">
                                                    <img src="<?php echo (($conversation['user']->getVendorProfilePic())); ?>" class="media-object img-thumbnail" style="width:80px">
                                                    <i class="fa fa-circle <?php echo (($conversation['user']->isOnline()=='1')?:'fa-red'); ?> " aria-hidden="true"></i>
                                                </a>
                                            @else
                                                <a href="{{route('conversation', ['user_id'=>$conversation['user']->getEncryptedId()])}}">
                                                    <img src="<?php echo (($conversation['user']->getVendorProfilePic())); ?>" class="media-object img-thumbnail" style="width:80px">
                                                    <i class="fa fa-circle <?php echo (($conversation['user']->isOnline()=='1')?:'fa-red'); ?> " aria-hidden="true"></i>
                                                </a>
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                @if($validateUserCanRead == false && $conversation['old_conversation_exists'] == false)
                                                    <a href="{{route('paid-membership')}}" class="msg-name">{{$conversation['user']->get_firstname()}} <!--{{config('constants.site_code').$conversation['user']['id']}}--></a>
                                                    &nbsp;&nbsp;<small><i>{{ts2time($conversation['msg']['createdAt'])}}</i></small>
                                                @else
                                                    <a href="{{route('conversation', ['user_id'=>$conversation['user']->getEncryptedId()])}}" class="msg-name">{{$conversation['user']->get_firstname()}} <!--{{config('constants.site_code').$conversation['user']['id']}}--></a>
                                                    <div class="dropdown pull-right">
                                                        <button class="btn btn-note dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="{{route('send-archive', ['user_id'=>$conversation['user']->getEncryptedId()])}}">Archive</a></li>
                                                            <li><a href="{{route('send-trash', ['user_id'=>$conversation['user']->getEncryptedId(), 'pageType'=>'inbox'])}}">Trash</a></li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </h4>
{{--                                            <p><small>{{$conversation['user']->city->name}}, {{$conversation['user']->country->name}}</small></p>--}}
                                            @if($validateUserCanRead == false && $conversation['old_conversation_exists'] == false)
                                            <a href="{{ route('paid-membership') }}">Upgrade to Read</a>
                                            @endif
                                            @if($conversation['msg']->unread_count>0)
                                                <p><strong style="color: red;" >New</strong>
                                                    &nbsp;&nbsp;<small><i>{{ts2time($conversation['msg']['createdAt'])}}</i></small> </p>
                                            @else
                                                @if($conversation['last_message'][0]['sender_id']==Auth::id())
                                                    @if($conversation['last_message'][0]['read_status']=='N')
                                                        <p><strong><i class="fa fa-reply" aria-hidden="true"></i></strong> Replied</p>
                                                    @else
                                                        <p><strong><i class="fa fa-reply" aria-hidden="true"></i></strong> Read </p>
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="media">
                                    <div class="alert alert-info" role="alert">
                                        No messages found.
                                    </div>
                                </div>
                            @endif

                        </div>
                        <div class="col-sm-12 col-xs-12">
                            @if(config('usersmanagement.enableMsgPagination'))
                                {{ ($conversationObj)->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script>
        $('.show_unread').click(function() {
            if (this.checked) {  // if checkbox is selected
                window.location.href="{{route('inbox_showread', ['user_id'=>'unread'])}}";
            } else {
                window.location.href="{{route('inbox')}}";
            }
        });
    </script>

@endsection