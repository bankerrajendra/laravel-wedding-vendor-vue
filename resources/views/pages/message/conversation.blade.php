@extends('layouts.app')
@section('template_title')
    Messages
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">


                @include('partials.message-navigation')


                <div class="col-sm-8 msg-section no_padding chat-section">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{session('success')}}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{!! $error !!} </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <div class="panel panel-default">
                            <div class="panel-heading chat_header">
                                <a href="@if($pagetype=='trash') {{route('trash')}} @elseif($pagetype=='inbox') {{route('inbox')}} @elseif($pagetype=='archive') {{route('archive')}}   @elseif($pagetype=='sent')  {{route('sent')}}     @endif"><i class="fa fa-chevron-left"></i></a> &nbsp;&nbsp;&nbsp;{{$otherUser->get_firstname()}}, {{$otherUser->getAge()}}
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn  btn-xs dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-v"></i>
                                    </button>
                                    <ul class="dropdown-menu slidedown"><?php $blockedFlag=user_blocked_by_me($otherUser->id);  $skippedFlag=user_skipped_by_me($otherUser->id)  ?>
                                        @if(@$pagetype=='trash')
                                            <li><a href="{{route('move-to-inbox', ['user_id'=>$otherUser->getEncryptedId()])}}">Move to Inbox</a></li>
                                            <li><a href="{{route('move-to-archive', ['user_id'=>$otherUser->getEncryptedId()])}}">Move to Archive</a></li>
                                            <li><a href="{{route('parmanent-delete', ['user_id'=>$otherUser->getEncryptedId()])}}">Delete Conversation</a></li>
                                        @elseif(@$pagetype=='archive')
                                            <li><a href="{{route('send-inbox', ['user_id'=>$otherUser->getEncryptedId()])}}">Inbox Conversation</a></li>
                                            <li><a href="{{route('send-trash', ['user_id'=>$otherUser->getEncryptedId(), 'pageType'=>'archive'])}}">Delete Conversation</a></li>

                                        @elseif(@$pagetype=='sent')
                                            <li><a href="{{route('send-trash', ['user_id'=>$otherUser->getEncryptedId(), 'pageType'=>'archive'])}}">Delete Conversation</a></li>
                                        @else
                                            <li><a href="{{route('send-archive', ['user_id'=>$otherUser->getEncryptedId()])}}">Archive Conversation</a></li>
                                            <li><a href="{{route('send-trash', ['user_id'=>$otherUser->getEncryptedId(), 'pageType'=>(($pagetype=='filter')?'filter':'inbox')])}}">Delete Conversation</a></li>
                                            <?php $blocked_me_flag=user_blocked_to_me($otherUser->id); $skipped_me_flag=user_skipped_to_me($otherUser->id);  ?>
                                            @if($blocked_me_flag<1 && $skipped_me_flag<1 && $otherUser->banned == 0 && $otherUser->deactivated==0 && $otherUser->deleted_at==null && $otherUser->account_show=='Y')
                                                @if($authHasPrivatePhoto && $blockedFlag<1 && $skippedFlag<1)
                                                    @if($authPhotoShareStatus)
                                                        <li><a href="javascript:;" class="unsharePhotos" data-profileid="{{$otherUser->getEncryptedId()}}">Unshare Photos</a></li>
                                                    @else
                                                        <li><a href="javascript:;" class="sharePhotos" data-profileid="{{$otherUser->getEncryptedId()}}">Share Photos</a></li>
                                                    @endif
                                                @endif
                                                <li>
                                                    @if($blockedFlag>0)
                                                        <a href="javascript:;" class="unblockProfile" data-profileid="{{$otherUser->getEncryptedId()}}">UnBlock User</a>
                                                    @else
                                                        <a  data-toggle="modal" onclick="blockUserData();" data-target="#demo-4" >Block User</a>
                                                    @endif
                                                </li>
                                                <li><a data-target="#reportUser" data-toggle="modal" href="#">Report User</a></li>
                                            @endif
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="chat-list" id="testDiv3" style="">
                                @foreach($conversation as $value)
<!--                                    --><?php //echo " == ".$value->sender_id . "::".$otherUser->id;
//                                    //if(!in_array($currentRoute, $routesAllowed) && !($user->isAdmin()) && $user->hasRole('vendor') && !($user->hasRole('unverified'))) {
//                                    echo $authUser->hasRole('vendor');
//                                    ?>
                                    @if($authUser->id==$value->sender_id)
                                        <div class="media">
                                            <div class="media-body text-right">
                                                <span>{{$value->message}}
                                                    <br>
                                                    <small>{{date('h:i A, d F Y', strtotime($value->created_at->setTimezone(Auth::user()->getUserTimeZone())))}}</small>
                                                </span>
                                            </div>
                                            <div class="media-right">
                                                <img src="{{($authUser->getVendorProfilePic(true))}}" class="media-object img-circle" style="width:50px">
                                            </div>
                                        </div>
                                    @else
                                        <div class="media text-left">
                                            <div class="media-left">
                                                <a href="{{$otherUser->getVendorProfileLink()}}" target="_blank"><img src="{{($otherUser->getVendorProfilePic())}}" class="media-object  img-circle" style="width:50px"></a>
                                            </div>
                                            <div class="media-body">
                                                <span>{{$value->message}}
                                                    <br>
                                                    <small>{{date('h:i A, d F Y', strtotime($value->created_at->setTimezone(Auth::user()->getUserTimeZone())))}}</small>
                                                </span>
                                            </div>
                                        </div>
                                    @endif

                                <!-- Right-aligned -->
                                @endforeach

                            </div>
                            @if(@$pagetype!='trash' && @$pagetype!='archive' && @$pagetype!='sent')
                                <?php $blocked_me_flag=user_blocked_to_me($otherUser->id);  $skipped_me_flag=user_skipped_to_me($otherUser->id); ?>
                                @if($blocked_me_flag<1 && $skipped_me_flag<1 && $otherUser->banned == 0 && $otherUser->deactivated==0 && $otherUser->deleted_at==null && $otherUser->account_show=='Y')
                                    @if($blockedFlag>0 || $skippedFlag>0)<div class="panel-footer" style=""><div id="error_msg" style="color: #a94442; ">User blocked/hide by you. Please unblock/unhide user first.</div>@endif
                                        <div class="panel-footer" style=""><div id="error_msg" style="color: #a94442; display: none; ">Message is Required.</div>
                                            {!! Form::open(array('route' => array('send-message'), 'method' => 'post', 'name' => 'messageFrm', 'id' => 'messageFrm', 'class' => 'needs-validation', 'role' => 'form')) !!}
                                            <input type="hidden" name="receiver_id" value="{{$rec_id}}"><input type="hidden" name="pageType" value="{{$pagetype}}">
                                            <div class="input-group">
                                                <input id="message" name="message" @if($blockedFlag>0 || $skippedFlag>0) disabled @endif type="text" class="form-control" placeholder="Type your message here...">
                                                <span class="input-group-btn">
                                        <button class="btn" type="text" @if($blockedFlag>0 || $skippedFlag>0) disabled="disabled" @endif><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                    </span>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>


                                        @endif


                                        @endif

                                    </div>
                        </div>

                </div>
            </div>
        </div>

        <div class="modal fade head_popup" id="reportUser" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" id="resetPublic" value="reset">×</button>
                        <h4 class="modal-title" id="avatar-modal-label">Are you sure you want to report this user ?</h4>
                    </div>

                    <div class="modal-body report">
                        <br>
                        <form id="reportUserForm" name="reportUserForm" method="post" class="needs-validation" role="form" action="" >
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <input class="form-control" id="reason" name="reason" placeholder="Reason" type="text"  />
                                    <span id="reason-error" class="help-block1 error-help-block1" style="display: none">The reason field is required.</span>
                                </div>
                                <div class="col-md-12 form-group">
                                    <input type="hidden" id="userId" name="userId" value="{{$otherUser->getEncryptedId()}}">
                                    <textarea class="form-control" id="description" name="description" placeholder="Description" style="height:100px;"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-md-12 form-group">
                                    <button type="submit" class="btn btn-success"> Submit</button>&nbsp;&nbsp;
                                    <button type="button" class="btn btn-setting" class="close" data-dismiss="modal"> Cancel</button>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade text-center" id="reportSuccess">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" id='resetPublic1' value='reset'>&times;</button>
                        <h4 class="modal-title" style="color:#fff;">Thank You!</h4>
                    </div>

                    <div class="modal-body report">
                        <br>
                        <p>Your report has been submitted successfully. Admin will review it and will take required action.</p>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 form-group">
                                <button type="button" class="btn btn-setting" class="close" data-dismiss="modal">Okay</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="demo-4" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center report">
                        <h6 class="modal-title"></h6>
                        <h4>Are you sure you want to block this user ?</h4>
                        <br>
                        <a href="javascript:;" class="btn btn-success blockProfile" data-profileid="{{$otherUser->getEncryptedId()}}"> Ok</a>&nbsp;&nbsp;
                        <button type="button" class="btn btn-setting" data-dismiss="modal"> Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script>
        $.fn.scrollDown=function(){
            let el=$(this)
            el.scrollTop(el[0].scrollHeight)
        }

        $('#testDiv3').scrollDown();
    </script>
@endsection