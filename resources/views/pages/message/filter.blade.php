@extends('layouts.app')
@section('template_title')
    Messages
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('template_linked_css')
    <link href="{{asset('css/select-box.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

    <div class="msg-section">
        <div class="container">
            <div class="row">

                @include('partials.message-navigation')

                <div class="col-sm-8 panel-body  panel-sec no-padding">
                    <div class="tab-content message">
                        @include('partials.message-navigation-mobile')
                        <div id="myModal" class="modal modal_popup fade" role="dialog">
                            <div class="modal-dialog modal-sm">
                                <!-- Modal content-->
                                <div class="modal-content modal_content" >
                                    <div class="modal-header" style="background:#fff!important;">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <div class="btn-group">
                                            <a class="btn @if($search_arry['filter_status']=="0") btn-danger @else btn-default @endif btn-filter " data-action="off" id="onFilter" >Off</a>
                                            <a class="btn @if($search_arry['filter_status']=="1") btn-success @else btn-default @endif btn-filter " data-action="on" id="offFilter" >On</a>
                                        </div>
                                    </div>
                                    <div class="modal-body" id="SearchParameters">
                                        <form method="post" name="filter-form" id="filter-form" action="{{route('post-filter-form')}}">
                                            @csrf
                                            <input type="hidden" name="status" id="status" value="{{$search_arry['filter_status']}}">
                                            <div class="panel-group" id="accordion">
                                                <!--Religion-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapse20" aria-expanded="<?php if(!empty($search_arry['religion'])) : echo "true"; else: echo "false"; endif; ?>"  class="">Religion<small><i class="fa fa-<?php if(!empty($search_arry['religion'])) : echo "minus"; else: echo "plus"; endif; ?>-circle pull-right"></i></small></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse20" class="panel-collapse collapse <?php if(!empty($search_arry['religion'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['religion'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                                        <div class="panel-body  panel-sec ne-search-option">
                                                            <select name="religion" id="religion" class="form-control">
                                                                @foreach($religions as $name=>$rId)
                                                                    <option value="{{$rId}}">{{ ucfirst($name) }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Religion-->

                                                <!--Body Type-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapse24" aria-expanded="<?php if(!empty($search_arry['body_type'])) : echo "true"; else: echo "false"; endif; ?>" class="">Body Type<small><i class="fa fa-<?php if(!empty($search_arry['body_type'])) : echo "minus"; else: echo "plus"; endif; ?>-circle pull-right"></i></small></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse24" class="panel-collapse collapse <?php if(!empty($search_arry['body_type'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['body_type'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                                        <div class="sm-panel-body  panel-sec">
                                                            <select  data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5" name="body_type[]">
                                                                <?php $search_arry_body_type=explode(",",$search_arry['body_type']); ?>
                                                                <option <?php if( !empty($search_arry['body_type']) && in_array(config('constants.doesnt_matter'), $search_arry_body_type) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(config('constants.body_type') as $key=>$cpl)
                                                                    <option <?php if( !empty($search_arry_body_type) && in_array($key, $search_arry_body_type) ) { echo "selected"; } ?> value="{{ $key }}">{{ $cpl }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Body Type-->

                                                <!--Age-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse25" aria-expanded="<?php if(!empty($search_arry['age_from'])) : echo "true"; else: echo "false"; endif; ?>">Age<small><i class="fa fa-<?php if(!empty($search_arry['age_from'])) : echo "minus"; else: echo "plus"; endif; ?>-circle pull-right"></i></small></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse25" class="panel-collapse collapse  <?php if(!empty($search_arry['age_from'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['age_from'])) : echo "true"; else: echo "false"; endif; ?>" >
                                                        <div class="panel-body  panel-sec">
                                                            <p>
                                                                <input type="text" name="ageRange" id="age"  value="0" readonly class="input-filter" >
                                                            </p>
                                                            <div id="slider-age"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Age-->

                                                <!--Height-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse"  href="#collapse26" aria-expanded="<?php if(!empty($search_arry['height_from'])) : echo "true"; else: echo "false"; endif; ?>">Height<small><i class="fa fa-<?php if(!empty($search_arry['height_from'])) : echo "minus"; else: echo "plus"; endif; ?>-circle pull-right"></i></small></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse26" class="panel-collapse collapse <?php if(!empty($search_arry['height_from'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['height_from'])) : echo "true"; else: echo "false"; endif; ?>">
                                                        <div class="panel-body  panel-sec">
                                                            <p>
                                                                <input type="text" name="heightRange" id="height"  value="0" readonly class="input-filter">
                                                            </p>
                                                            <div id="slider-height"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Height-->

                                                <!--Drinking-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapse29" aria-expanded="<?php if(!empty($search_arry['drink'])) : echo "true"; else: echo "false"; endif; ?>" class="">Drinking<small><i class="fa fa-<?php if(!empty($search_arry['drink'])) : echo "minus"; else: echo "plus"; endif; ?>-circle pull-right"></i></small></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse29" class="panel-collapse collapse <?php if(!empty($search_arry['drink'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['drink'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                                        <div class="panel-body  panel-sec ne-search-option">
                                                            <select name="drink[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">
                                                                <?php $search_arry_drink=explode(",",$search_arry['drink']); ?>
                                                                <option <?php if( !empty($search_arry_drink) && in_array(config('constants.doesnt_matter'), $search_arry_drink) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(config('constants.drink') as $rId=>$name)
                                                                    <option <?php if( !empty($search_arry_drink) && in_array($rId, $search_arry_drink) ) { echo "selected"; } ?> value="{{ $rId }}">{{ $name }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Drinking-->

                                                <!--Smoking-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapse28" aria-expanded="<?php if(!empty($search_arry['smoke'])) : echo "true"; else: echo "false"; endif; ?>" class="">Smoking<small><i class="fa fa-<?php if(!empty($search_arry['smoke'])) : echo "minus"; else: echo "plus"; endif; ?>-circle pull-right"></i></small></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse28" class="panel-collapse collapse <?php if(!empty($search_arry['smoke'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['smoke'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                                        <div class="panel-body  panel-sec ne-search-option">
                                                            <select name="smoke[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">
                                                                <?php $search_arry_smoke=explode(",",$search_arry['smoke']); ?>
                                                                <option <?php if( !empty($search_arry_smoke) && in_array(config('constants.doesnt_matter'), $search_arry_smoke) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(config('constants.smoke') as $rId=>$name)
                                                                    <option <?php if( !empty($search_arry_smoke) && in_array($rId, $search_arry_smoke) ) { echo "selected"; } ?> value="{{ $rId }}">{{ $name }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Smoking-->

                                                <!--Food Preference-->
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapse27" aria-expanded="<?php if(!empty($search_arry['food'])) : echo "true"; else: echo "false"; endif; ?>" class="">Diet<small><i class="fa fa-<?php if(!empty($search_arry['food'])) : echo "minus"; else: echo "plus"; endif; ?>-circle pull-right"></i></small></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse27" class="panel-collapse collapse <?php if(!empty($search_arry['food'])) : echo "in"; endif; ?>" aria-expanded="<?php if(!empty($search_arry['food'])) : echo "true"; else: echo "false"; endif; ?>" style="">
                                                        <div class="panel-body  panel-sec ne-search-option">
                                                            <select name="food[]" data-placeholder="Select" style="display:none" class="chosen-select form-control" multiple tabindex="5">
                                                                <?php $search_arry_food=explode(",",$search_arry['food']); ?>
                                                                <option <?php if( !empty($search_arry_food) && in_array(config('constants.doesnt_matter'), $search_arry_food) ) { echo "selected"; } ?> value="{{config('constants.doesnt_matter')}}">Doesn't matter</option>
                                                                @foreach(config('constants.food') as $rId=>$name)
                                                                    <option <?php if( !empty($search_arry_food) && in_array($rId, $search_arry_food) ) { echo "selected"; } ?> value="{{ $rId }}">{{ $name }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Food Preference-->

                                            </div>
                                            <div class="panel-body  panel-sec">
                                                <p>Allow emails  from users without images on their profile:</p>
                                                <label class="radio-inline">
                                                    <input type="radio" name="allow_without_image" value="1" @if($search_arry['allow_without_image']=="1") checked="" @endif>Yes
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="allow_without_image" value="0"  @if($search_arry['allow_without_image']=="0") checked="" @endif >No
                                                </label>
                                            </div>

                                            <div class="panel-body panel-sec">
                                                <input type="submit" class="btn btn-success search-result btn-block" value="SAVE" >
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4 class="hidden-xs">Filter <a href="javascript:void(0);" type="button" class="filter-icon pull-right hidden-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-filter" aria-hidden="true"></i></a></h4>
                        <hr>
                        @if(!is_null($conversations))
                            @foreach($conversations as $conversation)
                                <?php //echo (($conversation['user']->hasCompletedProfile()));
                                //  print_r($conversation);
                                //  die;
                                ?>
                                <div class="media">
                                    <div class="media-left">
                                        @if($conversation['user']->privatePhotoPermissionCnt()>0) <div class="un-lock-2"><img src="{{URL::asset('img/unlock.png')}}"></div>  @endif
                                        <a href="{{route('filter-conversation', ['user_id'=>$conversation['user']->getEncryptedId()])}}">
                                            <img src="<?php echo (($conversation['user']->getVendorProfilePic())); ?>" class="media-object img-thumbnail" style="width:80px">
                                            <i class="fa fa-circle <?php echo (($conversation['user']->isOnline()=='1')?:'fa-red'); ?> " aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="{{route('filter-conversation', ['user_id'=>$conversation['user']->getEncryptedId()])}}" class="msg-name">{{$conversation['user']['first_name']}} <!--{{config('constants.site_code').$conversation['user']['id']}}--></a>
                                            &nbsp;&nbsp;<small><i>{{ts2time($conversation['msg']['createdAt'])}}</i></small>
                                            <a class="pull-right" href="{{route('send-trash', ['user_id'=>$conversation['user']->getEncryptedId(), 'pageType'=>'filter'])}}"><i class="fa fa-trash-o"></i></a>

                                        </h4>
                                        <p><small>{{$conversation['user']->city->name}}, {{$conversation['user']->country->name}}</small></p>
                                        @if($conversation['msg']->unread_count<=0)<p><strong><i class="fa fa-reply" aria-hidden="true"></i></strong> Replied </p>@endif
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="media">
                                <div class="alert alert-info" role="alert">
                                    No messages found.
                                </div>
                            </div>
                        @endif

                    </div>

                    @if(config('usersmanagement.enableMsgPagination'))
                        {{ ($conversationObj)->links() }}
                    @endif

                </div>

            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script>
        $('.show_unread').click(function() {
            if (this.checked) {  // if checkbox is selected
                window.location.href="{{route('inbox_showread', ['user_id'=>'unread'])}}";
            } else {
                window.location.href="{{route('inbox')}}";
            }
        });

        // $('.collapse').on('shown.bs.collapse',function(){
        //     $(this).parent().find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
        // }).on('hidden.bs.collapse',function(){
        //     $(this).parent().find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
        // });

        $(document).ready(function(){
            $('#SearchParameters .collapse').on('shown.bs.collapse',function(){
                $(this).parent().find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-minus-circle');
            }).on('hidden.bs.collapse',function(){
                $(this).parent().find('.fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
            })
        });

        $(function(){
            // Age slider
            $( "#slider-age" ).slider({
                range: true,
                min: <?php echo config('constants.search_defaults.start_age'); ?>,
                max: <?php echo config('constants.search_defaults.end_age'); ?>,
                values: [<?php echo (int) $age_from; ?>, <?php echo (int) $age_to; ?>],
                slide: function( event, ui ) {
                    $( "#age" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ]);
                },
                stop : function(event, ui)
                {
                    $( "#age" ).val($( "#slider-age" ).slider( "values", 0 ) + " - " + $( "#slider-age" ).slider( "values", 1 ) );

                }
            });
            $( "#age" ).val($( "#slider-age" ).slider( "values", 0 ) + " - " + $( "#slider-age" ).slider( "values", 1 ) );

            // Height slider
            $( "#slider-height" ).slider({
                range: true,
                min: <?php echo config('constants.search_defaults.start_height'); ?>,
                max: <?php echo config('constants.search_defaults.end_height'); ?>,
                values: [<?php echo (int) $height_from; ?>, <?php echo (int) $height_to; ?>],
                slide: function( event, ui ) {
                    $("#height").val(ui.values[ 0 ] + " cm " + " - " + ui.values[ 1 ] + " cm"  );
                },
                stop : function(event, ui)
                {
                    $( "#height" ).val($( "#slider-height" ).slider( "values", 0 ) + " cm " + " - " + $( "#slider-height" ).slider( "values", 1 )+ " cm " );
                }
            });
            $( "#height" ).val($( "#slider-height" ).slider( "values", 0 ) + " cm " + " - " + $( "#slider-height" ).slider( "values", 1 )+ " cm " );
        });

        $('.btn-filter').click(function() {
            var action=$(this).data('action'); //show/hide
            var curobj='on';
            var cursuccess='btn-success';
            var anosuccess='btn-danger';
            var status='0';
            if(action=='on') {
                curobj = 'off';
                status='1';
                anosuccess='btn-success';
                cursuccess='btn-danger';
            }

            jQuery('#'+action+'Filter').removeClass(cursuccess);
            jQuery('#'+action+'Filter').addClass('btn-default');
            jQuery('#'+curobj+'Filter').removeClass('btn-default');
            jQuery('#'+curobj+'Filter').addClass(anosuccess);
            jQuery('#status').val(status);
        });

    </script>

@endsection