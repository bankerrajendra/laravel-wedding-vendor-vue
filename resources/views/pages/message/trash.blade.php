@extends('layouts.app')
@section('template_title')
    Messages
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="vendor-setting">
        <div class="container">
            <div class="row">

                @include('partials.message-navigation')

                <div class="col-sm-8 msg-section">
                    <div class="row hidden-xs">
                        <div class="col-sm-6 col-xs-6"><h4>Trash</h4></div>
                    </div>

                    @include('partials.message-navigation-mobile')

                    <div class="row">
                        <div class="col-sm-12 col-xs-12 no_padding">
                            @if(!is_null($conversations))
                                @foreach($conversations as $conversation)
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="{{route('trash-conversation', ['user_id'=>$conversation['user']->getEncryptedId()])}}">
                                                <img src="<?php echo (($conversation['user']->getVendorProfilePic())); ?>" class="media-object img-thumbnail" style="width:80px">
                                                <i class="fa fa-circle <?php echo (($conversation['user']->isOnline()=='1')?:'fa-red'); ?> " aria-hidden="true"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <a href="{{route('trash-conversation', ['user_id'=>$conversation['user']->getEncryptedId()])}}" class="msg-name">{{$conversation['user']->get_firstname()}} </a>
                                                &nbsp;&nbsp;<small><i>{{ts2time($conversation['msg']->createdAt)}}</i></small>
                                            </h4>

                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="media">
                                    <div class="alert alert-info" role="alert">
                                        No messages found.
                                    </div>
                                </div>
                            @endif

                        </div>
                        <div class="col-sm-12 col-xs-12">
                            @if(config('usersmanagement.enableMsgPagination'))
                                {{ ($conversationObj)->links() }}
                            @endif
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
@endsection