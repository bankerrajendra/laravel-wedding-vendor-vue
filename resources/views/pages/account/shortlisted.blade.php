@extends('layouts.app')
@section('template_title')
    Shortlisted Profiles
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="self-profile account-set">
        <div class="container">
            <div class="row">

                @include('partials.account-navigation')
                <div class="col-sm-8 panel-body">
                    <div class="tab-content">
                        <h4>Shortlisted Profiles</h4>
                        <hr>
                        @if($shortlistedUsers)
                            @foreach($shortlistedUsers as $hiddenProfile)
                                <?php $blockedProf=$hiddenProfile->to_user(); ?>
                                <div class="media" id="shortlist-{{$hiddenProfile->id}}">
                                    <div class="media-left">
                                        @if($blockedProf->privatePhotoPermissionCnt()>0) <div class="un-lock-2"><img src="{{URL::asset('img/unlock.png')}}"></div>  @endif
                                        <a href="{{$blockedProf->getVendorProfileLink()}}" >
                                            <img src="{{$blockedProf->getVendorProfilePic()}}" class="media-object img-thumbnail" style="width:70px">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="{{route('user-profile', ['user_id'=>$blockedProf->getEncryptedId()])}}" style="color: #469c46;">{{ucfirst($blockedProf->first_name)}}</a> &nbsp;&nbsp;<small><i>{{$blockedProf->getAge()}} Years {{$blockedProf->city->name}}, {{$blockedProf->country->name}}</i></small>
                                            <a href="javascript:;" class="pull-right deleteShortlisted" data-profileid="{{$blockedProf->getEncryptedId()}}" data-shortlistid="{{$hiddenProfile->id}}"><i class="fa fa-remove"></i> Remove</a>

                                        </h4>
                                        <p>
                                            @if(!is_null($blockedProf->users_information->about)){{ showReadMore($blockedProf->users_information->about, 90, $blockedProf->getVendorProfileLink()) }}@endif

                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <br><br>
                            <div style="padding-left: 20px">No Record Found</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
@endsection