@extends('layouts.app')
@section('template_title')
    Messages
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="self-profile account-set">
        <div class="container">
            <div class="row">

                @include('partials.account-navigation')
                <div class="col-sm-8 panel-body">
                    <div class="row">
                        <div class="col-sm-12 setting-section">
                            <div class="panel panel-success">
                                <div class="panel-heading"><h4 class="panel-title">Email Address</h4></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-4 col-lg-3">
                                            <ul class="list-group">
                                                <li class="list-group-item">Email:</li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-8 col-xs-8 col-lg-6">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <span class="text-info1">{{$currUser->email}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Password <a class="pull-right" id="change-pwd" href="javascript:void(0)"><i class="fa fa-pencil" aria-hidden="true"></i> <small>Change Password</small></a></h4>
                                </div>

                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-4 col-xs-4 col-lg-3">
                                            <ul class="list-group">
                                                <li class="list-group-item">Password:</li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-8 col-xs-8 col-lg-6">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <span class="text-info2">********</span>
                                                    <form method="post" action="{{ route('post-change-password') }}" id="changePasswordForm" style="display: none;">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <input type="password" class="form-control" placeholder="Current Password" name="current_password" id="current_password">
                                                            @if ($errors->has('current-password'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('current-password') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>

                                                        <div class="form-group">
                                                            <input type="password" class="form-control" placeholder="New Password" name="new_password" id="new_password">
                                                            @if ($errors->has('new-password'))
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('new-password') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>

                                                        <div class="form-group">
                                                            <input type="password" class="form-control" placeholder="Confirm Password" name="new_password_confirmation" id="new_password_confirmation">
                                                        </div>

                                                        <input class="btn btn-success" type="submit" value="Update">
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Address<a class="pull-right" id="edit-add" href="javascript:void(0)"><i class="fa fa-pencil" aria-hidden="true"></i> <small>Edit Address</small></a></h4>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-4 col-lg-3">
                                            <ul class="list-group">
                                                <li class="list-group-item">Country:</li>
                                                <li class="list-group-item">State:</li>
                                                <li class="list-group-item">City:</li>
                                                <li class="list-group-item">Area Code:</li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-8 col-xs-8 col-lg-6">
                                            <ul class="list-group">
                                                <li class="list-group-item text-info3" id="countryId">{{@$currUser->country->name}}</li>
                                                <li class="list-group-item text-info3" id="stateId">{{@$currUser->state->name}}</li>
                                                <li class="list-group-item text-info3" id="cityId">{{@$currUser->city->name}}</li>
                                                <li class="list-group-item text-info3" id="zipcodeId">{{@$currUser->users_information['zipcode']}}</li>
                                            </ul>
                                                <form id="changeLocationForm" style="display: none;" method="post" action="{{ route('post-change-location') }}">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <select name="country_code" class="form-control selectpicker dynamic_differ" dependent="state" id="country" data-show-subtext="true" data-live-search="true" >
                                                            <option value="">Select</option>
                                                            @foreach($countries as $ctr)
                                                                <option value="{{ $ctr->id }}" {{$currUser->country_id == $ctr->id ? "selected":"" }}>{{ $ctr->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block country_code_err invalid-feedback"  style=";font-weight:normal;color:#a94442;display:none">
                                                            Country field is required
                                                        </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <select class="form-control selectpicker dynamic_differ" dependent="city" id="state" data-show-subtext="true" data-live-search="true" name="state_id">
                                                            <option value="">Select</option>
                                                            @foreach(getStateByCountry($currUser->country_id) as $ct)
                                                                <option value="{{ $ct->id }}" {{ ($currUser->state_id) == $ct->id ? "selected":"" }}>{{ $ct->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block state_code_err invalid-feedback"  style=";font-weight:normal;color:#a94442;display:none">
                                                            State field is required
                                                        </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  id="city" name="city_id">
                                                            <option value="">Select</option>
                                                            @foreach(getCityByState($currUser->state_id) as $ct)
                                                                <option value="{{ $ct->id }}" {{($currUser->city_id) == $ct->id ? "selected":"" }}>{{ $ct->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="help-block city_code_err invalid-feedback"  style=";font-weight:normal;color:#a94442;display:none">
                                                            City field is required
                                                        </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="zipcode" value="{{ ($currUser->users_information['zipcode']) }}" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="btn btn-warning" type="submit" value="Update"></div>
                                                </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Your Profile Information</h4>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>Body Type:</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a class="btn {{($currUser->users_information->body_type_show=='Y'?'btn-success':'btn-default')}} btn-sm " data-action="hide" id="showBody_type" data-tb="users_information"  data-code="Body_type" >Show</a>
                                                            <a class="btn {{($currUser->users_information->body_type_show=='N'?'btn-success':'btn-default')}} btn-sm " data-action="show" id="hideBody_type" data-tb="users_information" data-code="Body_type" >Hide</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Body Weight:</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a class="btn {{($currUser->users_information->weight_show=='Y'?'btn-success':'btn-default')}} btn-sm " data-action="hide" id="showWeight" data-tb="users_information"  data-code="Weight" >Show</a>
                                                            <a class="btn {{($currUser->users_information->weight_show=='N'?'btn-success':'btn-default')}} btn-sm " data-action="show" id="hideWeight" data-tb="users_information" data-code="Weight" >Hide</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Complexion:</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a class="btn {{($currUser->users_information->complexion_show=='Y'?'btn-success':'btn-default')}} btn-sm " data-action="hide" id="showComplexion" data-tb="users_information"  data-code="Complexion" >Show</a>
                                                            <a class="btn {{($currUser->users_information->complexion_show=='N'?'btn-success':'btn-default')}} btn-sm " data-action="show" id="hideComplexion" data-tb="users_information" data-code="Complexion" >Hide</a>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Horoscope:</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a class="btn {{($currUser->users_information->horoscope_show=='Y'?'btn-success':'btn-default')}} btn-sm " data-action="hide" id="showHoroscope" data-tb="users_information"  data-code="Horoscope" >Show</a>
                                                            <a class="btn {{($currUser->users_information->horoscope_show=='N'?'btn-success':'btn-default')}} btn-sm " data-action="show" id="hideHoroscope" data-tb="users_information" data-code="Horoscope" >Hide</a>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Sect/Math-hab:</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a class="btn {{($currUser->users_information->sect_show=='Y'?'btn-success':'btn-default')}} btn-sm " data-action="hide" id="showSect" data-tb="users_information"  data-code="Sect" >Show</a>
                                                            <a class="btn {{($currUser->users_information->sect_show=='N'?'btn-success':'btn-default')}} btn-sm " data-action="show" id="hideSect" data-tb="users_information" data-code="Sect" >Hide</a>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Sub-Caste:</td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <a class="btn {{($currUser->users_information->subcaste_show=='Y'?'btn-success':'btn-default')}} btn-sm " data-action="hide" id="showSubcaste" data-tb="users_information"  data-code="Subcaste" >Show</a>
                                                            <a class="btn {{($currUser->users_information->subcaste_show=='N'?'btn-success':'btn-default')}} btn-sm " data-action="show" id="hideSubcaste" data-tb="users_information" data-code="Subcaste" >Hide</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-success">
                                <div class="panel-heading"><h4 class="panel-title">Deactive/Delete Account</h4></div>
                                <div class="panel-body d_account">
                                    <div class="btn-group">
                                        <a class="btn {{($currUser->account_show=='Y'?'btn-success':'btn-default')}} btn-sm" data-action="hide" id="showAccount" data-tb="users" data-code="Account" >Show Profile</a>
                                        <a class="btn {{($currUser->account_show=='N'?'btn-success':'btn-default')}} btn-sm" data-action="show" id="hideAccount" data-tb="users" data-code="Account" >Hide Profile</a>
                                    </div>
                                    <p>Click to check or uncheck the sections you want to show or hide your profile When your profile is hidden you will not appear in any dashboards or search results.</p>
                                    <hr>
                                    <a class="btn btn-warning btn-sm" data-target="#myModal" data-toggle="modal">Deactivate / Delete Account</a>
                                    <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button class="close" type="button" data-dismiss="modal">×</button>
                                                    <h4 class="modal-title">Deactivate or Delete Account</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>If you're just hoping to take a break, we recommend disabling your account, not deleting it.</p>
                                                    <p><b>Disable your account</b></p>
                                                    <p>Your profile and photos will be removed from the site, but we'll keep everything for you in case you return so that you can start right where you left off.</p>
                                                    <p>To reactivate your account, simply log back in at any time.</p>
                                                    <a class="btn btn-success btn-sm" href="{{route('disable-account')}}">Deactivate Account</a>
                                                    <p><b>Delete your account</b></p>
                                                    <hr>
                                                    <p>This is PERMANENT</p>
                                                    <p>If you want to use MuslimWedding again, you'll need to create a new account and fill out your profile again.</p>
                                                    <p><b>Note:</b> Any messages you've sent will still be in the recipients' inboxes, even if you delete your account.</p>
                                                    <a class="btn btn-warning btn-sm" href="{{route('delete-account')}}">Delete Account</a>
                                                    <p>Once you delete your account, your profile will be removed from search and will no longer be viewable by other members and you must sign up again as a new customer. if you want to use our website. We will retain certain data for analytical purposes and record-keeping integrity, as well as to prevent fraud, collect any fees owed, enforce our terms and conditions, take actions we deem necessary to protect the integrity of our Services or our users, or take other actions otherwise permitted by law.</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <p>Please bear in mind that deactivating or deleting your Account removes all information associated with your account from search and will no longer be viewable by other members.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script>
        $('#change-pwd').click(function(){
            $('#changePasswordForm').show();
            $('.text-info2').hide();
        });

        $('#edit-add').click(function(){
            $('#changeLocationForm').show();
            $('.text-info3').hide();
        });

        $("#changePasswordForm").on('submit', function(e) {

            e.preventDefault();
            var _token = $('input[name="_token"]').val();
            var url = route('post-change-password');

            $.ajax({
                url: url,
                method: "POST",
                data: $("#changePasswordForm").serialize(),
                success: function (result) {
                    // sessionStorage.setItem("profile_unverified_popup_shown", "yes");
                    // $(".modal-footer").prepend("<span style='color:green'>Profile updated successfuly! Wait for admin to verify it again</span>");
                    // setTimeout(function () {
                    //     $("#FeedbackModal").modal("hide");
                    // }, 1500)
                    if(typeof(result.error)!="undefined") {
                            //$("#changePasswordForm").closest('.panel-body').prepend('<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button> ' + result.error + '</div>');
                        var el = document.createElement("div");
                        el.setAttribute("class","alert alert-danger avatar-alert alert-dismissable");
                        el.innerHTML =  result.error;
                        setTimeout(function(){
                            el.parentNode.removeChild(el);
                        },2000);
                        $("#changePasswordForm").closest('.panel-body').prepend(el);
                    } else if(typeof(result.success)!="undefined") {
                        //$("#changePasswordForm").closest('.panel-body').prepend('<div class="alert alert-success avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button> ' + result.success + '</div>');
                        var el = document.createElement("div");
                        el.setAttribute("class","alert alert-success avatar-alert alert-dismissable");
                        el.innerHTML =  result.success;
                        setTimeout(function(){
                            el.parentNode.removeChild(el);
                        },2000);
                        $("#changePasswordForm").closest('.panel-body').prepend(el);

                        $('#changePasswordForm').hide();
                        $('.text-info2').show();
                    }
                }
            })
        });

        $("#changeLocationForm").on('submit', function(e) {

            e.preventDefault();
            var _token = $('input[name="_token"]').val();
            var url = route('post-change-location');

            $.ajax({
                url: url,
                method: "POST",
                data: $("#changeLocationForm").serialize(),
                success: function (result) {
                    if(typeof(result.error)!="undefined") {
                        //$("#changePasswordForm").closest('.panel-body').prepend('<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button> ' + result.error + '</div>');
                        var el = document.createElement("div");
                        el.setAttribute("class","alert alert-danger avatar-alert alert-dismissable");
                        el.innerHTML =  result.error;
                        setTimeout(function(){
                            el.parentNode.removeChild(el);
                        },2000);
                        $("#changeLocationForm").closest('.panel-body').prepend(el);
                    } else if(typeof(result.success)!="undefined") {
                        //$("#changePasswordForm").closest('.panel-body').prepend('<div class="alert alert-success avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button> ' + result.success + '</div>');
                        var el = document.createElement("div");
                        el.setAttribute("class","alert alert-success avatar-alert alert-dismissable");
                        el.innerHTML =  result.success;
                        setTimeout(function(){
                            el.parentNode.removeChild(el);
                        },2000);
                        $("#changeLocationForm").closest('.panel-body').prepend(el);
                        $("#countryId").text(result.country);
                        $("#stateId").text(result.state);
                        $("#cityId").text(result.city);
                        $("#zipcodeId").text(result.zipcode);
                        $('#changeLocationForm').hide();
                        $('.text-info3').show();
                    }
                }
            })
        });

    </script>

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection