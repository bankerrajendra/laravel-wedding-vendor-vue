@extends('layouts.app')
@section('template_title')
    Messages
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="self-profile">
        <div class="container">
            <div class="row">
                <?php $unread_notification_counter = get_notification_counter(); ?>
                @include('partials.account-navigation')

                <div class="col-sm-8 panel-body">

                    <ul class="nav nav-tabs per_nav">
                        <li class="@if($page=="request") active  @endif" ><a  href="{{route('private-photo-permission')}}">Private Photo Permission</a>@if($unread_notification_counter['private_photo_req_cnt']>0) <div class="cir">{{$unread_notification_counter['private_photo_req_cnt']}}</div>@endif</li>
                        <li class="@if($page=="shared") active  @endif" ><a  href="{{route('share-photo-permission')}}">Share Photo Permission</a></li>
                    </ul>

                    <div class="tab-content">
                        @if($page=="request")
                            <div id="home" class="tab-pane fade @if($page=='request') in active @endif">

                                @if($sharedProfiles && count($sharedProfiles)>0)
                                    <div class="panel panel-success inner-panel">
                                        <div class="panel-heading text-right"><a href="{{route('deny-all-photos')}}"> Deny All</a></div>
                                    </div>
                                    @foreach($sharedProfiles as $sharedProfile)
                                        <?php $sharedProf=$sharedProfile->from_user();

                                        ?>
                                        <div class="media">
                                            <div class="media-left">
                                                @if($sharedProf->privatePhotoPermissionCnt()>0) <div class="un-lock-2"><img src="{{URL::asset('img/unlock.png')}}"></div>  @endif
                                                <a href="{{$sharedProf->profileLink()}}" >
                                                    <img src="{{$sharedProf->getVendorProfilePic()}}" class="media-object img-thumbnail" style="width:70px">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="{{route('user-profile', ['user_id'=>$sharedProf->getEncryptedId()])}}"  style="color: #469c46;" >{{$sharedProf->first_name}}</a> &nbsp;&nbsp;<small><i>{{$sharedProf->getAge()}} Years</i></small>
                                                    <div class="dropdown pull-right">
                                                        <a class="dropdown-toggle" href="" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                                        <ul class="dropdown-menu">
                                                            @if($sharedProfile->status!='A' && !(($sharedProf->banned == 1 || $sharedProf->deactivated == 1 || $sharedProf->deleted_at != null || $sharedProf->account_show=='N')) && !user_blocked_to_me($sharedProf->id) && !user_skipped_to_me($sharedProf->id))
                                                                <li><a href="javascript:;" class="acceptRequestsPhotos" data-profileid="{{$sharedProf->getEncryptedId()}}">Accept</a></li>
                                                            @endif
                                                            <li><a href="javascript:;" class="denyRequestsPhotos" data-profileid="{{$sharedProf->getEncryptedId()}}">Deny</a></li>
                                                        </ul>
                                                    </div>
                                                </h4>
                                                <p><small>{{$sharedProf->city->name}}, {{$sharedProf->country->name}}</small></p>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <br><br>
                                    <div style="padding-left: 20px">No Record Found</div>
                                @endif

                            </div>
                        @elseif($page=="shared")
                            <div id="menu1" class="tab-pane fade @if($page=='shared') in active @endif">


                                @if($sharedProfiles && count($sharedProfiles)>0)
                                    <div class="panel panel-success inner-panel">
                                        <div class="panel-heading text-right"><a href="{{route('unshare-all-photos')}}"> Unshare All</a></div>
                                    </div>
                                    @foreach($sharedProfiles as $sharedProfile)
                                        <?php $sharedProf=$sharedProfile->to_user(); ?>
                                        <div class="media">
                                            <div class="media-left">
                                                @if($sharedProf->privatePhotoPermissionCnt()>0) <div class="un-lock-2"><img src="{{URL::asset('img/unlock.png')}}"></div>  @endif
                                                <a href="{{$sharedProf->profileLink()}}" >
                                                <img src="{{$sharedProf->getVendorProfilePic()}}" class="media-object img-thumbnail" style="width:70px">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="{{route('user-profile', ['user_id'=>$sharedProf->getEncryptedId()])}}"  style="color: #469c46;" >{{$sharedProf->first_name}}</a> &nbsp;&nbsp;<small><i>{{$sharedProf->getAge()}} Years</i></small>
                                                    <div class="dropdown pull-right">
                                                        <a class="dropdown-toggle" href="" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="javascript:;" class="unsharePhotos" data-profileid="{{$sharedProf->getEncryptedId()}}">Unshare</a></li>
                                                        </ul>
                                                    </div>
                                                </h4>
                                                <p><small>{{$sharedProf->city->name}}, {{$sharedProf->country->name}}</small></p>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <br><br>
                                    <div style="padding-left: 20px">No Record Found</div>
                                @endif

                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
@endsection