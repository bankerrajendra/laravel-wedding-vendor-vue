@extends('layouts.app')
@section('template_title')
    Hidden Members
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="self-profile account-set">
        <div class="container">
            <div class="row">

                @include('partials.account-navigation')
                <div class="col-sm-8 panel-body">
                    <div class="tab-content">
                        <h4>Hidden Members</h4>
                        <hr>
                        @if($hiddenProfiles)
                            @foreach($hiddenProfiles as $hiddenProfile)
                                <?php $blockedProf=$hiddenProfile->to_user(); ?>
                                <div class="media" id="skip-{{$blockedProf->id}}">
                                    <div class="media-left">
                                        @if($blockedProf->privatePhotoPermissionCnt()>0) <div class="un-lock-2"><img src="{{URL::asset('img/unlock.png')}}"></div>  @endif
                                        <a href="{{$blockedProf->profileLink()}}" >
                                            <img src="{{$blockedProf->getVendorProfilePic()}}" class="media-object img-thumbnail" style="width:70px">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="{{route('user-profile', ['user_id'=>$blockedProf->getEncryptedId()])}}" style="color: #469c46;">{{ucfirst($blockedProf->first_name)}}</a> &nbsp;&nbsp;<small><i>{{$blockedProf->getAge()}} Years</i></small>
                                            <a href="javascript:;" class="deleteSkippedProfile pull-right" data-profileid="{{$blockedProf->getEncryptedId()}}" data-skipid="{{$blockedProf->id}}"  >Unhide</a>
                                        </h4>
                                        <p>{{$blockedProf->city->name}}, {{$blockedProf->country->name}}</p>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <br><br>
                            <div style="padding-left: 20px">No Record Found</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
@endsection