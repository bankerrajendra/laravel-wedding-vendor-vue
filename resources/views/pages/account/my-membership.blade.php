@extends('layouts.app')
@section('template_linked_css')
	<link href="{{asset('css/editor.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
	<div class="vendor-setting">
		<div class="container">
			<div class="row">
				@if(!(Auth::user()->is_active()))
					<div class="text-center">
						<div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
					</div>
				@endif
				<div class="col-sm-4 hidden-xs">
					@include('partials.vendor-side-menu')
				</div>
				<div class="col-sm-8">
					@if(session('success'))
						<div class="alert alert-success" role="alert">
							{{session('success')}}
						</div>
					@endif
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<h4>My Membership</h4>
					<hr>

					@if(!empty($membership) && count($membership)>0)
						@if(date("Y-m-d 00:00:00") > $latest_membership->membership_end_date)
							<p>Your membership is being expired on {{ date('F d, Y', strtotime($latest_membership->membership_end_date)) }}, It's time to renew your membership.</p>
						@else
							<p>
								<b>
									{{$latest_membership->duration}} {{$latest_membership->duration_type}} {{$latest_membership->plan_name}}
								</b>
							</p>
							<p>Your membership will expire on {{ date('F d, Y', strtotime($latest_membership->membership_end_date)) }} We will notify you when it's time to renew your membership.</p>
						@endif
						<button type="button" class="btn btn-success" data-toggle="collapse" data-target="#payment-history" aria-expanded="true">View Payments</button>
						<div id="payment-history" class="collapse" aria-expanded="true" style="">
							<br>
							<h4 class="modal-title"><b>Receipt</b></h4>
							<br>
							<table class="table table-hover">
								<thead>
								<tr>
									<th>Plan</th>
									<th>Date</th>
									<th>Price</th>
									<th>Method</th>
								</tr>
								</thead>
								<tbody>
								@foreach($membership as $membership_records)
									<tr>
										<td>{{$membership_records->duration}} {{$membership_records->duration_type}} {{$membership_records->plan_name}}</td>
										<td>{{ date('F d, Y', strtotime($membership_records->created_at)) }}</td>
										<td>{{$membership_records->currency}} {{$membership_records->plan_amount}}</td>
										<td>
											@if($membership_records->payment_gateway_name == "Helcim" || $membership_records->payment_gateway_name == "Merrco")
												{{$membership_records->order_info->card_type}}
											@else
												{{$membership_records->payment_gateway_name}}
											@endif
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
							<br>
							<p><i>Payment will show up as *SNV Wedding Gallery Approved</i></p>
						</div>
					@else
						<p>You have <b>Free</b> membership plan.</p>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection
@section('footer_scripts')
@endsection