@extends('layouts.app')
@section('template_title')
    Notification
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="self-profile  set-membership">
        <div class="container">
            <div class="row">
                <?php $unread_notification_counter = get_notification_counter(); ?>
                @include('partials.account-navigation')
				<div class="col-sm-8 panel-body">
					@if(session('success'))
						<div class="alert alert-success" role="alert">
							{{session('success')}}
						</div>
					@endif
					@if ($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<ul class="nav nav-tabs mobile-nav">
						<li class="active"><a data-toggle="tab" href="#email-settings" aria-expanded="false">Email Settings</a></li>
						<li class="" id="flag-notification-tab"><a data-toggle="tab" href="#flag-notification" aria-expanded="false">Flag Notification</a>@if($unread_notification_counter['flag_notif_cnt']>0) <div class="cir">{{$unread_notification_counter['flag_notif_cnt']}}</div>@endif</li>
					</ul>
					<div class="tab-content">
						<div id="email-settings" class="tab-pane fade active in">
							<div class="panel-body">
								<form method="post" name="submit-email-notification" action="{{ route('handle-email-notification') }}">
								@csrf
									<div class="checkbox">
										<label><input type="checkbox" value="Y" name="notify_sends_message" @if(isset($user_info) && $user_info->notify_sends_message == "Y")checked="checked"@endif> If someone sends me a message</label>
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="Y" name="notify_favorites_me" @if(isset($user_info) && $user_info->notify_favorites_me == "Y")checked="checked"@endif> If someone favorites me</label>
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="Y" name="notify_views_profile" @if(isset($user_info) && $user_info->notify_views_profile == "Y")checked="checked"@endif> If someone views my profile</label>
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="Y" name="notify_content_approve_deny" @if(isset($user_info) && $user_info->notify_content_approve_deny == "Y")checked="checked"@endif> Member Content approved or denied</label>
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="Y" name="notify_new_matched_profiles" @if(isset($user_info) && $user_info->notify_new_matched_profiles == "Y")checked="checked"@endif> New & Matched Profiles</label>
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="Y" name="notify_news_letter" @if(isset($user_info) && $user_info->notify_news_letter == "Y")checked="checked"@endif> Email newsletters</label>
									</div>

									<hr>
									<button class="btn btn-success" type="submit">Save Changes</button>
								</form>
							</div>
						</div>
						<div id="flag-notification" class="tab-pane fade">
							@if($reports->count() > 0)
								@foreach($reports as $report)
									<div class="media" id="report-sec-{{$report->id}}">
										<div class="media-body">
											<h4 class="media-heading">ADMIN &nbsp;&nbsp;<small><i>{{ date('h:i a, d M Y', strtotime($report->updated_at)) }}</i></small> <a href="javascript:void(0);" class="pull-right trashReport" data-report-id="{{$report->id}}"><i class="fa fa-trash-o"></i></a></h4> 
											<p><span class="label label-warning">Admin</span> {{$report->admin_reply}}</p>
											@if($report->type == "report")
												<p><span class="label label-success">Me</span> 
												@if($report->to_user_reply != "")
													{{$report->to_user_reply}}
												@else
													<span style="display: none;" id="toUserReplyTxt-{{$report->id}}"></span>
													<div id="sbmt-rply-{{$report->id}}">
														<textarea name="to_user_reply" id="to-user-reply-{{$report->id}}" class="form-control"></textarea>
														<br /><input type="button" name="submit-user-reply" class="btn btn-small btn-success sbmtToUserReply" value="Submit" data-report-id="{{$report->id}}" />
													</div>
												@endif
												</p>
											@endif
										</div>
									</div>
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
jQuery(".sbmtToUserReply").on('click', function(e) {
    var url = '{{route("report-user-reply")}}';
    var report_id = $(this).data('reportId');
    var to_user_reply = $("#to-user-reply-"+report_id).val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:url,
        method:"POST",
        data:{ 
            report_id: report_id,
            to_user_reply: to_user_reply,
            _token:_token
        },
        success:function(result)
        {
           if(result.status == 1) {
			   	$("#to-user-reply-"+report_id).before('<div class="alert alert-success">'+result.message+'</div><br />');
				$("#toUserReplyTxt-"+report_id).html(to_user_reply).show().fadeIn();
				$("#sbmt-rply-"+report_id).fadeOut(1000, function() { 
					$(this).remove();
				});
				$('.alert-success').delay(3000).fadeOut();
           } else {
				$("#to-user-reply-"+report_id).before('<div class="alert alert-danger">'+result.message+'</div><br />');
				$('.alert-danger').delay(3000).fadeOut();
           }
        }
    })
});
jQuery(".trashReport").on('click', function(e) {
    var url = '{{route("report-user-trash")}}';
    var report_id = $(this).data('reportId');
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:url,
        method:"POST",
        data:{ 
            report_id: report_id,
            _token:_token
        },
        success:function(result)
        {
           if(result.status == 1) {
				$("#report-sec-"+report_id).fadeOut(1000, function() { 
					$(this).remove();
				});
           } else {
				alert("some error while deleting.");
           }
        }
    })
});

jQuery("#flag-notification-tab").on('click', function(e) {
    var url = '{{route("mark-flag-notification-read")}}';
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:url,
        method:"POST",
        data:{ 
            _token:_token
        },
        success:function(result)
        {
           if(result.status != 1) {
				alert("some error while mark flag notofication as read.");
           }
        }
    })
});
</script>
@endsection