@extends('adminlte::page')
@section('title')
    Showing {{$page_title}} Members
@endsection
@section('content_header')
    <h1>{{$page_title}} Members</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
        .trf {
            transform: rotate(61deg);
            -ms-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            /* -webkit-transform: rotate(90deg); */
            -o-transform: rotate(90deg);
            margin-top: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">


                        {{--@include('partials.admin-user-navigation')--}}

                        </div>
                    </div>

                    <div class="box-body">
                        <?php $stats=admin_user_counter(); ?>
                        @if(isset($type))
                        <span id="pull-left">
                            <a href="{{ route('paid-members') }}">
                                <button class="btn @if($type == '') btn-info @else btn-default  @endif "><i class="fa fa-users"></i> All Members ({{$all_count}})</button>
                            </a>
                            <a href="{{ route('paid-members', [ 'type' => 'p' ]) }}">
                                <button class="btn @if($type == 'p') btn-info @else btn-default  @endif "><i class="fa fa-users"></i> Paid Members ({{$pm_count}})</button>
                            </a>
                            <a href="{{ route('paid-members', [ 'type' => 'fd' ]) }}">
                                <button class="btn @if($type == 'fd') btn-info @else btn-default  @endif "><i class="fa fa-users"></i> Free Daimond Members ({{$fdm_count}})</button>
                            </a>
                        </span>
                        @endif
                        <div class="table-responsive users-table">

                            <table class="table table-striped table-sm data-table">

                                <caption id="user_count">
                                    <button class="btn btn-default btn-sm check_all" ><i style="clor:#658898" class="fa fa-circle"></i></button>
									{{ trans_choice('usersmanagement.users-table.caption', 1, ['userscount' => $users->total()]) }}
                                </caption>

                                <thead class="thead">
                                <tr>
                                    <th>@lang('usersmanagement.users-table.id')</th>
                                    <th>Image</th>
                                    <th>@lang('usersmanagement.users-table.name')</th>
                                    <th class="hidden-xs">@lang('usersmanagement.users-table.email')</th>
                                    <th>Current Status</th>
                                    <th>Country</th>

                                    <th class="hidden-sm hidden-xs hidden-md">Membership Start</th>
                                    <th class="hidden-sm hidden-xs hidden-md">Membership End</th>
                                    <th>@lang('usersmanagement.users-table.actions')</th>


                                    <th class="no-search no-sort"></th>
                                    <th class="no-search no-sort"></th>
                                    <th class="no-search no-sort"></th>
                                </tr>
                                </thead>
                                <tbody id="users_table">

                                @foreach($users as $user)
                                    <tr>
                                        <td>{{--<input type="checkbox" id="{{ $user->id }}" class="checkable">--}} {{$pageNo++}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('vendors/' . $user->id . '/edit') }}"  title="Edit" style="background: none; border: none">
                                                <img src="{{$user->getVendorProfilePic()}}" width="100">
                                            </a>
                                        </td>
                                        <td>{{$user->first_name.' '.$user->last_name}}</td>
                                        <td class="hidden-xs">
                                            <a href="mailto:{{ $user->email }}" title="email {{ $user->email }}">{{ $user->email }}</a>
                                            <br /><br />
                                            <a class="btn btn-info send-email" href="javascript:void(0);" data-toggle="modal" title="Send Email" data-user-id="{{$user->id}}">
                                                Send Email
                                            </a>
                                        </td>

                                        <td>
                                            @if($user->banned == "1")
                                                <button class='btn btn-sm btn-warning'><i class="fa fa-ban"></i> Banned </button>
                                            @elseif($user->deactivated == "1")
                                                <button class='btn btn-sm btn-danger'><i class="fa fa-power-off"></i> Deactivated </button>
                                            @elseif($user->access_grant == "N")
                                                <button class='btn btn-sm btn-info'><i class="fa fa-thumbs-down"></i> Access Revoked </button>
                                            @elseif($user->deleted_at !== null)
                                                <button class='btn btn-danger btn-info'><i class="fa fa-remove"></i> Deleted </button>
                                            @else
                                                <button class='btn btn-sm btn-success'><i class="fa fa-check"></i> Active </button>
                                            @endif
                                        </td>
                                        <td>@if($user->country){{$user->country->name}}@endif</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{date('F d, Y', strtotime($user->paid_members->first()->membership_start_date))}}</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{date('F d, Y', strtotime($user->paid_members->first()->membership_end_date))}}
                                            <br>
                                            <a class="btn btn-success view-payments" href="javascript:void(0);" data-toggle="modal" title="View Payments" data-user-id="{{$user->id}}">
                                                View Payments
                                            </a>
                                        </td>
                                        <td>
                                            <a class="btn btn-success edit-plan" href="javascript:void(0);" data-toggle="modal" title="Edit Plan" data-user-id="{{$user->id}}">
                                                Edit Plan
                                            </a>
                                            {{-- <br /><br />
                                            <a class="btn btn-primary send-sms" href="javascript:void(0);" data-toggle="modal" title="Send SMS" data-user-id="{{$user->id}}">
                                                Send SMS
                                            </a> --}}
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('vendors/' . $user->id . '/edit') }}" data-toggle="tooltip" title="Edit">
                                                {{--@lang('usersmanagement.buttons.edit')--}}Edit
                                            </a>
                                            {{--<br/>--}}
                                            {{--@if($user->featured == 0)--}}
                                                {{--<a class="btn btn-success" href="{{ URL::to('users/' . $user->id . '/featured') }}">Make Featured</a>--}}
                                            {{--@else--}}
                                                {{--<a class="btn btn-primary" href="{{ URL::to('users/' . $user->id . '/remove-featured') }}">Remove Featured</a>--}}
                                            {{--@endif--}}
                                        </td>
                                        <td>
											@if($user->banned == "1")
                                                <a class="btn btn-sm btn-primary btn-block" href="{{ URL::to('vendors/' . $user->id . '/unban') }}" data-toggle="tooltip" title="Ban User">Unban</a>
											@else
                                                <a class="btn btn-sm btn-warning btn-block" href="{{ URL::to('vendors/' . $user->id . '/ban') }}" data-toggle="tooltip" title="Ban User">Ban User</a>
											@endif
                                        </td>
                                        <td>
                                            {!! Form::open(array('url' => 'vendors/' . $user->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                            {!! Form::hidden('_method', 'DELETE') !!}
                                            {!! Form::button(trans('usersmanagement.buttons.delete'), array('class' => 'btn btn-danger btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this user ?')) !!}
                                            {!! Form::close() !!}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @if(config('usersmanagement.enablePagination'))
                                {{ $users->links() }}
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modals.modal-delete')
    @include('modals.modal-show-membership-history')
    @include('modals.modal-edit-membership-plan')
    @include('modals.modal-send-sms')
    @include('modals.modal-send-email')
    @csrf
@endsection

@section('js')
    @if ((count($users) > config('usersmanagement.datatablesJsStartCount')) && config('usersmanagement.enabledDatatablesJs'))
        {{--@include('scripts.datatables')--}}
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('usersmanagement.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    @if(config('usersmanagement.enableSearchUsers'))
        @include('scripts.search-users')
    @endif
    @include('scripts.paid-members')
    @include('scripts.send-sms')
    @include('scripts.send-email')
@endsection
