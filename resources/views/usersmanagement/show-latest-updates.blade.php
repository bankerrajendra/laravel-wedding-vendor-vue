@extends('adminlte::page')
@section('title')
    Showing Latest Updates
@endsection
@section('content_header')
    <h1>Latest Updates</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
        .trf {
            transform: rotate(61deg);
            -ms-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            /* -webkit-transform: rotate(90deg); */
            -o-transform: rotate(90deg);
            margin-top: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header">
                    </div>

                    <div class="box-body">

                        <div class="table-responsive users-table">

                            <table class="table table-striped table-sm data-table">

                                <caption id="user_count">
                                    <button class="btn btn-default btn-sm check_all" ><i style="clor:#658898" class="fa fa-circle"></i></button>
									{{ $latest_updates->total() }} Total Records
                                </caption>

                                <thead class="thead">
                                <tr>
                                    <th>@lang('usersmanagement.users-table.id')</th>
                                    <th>Image</th>
                                    <th>@lang('usersmanagement.users-table.name')</th>
                                    <th class="hidden-xs">@lang('usersmanagement.users-table.email')</th>
                                    <th>Field</th>
                                    <th>Current Status</th>
                                    <th>Feedback</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody id="users_table">

                                @foreach($latest_updates as $update)
                                    <tr>
                                        <td>{{--<input type="checkbox" id="{{ $update->id }}" class="checkable">--}} {{$pageNo++}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('users/' . $update->user_id . '/edit') }}"  title="Edit" style="background: none; border: none">
                                                <img src="{{$update->user()->getVendorProfilePic()}}" width="100">
                                            </a>
                                        </td>
                                        <td>{{$update->first_name.' '.$update->last_name}}</td>
                                        <td class="hidden-xs">
                                            <a href="mailto:{{ $update->email }}" title="email {{ $update->email }}">{{ $update->email }}</a>
                                            <br /><br />
                                            <a class="btn btn-info send-email" href="javascript:void(0);" data-toggle="modal" title="Send Email" data-user-id="{{$update->user_id}}">
                                                Send Email
                                            </a>
                                        </td>

                                        <td><?php echo ucwords(str_replace("_", " ", $update->property_name)); ?></td>
                                        <td>
                                            @if($update->status == 1)
                                                User input required!
                                            @endif
                                        </td>
                                        <td>{{$update->feedback}}</td>
                                        <td>{{date("h:i a, F d Y ",strtotime($update->created_at))}}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @if(config('usersmanagement.enablePagination'))
                                {{ $latest_updates->links() }}
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modals.modal-send-sms')
    @include('modals.modal-send-email')
    @csrf
@endsection

@section('js')
    @include('scripts.send-sms')
    @include('scripts.send-email')
@endsection
