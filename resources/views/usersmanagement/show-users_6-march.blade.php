{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title')
    @lang('usersmanagement.showing-all-users')
@endsection
@section('content_header')
    <h1>Users</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;	
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
		.trf {
			transform: rotate(61deg);
			-ms-transform: rotate(90deg);
			-moz-transform: rotate(90deg);
			/* -webkit-transform: rotate(90deg); */
			-o-transform: rotate(90deg);
			margin-top: 20px;
		}
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">
							

                            @include('partials.admin-user-navigation')

                            <span id="card_title">
                                @lang('usersmanagement.showing-all-users')
                            </span>

                            <div class="btn-group pull-right btn-group-xs">
							
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                    <span class="sr-only">
                                        @lang('usersmanagement.users-menu-alt')
                                    </span>
                                </button>
								
                                <div class="dropdown-menu dropdown-menu-right">
								
                                    {{--<a class="dropdown-item" href="/users/create">--}}
                                    {{--<i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>--}}
                                    {{--@lang('usersmanagement.buttons.create-new')--}}
                                    {{--</a>--}}
                                    <a class="dropdown-item" href="/users/deleted">
                                        <i class="fa fa-fw fa-group" aria-hidden="true"></i>
                                        @lang('usersmanagement.show-deleted-users')
                                    </a>
                                </div>
								
                            </div>
                        </div>
                    </div>

                    <div class="box-body">

                        @if(config('usersmanagement.enableSearchUsers'))
                            @include('partials.search-users-form')
                        @endif
						
						<?php /** @Action Button on selected users profile **/ ?>						
						<span id="batch_label">
							<button class="btn btn-success btn-sm init_batch" id="batch_grant_access"><i class="fa fa-check-circle"></i> Grant Access</button>
							<button class="btn btn-primary btn-sm init_batch" id="batch_revoke_access"><i class="fa fa-undo"></i> Revoke Access</button>
							<button class="btn btn-primary btn-sm init_batch" id="batch_ban"><i class="fa fa-ban"></i> Ban User</button>
							<button class="btn btn-danger  btn-sm init_batch" id="batch_remove"><i class="fa fa-close"></i> Remove User</button>
							<button class="btn btn-primary btn-sm init_batch" id="batch_deactivate"><i class="fa fa-power-off"></i>  Deactivate</button>
							<button class="btn btn-primary btn-sm init_batch" id="batch_activate"><i class="fa fa-star"></i>  Activate Profile</button>
						</span>						
						
                        <div class="table-responsive users-table">
							
                            <table class="table table-striped table-sm data-table">
								
                                <caption id="user_count">									
                                    <button class="btn btn-default btn-sm check_all" ><i style="clor:#658898" class="fa fa-circle"></i></button> {{ trans_choice('usersmanagement.users-table.caption', 1, ['userscount' => $users->count()]) }}
                                </caption>
								
                                <thead class="thead">
							 
                                <tr>
                                    <th>@lang('usersmanagement.users-table.id')</th>
                                    <th>Image</th>
                                    <th>@lang('usersmanagement.users-table.name')</th>
                                    <th class="hidden-xs">@lang('usersmanagement.users-table.email')</th>
                                    {{--<th class="hidden-xs">@lang('usersmanagement.users-table.fname')</th>--}}
                                    {{--<th class="hidden-xs">@lang('usersmanagement.users-table.lname')</th>--}}
                                    {{--<th>@lang('usersmanagement.users-table.role')</th>--}}
                                    <th>Gender</th>
									<th>Current Status</th>
                                    <th>Country</th>
                                   
                                    <th class="hidden-sm hidden-xs hidden-md">@lang('usersmanagement.users-table.created')</th>
                                    <th class="hidden-sm hidden-xs hidden-md">@lang('usersmanagement.users-table.updated')</th>
                                    <th>@lang('usersmanagement.users-table.actions')</th>


                                    <th class="no-search no-sort"></th>
                                    <th class="no-search no-sort"></th>
                                </tr>
                                </thead>
                                <tbody id="users_table">

                                @foreach($users as $user)
                                    <?php
                                        $userrole='admin';
                                        foreach ($user->roles as $product) {
                                           $userrole=$product->slug;
                                        }
                                    ?>
                                    @if($userrole!='admin')

                                    <tr>
									@if(getFlaggedUserData($user->id, "first_name") != "" || getFlaggedUserData($user->id, "last_name") != "" || getFlaggedUserData($user->id, "mobile_number") != "" || getFlaggedUserData($user->id, "about") != "")
										<td><div class="trf" style="color:red"><i class="fa fa-warning"></i> Pending</div> <input type="checkbox" id="{{ $user->id }}" class="checkable"> {{$pageNo++}}</td>
									@else
                                        <td><input type="checkbox" id="{{ $user->id }}" class="checkable"> {{$pageNo++}}</td>
									@endif
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('users/' . $user->id . '/edit') }}"  title="Edit" style="background: none; border: none">
                                                <img src="{{$user->getVendorProfilePic()}}" width="100">
                                            </a>
                                        </td>
                                        <td>{{$user->name}}</td>
                                        <td class="hidden-xs"><a href="mailto:{{ $user->email }}" title="email {{ $user->email }}">{{ $user->email }}</a></td>
                                        {{--<td class="hidden-xs">{{$user->first_name}}</td>--}}
                                        {{--<td class="hidden-xs">{{$user->last_name}}</td>--}}
                                        {{--<td>--}}
                                        {{--@foreach ($user->roles as $user_role)--}}
                                        {{--@if ($user_role->name == 'User')--}}
                                        {{--@php $badgeClass = 'primary' @endphp--}}
                                        {{--@elseif ($user_role->name == 'Admin')--}}
                                        {{--@php $badgeClass = 'warning' @endphp--}}
                                        {{--@elseif ($user_role->name == 'Unverified')--}}
                                        {{--@php $badgeClass = 'danger' @endphp--}}
                                        {{--@else--}}
                                        {{--@php $badgeClass = 'default' @endphp--}}
                                        {{--@endif--}}
                                        {{--<span class="badge badge-{{$badgeClass}}">{{ $user_role->name }}</span>--}}
                                        {{--@endforeach--}}
                                        {{--</td>--}}
                                        <td>{{($user->gender=='M')?'Male':'Female'}}</td>
										<td>
											@if($user->banned == "1")
												<button class='btn btn-sm btn-warning'><i class="fa fa-ban"></i> Banned </button>
											@elseif($user->deactivated == "1")
												<button class='btn btn-sm btn-danger'><i class="fa fa-power-off"></i> Deactivated </button>
											@elseif($user->access_grant == "N")
												<button class='btn btn-sm btn-info'><i class="fa fa-thumbs-down"></i> Access Revoked </button>
											@elseif($user->deleted_at !== null)
												<button class='btn btn-danger btn-info'><i class="fa fa-remove"></i> Deleted </button>
											@else
												<button class='btn btn-sm btn-success'><i class="fa fa-check"></i> Active </button>
											@endif
										</td>
                                        <td>@if($user->country){{$user->country->name}}@endif</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{$user->created_at}}</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{$user->updated_at}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('users/' . $user->id . '/edit') }}" data-toggle="tooltip" title="Edit">
                                                {{--@lang('usersmanagement.buttons.edit')--}}Edit
                                            </a>
                                        <br/>
                                            @if($user->featured == 0)
                                                <a class="btn btn-success" href="{{ URL::to('users/' . $user->id . '/featured') }}">Make Featured</a>
                                            @else
                                                <a class="btn btn-primary" href="{{ URL::to('users/' . $user->id . '/remove-featured') }}">Remove Featured</a>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-warning btn-block" href="{{ URL::to('users/' . $user->id . '/ban') }}" data-toggle="tooltip" title="Ban User">Ban User
                                            </a>
                                        </td>
                                        <td>
                                            {!! Form::open(array('url' => 'users/' . $user->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                            {!! Form::hidden('_method', 'DELETE') !!}
                                            {!! Form::button(trans('usersmanagement.buttons.delete'), array('class' => 'btn btn-danger btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this user ?')) !!}
                                            {!! Form::close() !!}
                                        </td>
                                        <td>
                                            {{--<a class="btn btn-sm btn-success btn-block" href="{{ URL::to('users/' . $user->id) }}" data-toggle="tooltip" title="Show">--}}
                                            {{--@lang('usersmanagement.buttons.show')--}}
                                            {{--</a>--}}
                                        </td>

                                    </tr>
                                    <tr>
                                        <td><button class="btn btn-default btn-sm show_more_info" id="{{$user->id}}" >More info <i class="fa fa-arrow-down"></i></button></td>
                                    </tr>

                                    <tr style="display:none" class="rows show_more_info_tr_{{$user->id}}">

                                        <td colspan="10">

                                            <div class="row" style="width:100%;padding:3px;border:4px solid #d2d6de">
                                                <div class="col-sm-2" style="width:155px"><small><label>About : </label></small><br>

                                                    @if($user->users_information['about'] !== null)
														@if(strlen($user->users_information['about']) > 16)
															{!! substr($user->users_information['about'],0,16) !!}...
														@else
															{{$user->users_information['about']}}
														@endif
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <div class="col-sm-2"><small><label>Marital status : </label></small><br>
                                                    @if($user->users_information['marital_status'] !== null)
                                                        {{$user->users_information['marital_status']}}
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <div class="col-sm-2"><small><label>Height : </label></small><br>
                                                    @if($user->users_information['height'] !== null)
                                                        {{$user->users_information['height']}} (cm)
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <div class="col-sm-2"><small><label>Mobile number : </label></small><br>
                                                    @if($user->users_information['mobile_number'] !== null)
                                                        {{$user->users_information['mobile_number']}}
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <div class="col-sm-2"><small><label>Does pray : </label></small><br>
                                                    @if($user->users_information['does_pray'] !== null)
                                                        {{ucfirst($user->users_information['does_pray'])}}
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <div class="col-sm-2"><small><label>Born status : </label></small><br>
                                                    @if($user->users_information['is_born'] !== null)
                                                        {{ucfirst($user->users_information['is_born'])}}
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <br> <hr style="width:100%;border:1px solid #d2d6de">

                                                <div class="col-sm-2" style="width:155px"><small><label>Looking for : </label></small><br>
                                                    @if($user->users_information['looking_for'] !== null)
														@if(strlen($user->users_information['looking_for']) > 16)
															{!! substr($user->users_information['looking_for'],0,16) !!}...
														@else
															{{$user->users_information['looking_for']}}
														@endif
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>


                                                <div class="col-sm-2"><small><label>Religion Flag : </label></small><br>
                                                    @if($user->users_information['isReligionFlagged'] !== null)
                                                        {{$user->users_information['isReligionFlagged']}}
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <div class="col-sm-2"><small><label>Sect : </label></small><br>
                                                    @if($user->users_information['sect_id'] !== null)
                                                        {{getSectById($user->users_information['sect_id'])->name}}
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <div class="col-sm-2"><small><label>Subcast : </label></small><br>
                                                    @if($user->users_information['sub_cast_id'] !== null)
                                                        {{getSubcasteById($user->users_information['sub_cast_id'])->name}}
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <div class="col-sm-2"><small><label>Food : </label></small><br>
                                                    @if($user->users_information['food'] !== null)
                                                        {{$user->users_information['food']}}
                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

                                                <div class="col-sm-2"><small><label>Smoke : </label></small><br>
                                                    @if($user->users_information['smoke'] !== null)

                                                        @switch($user->users_information['smoke'])
                                                            @case('Y')
                                                            {{ 'Yes' }}
                                                            @break

                                                            @case('N')
                                                            {{ 'No' }}
                                                            @break

                                                            @default
                                                            {{ 'Sometimes' }}
                                                        @endswitch

                                                    @else
                                                        <small style="color:silver">Not-Specified</small>
                                                    @endif
                                                </div>

												<div class="col-sm-12" style="padding: 10px;border-right: 2px solid red;border-left: 2px solid red;width: 89%;font-family: sans-serif;">

													<small><label>Pendings Approvals</label></small><br>
													@if(getFlaggedUserData($user->id, "first_name") != "")
														<span class="btn btn-warning btn-xs">First Name</span>
													@endif

													@if(getFlaggedUserData($user->id, "last_name") != "")
														<small class="fa fa-plus" style="color:brown;font-size:10px;"></small>
														<span class="btn btn-warning btn-xs">Last Name</span>
													@endif

													@if(getFlaggedUserData($user->id, "mobile_number") != "")
														<small class="fa fa-plus" style="color:brown;font-size:10px;"></small>
														<span class="btn btn-warning btn-xs">Mobile Number</span>
													@endif

													@if(getFlaggedUserData($user->id, "about") != "")
														<small class="fa fa-plus" style="color:brown;font-size:10px;"></small>
														<span class="btn btn-warning btn-xs">About</span>
													@endif

													@if(getFlaggedUserData($user->id, "looking_for") != "")
														<small class="fa fa-plus" style="color:brown;font-size:10px;"></small>
														<span class="btn btn-warning btn-xs">Looking For</span>
													@endif

                                                </div>

                                            </div>

                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                                <tbody id="search_results"></tbody>
                                @if(config('usersmanagement.enableSearchUsers'))
                                    <tbody id="search_results"></tbody>
                                @endif

                            </table>

                            @if(config('usersmanagement.enablePagination'))
                                {{ $users->links() }}
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('modals.modal-delete')

@endsection

@section('js')
    @if ((count($users) > config('usersmanagement.datatablesJsStartCount')) && config('usersmanagement.enabledDatatablesJs'))
        {{--@include('scripts.datatables')--}}
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('usersmanagement.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    @if(config('usersmanagement.enableSearchUsers'))
        @include('scripts.search-users')
    @endif
@endsection

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
    $(document).ready(function(){
        $(".show_more_info").on("click", function(e){
            e.preventDefault();
            var id = $(this).attr("id");

            //$(this).parent().find(".show_more_info_tr").show();
            $(".show_more_info_tr_"+id).slideToggle(function(){
                $(".rows").not(".show_more_info_tr_"+id).hide();
                //$(this).css({"background-color":"red"});
            });
        })

		$(".check_all").on("click", function(){
			var elm = $(this);
			elm.find("i").toggleClass("fa fa-circle fa fa-check");
			if(elm.attr("action") !== "done"){
				elm.attr("action", "done");
				$(".checkable").attr("checked","checked");
			}else{
				$(".checkable").removeAttr("checked");
				elm.removeAttr("action");
			}
		})

		$("body").delegate(".init_batch","click", function(e){
			e.preventDefault();
			var elm = $(this);
			var checked_profile = [];
			var _action = elm.attr("id");
			$("#tempMsg").remove();

			if(_action === "batch_feature"){
				elm.parent().after("<span id='tempMsg'><strong>Error : </strong>Batch Feature Disabled</span>");
				return;
			}

			$(".checkable:checked").each(function(){
				var check = $(this);
					checked_profile.push(check.attr("id"));
			})

			/** Global ajax for all batch operations **/
			$.ajax({
				url: '{{ route("ajax.batch_ops") }}',
				method: "POST",
				data : "action="+_action+"&id="+checked_profile+"&_token="+$('input[name="_token"]').val(),
				success: function(result){
					$("#batch_label").html("<div class='alert alert-success'>Batch job completed successfully!</div>");
					setTimeout(function(){
						window.location.reload();
					},200)
				}

			})
		})
    })

</script>