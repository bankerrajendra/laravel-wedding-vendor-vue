{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title')
    Banned Users
@endsection
@section('content_header')
    <h1>Banned Users</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            @include('partials.admin-user-navigation')

							 <span id="card_title">
                               Banned Users
                            </span>

                            {{--<div class="btn-group pull-right btn-group-xs">--}}
                                {{--<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                    {{--<i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>--}}
                                    {{--<span class="sr-only">--}}
                                        {{--@lang('usersmanagement.users-menu-alt')--}}
                                    {{--</span>--}}
                                {{--</button>--}}
                                {{--<div class="dropdown-menu dropdown-menu-right">--}}
                                    {{--<a class="dropdown-item" href="/users/create">--}}
                                    {{--<i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>--}}
                                    {{--@lang('usersmanagement.buttons.create-new')--}}
                                    {{--</a>--}}
                                    {{--<a class="dropdown-item" href="/users/deleted">--}}
                                        {{--<i class="fa fa-fw fa-group" aria-hidden="true"></i>--}}
                                        {{--@lang('usersmanagement.show-deleted-users')--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>

                    <div class="box-body">
                        @if(count($users) === 0)

                            <tr>
                                <p class="text-center margin-half">
                                    No Records Found
                                </p>
                            </tr>

                        @else

                        {{--@if(config('usersmanagement.enableSearchUsers'))--}}
                            {{--@include('partials.search-users-form')--}}
                        {{--@endif--}}
						
						                        @include('partials.batch-action')

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table">
                                <caption id="user_count">
								 <button class="btn btn-default btn-sm check_all" ><i style="clor:#658898" class="fa fa-circle"></i></button>
                                    {{ trans_choice('usersmanagement.users-table.caption', 1, ['userscount' => $users->count()]) }}
                                </caption>
                                <thead class="thead">
                                <tr>
                                    <th>@lang('usersmanagement.users-table.id')</th>
                                    <th>Image</th>
                                    <th>@lang('usersmanagement.users-table.name')</th>
                                    <th class="hidden-xs">@lang('usersmanagement.users-table.email')</th>
                                    {{--<th class="hidden-xs">@lang('usersmanagement.users-table.fname')</th>--}}
                                    {{--<th class="hidden-xs">@lang('usersmanagement.users-table.lname')</th>--}}
                                    {{--<th>@lang('usersmanagement.users-table.role')</th>--}}
                                    <th>Gender</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th class="hidden-sm hidden-xs hidden-md">@lang('usersmanagement.users-table.created')</th>
                                    <th class="hidden-sm hidden-xs hidden-md">@lang('usersmanagement.users-table.updated')</th>
                                    <th>@lang('usersmanagement.users-table.actions')</th>
                                    <th class="no-search no-sort"></th>
                                    <th class="no-search no-sort"></th>
                                </tr>
                                </thead>
                                <tbody id="users_table">
                                @foreach($users as $user)
                                    <tr>
                                        <td><input type="checkbox" id="{{ $user->id }}" class="checkable"> {{$pageNo++}}</td>
                                        <td><img src="{{$user->getVendorProfilePic()}}" width="100"></td>
                                        <td>{{$user->name}}</td>
                                        <td class="hidden-xs"><a href="mailto:{{ $user->email }}" title="email {{ $user->email }}">{{ $user->email }}</a></td>
                                        {{--<td class="hidden-xs">{{$user->first_name}}</td>--}}
                                        {{--<td class="hidden-xs">{{$user->last_name}}</td>--}}
                                        {{--<td>--}}
                                        {{--@foreach ($user->roles as $user_role)--}}
                                        {{--@if ($user_role->name == 'User')--}}
                                        {{--@php $badgeClass = 'primary' @endphp--}}
                                        {{--@elseif ($user_role->name == 'Admin')--}}
                                        {{--@php $badgeClass = 'warning' @endphp--}}
                                        {{--@elseif ($user_role->name == 'Unverified')--}}
                                        {{--@php $badgeClass = 'danger' @endphp--}}
                                        {{--@else--}}
                                        {{--@php $badgeClass = 'default' @endphp--}}
                                        {{--@endif--}}
                                        {{--<span class="badge badge-{{$badgeClass}}">{{ $user_role->name }}</span>--}}
                                        {{--@endforeach--}}
                                        {{--</td>--}}
                                        <td>{{($user->gender=='M')?'Male':'Female'}}</td>
                                        <td>@if($user->country){{$user->country->name}}@endif</td>
                                        <td>@if($user->city){{$user->city->name}}@endif</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{date('h:i A, d F Y', strtotime($user->created_at->setTimezone(Auth::user()->getUserTimeZone())))}}</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{date('h:i A, d F Y', strtotime($user->updated_at->setTimezone(Auth::user()->getUserTimeZone())))}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('users/' . $user->id . '/edit') }}" data-toggle="tooltip" title="Edit">
                                                {{--@lang('usersmanagement.buttons.edit')--}}Edit
                                            </a>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-warning btn-block" href="{{ URL::to('users/' . $user->id . '/unban') }}" data-toggle="tooltip" title="UnBan User">UnBan User
                                            </a>
                                        </td>
                                        <td>
                                            {!! Form::open(array('url' => 'users/' . $user->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                            {!! Form::hidden('_method', 'DELETE') !!}
                                            {!! Form::button(trans('usersmanagement.buttons.delete'), array('class' => 'btn btn-danger btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this user ?')) !!}
                                            {!! Form::close() !!}
                                        </td>
                                        <td>
                                            {{--<a class="btn btn-sm btn-success btn-block" href="{{ URL::to('users/' . $user->id) }}" data-toggle="tooltip" title="Show">--}}
                                            {{--@lang('usersmanagement.buttons.show')--}}
                                            {{--</a>--}}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                                <tbody id="search_results"></tbody>
                                @if(config('usersmanagement.enableSearchUsers'))
                                    <tbody id="search_results"></tbody>
                                @endif

                            </table>

                            @if(config('usersmanagement.enablePagination'))
                                {{ $users->links() }}
                            @endif
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('js')
    @if ((count($users) > config('usersmanagement.datatablesJsStartCount')) && config('usersmanagement.enabledDatatablesJs'))
        {{--@include('scripts.datatables')--}}
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('usersmanagement.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    @if(config('usersmanagement.enableSearchUsers'))
        @include('scripts.search-users')
    @endif
@endsection
