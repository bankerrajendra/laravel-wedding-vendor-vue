@extends('adminlte::page') 
@section('title') @lang('usersmanagement.editing-user', ['name' => @$user->name]) @endsection 
@section('css')
<link href="{{asset('css/bootstrap-datepicker')}}.css" rel="stylesheet" type="text/css">
<style type="text/css">
    .btn-save,
    .pw-change-container {
        display: none;
    }
.checkbox label, .radio label {
    margin-right: 16px;
}
</style>
@endsection 
@section('content')
<div class="">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">&nbsp;</div>
                <div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a data-toggle="tab" href="#tab-basic-info" aria-expanded="true"> Basic Information</a></li>
                            <li class="text-uppercase"><a data-toggle="tab" href="#tab-profile-photo" aria-expanded="false"> Image</a></li>
                            <li class="text-uppercase"><a data-toggle="tab" href="#tab-track-detail" aria-expanded="false"> Tracking Details</a></li>
                        </ul>
						
						
						@if (session('success'))
							<div class="alert alert-success">
								{{ session('success') }}
							</div>
                        @endif
                        
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <?php
                            $userFlagData = getFlaggedUserData($user->id);

                            $arr=array();
                            foreach ($userFlagData as $flagData) {
                                $arr[$flagData->property_name] = $flagData;
                            }

                        ?>

                        <form method="POST" action="{{ route('admin-update-user') }}" id="registrationForm">
                        @csrf
                            <div class="tab-content">
                                <div id="tab-basic-info" class="tab-pane active">
                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Personal Information</h2>
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div>
                                                    <label>First Name</label>
                                                    <input class="form-control" type="text" name="first_name" value="{{ @$user->first_name }}" placeholder="First Name">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div>
                                                    <label>Last Name</label>
                                                    <input class="form-control" type="text" name="last_name" value="{{ @$user->last_name }}" placeholder="Last Name">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Email Address</label>
                                                <input class="form-control" type="text" name="email" value="{{ @$user->email }}" placeholder="Email Address">
                                            </div>
                                        </div>
                                        
                                        <!-- Change password -->
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Password</label>
                                                <input class="form-control" type="password" name="login_password" value="" placeholder="Password">
                                                <br /><small>Password must be minimum 6 characters.</small>
                                            </div>
                                        </div>
                                        <!-- Change password -->

                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Additional Information</h2>
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Event Date</label>
                                                @php
                                                    if(@$user->users_information['event_date'] != '') {
                                                        $expld_date = explode('-', $user->users_information['event_date']);
                                                        $event_date = $expld_date[2].'.'.$expld_date[1].'.'.$expld_date[0];
                                                    } else {
                                                        $event_date = '';
                                                    }    
                                                @endphp
                                                <input type="text" name="event_date" id="event_date" class="form-control" value="{{$event_date}}" />
                                            </div>
                                            <div class="col-md-6">
                                                <label>Budget</label>
                                                <select name="budget" id="budget" class="form-control">
                                                    <option value="">Unknown</option>
                                                    @foreach (config('constants.user_budget') as $budget_key => $budget_val)
                                                        <option @if($user->users_information['budget'] == $budget_key) selected="selected" @endif value="{{$budget_key}}">{{$budget_val}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Categories</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            @php
                                                if(@$user->users_information['vendor_categories'] != '') {
                                                    $vendor_categories = unserialize($user->users_information['vendor_categories']);
                                                } else {
                                                    $vendor_categories = [];
                                                }  
                                            @endphp
                                            @if($categories->count() > 0)
                                                @foreach ($categories as $category)
                                                    <div class="col-md-3">
                                                        <label class="checkbox-inline">
                                                            <input name="vendor_categories[]" type="checkbox" value="{{$category->id}}" @if(in_array($category->id, $vendor_categories)) checked="checked" @endif><span class="checkmark"></span> {{$category->name}}
                                                        </label>
                                                    </div>        
                                                @endforeach
                                            @endif
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-success">Grant Access</h2>
                                            </div>
                                        </div>

                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Grant/Revoke Access</label>
                                                <select class="form-control" name="grant_access">
                                                    <option disabled="">-- Please select an option --</option>
                                                        <option value="N" <?php if(@$user->access_grant == null ||  @$user->access_grant == "N"){ echo 'selected'; } ?>>Revoke Access</option>
                                                    <option value="Y"<?php if(@$user->access_grant == "Y"){ echo 'selected'; } ?>>Grant Access</option>

                                                </select>
                                                <small>
                                                <strong style="color: blue;">( Current Status : {{ ( $user->access_grant == "Y") ? 'Approved/Access Granted' : 'Disapproved/Access Revoked' }} )</strong>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                <div id="tab-profile-photo" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Profile Photo</h2>
                                            </div>
                                        </div>
                                        
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6" id="image-section">
                                                @if($userimage->count() > 0)
                                                    <img src="{{ config('constants.S3_WEDDING_IMAGES').$userimage[0]->image_thumb}}" class="img-thumbnail" />
                                                    <ul class="list-unstyled list-inline margin-top10">
                                                        <li>
                                                            <button type="button" class="btn btn-danger btn-xs deleteImage"  data-id="{{$userimage[0]->id}}" >Delete</button>
                                                        </li>
                                                    </ul>
                                                @else
                                                    {{--<img src="{{ asset('img/dummy.png') }}" class="img-thumbnail" >--}}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-track-detail" class="tab-pane">
                                    <div class="panel-body">
                                        <style>
                                            #map {
                                                height: 100%;
                                            }
                                            
                                            html,
                                            body {
                                                height: 100%;
                                                margin: 0;
                                                padding: 0;
                                            }
                                        </style>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Tracking Details</h2>
                                            </div>
                                        </div>

                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div>
                                                    <label>IP Address : <span class="text-danger">{{ $user->signup_ip_address }}</span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div>
                                                    @php
                                                        $ip_address = $user->signup_ip_address;
                                                        $county_info = unserialize(file_get_contents('http://pro.ip-api.com/php/'.$ip_address.'?key=RocDHDkeHbv0ask&fields=country'));
                                                        if(!empty($county_info)) {
                                                            $country_name = $county_info['country'];
                                                        } else {
                                                            $country_name = '';
                                                        }
                                                    @endphp
                                                    <label>Country Code : <span class="text-danger"> {{ $country_name }}</span></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-md btn-success">Save + Review</button>
                                    <a class="btn btn-md btn-default" href="{{ url('/users') }}">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modals.modal-save') @include('modals.modal-delete') @endsection 

@section('js') 
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    date.setDate(date.getDate()-1);
    $('#event_date').datepicker({format: "dd.mm.yyyy", startDate: date});
</script>
@include('scripts.delete-modal-script') @include('scripts.save-modal-script') @include('scripts.check-changed')
<script>
    var deleteImage = false;
    var imageId = 0;
    jQuery(".deleteImage").on('click', function() {
        jQuery("#confirmDelete .modal-title").html("Delete Image");
        jQuery("#confirmDelete .modal-body").html("<p>Are you sure you want to delete this Image?</p>");
        jQuery("#confirmDelete").modal('show');
        deleteImage = true;
        imageId = jQuery(this).data('id');
    });
	
    $('#confirmDelete').on('hidden.bs.modal', function() {
        deleteImage = false;
    });
	
    $('#confirmDelete .modal-footer #confirm').on('click', function() {
        if (deleteImage) {
            var _token = $('input[name="_token"]').val();
            var form = new FormData();
            form.append('_token', _token);
            form.append('id', imageId);
            jQuery.ajax({
                url: "{{route('admin-delete-image')}}",
                data: form,
                method: "post",
                processData: false,
                contentType: false
            }).done(function(response) {
                jQuery("#image-section").html('<img src="{{ asset('img/dummy.png') }}" class="img-responsive" style="width:100%;">');
                jQuery("#confirmDelete").modal('hide');
                deleteImage = false;
            });
        }
    })
</script>
@endsection