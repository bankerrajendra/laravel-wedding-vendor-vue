@extends('adminlte::page')
@section('title', 'Send Free Diamond Membership')
@section('content_header')
    <h1>Send Free Diamond Membership to Random 100 Active Users</h1>
@stop
@section('content')
<div class="row">
        <div class="col-lg-10 offset-lg-1">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if(session('success'))
                                <div class="alert alert-success" role="alert">
                                    {{session('success')}}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => array('handle-send-free-daimond-membership'), 'method' => 'post', 'role' => 'form',  'class' => 'needs-validation', 'style' => 'display:inline')) !!}
                            @if($mail_template_name != "")
                                <div class="form-group has-feedback row ">
                                    <div class="col-12 col-sm-6">&nbsp;</div>
                                </div>
                                <div class="form-group has-feedback row ">
                                    <label for="" class="col-md-3 control-label"> Email Template</label>
                                    <div class="col-md-9">
                                        <b>{{$mail_template_name}}</b> (<a href="{{ route('edit-template', [ 'type' => 'email', 'id' => $mail_id]) }}" target="_blank">edit</a>)
                                    </div>
                                </div>
                            @endif
                            <div class="form-group has-feedback row ">
								<div class="col-12 col-sm-6">&nbsp;</div>
							</div>
                            <div class="form-group has-feedback row ">
                                <label for="" class="col-md-3 control-label"> No. Free Users</label>
                                <div class="col-md-9">
                                    @if(isset($free_users) && $free_users != "")<strong>{{$free_users}}</strong>@endif
                                </div>
							</div>
							<div class="form-group has-feedback row ">
								<div class="col-12 col-sm-6">&nbsp;</div>
							</div>
                            <div class="form-group has-feedback row ">

                                <label for="package_id" class="col-md-3 control-label"> Plan</label>

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select name="package_id" id="package_id" class="form-control">
                                            <option selected="" value="">Select Plan</option>
                                            @if(count($packages) > 0)
                                                @foreach($packages as $package)
                                                <option value="{{$package->id}}">{{$package->duration}} {{$package->type}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
							<div class="form-group has-feedback row ">
								<div class="col-12 col-sm-6">&nbsp;</div>
							</div>
                            <div class="row">
								<div class="col-md-3">&nbsp;</div>
                                <div class="col-md-3 col-sm-6">
                                    {!! Form::button('Send', array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2','type' => 'submit')) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
@endsection

@section('footer_scripts')
	<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection
