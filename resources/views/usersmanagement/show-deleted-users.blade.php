{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title')
    Showing Deleted Users
@endsection
@section('content_header')
    <h1>Deleted Users</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/cssauth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
    </style>
@endsection

@section('template_title')
    Showing Deleted Users
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: .15em;
        }

    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">

                    <div class="box-body">

                        @include('partials.admin-user-navigation')
                      @if(count($users) === 0)

                            <tr>
                                <p class="text-center margin-half">
                                    No Records Found
                                </p>
                            </tr>

                        @else
                            @include('partials.batch-action')

                            <div class="table-responsive users-table">
                                <table class="table table-striped table-sm data-table">

                                    <caption id="user_count">
									 <button class="btn btn-default btn-sm check_all" ><i style="clor:#658898" class="fa fa-circle"></i></button>
                                        {{ trans_choice('usersmanagement.users-table.caption', 1, ['userscount' => count($users)]) }}
										
                                    </caption>



                                    <thead class="thead">
                                    <tr>
                                        <th>@lang('usersmanagement.users-table.id')</th>
                                        <th>Image</th>
                                        <th>@lang('usersmanagement.users-table.name')</th>
                                        <th class="hidden-xs">@lang('usersmanagement.users-table.email')</th>
                                        <th class="hidden-sm hidden-xs hidden-md">@lang('usersmanagement.users-table.created')</th>
                                        <th class="hidden-sm hidden-xs hidden-md">@lang('usersmanagement.users-table.updated')</th>
                                        <th>@lang('usersmanagement.users-table.actions')</th>
                                        <th class="no-search no-sort"></th>
                                        <th class="no-search no-sort"></th>
                                    </tr>
                                    </thead>
                                    <tbody id="users_table">
                                    {{$i=1}}
                                    @foreach($users as $user)
                                        <tr>
                                            <td> <input type="checkbox" id="{{ $user->id }}" class="checkable"> {{$i++}} </td>
                                            <td><img src="{{$user->getVendorProfilePic()}}" width="100"></td>
                                            <td>{{$user->name}}</td>
                                            <td class="hidden-xs"><a href="mailto:{{ $user->email }}" title="email {{ $user->email }}">{{ $user->email }}</a></td>
                                            <td class="hidden-sm hidden-xs hidden-md">{{date('h:i A, d F Y', strtotime($user->created_at->setTimezone(Auth::user()->getUserTimeZone())))}}</td>
                                            <td class="hidden-sm hidden-xs hidden-md">{{date('h:i A, d F Y', strtotime($user->updated_at->setTimezone(Auth::user()->getUserTimeZone())))}}</td>
                                            <td>
                                                {!! Form::model($user, array('action' => array('SoftDeletesController@update', $user->id), 'method' => 'PUT', 'data-toggle' => 'tooltip')) !!}
                                                {!! Form::button('<i class="fa fa-refresh" aria-hidden="true"></i>', array('class' => 'btn btn-success btn-block btn-sm', 'type' => 'submit', 'data-toggle' => 'tooltip', 'title' => 'Restore User')) !!}
                                                {!! Form::close() !!}
                                            </td>
                                            <td>
                                                {!! Form::model($user, array('action' => array('SoftDeletesController@destroy', $user->id), 'method' => 'DELETE', 'class' => 'inline', 'data-toggle' => 'tooltip', 'title' => 'Destroy User Record')) !!}
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                                {!! Form::button('<i class="fa fa-user-times" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm inline','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this user ? This will delete all data related to this user.')) !!}
                                                {!! Form::close() !!}
                                            </td>
                                            <td>
                                                {{--<a class="btn btn-sm btn-success btn-block" href="{{ URL::to('users/' . $user->id) }}" data-toggle="tooltip" title="Show">--}}
                                                {{--@lang('usersmanagement.buttons.show')--}}
                                                {{--</a>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modals.modal-delete')
@endsection

@section('js')

    @if (count($users) > 10)
        @include('scripts.datatables')
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @include('scripts.tooltips')

@endsection
