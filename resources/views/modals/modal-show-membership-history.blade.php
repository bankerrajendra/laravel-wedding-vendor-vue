<div class="modal fade " id="membershipHistoryModal" role="dialog" aria-labelledby="confirmSaveLabel" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">
                    Payment History for <span class="user-name"></span>
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Plan</th>
                            <th>Date</th>
                            <th>Price</th>
                            <th>Method</th>
                        </tr>
                    </thead>
                    <tbody id="table-records">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
