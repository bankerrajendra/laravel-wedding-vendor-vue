<div class="modal fade " id="sendSMS" role="dialog" aria-labelledby="confirmSaveLabel" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">
                    Send SMS
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="plan_detail">
                    <div class="alert alert-success" id="model_body_common_sms" style="display:none"></div>
                    <div class="alert alert-danger" id="model_body_common_sms_err" style="display:none"></div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group ">
                                <label class="col-sm-1 col-lg-1"></label>
                                <label class="col-sm-2 col-lg-2 control-label text-bold" style="padding-top: 0px;">User</label>
                                <div class="col-sm-6 col-lg-6">
                                    <span class="fa fa-user"></span>&nbsp;&nbsp;<span class="user-name"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group ">
                                <label class="col-sm-1 col-lg-1"></label>
                                <label class="col-sm-2 col-lg-2 control-label text-bold" style="padding-top: 0px;">Email</label>
                                <div class="col-sm-6 col-lg-6">
                                    <span class="fa fa-envelope"></span>&nbsp;&nbsp;<span class="user-email"></span>
                                    <span style="display:none;" class="user-id"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group ">
                                <label class="col-sm-1 col-lg-1"></label>
                                <label class="col-sm-2 col-lg-2 control-label text-bold" style="padding-top: 0px;">Mobile Number</label>
                                <div class="col-sm-6 col-lg-6">
                                    <span class="fa fa-phone"></span>&nbsp;&nbsp;<span class="user-mobile-number"></span>
                                    <span style="display: none;" id="user-mobile-number"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"><br></div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group ">
                            <label class="col-sm-1 col-lg-1"></label>
                            <label class="col-sm-2 col-lg-2 control-label text-bold">Message</label>
                            <div class="col-sm-6 col-lg-6">
                                <textarea rows="5" class="form-control" name="sms" id="sms" placeholder="Enter your message to Send"></textarea>
                            </div>
                            <label class="col-sm-1 col-lg-1"></label>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"><br></div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="button" id="updt-send-sms-submit-btn" class="btn btn-primary mr10" onclick="javascript:send_sms();">Send SMS</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
