<div class="modal fade " id="membershipEdiPlanModal" role="dialog" aria-labelledby="confirmSaveLabel" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">
                    Plan Assignment
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="plan_detail">
                    <div class="alert alert-success" id="model_body_common" style="display:none"></div>
                    <div class="alert alert-danger" id="model_body_common_err" style="display:none"></div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group ">
                                <label class="col-sm-1 col-lg-1"></label>
                                <label class="col-sm-2 col-lg-2 control-label text-bold" style="padding-top: 0px;">User</label>
                                <div class="col-sm-6 col-lg-6">
                                    <span class="fa fa-user"></span>&nbsp;&nbsp;<span class="user-name"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group ">
                                <label class="col-sm-1 col-lg-1"></label>
                                <label class="col-sm-2 col-lg-2 control-label text-bold" style="padding-top: 0px;">Email</label>
                                <div class="col-sm-6 col-lg-6">
                                    <span class="fa fa-envelope"></span>&nbsp;&nbsp;<span class="user-email"></span>
                                    <span style="display:none;" class="user-id"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"><br></div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group ">
                            <label class="col-sm-1 col-lg-1"></label>
                            <label class="col-sm-2 col-lg-2 control-label text-bold">Plan</label>
                            <div class="col-sm-6 col-lg-6">
                                <select name="package_id" id="package_id" class="form-control">
                                    <option selected="" value="">Select Plan</option>
                                    @if(count($packages) > 0)
                                        @foreach($packages as $package)
                                        <option value="{{$package->id}}">{{$package->duration}} {{$package->type}} ( {{config('constants.currency_code')}} {{$package->amount}} )</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <label class="col-sm-1 col-lg-1"></label>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-1 col-lg-1"></label>
                                <label class="col-sm-2 col-lg-2 control-label text-bold pr0">Payment&nbsp;Mode</label>
                                <div class="col-sm-6 col-lg-6">
                                    <select required="" name="payment_mode" id="payment_mode" class="form-control">
                                        <option selected="" value="">Select Payment Mode</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Credit Card">Credit Card</option>
                                        <option value="Debit Card">Debit Card</option>
                                        <option value="Other">Other</option>
                                        <option value="Cheque">Cheque</option>
                                    </select>
                                </div>
                                <label class="col-sm-1 col-lg-1"></label>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"><br></div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group ">
                            <label class="col-sm-1 col-lg-1"></label>
                            <label class="col-sm-2 col-lg-2 control-label text-bold">Payment&nbsp;Note</label>
                            <div class="col-sm-6 col-lg-6">
                                <textarea rows="5" class="form-control" name="payment_note" id="payment_note" placeholder="Enter Payment Note"></textarea>
                            </div>
                            <label class="col-sm-1 col-lg-1"></label>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"><br></div>
                    <div class="row form-horizontal">
                        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="button" id="updt-mem-submit-btn" class="btn btn-primary mr10" onclick="javascript:update_payment_member();">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
