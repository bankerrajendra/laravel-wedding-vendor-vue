@extends('layouts.app')
@section('template_title'){{$metafields['title']}}@endsection
@section('meta_description'){{$metafields['description']}}@endsection
@section('meta_keyword'){{$metafields['keyword']}}@endsection
@section('canonical'){{$metafields['canonical']}}@endsection
@section('template_linked_css')
<link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/owl.theme.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css">
<style>
.center {
    margin: auto;
    width: 60%;
    padding: 20px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.hideform {
    display: none;
}
#owl-demo .item{

    color: #FFF;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    text-align: center;
}
.owl-pagination{
    display:none;
}
.owl-prev {
    color: rgba(255, 255, 255, 0)!important;
    width: 15px;
    height: 53px;
    position: absolute;
    top: 40%;
    left:1%;
    display: block !important;
    border:0px solid black;
    background: url("{{asset('img/nav-icon.png')}}") no-repeat scroll 0 0!important;
}

.owl-next {
    color: rgba(255, 255, 255, 0)!important;
    width: 28px;
    height: 53px;
    position: absolute;
    top: 40%;
    right:1%;
    display: block !important;
    border:0px solid black;
    background: url("{{asset('img/nav-icon.png')}}") no-repeat scroll -24px 0px!important;
}
form[name="search-vendor-frm-home"] #locationTopList li {
    color: #000;
}
</style>
@endsection
@section('content')
@php $categoriesMn=menuVendorCategories(); @endphp
    <div class="banner-mobile hidden-lg hidden-md hidden-sm">
        <div class="container">
            <div class="row">
            </div>
        </div>
    </div>

    <div class="banner hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    {{--<h1>Let's find wedding vendors</h1>--}}
                </div>
            </div>
            <div class="row search-form hidden-xs">
                @if($categories->count() > 0)
                    @php
                        $search_vendor_route = route('search-vendor', ['category' => $categoriesMn[0]->slug])
                    @endphp
                @else
                    @php
                        $search_vendor_route = route('search-vendor')
                    @endphp
                @endif
                <form action="{{ $search_vendor_route }}" method="GET" name="search-vendor-frm-home">
                    <input type="hidden" name="location_type" value="" />
                    <input type="hidden" name="location_id" value="" />
                    <div class="col-sm-4 col-sm-offset-2 no-padding">
                        @if($categories->count() > 0)
                        <select name="category" class="form-control" style="display: none;">
                        @foreach($categoriesMn as $category)
                        <option value="{{$category->slug}}">{{$category->name}}</option>
                        @endforeach
                        </select>
                        @endif
                    </div>
                    <div class="col-sm-3 no-padding">
                        <input type="text" class="form-control" placeholder="City/State/Country" name="city" autocomplete="off">
                        <div id="locationTopList">
                        </div>
                    </div>
                    <div class="col-sm-3 no-padding">
                        <input type="submit" class="btn btn-default" value="SEARCH">
                    </div>
                </form>
            </div>
            <div class="row hidden-xs">
                <div class="col-sm-12 text-center">
                    <p>
                        {{--<a href="#">Wedding Venues</a>,--}}
                        {{--<a href="#">Wedding Photography</a>,--}}
                        {{--<a href="#">Wedding Music</a>,--}}
                        {{--<a href="#">Wedding Transportation</a>,--}}
                        {{--<a href="#">Wedding Invitations</a>,--}}
                        {{--<a href="#">Wedding Dresses</a>,--}}
                        {{--<a href="#">Wedding Flowers</a>--}}
                        <a href="#">Wedding Banquet Halls, Wedding Photography, Wedding Event Planners, Wedding Transportation, Wedding Dresses</a>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="container hidden-lg hidden-md hidden-sm">
        <div class="row search-form">
            <h1>Let's Find Wedding Vendors</h1>
            <form action="{{ route('search-vendor') }}" method="GET" name="search-vendor-frm-home-mobile">
                <div class="col-xs-10 no-padding">
                    <input type="text" class="form-control" data-toggle="modal" data-target="#myModal" placeholder="Choose a category">
                </div>
                <div class="col-xs-2 no-padding">
                    <button type="button" class="btn btn-default btn-block"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </form>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Filters</h4>
                </div>
                <div class="modal-body">
                    <ul class="listing">
                        @if($categories->count() > 0)
                            @foreach($categories as $category)
                                <li><a href="{{route('search-vendor', ['category' => $category->slug])}}">{{$category->name}}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--Footer-->
    <div class="sect">
        <div class="container">
            <div class="row" style="display: flex; flex-wrap: wrap;">
                <div class="col-sm-12">
                    <h3>Find Vendors At Every Budget</h3>
                    <p class="text-cent">
                        Punjabi Wedding offers best prices, availability and service reviews to find your perfect Indian Wedding Vendors. Plan your perfect wedding with resources for anything from banquet halls, beauty services, caterers, jewelry, photographers, transportation, makeup and videographers.  </p>
                    <br>
                </div>
                @if(count($topRandCats) > 0)
                    @foreach ($topRandCats as $topRandCat)
                        <div class="col-sm-4 col-xs-6">
                            <div class="img-text">
                                <a href="{{ route('search-vendor', ['category' => $topRandCat['slug']]) }}">
                                    <img src="{{config('constants.S3_WEDDING_IMAGES_CATEGORY').$topRandCat['image']}}" class="img-responsive">
                                    <div class="ven_head">
                                        <h5>{{$topRandCat['title']}}</h5>
                                    </div>
                                </a>
                            </div>
                        </div>   
                    @endforeach
                @endif
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 text-center">
                    <br>
                    <a href="{{ route('muslim-vendors') }}" class="btn-block read-more_s">Browse Professionals</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="featured">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3>Featured Wedding Vendors</h3>
                    <p class="text-cent">Our exclusive list of Wedding Vendors are featured in each city and category. Our free tool enables you to reach out to our featured Hair + Makeup, DJ+Lighting, Henna, Favors + Photobooth and many more vendors. We are confident that their high standards will meet all of your requirements.</p>
                    <br>
                </div>
                {{-- Featured vendors starts --}}
                @if(count($featuredVendors) > 0)
                <div id="owl-demo" class="owl-carousel owl-theme">
                    @foreach ($featuredVendors as $featuredVendor)
                        <div class="item">
                            <div class="post-slide2">
                                <div class="post-img">
                                    <a href="{{$featuredVendor['profile']}}"><img src="{{$featuredVendor['image']}}" alt=""></a>
                                </div>
                                <div class="post-content">
                                    <h3 class="post-title"><strong><a href="{{$featuredVendor['profile']}}">{{$featuredVendor['title']}}</a></strong></h3>
                                    <span>
                                    @for($r=1;$r<=5;$r++)
                                        <i class="fa @if($r <= $featuredVendor['average_rating']) fa-star @elseif($r >= $featuredVendor['average_rating']-0.5 && $r <= $featuredVendor['average_rating']+0.5) fa-star-half-o @else fa-star-o @endif" aria-hidden="true"></i>
                                    @endfor
                                    </span>
                                    @if(isset($featuredVendor['total_review']))
                                    <span>( {{$featuredVendor['total_review']}} Review(s) )</span>
                                    @endif
                                    <p class="post-description">{{$featuredVendor['description']}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif
                {{-- Featured vendors ends --}}
                <div class="col-sm-4 col-sm-offset-4 text-center">
                    <a href="{{ route('muslim-vendors') }}" class="read-more_s btn-block">View Featured Vendors</a>
                </div>
            </div>
        </div>
    </div>
    @if($totalShowcaseVids > 0)
    <div class="show-case">
        <div class="container">
            <div class="row">
                <div class="col-sm-12"><p class="text-center">

                        Punjabi Wedding have everything you need to plan your wedding. We have put together vendors showcasing their talent and services from jewelers and caterers, to photographers and venues, DJs, florists, videographers, bridal dresses, groom wears, photo booth, beauty and health services and many more products they offer.
                    </p></div>
                <div class="col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home-videos">Showcase Videos</a></li>
                        @if($eventVids->count() > 0)<li><a data-toggle="tab" href="#home-events-vids">Event Videos</a></li>@endif
                    </ul>

                    <div class="tab-content">
                        <div id="home-videos" class="tab-pane fade in active">
                            <div class="row">
                                <div id="showcase-videos" style="display: flex; flex-wrap: wrap;">
                                @include('pages.user.ajax.show-showcase-videos')
                                </div>
                                @if($totalShowcaseVids > $videoPerPage)
                                <div class="col-sm-4 col-sm-offset-4 text-center">
                                    <a href="javascript:void(0);" id="next-page-showcase-vids" data-next-page="2" class="read-more_showcase btn-block">Load More</a>
                                    <img src="{{asset('img/loader.gif')}}" id="spinner-load" width="40" style="display: none;margin-top: 10px;" />
                                </div>
                                @endif
                            </div>
                        </div>
                        <div id="home-events-vids" class="tab-pane fade in">
                            <div class="row">
                                <div id="events-videos" style="display: flex; flex-wrap: wrap;">
                                @include('pages.user.ajax.show-event-videos')
                                </div>
                                @if($eventVids->total() > $videoPerPage)
                                <div class="col-sm-4 col-sm-offset-4 text-center">
                                    <a href="javascript:void(0);" id="next-page-event-vids" data-next-page="2" class="read-more_showcase btn-block">Load More</a>
                                    <img src="{{asset('img/loader.gif')}}" id="spinner-load-event" width="40" style="display: none;margin-top: 10px;" />
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="blog hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3>Our Blog</h3>
                </div>
                @foreach($fetchBlogs as $blogRec)
                    <div class="col-sm-4 panel-body"><a href="{{route('blog-detail',['slug'=>$blogRec->slug])}}">
                        <div class="blog-text">
                            <img src="{{config('constants.upload_url_public'). config('constants.blogimage_upload_path_rel').$blogRec->blog_image}}" class="img-responsive">
                            <div class="blog-centered">
                                <p>{{$blogRec->title}}</p>
                            </div>
                        </div></a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="sect hidden-xs" style="border-top:1px solid #eeeeee;">
        {{-- <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a href="" style="margin-bottom: 16px;display: block;"><img src="{{asset('img/ad-banner.jpg')}}" class="img-responsive"></a>
                </div>
            </div>
        </div> --}}
        <div class="container">
            <div class="row hidden-xs">
                <div class="col-sm-12">
                    <h3 class="he_ad">All Wedding Vendor Categories</h3>
                </div>
                <?php
                    $c=0;
                    foreach($categories as $category) {
                        $catArr[$c]['slug'] = $category->slug;
                        $catArr[$c]['name'] = $category->name;
                        $c++;
                    }
                    $l=0;
                    $totCat=$categories->count();
                    if($totCat > 0) {
                        $noRows= ceil($totCat/4);
                        for($i=0;$i<4; $i++) { ?>
                            <div class="col-sm-3 col-xs-6">
                                <ul class="listing">
                                    <?php for($j=0;$j<$noRows;$j++)  {
                                        if($l==$c){ break;} ?>
                                        <li><a href="{{route('search-vendor', ['category' => $catArr[$l]['slug']])}}">{{$catArr[$l]['name']}}</a></li>
                                        <?php
                                        $l++;
                                    } ?>
                                </ul>
                            </div>
                        <?php
                        }
                    }
                ?>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/jquery.styled-select-box.js') }}"></script>
<script type="text/javascript">
function shortListVendor(vendorId) {
    var shortlistLnk = 'short-list-link-'+vendorId;
    $("#"+shortlistLnk).removeAttr("onclick");
    $("#"+shortlistLnk).html('<i class="fa fa-heart" aria-hidden="true" style="color:red;"></i>');
    var url = route('short-list-vendor-ajax')+'?vendor_id='+vendorId+'&shortlist=1';
    
    ajaxShortList = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {},
        success: function(result) {},
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {}
    });
}
var ajaxResultGetRequest = null
$("#next-page-showcase-vids").on('click', function() {
    var url = route('get-showcase-videos-ajax')+'?page='+$(this).data('next-page');
    ajaxResultGetRequest = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            // show loading image
            $('#next-page-showcase-vids').hide();
            $('#spinner-load').show();
        },
        success: function(result) {
            // show results
            $('#showcase-videos').append(result.videos);
            $('#next-page-showcase-vids').data('next-page', result.next_page);
            if(result.next_page == '') {
                $('#next-page-showcase-vids').hide();
            } else {
                $('#next-page-showcase-vids').show();
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            // hide loading image
            $('#spinner-load').hide();
            ajaxResultGetRequest = null;
        }
    });
});
// events vids starts //
var ajaxResultGetEventVids = null
$("#next-page-event-vids").on('click', function() {
    var url = route('get-event-videos-ajax')+'?page='+$(this).data('next-page');
    ajaxResultGetEventVids = $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            // show loading image
            $('#next-page-event-vids').hide();
            $('#spinner-load-event').show();
        },
        success: function(result) {
            // show results
            $('#events-videos').append(result.videos);
            $('#next-page-event-vids').data('next-page', result.next_page);
            if(result.next_page == '') {
                $('#next-page-event-vids').hide();
            } else {
                $('#next-page-event-vids').show();
            }
        },
        error: function(xhr, status, error){
            var errors = xhr.responseJSON;
        },
        complete: function () {
            // hide loading image
            $('#spinner-load-event').hide();
            ajaxResultGetRequest = null;
        }
    });
});
// event vids ends //
$('form[name="search-vendor-frm-home"] select[name="category"]').on('change', function() {
    $('form[name="search-vendor-frm-home"]').attr('action', "{{route('search-vendor')}}/"+this.value);
});
function SearchBySpec(type, id)
{
    if(type != '' && id != '') {
        $("input[name='location_type']").val(type);
        $("input[name='location_id']").val(id);
    }
}
$("select").styledSelect();
$("#owl-demo").owlCarousel({
    autoPlay: 3000, //Set AutoPlay to 3 seconds
    items : 3,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,3],
    navigation : true,
    navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>']
});
var topLocationRequest = null;
$("input[name='city']").on('keyup', function() {
    var value = $(this).val();
    topLocationRequest = $.ajax({
        type:"get",
        dataType: 'html',
        url: route('get-location-auto-suggestions'),
        data :{
            'key' : value               
        },
        beforeSend : function()    {           
            if(topLocationRequest != null) {
                topLocationRequest.abort();
            }
        },
        success: function(response) {
            $('#locationTopList').fadeIn();  
            $('#locationTopList').html(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // Error handling
        }
    });
});
$(document).on('click', 'ul.location-vendor-autosuggestion li', function(){
    $("input[name='city']").val($(this).text());
    $('#locationTopList').fadeOut();
});
$(document).on("focusout","input[name='city']",function(){
    $('#locationList').fadeOut();
});
</script>
@endsection
