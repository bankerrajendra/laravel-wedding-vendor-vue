{{--@extends('layouts.admin')--}}
@extends('adminlte::page')
@section('title')
    Showing All Vendors
@endsection
@section('content_header')
    <h1>Vendors</h1>
@endsection

@section('css')
    @if(config('laravelusers.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('laravelusers.datatablesCssCDN') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/vendor/adminlte/css/auth.css">
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
        #search_trigger{
            display: none;
        }
        .trf {
            transform: rotate(61deg);
            -ms-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            /* -webkit-transform: rotate(90deg); */
            -o-transform: rotate(90deg);
            margin-top: 20px;
        }
        .mrgn-top-5 {
            margin-top: 5px;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">


                            @include('partials.admin-vendor-navigation')

                            {{--<span id="card_title">--}}
                                {{--@lang('usersmanagement.showing-all-users')--}}
                            {{--</span>--}}

                            <div class="btn-group pull-right btn-group-xs">

                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                    <span class="sr-only">
                                        @lang('usersmanagement.users-menu-alt')
                                    </span>
                                </button>

                                <div class="dropdown-menu dropdown-menu-right">

                                    {{--<a class="dropdown-item" href="/users/create">--}}
                                    {{--<i class="fa fa-fw fa-user-plus" aria-hidden="true"></i>--}}
                                    {{--@lang('usersmanagement.buttons.create-new')--}}
                                    {{--</a>--}}
                                    <a class="dropdown-item" href="{{route('deleted.index')}}">
                                        <i class="fa fa-fw fa-group" aria-hidden="true"></i>
                                        @lang('usersmanagement.show-deleted-users')
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="box-body">

                        @if(config('usersmanagement.enableSearchUsers'))
                            @include('partials.search-users-form')
                        @endif

                        @include('partials.batch-action-vendor')


                        <div class="table-responsive users-table">

                            <table class="table table-striped table-sm data-table">

                                <caption id="user_count">
                                    <button class="btn btn-default btn-sm check_all" ><i style="color:#658898" class="fa fa-circle"></i></button>
									{{ trans_choice('usersmanagement.users-table.caption', 1, ['userscount' => $users->count()]) }}
                                </caption>

                                <thead class="thead">
                                <tr>
                                    <th>@lang('usersmanagement.users-table.id')</th>
                                    <th>Image / Type</th>
                                    <th>@lang('usersmanagement.users-table.name') / Company Name</th>
                                    <th class="hidden-xs">@lang('usersmanagement.users-table.email')</th>
                                    {{--<th class="hidden-xs">@lang('usersmanagement.users-table.fname')</th>--}}
                                    {{--<th class="hidden-xs">@lang('usersmanagement.users-table.lname')</th>--}}
                                    {{--<th>@lang('usersmanagement.users-table.role')</th>--}}
                                    <th>Current Status</th>
                                    <th>Country</th>

                                    <th class="hidden-sm hidden-xs hidden-md">@lang('usersmanagement.users-table.created')</th>
                                    <th class="hidden-sm hidden-xs hidden-md">@lang('usersmanagement.users-table.updated')</th>
                                    <th>@lang('usersmanagement.users-table.actions')</th>


                                    <th class="no-search no-sort"></th>
                                    <th class="no-search no-sort"></th>
                                </tr>
                                </thead>
                                <tbody id="users_table">

                                @foreach($users as $user)
                                    <?php
                                    $userFlagData = getFlaggedUserData($user->id);
                                    $arr=array();
                                    foreach ($userFlagData as $flagData) {
                                        $arr[$flagData->property_name] = $flagData;
                                    }
                                    // get user unapproved image data
                                    $image_approval = getNotApprovedImageVendorData($user->id);
                                    if($image_approval > 0) {
                                        $arr['image'] = $image_approval;
                                    }

                                    // get user unapproved videos data
                                    $videos_approval = getNotApprovedVideoVendorData($user->id);
                                    if($videos_approval > 0) {
                                        $arr['videos'] = $videos_approval;
                                    }

                                    // get user unapproved custom faqs data
                                    $custom_faqs_approval = getNotApprovedCustomFaqVendorData($user->id);
                                    if($custom_faqs_approval > 0) {
                                        $arr['custom_faqs'] = $custom_faqs_approval;
                                    }

                                    ?>

                                    <tr>
                                        @if(count($arr)>0)
                                            <td><div class="trf" style="color:red"><i class="fa fa-warning"></i> Pending</div> <input type="checkbox" id="{{ $user->id }}" class="checkable"> {{$pageNo++}}</td>
                                        @else
                                            <td><input type="checkbox" id="{{ $user->id }}" class="checkable"> {{$pageNo++}}</td>
                                        @endif
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('vendors/' . $user->id . '/edit') }}"  title="Edit" style="background: none; border: none">
                                                <img src="{{$user->getVendorProfilePic()}}" width="100">
                                            </a>
                                            {{-- <br><b>{{$user->showVendorType()->name}}</b> --}}
                                        </td>
                                            <td>{{$user->first_name.' '.$user->last_name}} / {{$user->vendors_information->company_name}}</td>
                                        <td class="hidden-xs">
                                            <a href="mailto:{{ $user->email }}" title="email {{ $user->email }}">{{ $user->email }}</a>
                                            <br /><br />
                                            <a class="btn btn-info send-email" href="javascript:void(0);" data-toggle="modal" title="Send Email" data-user-id="{{$user->id}}">
                                                Send Email
                                            </a>
                                        </td>

                                        <td>
                                            <?php $userStatus="0"; ?>
                                            @if($user->banned == "1")
                                                <button class='btn btn-sm btn-warning'><i class="fa fa-ban"></i> Banned </button>
                                            @elseif($user->deactivated == "1")
                                                <button class='btn btn-sm btn-danger'><i class="fa fa-power-off"></i> Deactivated </button>
                                            @elseif($user->access_grant == "N")
                                                <button class='btn btn-sm btn-info'><i class="fa fa-thumbs-down"></i> Access Revoked </button>
                                            @elseif($user->deleted_at !== null)
                                                <button class='btn btn-danger btn-info'><i class="fa fa-remove"></i> Deleted </button>
                                            @else
                                                    <?php $userStatus="1"; ?>
                                                <button class='btn btn-sm btn-success'><i class="fa fa-check"></i> Active </button>
                                            @endif
                                        </td>
                                        <td>@if($user->country){{$user->country->name}}@endif</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{date('h:i A, d F Y', strtotime($user->created_at->setTimezone(Auth::user()->getUserTimeZone())))}}</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{date('h:i A, d F Y', strtotime($user->updated_at->setTimezone(Auth::user()->getUserTimeZone())))}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('vendors/' . $user->id . '/edit') }}" data-toggle="tooltip" title="Edit">
                                                Edit
                                            </a>
                                            <br /><br />
                                            @if($user->getTotalEvents() > 0)<a class="btn btn-primary " href="{{route('admin-events')}}?vendor_id={{$user->id}}" title="View Events">
                                                View Events ({{$user->getTotalEvents()}})
                                            </a>@endif
                                            {{-- <a class="btn btn-primary send-sms" href="javascript:void(0);" data-toggle="modal" title="Send SMS" data-user-id="{{$user->id}}">
                                                Send SMS
                                            </a> --}}
                                        </td>
                                        <td>
											@if($user->banned == "1")
                                                <a class="btn btn-sm btn-primary btn-block" href="{{ URL::to('users/' . $user->id . '/unban') }}" data-toggle="tooltip" title="Ban User">Unban</a>
											@else
                                                <a class="btn btn-sm btn-warning btn-block" href="{{ URL::to('users/' . $user->id . '/ban') }}" data-toggle="tooltip" title="Ban User">Ban User</a>
											@endif
                                            <br>
                                            <br>
                                                @if($userStatus=="1")
                                                    @if($user->featured == 0)
                                                        <a class="btn btn-success" href="{{ URL::to('users/' . $user->id . '/featured') }}">Make Featured</a>
                                                    @else
                                                        <a class="btn btn-primary" href="{{ URL::to('users/' . $user->id . '/remove-featured') }}">Remove Featured</a>
                                                    @endif
                                                @endif

                                        </td>
                                        <td>
                                            {!! Form::open(array('url' => 'vendors/' . $user->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                            {!! Form::hidden('_method', 'DELETE') !!}
                                            {!! Form::button(trans('usersmanagement.buttons.delete'), array('class' => 'btn btn-danger btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Vendor', 'data-message' => 'Are you sure you want to delete this vendor ?')) !!}
                                            {!! Form::close() !!}
                                        </td>

                                    </tr>
                                    <tr>
                                        <td><button class="btn btn-default btn-sm show_more_info" id="{{$user->id}}" >More info <i class="fa fa-arrow-down"></i></button></td>
                                    </tr>

                                    <tr style="display:none" class="rows show_more_info_tr_{{$user->id}}">
                                        <td colspan="10">
                                            <div class="row" style="width:100%;padding:3px;border:4px solid #d2d6de">

                                                @if(count($arr)>0)
                                                    <div class="col-sm-12" style="padding: 10px;border-right: 2px solid red;border-left: 2px solid red;width: 89%;font-family: sans-serif;">

                                                        <small><label>Pendings Approvals</label></small><br>
                                                        @if(@$arr['first_name']->property_name=="first_name")
                                                            <span class="btn btn-warning btn-xs">First Name</span>
                                                        @endif

                                                        @if(@$arr['last_name']->property_name=="last_name")
                                                            <span class="btn btn-warning btn-xs">Last Name</span>
                                                        @endif

                                                        @if(@$arr['mobile_number']->property_name=="mobile_number")
                                                            <span class="btn btn-warning btn-xs">Mobile Number</span>
                                                        @endif

                                                        @if(@$arr['address']->property_name=="address")
                                                            <span class="btn btn-warning btn-xs">Address</span>
                                                        @endif

                                                        @if(@$arr['zip']->property_name=="zip")
                                                            <span class="btn btn-warning btn-xs">Zip</span>
                                                        @endif

                                                        @if(@$arr['image'] > 0)
                                                            <span class="btn btn-warning btn-xs">Image ({{@$arr['image']}})</span>
                                                        @endif

                                                        @if(@$arr['videos'] > 0)
                                                            <span class="btn btn-warning btn-xs">Videos ({{@$arr['videos']}})</span>
                                                        @endif

                                                        @if(@$arr['custom_faqs'] > 0)
                                                            <span class="btn btn-warning btn-xs">Custom FAQs ({{@$arr['custom_faqs']}})</span>
                                                        @endif

                                                        @php
                                                            $vendInforFldsArry = [
                                                                'company_name', 
                                                                'established_year', 
                                                                'meta_keywords', 
                                                                'business_information', 
                                                                'specials', 
                                                                'business_website', 
                                                                'facebook', 
                                                                'instagram', 
                                                                'pinterest', 
                                                                'twitter', 
                                                                'linkedin'
                                                            ];
                                                        @endphp

                                                        @foreach ($vendInforFldsArry as $vendInforFld)
                                                            @if(@$arr[$vendInforFld]->property_name==$vendInforFld)
                                                                <span class="btn btn-warning btn-xs">{{ucwords(str_replace('_', ' ',$vendInforFld))}}</span>
                                                            @endif    
                                                        @endforeach
                                                        

                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @if(config('usersmanagement.enablePagination'))
                                {{ $users->appends(['user_search_box' => $searchTerm])->links() }}
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modals.modal-delete')
    @include('modals.modal-send-sms')
    @include('modals.modal-send-email')
@endsection

@section('js')
    @if ((count($users) > config('usersmanagement.datatablesJsStartCount')) && config('usersmanagement.enabledDatatablesJs'))
        {{--@include('scripts.datatables')--}}
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('usersmanagement.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    @if(config('usersmanagement.enableSearchUsers'))
        @include('scripts.search-users')
    @endif
    @include('scripts.send-sms')
    @include('scripts.send-email')
@endsection
