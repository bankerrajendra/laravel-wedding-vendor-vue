 
@extends('adminlte::page') @section('title') @lang('usersmanagement.editing-user', ['name' => @$user->name]) @endsection @section('css')
<link href="{{asset('dist/css/cropper.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dist/css/main.css')}}" rel="stylesheet" type="text/css">
<style type="text/css">
.btn-save,
.pw-change-container {
    display: none;
}
.checkbox label, .radio label {
    margin-right: 16px;
}
</style>
@endsection @section('content')
<div class="">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="text-uppercase active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Basic Information</a></li>
                            {{-- <li class="text-uppercase"><a data-toggle="tab" href="#tab-2" aria-expanded="false">Additional Information</a></li> --}}
                            <li class="text-uppercase "><a data-toggle="tab" href="#tab-3" aria-expanded="false">Images</a></li>
                            <li class="text-uppercase"><a data-toggle="tab" href="#tab-4" aria-expanded="false">Videos</a></li>
                            <li class="text-uppercase"><a data-toggle="tab" href="#tab-5" aria-expanded="false">Showcase</a></li>
                            <li class="text-uppercase"><a data-toggle="tab" href="#tab-6" aria-expanded="false">FAQs</a></li>
                            @if(!empty($custom_faqs))<li class="text-uppercase"><a data-toggle="tab" href="#tab-7" aria-expanded="false">Custom FAQs</a></li>@endif
                            <li class="text-uppercase"><a data-toggle="tab" href="#tab-8" aria-expanded="false">Tracking Details</a></li>
                        </ul>
						
						@if (session('success'))
							<div class="alert alert-success">
								{{ session('success') }}
							</div>
                        @endif
                        
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <?php
                            $userFlagData = getFlaggedUserData($user->id);

                            $arr=array();
                            foreach ($userFlagData as $flagData) {
                                $arr[$flagData->property_name] = $flagData;
                            }

                        ?>

                        <form method="POST" action="{{ route('admin-update-vendor') }}" id="registrationForm">
                            <div class="tab-content">
                                {{--TAB 1: Basic Information STARTED --}}
                                <div id="tab-1" class="tab-pane active">
                                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                                        {{ csrf_field() }}

                                        <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Basic Information</h2>
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div>
                                                    <label>First Name</label>
                                                    <input class="form-control" type="text" name="first_name" value="{{ @$user->first_name }}" placeholder="First Name">
                                                </div>

                                                @if(isset($arr['first_name']) && $arr['first_name']->property_name=="first_name")
                                                    <div id="need-approval-val-first_name">Need approval: {{$arr['first_name']->property_value}}</div>
                                                    @if($arr['first_name']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="u-first_name-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-first_name-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-first_name-{{@$user->id}}">Approve</button>

                                                    @endif
                                                @else
                                                    @if($user->first_name != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="col-md-3">
                                                <div>
                                                    <label>Last Name</label>
                                                    <input class="form-control" type="text" name="last_name" value="{{ @$user->last_name }}" placeholder="Last Name">
                                                </div>

                                                @if(isset($arr['last_name']) && $arr['last_name']->property_name=="last_name")
                                                    <div id="need-approval-val-last_name">Need approval: {{$arr['last_name']->property_value}}</div>
                                                    @if($arr['last_name']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="u-last_name-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-last_name-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-last_name-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($user->last_name != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>

                                            <div class="col-md-3">
                                                
                                                <label>Email Address</label>
                                                <input class="form-control" type="text" name="email" value="{{ @$user->email }}" placeholder="Email Address">
                                            </div>
                                            <div class="col-md-1">
                                                <label>Country Code</label>
                                                <select class="form-control" name="mobile_country_code">
                                                    <option value="" disabled="" selected="">--Please select an option--</option>
                                                    @foreach($countryList as $country)
                                                        <option value="{{$country->id}}" id="{{$country->id}}" <?php echo (@$user->mobile_country_code == $country->id)?'selected':''; ?>  >+{{$country->phonecode}}  ({{$country->name}})</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            
                                            <div class="col-md-2">
                                                <div>
                                                <label>Mobile Number</label>
                                                <input class="form-control" type="text" name="mobile_number" value="{{ @$user->mobile_number }}" placeholder="Mobile Number">
                                                </div>

                                                @if(isset($arr['mobile_number']) && $arr['mobile_number']->property_name=="mobile_number")
                                                    <div id="need-approval-val-mobile_number">Need approval: {{$arr['mobile_number']->property_value}}</div>
                                                    @if($arr['mobile_number']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="u-mobile_number-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-mobile_number-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-mobile_number-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($user->mobile_number != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Change password -->
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Password</label>
                                                <input class="form-control" type="password" name="login_password" value="" placeholder="Password">
                                                <br /><small>Password must be minimum 6 characters.</small>
                                            </div>
                                        </div>
                                        <!-- Change password -->
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Location</h2>
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="address">Address</label>
                                                    <input type="text" name="address" id="address" class="form-control" value="{{$user->address}}" />
                                                </div>
                                                @if(isset($arr['address']) && $arr['address']->property_name=="address")
                                                    <div id="need-approval-val-address">Need approval: {{$arr['address']->property_value}}</div>
                                                    @if($arr['address']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="u-address-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-address-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-address-{{@$user->id}}">Approve</button>

                                                    @endif
                                                @else
                                                    @if($user->address != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Country</label>
                                                <select class="selectpicker  form-control dynamic" data-dependent='state' data-show-subtext="true" data-live-search="true" id="country"  name="country_code">
                                                    <option disabled="" >-- Please select an option --</option>
                                                    @foreach (@$countryList as $country)
                                                        <option value="{{ $country->id }}" {{ ( $country->id == @$user->country_id) ? 'selected' : '' }}> {{ $country->name }} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>State</label>
                                                <select name="state" id="state" class="form-control dynamic" data-dependent='city'>
                                                    @foreach (getStateByCountry($user->country_id) as $state)
                                                        <option value="{{ $state->id }}" {{ ( $state->id == @$user->state_id) ? 'selected' : '' }}> {{ $state->name }} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <div>
                                                    <label>City</label>
                                                    <select name="city" id="city" class="selectpicker  form-control dynamic" required="" aria-required="true">
                                                        <option value="">Select City</option>
                                                        @foreach(getCityByState($user->state_id) as $city)
                                                            <option value="{{ $city->id }}" {{ $user->city_id == $city->id ? "selected":"" }}>{{ $city->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div>
                                                    <label>Zipcode </label>
                                                    <input class="form-control" type="text"  name="zip" maxlength="8" value="{{ @$user->zip }}" placeholder="Zipcode" />
                                                </div>
                                                @if(isset($arr['zip']) && $arr['zip']->property_name=="zip")
                                                    <div id="need-approval-val-zip">Need approval: {{$arr['zip']->property_value}}</div>
                                                    @if($arr['zip']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="u-zip-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-zip-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="u-zip-{{@$user->id}}">Approve</button>

                                                    @endif
                                                @else
                                                    @if($user->zip != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>

                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Additional Information</h2>
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div>
                                                    <label for="company-name">Company Name/Title</label>
                                                    <input type="text" class="form-control" id="company-name" name="company_name" value="{{$vendorInfo->company_name}}" placeholder="Company Name" />
                                                </div>
                                                @if(isset($arr['company_name']) && $arr['company_name']->property_name=="company_name")
                                                    <div   id="need-approval-val-company_name">Need approval: {{$arr['company_name']->property_value}}</div>
                                                    @if($arr['company_name']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-company_name-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-company_name-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-company_name-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->company_name != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="col-md-4">
                                                <div>
                                                    <label for="established-year">Established Year</label>
                                                    <input type="text" class="form-control" id="established-year" name="established_year" value="{{$vendorInfo->established_year}}" placeholder="Established Year" />
                                                </div>
                                                @if(isset($arr['established_year']) && $arr['established_year']->property_name=="established_year")
                                                    <div    id="need-approval-val-established_year">Need approval: {{$arr['established_year']->property_value}}</div>
                                                    @if($arr['established_year']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-established_year-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-established_year-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-established_year-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->established_year != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="col-md-4">
                                                <div>
                                                    <label for="vendor_category">Category</label>
                                                    <select name="vendor_category" id="vendor_category" class="form-control">
                                                        <option value="">Select Category</option>
                                                        @foreach($vendor_categories as $category)
                                                            @if($category->id == $vendorInfo->vendor_category)
                                                            <option selected="selected" value="{{$category->id}}">{{$category->name}}</option>
                                                            @else
                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="company-slug">Company Slug</label>
                                                <input type="text" class="form-control" id="company-slug" name="company_slug" value="{{$vendorInfo->company_slug}}" placeholder="Company Slug" />
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="business_information">About Us</label>
                                                    <textarea class="form-control" name="business_information" id="business_information" rows="5">{{$vendorInfo->business_information}}</textarea>
                                                </div>
                                                @if(isset($arr['business_information']) && $arr['business_information']->property_name=="business_information")
                                                    <div id="need-approval-val-business_information">Need approval: {{$arr['business_information']->property_value}}</div>
                                                    @if($arr['business_information']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-business_information-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-business_information-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-business_information-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->business_information != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="col-md-3">
                                                <label for="display_address">Display Address</label>
                                                <br />
                                                <input type="checkbox" name="display_address" @if($vendorInfo->display_address == 1) checked="checked" @endif value="1" id="display_address" />
                                            </div>
                                            <div class="col-md-3">
                                                <label for="display_business_number">Display Business Number</label>
                                                <br />
                                                <input type="checkbox" name="display_business_number" @if($vendorInfo->display_business_number == 1) checked="checked" @endif value="1" id="display_business_number" />
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="specials">Description</label>
                                                    <textarea class="form-control" name="specials" id="specials" rows="5">{{$vendorInfo->specials}}</textarea>
                                                </div>
                                                @if(isset($arr['specials']) && $arr['specials']->property_name=="specials")
                                                    <div id="need-approval-val-specials">Need approval: {{$arr['specials']->property_value}}</div>
                                                    @if($arr['specials']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-specials-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-specials-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-specials-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->specials != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="meta_keywords">Meta Keywords</label>
                                                    <textarea class="form-control" name="meta_keywords" id="meta_keywords" rows="5">{{$vendorInfo->meta_keywords}}</textarea>
                                                </div>
                                                @if(isset($arr['meta_keywords']) && $arr['meta_keywords']->property_name=="meta_keywords")
                                                    <div id="need-approval-val-meta_keywords">Need approval: {{$arr['meta_keywords']->property_value}}</div>
                                                    @if($arr['meta_keywords']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-meta_keywords-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-meta_keywords-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-meta_keywords-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->meta_keywords != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="business_website">Business Website</label>
                                                    <input type="text" name="business_website" id="business_website" class="form-control" value="{{$vendorInfo->business_website}}" />
                                                </div>
                                                @if(isset($arr['business_website']) && $arr['business_website']->property_name=="business_website")
                                                    <div id="need-approval-val-business_website">Need approval: {{$arr['business_website']->property_value}}</div>
                                                    @if($arr['business_website']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-business_website-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-business_website-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-business_website-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->business_website != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="facebook">Facebook</label>
                                                    <input type="text" name="facebook" id="facebook" class="form-control" value="{{$vendorInfo->facebook}}" />
                                                </div>
                                                @if(isset($arr['facebook']) && $arr['facebook']->property_name=="facebook")
                                                    <div id="need-approval-val-facebook">Need approval: {{$arr['facebook']->property_value}}</div>
                                                    @if($arr['facebook']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload=via-facebook-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-facebook-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-facebook-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->facebook != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="instagram">Instagram</label>
                                                    <input type="text" name="instagram" id="instagram" class="form-control" value="{{$vendorInfo->instagram}}" />
                                                </div>
                                                @if(isset($arr['instagram']) && $arr['instagram']->property_name=="instagram")
                                                    <div id="need-approval-val-instagram">Need approval: {{$arr['instagram']->property_value}}</div>
                                                    @if($arr['instagram']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-instagram-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-instagram-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-instagram-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->instagram != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="pinterest">Pinterest</label>
                                                    <input type="text" name="pinterest" id="pinterest" class="form-control" value="{{$vendorInfo->pinterest}}" />
                                                </div>
                                                @if(isset($arr['pinterest']) && $arr['pinterest']->property_name=="pinterest")
                                                    <div id="need-approval-val-pinterest">Need approval: {{$arr['pinterest']->property_value}}</div>
                                                    @if($arr['pinterest']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-pinterest-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-pinterest-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-pinterest-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->pinterest != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="twitter">Twitter</label>
                                                    <input type="text" name="twitter" id="twitter" class="form-control" value="{{$vendorInfo->twitter}}" />
                                                </div>
                                                @if(isset($arr['twitter']) && $arr['twitter']->property_name=="twitter")
                                                    <div id="need-approval-val-twitter">Need approval: {{$arr['twitter']->property_value}}</div>
                                                    @if($arr['twitter']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-twitter-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-twitter-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-twitter-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->twitter != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="linkedin">Linked In</label>
                                                    <input type="text" name="linkedin" id="linkedin" class="form-control" value="{{$vendorInfo->linkedin}}" />
                                                </div>
                                                @if(isset($arr['linkedin']) && $arr['linkedin']->property_name=="linkedin")
                                                    <div id="need-approval-val-linkedin">Need approval: {{$arr['linkedin']->property_value}}</div>
                                                    @if($arr['linkedin']->status=='0')
                                                        <small><strong style="color:green"></strong></small><br>
                                                        <button class="btn btn-danger btn-xs leave_feedback" payload="vi-linkedin-{{@$user->id}}">Unapprove</button>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-linkedin-{{@$user->id}}" >Approve</button>
                                                    @else
                                                        <small><strong style="color:red">Disapproved. Waiting for response.</strong></small><br>
                                                        <button class="btn btn-success btn-xs approve_property" payload="vi-linkedin-{{@$user->id}}">Approve</button>
                                                    @endif
                                                @else
                                                    @if($vendorInfo->linkedin != '')
                                                        <small><strong style="color:green">Approved</strong></small><br>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>

                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-success">Grant Access</h2>
                                            </div>
                                        </div>

                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Grant/Revoke Access</label>
                                                <select class="form-control" name="grant_access">
                                                    <option disabled="">-- Please select an option --</option>
                                                    <option value="N" <?php if(@$user->access_grant == null ||  @$user->access_grant == "N"){ echo 'selected'; } ?>>Revoke Access</option>
                                                    <option value="Y"<?php if(@$user->access_grant == "Y"){ echo 'selected'; } ?>>Grant Access</option>
                                                </select>
                                                <small>
                                                <strong style="color: blue;">( Current Status : {{ ( $user->access_grant == "Y") ? 'Approved/Access Granted' : 'Disapproved/Access Revoked' }} )</strong>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- TAB 1: Basic Information ENDED --}}

                                {{-- TAB 2: Additional Information STARTED --}}
                                {{-- <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Additional Information</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                {{-- TAB 2: Additional Information ENDED --}}
                                
                                {{-- TAB 3: Images STARTED --}}	
                                <div id="tab-3" class="tab-pane ">
                                    <div class="panel-body">
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3 class="text-left text-danger">Cover</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 panel-body cover">
                                                <p class="text-center"><b>Gif,PNG &amp; JPG Format (MAX 1455*375 pixel)</b></p>
                                                <div class="crop-avatar text-center">
                                                    <div class="photo-panel">
                                                        <div class="avatar-view" data-original-title="" title="">
                                                            <span id="fileselector">
                                                                <a class="btn btn-default">
                                                                    <i class="fa fa-upload" aria-hidden="true"></i> Select Banner
                                                                </a>
                                                            </span>								
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <ul class="list-unstyled list-inline">
                                                    @foreach($userimages['cover'] as $cover)
                                                        <li id="image-{{$cover->id}}">
                                                            <div class="panel panel-danger" >
                                                                <div class="panel-body">
                                                                    <div class="text-center">
    
                                                                        <img class="img-rounded"    src="{{config('constants.S3_WEDDING_IMAGES').$cover->image_full}}" style="max-width: 900px;">
    
                                                                        <ul class="list-unstyled list-inline margin-top10">
    
                                                                            <?php if($cover->is_approved=='Y') { ?>
                                                                                <li><strong style="color:green">Approved</strong></li>   
                                                                            <?php } else if($cover->is_approved=='N') { ?>
                                                                            <li>
                                                                                <button class="btn btn-success btn-xs approve_photo" data-photoid="{{$cover->id}}"   >Approve</button>
                                                                            </li>
                                                                            <li>
                                                                                <button class="btn btn-warning btn-xs disapprove_photo" data-photoid="{{$cover->id}}" >Disapprove</button>
                                                                            </li>
                                                                            <?php } else if($cover->is_approved=='D') { ?>
    
                                                                            <li>
                                                                                <button class="btn btn-warning btn-xs disapprove_photo" disabled>Disapproved</button><br> <strong>Reason: <?php echo $cover->disapprove_reason; ?></strong>
                                                                            </li>
                                                                            <?php } ?>
    
                                                                            <li>
    
                                                                                <button type="button" class="btn btn-danger btn-xs deleteImage"  data-id="{{$cover->id}}" >Delete</button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3 class="text-left text-danger">Profile Pictures</h3>
                                                <div class="panel-body cover">
                                                    <p class="text-center"><b>Gif,PNG &amp; JPG Format (Dimension is 500*500 pixels.)</b></p>
                                                </div>
                                                <div class="crop-avatar-another text-center">
                                                    <div class="photo-panel">
                                                        <div class="avatar-view-photo" data-original-title="" title="">
                                                            <span id="fileselector">
                                                                <a class="btn btn-default">
                                                                    <i class="fa fa-upload photo-upload" aria-hidden="true"></i> Select Photo
                                                                </a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="list-unstyled list-inline">
                                                    @foreach($userimages['profile'] as $profile)
                                                        <li id="image-{{$profile->id}}">
                                                            <div class="panel panel-danger" >
                                                                <div class="panel-body">
                                                                    <div class="text-center">
    
                                                                        <img class="img-rounded"    src="{{config('constants.S3_WEDDING_IMAGES').$profile->image_thumb}}" style="max-width: 250px;">
    
                                                                        <ul class="list-unstyled list-inline margin-top10">
    
                                                                            <?php  if($profile->is_approved=='Y') { ?>
                                                                                <li><strong style="color:green">Approved</strong></li> 
                                                                            <?php } else if($profile->is_approved=='N') { ?>
                                                                            <li>
                                                                                <button class="btn btn-success btn-xs approve_photo" data-photoid="{{$profile->id}}"   >Approve</button>
                                                                            </li>
                                                                            <li>
                                                                                <button class="btn btn-warning btn-xs disapprove_photo" data-photoid="{{$profile->id}}" >Disapprove</button>
                                                                            </li>
                                                                            <?php } else if($profile->is_approved=='D') { ?>
    
                                                                            <li>
                                                                                <button class="btn btn-warning btn-xs disapprove_photo" disabled>Disapproved</button><br> <strong>Reason: <?php echo $profile->disapprove_reason; ?></strong>
                                                                            </li>
                                                                            <?php } ?>
    
                                                                            <li>
                                                                                <button type="button" class="btn btn-danger btn-xs deleteImage"  data-id="{{$profile->id}}" >Delete</button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- TAB 3: Images ENDED --}}	
                                
                                {{-- TAB 4: Videos STARTED --}}
                                <div id="tab-4" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Videos</h2>
                                            </div>
                                        </div>
                                        
                                        <div class="row text-center">	
                                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#addVideo">ADD NEW VIDEO</button>
                                        </div>
                                        <!-- Modal -->
                                        <div id="addVideo" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                                                        <br>
                                                        <h4 class="text-center" id="video-add-popup-title">ADD NEW VIDEO</h4>
                                                        <hr>
                                                        <div class="form-group">
                                                            <label for="video_title">Video Title</label>
                                                            <input type="text" name="video_title" id="video_title" class="form-control" placeholder="Video Title">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="embed_code">Video URL:</label>
                                                            <input type="text" name="embed_code" id="embed_code" class="form-control" placeholder="Enter youtube video url.">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="description">Description:</label>
                                                            <textarea class="form-control" rows="5" id="description" name="description" placeholder="Add description not more than 500 characters."></textarea>
                                                        </div>
                                                        <br>
                                                        <div class="text-center">
                                                            <button type="button" class="btn btn-default" style="background:#e48aaa!important;border-color:#e48aaa!important;" id="add-video-btn">ADD</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            @if(count($videos) > 0)
                                                @foreach ($videos as $video)
                                                    <div class="col-sm-3 col-xs-3 panel-body" id="video-box-{{$video->id}}">
                                                        <div class="thum">
                                                            <div class="embed-video" data-source="youtube" data-video-url="{{$video->embed_code}}" data-cc-policy="1"></div>
                                                            <p class="text-right"><a href="javascript:delete_video('{{$video->id}}');" class="delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a></p>
                                                        </div>
                                                        <ul class="list-unstyled list-inline margin-top10">
    
                                                            <?php  if($video->is_approved=='Y') { ?>
                                                                <li><strong style="color:green">Approved</strong></li> 
                                                            <?php } else if($video->is_approved=='N') { ?>
                                                            <li>
                                                                <button class="btn btn-success btn-xs approve_video" data-videoid="{{$video->id}}"   >Approve</button>
                                                            </li>
                                                            <li>
                                                                <button class="btn btn-warning btn-xs disapprove_video" data-videoid="{{$video->id}}" >Disapprove</button>
                                                            </li>
                                                            <?php } else if($video->is_approved=='D') { ?>

                                                            <li>
                                                                <button class="btn btn-warning btn-xs disapprove_video" disabled>Disapproved</button><br> <strong>Reason: <?php echo $video->disapprove_reason; ?></strong>
                                                            </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                @endforeach
                                            @endif
                                            <span id="video-emebed-section"></span>
                                        </div>
                                    </div>
                                </div>
                                {{-- TAB 4: Videos ENDED --}}

                                {{-- TAB 5: Showcase STARTED --}}
                                <div id="tab-5" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Showcase</h2>
                                            </div>
                                        </div>
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Associate Satellites</label>
                                            </div>
                                        </div>
                                        @if($satellites != "")
                                            @foreach ($satellites as $satellite)
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    {{$satellite->title}} : <a href="{{$satellite->url}}" target="_blank">(View Listing)</a>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <input type="checkbox" name="satellites[]" @if(in_array($satellite->id, $associated_satellites)) checked="checked" @endif value="{{$satellite->id}}" />
                                                </div>
                                            </div>	
                                            @endforeach
                                        @endif
                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Additional Categories</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div>
                                                @if(!empty($vendor_categories))
                                                    @php
                                                        $k = 1;
                                                    @endphp
                                                    @foreach ($vendor_categories as $category)
                                                        @if($vendorInfo->vendor_category != $category->id)
                                                            @if($k % 12 == 1)
                                                                <div class="col-sm-4">
                                                            @endif
                                                            <div class="checkbox">
                                                                <label><input name="categories[]" type="checkbox" value="{{$category->id}}" @if(!empty($additional_categories) && in_array($category->id, $additional_categories)) checked="checked" @endif>{{$category->name}} </label>
                                                            </div>
                                                            @if($k % 12 == 0)
                                                                </div>
                                                            @endif
                                                            @php
                                                                $k++;
                                                            @endphp
                                                        @endif
                                                    @endforeach
                                                    @if ($k % 12 != 1) </div> @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- TAB 5: Showcase ENDED --}}

                                {{-- TAB 6: FAQs STARTED --}}
                                <div id="tab-6" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">FAQs</h2>
                                            </div>
                                        </div>
                                        @if(!empty($faqs))
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($faqs as $faq)
                                            <div class="row">	
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="field-{{$faq->id}}">{{$i}}. {{$faq->question}}</label>
                                                        @switch($faq->answer_type)
                                                            @case('text')
                                                                <input type="text" name="field-{{$faq->id}}" id="field-{{$faq->id}}" class="form-control" placeholder="{{@$faq->vendor_faq_answer_placeholder->placeholder}}" value="{{@$faq->answer}}">
                                                                @break
                                                            @case('text_area')
                                                                <textarea class="form-control" name="field-{{$faq->id}}" id="field-{{$faq->id}}" placeholder="{{@$faq->vendor_faq_answer_placeholder->placeholder}}" rows="5">{{@$faq->answer}}</textarea>
                                                                @break
                                                            @case('radio')
                                                                @php
                                                                    $options_string =  $faq->vendor_faq_answer_options->options;
                                                                    $options_arry = explode(",", $options_string);
                                                                @endphp
                                                                @foreach ($options_arry as $option)
                                                                    <br /><label class="radio-inline">
                                                                        <input type="radio" name="field-{{$faq->id}}" value="{{trim($option)}}" @if(@$faq->answer == trim($option)) checked="checked" @endif>{{trim($option)}}
                                                                    </label>    
                                                                @endforeach
                                                                @break
                                                            @case('checkbox')
                                                                @php
                                                                    $options_string =  $faq->vendor_faq_answer_options->options;
                                                                    $options_arry = explode(",", $options_string);
                                                                    $chk_answers = explode(",", @$faq->answer);
                                                                    $k = 1;
                                                                @endphp
                                                                    <div class="row">
                                                                    @foreach ($options_arry as $option)
                                                                        @if($k % 3 == 1)
                                                                            <div class="col-sm-3">
                                                                        @endif
                                                                                <div class="checkbox">
                                                                                    <label><input type="checkbox" value="{{trim($option)}}"name="field-{{$faq->id}}[]" @if(in_array(trim($option), $chk_answers)) checked="checked" @endif>{{trim($option)}}</label>
                                                                                </div>
                                                                        @if($k % 3 == 0)
                                                                            </div>
                                                                        @endif
                                                                        @php
                                                                            $k++;    
                                                                        @endphp
                                                                    @endforeach
                                                                    @if ($k % 3 != 1) </div> @endif
                                                                    </div>
                                                                @break
                                                        @endswitch
                                                    </div>
                                                </div>
                                            </div>      
                                            @php
                                                $i++;    
                                            @endphp
                                            @endforeach
                                        @endif	
                                    </div>
                                </div>
                                {{-- TAB 6: FAQs ENDED --}}

                                {{-- TAB 7: Custo FAQs STARTED --}}
                                <div id="tab-7" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Custom FAQs</h2>
                                            </div>
                                        </div>
                                        @foreach ($custom_faqs as $custom_faq)
                                            <hr class="hr-line-dashed">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="cust-faq-que-{{$custom_faq->id}}">Question:</label>
                                                    <input type="text" class="form-control" id="cust-faq-que-{{$custom_faq->id}}" name="custom_question[{{$custom_faq->id}}]" value="{{$custom_faq->question}}" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="cust-faq-ans-{{$custom_faq->id}}">Answer:</label>
                                                    <textarea name="custom_answer[{{$custom_faq->id}}]" id="cust-faq-ans-{{$custom_faq->id}}" class="form-control" rows="5">{{$custom_faq->answer}}</textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <ul class="list-unstyled list-inline" style="margin-top: 10px;">
                                                        <?php if($custom_faq->is_approved == 'Y') { ?>
                                                            <li><strong style="color:green">Approved</strong></li>   
                                                        <?php } else if($custom_faq->is_approved == 'N') { ?>
                                                        <li>
                                                            <button class="btn btn-success btn-xs approve_custom_faq" data-cfaqid="{{$custom_faq->id}}"   >Approve</button>
                                                        </li>
                                                        <li>
                                                            <button class="btn btn-warning btn-xs disapprove_custom_faq" data-cfaqid="{{$custom_faq->id}}" >Disapprove</button>
                                                        </li>
                                                        <?php } else if($custom_faq->is_approved == 'D') { ?>
                                                        <li>
                                                            <button class="btn btn-warning btn-xs disapprove_custom_faq" disabled>Disapproved</button><br> <strong>Reason: <?php echo $custom_faq->disapprove_reason; ?></strong>
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                {{-- TAB 7: Custom FAQs ENDED --}}

                                {{-- TAB 8: Tracking STARTED --}}
                                <div id="tab-8" class="tab-pane">
                                    <div class="panel-body">
                                        <style>
                                            #map {
                                                height: 100%;
                                            }
                                            
                                            html,
                                            body {
                                                height: 100%;
                                                margin: 0;
                                                padding: 0;
                                            }
                                        </style>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="text-left text-danger">Tracking Details</h2>
                                            </div>
                                        </div>

                                        <hr class="hr-line-dashed">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div>
                                                    <label>IP Address : <span class="text-danger">{{ $user->signup_ip_address }}</span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div>
                                                    <label>Country Code : <span class="text-danger"> {{ $country_code }}</span></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- TAB 7: Tracking ENDED --}}
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-md btn-success">Save + Review</button>
                                    <a class="btn btn-md btn-default" href="{{ url('/vendors') }}">Cancel</a>
                                </div>
                            </div>		
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Cropping Model for Banner STARTED --}}
<div class="crop-avatar">
    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class="avatar-form" id="bannerVendorUploadForm" action="{{ route('ajax-upload-admin-vendor-cover') }}" enctype="multipart/form-data" method="post">
                    @csrf
                    <input type="hidden" value="" name="codeprofile">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                        <br>
                        <h4 class="text-center">Upload Banner</h4>
                        <hr>
                        <div class="avatar-body">
                            <div class="avatar-upload">
                                <input type="hidden" class="avatar-src" name="avatar_src">
                                <input type="hidden" class="avatar-data" name="avatar_data">
                                <label for="avatarInput">Select Photo</label>
                                <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                            </div>
                            
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="avatar-wrapper"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="avatar-preview preview-lg hidden-xs"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                    <div class="avatar-preview preview-md remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                    <div class="avatar-preview preview-sm remove hidden"><img src="{{ asset('img/banner-img.jpg') }}"></div>
                                    {{-- Zoom in out STARTED --}}
                                    <div class="avatar-btns">
                                        <div class="btn-group"><br>
                                            <button type="button" class="btn btn-default fa fa-search-plus" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                            <button type="button" class="btn btn-default fa fa-search-minus" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                        </div>
                                    </div>
                                    {{-- Zoom in out ENDED --}}
                                    <br>
                                    <button type="submit" class="btn btn-default btn-block avatar-save">Add Banner</button>
                                    <div class="text-center">
                                        <img src="{{asset('img/loader.gif')}}" id="loader-banner" width="20" style="display: none">
                                    </div>
                                </div>
                            </div>

                            <div class="row avatar-btns panel-body">
                                <div class="col-md-9 text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="avatar-view" title="" style="display:none" data-original-title="Change the profile Picture">
                                        <img src="{{ asset('img/banner-img.jpg') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Cropping Model for Banner ENDED --}}
{{-- Cropping Model for Photo STARTED --}}
<div class="crop-avatar-another">
    <div class="modal fade" id="avatar-modal-photo" aria-hidden="true" aria-labelledby="avatar-modal-photo-label" role="dialog" tabindex="-1" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class="avatar-form-photo" id="photoVendorUploadForm" action="{{ route('ajax-upload-admin-vendor-profile') }}" enctype="multipart/form-data" method="post">
                    @csrf
                    <input type="hidden" value="" name="codeprofile">
                    <input type="hidden" name="allowed_photos" value='1'>
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" style="text-shadow:none;font-size:30px;opacity:1;">×</button>
                        <br>
                        <h4 class="text-center">Upload Photo</h4>
                        <hr>
                        <div class="avatar-body-photo">
                            <div class="avatar-upload-photo">
                                <input type="hidden" class="avatar-src-photo" name="avatar_src">
                                <input type="hidden" class="avatar-data-photo" name="avatar_data_photo">
                                <label for="avatarInputPhoto">Select Photo</label>
                                <input type="file" class="avatar-input-photo" id="avatarInputPhoto" name="avatar_file_photo">
                            </div>
                            
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="avatar-wrapper-photo"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="avatar-preview-photo preview-lg hidden-xs"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                    <div class="avatar-preview-photo preview-md remove hidden"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                    <div class="avatar-preview-photo preview-sm remove hidden"><img src="{{ asset('img/dummy-profile.png') }}"></div>
                                    {{-- Zoom in out STARTED --}}
                                    <div class="avatar-btns">
                                        <div class="btn-group"><br>
                                            <button type="button" class="btn btn-default fa fa-search-plus zoom-in-btn" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                                            <button type="button" class="btn btn-default fa fa-search-minus zoom-out-btn" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                                        </div>
                                    </div>
                                    {{-- Zoom in out ENDED --}}
                                    <br>
                                    <button type="submit" class="btn btn-default btn-block avatar-save-photo">Add Photo</button>
                                    <div class="text-center">
                                        <img src="{{asset('img/loader.gif')}}" id="loader-photo" width="20" style="display: none">
                                    </div>
                                </div>
                            </div>

                            <div class="row avatar-btns-photo panel-body">
                                <div class="col-md-9 text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default" data-method="rotate" data-option="-90" title="Rotate -90 degrees"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default" data-method="rotate" data-option="90" title="Rotate 90 degrees"><i class="fa fa-repeat" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="avatar-view-photo" title="" style="display:none" data-original-title="Change the profile Picture">
                                        <img src="{{ asset('img/dummy-profile.png') }}" alt="Profile Picture" id="profileimage" style="height:auto;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Cropping Model for Photo ENDED --}}
@include('modals.modal-save') @include('modals.modal-delete') @endsection @section('js') 
<script src="{{asset('dist/js/cropper.min.js')}}"></script>
<script src="{{asset('dist/js/main-cover-admin.js')}}"></script>
<script src="{{asset('dist/js/main-photo-admin.js')}}"></script>
@include('scripts.delete-modal-script') @include('scripts.save-modal-script') @include('scripts.check-changed')
<script src="{{asset('js/embed.videos.js')}}"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js" type="text/javascript"></script>
<script>
        CKEDITOR.config.allowedContent=true;
        CKEDITOR.replace( 'business_information' );
        CKEDITOR.replace( 'specials' );
    
        $(".pimage").change(function(e) {
        for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
            var file = e.originalEvent.srcElement.files[i];
            var img = document.createElement("img");
            var reader = new FileReader();
            reader.onloadend = function() {
                img.src = reader.result;
            }
            reader.readAsDataURL(file);
            $("#aimage").after(img);
        }
    });
    // photo upload starts //
    $("#bannerVendorUploadForm").on('submit', function(e){
        e.preventDefault();
        var image = $("#avatarInput")[0].files[0];
        if (image === undefined){
            var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
            $(".avatar-upload").after(msg);
            return false;
        } else {
            $("#loader-banner").show();
            $("button.avatar-save").attr('disabled','disabled');
            var _token = $('input[name="_token"]').val();
            var thumb_data = $('input[name="avatar_data"]').val();
            var user_id = {{$user->id}};
            var form = new FormData();
            form.append('image', image);
            form.append('_token', _token);
            form.append('user_id', user_id);
            form.append('thumb_data', thumb_data);
            $.ajax({
                url: "{{route('ajax-upload-admin-vendor-cover')}}",
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(data) {
                    $("#loader-banner").hide();
                    var response = data[0];
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                    if(response.status === 0) {
                        $(".avatar-upload").after(msg);
                        document.location.reload(true);
                        //$("button.avatar-save").removeAttr('disabled');
                    } else {
                        $("#avatar-modal").modal('hide');
                        $(".crop-avatar .img-responsive").attr('src', response.thumb_path);
                    }
                },
                error: function(xhr, status, error) {
                    $("#loader-banner").hide();
                    var errors = xhr.responseJSON;
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                    $(".avatar-upload").after(msg);
                    $("button.avatar-save").removeAttr('disabled');
                }
            });
        }
    });
    $('#avatar-modal').on('hidden.bs.modal', function () {
        $("#loader-banner").hide();
        $(".avatar-body .alert").remove();
        $("#bannerVendorUploadForm")[0].reset();
        $("button.avatar-save").removeAttr('disabled');
    });
    /* For photo upload */
    $("#photoVendorUploadForm").on('submit', function(e){
        e.preventDefault();
        var image = $("#avatarInputPhoto")[0].files[0];
        if (image === undefined){
            var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>Image is required</div>';
            $(".avatar-upload-photo").after(msg);
            return false;
        } else {
            $("#loader-photo").show();
            $("button.avatar-save-photo").attr('disabled','disabled');
            var _token = $('input[name="_token"]').val();
            var thumb_data = $('input[name="avatar_data_photo"]').val();
            var allowed_photos = '<?php echo $public_photo_allowed; ?>';
            var user_id = {{$user->id}};
            var form = new FormData();
            form.append('image', image);
            form.append('_token', _token);
            form.append('user_id', user_id);
            form.append('thumb_data', thumb_data);
            form.append('allowed_photos', allowed_photos);
            $.ajax({
                url: "{{route('ajax-upload-admin-vendor-profile')}}",
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(data) {
                    $("#loader-photo").hide();
                    var response = data[0];
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+response.message+'</div>';
                    if(response.status === 0){
                        $(".avatar-upload-photo").after(msg);
                        $("button.avatar-save-photo").removeAttr('disabled');
                    } else {
                        $("#avatar-modal-photo").modal('hide');
                        document.location.reload(true);
                        //$(".crop-avatar-another .img-responsive").attr('src', response.thumb_path);
                    }
                },
                error: function(xhr, status, error){
                    $("#loader-photo").hide();
                    var errors = xhr.responseJSON;
                    var msg = '<div class="alert alert-danger avatar-alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">&times;</button>'+errors.errors.image[0]+'</div>';
                    $(".avatar-upload").after(msg);
                    $("button.avatar-save").removeAttr('disabled');
                }
            });
        }
    });
    $('#avatar-modal-photo').on('hidden.bs.modal', function () {
        $("#loader-photo").hide();
        $(".avatar-body-photo .alert").remove();
        $("#photoVendorUploadForm")[0].reset();
        $("button.avatar-save-photo").removeAttr('disabled');
    });
    // photo upload ends //
    $(document).ready(function() {
        $('.embed-video').embedVideo();
    });
    var deleteImage = false;
    var imageId = 0;
    jQuery(".deleteImage").on('click', function() {
        jQuery("#confirmDelete .modal-title").html("Delete Image");
        jQuery("#confirmDelete .modal-body").html("<p>Are you sure you want to delete this Image ?</p>");
        jQuery("#confirmDelete").modal('show');
        deleteImage = true;
        imageId = jQuery(this).data('id');
    });
	
    $('#confirmDelete').on('hidden.bs.modal', function() {
        deleteImage = false;
    });
	
    $('#confirmDelete .modal-footer #confirm').on('click', function() {
        if (deleteImage) {
            var _token = $('input[name="_token"]').val();
            var form = new FormData();
            form.append('_token', _token);
            form.append('id', imageId);
            jQuery.ajax({
                url: "{{ route('admin-delete-vendor-image') }}",
                data: form,
                method: "post",
                processData: false,
                contentType: false
            }).done(function(response) {
                jQuery("#image-" + imageId).remove();
                jQuery("#confirmDelete").modal('hide');
                imageId = 0;
                deleteImage = false;
            });

        }
    })
</script>



<script>
function delete_video(id) {
    var deleteVidUrl = "{{ route('admin-delete-vendor-video',':id')}}";
    deleteVidUrl = deleteVidUrl.replace(':id', id);
    $.ajax({
        url:deleteVidUrl,
        method:"GET",
        success:function(resp)
        {
            $('#video-box-'+id).remove();
        }
    })
}
    $(document).ready(function() {
        $("#add-video-btn").on("click", function(e){
            e.preventDefault();
            var video_title = $('input[name="video_title"]').val();
            var embed_code = $('input[name="embed_code"]').val();
            var description = $('textarea[name="description"]').val();
            var user_id = $('input[name="user_id"]').val();
            var token = $('input[name="_token"]').val();

            $.ajax({
                url:"{{ route('admin-vendor-video-submit') }}",
                method:"POST",
                data:"user_id="+user_id+"&video_title="+video_title+"&embed_code="+encodeURI(embed_code)+"&description="+description+"&_token="+token,
                success:function(resp)
                {
                    $("#video-add-popup-title").after('<div style="color:green">Video Added Successfully.<br />Reloading...</div>');
                    setTimeout(function() { window.location.reload(true);}, 2000);
                    $('#addVideo').modal('toggle');
                }
            })
        });
        $('.dynamic').change(function() {
            if ($(this).val() != '') {
                var select = $(this).attr("id");
                var value = $(this).val();
                var dependent = $(this).data('dependent');
                var _token = $('input[name="_token"]').val();
                var output;
                if(dependent=="state") {
                    $('#city').html('<option value="">Select City</option>');
                }
                $('#' + dependent).html('');
                $("#phone-country-code option[selected='selected']").removeAttr('selected');
                $("#phone-country-code option[id=" + value + "]").attr("selected", "selected");
                $.ajax({
                    url: "{{ route('ajax.fetchLocation') }}",
                    method: "POST",
                    data:{
                        select: select,
                        value: value,
                        _token: _token,
                        dependent: dependent
                    },
                    success: function(result) {
                        for (var i = 0; i < result.length; i++) {
                            output += "<option value=" + result[i].id + ">" + result[i].value + "</option>";
                        }
                        $('#' + dependent).append(output);
                    }

                })
            }
        });
    });


</script>

<script>
$(document).ready(function(){
	/** Approve pending user entries **/
	
	$(".approve_property").on("click", function(e)
	{
		e.preventDefault();
		var payload = $(this).attr("payload");
		var token = $('input[name="_token"]').val();
		var elm = $(this);

		$.ajax({
			url:"{{ route('ajax.manageUserEntries') }}",
			method:"POST",
			data:"_token="+token+"&action=approve_entry&payload="+payload,
			success:function(resp)
			{   
				elm.prop("disabled", true);
                elm.parent().find(".leave_feedback").remove();
                elm.parent().find("strong").css({"color":"green"}).text("Approved");
                elm.remove();
                // replace element content with approved.
                if(resp.field == 'company_name') {
                    $('input[name="'+resp.field+'"]').val(resp.value);
                    $('input[name="company_slug"]').val(resp.company_slug);
                } else if(resp.field == 'meta_keywords') {
                    $('textarea[name="'+resp.field+'"]').val(resp.value);
                } else if (resp.field == 'business_information' || resp.field == 'specials') {
                    window.location.reload(true);
                } else {
                    $('input[name="'+resp.field+'"]').val(resp.value);
                }
                if($("#need-approval-val-"+resp.field).length > 0) {
                    $("#need-approval-val-"+resp.field).remove()
                }
                //window.location.reload(true);
			}
		})
	})
	
	/** leave feedback to against user entries **/
	$(".leave_feedback").on("click", function(e){
		e.preventDefault();
		
		var message = prompt("Write a message","");
		var payload = $(this).attr("payload");
		var token = $('input[name="_token"]').val();
		var elm = $(this);
		 
		if(message == "" || message == null){
			alert("Message required.");
			return;
		}else{
			$.ajax({
				url:"{{ route('ajax.manageUserEntries') }}",
				method:"POST",
				data:"message="+message+"&_token="+token+"&action=leave_feedback&payload="+payload,
				success:function(resp)
				{ 				
					elm.parent().find("strong").css({"color":"red"}).text("Disapproved. Waiting for response.");
                    elm.remove();
				}
			})
		}
	})

 
    $(".approve_photo").on("click", function(e){
        e.preventDefault();
        //var message = prompt("Reason of Disappr","");
        //approve_photo
        var photo_id = $(this).data('photoid');
        var token = $('input[name="_token"]').val();

        var elm = $(this);

        $.ajax({
            url:"{{ route('ajax.approveVendorPhoto') }}",
            method:"POST",
            data:"photo_id="+photo_id+"&_token="+token,
            success:function(resp)
            {
                elm.parent().parent().find("li .disapprove_photo").remove();
                elm.text("Approved");
                elm.prop("disabled", true);
            }
        })
    })

    $(".disapprove_photo").on("click", function(e){
        e.preventDefault();
        var message = prompt("Reason of Disapprove","");
        var photo_id = $(this).data('photoid');
        var token = $('input[name="_token"]').val();

        var elm = $(this);

        $.ajax({
            url:"{{ route('ajax.disapproveVendorPhoto') }}",
            method:"POST",
            data:"message="+message+"&photo_id="+photo_id+"&_token="+token,
            success:function(resp)
            {
                elm.parent().parent().find("li .approve_photo").remove();
                elm.text("Disapproved");
//                $('input[')
                elm.prop("disabled", true);
            }
        })
    })

    $(".approve_video").on("click", function(e){
        e.preventDefault();
        //approve_video
        var video_id = $(this).data('videoid');
        var token = $('input[name="_token"]').val();

        var elm = $(this);

        $.ajax({
            url:"{{ route('ajax.approveVendorVideo') }}",
            method:"POST",
            data:"video_id="+video_id+"&_token="+token,
            success:function(resp)
            {
                elm.parent().parent().find("li .disapprove_video").remove();
                elm.text("Approved");
                elm.prop("disabled", true);
            }
        })
    })

    $(".disapprove_video").on("click", function(e){
        e.preventDefault();
        var message = prompt("Reason of Disapprove","");
        var video_id = $(this).data('videoid');
        var token = $('input[name="_token"]').val();

        var elm = $(this);

        $.ajax({
            url:"{{ route('ajax.disapproveVendorVideo') }}",
            method:"POST",
            data:"message="+message+"&video_id="+video_id+"&_token="+token,
            success:function(resp)
            {
                elm.parent().parent().find("li .approve_video").remove();
                elm.text("Disapproved");
                elm.prop("disabled", true);
            }
        })
    })

    $(".approve_custom_faq").on("click", function(e){
        e.preventDefault();
        var token = $('input[name="_token"]').val();
        var faq_id = $(this).data('cfaqid');
        var elm = $(this);

        $.ajax({
            url:"{{ route('ajax.approveVendorCustomFaq') }}",
            method:"POST",
            data:"faq_id="+faq_id+"&_token="+token,
            success:function(resp)
            {
                elm.parent().parent().find("li .disapprove_custom_faq").remove();
                elm.text("Approved");
                elm.prop("disabled", true);
            }
        })
    })

    $(".disapprove_custom_faq").on("click", function(e){
        e.preventDefault();
        var message = prompt("Reason of Disapprove","");
        var faq_id = $(this).data('cfaqid');
        var token = $('input[name="_token"]').val();

        var elm = $(this);

        $.ajax({
            url:"{{ route('ajax.disapproveVendorCustomFaq') }}",
            method:"POST",
            data:"message="+message+"&faq_id="+faq_id+"&_token="+token,
            success:function(resp)
            {
                elm.parent().parent().find("li .approve_custom_faq").remove();
                elm.text("Disapproved");
                elm.prop("disabled", true);
            }
        })
    })
	
	/** updating select box if current value = DM **/
	$(".chosen-select").on("change", function(e, val){
			
			var target = $(this);
			var value = $(this).val();
			/** 
				@ earlier used no-filter attribute is no longer functional here
				@ chosen lib will fire event on every multi dropdown now
				@ including lifestyles, country/city/state etc
			**/
			var qg = [];
			var ng = [];
			
			/** exclude data other than DM **/
			qg = value.filter(function(value, index, arr){
				return value !== "DM";
			});
			
			/** Make DM only in selection **/
			ng = value.filter(function(value, index, arr){
				return value == "DM";
			});
			
			/** 
			  @ val.selected returns only single value of item clicked recently 
			  @ Logic : val.selected gives last item checked and if its DM then we simply put {ng} else{qg}
			**/
			if(val.selected == "DM"){
				var filtered = ng;
			}else{
				var filtered = qg;
			}

			console.log("@value =>" + value);
			console.log("@qg =>" + qg);
			console.log("@ng =>" + ng);
			console.log("@DM pos =>" + value.indexOf("DM")); 
			
			/** Updating choosen dropdown with modified data **/
			$(target).val(filtered);
			$(target).trigger("chosen:updated");
		
	})
	
	      $('body').delegate(".dynamic_admin", "change", function() {

            if ($(this).val() != '') {
                var select = $(this).attr("id");
                var value = $(this).val();
                var dependent = $(this).attr('dependent');
                var _token = $('input[name="_token"]').val();

                var output;
                if (dependent == "state_p") {
                    $('#state_p').html('<option value="">Select City</option>');
					$('#city_p').html('<option value="">Select City</option>'); 
					$("#city_p").trigger("chosen:updated");
                }
                 
                $.ajax({
					url:"{{ route('ajax.fetchLocation') }}",
                    method: "POST",
                    data: {
                        select: select,
                        value: value,
                        _token: _token,
                        dependent: dependent
                    },
                    success: function(result) {
						output += "<option value='DM'>Doesn't Matter</option>";
                        for (var i = 0; i < result.length; i++) {
                            output += "<option value=" + result[i].id + ">" + result[i].value + "</option>";
                        }
						
						var new_output = output.replace("undefined","");
                        $('#' + dependent).append(new_output);

                        $('#' + dependent).trigger("chosen:updated");
                    }

                })
            }
        });

})

$(document).ready(function(){
	$("strong").each(function(elm){
		if($(this).text() == "Un-Approved"){
			$(this).css({
				"color":"red"
			});
		}
	})
})
</script>
@endsection