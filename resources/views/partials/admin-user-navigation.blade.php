<?php $stats=admin_user_counter(); ?>
<span id="pull-left">
    <a href="{{ route('users') }}">
        <button class="btn @if(\Route::current()->getName() == 'users') btn-info @else btn-default  @endif "><i class="fa fa-users"></i> All users({{$stats['all_users']}})</button>
    </a>

    <a href="{{ route('active-user') }}">
        <button class="btn @if(\Route::current()->getName() == 'active-user') btn-info @else btn-default  @endif "><i class="fa fa-check"></i> Active users({{$stats['active_users']}})</button>
    </a>
    <a href="{{ route('inactive-users') }}">
        <button class="btn  @if(\Route::current()->getName() == 'inactive-users') btn-info @else btn-default  @endif " ><i class="fa fa-remove"></i> Inactive({{$stats['inactive_users']}})</button>
    </a>
    <a href="{{ route('incomplete-users') }}">
        <button class="btn  @if(\Route::current()->getName() == 'incomplete-users') btn-info @else btn-default  @endif " ><i class="fa fa-remove"></i> Incomplete({{$stats['incomplete_users']}})</button>
    </a>

    <a href="{{ route('banned-users') }}">
        <button class="btn  @if(\Route::current()->getName() == 'banned-users') btn-info @else btn-default  @endif "><i class="fa fa-ban"></i> Banned users({{$stats['banned_users']}})</button>
    </a>

    <a href="{{ route('deactivated-users') }}">
        <button class="btn  @if(\Route::current()->getName() == 'deactivated-users') btn-info @else btn-default  @endif "><i class="fa fa-warning"></i> Deactivated users({{$stats['deactivated_users']}})</button>
    </a>

    <a href="{{route('users-deleted')}}">
        <button class="btn  @if(\Route::current()->getName() == 'users-deleted') btn-info @else btn-default  @endif "><i class="fa fa-remove"></i> Deleted users({{$stats['deleted_users']}})</button>
    </a>

    <a href="{{route('latest-updates')}}">
        <button class="mrgn-top-5 btn  @if(\Route::current()->getName() == 'latest-updates') btn-info @else btn-default  @endif "><i class="fa fa-check-square"></i> Latest Updates({{$stats['latest_updates']}})</button>
    </a>
	

</span>
 