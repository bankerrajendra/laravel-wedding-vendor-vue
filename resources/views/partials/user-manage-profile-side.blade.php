<div class="list-group">
    <a href="{{ route('user-manage-profile') }}" class="list-group-item {{ Request::is('myprofile/user-manage-profile', 'myprofile/user-manage-profile/*') ? 'active' : '' }}">My Profile</a>
    <a href="{{ route('user-manage-photo') }}" class="list-group-item {{ Request::is('myprofile/user-manage-photo', 'myprofile/user-manage-photo/*') ? 'active' : '' }}">Profile Photo</a>
    <a href="{{ route('request-a-quote') }}" class="list-group-item">Connect with Vendors </a>
    <a href="{{ route('muslim-vendors') }}" class="list-group-item">Browse Vendors</a>
</div>