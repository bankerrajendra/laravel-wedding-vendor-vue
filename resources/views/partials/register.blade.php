<div class="container">
	<div class="row">
		<div class="col-sm-offset-3 col-sm-6">
		 <form method="POST" action="{{ route('register') }}" id="registrationForm">
		 
		    {{ csrf_field() }} 
				 
			<div class="register">
				<div class="panel">
					<h3>Create Your Account</h3>
				</div>
				<div class="form-group">
					<label for="email"><span class="star">*</span>Email</label>
					<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{old('email')}}" placeholder="Enter your email id">
						@if ($errors->has('email'))
							<span class="invalid-feedback" style="color: #dc3545;">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
						
					<span class="help-block email_invalid_code_err invalid-feedback"  style="font-weight:normal;color:#a94442;display:none">
						Invalid email format
					</span>
				</div>
				
				<div class="form-group">
					<label for="pass"><span class="star">*</span>Password</label>
					<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{old('password')}}"  name="password" id="password" placeholder="Enter password">
							@if ($errors->has('password'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
				</div>
				
				
				<div class="form-group">
					<label for="pass"><span class="star">*</span>Confirm Password</label>
					<input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" value="{{old('password_confirmation')}}"  name="password_confirmation" id="password_confirmation" placeholder="Enter password">
							@if ($errors->has('password_confirmation'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('password_confirmation') }}</strong>
								</span>
							@endif
				</div>
				
				
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group ">
							<label for="fname"><span class="star">*</span>First Name</label>
							<input type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{old('first_name')}}"  name="first_name" id="first_name" placeholder="Enter First Name">
							@if ($errors->has('first_name'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('first_name') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
						<div class="form-group">
							<label for="lname"><span class="star">*</span>Last Name</label>
							<input type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{old('last_name')}}"  id="last_name" placeholder="Enter Last Name">
							@if ($errors->has('last_name'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('last_name') }}</strong>
								</span>
							@endif
						</div>	
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<label for="mobile"><span class="star">*</span>Mobile</label>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-4 col-xs-4">
						<div class="form-group">
							<select name="phone_country_code" id="phone_country_code" class="selectpicker form-control  {{ $errors->has('phone_country_code') ? ' is-invalid' : '' }}" data-show-subtext="true" data-live-search="true">
							 
								<option value="">Select Country Code</option>
							   @foreach($countries as $country)
									@if (Input::old('phone_country_code') == $country->id)
										<option value="{{$country->id}}" id="{{$country->id}}" selected>+{{$country->phonecode}}  ({{$country->name}})</option>
									@else
										<option value="{{$country->id}}" id="{{$country->id}}">+{{$country->phonecode}}  ({{$country->name}})</option>
									@endif

								@endforeach
							</select>	
							<span class="help-block phone_country_code_err invalid-feedback"  style="font-weight:normal;color:#a94442;display:none">
								Mobile country code field is required
							</span>
							
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-4 col-xs-8">	
						<div class="form-group">
							<input type="text" value="{{old('phone_number')}}"  class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" id="phone_number" name="phone_number" placeholder="Enter your mobile number">
							@if ($errors->has('phone_number'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('phone_number') }}</strong>
								</span>
							@endif
						</div>	
					</div>
				</div>
				<div id="netStepOnMobileInput" style="display:none"  >
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group ">
							<label for="religion"><span class="star">*</span>Religion</label>
							<select name="religion" id="religion" class="form-control religionMap" dependent="sect">
								<option value="">Select Religion</option>
								 
								@foreach($religions as $name=>$rId)



									@if (Input::old('religion') == $rId)
										<option value="{{$rId}}" selected>{{ ucfirst($name) }}</option>
									@else
										<option value="{{$rId}}">{{ ucfirst($name) }}</option>
									@endif


								@endforeach
							</select>
							@if ($errors->has('religion'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('religion') }}</strong>
								</span>
							@endif
							
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
						<div class="form-group">
							<label for="Sect"><span class="star"></span>Sect</label>
							<select name="sect" id="sect"  dependent="sub_cast" class="form-control religionMap implementOthers">
								<option value="">Select Sect</option>
								@foreach($communities as $comunity)


									@if (Input::old('sect') == $comunity->id)
										<option value="{{$comunity->id}}" selected>{{ ucfirst($comunity->name) }}</option>
									@else
										<option value="{{$comunity->id}}">{{ ucfirst($comunity->name) }}</option>
									@endif

								@endforeach
							</select>
							
							@if ($errors->has('sect'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('sect') }}</strong>
								</span>
							@endif
						</div>	
					</div>
				</div>
				
				<div class="form-group">
					<label for="SubCaste"><span class="star"></span>Community</label>
					<select name="sub_cast" id="sub_cast" class="form-control implementOthers selectpicker {{ $errors->has('sub_cast') ? ' is-invalid' : '' }}" data-show-subtext="true" data-live-search="true">
						<option value="">Select Community</option>
						@foreach($subcastes as $subcast)

							@if (Input::old('sub_cast') == $subcast->id)
								<option value="{{$subcast->id}}" selected>{{ ucfirst($subcast->name) }}</option>
							@else
								<option value="{{$subcast->id}}">{{ ucfirst($subcast->name) }}</option>
							@endif

						@endforeach
					</select>
					<span class="help-block sub_cast_err invalid-feedback"  style="font-weight:normal;color:#a94442;display:none">
								Sub-caste field is required
							</span>
				</div>






				
				<div class="row">
					<div class="col-xs-12">
						<label for="dob"><span class="star">*</span>Date Of Birth</label>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<div class="form-group">
							<select name="day" id="day" class="form-control">
								@for ($day = 1; $day <= 31; $day++)

									@if (Input::old('day') == $day)
										<option value="{{ $day }}" selected>{{ $day }}</option>
									@else
										<option value="{{ $day }}">{{ $day }}</option>
									@endif

								@endfor
							</select>
							@if ($errors->has('day'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('day') }}</strong>
								</span>
							@endif							
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<div class="form-group">
							<select name="month" id="month" class="form-control">
								@for ($month = 1; $month <= 12; $month++)

									@if (Input::old('month') == $month)
										<option value="{{ $month }}" selected>{{ $month }}</option>
									@else
										<option value="{{ $month }}">{{ $month }}</option>
									@endif
								@endfor
							</select>	

							@if ($errors->has('month'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('month') }}</strong>
								</span>
							@endif								
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<div class="form-group">
							<select name="year" id="year" class="form-control">
								@for ($year =  (now()->year - 70); $year <= (now()->year - 18); $year++)

									@if (Input::old('year') == $year)
										<option value="{{ $year }}" selected>{{ $year }}</option>
									@else
										<option value="{{ $year }}">{{ $year }}</option>
									@endif
								@endfor
							</select>	
							@if ($errors->has('year'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('year') }}</strong>
								</span>
							@endif								
						</div>
					</div>

					@if ($errors->has('dob'))
						<span class="help-block">
							<strong>{{ $errors->first('dob') }}</strong>
						</span>
					@endif
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group ">
							<label for="Marital Status"><span class="star">*</span>Marital Status</label>
							<select name="marital_status" id="marital_status" class="form-control">
								<option value="">Select Marital Status</option>
								@foreach($marital_status as $rId=>$name)


									@if (Input::old('marital_status') == $rId)
										<option value="{{$rId}}" selected>{{ ucfirst($name) }}</option>
									@else
										<option value="{{$rId}}">{{ ucfirst($name) }}</option>
									@endif

								@endforeach
							</select>
							@if ($errors->has('marital_status'))
								<span class="help-block">
									<strong>{{ $errors->first('marital_status') }}</strong>
								</span>
							@endif
						</div>
					</div>
					
					<input type="hidden" name="name" id="name" value="casos">		
					
					<input type="hidden" name="ethnicity" id="ethnicity" value="Indian">
					 
					
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
						<div class="form-group">
							<label for="Height"><span class="star">*</span>Height</label>
							<select name="height" id="height" class="form-control">
								<option value="">Select Height</option>

                                @foreach($heights as $rId=>$name)

									@if (Input::old('height') == $rId)
										<option value="{{$rId}}" selected>{{ ucfirst($name) }}</option>
									@else
										<option value="{{$rId}}">{{ ucfirst($name) }}</option>
									@endif

                                @endforeach

							</select>
							@if ($errors->has('height'))
								<span class="help-block">
									<strong>{{ $errors->first('height') }}</strong>
								</span>
							@endif

						</div>	
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group ">
							<label for="gender"><span class="star">*</span>Gender</label>
							<select name="gender" id="gender" class="form-control">
								<option value="">Select Gender</option>
								@foreach($gender as $rId=>$name)

									@if (Input::old('gender') == $rId)
										<option value="{{$rId}}" selected>{{ ucfirst($name) }}</option>
									@else
										<option value="{{$rId}}">{{ ucfirst($name) }}</option>
									@endif

								@endforeach

							</select>
							@if ($errors->has('gender'))
								<span class="help-block">
									<strong>{{ $errors->first('gender') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
						<div class="form-group">
							<label for="profession"><span class="star">*</span>Profession</label>
							<select name="profession" id="profession" class="form-control  selectpicker dynamic" data-show-subtext="true" data-live-search="true">
								<option value="">Select Profession</option>
								@foreach($professions as $profession)
									@if (Input::old('profession') == $profession->id)
										<option value="{{ $profession->id }}" selected>{{ $profession->profession }}</option>
									@else
										<option value="{{ $profession->id }}">{{ $profession->profession }}</option>
									@endif
								@endforeach
							</select>
							
							<span class="help-block profession_code_err invalid-feedback"  style=";font-weight:normal;color:#a94442;display:none">
								Profession field is required
							</span>
							
						</div>	
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group ">
							<label for="country"><span class="star">*</span>Country</label>
							<select name="country" id="country" class="selectpicker form-control dynamic" dependent='state' data-show-subtext="true" data-live-search="true">
							
								<option value="">Select Country</option>
								@foreach($countries as $country)

									@if (Input::old('country') == $country->id)
										<option value="{{$country->id}}" id="{{$country->id}}" selected>{{$country->name}}</option>
									@else
										<option value="{{$country->id}}" id="{{$country->id}}">{{$country->name}}</option>
									@endif
								@endforeach
							</select>
							<span class="help-block country_code_err invalid-feedback"  style=";font-weight:normal;color:#a94442;display:none">
								Country field is required
							</span>
							
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group ">
							<label for="State"><span class="star">*</span>State</label>
							<select name="state" id="state" class="form-control dynamic" dependent='city'>
								<option value="">Select State</option>

							</select>
							@if ($errors->has('state'))
								<span class="help-block">
									<strong>{{ $errors->first('state') }}</strong>
								</span>
							@endif
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group ">
							<label for="City"><span class="star">*</span>City</label>
							<select name="city" id="city" class="form-control">
								<option value="">Select City</option>

							</select>
							
							@if ($errors->has('city'))
								<span class="help-block">
									<strong>{{ $errors->first('city') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
						<div class="form-group">
							<label for="zip"><span class="star">*</span>Zip/Pin/Area Code</label>
							<input type="text" class="form-control" id="zipcode" name="zipcode" value="{{old('zipcode')}}"   placeholder="Enter Zip/Pin/Area Code">
							@if ($errors->has('zipcode'))
								<span class="help-block">
									<strong>{{ $errors->first('zipcode') }}</strong>
								</span>
							@endif
						</div>	
					</div>
				</div>	
				</div>				

				<div class="form-group ">
					<div class="checkbox">
						<input type="checkbox" name="term_and_conditions" style="margin-left: 0px"><label style="color: #333;"> I agree to the Privacy & Terms and Conditions</label>
					</div>
				</div>
				
				<div class="form-group">
					<input type="submit" class="form-control btn btn-warning" value="SUBMIT">

				</div></form>
				<p class="text-center">Already a Member? <a href="{{route('login')}}">Login</a></p>
				<p class="text-center">By signing up, you agree to our <a href="{{route('cmsroots', ['slug'=>'term-condition'])}}">Terms & Conditions</a>, <a href="{{route('cmsroots', ['slug'=>'privacy'])}}">Privacy Policy</a></p>
			</div>
		
		</div>
	</div>
</div>

@section('footer_scripts')
    @if(config('settings.reCaptchStatus'))
        <script src='https://www.google.com/recaptcha/api.js'></script>
    @endif
    <script>
        $(document).ready(function(){

            $('.dynamic').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).attr('dependent');
                    var _token = $('input[name="_token"]').val();
                    var output;
                    if(dependent=="state") {
                        $('#city').html('<option value="">Select City</option>');
                    }
                    $('#'+dependent).html('<option value="">Select '+ dependent + ' </option>');
                    $("#phone-country-code option[selected='selected']").removeAttr('selected');
                    $("#phone-country-code option[id="+value+"]").attr("selected","selected");
                    $.ajax({
                        url:"{{ route('ajax.fetchLocation') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        { 
                            for(var i=0; i<result.length; i++){
                                output += "<option value="+result[i].id+">"+result[i].value+"</option>";
                            }
                            $('#'+dependent).append(output);
                        }

                    })
                }
            });
        });
    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator !!}
@endsection