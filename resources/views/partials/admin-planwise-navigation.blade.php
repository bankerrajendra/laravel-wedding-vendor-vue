
<span id="pull-left">
    @foreach($packages as $package)
        <a href="{{ route('planwise-members', ['duration' => $package->duration, 'type' => $package->type]) }}">
            <button class="btn @if($duration == $package->duration && $type == $package->type) btn-info @else btn-default  @endif "><i class="fa fa-users"></i> {{$package->duration}} {{$package->type}}({{$package->count}})</button>
        </a>
    @endforeach
</span>
 