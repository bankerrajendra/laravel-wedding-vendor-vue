<div class="col-sm-4 panel-body hidden-xs">
    <div class="list-group sidebar">@php $unread_notification_counter = get_notification_counter(); @endphp
        <li class="list-group-item">Manage Settings</li>
        <a href="{{route('settings')}}" class="list-group-item {{ Request::is('account/setting') ? 'active' : null }}">Setting</a>
        <a href="{{route('notification')}}" class="list-group-item {{ Request::is('account/notification') ? 'active' : null }}">Notification @if($unread_notification_counter['flag_notif_cnt']>0) <span class="badge text-right">{{$unread_notification_counter['flag_notif_cnt']}}</span>@endif</a>
        <a href="{{route('private-photo-permission')}}" class="list-group-item {{ Request::is('account/sharePhotoPermission', 'account/privatePhotoPermission') ? 'active' : null }}">Photo Permisssions @if($unread_notification_counter['private_photo_req_cnt']>0) <span class="badge text-right">{{$unread_notification_counter['private_photo_req_cnt']}}</span>@endif</a>
        <a href="{{route('blocked-members')}}" class="list-group-item {{ Request::is('account/blockedMembers') ? 'active' : null }}">Blocked Members</a>
        <a href="{{route('hidden-profiles')}}" class="list-group-item {{ Request::is('account/hiddenProfiles') ? 'active' : null }}">Hidden Members</a>
        <a href="{{route('shortlisted-profiles')}}" class="list-group-item {{ Request::is('account/shortlistedProfiles') ? 'active' : null }}">Shortlisted Profiles</a>
        @if($site_membership_status == 1)
            <a href="{{route('my-membership')}}" class="list-group-item {{ Request::is('account/my-membership') ? 'active' : null }}">My Membership</a>
            <a href="{{route('paid-membership')}}" class="list-group-item {{ Request::is('paid-membership') ? 'active' : null }}">Upgrade Membership</a>
        @endif
        <a href="{{route('all-reviews')}}" class="list-group-item">All Reviews</a>
    </div>
</div>