@if(Auth::check())

	@if(Auth::user()->roles[0]->name=="Vendor" || Auth::user()->roles[0]->name=="Admin")

	{{--Vendor header Starts here--}}
		<!--mobile header-->

		@if( Request::is('account/*'))

			<nav class="navbar navbar-inverse navbar-fixed-top hidden-lg setting-nav">
				<div class="container">
					<div class="row">
						<div class="col-xs-2">
							<a href="{{route('vendor-business-mobile')}}"><h3><i class="fa fa-chevron-left" aria-hidden="true"></i></h3></a>
						</div>
						<div class="col-xs-10 text-center">
							<h4>@if( Request::is('account/vendor-business-info'))
									Business Information
								@elseif( Request::is('account/vendor-faqs'))
									Frequently Asked Questions
								@elseif( Request::is('account/vendor-photo-upload'))
									Banner Upload
								@elseif( Request::is('account/vendor-video'))
									Upload Videos
								@elseif( Request::is('account/vendor-showcase'))
									Showcase
								@elseif( Request::is('account/vendor-collect-reviews'))
									Collect Reviews
								@elseif( Request::is('account/vendor-reviews'))
									Reviews
								@else
									Manage Account
								@endif
							</h4>
						</div>
					</div>
				</div>
			</nav>
		@elseif(Request::is('settings/*'))
			<nav class="navbar navbar-inverse navbar-fixed-top hidden-lg setting-nav">
				<div class="container">
					<div class="row">
						<div class="col-xs-2">
							<a href="{{route('vendor-setting-mobile')}}"><h3><i class="fa fa-chevron-left" aria-hidden="true"></i></h3></a>
						</div>
						<div class="col-xs-10 text-center">
							<h4>@if( Request::is('settings/vendor-change-password'))
									Change Password
								@elseif( Request::is('settings/vendor-notification') )
									Notification
								@elseif( Request::is('settings/my-membership') )
									@if($site_membership_status == 1)My Membership @endif
								@elseif( Request::is('settings/disable-account') )
									Delete/Deactivate Account
								@elseif( Request::is('settings/deactivate-account') )
									Deactivate Account
								@elseif( Request::is('settings/vendor-delete-account') )
									Delete Account
								@endif
							</h4>
						</div>
					</div>
				</div>
			</nav>
		@else

		<nav class="navbar navbar-default hidden-lg {{ Request::is('conversation/*', 'filter-conversation/*', 'archive-conversation/*','trash-conversation/*') ? 'hidden-xs' : '' }}  navbar-fixed-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-2" style="padding-top:12px;">
						<span style="cursor:pointer;" onclick="javascript:openNav()"><img src="{{asset('img/menu.png')}}"></span>
					</div>
					<div id="mySidenav" class="sidenav">
						<div class="Sidemenu">
							<li><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a></li>
							<li><a href="{{config('app.url')}}"><img src="{{asset('img/m-logo.png')}}"></a></li>
							<li><a href="{{ route('vendor-business-mobile') }}"><i class="fa fa-users" aria-hidden="true"></i> Business Profile</a></li>
							@if($site_membership_status == 1)<li><a href="{{route('paid-membership')}}"><i class="fa fa-credit-card" aria-hidden="true"></i> My Membership</a></li>@endif
							<li><a href="{{ route('vendor-business-info') }}"><i class="fa fa-user" aria-hidden="true"></i> Manage Account</a></li>
							@if(Auth::user()->roles[0]->name=="Vendor")<li><a href="{{ Auth::user()->getVendorProfileLink() }}"><i class="fa fa-eye" aria-hidden="true"></i> Preview Listing</a></li>@endif
							<li><a href="{{ route('vendor-change-password') }}"><i class="fa fa-cog" aria-hidden="true"></i> Setting</a></li>
							<li><a href="{{route('inbox')}}"><i class="fa fa-envelope" aria-hidden="true"></i> Message</a></li>
							{{--<li><a href="#demo2"><i class="fa fa-heart" aria-hidden="true"></i> Activity</a></li>--}}
							<li><a href="{{ route('vendor-notification') }}"><i class="fa fa-bell-o" aria-hidden="true"></i> Notification</a></li>
							<li><a href="{{ route('vendor-reviews') }}"><i class="fa fa-pencil" aria-hidden="true"></i> Reviews</a></li>
							<li><a href="{{ route('logout') }}"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li>
						</div>
					</div>
					<div class="col-xs-10 text-center">
						<ul class="nav navbar mobile_nav navbar-right pull-right">
							<li class="pull-left"><a href="{{ route('inbox') }}" class="user-icon call"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
							<li class="pull-left"><a href="{{ route('vendor-business-mobile') }}" class="user-icon call"><i class="fa fa-user" aria-hidden="true"></i></a></li>
							<li class="pull-left"><a href="{{ route('vendor-change-password') }}" class="call"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		@endif
		<!--End mobile header-->

		<div class="container hidden-xs">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="{{config('app.url')}}" style="padding: 10px 15px;"><img src="{{ asset('img/inner-logo.png') }}"></a>
					</div>
					<ul class="nav navbar-nav" style="padding-left:5%;">
						<li><a href="{{route('inbox')}}"><i class="fa fa-envelope" aria-hidden="true"></i> MESSAGES</a></li>
						{{--<li><a href="#"><i class="fa fa-heart" aria-hidden="true"></i> ACTIVITY</a></li>--}}
						<li><a href="{{ route('vendor-notification') }}"><i class="fa fa-bell" aria-hidden="true"></i> NOTIFICATION</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						@if($site_membership_status == 1)<li><a href="{{route('paid-membership')}}" style="background: #000;color: #fff !important;border-radius: 4px;padding: 10px 11px;margin: 15px 0px;">Upgrade Now</a></li>@endif
						<li class="dropdown">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown"><img src="{{Auth::user()->getVendorProfilePic(true)}}" alt="" class="img-thumbnail" style="max-width: 50px">&nbsp;&nbsp;&nbsp;{{ Auth::user()->first_name }}&nbsp;&nbsp;&nbsp;<span class="caret"></span></a>
							<ul class="dropdown-menu" style="display: none;">
								@if($site_membership_status == 1)<a href="{{route('paid-membership')}}" class="btn btn-default btn-block">Upgrade Now</a>@endif
								<p class="text-center">Chat, message and connect instantly!</p>
								<div class="divider"></div>
								<li><a href="{{ route('vendor-business-info') }}"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
								<li><a href="{{ route('vendor-change-password') }}"><i class="fa fa-cog" aria-hidden="true"></i>Settings</a></li>
								{{--<li><a href="{{route('cmsroots', ['slug'=>'faqs'])}}"><i class="fa fa-lock" aria-hidden="true"></i>Help</a></li>--}}
								<li><a href="{{ route('logout') }}"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
								<div style="clear:both;"></div>
								@if($site_membership_status == 1)
									{{-- <hr>
									<li class="text-center"><a href="{{route('paid-membership')}}">Diamond Membership</a></li> --}}
								@endif
							</ul>
						</li>
					</ul>


				</div>
			</nav>
		</div>
		{{--Vendor header ends here--}}

	@elseif(Auth::user()->roles[0]->name=="User" || Auth::user()->roles[0]->name=="Unverified")
		<!--mobile header-->
		@if( Request::is('myprofile/*'))

			<nav class="navbar navbar-inverse navbar-fixed-top hidden-lg setting-nav">
				<div class="container">
					<div class="row">
						<div class="col-xs-2">
							<a href="{{route('user-profile-mobile')}}"><h3><i class="fa fa-chevron-left" aria-hidden="true"></i></h3></a>
						</div>
						<div class="col-xs-10 text-center">
							<h4>@if( Request::is('myprofile/user-manage-profile'))
									Manage Profile
								@elseif( Request::is('myprofile/user-manage-photo') )
									Manage Photo 
								@endif
							</h4>
						</div>
					</div>
				</div>
			</nav>
		@elseif( Request::is('settings/*'))

			<nav class="navbar navbar-inverse navbar-fixed-top hidden-lg setting-nav">
				<div class="container">
					<div class="row">
						<div class="col-xs-2">
							<a href="{{route('user-setting-mobile')}}"><h3><i class="fa fa-chevron-left" aria-hidden="true"></i></h3></a>
						</div>
						<div class="col-xs-10 text-center">
							<h4>@if( Request::is('settings/user-notification'))
									Notification
								@elseif( Request::is('settings/shortlisted-vendors') )
									Shortlisted Vendors
								@elseif( Request::is('settings/finalized-vendors') )
									Finalized Vendors
								@elseif( Request::is('settings/disable-account') )
									Delete/Deactivate Account
								@elseif( Request::is('settings/deactivate-account') )
									Deactivate Account
								@elseif( Request::is('settings/delete-account') )
									Delete Account
								@elseif( Request::is('settings/user-change-password') )
									Change Password
								@endif
							</h4>
						</div>
					</div>
				</div>
			</nav>
		@else
		<nav class="navbar navbar-default hidden-lg {{ Request::is('conversation/*', 'filter-conversation/*', 'archive-conversation/*','trash-conversation/*') ? 'hidden-xs' : '' }} navbar-fixed-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-2" style="padding-top:12px;">
						<span style="cursor:pointer;" onclick="openNav()"><img src="{{asset('img/menu.png')}}"></span>
					</div>
					<div id="mySidenav" class="sidenav">
						<div class="Sidemenu">
							<li><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a></li>
							<li><a href="{{config('app.url')}}"><img src="img/m-logo.png"></a></li>
							<li><a href="{{route('muslim-vendors')}}"><i class="fa fa-search" aria-hidden="true"></i> Search</a></li>
							<li><a href="{{route('inbox')}}"><i class="fa fa-envelope" aria-hidden="true"></i> Message</a></li>
							<li><a href="{{ route('user-notification') }}"><i class="fa fa-bell-o" aria-hidden="true"></i> Notification</a></li>
							<li><a href="{{route('user-manage-profile')}}"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
							<li><a href="{{route('user-change-password')}}"><i class="fa fa-cog" aria-hidden="true"></i> Setting</a></li>
							<li><a href="{{ route('logout') }}"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li>
						</div>
					</div>
					<div class="col-xs-10 text-center">
						<ul class="nav navbar mobile_nav navbar-right pull-right">
							<li class="pull-left"><a href="{{ route('inbox') }}" class="user-icon call"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
							<li class="pull-left"><a href="{{ route('user-manage-profile') }}" class="user-icon call"><i class="fa fa-user" aria-hidden="true"></i></a></li>
							<li class="pull-left"><a href="{{ route('user-change-password') }}" class="call"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		@endif
		<!--End mobile header-->

		<div class="container hidden-xs">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="{{config('app.url')}}" style="padding: 10px 15px;"><img src="{{asset('img/inner-logo.png')}}"></a>
					</div>
					<ul class="nav navbar-nav" style="padding-left:5%;">
						<li><a href="{{route('inbox')}}"><i class="fa fa-envelope" aria-hidden="true"></i> MESSAGES</a></li>
						<li><a href="{{asset('muslim-vendors')}}"><i class="fa fa-search" aria-hidden="true"></i> SEARCH</a></li>
						<li><a href="{{ route('user-notification') }}"><i class="fa fa-bell" aria-hidden="true"></i> NOTIFICATION</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a class="dropdown-toggle" type="button" data-toggle="dropdown"><img src="{{Auth::user()->getVendorProfilePic(true)}}" alt="" class="img-thumbnail" style="max-width: 50px">&nbsp;&nbsp;&nbsp;{{ Auth::user()->first_name }}&nbsp;&nbsp;&nbsp;<span class="caret"></span></a>
							<ul class="dropdown-menu" style="display: none;">
								<p class="text-center">Chat, message and connect instantly!</p>
								<div class="divider"></div>
								<li><a href="{{route('user-manage-profile')}}"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
								<li><a href="{{route('user-change-password')}}"><i class="fa fa-cog" aria-hidden="true"></i>Settings</a></li>
								{{--<li><a href="{{route('cmsroots', ['slug'=>'faqs'])}}"><i class="fa fa-lock" aria-hidden="true"></i>Help</a></li>--}}
								<li><a href="{{route('logout')}}"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
								<div style="clear:both;"></div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	@endif
	
@else
	<!-- Start mobile header-->
	<nav class="navbar navbar-default hidden-lg navbar-fixed-top mobile-nav">
		<div class="container">
			<div class="row">
				<div class="col-xs-2" style="padding-top:9px;">
					<span style="cursor:pointer;" onclick="openNav()"><img src="{{asset('img/menu.png')}}"></span>
				</div>
				<div id="mySidenav" class="sidenav">
					<div class="Sidemenu">
						<li><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a></li>
						<li><a href="{{config('app.url')}}"><img src="{{asset('img/m-logo.png')}}"></a></li>
						<li><a href="#myAccntMobMenu" data-toggle="collapse">My Account <i class="fa fa-angle-down pull-right"></i></a></li>
						<div id="myAccntMobMenu" class="collapse">
							<li><a href="{{route('vendor-signup')}}">Vendor Signup</a></li>
							<li><a href="{{route('user-signup')}}">User Signup</a></li>
							<li><a href="{{route('login')}}">Login</a></li>
							<li><a href="{{route('password.request')}}">Forgot Password</a></li>
						</div>
						<li><a href="{{ route('request-a-quote') }}">Connect With Vendors</a></li>
						<li><a href="{{route('muslim-vendors')}}">Search</a></li>
						<li><a href="{{ route('event-landing') }}">Events</a></li>
						<li><a href="{{route('vendor-event-signup')}}">Post an event</a></li>
						<li><a href="{{route('search-vendor', ['category' => 'marriage-bureau'])}}">Marriage Bureau</a></li>
						@if($site_satellites_lists->count() > 0)
						<li><a href="#MobMenuOurSats" data-toggle="collapse">Visit Our Sites <i class="fa fa-angle-down pull-right"></i></a></li>
						<div id="MobMenuOurSats" class="collapse">
							<li><a onclick="openInNewTab('punjabiwedding','s','www.');" >PunjabiWedding.com</a></li>
							<li><a onclick="openInNewTab('marriagematrimony','s','www.');" >MarriageMatrimony.com</a></li>
							<li><a onclick="openInNewTab('muslimwedding','s','www.');" >MuslimWedding.com</a></li>
						</div>
						@endif
						<li><a href="#MobMenuHlpSprt" data-toggle="collapse">Help &amp; Support <i class="fa fa-angle-down pull-right"></i></a></li>
						<div id="MobMenuHlpSprt" class="collapse">
							<li><a href="{{route('cmsroots', ['slug'=>'aboutus'])}}">About Us</a></li>
							<li><a href="{{route('contact')}}">Contact</a></li>
						</div>
						<li><a href="#MobMnLrnMr" data-toggle="collapse">Learn More<i class="fa fa-angle-down pull-right"></i></a></li>
						<div id="MobMnLrnMr" class="collapse">
							{{--<li><a href="{{route('cmsroots', ['slug'=>'faqs'])}}">FAQ's</a></li>--}}
							<li><a href="{{route('sitemap')}}">Site Map</a></li>
							<li><a href="{{route('cmsroots', ['slug'=>'term-condition'])}}">Terms of Use</a></li>
							<li><a href="{{route('cmsroots', ['slug'=>'privacy'])}}">Privacy Policy</a></li>
						</div>
						<ul class="social">
							<li class="fb"><a href="{{getGeneralSiteSetting('site_facebook_link')}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li class="tw"><a href="{{getGeneralSiteSetting('site_twitter_link')}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li class="lin"><a href="{{getGeneralSiteSetting('site_linkedin_link')}}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							<li class="youtube"><a href="{{getGeneralSiteSetting('site_youtube_link')}}" target="_blank"><i class="fa fa-youtube"></i></a></li>
							<li class="insta"><a href="{{getGeneralSiteSetting('site_instagram_link')}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-xs-7 nopadding text-center">
					<a class="mobile-navbar-brand" href="{{config('app.url')}}"><img src="{{asset('img/logo.png')}}" alt="Logo" class="img-responsive"></a>
				</div>
				<div class="col-xs-3 text-right">
					<a href="{{route('login')}}" class="btn btn-danger btn-block">LOGIN</a>
				</div>
			</div>
		</div>
	</nav>
	<!--End mobile header-->
	<div class="top-header hidden-xs">
		<div class="container text-right">
			<a href="{{route('contact')}}">Contact Us</a> |
			<a href="{{route('vendor-event-signup')}}">Post An Event</a> | <a href="{{route('vendor-signup')}}">Free Business Listing</a> | <a href="{{route('blogs')}}">Blog</a>
		</div>
	</div>
	<div class="container hidden-xs">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="{{route('welcome')}}"><img src="{{asset('img/logo.png')}}" style="width:200px;"></a>
			</div>
			<ul class="nav navbar-nav" style="padding-left:0%;">
				<li class="dropdown dropdown-large">
					<a href="{{route('muslim-vendors')}}" class="dropdown-toggle disabled" data-toggle="dropdown"> WEDDING VENDORS</a>
					<ul class="dropdown-menu dropdown-menu-large row" style="display: none;">
                        <?php
                        $c=0;
                        $categoriesMn=menuVendorCategories();
                        foreach($categoriesMn as $category) {
                            $catArr[$c]['name'] = $category->name;
							$catArr[$c]['slug'] = $category->slug;
							$c++;
                        }
                        $l=0;
                        $totCat=$categoriesMn->count();

                        if($totCat > 0) {
							$noRows= ceil($totCat/4);
							for($i=0;$i<4; $i++) { ?>
								<li class="col-sm-3">
									<ul>
									<?php for($j=0;$j<$noRows;$j++)  {
										if($l==$c){ break;} ?>
											<li><a href="{{route('search-vendor', ['category' => $catArr[$l]['slug']])}}">{{$catArr[$l]['name']}}</a></li>
										<?php
										$l++;
									} ?>
									</ul>
								</li>
							<?php
							}
                        }
                        ?>

					</ul>
				</li>
				<li><a href="{{ route('event-landing') }}">EVENTS</a></li>
				<li><a href="{{route('search-vendor', ['category' => 'marriage-bureau'])}}">MARRIAGE BUREAU</a></li>
				<li><a href="{{ route('request-a-quote') }}">CONNECT WITH VENDORS</a></li>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				@if(Auth::check())
					<li><a href="{{route('logout')}}">LOGOUT</a></li>
				@else
					<li><a href="{{route('vendor-signup')}}"><i class="fa fa-user"></i> VENDOR SIGN UP</a></li>
					<li><a href="{{route('login')}}">LOGIN NOW</a></li>
				@endif
			</ul>
			</div>
		</nav>
	</div>
@endif
