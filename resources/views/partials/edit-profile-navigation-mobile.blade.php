<div class="settings hidden-lg hidden-md hidden-sm">
    <ul class="nav nav-tabs">
        <li class="@if(\Route::current()->getName() == 'edit-profile') active @endif"><a href="{{route('edit-profile')}}">Edit Profile</a></li>
        <li class="@if(\Route::current()->getName() == 'manage-photos') active @endif"><a href="{{route('manage-photos')}}">Manage Photos</a></li>
        <li class="@if(\Route::current()->getName() == 'my-profile') active @endif"><a href="{{route('my-profile')}}">My Profile</a></li>
    </ul>
</div>