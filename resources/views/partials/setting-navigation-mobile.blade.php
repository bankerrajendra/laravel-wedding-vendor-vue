<div class="settings hidden-lg hidden-md hidden-sm">
    <ul class="nav nav-tabs">
        <li class="@if(\Route::current()->getName() == 'setting') active @endif"><a href="{{route('setting')}}">My Account</a></li>
        {{--<li ><a href="#">Membership</a></li>--}}
        <li class="@if(\Route::current()->getName() == 'skipped-profiles') active @endif"><a href="{{route('skipped-profiles')}}">Skip Members</a></li>
        <li class="@if(\Route::current()->getName() == 'blocked-profiles') active @endif"><a href="{{route('blocked-profiles')}}">Block Members</a></li>
    </ul>
</div>