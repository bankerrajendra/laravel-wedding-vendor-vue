<?php $stats=admin_user_counter(); ?>
<span id="pull-left">
    <a href="{{ route('vendors') }}">
        <button class="btn @if(\Route::current()->getName() == 'vendors') btn-info @else btn-default  @endif "><i class="fa fa-users"></i> All vendors({{$stats['all_vendors']}})</button>
    </a>

    <a href="{{ route('active-vendors') }}">
        <button class="btn @if(\Route::current()->getName() == 'active-vendors') btn-info @else btn-default  @endif "><i class="fa fa-check"></i> Active users({{$stats['active_vendors']}})</button>
    </a>
    <a href="{{ route('inactive-vendors') }}">
        <button class="btn  @if(\Route::current()->getName() == 'inactive-vendors') btn-info @else btn-default  @endif " ><i class="fa fa-remove"></i> Inactive({{$stats['inactive_vendors']}})</button>
    </a>
    <a href="{{ route('incomplete-vendors') }}">
        <button class="btn  @if(\Route::current()->getName() == 'incomplete-vendors') btn-info @else btn-default  @endif " ><i class="fa fa-remove"></i> Incomplete({{$stats['incomplete_vendors']}})</button>
    </a>

    <a href="{{ route('banned-vendors') }}">
        <button class="btn  @if(\Route::current()->getName() == 'banned-vendors') btn-info @else btn-default  @endif "><i class="fa fa-ban"></i> Banned users({{$stats['banned_vendors']}})</button>
    </a>

    <a href="{{ route('deactivated-vendors') }}">
        <button class="btn  @if(\Route::current()->getName() == 'deactivated-vendors') btn-info @else btn-default  @endif "><i class="fa fa-warning"></i> Deactivated users({{$stats['deactivated_vendors']}})</button>
    </a>

    <a href="{{route('deleted.index')}}">
        <button class="btn  @if(\Route::current()->getName() == 'deleted.index') btn-info @else btn-default  @endif "><i class="fa fa-remove"></i> Deleted users({{$stats['deleted_vendors']}})</button>
    </a>

    <a href="{{route('latest-updates-vendors')}}">
        <button class="mrgn-top-5 btn  @if(\Route::current()->getName() == 'latest-updates-vendors') btn-info @else btn-default  @endif "><i class="fa fa-check-square"></i> Latest Updates({{$stats['latest_updates_vendors']}})</button>
    </a>
	

</span>
 