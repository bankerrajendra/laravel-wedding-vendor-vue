<div class="list-group">
    <a href="{{ route('vendor-business-info') }}" class="list-group-item {{ Request::is('account/vendor-business-info', 'account/vendor-business-info/*') ? 'active' : '' }}">About Your Business</a>
    <a href="{{ route('vendor-faqs') }}" class="list-group-item {{ Request::is('account/vendor-faqs', 'account/vendor-faqs/*') ? 'active' : '' }}">Faqs</a>
    <a href="{{ route('vendor-photo-upload') }}" class="list-group-item {{ Request::is('account/vendor-photo-upload', 'account/vendor-photo-upload/*') ? 'active' : '' }}">Photo Upload</a>
    <a href="{{ route('vendor-video') }}" class="list-group-item {{ Request::is('account/vendor-video', 'account/vendor-video/*') ? 'active' : '' }}">Video</a>
    <a href="{{ route('vendor-showcase') }}" class="list-group-item {{ Request::is('account/vendor-showcase', 'account/vendor-showcase/*') ? 'active' : '' }}">Showcase</a>
    <a href="{{ route('vendor-collect-reviews') }}" class="list-group-item {{ Request::getRequestUri() == '/account/vendor-collect-reviews' ? 'active' : '' }}">Collect Review</a>
    <a href="{{ route('vendor-reviews') }}" class="list-group-item {{ Request::is('account/vendor-reviews', 'account/vendor-reviews/*') ? 'active' : '' }}">Reviews</a>
    <a href="{{ route('vendor-collect-reviews') }}?type=event" class="list-group-item {{ Request::getRequestUri() == '/account/vendor-collect-reviews?type=event' ? 'active' : '' }}">Event Collect Review</a>
    {{-- Show Vendors Events --}}
    <a href="{{route('add-event')}}" class="list-group-item {{ Request::is('add-event') ? 'active' : '' }}"><i class="fa fa-plus" aria-hidden="true"></i> Post New Event</a>
    <a href="javascript:void(0);" class="list-group-item {{ Request::is('event/edit/*') ? 'active' : '' }}">Events <i class="fa fa-angle-double-down"></i></a>
    @if($events != null)
        @foreach ($events as $event)
            <a href="{{$event->getEditLink()}}" class="list-group-item @if(Request::url() == $event->getEditLink()) active @endif }}"><i class="fa fa-angle-right"></i> {{$event->event_name}}</a>
            <a class="list-group-item" style="padding:0px 0px 0px 30px" href="{{route('show-events-reviews', [$event->getEncryptedId()])}}"><small>View Reviews</small></a>
        @endforeach
    @endif
</div>