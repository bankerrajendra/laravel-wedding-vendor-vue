<div class="list-group">
    <a href="{{ route('vendor-change-password') }}" class="list-group-item {{ Request::is('settings/vendor-change-password', 'settings/vendor-change-password/*') ? 'active' : '' }}"><i class="fa fa-user" aria-hidden="true"></i> Change Password</a>
    <a href="{{ route('vendor-notification') }}" class="list-group-item {{ Request::is('settings/vendor-notification', 'settings/vendor-notification/*') ? 'active' : '' }}"><i class="fa fa-envelope" aria-hidden="true"></i> Notification</a>
    <a href="{{ route('my-membership') }}" class="list-group-item {{ Request::is('settings/my-membership', 'settings/my-membership/*') ? 'active' : '' }}"><i class="fa fa-credit-card" aria-hidden="true"></i> My Membership</a>
    <a href="{{ route('vendor-delete-account') }}" class="list-group-item {{ Request::is('settings/vendor-delete-account', 'settings/vendor-delete-account/*') ? 'active' : '' }}"><i class="fa fa-trash-o" aria-hidden="true"></i> Deactivate Account</a>
</div>