@extends('layouts.app')
@section('template_title')
    Messages
@endsection
@section('template_fastload_css')
@endsection
@section('navclasses')
    navbar-fixed-top
@endsection
@section('content')
    <div class="self-profile account-set">
        <div class="container">
            <div class="row">

                <div class="col-sm-4 col-sm-offset-4">
                    <div class="list-group mobile-sidebar">@php $unread_notification_counter = get_notification_counter(); @endphp
                        <li class="list-group-item text-center">Settings</li>
                        <a href="{{route('settings')}}" class="list-group-item">Account <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{route('notification')}}" class="list-group-item">Notification @if($unread_notification_counter['flag_notif_cnt']>0) <span class="badge">{{$unread_notification_counter['flag_notif_cnt']}}</span>  @endif<i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{route('private-photo-permission')}}" class="list-group-item">Photo Permisssions  @if($unread_notification_counter['private_photo_req_cnt']>0) <span class="badge">{{$unread_notification_counter['private_photo_req_cnt']}}</span>@endif<i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{route('blocked-members')}}" class="list-group-item">Blocked Members <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{route('hidden-profiles')}}" class="list-group-item">Hidden Members <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        <a href="{{route('shortlisted-profiles')}}" class="list-group-item">Shortlisted Profiles <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        @if($site_membership_status == 1)
                            <a href="{{route('my-membership')}}" class="list-group-item">My Membership <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                            <a href="{{route('paid-membership')}}" class="list-group-item">Upgrade Membership <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                        @endif
                        <a href="{{route('all-reviews')}}" class="list-group-item">All Reviews <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
@endsection