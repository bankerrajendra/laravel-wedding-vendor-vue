@if(!(Auth::user()->is_active()))
    <div class="text-center">
        <div class="alert alert-info">{{config('constants.account_inactive_text')}}</div>
    </div>
@endif
<?php $unread_count=get_inbox_unread_count();?>
<div class="col-sm-4 hidden-xs">
    <div class="list-group">
        {{--<li class="list-group-item">Message</li>--}}
        <a href="{{route('inbox')}}" class="list-group-item {{ (Request::is('inbox', 'inbox/*') || Request::routeIs('conversation')) ? 'active' : null }}  "><i class="fa fa-inbox"></i>&nbsp;&nbsp; Inbox @if($unread_count['inbox']>0) <span class="badge">{{$unread_count['inbox']}}</span>@endif </a>
        <a href="{{route('sent')}}" class="list-group-item {{ Request::is('sent', 'sent/*', 'conversation/*/sent') ? 'active' : null }} "><i class="fa fa-send" aria-hidden="true"></i>&nbsp;&nbsp; Sent  </a>
        <a href="{{route('archive')}}" class="list-group-item {{ Request::is('archive', 'archive/sendtoinbox/*', 'archive-conversation/*') ? 'active' : null }} "><i class="fa fa-archive" aria-hidden="true"></i>&nbsp;&nbsp; Archive @if($unread_count['archive']>0) <span class="badge text-right">{{$unread_count['archive']}}</span>@endif </a>
        <a href="{{route('trash')}}" class="list-group-item {{ Request::is('trash', 'trash/trashtoinbox/*', 'trash-conversation/*') ? 'active' : null }} "><i class=" fa fa-trash-o"></i>&nbsp;&nbsp; Trash</a>
    </div>
</div>