<div class="col-sm-4 hidden-xs panel-body">
    <div class="left-side-bar" id="sidenav" style="@yield('sidenav-styles')">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="">
                                <div class="row">
                                    <div class="list-group sidebar">
                                        <a href="{{route('public.home')}}" class="list-group-item @if(\Route::current()->getName() == 'public.home') active @endif"><i class="fa fa-handshake-o" aria-hidden="true"></i> <span>Connect</span></a>
                                        <a href="{{route('new-online')}}" class="list-group-item @if(\Route::current()->getName() == 'new-online') active @endif"><i class="fa fa-users" aria-hidden="true"></i> <span>New Users</span></a>
                                        <a href="{{route('search')}}" class="list-group-item @if(\Route::current()->getName() == 'search') active @endif"><i class="fa fa-search" aria-hidden="true"></i> <span> Search</span></a>
                                        <a href="{{route('inbox')}}" class="list-group-item @if(\Route::current()->getName() == 'inbox' || \Route::current()->getName() == 'conversation') active @endif"><i class="fa fa-envelope" aria-hidden="true"></i> <span>Messages</span>@if($notificationCount['messages'] > 0)<span class="badge">{{$notificationCount['messages']}}</span>@endif</a>
                                        {{--<a href="#" class="list-group-item "><i class="fa fa-users" aria-hidden="true"></i> <span>Matches</span></a>--}}
                                    </div>
                                    <hr>
                                    <div class="list-group sidebar">
                                        <a href="{{route('favourited-profiles')}}" class="list-group-item @if(\Route::current()->getName() == 'favourited-profiles') active @endif">My Favorites @if($notificationCount['myFavourites'] > 0)<span class="badge">{{$notificationCount['myFavourites']}}</span> @endif </a>
                                        <a href="{{route('who-favourited-me-profiles')}}" class="list-group-item @if(\Route::current()->getName() == 'who-favourited-me-profiles') active @endif">Who Favorited Me @if($notificationCount['favouritedMe'] > 0)<span class="badge">{{$notificationCount['favouritedMe']}}</span> @endif </a>
                                        <a href="{{route('viewed-me-profiles')}}" class="list-group-item @if(\Route::current()->getName() == 'viewed-me-profiles') active @endif">Viewed Me @if($notificationCount['viewedMe'] > 0)<span class="badge">{{$notificationCount['viewedMe']}}</span> @endif </a>
                                        <a href="{{route('shortlisted-profiles')}}" class="list-group-item @if(\Route::current()->getName() == 'shortlisted-profiles') active @endif">Shortlisted Profiles</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
