<div class="col-sm-4 panel-body hidden-xs">
    <div class="list-group sidebar">
        <li class="list-group-item">Manage Profile</li>
        <a href="{{ route('my-profile') }}" class="list-group-item @if(\Route::current()->getName() == 'my-profile') active @endif">Edit Personal Profile</a>
        <a href="{{ route('my-hobbies') }}" class="list-group-item @if(\Route::current()->getName() == 'my-hobbies') active @endif">Edit Hobbies & Interests</a>
        <a href="{{ route('partner-preference') }}" class="list-group-item @if(\Route::current()->getName() == 'partner-preference') active @endif">Edit Partner Profile</a>
        <a href="{{ route('my-photos') }}" class="list-group-item @if(\Route::current()->getName() == 'my-photos') active @endif">Manage Photo</a>
        <a href="{{ route('self-profile') }}" class="list-group-item @if(\Route::current()->getName() == 'self-profile') active @endif">View Profile</a>
    </div>
</div>