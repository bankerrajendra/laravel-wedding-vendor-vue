<?php $unread_count=get_inbox_unread_count();

?>
<div class="row hidden-md hidden-sm hidden-lg">
    <div class="col-sm-6 col-xs-6"><h4><a href="javascript:void(0);" data-toggle="collapse" data-target="#demo">All Messages <span class="caret"></span></a></h4></div>
    @if(Request::is('sent','sent/*'))
        <div class="col-sm-6 col-xs-6"><a href="javascript:void(0);" type="button" class="pull-right filter-icon" data-toggle="modal" data-target="#myModal"><i class="fa fa-filter" aria-hidden="true"></i></a></div>
    @endif
    @if(Request::is('inbox','inbox/*'))
        <div class="col-sm-6 col-xs-6"><div class="checkbox pull-right unread" style="display:inline;"><label><input type="checkbox" class="show_unread" id="show_unread" @if($showunread=="unread") checked @endif value=""> Show Unread</label> &nbsp;</div></div>
    @endif

    <div  class="col-xs-12 no-padding">
        <div id="demo" class="collapse msg-list">
            <ul>
                <li><a href="{{route('inbox')}}">Inbox @if($unread_count['inbox']>0) <span class="badge text-right">{{$unread_count['inbox']}}</span>@endif @if(Request::is('inbox', 'inbox/*') || Request::routeIs('conversation')) <i class="fa fa-check pull-right" aria-hidden="true"></i>@endif </a></li>
                <li><a href="{{route('sent')}}">Sent  @if(Request::is('sent', 'sent/*', 'conversation/*/sent')) <i class="fa fa-check pull-right" aria-hidden="true"></i>@endif </a></li>
                <li><a href="{{route('archive')}}">Archive @if($unread_count['archive']>0) <span class="badge text-right">{{$unread_count['archive']}}</span>@endif @if(Request::is('archive', 'archive/sendtoinbox/*', 'archive-conversation/*')) <i class="fa fa-check pull-right" aria-hidden="true"></i>@endif </a></li>
                <li><a href="{{route('trash')}}">Trash @if(Request::is('trash', 'trash/trashtoinbox/*', 'trash-conversation/*')) <i class="fa fa-check pull-right" aria-hidden="true"></i>@endif</a></li>
            </ul>
        </div>
    </div>
</div>