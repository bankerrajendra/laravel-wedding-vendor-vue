<br>
<ul class="nav nav-tabs hidden-xs">
    <li class="@if(\Route::current()->getName() == 'setting') active @endif"><a href="{{route('setting')}}">My Account</a></li>
    {{--<li ><a href="#">Membership</a></li>--}}
    <li class="@if(\Route::current()->getName() == 'skipped-profiles') active @endif"><a href="{{route('skipped-profiles')}}">Skip Members</a></li>
    <li class="@if(\Route::current()->getName() == 'blocked-profiles') active @endif"><a href="{{route('blocked-profiles')}}">Block Members</a></li>
    <li class="@if(\Route::current()->getName() == 'change-password') active @endif"><a href="{{route('change-password')}}">Change Password</a></li>
</ul>