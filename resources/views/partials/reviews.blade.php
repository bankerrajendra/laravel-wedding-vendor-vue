<div id="feedback"><a href="#" data-toggle="modal" data-target="#siteFeebackReviewModal"><i class="fa fa-star"></i> Reviews</a></div>
<div class="modal fade" id="siteFeebackReviewModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-body">
		<div class="row">
			<div class="col-sm-12">
				<button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 class="pull-left">Reviews</h3>
			</div>
		</div>
		@if($reviews->count() > 0)
			@foreach ($reviews as $review)
			<div class="media">
				<a class="pull-left" href="{{$review->user->getVendorProfileLink()}}">
					<img class="media-object img-circle" src="{{$review->user->getProfilePicForGuest()}}" width="80" height="80" alt="">
				</a>
				<h4 class="media-heading">{{$review->user->first_name}} {{$review->user->last_name}}</h4>
				<ul class="list-inline list-unstyled">
					<li>
						@for ($i = 1; $i <= 5; $i++)
							@php
								$star_state = 'fa-star-o';
								if($review->rating >= $i) :
									$star_state = 'fa-star';
								endif
							@endphp
							<span class="fa {{$star_state}}"></span>	
						@endfor
					</li>
					<li>|</li>
				<li><span><i class="glyphicon glyphicon-calendar"></i> {{$review->created_at->diffForHumans()}} </span></li>
				</ul>
			<p>{{$review->message}}</p>
			</div>
			@endforeach
		<h5><a href="{{route('reviews')}}" class="read_more">See All </a></h5>
		@else
			No record found.
		@endif
	  </div>
	</div>
  </div>
</div>