<option value="Pacific/Midway" @if($site_timezone == "Pacific/Midway") selected="selected" @endif>(UTC-11:00) Midway Island</option>
<option value="Pacific/Samoa" @if($site_timezone == "Pacific/Samoa") selected="selected" @endif>(UTC-11:00) Samoa</option>
<option value="Pacific/Honolulu" @if($site_timezone == "Pacific/Honolulu") selected="selected" @endif>(UTC-10:00) Hawaii</option>
<option value="US/Alaska" @if($site_timezone == "US/Alaska") selected="selected" @endif>(UTC-09:00) Alaska</option>
<option value="America/Los_Angeles" @if($site_timezone == "America/Los_Angeles") selected="selected" @endif>(UTC-08:00) Pacific Time (US &amp; Canada)</option>
<option value="America/Tijuana" @if($site_timezone == "America/Tijuana") selected="selected" @endif>(UTC-08:00) Tijuana</option>
<option value="US/Arizona" @if($site_timezone == "US/Arizona") selected="selected" @endif>(UTC-07:00) Arizona</option>
<option value="America/Chihuahua" @if($site_timezone == "America/Chihuahua") selected="selected" @endif>(UTC-07:00) La Paz</option>
<option value="America/Mazatlan" @if($site_timezone == "America/Mazatlan") selected="selected" @endif>(UTC-07:00) Mazatlan</option>
<option value="US/Mountain" @if($site_timezone == "US/Mountain") selected="selected" @endif>(UTC-07:00) Mountain Time (US &amp; Canada)</option>
<option value="America/Managua" @if($site_timezone == "America/Managua") selected="selected" @endif>(UTC-06:00) Central America</option>
<option value="US/Central" @if($site_timezone == "US/Central") selected="selected" @endif>(UTC-06:00) Central Time (US &amp; Canada)</option>
<option value="America/Mexico_City" @if($site_timezone == "America/Mexico_City") selected="selected" @endif>(UTC-06:00) Mexico City</option>
<option value="America/Monterrey" @if($site_timezone == "America/Monterrey") selected="selected" @endif>(UTC-06:00) Monterrey</option>
<option value="Canada/Saskatchewan" @if($site_timezone == "Canada/Saskatchewan") selected="selected" @endif>(UTC-06:00) Saskatchewan</option>
<option value="America/Bogota" @if($site_timezone == "America/Bogota") selected="selected" @endif>(UTC-05:00) Quito</option>
<option value="US/Eastern" @if($site_timezone == "US/Eastern") selected="selected" @endif>(UTC-05:00) Eastern Time (US &amp; Canada)</option>
<option value="US/East-Indiana" @if($site_timezone == "US/East-Indiana") selected="selected" @endif">(UTC-05:00) Indiana (East)</option>
<option value="America/Lima" @if($site_timezone == "America/Lima") selected="selected" @endif>(UTC-05:00) Lima</option>
<option value="Canada/Atlantic" @if($site_timezone == "Canada/Atlantic") selected="selected" @endif>(UTC-04:00) Atlantic Time (Canada)</option>
<option value="America/Caracas" @if($site_timezone == "America/Caracas") selected="selected" @endif>(UTC-04:30) Caracas</option>
<option value="America/La_Paz" @if($site_timezone == "America/La_Paz") selected="selected" @endif>(UTC-04:00) La Paz</option>
<option value="America/Santiago" @if($site_timezone == "America/Santiago") selected="selected" @endif>(UTC-04:00) Santiago</option>
<option value="Canada/Newfoundland" @if($site_timezone == "Canada/Newfoundland") selected="selected" @endif>(UTC-03:30) Newfoundland</option>
<option value="America/Sao_Paulo" @if($site_timezone == "America/Sao_Paulo") selected="selected" @endif>(UTC-03:00) Brasilia</option>
<option value="America/Argentina/Buenos_Aires" @if($site_timezone == "America/Argentina/Buenos_Aires") selected="selected" @endif">(UTC-03:00) Georgetown</option>
<option value="America/Godthab" @if($site_timezone == "America/Godthab") selected="selected" @endif>(UTC-03:00) Greenland</option>
<option value="America/Noronha" @if($site_timezone == "America/Noronha") selected="selected" @endif>(UTC-02:00) Mid-Atlantic</option>
<option value="Atlantic/Azores" @if($site_timezone == "Atlantic/Azores") selected="selected" @endif>(UTC-01:00) Azores</option>
<option value="Atlantic/Cape_Verde" @if($site_timezone == "Atlantic/Cape_Verde") selected="selected" @endif>(UTC-01:00) Cape Verde Is.</option>
<option value="Africa/Casablanca" @if($site_timezone == "Africa/Casablanca") selected="selected" @endif>(UTC+00:00) Casablanca</option>
<option value="Europe/London" @if($site_timezone == "Europe/London") selected="selected" @endif>(UTC+00:00) London</option>
<option value="Etc/Greenwich" @if($site_timezone == "Etc/Greenwich") selected="selected" @endif>(UTC+00:00) Greenwich Mean Time : Dublin</option>
<option value="Europe/Lisbon" @if($site_timezone == "Europe/Lisbon") selected="selected" @endif>(UTC+00:00) Lisbon</option>
<option value="Africa/Monrovia" @if($site_timezone == "Africa/Monrovia") selected="selected" @endif>(UTC+00:00) Monrovia</option>
<option value="UTC" @if($site_timezone == "UTC") selected="selected" @endif>(UTC+00:00) UTC</option>
<option value="Europe/Amsterdam" @if($site_timezone == "Europe/Amsterdam") selected="selected" @endif>(UTC+01:00) Amsterdam</option>
<option value="Europe/Belgrade" @if($site_timezone == "Europe/Belgrade") selected="selected" @endif>(UTC+01:00) Belgrade</option>
<option value="Europe/Berlin" @if($site_timezone == "Europe/Berlin") selected="selected" @endif>(UTC+01:00) Bern</option>
<option value="Europe/Bratislava" @if($site_timezone == "Europe/Bratislava") selected="selected" @endif>(UTC+01:00) Bratislava</option>
<option value="Europe/Brussels" @if($site_timezone == "Europe/Brussels") selected="selected" @endif>(UTC+01:00) Brussels</option>
<option value="Europe/Budapest" @if($site_timezone == "Europe/Budapest") selected="selected" @endif>(UTC+01:00) Budapest</option>
<option value="Europe/Copenhagen" @if($site_timezone == "Europe/Copenhagen") selected="selected" @endif>(UTC+01:00) Copenhagen</option>
<option value="Europe/Ljubljana" @if($site_timezone == "Europe/Ljubljana") selected="selected" @endif>(UTC+01:00) Ljubljana</option>
<option value="Europe/Madrid" @if($site_timezone == "Europe/Madrid") selected="selected" @endif>(UTC+01:00) Madrid</option>
<option value="Europe/Paris" @if($site_timezone == "Europe/Paris") selected="selected" @endif>(UTC+01:00) Paris</option>
<option value="Europe/Prague" @if($site_timezone == "Europe/Prague") selected="selected" @endif>(UTC+01:00) Prague</option>
<option value="Europe/Rome" @if($site_timezone == "Europe/Rome") selected="selected" @endif>(UTC+01:00) Rome</option>
<option value="Europe/Sarajevo" @if($site_timezone == "Europe/Sarajevo") selected="selected" @endif>(UTC+01:00) Sarajevo</option>
<option value="Europe/Skopje" @if($site_timezone == "Europe/Skopje") selected="selected" @endif>(UTC+01:00) Skopje</option>
<option value="Europe/Stockholm" @if($site_timezone == "Europe/Stockholm") selected="selected" @endif>(UTC+01:00) Stockholm</option>
<option value="Europe/Vienna" @if($site_timezone == "Europe/Vienna") selected="selected" @endif>(UTC+01:00) Vienna</option>
<option value="Europe/Warsaw" @if($site_timezone == "Europe/Warsaw") selected="selected" @endif>(UTC+01:00) Warsaw</option>
<option value="Africa/Lagos" @if($site_timezone == "Africa/Lagos") selected="selected" @endif>(UTC+01:00) West Central Africa</option>
<option value="Europe/Zagreb" @if($site_timezone == "Europe/Zagreb") selected="selected" @endif>(UTC+01:00) Zagreb</option>
<option value="Europe/Athens" @if($site_timezone == "Europe/Athens") selected="selected" @endif>(UTC+02:00) Athens</option>
<option value="Europe/Bucharest" @if($site_timezone == "Europe/Bucharest") selected="selected" @endif>(UTC+02:00) Bucharest</option>
<option value="Africa/Cairo" @if($site_timezone == "Africa/Cairo") selected="selected" @endif>(UTC+02:00) Cairo</option>
<option value="Africa/Harare" @if($site_timezone == "Africa/Harare") selected="selected" @endif>(UTC+02:00) Harare</option>
<option value="Europe/Helsinki" @if($site_timezone == "Europe/Helsinki") selected="selected" @endif>(UTC+02:00) Kyiv</option>
<option value="Europe/Istanbul" @if($site_timezone == "Europe/Istanbul") selected="selected" @endif>(UTC+02:00) Istanbul</option>
<option value="Asia/Jerusalem" @if($site_timezone == "Asia/Jerusalem") selected="selected" @endif>(UTC+02:00) Jerusalem</option>
<option value="Africa/Johannesburg" @if($site_timezone == "Africa/Johannesburg") selected="selected" @endif>(UTC+02:00) Pretoria</option>
<option value="Europe/Riga" @if($site_timezone == "Europe/Riga") selected="selected" @endif>(UTC+02:00) Riga</option>
<option value="Europe/Sofia" @if($site_timezone == "Europe/Sofia") selected="selected" @endif>(UTC+02:00) Sofia</option>
<option value="Europe/Tallinn" @if($site_timezone == "Europe/Tallinn") selected="selected" @endif>(UTC+02:00) Tallinn</option>
<option value="Europe/Vilnius" @if($site_timezone == "Europe/Vilnius") selected="selected" @endif>(UTC+02:00) Vilnius</option>
<option value="Asia/Baghdad" @if($site_timezone == "Asia/Baghdad") selected="selected" @endif>(UTC+03:00) Baghdad</option>
<option value="Asia/Kuwait" @if($site_timezone == "Asia/Kuwait") selected="selected" @endif>(UTC+03:00) Kuwait</option>
<option value="Europe/Minsk" @if($site_timezone == "Europe/Minsk") selected="selected" @endif>(UTC+03:00) Minsk</option>
<option value="Africa/Nairobi" @if($site_timezone == "Africa/Nairobi") selected="selected" @endif>(UTC+03:00) Nairobi</option>
<option value="Asia/Riyadh" @if($site_timezone == "Asia/Riyadh") selected="selected" @endif>(UTC+03:00) Riyadh</option>
<option value="Europe/Volgograd" @if($site_timezone == "Europe/Volgograd") selected="selected" @endif>(UTC+03:00) Volgograd</option>
<option value="Asia/Tehran" @if($site_timezone == "Asia/Tehran") selected="selected" @endif>(UTC+03:30) Tehran</option>
<option value="Asia/Muscat" @if($site_timezone == "Asia/Muscat") selected="selected" @endif>(UTC+04:00) Muscat</option>
<option value="Asia/Baku" @if($site_timezone == "Asia/Baku") selected="selected" @endif>(UTC+04:00) Baku</option>
<option value="Europe/Moscow" @if($site_timezone == "Europe/Moscow") selected="selected" @endif>(UTC+04:00) St. Petersburg</option>
<option value="Asia/Tbilisi" @if($site_timezone == "Asia/Tbilisi") selected="selected" @endif>(UTC+04:00) Tbilisi</option>
<option value="Asia/Yerevan" @if($site_timezone == "Asia/Yerevan") selected="selected" @endif>(UTC+04:00) Yerevan</option>
<option value="Asia/Kabul" @if($site_timezone == "Asia/Kabul") selected="selected" @endif>(UTC+04:30) Kabul</option>
<option value="Asia/Karachi" @if($site_timezone == "Asia/Karachi") selected="selected" @endif>(UTC+05:00) Karachi</option>
<option value="Asia/Tashkent" @if($site_timezone == "Asia/Tashkent") selected="selected" @endif>(UTC+05:00) Tashkent</option>
<option value="Asia/Calcutta" @if($site_timezone == "Asia/Calcutta") selected="selected" @endif>(UTC+05:30) Sri Jayawardenepura</option>
<option value="Asia/Kolkata" @if($site_timezone == "Asia/Kolkata") selected="selected" @endif>(UTC+05:30) Kolkata</option>
<option value="Asia/Katmandu" @if($site_timezone == "Asia/Katmandu") selected="selected" @endif>(UTC+05:45) Kathmandu</option>
<option value="Asia/Almaty" @if($site_timezone == "Asia/Almaty") selected="selected" @endif>(UTC+06:00) Almaty</option>
<option value="Asia/Dhaka" @if($site_timezone == "Asia/Dhaka") selected="selected" @endif>(UTC+06:00) Dhaka</option>
<option value="Asia/Yekaterinburg" @if($site_timezone == "Asia/Yekaterinburg") selected="selected" @endif>(UTC+06:00) Ekaterinburg</option>
<option value="Asia/Rangoon" @if($site_timezone == "Asia/Rangoon") selected="selected" @endif>(UTC+06:30) Rangoon</option>
<option value="Asia/Bangkok" @if($site_timezone == "Asia/Bangkok") selected="selected" @endif>(UTC+07:00) Hanoi</option>
<option value="Asia/Jakarta" @if($site_timezone == "Asia/Jakarta") selected="selected" @endif>(UTC+07:00) Jakarta</option>
<option value="Asia/Novosibirsk" @if($site_timezone == "Asia/Novosibirsk") selected="selected" @endif>(UTC+07:00) Novosibirsk</option>
<option value="Asia/Hong_Kong" @if($site_timezone == "Asia/Hong_Kong") selected="selected" @endif>(UTC+08:00) Hong Kong</option>
<option value="Asia/Chongqing" @if($site_timezone == "Asia/Chongqing") selected="selected" @endif>(UTC+08:00) Chongqing</option>
<option value="Asia/Krasnoyarsk" @if($site_timezone == "Asia/Krasnoyarsk") selected="selected" @endif>(UTC+08:00) Krasnoyarsk</option>
<option value="Asia/Kuala_Lumpur" @if($site_timezone == "Asia/Kuala_Lumpur") selected="selected" @endif>(UTC+08:00) Kuala Lumpur</option>
<option value="Australia/Perth" @if($site_timezone == "Australia/Perth") selected="selected" @endif>(UTC+08:00) Perth</option>
<option value="Asia/Singapore" @if($site_timezone == "Asia/Singapore") selected="selected" @endif>(UTC+08:00) Singapore</option>
<option value="Asia/Taipei" @if($site_timezone == "Asia/Taipei") selected="selected" @endif>(UTC+08:00) Taipei</option>
<option value="Asia/Ulan_Bator" @if($site_timezone == "Asia/Ulan_Bator") selected="selected" @endif>(UTC+08:00) Ulaan Bataar</option>
<option value="Asia/Urumqi" @if($site_timezone == "Asia/Urumqi") selected="selected" @endif>(UTC+08:00) Urumqi</option>
<option value="Asia/Irkutsk" @if($site_timezone == "Asia/Irkutsk") selected="selected" @endif>(UTC+09:00) Irkutsk</option>
<option value="Asia/Tokyo" @if($site_timezone == "Asia/Tokyo") selected="selected" @endif>(UTC+09:00) Tokyo</option>
<option value="Asia/Seoul" @if($site_timezone == "Asia/Seoul") selected="selected" @endif>(UTC+09:00) Seoul</option>
<option value="Australia/Adelaide" @if($site_timezone == "Australia/Adelaide") selected="selected" @endif>(UTC+09:30) Adelaide</option>
<option value="Australia/Darwin" @if($site_timezone == "Australia/Darwin") selected="selected" @endif>(UTC+09:30) Darwin</option>
<option value="Australia/Brisbane" @if($site_timezone == "Australia/Brisbane") selected="selected" @endif>(UTC+10:00) Brisbane</option>
<option value="Australia/Canberra" @if($site_timezone == "Australia/Canberra") selected="selected" @endif>(UTC+10:00) Canberra</option>
<option value="Pacific/Guam" @if($site_timezone == "Pacific/Guam") selected="selected" @endif>(UTC+10:00) Guam</option>
<option value="Australia/Hobart" @if($site_timezone == "Australia/Hobart") selected="selected" @endif>(UTC+10:00) Hobart</option>
<option value="Australia/Melbourne" @if($site_timezone == "Australia/Melbourne") selected="selected" @endif>(UTC+10:00) Melbourne</option>
<option value="Pacific/Port_Moresby" @if($site_timezone == "Pacific/Port_Moresby") selected="selected" @endif>(UTC+10:00) Port Moresby</option>
<option value="Australia/Sydney" @if($site_timezone == "Australia/Sydney") selected="selected" @endif>(UTC+10:00) Sydney</option>
<option value="Asia/Yakutsk" @if($site_timezone == "Asia/Yakutsk") selected="selected" @endif>(UTC+10:00) Yakutsk</option>
<option value="Asia/Vladivostok" @if($site_timezone == "Asia/Vladivostok") selected="selected" @endif>(UTC+11:00) Vladivostok</option>
<option value="Pacific/Auckland" @if($site_timezone == "Pacific/Auckland") selected="selected" @endif>(UTC+12:00) Wellington</option>
<option value="Pacific/Fiji" @if($site_timezone == "Pacific/Fiji") selected="selected" @endif>(UTC+12:00) Marshall Is.</option>
<option value="Pacific/Kwajalein" @if($site_timezone == "Pacific/Kwajalein") selected="selected" @endif>(UTC+12:00) International Date Line West</option>
<option value="Asia/Kamchatka" @if($site_timezone == "Asia/Kamchatka") selected="selected" @endif>(UTC+12:00) Kamchatka</option>
<option value="Asia/Magadan" @if($site_timezone == "Asia/Magadan") selected="selected" @endif>(UTC+12:00) Solomon Is.</option>
<option value="Pacific/Tongatapu" @if($site_timezone == "Pacific/Tongatapu") selected="selected" @endif>(UTC+13:00) Nuku'alofa</option>