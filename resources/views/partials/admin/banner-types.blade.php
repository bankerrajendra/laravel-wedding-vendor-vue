<select class="custom-select form-control" name="banner_type" id="banner_type" >
    <option value="">Select Banner Type</option>
    <option @if(isset($banner) && $banner->banner_type == "Free Membership") selected="selected" @endif value="Free Membership">Free Membership (350px * 500px)</option>
    <option @if(isset($banner) && $banner->banner_type == "Dashboard") selected="selected" @endif value="Dashboard">Dashboard (900px * 110px)</option>
    <option @if(isset($banner) && $banner->banner_type == "Search") selected="selected" @endif value="Search">Search (1000px * 160px)</option>
    <option @if(isset($banner) && $banner->banner_type == "View Profile") selected="selected" @endif value="View Profile">View Profile (263px * 263px)</option>
    <option @if(isset($banner) && $banner->banner_type == "Connect - 1") selected="selected" @endif value="Connect - 1">Connect (360px * 400px)</option>
    <option @if(isset($banner) && $banner->banner_type == "Connect - 2") selected="selected" @endif value="Connect - 2">Connect (360px * 510px)</option>
    <option @if(isset($banner) && $banner->banner_type == "Connect - 3") selected="selected" @endif value="Connect - 3">Connect (360px * 630px)</option>
</select>