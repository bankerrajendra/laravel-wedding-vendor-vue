<footer  @if($user = Auth::user()) class="hidden-xs" @endif>
	<div class="container">
		@if(isset($index_page_flag) && $index_page_flag=='1')
			<div class="row hidden-xs">
				<div class="col-sm-12">
					<div class="info-title">Visit Our Sites</div>
					<hr>
				</div>
				<div class="col-sm-3">
					<ul class="listing">
						<div class="checkbox">
							<li><a href="https://www.marriagematrimony.com" target="_blank">MarriageMatrimony.com</a></li>
							<li><a href="https://www.punjabiwedding.com"  target="_blank">PunjabiWedding.com</a></li>
							<li><a href="https://www.muslimwedding.com"  target="_blank">MuslimWedding.com</a></li>

						</div>
					</ul>
				</div>
			</div>
		@endif
		<div class="row">
			<div class="col-sm-12">
				<br class="hidden-xs">
			</div>
			<div class="col-sm-3 col-xs-6 hidden-xs">
				<div class="info-title">Company</div>
				<hr>
				<ul class="listing">
					<li><a href="{{route('cmsroots', ['slug'=>'aboutus'])}}">About Us</a></li>
					<li><a href="{{route('blogs')}}">Blog</a></li>
					<li><a href="{{route('contact')}}">Contact</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-xs-6 hidden-xs">
				<div class="info-title">Information</div>
				<hr>
				<ul class="listing">
					{{--<li><a href="{{route('cmsroots', ['slug'=>'faqs'])}}">FAQ's</a></li>--}}
					<li><a href="{{route('sitemap')}}">Site Map</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-xs-6 hidden-xs">
				<div class="info-title">Legal</div>
				<hr>
				<ul class="listing">
					<li><a href="{{route('cmsroots', ['slug'=>'term-condition'])}}">Terms of Use</a></li>
					<li><a href="{{route('cmsroots', ['slug'=>'privacy'])}}">Privacy Policy</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
				<div class="info-title">Subscribe to Newsletter</div>
				<hr>
				<span></span>
				<div class="text-danger" id="newsletter-email-blank-err" style="display:none;"></div>
				<div class="text-success" id="newsletter-email-succ-msg" style="display:none;"></div>
				<form name="subscribe-newsletter-frm" method="post" action="{{route('ajax-subscribe-newsletter-action')}}">
					@csrf
					<div class="input-group">
						<input type="email" class="form-control" name="newsletter-email"  placeholder="Enter your email">
						<div class="input-group-btn">
							<button class="btn btn-default" type="submit">Submit</button>
						</div>
					</div>
				</form>
				<ul class="social-footer social-m">
					<li><a href="{{getGeneralSiteSetting('site_facebook_link')}}" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
					<li><a href="{{getGeneralSiteSetting('site_twitter_link')}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					{{--<li><a href=""><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>--}}
					<li><a href="{{getGeneralSiteSetting('site_linkedin_link')}}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					<li><a href="{{getGeneralSiteSetting('site_youtube_link')}}" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
					<li><a href="{{getGeneralSiteSetting('site_instagram_link')}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="footer-bottom">
					{!!getGeneralSiteSetting('site_footer_text')!!}
				</div>
			</div>
		</div>
	</div>
</footer>