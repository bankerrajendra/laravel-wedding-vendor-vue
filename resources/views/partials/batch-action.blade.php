<?php
	$route = url()->current();
	$exp = explode("/", $route);
	$segment = $exp[3];
?>
@if((\Route::current()->getName() != 'deactivated-users') && (\Route::current()->getName() != 'deleted.index'))
<span id="batch_label">
	@if((\Route::current()->getName() == 'users')   || (\Route::current()->getName() == 'inactive-users'))
		<button class="btn btn-success btn-sm init_batch" id="batch_grant_access"><i class="fa fa-check-circle"></i> Grant Access</button>
	@endif

	@if((\Route::current()->getName() == 'users')   || (\Route::current()->getName() == 'active-user'))
		<button class="btn btn-primary btn-sm init_batch" id="batch_revoke_access"><i class="fa fa-undo"></i> Revoke Access</button>
	@endif


		@if($segment == "banned-users")
			<button class="btn btn-primary btn-sm init_batch" id="batch_unban"><i class="fa fa-ban"></i> Un-Ban User</button>
		@else
			<button class="btn btn-primary btn-sm init_batch" id="batch_ban"><i class="fa fa-ban"></i> Ban User(s)</button>
		@endif
		<button class="btn btn-danger  btn-sm init_batch" id="batch_remove"><i class="fa fa-close"></i> Delete User(s)</button>



	<button class="btn btn-primary btn-sm init_batch" id="batch_deactivate" style="display:none"><i class="fa fa-power-off"></i>  Deactivate</button>
	<button class="btn btn-primary btn-sm init_batch" id="batch_activate" style="display:none"><i class="fa fa-star"></i>  Activate Profile</button>
</span>
@endif
