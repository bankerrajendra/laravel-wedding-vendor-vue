<?php $unread_activity_counter = get_activity_counter(); ?>
<ul class="nav nav-tabs">
    <li class="{{ Request::is('viewed-me') ? 'active' : null }}"><a data-toggle="" href="{{route('viewed-me')}}">Viewed Me</a>@if($unread_activity_counter['viewed_me_cnt']>0) <div class="cir">{{$unread_activity_counter['viewed_me_cnt']}}</div>@endif</li>
    <li class="{{ Request::is('my-favorites') ? 'active' : null }}"><a data-toggle="" href="{{route('my-favorites')}}">My Favorites</a></li>
    <li class="{{ Request::is('favorited-me') ? 'active' : null }}"><a data-toggle="" href="{{route('favorited-me')}}">Favorited Me</a>@if($unread_activity_counter['who_fav_me_cnt']>0) <div class="cir">{{$unread_activity_counter['who_fav_me_cnt']}}</div>@endif</li>
</ul>