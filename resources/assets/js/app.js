
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
let axios = require('axios');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('dashboards', require('./components/UserDashboards.vue').default);
Vue.component('search-filter', require('./components/SearchComponent.vue').default);
Vue.component('activity', require('./components/ActivityComponent.vue').default);
Vue.component('conversation-component', require('./components/Messages/ConversationComponent.vue').default);

import Paginate from 'vuejs-paginate'

Vue.component('paginate', Paginate)
Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
});

const app = new Vue({
    el: '#app'
});
