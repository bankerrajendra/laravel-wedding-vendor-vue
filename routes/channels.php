<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

// Broadcast::channel('App.User.{id}', function ($user, $id) {
//     return (int) $user->id === (int) $id;
// });
/**
 * For message chat module to show user typing and show instant message
 */
Broadcast::channel('messages.{sender}.{receiver}', function ($user, $sender, $receiver) {
    if($user->id === (int) $sender || $user->id == (int) $receiver) {
        return true;
    } else {
        return false;
    }
});
Broadcast::channel('chart', function ($user) {
    return [
        'name' => $user->name,
    ];
});
