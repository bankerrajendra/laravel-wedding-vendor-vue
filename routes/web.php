<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
*/

// Homepage Route
Route::get('/', 'WelcomeController@welcome')->name('welcome');
Route::get('get-showcase-videos-ajax', 'WelcomeController@getVendorShowcaseVideosAjax')->name('get-showcase-videos-ajax');
Route::get('get-event-videos-ajax', 'WelcomeController@getEventShowcaseVideosAjax')->name('get-event-videos-ajax');
// Route::get('/', 'Auth\RegisterController@showRegistrationForm')->name('welcome');
Route::get('vendor-signup', 'Auth\RegisterController@showSmallRegistrationForm')->name('vendor-signup');
// EVENTS ROUTES STARTED //
Route::get('event-signup', 'Auth\RegisterController@showEventRegistrationForm')->name('vendor-event-signup');
Route::post('ajax-upload-event-poster', 'Auth\RegisterController@imageEventPosterUpload')->name('ajax-upload-event-poster');
Route::post('ajax-upload-event-cover', 'Auth\RegisterController@imageEventCoverUpload')->name('ajax-upload-event-cover');
Route::post('ajax-upload-event-profile', 'Auth\RegisterController@imageEventProfileUpload')->name('ajax-upload-event-profile');
Route::post('ajax-check-vendor-exists', 'EventController@checkVendorExists')->name('ajax-check-vendor-exists');
Route::get('events/{category?}', 'EventController@showSearch')->name('search-events');
Route::post('handle-event-message-request', 'EventController@handleSubmitMessageRequest')->name('handle-event-message-request');
Route::get('get-event-search-auto-suggestions', 'EventController@getSearchAutoSuggestions')->name('get-event-search-auto-suggestions');
Route::get('event/{eventId}', 'EventController@showDetail')->name('show-event');
Route::get('event-not-found', 'EventController@notFound')->name('event-not-found');
Route::get('event/reviews/{eventId}', 'EventController@showReviews')->name('show-events-reviews');
Route::get('get-event-reviews-ajax', 'EventController@getReviewAjax')->name('get-event-reviews-ajax');
Route::get('get-event-artists-auto-suggestions', 'EventController@getArtistsAutoSuggestions')->name('get-event-artists-auto-suggestions');
Route::get('event-landing/{countryId?}', 'EventController@showLanding')->name('event-landing');
// EVENTS ROUTES ENDED //
Route::get('user-signup', 'Auth\RegisterController@showUserSmallRegistrationForm')->name('user-signup');
Route::get('near-me/{userId}', 'VendorController@showVendorDetail')->name('show-vendor');
Route::get('reviews/{vendorId}', 'VendorController@showVendorReviews')->name('show-reviews');
Route::get('get-search-auto-suggestions', 'VendorController@getSearchAutoSuggestions')->name('get-search-auto-suggestions');
Route::get('get-location-auto-suggestions', 'VendorController@getLocaltionAutoSuggestions')->name('get-location-auto-suggestions');
Route::get('get-vendor-reviews-ajax', 'VendorController@getVendorReviewAjax')->name('get-vendor-reviews-ajax');

Route::post('handle-new-price-request', 'GuestController@handleSubmitNewPriceRequest')->name('handle-new-price-request');
//Route::get('search-vendor/{category?}', 'VendorController@showSearchVendors')->name('search-vendor');
Route::get('/join', 'Auth\RegisterController@showSmallRegistrationForm')->name('join');
Route::get('/unsubscribe/{token}/{type?}','Auth\UserInformationController@unsubscribe')->name('unsubscribe');
Route::view('/unsubscribed','Auth\UserInformationController@showUnsubscribe');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');

Route::get('/{countryname}_states','CmsCityStateController@getStates')->name('get-states');
Route::get('/{countryname}_cities','CmsCityStateController@getCities')->name('get-cities');
Route::get('/country/{country}','CmsCityStateController@getCountryDetail')->name('get-country');

Route::get('/state/{state}','CmsCityStateController@getStateDetail')->name('get-state');
Route::get('/city/{city}','CmsCityStateController@getCityDetail')->name('get-city');

Route::get('/matrimony/{religionname}-matrimony','CmsCityStateController@getReligion')->name('get-religion');
Route::get('/matrimony/{sectname}-matrimony','CmsCityStateController@getSect')->name('get-sect');
Route::get('/mother-tongue','CmsCityStateController@getLanguages')->name('get-languages');
Route::get('/mother-tongue/{languagename}','CmsCityStateController@getLanguageDetail')->name('get-language');
Route::get('/profile-not-found', 'GuestController@profileNotFound')->name('profile-not-found');
// User Request Quote bulk
Route::get('request-a-quote', 'GuestController@showRequestQuote')->name('request-a-quote');
Route::match(['get', 'post'], 'user-choose-vendor', 'GuestController@showUserChooseVendor')->name('user-choose-vendor');
Route::match(['get', 'post'], 'user-vendor-message', 'GuestController@showUserVendorMessage')->name('user-vendor-message');
Route::match(['get', 'post'], 'user-vendor-mail', 'GuestController@showUserVendorMail')->name('user-vendor-mail');
Route::post('handle-submit-user-vendor-mail', 'GuestController@handleUserVendorRequestAQuote')->name('handle-submit-user-vendor-mail');
// Authentication Routes
Auth::routes();

// Public Routes
Route::group(['middleware' => ['web', 'activity']], function () {

    // Activation Routes
    Route::get('/getApiToken', ['uses' => 'ApiTokenController@update']);
    Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

    Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);

    Route::get('/activation', ['as' => 'authenticated.activation-resend', 'uses' => 'Auth\ActivateController@resend']);
    Route::get('/exceeded', ['as' => 'exceeded', 'uses' => 'Auth\ActivateController@exceeded']);

    // Socialite Register Routes
    Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

    // Route to for user to reactivate their user deleted account.
    Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);

    Route::post('ajax/fetchLocation', ['as' => 'ajax.fetchLocation', 'uses' => 'AjaxController@fetchLocation']);
    Route::post('ajax/mapReligion', ['as' => 'ajax.mapReligion', 'uses' => 'AjaxController@mapReligion']);
    Route::post('ajax/manageUserEntries', ['as' => 'ajax.manageUserEntries', 'uses' => 'AjaxController@manageUserEntries']);
    Route::post('ajax/updateFlagedUserre_entry', ['as' => 'ajax.updateFlagedUserre_entry', 'uses' => 'AjaxController@updateFlagedUserre_entry']);
    Route::post('ajax/approvePhoto', ['as' => 'ajax.approvePhoto', 'uses' => 'AjaxController@approvePhoto']);
    Route::post('ajax/disapprovePhoto', ['as' => 'ajax.disapprovePhoto', 'uses' => 'AjaxController@disapprovePhoto']);
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated', 'activity','grantAccess']], function () {

    /**
     * Events Related STARTED
     */
    Route::get('add-event', 'EventController@showAdd')->name('add-event');
    Route::post('handle-event-add', 'EventController@handleAdd')->name('handle-event-add');
    Route::post('ajax-upload-event-add-poster', 'EventController@imageEventPosterUpload')->name('ajax-upload-event-add-poster');
    Route::post('ajax-upload-event-add-cover', 'EventController@imageEventCoverUpload')->name('ajax-upload-event-add-cover');
    Route::post('ajax-upload-event-add-profile', 'EventController@imageEventProfileUpload')->name('ajax-upload-event-add-profile');
    Route::get('event/edit/{eventId}', 'EventController@showEdit')->name('edit-event');
    Route::post('handle-event-edit', 'EventController@handleEdit')->name('handle-event-edit');
    Route::post('ajax-upload-event-edit-poster', 'EventController@imageEditEventPosterUpload')->name('ajax-upload-event-edit-poster');
    Route::post('ajax-upload-event-edit-cover', 'EventController@imageEditEventCoverUpload')->name('ajax-upload-event-edit-cover');
    Route::post('ajax-upload-event-edit-profile', 'EventController@imageEditEventProfileUpload')->name('ajax-upload-event-edit-profile');
    
    Route::get('account/vendor-events-reviews', 'EventController@showVendorReviews')->name('vendor-events-reviews');
    /**
     * Events Related ENDED
     */

    /**
     * Vendor realted routes
     */
    Route::get('upload-vendor-cover', 'Auth\UserInformationController@showUploadCoverForm')->name('upload-vendor-cover');
    Route::post('ajax-upload-vendor-cover', 'UserController@imageCoverUpload')->name('ajax-upload-vendor-cover');
    Route::get('upload-vendor-profile', 'Auth\UserInformationController@showUploadProfileForm')->name('upload-vendor-profile');
    Route::post('ajax-upload-vendor-profile', 'UserController@imageProfileUpload')->name('ajax-upload-vendor-profile');
    Route::post('ajax-remove-vendor-profile-image', 'UserController@imageProfileDelete')->name('ajax-remove-vendor-profile-image');
    Route::get('account/vendor-business-info', 'Auth\UserInformationController@showVendorInfo')->name('vendor-business-info');
    Route::get('vendor-manage', 'Auth\UserInformationController@showVendorMobile')->name('vendor-business-mobile');
    Route::get('user-profile', 'Auth\UserInformationController@showUserProfileMobile')->name('user-profile-mobile');
    Route::get('user-setting', 'Auth\UserInformationController@showUserSettingMobile')->name('user-setting-mobile');
    Route::get('vendor-setting', 'Auth\UserInformationController@showVendorSettingMobile')->name('vendor-setting-mobile');


    Route::post('handle-post-vendor-business-info', 'UserController@handleVendorInfo')->name('handle-post-vendor-business-info');
    Route::get('account/vendor-faqs', 'Auth\UserInformationController@showVendorFaqs')->name('vendor-faqs');
    Route::get('get-vendor-faqs-ajax', 'Auth\UserInformationController@getVendorFaqsAjax')->name('get-vendor-faqs-ajax');
    Route::post('handle-submit-vendor-faq-answers', 'UserController@handleSubmitFaqAnswers')->name('handle-submit-vendor-faq-answers');
    Route::post('handle-submit-vendor-faq-custom', 'UserController@handleSubmitFaqCustom')->name('handle-submit-vendor-faq-custom');
    Route::post('ajax-remove-vendor-custom-faq', 'UserController@ajaxDeleteCustomFaq')->name('ajax-remove-vendor-custom-faq');
    Route::get('account/vendor-photo-upload', 'Auth\UserInformationController@showVendorPhotoUploadForm')->name('vendor-photo-upload');
    Route::get('set-vendor-profile-image/{id}', 'UserController@setVendorProfileImage')->name('set-vendor-profile-image');
    Route::get('account/vendor-video', 'Auth\UserInformationController@showVendorVideo')->name('vendor-video');
    Route::post('handle-vendor-video-submit', 'UserController@handleVendorVideoSubmit')->name('handle-vendor-video-submit');
    Route::get('delete-vendor-video/{id}', 'UserController@handleVendorVideoDelete')->name('delete-vendor-video');
    Route::get('account/vendor-showcase', 'Auth\UserInformationController@showVendorShowcaseForm')->name('vendor-showcase');
    Route::post('handle-vendor-showcase-submit', 'UserController@handleVendorShowcaseSubmit')->name('handle-vendor-showcase-submit');
    Route::post('ajax-handle-vendor-satellites-submit', 'UserController@ajaxHandleVendorSatellitesSubmit')->name('ajax-handle-vendor-satellites-submit');
    Route::get('account/vendor-collect-reviews', 'Auth\UserInformationController@showVendorCollectReviews')->name('vendor-collect-reviews');
    Route::post('handle-submit-collect-reviews', 'Auth\UserInformationController@handleSubmitVendorCollectReviews')->name('handle-submit-collect-reviews');
    Route::get('account/vendor-reviews', 'Auth\UserInformationController@showVendorReviews')->name('vendor-reviews');
    
    // Vendor Setting
    Route::get('settings/vendor-change-password', 'Auth\UserInformationController@showChangePassword')->name('vendor-change-password');
    Route::post('handle-vendor-change-password', 'Auth\UserInformationController@handleChangePassword')->name('handle-vendor-change-password');
    Route::get('settings/vendor-notification', 'Auth\UserInformationController@showVendorNotification')->name('vendor-notification');
    Route::post('settings/handle-vendor-email-notification','Auth\UserInformationController@handleVendorEmailNotification')->name('handle-vendor-email-notification');
    Route::get('settings/vendor-delete-account', 'Auth\UserInformationController@showVendorAccountDelete')->name('vendor-delete-account');
    Route::post('hide-show-account', 'Auth\UserInformationController@handleHideShowAccount')->name('hide-show-account');

    // Activation Routes
    Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
    Route::get('/additional-information', ['uses' => 'Auth\UserInformationController@showUserInformationForm'])->name('additional-information');
    Route::post('/additional-information', ['uses' => 'Auth\UserInformationController@update'])->name('post-additional-information');
    Route::get('/pending-information',['uses' => 'Auth\UserInformationController@showPendingInformationForm'])->name('pending-information');
    Route::post('/pending-information',['uses' => 'Auth\UserInformationController@savePendingInformation'])->name('post-pending-information');
    Route::get('/upload-image',['uses'=>'Auth\UserInformationController@showUploadImageForm'])->name('upload-image');
    Route::post('/ajax-upload-image',['uses'=>'UserController@imageUpload'])->name('ajax-upload-image');
    Route::get('/delete-image/{id}/{activesection}',['uses'=>'UserController@deleteImage'])->name('ajax-delete-image');
    Route::get('/movetoprivate-image/{id}',['uses'=>'UserController@movetoprivateImage'])->name('ajax-movetoprivate-image');
    Route::get('/movetopublic-image/{id}',['uses'=>'UserController@movetopublicImage'])->name('ajax-movetopublic-image');
    Route::get('/set-profile-image/{id}',['uses'=>'UserController@setProfileImage'])->name('set-profile-image');
    Route::post('/ajax-site_contact_telephone',['uses'=>'CmsController@getTelephone'])->name('ajax-site_contact_telephone');

    // Upload user photo step 2
    Route::get('user-upload-photo', ['uses' => 'Auth\UserInformationController@showUserUploadPhoto'])->name('user-upload-photo');
    Route::post('ajax-user-upload-photo', ['uses' => 'UserController@imageUploadUser'])->name('ajax-user-upload-photo');

    Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');
    Route::get('/change-password', ['uses' => 'Auth\ChangePasswordController@showChangePasswordForm'])->name('change-password');
    //Route::post('/change-password', ['uses' => 'Auth\ChangePasswordController@changePassword'])->name('post-change-password');

    //Route::get('/deactivate-account',['uses' => 'UsersManagementController@deactivateAccount'])->name('deactivate-account');  // Deactivate Account First Page
    Route::get('/disable-account',['uses' => 'UsersManagementController@disableAccount'])->name('disable-account');
    Route::post('/deactivating-account', ['uses' => 'UsersManagementController@deactivatingAccount'])->name('deactivating-account');
    Route::post('/restoreInactiveUser', ['uses' => 'UsersManagementController@restoreInactiveUser'])->name('restoreInactiveUser');
    Route::get('/delete-account', ['uses' => 'UsersManagementController@deleteAccount'])->name('delete-account');
    Route::get('/deleting-account', ['uses' => 'UsersManagementController@deletingAccount'])->name('deleting-account');


    Route::post('/like-profile', ['uses' => 'Activity\FavouriteController@likeProfile'])->name('like-profile');
    Route::post('/shortlist-profile', ['uses' => 'Activity\ShortlistController@shortlistProfile'])->name('shortlist-profile');
    Route::post('/skip-profile', ['uses' => 'Activity\SkipController@skipProfile'])->name('skip-profile');
    Route::post('/delete-viewed-me',['uses'=>'Activity\ViewedController@deleteViewedMe'])->name('delete-viewed-me');
    Route::post('/delete-shortlisted',['uses'=>'Activity\ShortlistController@deleteShortlisted'])->name('delete-shortlisted');
    Route::post('/like-back',['uses'=>'Activity\FavouriteController@likeBackProfile'])->name('like-back');
    Route::post('/delete-liked',['uses'=>'Activity\FavouriteController@deleteLikedProfile'])->name('delete-liked');
    Route::post('/delete-skipped', ['uses'=>'Activity\SkipController@deleteSkippedProfile'])->name('delete-skipped');
    Route::post('/block-profile', ['uses'=>'Activity\BlockController@blockProfile'])->name('block-profile');
    Route::post('/unblock-profile', ['uses'=>'Activity\BlockController@unblockProfile'])->name('unblock-profile');
    Route::post('/report-user',['uses'=>'Activity\ReportController@reportUser'])->name('report-user');
    Route::post('/report-admin-reply',['uses'=>'Activity\ReportController@reportAdminReply'])->name('report-admin-reply');
    Route::get('/report-report-trash/{report_id}',['uses'=>'Activity\ReportController@reportAdminTrash'])->name('report-report-trash');
    Route::post('/admin-update-user',['uses'=>'UsersManagementController@admin_update_user'])->name('admin-update-user');
    Route::post('/share-photos', ['uses'=>'Activity\PhotoController@sharePhotos'])->name('share-photos');
    Route::post('/unshare-photos', ['uses'=>'Activity\PhotoController@unsharePhotos'])->name('unshare-photos');
    Route::get('/unshare-all-photos', ['uses'=>'Activity\PhotoController@unshareAllPhotos'])->name('unshare-all-photos');

    Route::post('/request-to-view', ['uses'=>'Activity\PhotoController@requestToView'])->name('request-toview');
    Route::post('/accept-photo-request', ['uses'=>'Activity\PhotoController@acceptPhotoRequest'])->name('accept-photo-request');
    Route::post('/deny-photo-request', ['uses'=>'Activity\PhotoController@denyPhotoRequest'])->name('deny-photo-request');
    Route::get('/deny-all-photos', ['uses'=>'Activity\PhotoController@denyAllPhotos'])->name('deny-all-photos');

    Route::post('/send-photo-request', ['uses'=>'Activity\PhotoController@sendPhotoRequest'])->name('send-photo-request');

    Route::get('/viewed-me', ['uses' => 'Activity\ViewedController@showViewedMeUsers'])->name('viewed-me');
    Route::get('/my-favorites', ['uses' => 'Activity\FavouriteController@showMyFavourites'])->name('my-favorites');
    Route::get('/favorited-me', ['uses' => 'Activity\FavouriteController@showWhoFavouritedMe'])->name('favorited-me');

//    Route::get('/shortlisted-profiles', ['uses' => 'Activity\ShortlistController@showShortlistedUsers'])->name('shortlisted-profiles');
//    Route::get('/favourited-profiles', ['uses' => 'Activity\FavouriteController@showMyFavourites'])->name('favourited-profiles');
//    Route::get('/who-favourited-me-profiles', ['uses' => 'Activity\FavouriteController@showWhoFavouritedMe'])->name('who-favourited-me-profiles');
    Route::get('/skipped-profiles', ['uses' => 'Activity\SkipController@showSkippedProfiles'])->name('skipped-profiles');
    Route::get('/blocked-profiles', ['uses' => 'Activity\BlockController@showBlockedProfiles'])->name('blocked-profiles');
//    Route::get('/profile/{name}', ['uses' => 'UserController@getProfile'])->name('user-profile');
    Route::get('/profile/{name}/popup', ['uses' => 'UserController@getProfilePopup'])->name('user-profile-popup');


    Route::get('/connect', ['uses' => 'UserController@getConnect'])->name('connect');

    // Account/Notification Module Routes start here
    Route::get('/account/setting',['uses' => 'AccountController@settings'])->name('settings');
    Route::post('/post-change-password', ['uses' => 'AccountController@changePassword'])->name('post-change-password');
    Route::post('/post-change-location', ['uses' => 'AccountController@changeLocation'])->name('post-change-location');

    Route::post('settings-showhide', ['uses'=> 'AccountController@saveShowSettings'])->name('settings-showhide');
    Route::get('/account/sharePhotoPermission',['uses' => 'AccountController@sharePhotoPermission'])->name('share-photo-permission');
    Route::get('/account/privatePhotoPermission',['uses' => 'AccountController@privatePhotoPermission'])->name('private-photo-permission');
    Route::get('/account/blockedMembers',['uses' => 'AccountController@blockedMembers'])->name('blocked-members');
    Route::get('/account/hiddenProfiles',['uses' => 'AccountController@hiddenProfiles'])->name('hidden-profiles');
    Route::get('/account/shortlistedProfiles', ['uses' => 'AccountController@shortlistedUsers'])->name('shortlisted-profiles');
    Route::get('/settings/my-membership',['uses' => 'AccountController@showMyMembership'])->name('my-membership');
    Route::get('/account/all-reviews', 'AccountController@getUserReviews')->name('all-reviews');
    Route::view('/account-mobile','partials.account-navigation-mobile')->name('account-mobile');
    /**
     * Notification
     */
    Route::get('/account/notification','AccountController@showNotification')->name('notification');
    Route::post('/account/report-user-reply', 'AccountController@handleUserReportReply')->name('report-user-reply');
    Route::post('/account/report-user-trash', 'AccountController@handleUserReportTrash')->name('report-user-trash');
    Route::post('/account/handle-email-notification','AccountController@handleEmailNotification')->name('handle-email-notification');
    Route::post('/account/mark-flag-notification-read', 'AccountController@markFlagNotificationRead')->name('mark-flag-notification-read');
    // END: Account/Notification Module Routes start here

    //Messaging Module Routes start here
    Route::get('/inbox', ['uses' => 'MessageController@getInbox'])->name('inbox');
    Route::get('/inbox/{unread}', ['uses' => 'MessageController@getInbox'])->name('inbox_showread');
    Route::get('/inbox/sendtoarchive/{userId}', ['uses' => 'MessageController@sendArchive'])->name('send-archive');
    Route::get('/conversation/{userId}', ['uses' => 'MessageController@getConversation'])->name('conversation');
    Route::get('/inbox/sendtotrash/{userId}/{pageType}', ['uses' => 'MessageController@sendTrash'])->name('send-trash');
    Route::get('/conversation', function () {
        return redirect(route('inbox'));
    });

    Route::get('/sent', ['uses' => 'MessageController@getSent'])->name('sent');
    Route::get('/conversation/{userId}/{pagetype}', ['uses' => 'MessageController@getConversation'])->name('sent-conversation');
//    Route::get('/sent/sendtotrash/{userId}/{pageType}', ['uses' => 'MessageController@sendTrash'])->name('send-trash');
//    Route::get('/sent-conversation', function () {
//        return redirect(route('inbox'));
//    });

    Route::get('/filter', ['uses' => 'MessageController@getFilter'])->name('filter');
    Route::post('/filter-form', 'MessageController@postFilter')->name('post-filter-form');

    Route::get('/filter-conversation/{userId}', ['uses' => 'MessageController@getFilterConversation'])->name('filter-conversation');
    Route::get('/filter-conversation', function () {
        return redirect(route('filter'));
    });


    Route::get('/archive', ['uses' => 'MessageController@getArchive'])->name('archive');
    Route::get('/archive/sendtoinbox/{userId}', ['uses' => 'MessageController@sendInbox'])->name('send-inbox');
    Route::get('/archive-conversation/{userId}', ['uses' => 'MessageController@getArchiveConversation'])->name('archive-conversation');
    Route::get('/archive-conversation', function () {
        return redirect(route('archive'));
    });

    Route::get('/trash', ['uses' => 'MessageController@getTrash'])->name('trash');
    Route::get('/trash/trashtoinbox/{userId}', ['uses' => 'MessageController@moveToInbox'])->name('move-to-inbox');
    Route::get('/trash/trashtoarchive/{userId}', ['uses' => 'MessageController@moveToArchive'])->name('move-to-archive');
    Route::get('/trash/movetodelete/{userId}', ['uses' => 'MessageController@moveToDelete'])->name('parmanent-delete');
    Route::get('/trash-conversation/{userId}', ['uses' => 'MessageController@getTrashConversation'])->name('trash-conversation');
    Route::get('/trash-conversation', function () {
        return redirect(route('trash'));
    });


    Route::post('/send-message', ['uses'=>'MessageController@sendMessage'])->name('send-message');
    Route::post('/send-message-view', ['uses'=>'MessageController@sendMessageView'])->name('send-message-view');

    Route::get('/delete-conversation/{userId}', ['uses' => 'MessageController@deleteConversation'])->name('delete-conversation');

    Route::get('/trash', ['uses' => 'MessageController@getTrash'])->name('trash');
    //Messaging Module Routes end here


    //View Profile start here
    Route::get('/profile/{name}', ['uses' => 'UserController@getProfile'])->name('user-profile');




    Route::get('/setting', ['uses' => 'UserSettingController@showSettingPage'])->name('setting');
    Route::post('/setting', ['uses'=> 'UserSettingController@savePartnerPreferences'])->name('save-partner-preferences');
    Route::post('alert-settings', ['uses'=> 'UserSettingController@saveAlertSettings'])->name('alert-settings');

    Route::get('/edit-profile',['uses'=> 'UserSettingController@showEditProfileForm'])->name('edit-profile');
    Route::get('/my-profile', ['uses'=> 'UserSettingController@getProfile'])->name('my-profile');
    Route::get('/my-hobbies', ['uses'=> 'UserSettingController@editHobbies'])->name('my-hobbies');
    Route::get('/partner-preference', ['uses'=> 'UserSettingController@editPartnerPreference'])->name('partner-preference');
    Route::get('/my-photos', ['uses'=> 'UserSettingController@showMyPhotosPage'])->name('my-photos');
    Route::get('/self-profile', ['uses'=> 'UserSettingController@getSelfProfile'])->name('self-profile');


    Route::get('/manage-photos', ['uses'=>'UserSettingController@showManagePhotosPage'])->name('manage-photos');
    Route::post('/edit-profile',['uses'=> 'UserSettingController@updateProfile'])->name('post-edit-profile');

//    Route::any('/search',['uses'=> 'SearchController@showSearchPage'])->name('search');
    Route::get('/get-membership', ['uses'=>'MembershipController@showMembershipPage'])->name('membership');
    Route::get('/new-online',['uses'=>'UserController@newonline'])->name('new-online');
    Route::get('/dashboard',['uses'=>'UserController@newonline'])->name('dashboard');

    Route::post('/updateUserFrontend',['uses'=> 'UsersManagementController@updateUserFrontend'])->name('updateUserFrontend');
    Route::post('/updateHobbiesFrontend',['uses'=> 'UsersManagementController@updateHobbiesFrontend'])->name('updateHobbiesFrontend');
    Route::post('/updatePartnerPreferenceFrontend',['uses'=> 'UsersManagementController@updatePartnerPreferenceFrontend'])->name('updatePartnerPreferenceFrontend');

    // New Search - Rajendra Banker - 1st April 2019
    Route::any('/search', 'NewSearchController@newSearchPage')->name('new-search');
    //Route::any('/search-result', 'NewSearchController@newSearchApi')->name('searchResult');
    Route::get('improve/add', 'ImprovesController@showAddImprove')->name('add-improve');
    Route::post('improve/handle-submit', 'ImprovesController@handleSubmitImprove')->name('handle-improve-submit');

    // User Review
    Route::get('write-review/{vendorId}', 'ReviewsController@showWriteReview')->name('write-review');
    Route::post('write-review/handle-submit', 'ReviewsController@handleSubmitReview')->name('handle-review-submit');
    // Event Review
    Route::get('write-event-review/{eventId}', 'EventsReviewsController@showWriteReview')->name('write-event-review');
    Route::post('write-event-review/handle-submit', 'EventsReviewsController@handleSubmitReview')->name('handle-event-review-submit');
    // Shortlist Vendor
    Route::get('short-list-vendor-ajax', 'VendorController@shortListVendor')->name('short-list-vendor-ajax');
    Route::get('short-list-finalize-vendor-ajax', 'VendorController@shortListFinalizeVendor')->name('short-list-finalize-vendor-ajax');
    Route::get('settings/shortlisted-vendors', 'UserController@showShortListedVendors')->name('shortlisted-vendors');
    Route::get('settings/finalized-vendors', 'UserController@showFinalizedVendors')->name('finalized-vendors');
    // User Setting
    Route::get('settings/user-change-password', 'UserController@showChangePassword')->name('user-change-password');
    Route::post('handle-user-change-password', 'UserController@handleChangePassword')->name('handle-user-change-password');
    Route::get('settings/disable-account', 'UserController@showDisableAccount')->name('user-disable-account');
    Route::get('settings/deactivate-account', 'UserController@showDeactivateAccount')->name('user-deactivate-account');
    Route::get('settings/delete-account', 'UserController@showDeleteAccount')->name('user-delete-account');
    Route::get('settings/user-notification', 'UserController@showUserNotification')->name('user-notification');
    // User Profile
    Route::get('myprofile/user-manage-profile', 'UserController@showManageProfile')->name('user-manage-profile');
    Route::post('handle-submit-manage-profile', 'UserController@handleSubmitManageProfile')->name('handle-submit-manage-profile');
    Route::get('myprofile/user-manage-photo', 'UserController@showManageUserPhoto')->name('user-manage-photo');
    Route::post('ajax-remove-user-profile-image', 'UserController@imageProfilePhotoDelete')->name('ajax-remove-user-profile-image');
});



    // Registered and Activated User Routes
    //Route::group(['middleware' => ['auth', 'activated', 'socialLogin' , 'userInfo', 'activity', 'twostep']], function () {
    Route::group(['middleware' => ['auth', 'activated', 'socialLogin' , 'userInfo', 'activity', 'twostep']], function () {

    //  Homepage Route - Redirect based on user role is in controller.
    Route::get('/home', ['as' => 'public.home',   'uses' => 'UserController@index']);
    Route::get('/home/{partner}', 'UserController@index')->name('dashboard-partner-diamond');


        Route::get('/userDash', ['as' => 'user-dash',   'uses' => 'UserController@userDashboard']);
        Route::get('/userDash/{partner}', ['as' => 'user-dash',   'uses' => 'UserController@userDashboard']);


        Route::any('/userActivity', ['as' => 'user-activity',   'uses' => 'Activity\ViewedController@userActivity']);

        Route::resource('vueitems','UserController');
    // Show users profile - viewable by other users.
    Route::get('profile/{username}', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@show',
    ]);
    // Membership Page - Rajendra Banker 9th April 2019
    Route::get('membership', 'MembershipController@ShowFreeMemberShip')->name('free-membership');
    // Paid Membership Page - Rajendra Banker 11th April 2019
    Route::get('paid-membership', 'MembershipController@ShowPaidMemberShip')->name('paid-membership');
    // Helcim Payment Submission Handle - Rajendra Banker 11th April 2019
    Route::post('handle-helcim-payment-submission', 'MembershipController@HandleHelcimPaymentSubmission')->name('handle-helcim-payment-submission');
    // Merrco Payment Submission Handle - Rajendra Banker 11th April 2019
    Route::post('handle-merrco-payment-submission', 'MembershipController@HandleMerrcoPaymentSubmission')->name('handle-merrco-payment-submission');
    // route for processing payment paypal
    Route::post('handle-paypal-payment-submission', 'MembershipController@payWithpaypal')->name('handle-paypal-payment-submission');
    // route for check status of the payment
    Route::get('payment-status-paypal-success', 'MembershipController@getPaymentStatus')->name('payment-status-paypal-success');
    // route for cancel paypal url
    Route::get('payment-status-paypal-cancel', 'MembershipController@showPaymentPaypalCancel')->name('payment-status-paypal-cancel');
    // Show payment success page
    Route::get('payment-success', 'MembershipController@showPaymentSuccess')->name('payment-success');
    // Show payment error page
    Route::get('payment-error', 'MembershipController@showPaymentError')->name('payment-error');

});
// Registered, activated, and is current user routes.
Route::group(['middleware' => ['auth', 'activated', 'userInfo', 'currentUser', 'activity', 'twostep']], function () {

    // User Profile and Account Routes
    Route::resource(
        'profile',
        'ProfilesController', [
            'only' => [
                'show',
                'edit',
                'update',
                'create',
            ],
        ]
    );
    Route::put('profile/{username}/updateUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserAccount',
    ]);
    Route::put('profile/{username}/updateUserPassword', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@updateUserPassword',
    ]);
    Route::delete('profile/{username}/deleteUserAccount', [
        'as'   => '{username}',
        'uses' => 'ProfilesController@deleteUserAccount',
    ]);

    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', [
        'uses' => 'ProfilesController@userProfileAvatar',
    ]);

    // Route to upload user avatar.
    Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);
});

// Registered, activated, and is admin routes.
Route::group(['middleware' => ['auth', 'activated', 'role:admin', 'activity', 'twostep']], function () {
    Route::resource('/users/deleted', 'SoftDeletesController', [
        'only' => [
            'index', 'show', 'update', 'destroy',
        ],
    ]);

    Route::resource('users', 'UsersManagementController', [
        'names' => [
            'index'   => 'users',
            'destroy' => 'user.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);

    Route::get('users/deleted', 'SoftDeletesController@index')->name('users-deleted');

    Route::post('search-users', 'UsersManagementController@search')->name('search-users');
    Route::get('users/{id}/ban', 'UsersManagementController@banUser')->name('ban-user');
    Route::get('users/{id}/unban', 'UsersManagementController@unBanUser')->name('unban-user');
    Route::get('users/{id}/featured', 'UsersManagementController@featured')->name('featured');
    Route::get('users/{id}/remove-featured', 'UsersManagementController@removeFeatured')->name('remove-featured');
    Route::get('active-user', 'UsersManagementController@getActiveUsers')->name('active-user');
    Route::get('banned-users', 'UsersManagementController@getBannedUsers')->name('banned-users');
    Route::get('featured-users', 'UsersManagementController@getFeaturedUsers')->name('featured-users');
    Route::get('deactivated-users', 'UsersManagementController@getDeactivatedUsers')->name('deactivated-users');
    Route::get('inactive-users', 'UsersManagementController@getInactiveUsers')->name('inactive-users');
    Route::get('incomplete-users', 'UsersManagementController@getIncompleteUsers')->name('incomplete-users');
    Route::post('/admin/delete-image', 'UsersManagementController@deleteImage')->name('admin-delete-image');
    // Added by Rajendra Banker - Latest Updates - 14th May 2019
    Route::get('admin/latest-updates', 'UsersManagementController@showLatestUpdates')->name('latest-updates');

    Route::resource('themes', 'ThemesManagementController', [
        'names' => [
            'index'   => 'themes',
            'destroy' => 'themes.destroy',
        ],
    ]);

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('routes', 'AdminDetailsController@listRoutes');
    Route::get('active-users', 'AdminDetailsController@activeUsers');
    // Added by Rajendra Banker - Manage members - 25th April 2019
    Route::get('admin/paid-members/{type?}', 'MembershipController@showPaidMembers')->name('paid-members');
    Route::post('ajax/member-payment-history', 'AjaxController@showUserPaymentHistory')->name('member-payment-history');
    Route::post('ajax/member-info', 'AjaxController@showUserInfo')->name('member-info');
    Route::post('ajax/update-member-plan', 'AjaxController@handleEditPlanAction')->name('update-member-plan');
    Route::get('admin/upcoming-renewal', 'MembershipController@showUpcomingRenewalMembers');
    Route::get('admin/expired-members', 'MembershipController@showExpiredMembers');
    Route::get('admin/planwise-members', 'MembershipController@showPlanWiseMembers')->name('planwise-members');
    // Added by Rajendra Banker - Send Free Diamond Membership - 14th May 2019
    Route::get('admin/free-diamond-membership', 'MembershipController@showSendFreeDiamondMembership')->name('free-diamond-membership');
    Route::post('admin/handle-send-free-daimond-membership', 'MembershipController@handleSendFreeDiamondMembership')->name('handle-send-free-daimond-membership');

    // General Content Templates - Rajendra Banker - 29th April 2019
    Route::get('admin/templates/{type}/list', 'NewsLetterEmailSmsController@showListings')->name('admin-templates-list');
    Route::get('admin/templates/{type}/add', 'NewsLetterEmailSmsController@showAddTemplate')->name('admin-templates-add');
    Route::get('admin/templates/{type}/edit/{id}', 'NewsLetterEmailSmsController@showEditTemplate')->name('edit-template');
    Route::get('admin/templates/{type}/view/{id}', 'NewsLetterEmailSmsController@showViewTemplate')->name('admin-view-template');
    Route::post('admin/handle-add-template', 'NewsLetterEmailSmsController@handleAddTemplate')->name('handle-add-template');
    Route::post('admin/handle-edit-template', 'NewsLetterEmailSmsController@handleEditTemplate')->name('handle-edit-template');
    Route::post('admin/handle-manage-bulk-template', 'NewsLetterEmailSmsController@handleBulkActionTemplate')->name('handle-manage-bulk-template');
    Route::get('admin/templates/sms/config', 'NewsLetterEmailSmsController@showSMSConfiguarion');
    Route::post('admin/handle-sms-config', 'NewsLetterEmailSmsController@handleSMSConfiguarion')->name('handle-sms-config');
    Route::get('admin/templates/news-letter/subscribers', 'NewsLetterEmailSmsController@showSubscriberListing');
    Route::post('admin/handle-manage-bulk-newsletter-subscribers', 'NewsLetterEmailSmsController@handleBulkActionNewsLetterSubscribers')->name('handle-manage-bulk-newsletter-subscribers');
    // Newsletter send
    Route::get('admin/templates/news-letter/send', 'NewsLetterEmailSmsController@showNewsLetterSend');
    Route::post('admin/get-members-for-newsletter', 'NewsLetterEmailSmsController@getMembersForNewsLetter')->name('ajax-show-members-newsletter-send');
    Route::post('admin/handle-send-newsletter-submit', 'NewsLetterEmailSmsController@handleNewsLetterSubmit')->name('handle-send-newsletter-submit');
    // SMS
    Route::post('admin/send-sms-member', 'SmsController@sendSms')->name('send-sms-member');
    // Email
    Route::post('admin/send-email-member', 'EmailController@sendEmail')->name('send-email-member');
    // Reviews
    Route::get('admin/reviews/list', 'ReviewsController@showAdminListings')->name('admin-review-list');
    Route::get('admin/reviews/edit/{id}', 'ReviewsController@showEditReview')->name('admin-review-edit');
    Route::post('admin/handle-edit-review', 'ReviewsController@handleEditreview')->name('handle-edit-review');
    Route::get('admin/reviews/view/{id}', 'ReviewsController@showViewReview')->name('admin-review-view');
    Route::post('admin/handle-manage-bulk-reviews', 'ReviewsController@handleBulkActionReview')->name('handle-manage-bulk-reviews');
    // Improves
    Route::get('admin/improves/list', 'ImprovesController@showAdminListings')->name('admin-improves-list');
    Route::get('admin/improves/view/{id}', 'ImprovesController@showViewImprove')->name('admin-improves-view');
    Route::post('admin/handle-manage-bulk-improves', 'ImprovesController@handleBulkActionImprove')->name('handle-manage-bulk-improves');
    // Site Settings
    Route::get('admin/site-settings/logo-favicon', 'SiteSettingsController@showLogoFavicon');
    Route::post('admin/site-settings/handle-logo-favicon-submit', 'SiteSettingsController@handleLogoFavicon')->name('handle-logo-favicon-submit');
    Route::get('admin/site-settings/matri-prefix', 'SiteSettingsController@showMatriPrefix');
    Route::post('admin/site-settings/handle-matri-prefix-submit', 'SiteSettingsController@handleMatriPrefix')->name('handle-matri-prefix-submit');
    Route::get('admin/site-settings/email', 'SiteSettingsController@showEmail');
    Route::post('admin/site-settings/handle-site-email-submit', 'SiteSettingsController@handleEmail')->name('handle-site-email-submit');
    Route::get('admin/site-settings/basic', 'SiteSettingsController@showBasic');
    Route::post('admin/site-settings/handle-site-basic-settings-submit', 'SiteSettingsController@handleBasic')->name('handle-site-basic-settings-submit');
    Route::get('admin/site-settings/social-media', 'SiteSettingsController@showSocialMedia');
    Route::post('admin/site-settings/handle-social-media-submit', 'SiteSettingsController@handleSocialMedia')->name('handle-social-media-submit');
    Route::get('admin/site-settings/change-password', 'SiteSettingsController@showChangePassword');
    Route::post('admin/site-settings/handle-admin-change-password-submit', 'SiteSettingsController@handleChangePassword')->name('handle-admin-change-password-submit');

    Route::get('admin/cms-city-state', 'CmsCityStateController@getCmsCityState')->name('cms-city-state');
    Route::get('admin/cms-city-state/{name}', 'CmsCityStateController@getCmsCityState')->name('cms-city-states');
    Route::get('admin/cms-city-state/{id}/edit', 'CmsCityStateController@setCmsCityState')->name('set-cms-city-states');
    Route::post('admin/cms-city-state/{id}/update', 'CmsCityStateController@updateCmsCityState')->name('update-cms-city-states');
    Route::get('admin/reports','Activity\ReportController@showReports')->name('user-reports');

    Route::get('admin/cms', 'CmsController@getCms')->name('cms-listing');
    Route::get('admin/cms/{id}/edit', 'CmsController@setCmsPage')->name('set-cms-page');
    Route::post('admin/cms/{id}/update', 'CmsController@updateCmsPage')->name('update-cms-page');
    Route::get('admin/blog', 'BlogController@getBlog')->name('blog');
    Route::get('admin/blog/add', 'BlogController@addBlog')->name('add-blog');
    Route::post('admin/blog/update', 'BlogController@updateBlog')->name('update-blog');
    Route::get('admin/blog/{id}/edit', 'BlogController@editBlog')->name('edit-blog');
    Route::post('admin/blog/{id}/postedit', 'BlogController@posteditBlog')->name('post-edit-blog');
    Route::get('admin/blog/{id}/delete', 'BlogController@deleteBlog')->name('delete-blog');
    Route::get('admin/blog/comments', 'BlogController@getComments')->name('blog-comments');
    Route::get('admin/blog/{id}/comments', 'BlogController@getComments')->name('blogwise-comments');
    Route::get('admin/blog/comments/{id}/{status}', 'BlogController@actionComments')->name('approve_comment');
    Route::get('admin/blog/comment/{id}/delete', 'BlogController@deleteComment')->name('delete-comment');


    Route::get('admin/membership-plans', 'MembershipController@getPlans')->name('membership-plans');
    Route::get('admin/membership-benefits', 'MembershipController@getPlanbenefits')->name('membership-benefits');
    Route::get('admin/membership/add', 'MembershipController@addBenefit')->name('add-benefits');
    Route::post('admin/membership/update', 'MembershipController@updateBenefit')->name('update-benefit');
    Route::get('admin/membership/{id}/edit', 'MembershipController@editBenefit')->name('edit-benefit');
    Route::get('admin/membership/{id}/delete', 'MembershipController@deleteBenefit')->name('delete-benefit');
    Route::post('admin/membership/postedit', 'MembershipController@posteditBenefit')->name('post-edit-benefit');
    Route::get('admin/membership/addplan', 'MembershipController@addPlan')->name('add-plan');
    Route::post('admin/membership/updateplan', 'MembershipController@updatePlan')->name('update-plan');
    Route::get('admin/membership/{id}/editplan', 'MembershipController@editPlan')->name('edit-plan');
    Route::get('admin/membership/{id}/deleteplan', 'MembershipController@deletePlan')->name('delete-plan');
    Route::post('admin/membership/posteditplan', 'MembershipController@posteditPlan')->name('post-edit-plan');
    Route::post('ajax/batch_ops', ['as' => 'ajax.batch_ops', 'uses' => 'AjaxController@batch_ops']);
    ## Religion module Routes ###
    Route::get('admin/manage-religions', 'ReligionController@listReligions')->name('religions');
    Route::get('admin/manage-religions/{id}/edit', 'ReligionController@editReligions')->name('editReligions');
    Route::get('admin/manage-religions/create', 'ReligionController@createReligions')->name('createReligions');
    Route::post('admin/manage-religions/save', 'ReligionController@saveReligion')->name('saveReligion');
    Route::post('admin/manage-religions/{id}/update', 'ReligionController@updateReligion')->name('updateReligion');
    Route::get('admin/manage-religions/{id}/delete', 'ReligionController@deleteReligion')->name('deleteReligion');
    ## @end Religion module Routes ##
    ## Manage Adnmin->Country ##
    Route::get('admin/manage-countries', 'BasicinfoController@listCountries')->name('admin-countries');
    ## END: Manage Adnmin->Country ##

    ## Manage Admin->States ##
    Route::get('admin/manage-states', 'BasicinfoController@getStates')->name('admin-states');
    Route::get('admin/manage/{id}/editstate', 'BasicinfoController@editState')->name('edit-state');
    Route::post('admin/manage/posteditstate', 'BasicinfoController@posteditState')->name('post-edit-state');
    Route::get('admin/manage/addstate', 'BasicinfoController@addState')->name('add-state');
    Route::post('admin/manage/updatestate', 'BasicinfoController@updateState')->name('update-state');
    Route::get('admin/manage/{id}/deletestate', 'BasicinfoController@deleteState')->name('delete-state');
    ## Manage END: Admin->States ##

    ## Manage Admin->Cities ##
    Route::get('admin/manage-cities', 'BasicinfoController@getCities')->name('admin-cities');
    Route::get('admin/manage/{id}/editcity', 'BasicinfoController@editCity')->name('edit-city');
    Route::post('admin/manage/posteditcity', 'BasicinfoController@posteditCity')->name('post-edit-city');
    Route::get('admin/manage/addcity', 'BasicinfoController@addCity')->name('add-city');
    Route::post('admin/manage/updatecity', 'BasicinfoController@updateCity')->name('update-city');
    Route::get('admin/manage/{id}/deletecity', 'BasicinfoController@deleteCity')->name('delete-city');
    ## Manage End: Admin->Cities ##

    ## Manage Admin->Sects ##
    Route::get('admin/manage-sects', 'BasicinfoController@getSects')->name('admin-sects');
    Route::get('admin/manage/{id}/editsects', 'BasicinfoController@editSect')->name('edit-sects');
    Route::post('admin/manage/posteditsects', 'BasicinfoController@posteditSect')->name('post-edit-sects');
    Route::get('admin/manage/addsects', 'BasicinfoController@addSect')->name('add-sects');
    Route::post('admin/manage/updatesects', 'BasicinfoController@updateSect')->name('update-sects');
    Route::get('admin/manage/{id}/deletesects', 'BasicinfoController@deleteSect')->name('delete-sects');
    ## Manage END: Admin->Sects ##

    ## Manage Admin->Subcaste ##
    Route::get('admin/manage-subcastes', 'BasicinfoController@getSubcastes')->name('admin-subcastes');
    Route::get('admin/manage/{id}/editsubcaste', 'BasicinfoController@editSubcaste')->name('edit-subcaste');
    Route::post('admin/manage/posteditsubcaste', 'BasicinfoController@posteditSubcaste')->name('post-edit-subcaste');
    Route::get('admin/manage/addsubcaste', 'BasicinfoController@addSubcaste')->name('add-subcaste');
    Route::post('admin/manage/updatesubcaste', 'BasicinfoController@updateSubcaste')->name('update-subcaste');
    Route::get('admin/manage/{id}/deletesubcaste', 'BasicinfoController@deleteSubcaste')->name('delete-subcaste');
    ## Manage End: Admin->Subcastes ##

    ## Manage Admin->Subcaste ##
    Route::get('admin/manage-languages', 'BasicinfoController@getLanguages')->name('admin-languages');
    Route::get('admin/manage/{id}/editlanguage', 'BasicinfoController@editLanguage')->name('edit-language');
    Route::post('admin/manage/posteditlanguage', 'BasicinfoController@posteditLanguage')->name('post-edit-language');
    Route::get('admin/manage/addlanguage', 'BasicinfoController@addLanguage')->name('add-language');
    Route::post('admin/manage/updatelanguage', 'BasicinfoController@updateLanguage')->name('update-language');
    Route::get('admin/manage/{id}/deletelanguage', 'BasicinfoController@deleteLanguage')->name('delete-language');
    ## Manage End: Admin->Subcastes ##

    ## Manage Admin->Banner ##
    Route::get('admin/banners', 'BannerController@getBanners')->name('admin-banners');
    Route::post('admin/banners/handle-manage-bulk-banners', 'BannerController@handleBulkActionBanner')->name('handle-manage-bulk-banners');
    Route::get('admin/banners/add', 'BannerController@addBanner')->name('add-banner');
    Route::post('admin/banners/handle-add-banner', 'BannerController@handleAddBanner')->name('handle-add-banner');
    Route::get('admin/banners/edit/{id}', 'BannerController@editBanner')->name('edit-banner');
    Route::post('admin/banners/handle-edit-banner', 'BannerController@handleEditBanner')->name('handle-edit-banner');
    ## Manage END: Admin->Banner ##

    // Added by Rajendra Banker for managing payment gateways - 20th April 2019
    Route::get('admin/manage-payment-gateways', 'ManagePaymentGatewaysController@showManagePaymentGateways')->name('manage-payment-gateways');
    Route::post('admin/handle-payment-gateways', 'ManagePaymentGatewaysController@handleManagePaymentGateways')->name('handle-payment-gateways');
    Route::get('admin/membership-on-off', 'ManagePaymentGatewaysController@showMembershipOnOff')->name('membership-on-off');
    Route::post('admin/handle-membership-on-off', 'ManagePaymentGatewaysController@handleMembershipOnOff')->name('handle-membership-on-off');

    ## Manage Admin->Satellites Management ##
    Route::get('admin/satellites', 'SatelliteController@getSatellites')->name('admin-satellites');
    Route::post('admin/satellites/handle-manage-bulk-satellites', 'SatelliteController@handleBulkActionSatellite')->name('handle-manage-bulk-satellites');
    Route::get('admin/satellites/add', 'SatelliteController@addSatellite')->name('add-satellite');
    Route::post('admin/satellites/handle-add-satellite', 'SatelliteController@handleAddSatellite')->name('handle-add-satellite');
    Route::get('admin/satellites/edit/{id}', 'SatelliteController@editSatellite')->name('edit-satellite');
    Route::post('admin/satellites/handle-edit-satellite', 'SatelliteController@handleEditSatellite')->name('handle-edit-satellite');
    ## Manage END: Admin->Satellites Management ##

    ## Manage Admin->Vendor Category Management ##
    Route::get('admin/vendor-categories', 'VendorCategoryController@get')->name('admin-vendor-categories');
    Route::post('admin/vendor-categories/handle-manage-bulk-vendor-categories', 'VendorCategoryController@handleBulkAction')->name('handle-manage-bulk-vendor-categories');
    Route::get('admin/vendor-categories/add', 'VendorCategoryController@add')->name('add-vendor-category');
    Route::post('admin/vendor-categories/handle-add-vendor-category', 'VendorCategoryController@handleAdd')->name('handle-add-vendor-category');
    Route::get('admin/vendor-categories/edit/{id}', 'VendorCategoryController@edit')->name('edit-vendor-category');
    Route::post('admin/vendor-categories/handle-edit-vendor-category', 'VendorCategoryController@handleEdit')->name('handle-edit-vendor-category');
    Route::get('admin/vendor-categories/view/{id}', 'VendorCategoryController@view')->name('view-vendor-category');
    ## Manage END: Admin->Vendor Category Management ##

    ## Manage Admin->Vendor FAQs Management ##
    Route::get('admin/vendor-faqs', 'VendorFaqController@get')->name('admin-vendor-faqs');
    Route::post('admin/vendor-faqs/handle-manage-bulk-vendor-faqs', 'VendorFaqController@handleBulkAction')->name('handle-manage-bulk-vendor-faqs');
    Route::get('admin/vendor-faqs/add', 'VendorFaqController@add')->name('add-vendor-faq');
    Route::post('admin/vendor-faqs/handle-add-vendor-faq', 'VendorFaqController@handleAdd')->name('handle-add-vendor-faq');
    Route::get('admin/vendor-faqs/edit/{id}', 'VendorFaqController@edit')->name('edit-vendor-faq');
    Route::post('admin/vendor-faqs/handle-edit-vendor-faq', 'VendorFaqController@handleEdit')->name('handle-edit-vendor-faq');
    ## Manage END Admin->Vendor FAQs Management ##

    ## Manage Admin->Vendors Management ##
    Route::resource('/vendors/deleted', 'SoftDeletesVendorController', [
        'only' => [
            'index', 'show', 'update', 'destroy',
        ]
    ]);

    Route::resource('vendors', 'VendorsManagementController', [
        'names' => [
            'index'   => 'vendors',
            'destroy' => 'user.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);
    Route::post('admin-update-vendor',['uses'=>'VendorsManagementController@admin_update_vendor'])->name('admin-update-vendor');
    Route::post('admin-vendor-video-submit', 'VendorsManagementController@handleVendorVideoSubmit')->name('admin-vendor-video-submit');
    Route::get('admin-delete-vendor-video/{id}', 'VendorsManagementController@handleVendorVideoDelete')->name('admin-delete-vendor-video');
    Route::post('/admin/delete-vendor-image', 'VendorsManagementController@deleteImage')->name('admin-delete-vendor-image');
    Route::post('ajax/approveVendorPhoto', ['as' => 'ajax.approveVendorPhoto', 'uses' => 'AjaxController@approveVendorPhoto']);
    Route::post('ajax/disapproveVendorPhoto', ['as' => 'ajax.disapproveVendorPhoto', 'uses' => 'AjaxController@disapproveVendorPhoto']);
    Route::post('ajax/approveVendorVideo', ['as' => 'ajax.approveVendorVideo', 'uses' => 'AjaxController@approveVendorVideo']);
    Route::post('ajax/disapproveVendorVideo', ['as' => 'ajax.disapproveVendorVideo', 'uses' => 'AjaxController@disapproveVendorVideo']);
    Route::post('ajax/approveVendorCustomFaq', ['as' => 'ajax.approveVendorCustomFaq', 'uses' => 'AjaxController@approveVendorCustomFaq']);
    Route::post('ajax/disapproveVendorCustomFaq', ['as' => 'ajax.disapproveVendorCustomFaq', 'uses' => 'AjaxController@disapproveVendorCustomFaq']);
    Route::get('admin/latest-updates-vendors', 'VendorsManagementController@showLatestUpdates')->name('latest-updates-vendors');
    // Temp route for admin only to update the vendor company slugs
    Route::get('admin/update-vendor-company-slug', 'VendorsManagementController@updateAllVendorsComapnySlugs');

    Route::get('active-vendors', 'VendorsManagementController@getActiveUsers')->name('active-vendors');
    Route::get('banned-vendors', 'VendorsManagementController@getBannedUsers')->name('banned-vendors');
    //Route::get('featured-vendors', 'VendorsManagementController@getFeaturedUsers')->name('featured-vendors');
    Route::get('deactivated-vendors', 'VendorsManagementController@getDeactivatedUsers')->name('deactivated-vendors');
    Route::get('inactive-vendors', 'VendorsManagementController@getInactiveUsers')->name('inactive-vendors');
    Route::get('incomplete-vendors', 'VendorsManagementController@getIncompleteUsers')->name('incomplete-vendors');

    Route::post('ajax-upload-admin-vendor-cover', 'VendorsManagementController@imageCoverUpload')->name('ajax-upload-admin-vendor-cover');
    Route::post('ajax-upload-admin-vendor-profile', 'VendorsManagementController@imageProfileUpload')->name('ajax-upload-admin-vendor-profile');
    ## Manage END: Admin->Vendors Management ##

    ## Event Managements STARTED ##
    Route::get('admin/events', 'EventController@getAdminRecords')->name('admin-events');
    Route::post('admin/events/handle-manage-bulk-events', 'EventController@handleBulkAction')->name('handle-manage-bulk-events');
    Route::get('admin/events/add', 'EventController@adminAdd')->name('admin-add-event');
    Route::post('admin/events/handle-admin-add-event', 'EventController@handleAdminAdd')->name('handle-admin-add-event');
    Route::get('admin/events/edit/{id}', 'EventController@adminEdit')->name('admin-edit-event');
    Route::post('admin/events/handle-admin-edit-event', 'EventController@handleAdminEdit')->name('handle-admin-edit-event');
    Route::post('ajax/approveEventPhoto', ['as' => 'ajax.approveEventPhoto', 'uses' => 'EventController@approveEventPhoto']);
    Route::post('ajax/disapproveEventPhoto', ['as' => 'ajax.disapproveEventPhoto', 'uses' => 'EventController@disapproveEventPhoto']);
    Route::post('/admin/delete-event-image', 'EventController@deleteImage')->name('admin-delete-event-image');
    // Events Reviews
    Route::get('admin/events-reviews/list', 'EventsReviewsController@showAdminListings')->name('admin-events-reviews-list');
    Route::get('admin/events-reviews/edit/{id}', 'EventsReviewsController@showEditReview')->name('admin-events-reviews-edit');
    Route::post('admin/handle-edit-event-review', 'EventsReviewsController@handleEditreview')->name('handle-edit-event-review');
    Route::get('admin/events-reviews/view/{id}', 'EventsReviewsController@showViewReview')->name('admin-events-reviews-view');
    Route::post('admin/handle-manage-bulk-event-reviews', 'EventsReviewsController@handleBulkActionReview')->name('handle-manage-bulk-event-reviews');
    ## Event Managements ENDED ##
    ## Artist Managements STARTED ##
    Route::get('admin/artists', 'ArtistController@getAdminRecords')->name('admin-artists');
    Route::post('admin/artists/handle-manage-bulk-artists', 'ArtistController@handleBulkAction')->name('handle-manage-bulk-artists');
    Route::get('admin/artists/add', 'ArtistController@adminAdd')->name('admin-add-artist');
    Route::post('admin/artists/handle-admin-add-artist', 'ArtistController@handleAdminAdd')->name('handle-admin-add-artist');
    Route::get('admin/artists/edit/{id}', 'ArtistController@adminEdit')->name('admin-edit-artist');
    Route::post('admin/artists/handle-admin-edit-artist', 'ArtistController@handleAdminEdit')->name('handle-admin-edit-artist');
    Route::post('/admin/delete-artist-image', 'ArtistController@deleteImage')->name('admin-delete-artist-image');
    ## Artist Managements STARTED ##
});

Route::get('reviews','ReviewsController@getReviews')->name('reviews');
Route::redirect('/php', '/phpinfo', 301);
Route::post('/sendcontact','CmsController@sendContact')->name('send-contact');
Route::get('/contact','CmsController@contact')->name('contact');
Route::get('/sitemap','CmsController@sitemap')->name('sitemap');
Route::get('/blog','BlogController@getBlogs')->name('blogs');
Route::get('/blog/{slug}', 'BlogController@getBlogInfo')->name('blog-detail');
Route::post('/blogcomments','BlogController@addComment')->name('blog-comments');
Route::get('/{slug}.php','CmsController@getPageInfo')->name('cmsroots');


Route::get('{category?}', 'VendorController@showSearchVendors')->name('search-vendor');
Route::get('muslim-vendors', 'VendorController@showSearchVendors')->name('muslim-vendors');
//// Subscription mail ajax handle Rajendra Banker on 2nd May 2019
Route::post('ajax/subscribe-newsletter-action', 'NewsLetterEmailSmsController@ajaxHandleNewsLetterSubscription')->name('ajax-subscribe-newsletter-action');