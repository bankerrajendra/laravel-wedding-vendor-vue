<?php

namespace Nahid\Talk\Conversations;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Conversation extends Model
{
    protected $table = 'conversations';
    public $timestamps = true;
    public $fillable = [
        'user_one',
        'user_two',
        'status',
    ];

    /*
     * make a relation between message
     *
     * return collection
     * */
    public function messages()
    {
        return $this->hasMany('Nahid\Talk\Messages\Message', 'conversation_id')
            ->with('sender');
    }

    /*
     * make a relation between first user from conversation
     *
     * return collection
     * */
    public function userone()
    {
        return $this->belongsTo(config('talk.user.model', 'App\User'),  'user_one');
    }

    /*
   * make a relation between second user from conversation
   *
   * return collection
   * */
    public function usertwo()
    {
        return $this->belongsTo(config('talk.user.model', 'App\User'),  'user_two');
    }

    public function scopeSkipWhoBlockedByMe($query, $userId=null){
        if($userId){
            $user = User::find($userId);
        }
        else{
            $user = Auth::user();
        }
        return $query->whereNotIn('user_one', function($subQuery) use ($user){
            $subQuery->select('to_user')->from('blocked_users')->where('from_user','=', $user->id);
        })->whereNotIn('user_two', function($subQuery) use ($user){
            $subQuery->select('to_user')->from('blocked_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipWhoBlockedMe($query, $userId=null){
        if($userId){
            $user = User::find($userId);
        }
        else{
            $user = Auth::user();
        }
        return $query->whereNotIn('user_one', function($subQuery) use ($user){
            $subQuery->select('from_user')->from('blocked_users')->where('to_user','=', $user->id);
        })->whereNotIn('user_two', function($subQuery) use ($user){
            $subQuery->select('from_user')->from('blocked_users')->where('to_user','=', $user->id);
        });
    }

    public function scopeSkipSkippedUsers($query, $userId=null){
        if($userId){
            $user = User::find($userId);
        }
        else{
            $user = Auth::user();
        }
        return $query->whereNotIn('user_one', function($subQuery) use ($user){
            $subQuery->select('to_user')->from('skipped_users')->where('from_user','=', $user->id);
        })->whereNotIn('user_two', function($subQuery) use ($user){
            $subQuery->select('to_user')->from('skipped_users')->where('from_user','=', $user->id);
        });
    }

    public function scopeSkipWhoSkippedMe($query, $userId=null){
        if($userId){
            $user = User::find($userId);
        }
        else{
            $user = Auth::user();
        }
        return $query->whereNotIn('user_one', function($subQuery) use ($user){
            $subQuery->select('from_user')->from('skipped_users')->where('to_user','=', $user->id);
        })->whereNotIn('user_two', function($subQuery) use ($user){
            $subQuery->select('from_user')->from('skipped_users')->where('to_user','=', $user->id);
        });
    }

    public function scopeSkipDeletedUsers($query){
        return $query->whereNotIn('user_one', function($subQuery){
            $subQuery->select('id')->from('users')->where('deleted_at','!=', null);
        })->whereNotIn('user_two', function($subQuery){
            $subQuery->select('id')->from('users')->where('deleted_at','!=', null);
        });
    }

    // not use the permanently deleted users
    public function scopeIncludeExistingUsers($query) {
        return $query->whereIn('user_two', function ($subQuery){
            $subQuery->select('id')->from('users')->where('id',\DB::raw('user_two'));
        })->whereIn('user_one', function($subQuery) {
            $subQuery->select('id')->from('users')->where('id',\DB::raw('user_one'));
        });


    }
}
