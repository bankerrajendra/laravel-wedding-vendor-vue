<?php

use Illuminate\Database\Seeder;

use App\Models\Religion;
use App\Models\Community;
use App\Models\Language;
use App\Models\CmsCityState;

class AddOtherInCmsCityStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->seedReligion();
        $this->seedLanguage();

    }

    public function seedReligion() {
        $religions=Religion::where('id',3)->get();

        foreach ($religions as $religion) {
            CmsCityState::create([
                'page_type' => 'Religion',
                'type_id' => $religion->id,
                'type_name' => $religion->religion,
                'slug' => strtolower(str_replace(' ', '-', trim($religion->religion))),
                'home_page_visibility' => 1,
                'status' => 1,
                'site_id'  => 1,
            ]);
            $this->seedCommunity($religion->id);
        }

    }

    public function seedCommunity($religionId) {
        $communities=Community::where('religion_id',$religionId)->get();
        foreach ($communities as $community) {
            CmsCityState::create([
                'page_type' => 'Community',
                'type_id' => $community->id,
                'type_name' => $community->name,
                'slug' => strtolower(str_replace(' ', '-', trim($community->name))),
                'home_page_visibility' => 1,
                'status' => 1,
                'site_id'  => 1,
            ]);
        }
    }

    public function seedLanguage() {
        $languages=Language::where('status',1)->get();
        foreach ($languages as $language) {
            CmsCityState::create([
                'page_type' => 'Language',
                'type_id' => $language->old_id,
                'type_name' => $language->language,
                'slug' => strtolower(str_replace(' ', '-', trim($language->language))),
                'home_page_visibility' => 1,
                'status' => 1,
                'site_id'  => 1,
            ]);
        }
    }
}
