<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Community;
class CommunitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Schema::disableForeignKeyConstraints();

        Community::truncate();
        Schema::enableForeignKeyConstraints();
        $communities = array(
            array('id'=>'1','old_id'=>'624','religion_id'=>'3', 'name'=>'Muslim',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'2','old_id'=>'483','religion_id'=>'3', 'name'=>'Maliki' ,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'3','old_id'=>'484','religion_id'=>'3', 'name'=>'Shafi',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'4','old_id'=>'465','religion_id'=>'3', 'name'=>'Shia',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'5','old_id'=>'466','religion_id'=>'3', 'name'=>'Sunni',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'6','old_id'=>'481','religion_id'=>'3', 'name'=>'Hanabali',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'7','old_id'=>'482','religion_id'=>'3', 'name'=>'Hanafi',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
			array('id'=>'8','old_id'=>'482','religion_id'=>'3', 'name'=>'Others',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
			
        );
        DB::table('communities')->insert($communities);
    }
}
