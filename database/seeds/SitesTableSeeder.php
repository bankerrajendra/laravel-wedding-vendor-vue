<?php

use Illuminate\Database\Seeder;
use App\Models\Site;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Schema::disableForeignKeyConstraints();
        Site::truncate();
        Schema::enableForeignKeyConstraints();
        $sites = array(
            array('id'=>'1','name'=>'Muslimwedding','site_url'=>'www.muslimwedding.com','site_title'=>'Muslim Wedding','logo_path'=>'logo.png','status'=>'1','site_user_code'=>'MWED'),
            array('id'=>'2','name'=>'Punjabiwedding','site_url'=>'www.punjabiwedding.com','site_title'=>'Punjabi Wedding','logo_path'=>'logo.png','status'=>'1','site_user_code'=>'PWED'),
            array('id'=>'3','name'=>'Hinduwedding','site_url'=>'www.hinduwedding.com','site_title'=>'Hindu Wedding','logo_path'=>'logo.png','status'=>'1','site_user_code'=>'HWED'),
        );
        DB::table('sites')->insert($sites);
    }
}
