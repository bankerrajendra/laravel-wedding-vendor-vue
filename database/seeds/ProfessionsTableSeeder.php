<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProfessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('professions')->delete();
        $professions = array(
            array('id' => '1','profession' => 'Lecturer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '2','profession' => 'Sports', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '3','profession' => 'Banking Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '4','profession' => 'Chartered Accountant', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '5','profession' => 'Admin Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '6','profession' => 'Company Secretary', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '7','profession' => 'Finance Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '8','profession' => 'Accounting Professional Others', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '9','profession' => 'Human Resources Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '10','profession' => 'Actor', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '11','profession' => 'Advertising Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '12','profession' => 'Entertainment Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '13','profession' => 'Event Manager', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '14','profession' => 'Journalist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '15','profession' => 'Media Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '16','profession' => 'Public Relations Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '17','profession' => 'Farming', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '18','profession' => 'Horticulturist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '19','profession' => 'Agricultural Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '20','profession' => 'Air Hostess / Flight Attendant', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '21','profession' => 'Pilot / Co-Pilot', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '22','profession' => 'Other Airline Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '23','profession' => 'Architect', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '24','profession' => 'Interior Designer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '25','profession' => 'Landscape Architect', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '26','profession' => 'Artists, Animators & Web Designers', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '27','profession' => 'Animator', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '28','profession' => 'Commercial Artist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '29','profession' => 'Web / UX Designers', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '30','profession' => 'Artist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '31','profession' => 'Beautician', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '32','profession' => 'Fashion Designer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '33','profession' => 'Hairstylist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '34','profession' => 'Jewellery Designer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '35','profession' => 'Designer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '36','profession' => 'Customer Support / BPO / KPO Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '37','profession' => 'IAS / IRS / IES / IFS', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '38','profession' => 'Indian Police Services (IPS)', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '39','profession' => 'Law Enforcement Employee', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '40','profession' => 'Airforce', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '41','profession' => 'Army', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '42','profession' => 'Navy', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '43','profession' => 'Defense Services', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '44','profession' => 'Professor', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '45','profession' => 'Research Assistant', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '46','profession' => 'Research Scholar', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '47','profession' => 'Teacher', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '48','profession' => 'Training Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '49','profession' => 'Civil Engineer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '50','profession' => 'Electronics / Telecom Engineer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '51','profession' => 'Mechanical / Production Engineer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '52','profession' => 'Non IT Engineer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '53','profession' => 'Chef / Sommelier / Food Critic', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '54','profession' => 'Catering Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '55','profession' => 'Hotel & Hospitality Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '56','profession' => 'Software Developer / Programmer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '57','profession' => 'Software Consultant', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '58','profession' => 'Hardware & Networking professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '59','profession' => 'Software Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '60','profession' => 'Lawyer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '61','profession' => 'Legal Assistant', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '62','profession' => 'Legal Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '63','profession' => 'Dentist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '64','profession' => 'Doctor', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '65','profession' => 'Medical Transcriptionist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '66','profession' => 'Nurse', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '67','profession' => 'Pharmacist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '68','profession' => 'Physician Assistant', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '69','profession' => 'Physiotherapist / Occupational Therapist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '70','profession' => 'Psychologist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '71','profession' => 'Surgeon', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '72','profession' => 'Veterinary Doctor', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '73','profession' => 'Therapist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '74','profession' => 'Medical / Healthcare Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '75','profession' => 'Merchant Naval Officer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '76','profession' => 'Mariner', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '77','profession' => 'Marketing Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '78','profession' => 'Sales Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '79','profession' => 'Biologist / Botanist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '80','profession' => 'Physicist', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '81','profession' => 'Science Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '82','profession' => 'CEO / Chairman / Director / President', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '83','profession' => 'VP / AVP / GM / DGM', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '84','profession' => 'Sr. Manager / Manager', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '85','profession' => 'Consultant / Supervisor / Team Leads', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '86','profession' => 'Agent / Broker / Trader / Contractor', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '87','profession' => 'Business Owner / Entrepreneur', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '88','profession' => 'Politician', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '89','profession' => 'Social Worker / Volunteer / NGO', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '90','profession' => 'Sportsman', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '91','profession' => 'Travel & Transport Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '92','profession' => 'Writer', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '93','profession' => 'Student', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '94','profession' => 'Advertising Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => '95','profession' => 'Not working', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now())
        );
        DB::table('professions')->insert($professions);
    }
}
