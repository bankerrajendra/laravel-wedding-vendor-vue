<?php

use Illuminate\Database\Seeder;
use App\Models\MembershipBenefits;

class MembershipBenefitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('membership_benefits')->delete();
        $benefits = array(
            array('id' => '1','name' => 'Send &amp; Receive Unlimited Messages','status' => 'A'),
            array('id' => '2','name' => 'Send invitations with profile details','status' => 'A'),
            array('id' => '3','name' => 'Reach out and connect','status' => 'A'),
            array('id' => '4','name' => 'Selected New Matches','status' => 'A'),
            array('id' => '5','name' => 'Selected Partner Preferences','status' => 'A'),
            array('id' => '6','name' => 'Display profile in your selected country','status' => 'A'),
            array('id' => '7','name' => 'Priority Over Free Members','status' => 'A'),
            array('id' => '8','name' => 'Can see which members are Diamond','status' => 'A'),
            array('id' => '9','name' => 'Show up first on Dashboard/Connect!','status' => 'A'),
            array('id' => '10','name' => 'Stand out in all searches Platinum','status' => 'A'),
            array('id' => '11','name' => 'Find out the date and time someone viewed your profile','status' => 'A'),
            array('id' => '12','name' => 'See who viewed your profile','status' => 'A'),
            array('id' => '13','name' => 'See who wants to meet you','status' => 'A'),
            array('id' => '14','name' => 'This upgrade doubles your chances of meeting someone','status' => 'A'),
            array('id' => '15','name' => 'Upload 10 images','status' => 'A'),
            array('id' => '16','name' => 'Send & Receive Unlimited Messages 1','status' => 'A')
        );
        DB::table('membership_benefits')->insert($benefits);
    }
}
