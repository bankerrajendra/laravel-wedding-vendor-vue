<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EducationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('educations')->delete();
        $educations = array(
            array('id' => 7,'education'=>'Graduate Degree', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 8,'education'=>'None', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 10,'education'=>'Doctoral', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 11,'education'=>'Master\'s', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 12,'education'=>'Some College', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 13,'education'=>'Bachelor\'s', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 14,'education'=>'Undergraduate', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 15,'education'=>'Associate\'s Degree', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 17,'education'=>'High School', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 18,'education'=>'Professional Degree', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 19,'education'=>'Trade school', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 20,'education'=>'College Degree', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 21,'education'=>'Intermediate', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 22,'education'=>'Post Graduate', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 23,'education'=>'Phd/Post Doctoral', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 24,'education'=>'Other', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now())
        );

        DB::table('educations')->insert($educations);
    }
}
