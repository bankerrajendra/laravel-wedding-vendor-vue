<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Subcaste;

class SubcastesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Schema::disableForeignKeyConstraints();
        Subcaste::truncate();
        Schema::enableForeignKeyConstraints();
        $subcastes = array(
            array('id'=>'1','name'=>'No Caste','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'2','name'=>'Ansari','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'3','name'=>'Arain','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'4','name'=>'Awan','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'5','name'=>'Bohra','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'6','name'=>'Brohi','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'7','name'=>'Butt','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'8','name'=>'Chanio','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'9','name'=>'Dekkani','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'10','name'=>'Dogar','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'11','name'=>'Dudekula','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'12','name'=>'Durrani','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'13','name'=>'Farooqui','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'14','name'=>'Gujjar','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'15','name'=>'Jamali','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'16','name'=>'Jamot','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'17','name'=>'Janjua','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'18','name'=>'Jat','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'19','name'=>'Jatoi','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'20','name'=>'Khattak','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'21','name'=>'Khawaja','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'22','name'=>'Khoja','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'23','name'=>'Khokhar','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'24','name'=>'Khosa','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'25','name'=>'Lebbai','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'26','name'=>'Lodhi','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'27','name'=>'Mapla','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'28','name'=>'Maraicar','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'29','name'=>'Memon','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'30','name'=>'Minhas','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'31','name'=>'Mirani','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'32','name'=>'Mirza','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'33','name'=>'Mughal','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'34','name'=>'Naqvi','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'35','name'=>'Pathan','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'36','name'=>'Qureshi','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'37','name'=>'Rajput','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'38','name'=>'Rana','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'39','name'=>'Rowther','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'40','name'=>'Sheikh','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'41','name'=>'Siddiqui','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'42','name'=>'Sipra','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'43','name'=>'Solangi','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id'=>'44','name'=>'Syed','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
			array('id'=>'45','name'=>'Others','created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
			
        );
        DB::table('subcastes')->insert($subcastes);
    }
}
