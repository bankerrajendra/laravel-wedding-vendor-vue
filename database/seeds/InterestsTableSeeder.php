<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interests')->delete();
        $interests = array(
            array('id' => 1,'interest'=>'Animals', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 2,'interest'=>'Cars', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 3,'interest'=>'Charitable activities', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 4,'interest'=>'Gadgets', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 5,'interest'=>'Dancing', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 6,'interest'=>'Hiking', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 7,'interest'=>'Internet', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 8,'interest'=>'Music', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 9,'interest'=>'Painting', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 10,'interest'=>'Shopping', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 11,'interest'=>'Photography', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 12,'interest'=>'Reading', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 13,'interest'=>'Sports', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 14,'interest'=>'Couch surfing', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 15,'interest'=>'Theatre', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 16,'interest'=>'Travelling', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 17,'interest'=>'Writing', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 18,'interest'=>'Games', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 19,'interest'=>'Movies', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 20,'interest'=>'Eating out', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            array('id' => 21,'interest'=>'Bars/Pubs', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),

        );
        DB::table('interests')->insert($interests);
    }
}
