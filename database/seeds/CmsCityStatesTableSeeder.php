<?php

use Illuminate\Database\Seeder;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Models\CmsCityState;

class CmsCityStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $cityglobalArr;

    public function run()
    {
       // CmsCityState::truncate();
        $this->cityglobalArr=array();
        $this->seedCountries();        //
//  Manual entries in this table to do after seeder
//        INSERT INTO `cms_city_states` (`page_type`, `type_id`, `type_name`, `slug`, `home_page_visibility`, `status`, `created_at`, `updated_at`, `page_name`) VALUES
//    ('City', 19239, 'Richmond', 'richmond', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-04 02:19:45', '2018-10-04 02:19:45', 'usa'),
//('City', 20318, 'Cambridge', 'cambridge', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-04 02:19:45', '2018-10-04 02:19:45', 'usa'),
//('City', 21561, 'Huntsville', 'huntsville', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-04 02:19:45', '2018-10-04 02:19:45', 'usa'),
//('City', 21591, 'Midland', 'midland', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-04 02:19:45', '2018-10-04 02:19:45', 'usa');

    }

    public function seedCities($state_id) {
        $cities=City::where('state_id',$state_id)->get();
        foreach ($cities as $city) {

            $slug=strtolower(str_replace("'","",str_replace(' ','-',trim($city->name))));
            //$cityglobalArr
            if(!in_array($slug,$this->cityglobalArr)) {
                array_push($this->cityglobalArr, $slug);

                $code = 'usa';
                if ($city->state->country->code == 'CA') {
                    $code = 'canada';
                }
                CmsCityState::create([
                    'page_type' => 'City',
                    'page_name' => $code,
                    'type_id' => $city->id,
                    'type_name' => $city->name,
                    'slug' => $slug,
                    'home_page_visibility' => 1,
                    'status' => 1,
                ]);
            }
        }
    }

    public function seedStates($country_id) {
        $states=State::where('country_id',$country_id)->get();
        foreach ($states as $state) {
            $code='usa';
            if ($state->country->code=='CA') {
                $code='canada';
            }
            CmsCityState::create([
                'page_type'=>'State',
                'page_name'=>$code,
                'type_id'=>$state->id,
                'type_name'=>$state->name,
                'slug'=>strtolower(str_replace(' ','-',trim($state->name))),
                'home_page_visibility'=>1,
                'status' => 1,
            ]);
            $this->seedCities($state->id);
        }
    }

    public function seedCountries() {
        $countries=Country::wherein('code',['US','CA'])->get();
        foreach ($countries as $country) {
            CmsCityState::create([
                'page_type'=>'Country',
                'type_id'=>$country->id,
                'type_name'=>$country->name,
                'slug'=>strtolower(str_replace(' ','-',trim($country->name))),
                'home_page_visibility'=>1,
                'status' => 1,
            ]);
            $this->seedStates($country->id);
        }
    }
}