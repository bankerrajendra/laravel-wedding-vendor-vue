<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ConnectRelationshipsSeeder::class);
        $this->call(ThemesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(EducationsTableSeeder::class);
        $this->call(ProfessionsTableSeeder::class);
        $this->call(InterestsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CmsCityStatesTableSeeder::class);
        $this->call(CmsPagesTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(ReligionsTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);

        Model::reguard();
    }
}
