<?php

use Illuminate\Database\Seeder;

use App\Models\CmsPage;
use Carbon\Carbon;

class CmsPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*'page_title',
		'page_description',
		'meta_title',
		'meta_keyword',
		'meta_description',
		'slug',
		'status'*/
        CmsPage::insert(
            [
                [
                    'page_title'=>'About Us',
                    'page_description'=>'About Us Description',
                    'meta_title'=>'',
                    'meta_keyword'=>'',
                    'meta_description'=>'',
                    'slug'=>'aboutus',
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at'=>Carbon::now()
                ],
                [
                    'page_title'=>'How it Works',
                    'page_description'=>'How it Works Description',
                    'meta_title'=>'',
                    'meta_keyword'=>'',
                    'meta_description'=>'',
                    'slug'=>'howitworks',
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at'=>Carbon::now()
                ],
                [
                    'page_title'=>'Terms of Use',
                    'page_description'=>'Terms of Use Description',
                    'meta_title'=>'',
                    'meta_keyword'=>'',
                    'meta_description'=>'',
                    'slug'=>'term-condition',
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at'=>Carbon::now()
                ],
                [
                    'page_title'=>'Privacy Policy',
                    'page_description'=>'Privacy Policy Description',
                    'meta_title'=>'',
                    'meta_keyword'=>'',
                    'meta_description'=>'',
                    'slug'=>'privacy',
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at'=>Carbon::now()
                ],
                [
                    'page_title'=>'Disclaimers',
                    'page_description'=>'Disclaimers',
                    'meta_title'=>'',
                    'meta_keyword'=>'',
                    'meta_description'=>'',
                    'slug'=>'disclaimers',
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at'=>Carbon::now()
                ],
                [
                    'page_title'=>'Code of Conduct',
                    'page_description'=>'Code of Conduct Description',
                    'meta_title'=>'',
                    'meta_keyword'=>'',
                    'meta_description'=>'',
                    'slug'=>'code-of-conduct',
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at'=>Carbon::now()
                ],
                [
                    'page_title'=>'Sitemap',
                    'page_description'=>'Sitemap Description',
                    'meta_title'=>'',
                    'meta_keyword'=>'',
                    'meta_description'=>'',
                    'slug'=>'sitemap',
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at'=>Carbon::now()
                ],
                [
                    'page_title'=>'FAQ',
                    'page_description'=>'Faq Description',
                    'meta_title'=>'',
                    'meta_keyword'=>'',
                    'meta_description'=>'',
                    'slug'=>'faqs',
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at'=>Carbon::now()
                ]
            ]
            );
    }
}
