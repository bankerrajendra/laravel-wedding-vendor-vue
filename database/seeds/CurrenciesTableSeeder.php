<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('currencies')->delete();
        $currencies = array(
            array('id' => '1','name' => 'Indian Rupee','code' => 'INR','status' => '1'),
            array('id' => '2','name' => 'US Dollar','code' => 'USD','status' => '1'),
            array('id' => '3','name' => 'Canadian Dollar','code' => 'CAD','status' => '1'),
            array('id' => '4','name' => '(South) Korean Wonn','code' => 'KRW','status' => '1'),
            array('id' => '5','name' => 'Afghanistan Afghani','code' => 'AFA','status' => '1'),
            array('id' => '6','name' => 'Albanian Lek','code' => 'ALL','status' => '1'),
            array('id' => '7','name' => 'Algerian Dinar','code' => 'DZD','status' => '1'),
            array('id' => '8','name' => 'Andorran Peseta','code' => 'ADP','status' => '1'),
            array('id' => '9','name' => 'Angolan Kwanza','code' => 'AOK','status' => '1'),
            array('id' => '10','name' => 'Argentine Peso','code' => 'ARS','status' => '1'),
            array('id' => '11','name' => 'Armenian Dram','code' => 'AMD','status' => '1'),
            array('id' => '12','name' => 'Aruban Florin','code' => 'AWG','status' => '1'),
            array('id' => '13','name' => 'Australian Dollar','code' => 'AUD','status' => '1'),
            array('id' => '14','name' => 'Bahamian Dollar','code' => 'BSD','status' => '1'),
            array('id' => '15','name' => 'Bahraini Dinar','code' => 'BHD','status' => '1'),
            array('id' => '16','name' => 'Bangladeshi Taka','code' => 'BDT','status' => '1'),
            array('id' => '17','name' => 'Barbados Dollar','code' => 'BBD','status' => '1'),
            array('id' => '18','name' => 'Belize Dollar','code' => 'BZD','status' => '1'),
            array('id' => '19','name' => 'Bermudian Dollar','code' => 'BMD','status' => '1'),
            array('id' => '20','name' => 'Bhutan Ngultrum','code' => 'BTN','status' => '1'),
            array('id' => '21','name' => 'Bolivian Boliviano','code' => 'BOB','status' => '1'),
            array('id' => '22','name' => 'Botswanian Pula','code' => 'BWP','status' => '1'),
            array('id' => '23','name' => 'Brazilian Real','code' => 'BRL','status' => '1'),
            array('id' => '24','name' => 'British Pound','code' => 'GBP','status' => '1'),
            array('id' => '25','name' => 'Brunei Dollar','code' => 'BND','status' => '1'),
            array('id' => '26','name' => 'Bulgarian Lev','code' => 'BGN','status' => '1'),
            array('id' => '27','name' => 'Burma Kyat','code' => 'BUK','status' => '1'),
            array('id' => '28','name' => 'Burundi Franc','code' => 'BIF','status' => '1'),
            array('id' => '30','name' => 'Cape Verde Escudo','code' => 'CVE','status' => '1'),
            array('id' => '31','name' => 'Cayman Islands Dollar','code' => 'KYD','status' => '1'),
            array('id' => '32','name' => 'Chilean Peso','code' => 'CLP','status' => '1'),
            array('id' => '33','name' => 'Chilean Unidades de Fomento','code' => 'CLF','status' => '1'),
            array('id' => '34','name' => 'Colombian Peso','code' => 'COP','status' => '1'),
            array('id' => '35','name' => 'CommunautÃ© FinanciÃ¨re Africaine BCEAO - Francs','code' => 'XOF','status' => '1'),
            array('id' => '36','name' => 'CommunautÃ© FinanciÃ¨re Africaine BEAC, Francs','code' => 'XAF','status' => '1'),
            array('id' => '37','name' => 'Comoros Franc','code' => 'KMF','status' => '1'),
            array('id' => '38','name' => 'Comptoirs FranÃ§ais du Pacifique Francs','code' => 'XPF','status' => '1'),
            array('id' => '39','name' => 'Costa Rican Colon','code' => 'CRC','status' => '1'),
            array('id' => '40','name' => 'Cuban Peso','code' => 'CUP','status' => '1'),
            array('id' => '41','name' => 'Cyprus Pound','code' => 'CYP','status' => '1'),
            array('id' => '42','name' => 'Czech Republic Koruna','code' => 'CZK','status' => '1'),
            array('id' => '43','name' => 'Danish Krone','code' => 'DKK','status' => '1'),
            array('id' => '44','name' => 'Democratic Yemeni Dinar','code' => 'YDD','status' => '1'),
            array('id' => '45','name' => 'Dominican Peso','code' => 'DOP','status' => '1'),
            array('id' => '46','name' => 'East Caribbean Dollar','code' => 'XCD','status' => '1'),
            array('id' => '47','name' => 'East Timor Escudo','code' => 'TPE','status' => '1'),
            array('id' => '48','name' => 'Ecuador Sucre','code' => 'ECS','status' => '1'),
            array('id' => '49','name' => 'Egyptian Pound','code' => 'EGP','status' => '1'),
            array('id' => '50','name' => 'El Salvador Colon','code' => 'SVC','status' => '1'),
            array('id' => '51','name' => 'Estonian Kroon (EEK)','code' => 'EEK','status' => '1'),
            array('id' => '52','name' => 'Ethiopian Birr','code' => 'ETB','status' => '1'),
            array('id' => '53','name' => 'Euro','code' => 'EUR','status' => '1'),
            array('id' => '54','name' => 'Falkland Islands Pound','code' => 'FKP','status' => '1'),
            array('id' => '55','name' => 'Fiji Dollar','code' => 'FJD','status' => '1'),
            array('id' => '56','name' => 'Gambian Dalasi','code' => 'GMD','status' => '1'),
            array('id' => '57','name' => 'Ghanaian Cedi','code' => 'GHC','status' => '1'),
            array('id' => '58','name' => 'Gibraltar Pound','code' => 'GIP','status' => '1'),
            array('id' => '59','name' => 'Gold, Ounces','code' => 'XAU','status' => '1'),
            array('id' => '60','name' => 'Guatemalan Quetzal','code' => 'GTQ','status' => '1'),
            array('id' => '61','name' => 'Guinea Franc','code' => 'GNF','status' => '1'),
            array('id' => '62','name' => 'Guinea-Bissau Peso','code' => 'GWP','status' => '1'),
            array('id' => '63','name' => 'Guyanan Dollar','code' => 'GYD','status' => '1'),
            array('id' => '64','name' => 'Haitian Gourde','code' => 'HTG','status' => '1'),
            array('id' => '65','name' => 'Honduran Lempira','code' => 'HNL','status' => '1'),
            array('id' => '66','name' => 'Hong Kong Dollar','code' => 'HKD','status' => '1'),
            array('id' => '67','name' => 'Hungarian Forint','code' => 'HUF','status' => '1'),
            array('id' => '69','name' => 'Indonesian Rupiah','code' => 'IDR','status' => '1'),
            array('id' => '70','name' => 'International Monetary Fund (IMF) Special Drawing','code' => 'XDR','status' => '1'),
            array('id' => '71','name' => 'Iranian Rial','code' => 'IRR','status' => '1'),
            array('id' => '72','name' => 'Iraqi Dinar','code' => 'IQD','status' => '1'),
            array('id' => '73','name' => 'Irish Punt','code' => 'IEP','status' => '1'),
            array('id' => '74','name' => 'Israeli Shekel','code' => 'ILS','status' => '1'),
            array('id' => '75','name' => 'Jamaican Dollar','code' => 'JMD','status' => '1'),
            array('id' => '76','name' => 'Japanese Yen','code' => 'JPY','status' => '1'),
            array('id' => '77','name' => 'Jordanian Dinar','code' => 'JOD','status' => '1'),
            array('id' => '78','name' => 'Kampuchean (Cambodian) Riel','code' => 'KHR','status' => '1'),
            array('id' => '79','name' => 'Kenyan Schilling','code' => 'KES','status' => '1'),
            array('id' => '80','name' => 'Kuwaiti Dinar','code' => 'KWD','status' => '1'),
            array('id' => '81','name' => 'Lao Kip','code' => 'LAK','status' => '1'),
            array('id' => '82','name' => 'Lebanese Pound','code' => 'LBP','status' => '1'),
            array('id' => '83','name' => 'Lesotho Loti','code' => 'LSL','status' => '1'),
            array('id' => '84','name' => 'Liberian Dollar','code' => 'LRD','status' => '1'),
            array('id' => '85','name' => 'Libyan Dinar','code' => 'LYD','status' => '1'),
            array('id' => '86','name' => 'Macau Pataca','code' => 'MOP','status' => '1'),
            array('id' => '87','name' => 'Malagasy Franc','code' => 'MGF','status' => '1'),
            array('id' => '88','name' => 'Malawi Kwacha','code' => 'MWK','status' => '1'),
            array('id' => '89','name' => 'Malaysian Ringgit','code' => 'MYR','status' => '1'),
            array('id' => '90','name' => 'Maldive Rufiyaa','code' => 'MVR','status' => '1'),
            array('id' => '91','name' => 'Maltese Lira','code' => 'MTL','status' => '1'),
            array('id' => '92','name' => 'Mauritanian Ouguiya','code' => 'MRO','status' => '1'),
            array('id' => '93','name' => 'Mauritius Rupee','code' => 'MUR','status' => '1'),
            array('id' => '94','name' => 'Mexican Peso','code' => 'MXP','status' => '1'),
            array('id' => '95','name' => 'Mongolian Tugrik','code' => 'MNT','status' => '1'),
            array('id' => '96','name' => 'Moroccan Dirham','code' => 'MAD','status' => '1'),
            array('id' => '97','name' => 'Mozambique Metical','code' => 'MZM','status' => '1'),
            array('id' => '98','name' => 'Namibian Dollar','code' => 'NAD','status' => '1'),
            array('id' => '99','name' => 'Nepalese Rupee','code' => 'NPR','status' => '1'),
            array('id' => '100','name' => 'Netherlands Antillian Guilder','code' => 'ANG','status' => '1'),
            array('id' => '101','name' => 'New Yugoslavia Dinar','code' => 'YUD','status' => '1'),
            array('id' => '102','name' => 'New Zealand Dollar','code' => 'NZD','status' => '1'),
            array('id' => '103','name' => 'Nicaraguan Cordoba','code' => 'NIO','status' => '1'),
            array('id' => '104','name' => 'Nigerian Naira','code' => 'NGN','status' => '1'),
            array('id' => '105','name' => 'North Korean Won','code' => 'KPW','status' => '1'),
            array('id' => '106','name' => 'Norwegian Kroner','code' => 'NOK','status' => '1'),
            array('id' => '107','name' => 'Omani Rial','code' => 'OMR','status' => '1'),
            array('id' => '108','name' => 'Pakistan Rupee','code' => 'PKR','status' => '1'),
            array('id' => '109','name' => 'Palladium Ounces','code' => 'XPD','status' => '1'),
            array('id' => '110','name' => 'Panamanian Balboa','code' => 'PAB','status' => '1'),
            array('id' => '111','name' => 'Papua New Guinea Kina','code' => 'PGK','status' => '1'),
            array('id' => '112','name' => 'Paraguay Guarani','code' => 'PYG','status' => '1'),
            array('id' => '113','name' => 'Peruvian Nuevo Sol','code' => 'PEN','status' => '1'),
            array('id' => '114','name' => 'Philippine Peso','code' => 'PHP','status' => '1'),
            array('id' => '115','name' => 'Platinum, Ounces','code' => 'XPT','status' => '1'),
            array('id' => '116','name' => 'Polish Zloty','code' => 'PLN','status' => '1'),
            array('id' => '117','name' => 'Qatari Rial','code' => 'QAR','status' => '1'),
            array('id' => '118','name' => 'Romanian Leu','code' => 'RON','status' => '1'),
            array('id' => '119','name' => 'Russian Ruble','code' => 'RUB','status' => '1'),
            array('id' => '120','name' => 'Rwanda Franc','code' => 'RWF','status' => '1'),
            array('id' => '121','name' => 'Samoan Tala','code' => 'WST','status' => '1'),
            array('id' => '122','name' => 'Sao Tome and Principe Dobra','code' => 'STD','status' => '1'),
            array('id' => '123','name' => 'Saudi Arabian Riyal','code' => 'SAR','status' => '1'),
            array('id' => '124','name' => 'Seychelles Rupee','code' => 'SCR','status' => '1'),
            array('id' => '125','name' => 'Sierra Leone Leone','code' => 'SLL','status' => '1'),
            array('id' => '126','name' => 'Silver, Ounces','code' => 'XAG','status' => '1'),
            array('id' => '127','name' => 'Singapore Dollar','code' => 'SGD','status' => '1'),
            array('id' => '128','name' => 'Slovak Koruna','code' => 'SKK','status' => '1'),
            array('id' => '129','name' => 'Solomon Islands Dollar','code' => 'SBD','status' => '1'),
            array('id' => '130','name' => 'Somali Schilling','code' => 'SOS','status' => '1'),
            array('id' => '131','name' => 'South African Rand','code' => 'ZAR','status' => '1'),
            array('id' => '132','name' => 'Sri Lanka Rupee','code' => 'LKR','status' => '1'),
            array('id' => '133','name' => 'St. Helena Pound','code' => 'SHP','status' => '1'),
            array('id' => '134','name' => 'Sudanese Pound','code' => 'SDP','status' => '1'),
            array('id' => '135','name' => 'Suriname Guilder','code' => 'SRG','status' => '1'),
            array('id' => '136','name' => 'Swaziland Lilangeni','code' => 'SZL','status' => '1'),
            array('id' => '137','name' => 'Swedish Krona','code' => 'SEK','status' => '1'),
            array('id' => '138','name' => 'Swiss Franc','code' => 'CHF','status' => '1'),
            array('id' => '139','name' => 'Syrian Potmd','code' => 'SYP','status' => '1'),
            array('id' => '140','name' => 'Taiwan Dollar','code' => 'TWD','status' => '1'),
            array('id' => '141','name' => 'Tanzanian Schilling','code' => 'TZS','status' => '1'),
            array('id' => '142','name' => 'Thai Baht','code' => 'THB','status' => '1'),
            array('id' => '143','name' => 'Tongan Paanga','code' => 'TOP','status' => '1'),
            array('id' => '144','name' => 'Trinidad and Tobago Dollar','code' => 'TTD','status' => '1'),
            array('id' => '145','name' => 'Tunisian Dinar','code' => 'TND','status' => '1'),
            array('id' => '146','name' => 'Turkish Lira','code' => 'TRY','status' => '1'),
            array('id' => '147','name' => 'Uganda Shilling','code' => 'UGX','status' => '1'),
            array('id' => '148','name' => 'United Arab Emirates Dirham','code' => 'AED','status' => '1'),
            array('id' => '149','name' => 'Uruguayan Peso','code' => 'UYU','status' => '1'),
            array('id' => '151','name' => 'Vanuatu Vatu','code' => 'VUV','status' => '1'),
            array('id' => '152','name' => 'Venezualan Bolivar','code' => 'VEF','status' => '1'),
            array('id' => '153','name' => 'Vietnamese Dong','code' => 'VND','status' => '1'),
            array('id' => '154','name' => 'Yemeni Rial','code' => 'YER','status' => '1'),
            array('id' => '155','name' => 'Yuan (Chinese) Renminbi','code' => 'CNY','status' => '1'),
            array('id' => '156','name' => 'Zaire Zaire','code' => 'ZRZ','status' => '1'),
            array('id' => '157','name' => 'Zambian Kwacha','code' => 'ZMK','status' => '1'),
            array('id' => '158','name' => 'Zimbabwe Dollar','code' => 'ZWD','status' => '1'),
        );
        DB::table('currencies')->insert($currencies);
    }
}
