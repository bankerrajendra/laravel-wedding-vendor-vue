<?php

use Illuminate\Database\Seeder;
use App\Models\Religion;

class ReligionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Schema::disableForeignKeyConstraints();
        Religion::truncate();
        Schema::enableForeignKeyConstraints();
        $religions = array(
            array('id'=>'1','old_id'=>'30','religion'=>'Islamic'),
            array('id'=>'2','old_id'=>'32','religion'=>'Hindu'),
            array('id'=>'3','old_id'=>'34','religion'=>'Muslim'),
            array('id'=>'4','old_id'=>'39','religion'=>'Christian'),
            array('id'=>'5','old_id'=>'40','religion'=>'Jewish'),
            array('id'=>'6','old_id'=>'41','religion'=>'Jain'),
            array('id'=>'7','old_id'=>'42','religion'=>'Buddhist'),
            array('id'=>'8','old_id'=>'43','religion'=>'Sikh'),
            array('id'=>'9','old_id'=>'44','religion'=>'Catholic'),
            array('id'=>'10','old_id'=>'57','religion'=>'Parsi'),
        );
        DB::table('religions')->insert($religions);
    }
}
