<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReplyActiveFieldsToReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->enum('user_deleted', ['0','1'])->after('description')->default('0');
            $table->string('to_user_reply', 250)->nullable()->after('description');
            $table->string('admin_reply', 250)->nullable()->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function (Blueprint $table) {
            $table->dropColumn('admin_reply');
            $table->dropColumn('to_user_reply');
            $table->dropColumn('user_deleted');
        });
    }
}
