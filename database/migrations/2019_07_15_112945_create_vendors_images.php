<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('vendors_images')) {
			Schema::create('vendors_images', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('vendor_id')->unsigned();
				$table->foreign('vendor_id')->references('id')->on('users')->onDelete('cascade');
				$table->string('image_thumb')->nullable();
				$table->string('image_full')->nullable();
				$table->enum('is_profile_image',['Y','N'])->default('N');
				$table->enum('image_type',['cover','profile'])->default('profile');
				$table->enum('is_approved',['Y','N'])->default('N');
				$table->string('disapprove_reason',200)->nullable();
				$table->timestamps();
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		if(!Schema::hasTable('vendors_images')){
			Schema::dropIfExists('vendors_images');
		}
    }
}
