<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_information', function (Blueprint $table) {
            $table->dropColumn('drink');
            $table->dropColumn('smoke');
            $table->dropColumn('food');
            $table->dropForeign('users_information_education_id_foreign');
            $table->dropForeign('users_information_profession_id_foreign');
            $table->dropColumn('education_id');
            $table->dropColumn('profession_id');
            $table->dropColumn('about');
            $table->dropColumn('ethnicity');
            $table->dropColumn('country_code');
            $table->dropColumn('mobile_number');
            $table->dropColumn('number_verification_code');
            $table->dropColumn('number_verified');
            $table->dropColumn('instant_alert_notification');
            $table->dropColumn('activity_notification');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('religion_id');
            $table->dropColumn('sect_id');
            $table->dropColumn('sub_cast_id');
            $table->dropColumn('isReligionFlagged');
            $table->dropColumn('height');
            $table->dropColumn('looking_for');
            $table->dropColumn('does_pray');
            $table->dropColumn('is_born');
            $table->dropColumn('marital_status');
            $table->dropColumn('complexion');
            $table->dropColumn('eye_color');
            $table->dropColumn('body_type');
            $table->dropColumn('weight');
            $table->dropColumn('weight_measure');
            $table->dropColumn('horoscope');
            $table->dropColumn('language_id');
            $table->dropColumn('currency');
            $table->dropColumn('annual_income');
            $table->dropColumn('zipcode');
            $table->dropColumn('access_grant');
            $table->dropColumn('profile_created_by');
            $table->dropColumn('preferred_movies');
            $table->dropColumn('sports');
            $table->dropColumn('hobbies');
            $table->dropColumn('interests');
            $table->dropColumn('favourite_music');
            $table->dropColumn('favourite_reads');
            $table->dropColumn('native_place');
            $table->dropColumn('father_occupation_id');
            $table->dropColumn('mother_occupation_id');
            $table->dropColumn('number_of_sister');
            $table->dropColumn('number_of_brother');
            $table->dropColumn('family_location');
            $table->dropColumn('family_type');
            $table->dropColumn('spoken_languages');
            $table->dropColumn('body_type_show');
            $table->dropColumn('weight_show');
            $table->dropColumn('complexion_show');
            $table->dropColumn('horoscope_show');
            $table->dropColumn('sect_show');
            $table->dropColumn('subcaste_show');
            $table->dropColumn('notify_sends_message');
            $table->dropColumn('notify_favorites_me');
            $table->dropColumn('notify_views_profile');
            $table->dropColumn('notify_content_approve_deny');
            $table->dropColumn('notify_new_matched_profiles');
            $table->dropColumn('notify_news_letter');
            $table->dropColumn('notified_membership_renewal_five_days');
            $table->dropColumn('notified_membership_renewal_two_days');
            $table->date('event_date')->after('user_id');
            $table->string('budget', 50)->after('event_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_information', function (Blueprint $table) {
            $table->dropColumn('event_date');
            $table->dropColumn('budget');
        });
    }
}
