<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveForeignPlanIdPaidMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paid_members', function (Blueprint $table) {
            $table->dropForeign('paid_members_plan_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paid_members', function (Blueprint $table) {
            $table->foreign('plan_id')->references('id')->on('membership_plans');
        });
    }
}
