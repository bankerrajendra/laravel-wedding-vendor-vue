<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaidMembersOrdersInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paid_members_orders_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paid_membership_id')->unsigned();
            $table->foreign('paid_membership_id')->references('id')->on('paid_members');
            $table->integer('transaction_id')->unsigned();
            $table->string('transaction_type', 20)->default('purchase')->nullable();
            $table->dateTime('transaction_date_time');
            $table->integer('card_user_country')->unsigned();
            $table->integer('card_user_phone_country_code')->unsigned();
            $table->string('card_user_mobile_number', 30)->nullable();
            $table->string('card_holder_name', 200)->nullable();
            $table->string('card_billing_add_1', 200)->nullable();
            $table->string('card_billing_add_2', 200)->nullable();
            $table->string('card_billing_city', 200)->nullable();
            $table->integer('card_billing_state')->unsigned();
            $table->integer('card_billing_zip_code')->unsigned();
            $table->double('transaction_amount');
            $table->string('currency', 25)->nullable();
            $table->string('card_number', 30)->nullable();
            $table->string('card_type', 25)->nullable();
            $table->string('approval_code', 25)->nullable();
            $table->string('order_number', 50)->nullable();
            $table->string('customer_code', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paid_members_orders_info');
    }
}
