<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('age_from')->nullable();
            $table->integer('age_to')->nullable();


            $table->integer('height_from')->nullable();
            $table->integer('height_to')->nullable();
            $table->string('drink',250)->nullable();
            $table->string('smoke',250)->nullable();
            $table->string('diet',250)->nullable();
            $table->string('religion',250)->nullable();
            $table->string('education',250)->nullable();
            $table->string('body_type',250)->nullable();
            $table->string('marital_status',50)->nullable();
            $table->char('have_children',2)->nullable();
            $table->string('community',150)->nullable();
            $table->integer('mother_tongue')->nullable();
            $table->char('manglik',1)->nullable();

            $table->string('country_living',250)->nullable();
            $table->string('state_living',250)->nullable();
            $table->string('city_district',250)->nullable();
            $table->string('professional_area',250)->nullable();
            $table->string('annual_income',250)->nullable();
            $table->string('currency',250)->nullable();
            $table->string('skin_tone',250)->nullable();
            $table->string('eye_color',250)->nullable();
            $table->string('languages',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_preferences');
    }
}
