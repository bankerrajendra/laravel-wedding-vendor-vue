<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWebSocialLinksToVendoreventinformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_events_information', function (Blueprint $table) {
            $table->string('facebook','250')->nullable()->after('video_url');
            $table->string('instagram','250')->nullable()->after('video_url');
            $table->string('twitter','250')->nullable()->after('video_url');
            $table->string('pinterest', '250')->nullable()->after('video_url');
            $table->string('linkedin', '250')->nullable()->after('video_url');
            $table->string('business_website','250')->nullable()->after('video_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_events_information', function (Blueprint $table) {
            $table->dropColumn('business_website');
            $table->dropColumn('facebook');
            $table->dropColumn('instagram');
            $table->dropColumn('twitter');
            $table->dropColumn('pinterest');
            $table->dropColumn('linkedin');
        });
    }
}
