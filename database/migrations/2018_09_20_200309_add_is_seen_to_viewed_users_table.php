<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSeenToViewedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('viewed_users', function (Blueprint $table) {
            //
            $table->boolean('is_seen')->default(0)->after('to_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('viewed_users', function (Blueprint $table) {
            //
            $table->dropColumn('is_seen');
        });
    }
}
