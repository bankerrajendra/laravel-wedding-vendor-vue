<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAmountDurationFromPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('membership_plans', function (Blueprint $table) {
            // added image new field for store icon name
            $table->dropColumn('plan_amount');
            $table->dropColumn('duration');
            $table->dropColumn('duration_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('membership_plans', function (Blueprint $table) {
            $table->double('plan_amount');
            $table->integer('duration')->nullable();
            $table->enum("duration_type",['Days', 'Month'])->nullable();
        });
    }
}
