<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorTypeVendorsCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_categories', function (Blueprint $table) {
            $table->integer('vendor_type')->nullable()->default(1)->after('display_in_footer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_categories', function (Blueprint $table) {
            $table->dropColumn('vendor_type');
        });
    }
}
