<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_user')->unsigned();
            $table->foreign('from_user')->references('id')->on('users')->onDelete('cascade');
            $table->integer('to_user')->unsigned();
            $table->foreign('to_user')->references('id')->on('users')->onDelete('cascade');
            $table->enum('notification_type',['private_photo_request','share_photo','profile_photo_request'])->nullable();
            $table->enum('status',['A','P','D'])->nullable();    // Accepted, Pending, Declined
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_notifications');
    }
}
