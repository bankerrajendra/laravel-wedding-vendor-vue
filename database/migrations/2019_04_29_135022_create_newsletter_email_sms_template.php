<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsletterEmailSmsTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletter_email_sms_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['news-letter', 'email', 'sms'])->nullable();
            $table->string('name', 250)->nullable();
            $table->longText('description')->nullable();
            $table->enum('status', ['A', 'I'])->nullable();
            $table->integer('site_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletter_email_sms_templates');
    }
}
