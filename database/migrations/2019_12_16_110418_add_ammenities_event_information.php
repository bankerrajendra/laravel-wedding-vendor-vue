<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmmenitiesEventInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_events_information', function (Blueprint $table) {
            $table->boolean('disable_seating')->default(0)->after('video_url');
            $table->boolean('babysitting_services')->default(0)->after('video_url');
            $table->boolean('drinks_for_sale')->default(0)->after('video_url');
            $table->boolean('food_for_sale')->default(0)->after('video_url');
            $table->boolean('parking')->default(0)->after('video_url');
            $table->boolean('online_seat_selection')->default(0)->after('video_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_events_information', function (Blueprint $table) {
            $table->dropColumn('disable_seating');
            $table->dropColumn('babysitting_services');
            $table->dropColumn('drinks_for_sale');
            $table->dropColumn('food_for_sale');
            $table->dropColumn('parking');
            $table->dropColumn('online_seat_selection');
        });
    }
}
