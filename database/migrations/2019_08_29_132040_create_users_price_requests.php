<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPriceRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_price_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('first_name','25')->nullable();
            $table->string('last_name','25')->nullable();
            $table->integer('mobile_number')->unsigned();
            $table->date('event_date');
            $table->enum('preferred_contact_method', ['email','phone','message'])->default('email');
            $table->text('about_extra_needs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_price_requests');
    }
}
