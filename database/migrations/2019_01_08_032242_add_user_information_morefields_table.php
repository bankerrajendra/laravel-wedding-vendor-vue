<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserInformationMorefieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		if(!Schema::hasTable('users_information')){
         Schema::table('users_information', function (Blueprint $table) {
             $table->integer('religion_id')->unsigned()->nullable();
             $table->integer('sect_id')->unsigned()->nullable();
             $table->integer('sub_cast_id')->unsigned()->nullable();
             $table->string('isReligionFlagged', 45)->nullable();
             $table->string('height',20)->nullable();
             $table->text('looking_for')->nullable();
             $table->enum('does_pray',['always','never','sometimes'])->nullable();
             $table->enum('is_born',['yes','reverted'])->nullable();
             $table->enum('marital_status',['single','married','widowed','separated','divorced','never_married','annulled'])->nullable();
         });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
