<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfileUpdatedToUserProfileFlags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profile_flags', function (Blueprint $table) {
            $table->enum('profile_updated', ['0','1'])->after('status')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profile_flags', function (Blueprint $table) {
            $table->dropColumn('profile_updated');
        });
    }
}
