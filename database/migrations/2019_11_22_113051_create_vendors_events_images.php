<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsEventsImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('vendors_events_images')) {
			Schema::create('vendors_events_images', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('event_id')->unsigned();
				$table->string('image_thumb')->nullable();
				$table->string('image_full')->nullable();
				$table->enum('is_profile_image',['Y','N'])->default('Y');
				$table->enum('image_type',['cover','profile'])->default('profile');
				$table->enum('is_approved',['Y','N'])->default('N');
                $table->string('disapprove_reason',200)->nullable();
                $table->enum("is_new", ['0', '1'])->default('1');
				$table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasTable('vendors_events_images')){
			Schema::dropIfExists('vendors_events_images');
		}
    }
}
