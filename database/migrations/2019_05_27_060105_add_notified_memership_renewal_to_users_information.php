<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotifiedMemershipRenewalToUsersInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_information', function (Blueprint $table) {
            $table->enum('notified_membership_renewal_five_days',['N','Y'])->default('N');
            $table->enum('notified_membership_renewal_two_days',['N','Y'])->default('N');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_information', function (Blueprint $table) {
            $table->dropColumn('notified_membership_renewal_five_days');
            $table->dropColumn('notified_membership_renewal_two_days');
        });
    }
}
