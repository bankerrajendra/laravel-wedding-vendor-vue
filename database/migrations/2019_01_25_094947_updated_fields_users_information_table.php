<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatedFieldsUsersInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
       	if(!Schema::hasTable('users_information')){
        Schema::table('users_information', function (Blueprint $table) {
            $table->string('spoken_languages', 20)->nullable();
			 
        });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			if(!Schema::hasTable('users_information')){
          Schema::table('users_information', function (Blueprint $table) {
            $table->dropColumn('spoken_languages');
             
			 
        });
			}
    }
}
