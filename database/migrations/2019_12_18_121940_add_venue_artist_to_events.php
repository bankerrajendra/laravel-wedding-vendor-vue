<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVenueArtistToEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_events_information', function (Blueprint $table) {
            $table->string('venue','250')->nullable()->after('event_name');
            $table->string('artists','250')->nullable()->after('video_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_events_information', function (Blueprint $table) {
            $table->dropColumn('venue');
            $table->dropColumn('artists');
        });
    }
}
