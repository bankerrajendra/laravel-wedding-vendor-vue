<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsCategorySatellitesInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors_category_satellites_information', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_category_id')->nullable();
            $table->integer('satellite_id')->nullable();
            $table->string('title','250');
            $table->text('keywords');
            $table->text('description');
            $table->text('meta_description');
            $table->string('icon_image', '250');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors_category_satellites_information');
    }
}
