<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors_information', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->nullable();
            $table->string('company_name','250')->nullable();
            $table->integer('vendor_category')->nullable();
            $table->text('business_information')->nullable();
            $table->boolean('display_address')->default(1);
            $table->boolean('display_business_number')->default(1);
            $table->string('business_website','250')->nullable();
            $table->string('facebook','250')->nullable();
            $table->string('instagram','250')->nullable();
            $table->string('twitter','250')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors_information');
    }
}
