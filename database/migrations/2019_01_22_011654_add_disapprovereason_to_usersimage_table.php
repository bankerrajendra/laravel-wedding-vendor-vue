<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDisapprovereasonToUsersimageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_images', function (Blueprint $table) {
            //
            $table->string('disapprove_reason',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_images', function (Blueprint $table) {
            //
            $table->dropColumn('disapprove_reason');
        });
    }
}
