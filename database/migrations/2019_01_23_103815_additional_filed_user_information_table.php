<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalFiledUserInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if(!Schema::hasTable('users_information')){
        Schema::table('users_information', function (Blueprint $table) {
            $table->string('native_place', 120)->nullable();
			$table->integer('father_occupation_id')->nullable();
			$table->integer('mother_occupation_id')->nullable();
			$table->integer('number_of_sister')->nullable();
			$table->integer('number_of_brother')->nullable();
			$table->string("family_location", 120)->nullable();
			$table->enum("family_type",['joint','nuclear'])->nullable();
        });
	   }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		if(!Schema::hasTable('users_information')){
          Schema::table('users_information', function (Blueprint $table) {
            $table->dropColumn('native_place');
            $table->dropColumn('father_occupation_id');
			$table->dropColumn('mother_occupation_id');
			$table->dropColumn('number_of_sister');
			$table->dropColumn('number_of_brother');
			$table->dropColumn('family_location');
			$table->dropColumn('family_type');
			 
        });
		}
    }
}
