<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSeenToFavouritedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('favourited_users', function (Blueprint $table) {
            //
            $table->boolean('is_seen')->default(0)->after('to_user_status');
            $table->boolean('is_from_seen')->default(0)->after('is_seen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('favourited_users', function (Blueprint $table) {
            //
            $table->dropColumn('is_seen');
            $table->dropColumn('is_from_seen');
        });
    }
}
