<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOthersInfoReligionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('others_info_religions', function (Blueprint $table) {
            $table->increments('id');
			$table->char('name',45)->nullable();
			$table->char('others_info_source',20)->nullable();
			//$table->integer('user_id');
            $table->foreign('user_id')->references('id')->on('users');	
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('others_info_religions');
    }
}
