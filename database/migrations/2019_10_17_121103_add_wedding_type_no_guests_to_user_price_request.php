<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWeddingTypeNoGuestsToUserPriceRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_price_requests', function (Blueprint $table) {
            $table->integer('no_of_guest')->nullable()->after('about_extra_needs');
            $table->enum('wedding_type', ['Hindu','Muslim','Sikh', 'Christian'])->default('Hindu')->nullable()->after('about_extra_needs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_price_requests', function (Blueprint $table) {
            $table->dropColumn('no_of_guest');
            $table->dropColumn('wedding_type');
        });
    }
}
