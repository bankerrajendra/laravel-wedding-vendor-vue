<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsEventsInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors_events_information', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->nullable();
            $table->string('event_name','250')->nullable();
            $table->string('address', 250)->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('state_id')->unsigned()->nullable();
			$table->integer('city_id')->unsigned()->nullable();
            $table->char('zip',18)->nullable();
            $table->text('description')->nullable();
            $table->text('ticket_information')->nullable();
            $table->text('categories')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->string('ticket_url','250')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->string('video_url','250')->nullable();
            $table->boolean('is_approved')->default(0);
            $table->boolean('is_paid')->default(0);
            $table->integer('site_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors_events_information');
    }
}
