<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageTypeToUsersImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(Schema::hasTable('users_images')){
			Schema::table('users_images', function (Blueprint $table) {
				 $table->enum('image_type',['public','private'])->default('private')->after('is_profile_image');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		if(!Schema::hasTable('users_images')){
			Schema::table('users_images', function (Blueprint $table) {
				 $table->dropColumn('image_type');
			});
		}
    }
}
