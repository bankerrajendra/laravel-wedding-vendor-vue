<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class ChangeEventDatesTimestampEventInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `vendors_events_information` CHANGE `start_date` `start_date` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';");
        DB::statement("ALTER TABLE `vendors_events_information` CHANGE `end_date` `end_date` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `vendors_events_information` CHANGE `start_date` `start_date` DATE NOT NULL DEFAULT '0000-00-00 00:00:00', CHANGE `end_date` `end_date` DATE NOT NULL DEFAULT '0000-00-00 00:00:00';");
    }
}
