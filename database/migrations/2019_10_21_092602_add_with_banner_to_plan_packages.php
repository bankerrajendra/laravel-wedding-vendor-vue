<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWithBannerToPlanPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('membership_plan_packages', function (Blueprint $table) {
            $table->enum('with_banner', ['0','1'])->default('0')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('membership_plan_packages', function (Blueprint $table) {
            $table->dropColumn('with_banner');
        });
    }
}
