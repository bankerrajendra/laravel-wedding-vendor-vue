<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHobbiesToUsersInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('users_images')){
        Schema::table('users_information', function (Blueprint $table) {
            //
            $table->char('preferred_movies',60)->nullable();
            $table->char('sports',60)->nullable();
            $table->char('hobbies',60)->nullable();
            $table->char('interests',60)->nullable();
            $table->char('favourite_music',60)->nullable();
            $table->char('favourite_reads',60)->nullable();

        });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		if(!Schema::hasTable('users_images')){
        Schema::table('users_information', function (Blueprint $table) {
            //
            $table->dropColumn('preferred_movies');
            $table->dropColumn('sports');
            $table->dropColumn('hobbies');
            $table->dropColumn('interests');
            $table->dropColumn('favourite_music');
            $table->dropColumn('favourite_reads');
        });
		}
    }
}