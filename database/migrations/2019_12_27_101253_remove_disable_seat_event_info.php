<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDisableSeatEventInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_events_information', function (Blueprint $table) {
            $table->dropColumn('disable_seating');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_events_information', function (Blueprint $table) {
            $table->boolean('disable_seating')->default(0)->after('babysitting_services');
        });
    }
}
