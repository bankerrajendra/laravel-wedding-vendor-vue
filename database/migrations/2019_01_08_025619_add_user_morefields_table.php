<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserMorefieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		if(!Schema::hasTable('users')){
        Schema::table('users', function (Blueprint $table) {
            $table->integer('state_id')->unsigned()->nullable();
            $table->integer('site_id')->unsigned()->nullable();

        });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('state_id');
            $table->dropColumn('site_id');
        });
    }
}
