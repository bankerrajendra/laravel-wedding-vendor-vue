<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateStatusMemberPlanPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `membership_plan_packages` CHANGE COLUMN `status` `status` ENUM('I', 'A', 'ADMIN') NOT NULL DEFAULT 'A'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `membership_plan_packages` CHANGE COLUMN `status` `status` ENUM('I', 'A') NOT NULL DEFAULT 'A'");
    }
}
