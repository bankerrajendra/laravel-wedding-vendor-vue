<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailSettingsToUserInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Email setting for notifications
         */
        Schema::table('users_information', function (Blueprint $table) {
            $table->enum('notify_sends_message',['N','Y'])->default('Y');
            $table->enum('notify_favorites_me',['N','Y'])->default('Y');
            $table->enum('notify_views_profile',['N','Y'])->default('Y');
            $table->enum('notify_content_approve_deny',['N','Y'])->default('Y');
            $table->enum('notify_news_letter',['N','Y'])->default('Y');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_information', function (Blueprint $table) {
            //
            $table->dropColumn('notify_sends_message');
            $table->dropColumn('notify_favorites_me');
            $table->dropColumn('notify_views_profile');
            $table->dropColumn('notify_content_approve_deny');
            $table->dropColumn('notify_news_letter');
        });
    }
}
