<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCategoryApproveFromVendorShowcase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_showcase', function (Blueprint $table) {
            $table->dropColumn('categories_approved');
            $table->dropColumn('disapprove_reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_showcase', function (Blueprint $table) {
            $table->enum('categories_approved', ['Y','N'])->default('N')->after('additional_categories');
            $table->string('disapprove_reason', 200)->nullable()->after('categories_approved');
        });
    }
}
