<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationsVendorsInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_information', function (Blueprint $table) {
            $table->enum("notify_sends_message", ['Y','N'])->default('Y')->nullable();
            $table->enum("notify_favorites_me", ['Y','N'])->default('Y')->nullable();
            $table->enum("notify_views_profile", ['Y','N'])->default('Y')->nullable();
            $table->enum("notify_content_approve_deny", ['Y','N'])->default('Y')->nullable();
            $table->enum("notify_news_letter", ['Y','N'])->default('Y')->nullable();
            $table->enum("notified_membership_renewal_five_days", ['Y','N'])->default('Y')->nullable();
            $table->enum("notified_membership_renewal_two_days", ['Y','N'])->default('Y')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_information', function (Blueprint $table) {
            $table->dropColumn('notify_sends_message');
            $table->dropColumn('notify_favorites_me');
            $table->dropColumn('notify_views_profile');
            $table->dropColumn('notify_content_approve_deny');
            $table->dropColumn('notify_news_letter');
            $table->dropColumn('notified_membership_renewal_five_days');
            $table->dropColumn('notified_membership_renewal_two_days');
        });
    }
}
