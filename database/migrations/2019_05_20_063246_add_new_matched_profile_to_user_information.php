<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewMatchedProfileToUserInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Email setting for notifications
         */
        Schema::table('users_information', function (Blueprint $table) {
            $table->enum('notify_new_matched_profiles',['N','Y'])->default('Y')->after('notify_content_approve_deny');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_information', function (Blueprint $table) {
            $table->dropColumn('notify_new_matched_profiles');
        });
    }
}
