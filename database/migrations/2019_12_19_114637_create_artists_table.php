<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name','250')->nullable();
            $table->integer('vendor_id')->default(0)->unsigned();
            $table->string('image_thumb')->nullable();
            $table->string('image_full')->nullable();
            $table->enum("is_new", ['0', '1'])->default('1');
            $table->enum('is_approved', ['Y','N'])->default('N');
            $table->string('disapprove_reason',200)->nullable();
            $table->integer('site_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
