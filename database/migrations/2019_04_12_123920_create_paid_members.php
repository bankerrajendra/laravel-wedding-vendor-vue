<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaidMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paid_members', function (Blueprint $table) {
            $table->increments('id');
            // Plan Related Information
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('membership_plans');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('plan_name', 30);
            $table->double('plan_amount');
            $table->string('currency',25)->nullable();
            $table->integer('duration')->nullable();
            $table->enum("duration_type",['Days', 'Month'])->nullable();
            $table->dateTime('membership_start_date');
            $table->dateTime('membership_end_date');
            // Order / Transaction Related Information
            $table->string('payment_gateway_name', 50)->nullable();
            $table->integer('site_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paid_members');
    }
}
