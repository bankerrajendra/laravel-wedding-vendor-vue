<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPackageIdPaidByPaidMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paid_members', function (Blueprint $table) {
            $table->enum('paid_by', ['User','Admin'])->after('user_id')->default('User');
            $table->integer('package_id')->after('user_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paid_members', function (Blueprint $table) {
            $table->dropColumn('paid_by');
            $table->dropColumn('package_id');
        });
    }
}
