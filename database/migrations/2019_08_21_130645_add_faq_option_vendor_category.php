<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFaqOptionVendorCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_categories', function (Blueprint $table) {
            $table->boolean('has_admin_faqs')->nullable()->default(1)->after('satellites');
            $table->boolean('has_custom_faqs')->nullable()->default(0)->after('has_admin_faqs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_categories', function (Blueprint $table) {
            $table->dropColumn('has_admin_faqs');
            $table->dropColumn('has_custom_faqs');
        });
    }
}
