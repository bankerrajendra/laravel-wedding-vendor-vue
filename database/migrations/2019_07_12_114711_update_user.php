<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('date_of_birth');
            $table->dropForeign('users_city_id_foreign');
            $table->dropColumn('city_id');
            $table->dropColumn('site_id');
            $table->string('city', 250)->after('country_id')->nullable();
            $table->string('mobile_country_code', 191)->after('city')->nullable();
            $table->string('mobile_number', 191)->after('mobile_country_code')->nullable();
            $table->string('address', 250)->after('mobile_number')->nullable();
            $table->char('zip',18)->after('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum("gender",['M', 'F'])->after('password')->nullable();
            $table->date('date_of_birth')->after('gender')->nullable();
            $table->integer('city_id')->unsigned()->after('country_id')->nullable();
            $table->integer('site_id')->unsigned()->after('state_id')->nullable();
            $table->dropColumn('city');
            $table->dropColumn('mobile_country_code');
            $table->dropColumn('mobile_number');
            $table->dropColumn('address');
            $table->dropColumn('zip');
        });
    }
}
