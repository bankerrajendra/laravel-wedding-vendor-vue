<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsVendorInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_information', function (Blueprint $table) {
            $table->string('pinterest', '250')->nullable()->after('instagram');
            $table->string('linkedin', '250')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_information', function (Blueprint $table) {
            $table->dropColumn('pinterest');
            $table->dropColumn('linkedin');
        });
    }
}
