<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempEventsImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('temporary_events_images')) {
			Schema::create('temporary_events_images', function (Blueprint $table) {
				$table->increments('id');
				$table->text('image_id')->nullable();
				$table->string('image_thumb')->nullable();
				$table->string('image_full')->nullable();
				$table->enum('image_type',['cover','profile'])->default('profile');
				$table->timestamps();
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasTable('temporary_events_images')){
			Schema::dropIfExists('temporary_events_images');
		}
    }
}
