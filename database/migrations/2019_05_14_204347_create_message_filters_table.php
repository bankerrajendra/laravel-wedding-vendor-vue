<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_filters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('age_from')->nullable();
            $table->integer('age_to')->nullable();
            $table->integer('height_from')->nullable();
            $table->integer('height_to')->nullable();
            $table->string('drink',50)->nullable();
            $table->string('smoke',50)->nullable();
            $table->string('diet',50)->nullable();
            $table->string('religion',100)->nullable();
            $table->string('body_type',250)->nullable();
            $table->enum('allow_without_images', ['0','1'])->default('1');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_filters');
    }
}
