<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationUsersInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_information', function (Blueprint $table) {
            $table->enum("notify_sends_message", ['Y','N'])->default('Y')->nullable();
            $table->enum("notify_news_letter", ['Y','N'])->default('Y')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_information', function (Blueprint $table) {
            $table->dropColumn('notify_sends_message');
            $table->dropColumn('notify_news_letter');
        });
    }
}
