<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan_name', 30);
            $table->string('plan_type',25)->nullable();
            $table->double('plan_amount');
            $table->string('currency',25)->nullable();
            $table->integer('duration')->nullable();
            $table->enum("duration_type",['Days', 'Month'])->nullable();
            $table->integer('public_photos_allowed')->default(1);
            $table->integer('private_photos_allowed')->default(1);
            $table->integer('msg_allowed')->nullable();
            $table->enum("status",['I','A'])->default('A');
            $table->string('membership_benefits',250);
            $table->integer('site_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_plans');
    }
}
