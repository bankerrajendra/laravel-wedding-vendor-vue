<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProfileFlags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('user_profile_flags')){
        Schema::create('user_profile_flags', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id');
			$table->char('property_name', 15)->nullable();
			$table->char('feedback', 20)->nullable();
			$table->char('table_remark', 4)->nullable();
            $table->timestamps();
        });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		if(!Schema::hasTable('user_profile_flags')){
			Schema::dropIfExists('user_profile_flags');
		}
    }
}
