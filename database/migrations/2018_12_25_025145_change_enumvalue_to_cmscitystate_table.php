<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEnumvalueToCmscitystateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_city_states', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE cms_city_States MODIFY page_type enum('Country','City','State','Language','Religion','Community') ");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_city_states', function (Blueprint $table) {
            DB::statement("ALTER TABLE cms_city_States MODIFY page_type enum('Country','City','State') ");
            //
        });
    }
}
