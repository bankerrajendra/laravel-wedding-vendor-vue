<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransactionidZipcodePaidMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paid_members_orders_info', function (Blueprint $table) {
            $table->string('transaction_id', 100)->nullable()->change();
            $table->string('card_billing_zip_code', 100)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paid_members_orders_info', function (Blueprint $table) {
            $table->integer('transaction_id')->unsigned()->change();
            $table->integer('card_billing_zip_code')->unsigned()->change();
        });
    }
}
