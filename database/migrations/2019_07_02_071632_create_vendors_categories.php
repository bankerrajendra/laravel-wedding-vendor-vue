<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name','250');
            $table->string('slug','250');
            $table->boolean('display_in_footer')->default(0);
            $table->boolean('status')->default(1);
            $table->text('satellites');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors_categories');
    }
}
