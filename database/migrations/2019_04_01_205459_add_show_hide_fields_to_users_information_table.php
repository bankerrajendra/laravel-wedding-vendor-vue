<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowHideFieldsToUsersInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_information', function (Blueprint $table) {
            //
            $table->enum('body_type_show',['N','Y'])->default('Y');
            $table->enum('weight_show',['N','Y'])->default('Y');
            $table->enum('complexion_show',['N','Y'])->default('Y');
            $table->enum('horoscope_show',['N','Y'])->default('Y');
            $table->enum('sect_show',['N','Y'])->default('Y');
            $table->enum('subcaste_show',['N','Y'])->default('Y');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_information', function (Blueprint $table) {
            //
            $table->dropColumn('body_type_show');
            $table->dropColumn('weight_show');
            $table->dropColumn('complexion_show');
            $table->dropColumn('horoscope_show');
            $table->dropColumn('sect_show');
            $table->dropColumn('subcaste_show');
        });
    }
}
