<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewVendorImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors_images', function (Blueprint $table) {
            //
            $table->enum("is_new", ['0', '1'])->default('1')->after('image_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors_images', function (Blueprint $table) {
            //
            $table->dropColumn('is_new');
        });
    }
}
