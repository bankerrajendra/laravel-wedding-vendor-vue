<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUsersInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
		if(!Schema::hasTable('users_information')){
        Schema::table('users_information', function (Blueprint $table) {
            $table->char('complexion',15)->nullable();
            $table->char('eye_color',15)->nullable();
			$table->char('body_type',15)->nullable();			
			$table->float('weight')->nullable();
			$table->enum('weight_measure',['lbs','kg'])->nullable();			
			$table->char('horoscope',15)->nullable();
			$table->integer('language_id')->unsigned()->nullable();
			$table->char('currency',10)->nullable();
			$table->char('annual_income',22)->nullable();
			$table->char('zipcode',18)->nullable();
			$table->enum('access_grant',['Y','N'])->nullable();
			$table->char('profile_created_by',15)->nullable();
			
        });

		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {       
		if(!Schema::hasTable('users_information')){	
        Schema::table('users_information', function (Blueprint $table) {
            $table->dropColumn('complexion');
            $table->dropColumn('eye_color');
			$table->dropColumn('body_type');
			$table->dropColumn('weight');
			$table->dropColumn('weight_measure');
			$table->dropColumn('horoscope');
			$table->dropColumn('language_id');
			$table->dropColumn('currency');
			$table->dropColumn('annual_income');
			$table->dropColumn('zipcode'); 
			$table->dropColumn('access_grant');
			$table->dropColumn('profile_created_by');
        });
		}
    }
}