<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationFieldsToUsersInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_information', function (Blueprint $table) {
            //
            $table->boolean('activity_notification')->default(1)->after('number_verified');
            $table->boolean('instant_alert_notification')->default(1)->after('number_verified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_information', function (Blueprint $table) {
            //
            $table->dropColumn('activity_notification');
            $table->dropColumn('instant_alert_notification');
        });
    }
}
