<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsCityStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_city_states', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('page_type', ['Country','City','State'])->nullable();
            $table->integer('type_id')->nullable();
            $table->string('slug',100)->nullable();
            $table->boolean('home_page_visibility')->default(1);
            $table->boolean('status')->default(1);
            $table->text('meta_title')->nullable();
            $table->text('meta_keyword')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('page_description')->nullable();
            $table->string('page_image',100)->nullable();
            $table->string('image_name', 60)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_city_states');
    }
}
